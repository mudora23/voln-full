{
    "id": "52515fa7-3521-4020-b0fe-e9d03cebed72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_Streets_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8028d8a7-8b52-4b5a-a656-21c39f6abbb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52515fa7-3521-4020-b0fe-e9d03cebed72",
            "compositeImage": {
                "id": "b8362f20-e025-4201-953c-5d40d98de63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8028d8a7-8b52-4b5a-a656-21c39f6abbb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269835ba-2645-4aaf-94e8-89f78c395401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8028d8a7-8b52-4b5a-a656-21c39f6abbb6",
                    "LayerId": "521c09d2-e835-4ed6-b9b8-3d379f566d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "521c09d2-e835-4ed6-b9b8-3d379f566d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52515fa7-3521-4020-b0fe-e9d03cebed72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}