{
    "id": "c52cbeb5-994a-4eac-aaf3-c571201bd57e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_polygon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 9,
    "bbox_right": 50,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bed41cd8-69df-49a3-9b43-89f87d28edd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c52cbeb5-994a-4eac-aaf3-c571201bd57e",
            "compositeImage": {
                "id": "7bf48335-1e81-4af8-a767-2752cddc3db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed41cd8-69df-49a3-9b43-89f87d28edd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7344c5b-326e-4a86-86db-3a228b03fbd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed41cd8-69df-49a3-9b43-89f87d28edd8",
                    "LayerId": "617ba5d9-bcd8-42fe-9c0e-2ffc88bb2744"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "617ba5d9-bcd8-42fe-9c0e-2ffc88bb2744",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c52cbeb5-994a-4eac-aaf3-c571201bd57e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 29,
    "yorig": 25
}