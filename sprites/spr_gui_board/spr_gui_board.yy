{
    "id": "df564e1b-66ae-455c-be94-9e279349b910",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_board",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 342,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c664d322-f020-4d7f-8b7b-f348a87ee7b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df564e1b-66ae-455c-be94-9e279349b910",
            "compositeImage": {
                "id": "9dc43dc2-211f-4203-b150-4ce3f9c50170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c664d322-f020-4d7f-8b7b-f348a87ee7b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d904d7f-040f-4bf1-b0b7-81a955106a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c664d322-f020-4d7f-8b7b-f348a87ee7b5",
                    "LayerId": "eee5c634-6d0b-48bb-a1bf-ba74e830748f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 343,
    "layers": [
        {
            "id": "eee5c634-6d0b-48bb-a1bf-ba74e830748f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df564e1b-66ae-455c-be94-9e279349b910",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}