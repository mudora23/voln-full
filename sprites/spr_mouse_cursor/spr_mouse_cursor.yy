{
    "id": "e3aeddde-3694-4985-b187-56b94cc32aac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1787326-6476-4528-be69-c9b246335774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3aeddde-3694-4985-b187-56b94cc32aac",
            "compositeImage": {
                "id": "7f9cb0b0-85e0-4476-accc-c1167af06b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1787326-6476-4528-be69-c9b246335774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf12fc4-e291-4dc8-8faa-772d33d0f2d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1787326-6476-4528-be69-c9b246335774",
                    "LayerId": "f4328f98-14a3-48b5-b87f-92712d336d26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "f4328f98-14a3-48b5-b87f-92712d336d26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3aeddde-3694-4985-b187-56b94cc32aac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 12,
    "yorig": 10
}