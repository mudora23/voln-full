{
    "id": "bc12f722-ddca-4528-a434-f5ef844d785a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_bot_right_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e8dfb5b-b0af-431b-865a-f23be4204e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc12f722-ddca-4528-a434-f5ef844d785a",
            "compositeImage": {
                "id": "8ae0c85c-ad67-49a9-9e8e-978bfd724643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e8dfb5b-b0af-431b-865a-f23be4204e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab0ed24-2045-4abb-a69f-2079a4dc88d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e8dfb5b-b0af-431b-865a-f23be4204e19",
                    "LayerId": "0b54c664-774e-40be-969c-56aaddf92666"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "0b54c664-774e-40be-969c-56aaddf92666",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc12f722-ddca-4528-a434-f5ef844d785a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 121,
    "yorig": 121
}