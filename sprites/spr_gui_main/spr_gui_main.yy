{
    "id": "28ef0398-55bd-48a7-add6-7796bcb896ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_main",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 438,
    "bbox_left": 0,
    "bbox_right": 1041,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28a9ecf2-ee2c-42dc-bfd2-94b9a2588ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28ef0398-55bd-48a7-add6-7796bcb896ef",
            "compositeImage": {
                "id": "14af0b3c-904b-49b4-ac64-ba1f3edf33aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a9ecf2-ee2c-42dc-bfd2-94b9a2588ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02b6d38c-e82d-48a5-816d-20f5170c2857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a9ecf2-ee2c-42dc-bfd2-94b9a2588ade",
                    "LayerId": "d42d06a6-fb32-4431-9120-282e328bd25d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 440,
    "layers": [
        {
            "id": "d42d06a6-fb32-4431-9120-282e328bd25d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28ef0398-55bd-48a7-add6-7796bcb896ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1042,
    "xorig": 0,
    "yorig": 0
}