{
    "id": "b866960e-3700-4dd4-a373-c01ed1cce006",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_left_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 142,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cde5dc3c-c850-48a6-ab24-1a551aa44f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b866960e-3700-4dd4-a373-c01ed1cce006",
            "compositeImage": {
                "id": "0b6d9a09-9f6c-42ef-9f12-737f54ba4656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cde5dc3c-c850-48a6-ab24-1a551aa44f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f20ab81e-6e00-44ce-b54c-afbf852d7c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cde5dc3c-c850-48a6-ab24-1a551aa44f73",
                    "LayerId": "66f6f350-174f-481e-bc9e-1ef795e4fc70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "66f6f350-174f-481e-bc9e-1ef795e4fc70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b866960e-3700-4dd4-a373-c01ed1cce006",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 143,
    "xorig": 26,
    "yorig": 24
}