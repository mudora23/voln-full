{
    "id": "99d45590-4777-4bef-a7cb-c9b4dba777a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_anon_male_m",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1413,
    "bbox_left": 172,
    "bbox_right": 881,
    "bbox_top": 47,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a916e31-231b-4a27-9769-84976c669bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99d45590-4777-4bef-a7cb-c9b4dba777a8",
            "compositeImage": {
                "id": "cade1cd4-81d8-48a4-b585-9e8d8169c062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a916e31-231b-4a27-9769-84976c669bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87127073-cac7-4c60-8564-a54e26060bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a916e31-231b-4a27-9769-84976c669bf6",
                    "LayerId": "1c76071f-52e6-4d0f-b7e8-bf6dc5476eb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1414,
    "layers": [
        {
            "id": "1c76071f-52e6-4d0f-b7e8-bf6dc5476eb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99d45590-4777-4bef-a7cb-c9b4dba777a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 985,
    "xorig": 0,
    "yorig": 0
}