{
    "id": "5b22f58a-4358-4085-8924-bd7c0fd06a63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ku_n_cs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1721,
    "bbox_left": 128,
    "bbox_right": 991,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd4c47d3-ee0a-4b0d-80ac-8d342a779192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b22f58a-4358-4085-8924-bd7c0fd06a63",
            "compositeImage": {
                "id": "4daa18bc-f690-43be-a9a6-f6b905c08e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4c47d3-ee0a-4b0d-80ac-8d342a779192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59444569-c424-4f76-a3d8-a2478046379b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4c47d3-ee0a-4b0d-80ac-8d342a779192",
                    "LayerId": "e5375add-67b7-4a18-81bb-0396afe79a14"
                }
            ]
        },
        {
            "id": "960dd6c4-3544-4b43-9d13-eef12396e350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b22f58a-4358-4085-8924-bd7c0fd06a63",
            "compositeImage": {
                "id": "87ad125e-1419-46a4-87f2-5400c7dc30b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960dd6c4-3544-4b43-9d13-eef12396e350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a505fe51-d491-4cba-ade9-13221860d200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960dd6c4-3544-4b43-9d13-eef12396e350",
                    "LayerId": "e5375add-67b7-4a18-81bb-0396afe79a14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1779,
    "layers": [
        {
            "id": "e5375add-67b7-4a18-81bb-0396afe79a14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b22f58a-4358-4085-8924-bd7c0fd06a63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1086,
    "xorig": 0,
    "yorig": 0
}