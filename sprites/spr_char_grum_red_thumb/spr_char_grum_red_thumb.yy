{
    "id": "73337468-38ea-4189-bd16-285acc9cb87b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_red_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ac916eb-ff3c-44b5-8b05-2ccabc0838b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73337468-38ea-4189-bd16-285acc9cb87b",
            "compositeImage": {
                "id": "f9def424-bd67-4baf-a4be-23cbf091e130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ac916eb-ff3c-44b5-8b05-2ccabc0838b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8433391-909c-4d80-aecc-f29c15fdd7d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ac916eb-ff3c-44b5-8b05-2ccabc0838b0",
                    "LayerId": "6224b4a5-46a3-4837-b82e-ff2cfab3e6fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "6224b4a5-46a3-4837-b82e-ff2cfab3e6fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73337468-38ea-4189-bd16-285acc9cb87b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}