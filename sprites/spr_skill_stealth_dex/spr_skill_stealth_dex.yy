{
    "id": "c9e78ef0-9e47-460e-939a-6ca52484f0e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_stealth_dex",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff0978f5-010c-4e86-a1f6-9e10edaf5146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e78ef0-9e47-460e-939a-6ca52484f0e7",
            "compositeImage": {
                "id": "ea53b17d-94ad-47ec-951d-bb6aba5b9f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0978f5-010c-4e86-a1f6-9e10edaf5146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc7cbc8-14dd-452e-9412-04288bbbb288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0978f5-010c-4e86-a1f6-9e10edaf5146",
                    "LayerId": "5b2b4baa-1c9c-48f0-a6b9-e9143cfced98"
                },
                {
                    "id": "fb747bbb-02f7-4f2e-9249-af0b9637b068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0978f5-010c-4e86-a1f6-9e10edaf5146",
                    "LayerId": "2f633ec6-fa6a-44c9-86f5-7a97e9276b12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "2f633ec6-fa6a-44c9-86f5-7a97e9276b12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9e78ef0-9e47-460e-939a-6ca52484f0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5b2b4baa-1c9c-48f0-a6b9-e9143cfced98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9e78ef0-9e47-460e-939a-6ca52484f0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}