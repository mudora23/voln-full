{
    "id": "30434ac9-0c05-4901-a224-620b7ad6f8d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_weight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 6,
    "bbox_right": 43,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33f78883-e7af-4ffd-a493-8a402cd72159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30434ac9-0c05-4901-a224-620b7ad6f8d3",
            "compositeImage": {
                "id": "8b2b6546-bc5c-4214-a339-b27b560b2144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f78883-e7af-4ffd-a493-8a402cd72159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe1f86a-4f64-480b-a282-abcfad6a68ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f78883-e7af-4ffd-a493-8a402cd72159",
                    "LayerId": "ac6d0086-87bb-4ad5-9367-e38d03ff0193"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "ac6d0086-87bb-4ad5-9367-e38d03ff0193",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30434ac9-0c05-4901-a224-620b7ad6f8d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}