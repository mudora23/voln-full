{
    "id": "46cc0eca-60c6-40c7-af9f-3ce922f884fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1970,
    "bbox_left": 0,
    "bbox_right": 1214,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a17b22e-e213-45f4-b76f-1d7ff7884101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46cc0eca-60c6-40c7-af9f-3ce922f884fd",
            "compositeImage": {
                "id": "45c9ed43-d2b7-4193-a843-913770533fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a17b22e-e213-45f4-b76f-1d7ff7884101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a908df-6264-40f8-b27e-7c59a41b8974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a17b22e-e213-45f4-b76f-1d7ff7884101",
                    "LayerId": "43efb49f-8c1b-4cdc-a3ac-42109d400ffe"
                }
            ]
        },
        {
            "id": "d409dec2-d435-401a-a871-0b82709488af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46cc0eca-60c6-40c7-af9f-3ce922f884fd",
            "compositeImage": {
                "id": "24e03db6-8583-47b5-aa98-511aa02d8f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d409dec2-d435-401a-a871-0b82709488af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b64a395-a820-4514-9d56-cf310eedcbf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d409dec2-d435-401a-a871-0b82709488af",
                    "LayerId": "43efb49f-8c1b-4cdc-a3ac-42109d400ffe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1971,
    "layers": [
        {
            "id": "43efb49f-8c1b-4cdc-a3ac-42109d400ffe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46cc0eca-60c6-40c7-af9f-3ce922f884fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1219,
    "xorig": 0,
    "yorig": 0
}