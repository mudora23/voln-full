{
    "id": "704e5376-0662-4678-a105-85d1a1e9652f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ku_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 8,
    "bbox_right": 299,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5f1fd78-168f-43db-bb64-e52e7f4fa0af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "704e5376-0662-4678-a105-85d1a1e9652f",
            "compositeImage": {
                "id": "88c4be70-e8ad-4a8e-937f-0f6b99de114e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f1fd78-168f-43db-bb64-e52e7f4fa0af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89f0d126-27e3-451a-afd4-740473ce71aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f1fd78-168f-43db-bb64-e52e7f4fa0af",
                    "LayerId": "d6180113-8c1f-4e68-996e-8ae69d3ab991"
                }
            ]
        },
        {
            "id": "f666e5fc-6453-4807-bf98-c82ff06ae1bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "704e5376-0662-4678-a105-85d1a1e9652f",
            "compositeImage": {
                "id": "b4c28740-28d4-45a5-a166-ecc2cf9e8cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f666e5fc-6453-4807-bf98-c82ff06ae1bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65df3338-37d2-40ae-86ac-ac16ffd80e90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f666e5fc-6453-4807-bf98-c82ff06ae1bf",
                    "LayerId": "d6180113-8c1f-4e68-996e-8ae69d3ab991"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "d6180113-8c1f-4e68-996e-8ae69d3ab991",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "704e5376-0662-4678-a105-85d1a1e9652f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}