{
    "id": "5bddecff-2197-4696-b072-be9ae60e5948",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_indigo_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "138793f9-9189-4849-9d19-1f84e0f3fd9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bddecff-2197-4696-b072-be9ae60e5948",
            "compositeImage": {
                "id": "18eecaaf-f4c5-45c2-855a-f6ecaa6f465a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "138793f9-9189-4849-9d19-1f84e0f3fd9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fa95e7-ba69-49ed-980c-a6cc622b9689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "138793f9-9189-4849-9d19-1f84e0f3fd9e",
                    "LayerId": "39ba2895-2d45-43b9-974d-c583af7a8a68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "39ba2895-2d45-43b9-974d-c583af7a8a68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bddecff-2197-4696-b072-be9ae60e5948",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}