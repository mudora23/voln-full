{
    "id": "e40aba2c-15c4-4deb-8032-41e69a1a3c48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_wreck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 188,
    "bbox_left": 7,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1fec6a4-cbda-4b0e-988d-0831843fa131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e40aba2c-15c4-4deb-8032-41e69a1a3c48",
            "compositeImage": {
                "id": "147e10a4-aa13-4db9-ab35-96d5bfe69f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1fec6a4-cbda-4b0e-988d-0831843fa131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "034cefbc-6101-4948-af85-b23e51a5bdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1fec6a4-cbda-4b0e-988d-0831843fa131",
                    "LayerId": "a341e5b1-7127-47e1-ba6a-b735b14465d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "a341e5b1-7127-47e1-ba6a-b735b14465d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e40aba2c-15c4-4deb-8032-41e69a1a3c48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}