{
    "id": "e5013043-b24b-46a0-ba13-91b3b03b4b71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Cult_RitualPit_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0add88e-1adf-40ac-a21c-149e9bc6b088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5013043-b24b-46a0-ba13-91b3b03b4b71",
            "compositeImage": {
                "id": "5bb36c7a-e988-4e8d-bc47-6bde5aeac149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0add88e-1adf-40ac-a21c-149e9bc6b088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4158844-d5e7-475e-af0a-20126d4d00b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0add88e-1adf-40ac-a21c-149e9bc6b088",
                    "LayerId": "f157b69e-f92d-4d7a-bffb-03e3a40e5763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "f157b69e-f92d-4d7a-bffb-03e3a40e5763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5013043-b24b-46a0-ba13-91b3b03b4b71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}