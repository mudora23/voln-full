{
    "id": "371fd973-0bba-46a3-85b4-b4e464ce922a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_4_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a4b549d-63b2-4fa5-9f7d-3140820ada43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "371fd973-0bba-46a3-85b4-b4e464ce922a",
            "compositeImage": {
                "id": "738b4d95-5dbd-4a02-991b-f2bb2681207e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4b549d-63b2-4fa5-9f7d-3140820ada43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9e538e-5642-401c-a29d-3f40905e797c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4b549d-63b2-4fa5-9f7d-3140820ada43",
                    "LayerId": "7e1d05a5-8690-417b-95e4-8ade4799a7a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "7e1d05a5-8690-417b-95e4-8ade4799a7a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "371fd973-0bba-46a3-85b4-b4e464ce922a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}