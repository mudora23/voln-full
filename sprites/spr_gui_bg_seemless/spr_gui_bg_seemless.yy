{
    "id": "413444ec-df91-4c5f-b77a-c6f154967ed0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_bg_seemless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 689,
    "bbox_left": 0,
    "bbox_right": 689,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66b4143a-c8ea-4562-845d-3cc3301ae6ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "413444ec-df91-4c5f-b77a-c6f154967ed0",
            "compositeImage": {
                "id": "66ccc9a3-9d43-4ac3-b831-5f117ecd30e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b4143a-c8ea-4562-845d-3cc3301ae6ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e49f0aa-cf88-4d3b-9a47-949926ff8579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b4143a-c8ea-4562-845d-3cc3301ae6ef",
                    "LayerId": "9923b7a4-1e2a-4251-a6ff-643ea02ddbd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 690,
    "layers": [
        {
            "id": "9923b7a4-1e2a-4251-a6ff-643ea02ddbd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "413444ec-df91-4c5f-b77a-c6f154967ed0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 690,
    "xorig": 0,
    "yorig": 0
}