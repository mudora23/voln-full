{
    "id": "4186ac8e-6dc0-4635-a3c8-bfb02c8cf033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Nest_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d33ba7a8-fe68-42ab-971e-267d2d1f6b36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4186ac8e-6dc0-4635-a3c8-bfb02c8cf033",
            "compositeImage": {
                "id": "deab57b1-3ed8-4327-bdae-487afdae143a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33ba7a8-fe68-42ab-971e-267d2d1f6b36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992ce779-82d2-4881-8efa-69c55a249151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33ba7a8-fe68-42ab-971e-267d2d1f6b36",
                    "LayerId": "ce392563-e0b1-4175-b86e-4d7d7546b101"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "ce392563-e0b1-4175-b86e-4d7d7546b101",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4186ac8e-6dc0-4635-a3c8-bfb02c8cf033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}