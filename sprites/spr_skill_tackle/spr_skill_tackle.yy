{
    "id": "d7e109cc-79b5-49f3-be68-c62594ae3bf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_tackle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eef2e8b8-2fd2-4061-8bff-7544e7193ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e109cc-79b5-49f3-be68-c62594ae3bf3",
            "compositeImage": {
                "id": "17c516e9-9dbf-464f-9a39-c6d1694c6d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef2e8b8-2fd2-4061-8bff-7544e7193ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb50870-842d-4e15-baf8-af2e2868b6a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef2e8b8-2fd2-4061-8bff-7544e7193ba4",
                    "LayerId": "82b0b6ab-1026-4b2d-a722-1b2ff4af87af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "82b0b6ab-1026-4b2d-a722-1b2ff4af87af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7e109cc-79b5-49f3-be68-c62594ae3bf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}