{
    "id": "7e6a4842-097d-4fb1-82dc-9da1ef2889bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_orange_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "245e6eb2-70ae-4f18-a967-8216df4ac3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e6a4842-097d-4fb1-82dc-9da1ef2889bb",
            "compositeImage": {
                "id": "5635e90a-bcdb-4683-bf06-9aaeb11d5a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245e6eb2-70ae-4f18-a967-8216df4ac3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9d6c1d-9b65-4414-bdbc-ab6a509fc532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245e6eb2-70ae-4f18-a967-8216df4ac3dc",
                    "LayerId": "c73b54df-cf6f-496c-86f2-514d5d6fa6b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "c73b54df-cf6f-496c-86f2-514d5d6fa6b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e6a4842-097d-4fb1-82dc-9da1ef2889bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}