{
    "id": "edafb3ab-6fa8-4bce-97f2-7c42d9aa5eef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Odmar_Castle_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81b2a9c4-bb08-449e-b670-de51bfbea6f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edafb3ab-6fa8-4bce-97f2-7c42d9aa5eef",
            "compositeImage": {
                "id": "57cad6df-a0a2-4694-86ac-a7e467f1b62c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b2a9c4-bb08-449e-b670-de51bfbea6f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8ff5761-f750-471a-b79b-267c872f6be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b2a9c4-bb08-449e-b670-de51bfbea6f9",
                    "LayerId": "27d6e40c-f62d-4dc1-91ac-1aa4a568554f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "27d6e40c-f62d-4dc1-91ac-1aa4a568554f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edafb3ab-6fa8-4bce-97f2-7c42d9aa5eef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}