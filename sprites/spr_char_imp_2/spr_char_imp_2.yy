{
    "id": "5e4567d5-e0f7-4800-bf18-65b9311f3adc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_imp_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1493,
    "bbox_left": 54,
    "bbox_right": 813,
    "bbox_top": 309,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04644e30-acef-4fb5-9d19-b91f45322e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4567d5-e0f7-4800-bf18-65b9311f3adc",
            "compositeImage": {
                "id": "3acd3676-3b00-4821-97b1-367a12a46a59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04644e30-acef-4fb5-9d19-b91f45322e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3efb06da-31be-4320-a23e-c306453fc2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04644e30-acef-4fb5-9d19-b91f45322e4a",
                    "LayerId": "4d10a0bf-04e8-4348-9e1c-a2d89c0695f0"
                }
            ]
        },
        {
            "id": "ae6583a7-998b-401e-b1a6-2a9c62d20768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4567d5-e0f7-4800-bf18-65b9311f3adc",
            "compositeImage": {
                "id": "e3f4b603-41a8-4384-aef9-d321074d11a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae6583a7-998b-401e-b1a6-2a9c62d20768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e72c9a-306e-4116-98d2-4f6ad3ec89ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae6583a7-998b-401e-b1a6-2a9c62d20768",
                    "LayerId": "4d10a0bf-04e8-4348-9e1c-a2d89c0695f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1520,
    "layers": [
        {
            "id": "4d10a0bf-04e8-4348-9e1c-a2d89c0695f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e4567d5-e0f7-4800-bf18-65b9311f3adc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 916,
    "xorig": 0,
    "yorig": 0
}