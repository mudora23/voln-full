{
    "id": "d3a3fc35-0b90-4ccb-8a63-4da13495b99a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd26b78b-aede-4c4a-9f72-ecfe4e3e186c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3a3fc35-0b90-4ccb-8a63-4da13495b99a",
            "compositeImage": {
                "id": "183c3c0a-f58d-4de0-a8f4-5c9caa1561c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd26b78b-aede-4c4a-9f72-ecfe4e3e186c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce17e55-c721-4ed4-aa4c-24057447665b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd26b78b-aede-4c4a-9f72-ecfe4e3e186c",
                    "LayerId": "c0ad044e-2be4-4ae1-a141-52f58eba998f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "c0ad044e-2be4-4ae1-a141-52f58eba998f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3a3fc35-0b90-4ccb-8a63-4da13495b99a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}