{
    "id": "d55beff9-03d0-495b-ac73-97157a664672",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Yim_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 14,
    "bbox_right": 299,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc9139be-1b88-4c95-ba98-5575b2367e84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d55beff9-03d0-495b-ac73-97157a664672",
            "compositeImage": {
                "id": "3f56e046-e6be-47eb-a3d2-b35864f86c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc9139be-1b88-4c95-ba98-5575b2367e84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282d8579-a673-4ab6-9a24-cfe7d4dd8231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc9139be-1b88-4c95-ba98-5575b2367e84",
                    "LayerId": "77efe891-5b52-47b8-96c9-d06e79cabcd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "77efe891-5b52-47b8-96c9-d06e79cabcd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d55beff9-03d0-495b-ac73-97157a664672",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}