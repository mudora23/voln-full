{
    "id": "e3e8d7ed-f716-463d-b718-db5dada07d70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_5_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14c7c97a-84f0-4935-bdf8-5055d421b4fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3e8d7ed-f716-463d-b718-db5dada07d70",
            "compositeImage": {
                "id": "81e24302-885d-45a8-9ad0-34a5a20ca9c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c7c97a-84f0-4935-bdf8-5055d421b4fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e418f0de-6d93-4766-9914-ef082a28718a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c7c97a-84f0-4935-bdf8-5055d421b4fe",
                    "LayerId": "07f343c3-0d91-44e3-b6cd-80708b458bb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "07f343c3-0d91-44e3-b6cd-80708b458bb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3e8d7ed-f716-463d-b718-db5dada07d70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}