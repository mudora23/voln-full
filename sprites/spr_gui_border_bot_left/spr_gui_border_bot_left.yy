{
    "id": "27c9b213-e9d8-4a24-9acb-573252bd5c79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80b0a442-abb6-4509-a5e6-0437a51cefdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27c9b213-e9d8-4a24-9acb-573252bd5c79",
            "compositeImage": {
                "id": "d7cef1c8-ad50-456e-87f0-31db08a6e17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b0a442-abb6-4509-a5e6-0437a51cefdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd2a215c-92e2-4c82-a890-c62b19e553bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b0a442-abb6-4509-a5e6-0437a51cefdd",
                    "LayerId": "a44311b6-304c-4a7c-a0aa-5f8291507b91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 145,
    "layers": [
        {
            "id": "a44311b6-304c-4a7c-a0aa-5f8291507b91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27c9b213-e9d8-4a24-9acb-573252bd5c79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 26,
    "yorig": 119
}