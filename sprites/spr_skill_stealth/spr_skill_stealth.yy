{
    "id": "9e7eb5de-7e4c-4ebb-9fb1-357703558587",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_stealth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f87fa633-ca85-4a1b-9da8-0c834cc84856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e7eb5de-7e4c-4ebb-9fb1-357703558587",
            "compositeImage": {
                "id": "1bfce535-b3d7-450f-80e7-4c2b86dbffe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87fa633-ca85-4a1b-9da8-0c834cc84856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7996c35f-aa64-4380-9fb9-4183fbcaccb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87fa633-ca85-4a1b-9da8-0c834cc84856",
                    "LayerId": "83fe9d72-2b36-4b58-a6d3-835d9e26808a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "83fe9d72-2b36-4b58-a6d3-835d9e26808a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e7eb5de-7e4c-4ebb-9fb1-357703558587",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}