{
    "id": "f62a1792-14ce-4a7b-9b01-06b5bbbc598b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_hide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 189,
    "bbox_left": 25,
    "bbox_right": 183,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d28c0dd9-f0d3-4f09-81ca-9b46c7db83ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62a1792-14ce-4a7b-9b01-06b5bbbc598b",
            "compositeImage": {
                "id": "fe6faaef-8741-492c-a962-8268412f0bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d28c0dd9-f0d3-4f09-81ca-9b46c7db83ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97b05cf-e74b-4324-8b46-4e0d30b82c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d28c0dd9-f0d3-4f09-81ca-9b46c7db83ec",
                    "LayerId": "3be24779-4f99-4918-970d-ff33fa379482"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "3be24779-4f99-4918-970d-ff33fa379482",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f62a1792-14ce-4a7b-9b01-06b5bbbc598b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}