{
    "id": "f340883f-52a5-4607-bf20-6d94e56a6129",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_polygon_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a075f3a6-7004-4a69-abcf-146cd3496ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f340883f-52a5-4607-bf20-6d94e56a6129",
            "compositeImage": {
                "id": "21d1c5fe-029a-4be2-8aad-b0d3266a0640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a075f3a6-7004-4a69-abcf-146cd3496ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c45a74a-3528-445e-b2f2-1f043fe36543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a075f3a6-7004-4a69-abcf-146cd3496ebb",
                    "LayerId": "e0dabcc0-7fcf-445a-9afb-9761af367ecb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "e0dabcc0-7fcf-445a-9afb-9761af367ecb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f340883f-52a5-4607-bf20-6d94e56a6129",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 35
}