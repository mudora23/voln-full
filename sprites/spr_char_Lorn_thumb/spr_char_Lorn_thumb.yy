{
    "id": "19e76c50-fcde-495e-9d72-4a888115871d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Lorn_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 41,
    "bbox_right": 252,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ed698a8-88b2-480e-ac60-b68cb5543fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19e76c50-fcde-495e-9d72-4a888115871d",
            "compositeImage": {
                "id": "11b8ca5f-4c72-4a1e-8516-975158cfe4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed698a8-88b2-480e-ac60-b68cb5543fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b279410a-05be-4be8-9ccd-540ccc377a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed698a8-88b2-480e-ac60-b68cb5543fb4",
                    "LayerId": "dac62786-9f8b-43c4-8d18-c2cc1215e40a"
                }
            ]
        },
        {
            "id": "228a9a37-cf3f-48ac-b52b-a1b6deaba15c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19e76c50-fcde-495e-9d72-4a888115871d",
            "compositeImage": {
                "id": "e3ccd7c7-94c8-4aeb-af76-dd291a1bf8ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228a9a37-cf3f-48ac-b52b-a1b6deaba15c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6389365-2599-46aa-b20b-05c5f7723ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228a9a37-cf3f-48ac-b52b-a1b6deaba15c",
                    "LayerId": "dac62786-9f8b-43c4-8d18-c2cc1215e40a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "dac62786-9f8b-43c4-8d18-c2cc1215e40a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19e76c50-fcde-495e-9d72-4a888115871d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}