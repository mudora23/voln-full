{
    "id": "10f123fa-152b-41c9-9857-49ca5ed69c76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_doublehit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ba65bed-5f15-4241-8a86-0c143aabe5b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10f123fa-152b-41c9-9857-49ca5ed69c76",
            "compositeImage": {
                "id": "502650c6-20b7-4915-a062-54652dba40e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba65bed-5f15-4241-8a86-0c143aabe5b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7c4c98-21f3-4fad-8593-986e87756d34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba65bed-5f15-4241-8a86-0c143aabe5b9",
                    "LayerId": "413cef74-b3a9-485e-9932-a6f6b3405c4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "413cef74-b3a9-485e-9932-a6f6b3405c4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10f123fa-152b-41c9-9857-49ca5ed69c76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}