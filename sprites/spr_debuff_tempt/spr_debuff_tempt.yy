{
    "id": "c26fa57c-f289-4add-98a5-210adb425ee7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_tempt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 3,
    "bbox_right": 46,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0bfad7d-d65d-4c09-bcf6-5413a2997487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c26fa57c-f289-4add-98a5-210adb425ee7",
            "compositeImage": {
                "id": "2aeb01aa-9beb-4cea-b228-3ef5924e65f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0bfad7d-d65d-4c09-bcf6-5413a2997487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e46f84f-3a9a-4037-a88c-c275bf507801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0bfad7d-d65d-4c09-bcf6-5413a2997487",
                    "LayerId": "787dd778-f2f8-4342-a518-e3575a6ec6bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "787dd778-f2f8-4342-a518-e3575a6ec6bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c26fa57c-f289-4add-98a5-210adb425ee7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}