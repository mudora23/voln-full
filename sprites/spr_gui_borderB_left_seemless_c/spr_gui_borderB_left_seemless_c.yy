{
    "id": "2b25bbac-96dc-4732-b6b4-15ba339fa25e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_left_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e26b5156-90bc-42f6-9af5-d063994dd676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b25bbac-96dc-4732-b6b4-15ba339fa25e",
            "compositeImage": {
                "id": "1ff57d24-9e3f-4dd8-af37-6301f3c84f90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26b5156-90bc-42f6-9af5-d063994dd676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4f3eba-fe26-46c1-bdf8-cf89bd9a5bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26b5156-90bc-42f6-9af5-d063994dd676",
                    "LayerId": "f3c085c8-7686-4877-9c66-66a3db58e502"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "f3c085c8-7686-4877-9c66-66a3db58e502",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b25bbac-96dc-4732-b6b4-15ba339fa25e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 11,
    "yorig": 18
}