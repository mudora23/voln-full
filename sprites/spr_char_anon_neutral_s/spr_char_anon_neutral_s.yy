{
    "id": "85e93be6-8fe8-4853-a4ea-8dd9ee316f1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_anon_neutral_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1727,
    "bbox_left": 76,
    "bbox_right": 1030,
    "bbox_top": 161,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f35d0c8a-5aee-457a-a104-262c173fb44f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e93be6-8fe8-4853-a4ea-8dd9ee316f1c",
            "compositeImage": {
                "id": "e15262b1-878f-477f-9f83-b0a5f764c8f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f35d0c8a-5aee-457a-a104-262c173fb44f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ea0d73-78ca-40e0-bf37-0c949fb1f9d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f35d0c8a-5aee-457a-a104-262c173fb44f",
                    "LayerId": "499b459f-e6f1-49b5-b413-2511c82b3a58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1730,
    "layers": [
        {
            "id": "499b459f-e6f1-49b5-b413-2511c82b3a58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85e93be6-8fe8-4853-a4ea-8dd9ee316f1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}