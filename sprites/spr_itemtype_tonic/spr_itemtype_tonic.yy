{
    "id": "8ce74631-77fa-4f09-a793-e9e44a1fb2ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_itemtype_tonic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c43f2dd-044d-423d-8f3d-52c9d387d7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ce74631-77fa-4f09-a793-e9e44a1fb2ee",
            "compositeImage": {
                "id": "85e1ba50-94c0-4a10-8c16-24dd7f224fee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c43f2dd-044d-423d-8f3d-52c9d387d7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d556bc-5a9b-4126-b723-865c36621609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c43f2dd-044d-423d-8f3d-52c9d387d7b3",
                    "LayerId": "e086c0b9-446e-429a-9197-6cebee3e75e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e086c0b9-446e-429a-9197-6cebee3e75e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ce74631-77fa-4f09-a793-e9e44a1fb2ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}