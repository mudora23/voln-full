{
    "id": "c5d5c5a5-c2ac-4a7c-a81d-272199ee8c09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_royalpurple_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf315303-8ff4-45e2-908f-158d320af807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d5c5a5-c2ac-4a7c-a81d-272199ee8c09",
            "compositeImage": {
                "id": "d0fe0eb1-b944-4710-9460-ef0e01fd91fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf315303-8ff4-45e2-908f-158d320af807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9c455a-752c-4f6c-8ee2-df833b33a287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf315303-8ff4-45e2-908f-158d320af807",
                    "LayerId": "3ab5bc66-ce7c-4e69-9558-b6e9e5a3d075"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "3ab5bc66-ce7c-4e69-9558-b6e9e5a3d075",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5d5c5a5-c2ac-4a7c-a81d-272199ee8c09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}