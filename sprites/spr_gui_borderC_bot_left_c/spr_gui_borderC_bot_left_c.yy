{
    "id": "88c4941f-24fe-45b6-b9a1-eb84d572dbbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_bot_left_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3f1b566-fc65-4a44-972a-e4d9dda46440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88c4941f-24fe-45b6-b9a1-eb84d572dbbf",
            "compositeImage": {
                "id": "9ee9a091-92ea-49e0-92f1-d8897dd23146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f1b566-fc65-4a44-972a-e4d9dda46440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9823101-70d7-4f1e-b90d-f955f94aa8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f1b566-fc65-4a44-972a-e4d9dda46440",
                    "LayerId": "ca6d5efa-d43b-4290-8f0d-0b1180a4ae54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "ca6d5efa-d43b-4290-8f0d-0b1180a4ae54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88c4941f-24fe-45b6-b9a1-eb84d572dbbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 13,
    "yorig": 121
}