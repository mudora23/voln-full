{
    "id": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d735b17-1238-423a-80a6-6b84e4a80ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
            "compositeImage": {
                "id": "4459681c-cccf-40ac-8963-0832b0db4ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d735b17-1238-423a-80a6-6b84e4a80ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521f7088-aa57-48d6-a23f-cc5cdbf98ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d735b17-1238-423a-80a6-6b84e4a80ec2",
                    "LayerId": "006b12d9-3d44-4b08-a963-d08372895cc6"
                }
            ]
        },
        {
            "id": "9b1abea2-c57b-46a3-b184-1b5c20fed2eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
            "compositeImage": {
                "id": "445cf1d7-272e-4282-8ab4-ca4d58f28f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b1abea2-c57b-46a3-b184-1b5c20fed2eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3fc905b-9eda-460f-b2e2-d0fa2e516ab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b1abea2-c57b-46a3-b184-1b5c20fed2eb",
                    "LayerId": "006b12d9-3d44-4b08-a963-d08372895cc6"
                }
            ]
        },
        {
            "id": "fd62a4c9-2b5c-4a7e-afdd-3148bc3dec90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
            "compositeImage": {
                "id": "fef71d92-fc43-469b-9cf1-721159a8de65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd62a4c9-2b5c-4a7e-afdd-3148bc3dec90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96154d2d-c62b-4f2b-9679-abe95e893ad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd62a4c9-2b5c-4a7e-afdd-3148bc3dec90",
                    "LayerId": "006b12d9-3d44-4b08-a963-d08372895cc6"
                }
            ]
        },
        {
            "id": "54b72b98-ec72-471a-9727-6933726ed6f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
            "compositeImage": {
                "id": "eb67634c-029e-458b-954b-bde5ef33d14a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b72b98-ec72-471a-9727-6933726ed6f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32577ccf-61a3-4a1d-ba27-78d86317c423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b72b98-ec72-471a-9727-6933726ed6f0",
                    "LayerId": "006b12d9-3d44-4b08-a963-d08372895cc6"
                }
            ]
        },
        {
            "id": "64da6adb-cc19-40a4-9e7f-b732cc826e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
            "compositeImage": {
                "id": "6566f4fb-b3f0-47ad-bfbc-b9cc615fba22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64da6adb-cc19-40a4-9e7f-b732cc826e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207848d5-aa06-4bdb-bd7f-f40dd7700a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64da6adb-cc19-40a4-9e7f-b732cc826e62",
                    "LayerId": "006b12d9-3d44-4b08-a963-d08372895cc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "006b12d9-3d44-4b08-a963-d08372895cc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ce36e0b-fca6-4277-a975-8ed0242cd433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}