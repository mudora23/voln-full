{
    "id": "a2173074-aac2-404f-a44e-777ffa700e28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_1_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 10,
    "bbox_right": 283,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1c12204-5537-4fc7-8bcb-4e1df429c654",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2173074-aac2-404f-a44e-777ffa700e28",
            "compositeImage": {
                "id": "9758aac2-dddc-4a4d-baf6-c702648f3cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c12204-5537-4fc7-8bcb-4e1df429c654",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2102edea-629a-4c91-b534-bf720c5ab741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c12204-5537-4fc7-8bcb-4e1df429c654",
                    "LayerId": "fd16e3ab-14ca-47ad-9d69-2b3e86b16d25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "fd16e3ab-14ca-47ad-9d69-2b3e86b16d25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2173074-aac2-404f-a44e-777ffa700e28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}