{
    "id": "ab479469-2445-4503-8d16-d2d5d1945104",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Zeyd_f_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1930,
    "bbox_left": 204,
    "bbox_right": 929,
    "bbox_top": 107,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a995caec-8318-44a9-886e-50c5247f2b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab479469-2445-4503-8d16-d2d5d1945104",
            "compositeImage": {
                "id": "f1afd45f-89e1-4250-9562-facf2b9e35f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a995caec-8318-44a9-886e-50c5247f2b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3063a0e-5f1f-4e52-9577-1e8e032345ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a995caec-8318-44a9-886e-50c5247f2b98",
                    "LayerId": "0aac8041-52ef-4fb1-9412-fc230617f463"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "0aac8041-52ef-4fb1-9412-fc230617f463",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab479469-2445-4503-8d16-d2d5d1945104",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}