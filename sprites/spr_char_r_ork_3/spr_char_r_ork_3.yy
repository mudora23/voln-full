{
    "id": "9f8d051d-49bd-4dad-86ef-6bf993a7ae89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2003,
    "bbox_left": 0,
    "bbox_right": 938,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3e51f12-08a0-4683-b4e2-db20d09a36f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8d051d-49bd-4dad-86ef-6bf993a7ae89",
            "compositeImage": {
                "id": "5a21ccdb-8aea-479e-9f2f-6d9bb954681a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e51f12-08a0-4683-b4e2-db20d09a36f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8894b29-c673-4e5e-966c-00b5246e23a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e51f12-08a0-4683-b4e2-db20d09a36f7",
                    "LayerId": "c7c0cc8c-cde6-4ae0-9ae5-2243b3178f66"
                }
            ]
        },
        {
            "id": "a545a437-f676-4558-954f-84b282d57c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8d051d-49bd-4dad-86ef-6bf993a7ae89",
            "compositeImage": {
                "id": "40bbb09c-dacd-4212-80d6-38af7a78cd46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a545a437-f676-4558-954f-84b282d57c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d53698c9-e665-48d5-9914-344eea567cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a545a437-f676-4558-954f-84b282d57c3d",
                    "LayerId": "c7c0cc8c-cde6-4ae0-9ae5-2243b3178f66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2004,
    "layers": [
        {
            "id": "c7c0cc8c-cde6-4ae0-9ae5-2243b3178f66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f8d051d-49bd-4dad-86ef-6bf993a7ae89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 939,
    "xorig": 0,
    "yorig": 0
}