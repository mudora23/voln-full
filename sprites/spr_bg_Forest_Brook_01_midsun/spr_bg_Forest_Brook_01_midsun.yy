{
    "id": "77192e22-1b86-4d31-a524-d7b709b34867",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Brook_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "004ee2fd-4f9d-484c-91a3-a609398731f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77192e22-1b86-4d31-a524-d7b709b34867",
            "compositeImage": {
                "id": "7f4229ee-5e8d-4a49-8e06-c60fc5efb187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "004ee2fd-4f9d-484c-91a3-a609398731f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a11b92-aa32-40a2-ae30-3e46a9fdad29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "004ee2fd-4f9d-484c-91a3-a609398731f8",
                    "LayerId": "b9a6d7d7-17cb-4238-a0c4-84adc5517736"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "b9a6d7d7-17cb-4238-a0c4-84adc5517736",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77192e22-1b86-4d31-a524-d7b709b34867",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}