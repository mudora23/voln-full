{
    "id": "ba86a792-4429-4ea1-b6ff-e73c7af327a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "smp_fogNormal",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9e3f472-055a-4ea3-916c-8223d4130d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba86a792-4429-4ea1-b6ff-e73c7af327a6",
            "compositeImage": {
                "id": "fc006fc7-37ff-400a-ba8f-be9c004e799c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9e3f472-055a-4ea3-916c-8223d4130d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec362f97-576b-457c-b30d-c15e25f6f3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9e3f472-055a-4ea3-916c-8223d4130d42",
                    "LayerId": "2e552363-9d73-45fb-beef-3f780ba51b85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2e552363-9d73-45fb-beef-3f780ba51b85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba86a792-4429-4ea1-b6ff-e73c7af327a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}