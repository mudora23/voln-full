{
    "id": "3915fcbc-0c76-4f84-ab70-689a98f858ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Cabin_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63431656-e060-4b0e-972a-1e31ea709dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3915fcbc-0c76-4f84-ab70-689a98f858ba",
            "compositeImage": {
                "id": "8928161e-417b-441e-aa29-e70ed56b1068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63431656-e060-4b0e-972a-1e31ea709dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2011710f-ca81-4461-a67d-e67cc263652c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63431656-e060-4b0e-972a-1e31ea709dc5",
                    "LayerId": "d2bcf518-8043-4877-916c-1684e157cc64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "d2bcf518-8043-4877-916c-1684e157cc64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3915fcbc-0c76-4f84-ab70-689a98f858ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}