{
    "id": "0949adc9-c1c4-48f9-8e1c-0bf6277229ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_delete_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb93ae61-3725-465c-bf3e-097e97d2b9c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0949adc9-c1c4-48f9-8e1c-0bf6277229ca",
            "compositeImage": {
                "id": "9bd74a6e-5f9d-4752-80cb-eca812317abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb93ae61-3725-465c-bf3e-097e97d2b9c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dad6805-d907-4340-af97-e698c79ee862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb93ae61-3725-465c-bf3e-097e97d2b9c2",
                    "LayerId": "cfc99397-f96a-4453-a5b4-e44e065d6ab8"
                }
            ]
        },
        {
            "id": "ad9d96d2-29e0-4a42-9f1c-34c882191b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0949adc9-c1c4-48f9-8e1c-0bf6277229ca",
            "compositeImage": {
                "id": "7ed22e1b-78a3-489d-b121-2cfd73fa0568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad9d96d2-29e0-4a42-9f1c-34c882191b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb12a169-a7e3-49b3-beca-1c3322d3c6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad9d96d2-29e0-4a42-9f1c-34c882191b58",
                    "LayerId": "cfc99397-f96a-4453-a5b4-e44e065d6ab8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "cfc99397-f96a-4453-a5b4-e44e065d6ab8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0949adc9-c1c4-48f9-8e1c-0bf6277229ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}