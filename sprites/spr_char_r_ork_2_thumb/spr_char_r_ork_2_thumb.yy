{
    "id": "e307a7eb-e9c6-432b-8e5a-9360cb7126c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_2_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 3,
    "bbox_right": 276,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c579aa53-b5ee-4a0e-b072-2fdb30d3c659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e307a7eb-e9c6-432b-8e5a-9360cb7126c8",
            "compositeImage": {
                "id": "0c9435c6-8fbb-4fc6-aa3a-96426ef1cd02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c579aa53-b5ee-4a0e-b072-2fdb30d3c659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08ee7670-9685-4596-bc67-c36aa5eba408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c579aa53-b5ee-4a0e-b072-2fdb30d3c659",
                    "LayerId": "20ea6bac-6867-4446-ba94-b3517f3cff20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "20ea6bac-6867-4446-ba94-b3517f3cff20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e307a7eb-e9c6-432b-8e5a-9360cb7126c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}