{
    "id": "2521ffd5-c034-4b1f-b0aa-dcf9e4a45596",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_command_panel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19a92a7e-26ed-488a-8f81-9ac17618568e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2521ffd5-c034-4b1f-b0aa-dcf9e4a45596",
            "compositeImage": {
                "id": "b484b13f-5310-4c07-9455-99a1d051f397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a92a7e-26ed-488a-8f81-9ac17618568e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aba9b08-0a67-4b5b-9c7e-39250d5099ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a92a7e-26ed-488a-8f81-9ac17618568e",
                    "LayerId": "0631ac02-b18a-4519-8bc1-29db684f22fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "0631ac02-b18a-4519-8bc1-29db684f22fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2521ffd5-c034-4b1f-b0aa-dcf9e4a45596",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}