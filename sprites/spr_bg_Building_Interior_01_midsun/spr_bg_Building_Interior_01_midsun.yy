{
    "id": "7b790877-6351-442d-ba59-cb50aeb61646",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Building_Interior_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14a94df5-09a3-4ea1-bb65-254d6c556cd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b790877-6351-442d-ba59-cb50aeb61646",
            "compositeImage": {
                "id": "cf901392-d457-495d-8e97-9e0e288f914d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14a94df5-09a3-4ea1-bb65-254d6c556cd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09a19e7-5766-4dac-ba66-63c407065390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14a94df5-09a3-4ea1-bb65-254d6c556cd9",
                    "LayerId": "10d88783-253d-4bab-b677-489b2b25685a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "10d88783-253d-4bab-b677-489b2b25685a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b790877-6351-442d-ba59-cb50aeb61646",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}