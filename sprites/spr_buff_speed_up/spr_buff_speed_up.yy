{
    "id": "a4722359-1fe9-4c05-abf0-aabbc1906c4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_speed_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 1,
    "bbox_right": 49,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "913e7e17-1234-4d4d-b328-3a2e2e846879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4722359-1fe9-4c05-abf0-aabbc1906c4c",
            "compositeImage": {
                "id": "855e6a8b-c4f8-4d9d-bf7b-e99c592fd1a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913e7e17-1234-4d4d-b328-3a2e2e846879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822774d4-43cc-4d68-b961-50282237d2ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913e7e17-1234-4d4d-b328-3a2e2e846879",
                    "LayerId": "55b40578-e070-4c2b-b36b-2199fd6bcb07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "55b40578-e070-4c2b-b36b-2199fd6bcb07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4722359-1fe9-4c05-abf0-aabbc1906c4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}