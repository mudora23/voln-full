{
    "id": "dea81e16-6df2-4e67-a11e-f52fbc13c7eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ku_n_cr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1721,
    "bbox_left": 128,
    "bbox_right": 991,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a027631a-8225-4890-8007-417a0f5a5ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea81e16-6df2-4e67-a11e-f52fbc13c7eb",
            "compositeImage": {
                "id": "82f0878f-0619-47a9-93c6-f1c3531774c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a027631a-8225-4890-8007-417a0f5a5ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14563fd7-89d5-465a-8a0a-e0277373d15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a027631a-8225-4890-8007-417a0f5a5ee3",
                    "LayerId": "819f9420-db5e-4dfa-95d6-55930cf81b08"
                }
            ]
        },
        {
            "id": "409c1db2-40e6-469d-aead-66dc1263e22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea81e16-6df2-4e67-a11e-f52fbc13c7eb",
            "compositeImage": {
                "id": "31c385df-8655-48a8-bcd5-0a9bc608696d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "409c1db2-40e6-469d-aead-66dc1263e22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb68d18-10f8-4084-bf73-caf7b750ea18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "409c1db2-40e6-469d-aead-66dc1263e22e",
                    "LayerId": "819f9420-db5e-4dfa-95d6-55930cf81b08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1779,
    "layers": [
        {
            "id": "819f9420-db5e-4dfa-95d6-55930cf81b08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dea81e16-6df2-4e67-a11e-f52fbc13c7eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1086,
    "xorig": 0,
    "yorig": 0
}