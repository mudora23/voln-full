{
    "id": "3dd01e17-1d36-479f-9765-d331c0f3d966",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_3b_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 297,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43a87cab-1d29-419c-b506-fae9fed88d2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dd01e17-1d36-479f-9765-d331c0f3d966",
            "compositeImage": {
                "id": "3b35c67a-3688-43fd-9af9-255931b1709f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a87cab-1d29-419c-b506-fae9fed88d2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074967ab-12ae-458c-86aa-48cd9277f83b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a87cab-1d29-419c-b506-fae9fed88d2d",
                    "LayerId": "95430ad0-77d9-4ed9-aa60-ccb2f9cad6b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "95430ad0-77d9-4ed9-aa60-ccb2f9cad6b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dd01e17-1d36-479f-9765-d331c0f3d966",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}