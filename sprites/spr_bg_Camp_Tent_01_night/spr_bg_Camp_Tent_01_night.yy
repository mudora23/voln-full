{
    "id": "f65d2c3a-a08c-4344-8c36-e544bdb61d7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Camp_Tent_01_night",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96c7c417-350e-47a3-8013-760b080a35a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f65d2c3a-a08c-4344-8c36-e544bdb61d7f",
            "compositeImage": {
                "id": "d9eb5960-5328-4973-b3ed-7b08d5e724c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c7c417-350e-47a3-8013-760b080a35a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a39b99db-e050-40fd-9cac-fe86718b9524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c7c417-350e-47a3-8013-760b080a35a2",
                    "LayerId": "85f58fff-2d10-4f57-a590-d497010902da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "85f58fff-2d10-4f57-a590-d497010902da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f65d2c3a-a08c-4344-8c36-e544bdb61d7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}