{
    "id": "47379d6e-7194-49d2-a1c8-5ac23f63f558",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_royalpurple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b247a07d-a54e-4d9e-b6fa-2ad823d9be45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47379d6e-7194-49d2-a1c8-5ac23f63f558",
            "compositeImage": {
                "id": "cb062d78-9b15-4fe7-9a9d-8e2bd84c4633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b247a07d-a54e-4d9e-b6fa-2ad823d9be45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccedf311-539b-441d-a5d1-1ca0e5f982c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b247a07d-a54e-4d9e-b6fa-2ad823d9be45",
                    "LayerId": "9286c4dd-ca89-4746-aa03-3c1b46bfed08"
                }
            ]
        },
        {
            "id": "a081cf10-9f2a-477b-aed5-f19448782c06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47379d6e-7194-49d2-a1c8-5ac23f63f558",
            "compositeImage": {
                "id": "528114bf-1a36-4cfa-a5e2-b8c37185b3b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a081cf10-9f2a-477b-aed5-f19448782c06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee72ee1-995b-4b4b-8899-b259ab89ba6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a081cf10-9f2a-477b-aed5-f19448782c06",
                    "LayerId": "9286c4dd-ca89-4746-aa03-3c1b46bfed08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "9286c4dd-ca89-4746-aa03-3c1b46bfed08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47379d6e-7194-49d2-a1c8-5ac23f63f558",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}