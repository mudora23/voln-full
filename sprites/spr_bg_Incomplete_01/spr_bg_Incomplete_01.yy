{
    "id": "57a5de94-670c-4300-b266-be8013e7457d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Incomplete_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d58b617a-9884-4bc2-a801-eb0bd7750d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57a5de94-670c-4300-b266-be8013e7457d",
            "compositeImage": {
                "id": "4e108893-30d5-476c-8acf-42240f7d0c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58b617a-9884-4bc2-a801-eb0bd7750d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e654818e-51dc-4bc2-9bd8-2739a415930d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58b617a-9884-4bc2-a801-eb0bd7750d65",
                    "LayerId": "8ad9a9fd-78f7-43db-9e70-a20eebbfdd87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "8ad9a9fd-78f7-43db-9e70-a20eebbfdd87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57a5de94-670c-4300-b266-be8013e7457d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}