{
    "id": "88d2d72b-1d1c-47c6-84a0-1f66065c846a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59ab3517-7306-44b0-b7aa-e3569e1ca306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88d2d72b-1d1c-47c6-84a0-1f66065c846a",
            "compositeImage": {
                "id": "87d70e7d-c8ff-426c-8499-8e7618336c71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59ab3517-7306-44b0-b7aa-e3569e1ca306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f671e96-df1d-4f20-8160-e29d0a7037b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59ab3517-7306-44b0-b7aa-e3569e1ca306",
                    "LayerId": "f29d1e9c-2d94-4f12-926d-88f84d5eb9c1"
                }
            ]
        },
        {
            "id": "eddb3fbb-6345-4b26-8070-d49a74026a67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88d2d72b-1d1c-47c6-84a0-1f66065c846a",
            "compositeImage": {
                "id": "ccc4bc6b-872b-4e96-94c8-94a1bfe57752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eddb3fbb-6345-4b26-8070-d49a74026a67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688a8c7d-3ff1-48f7-a40e-e6210ad3a782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddb3fbb-6345-4b26-8070-d49a74026a67",
                    "LayerId": "f29d1e9c-2d94-4f12-926d-88f84d5eb9c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "f29d1e9c-2d94-4f12-926d-88f84d5eb9c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88d2d72b-1d1c-47c6-84a0-1f66065c846a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}