{
    "id": "50275add-6a41-4049-8d19-14388cac5612",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 234,
    "bbox_left": 0,
    "bbox_right": 1595,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd395156-9ec4-4b6b-adf7-d71a4635054a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50275add-6a41-4049-8d19-14388cac5612",
            "compositeImage": {
                "id": "416f93cb-a0ef-4f70-aefc-d51f578d313d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd395156-9ec4-4b6b-adf7-d71a4635054a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b184bc-a4d0-48a4-b26d-ec034ae5b636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd395156-9ec4-4b6b-adf7-d71a4635054a",
                    "LayerId": "e9f92b2e-b206-4b24-b76d-db1a7b22c8fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 235,
    "layers": [
        {
            "id": "e9f92b2e-b206-4b24-b76d-db1a7b22c8fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50275add-6a41-4049-8d19-14388cac5612",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1596,
    "xorig": 0,
    "yorig": 0
}