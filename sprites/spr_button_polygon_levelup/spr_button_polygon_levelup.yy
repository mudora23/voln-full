{
    "id": "8fbd898b-5dc1-4945-a3b3-e92f7f390957",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_polygon_levelup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 1,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6c57527-334d-4baf-9e3c-fe14824e362d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fbd898b-5dc1-4945-a3b3-e92f7f390957",
            "compositeImage": {
                "id": "a0bb7d29-0f2b-4d68-9e81-257871018a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c57527-334d-4baf-9e3c-fe14824e362d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "265bc6c9-2425-4229-822f-3f240db14acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c57527-334d-4baf-9e3c-fe14824e362d",
                    "LayerId": "5bd6951e-16f8-4fad-bdab-e038a55292c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "5bd6951e-16f8-4fad-bdab-e038a55292c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fbd898b-5dc1-4945-a3b3-e92f7f390957",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 37
}