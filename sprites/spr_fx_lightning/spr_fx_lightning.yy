{
    "id": "b24a4da7-85c9-4a60-b43f-fb78ff455104",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_lightning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 900,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7eef5c25-19e3-450b-a746-066c3836454a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b24a4da7-85c9-4a60-b43f-fb78ff455104",
            "compositeImage": {
                "id": "7a4b667c-93c5-4632-8560-26e40de939c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eef5c25-19e3-450b-a746-066c3836454a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "194d8b3f-d34f-47b6-95c1-a14ec575d31b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eef5c25-19e3-450b-a746-066c3836454a",
                    "LayerId": "6fc53dae-cf56-404a-8bc5-9f691a69ccdc"
                }
            ]
        },
        {
            "id": "5867b013-c725-4188-bd89-157fd093be29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b24a4da7-85c9-4a60-b43f-fb78ff455104",
            "compositeImage": {
                "id": "233f6ddc-4ca0-4f6b-9db5-ba0a2dd37786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5867b013-c725-4188-bd89-157fd093be29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5baa72b7-a1cb-4adc-88fc-29b51bcd4e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5867b013-c725-4188-bd89-157fd093be29",
                    "LayerId": "6fc53dae-cf56-404a-8bc5-9f691a69ccdc"
                }
            ]
        },
        {
            "id": "055eb28b-3319-450d-ba23-ab5b873167e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b24a4da7-85c9-4a60-b43f-fb78ff455104",
            "compositeImage": {
                "id": "208f4549-f007-475b-bdc5-647f384ec5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "055eb28b-3319-450d-ba23-ab5b873167e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eb117b4-8860-4676-807b-ff10178d4cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "055eb28b-3319-450d-ba23-ab5b873167e0",
                    "LayerId": "6fc53dae-cf56-404a-8bc5-9f691a69ccdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "6fc53dae-cf56-404a-8bc5-9f691a69ccdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b24a4da7-85c9-4a60-b43f-fb78ff455104",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 901,
    "xorig": 450,
    "yorig": 1079
}