{
    "id": "735568b2-4a3b-4c33-b085-e4db229d3838",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_scale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "186225e7-caed-48f6-87f2-fc676b7ca553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "735568b2-4a3b-4c33-b085-e4db229d3838",
            "compositeImage": {
                "id": "671e86d9-f97a-4fb5-88c1-9d60fc1a3f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186225e7-caed-48f6-87f2-fc676b7ca553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9764fc6c-8686-4dc3-9ca2-87d4604c7850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186225e7-caed-48f6-87f2-fc676b7ca553",
                    "LayerId": "e09f2175-6c9c-49c4-b638-1cde6de2eb07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 113,
    "layers": [
        {
            "id": "e09f2175-6c9c-49c4-b638-1cde6de2eb07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "735568b2-4a3b-4c33-b085-e4db229d3838",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}