{
    "id": "8d15041e-f47a-4b63-ac28-9febd6ddff0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_dkdkwhite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35f22507-667d-492a-97df-5e49f792e45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d15041e-f47a-4b63-ac28-9febd6ddff0c",
            "compositeImage": {
                "id": "67a42459-0084-4739-880b-fb53a2a5e244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f22507-667d-492a-97df-5e49f792e45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1916c84b-0fc4-466e-89f7-01f42704e359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f22507-667d-492a-97df-5e49f792e45c",
                    "LayerId": "fb75563f-da9d-444a-8152-045386a11d63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "fb75563f-da9d-444a-8152-045386a11d63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d15041e-f47a-4b63-ac28-9febd6ddff0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}