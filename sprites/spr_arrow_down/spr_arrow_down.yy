{
    "id": "9be350f3-50e5-4e34-9887-e050172b0f6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3ac3c2a-0041-4f55-b42c-42e51b42313d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9be350f3-50e5-4e34-9887-e050172b0f6d",
            "compositeImage": {
                "id": "26282cca-3b9d-46e8-8567-41e937836889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ac3c2a-0041-4f55-b42c-42e51b42313d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f45413-7f38-4b45-9222-fd6fc8615862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ac3c2a-0041-4f55-b42c-42e51b42313d",
                    "LayerId": "76682675-5009-40aa-9d74-3702f5d6e70c"
                }
            ]
        },
        {
            "id": "03427821-55e1-4be6-9fe7-fe0865c4a928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9be350f3-50e5-4e34-9887-e050172b0f6d",
            "compositeImage": {
                "id": "e559b535-de45-4664-bbf8-38fcbd9d391b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03427821-55e1-4be6-9fe7-fe0865c4a928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79dba728-2c22-4d23-892e-f831cff38542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03427821-55e1-4be6-9fe7-fe0865c4a928",
                    "LayerId": "76682675-5009-40aa-9d74-3702f5d6e70c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "76682675-5009-40aa-9d74-3702f5d6e70c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9be350f3-50e5-4e34-9887-e050172b0f6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}