{
    "id": "f03cf4b7-d035-4506-9239-767bc2e77c42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_fireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b842d006-b0d1-4ae7-8a2a-ab6f46af7ece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03cf4b7-d035-4506-9239-767bc2e77c42",
            "compositeImage": {
                "id": "1463e6d9-6ca3-4dbc-b709-037c3a46dd65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b842d006-b0d1-4ae7-8a2a-ab6f46af7ece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33264f8-474b-4e18-b89a-e3463a6f8e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b842d006-b0d1-4ae7-8a2a-ab6f46af7ece",
                    "LayerId": "0b5bdacb-672c-46c9-9528-e25b800d51c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "0b5bdacb-672c-46c9-9528-e25b800d51c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f03cf4b7-d035-4506-9239-767bc2e77c42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}