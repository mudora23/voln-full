{
    "id": "ccc34ad7-d461-4e13-9920-3059409c3349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bar_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90fb6869-a939-40f7-ae5e-8b58846106ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccc34ad7-d461-4e13-9920-3059409c3349",
            "compositeImage": {
                "id": "ff73e03e-8eb0-4391-9dcc-3a7eb00809c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90fb6869-a939-40f7-ae5e-8b58846106ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f42b88d-0558-4385-beeb-a3689fd2d957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90fb6869-a939-40f7-ae5e-8b58846106ea",
                    "LayerId": "8ba07ac3-6041-4be5-bc45-ef97ff5e17c3"
                },
                {
                    "id": "d0c5839d-0dc8-413b-8ccd-75783a002540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90fb6869-a939-40f7-ae5e-8b58846106ea",
                    "LayerId": "4d58c974-69cb-4d60-a83c-0cf3498f952e"
                }
            ]
        },
        {
            "id": "c8173fbb-1843-4267-87a3-fbdea5b67e8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccc34ad7-d461-4e13-9920-3059409c3349",
            "compositeImage": {
                "id": "d48cdcb1-0590-4204-a5d6-918b2cf3b15e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8173fbb-1843-4267-87a3-fbdea5b67e8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a778fa5-e2be-490b-8bf2-74da7034b46a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8173fbb-1843-4267-87a3-fbdea5b67e8f",
                    "LayerId": "8ba07ac3-6041-4be5-bc45-ef97ff5e17c3"
                },
                {
                    "id": "de91b8fa-8570-4fd2-89df-c142d1aed2e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8173fbb-1843-4267-87a3-fbdea5b67e8f",
                    "LayerId": "4d58c974-69cb-4d60-a83c-0cf3498f952e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "4d58c974-69cb-4d60-a83c-0cf3498f952e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccc34ad7-d461-4e13-9920-3059409c3349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 70,
            "visible": true
        },
        {
            "id": "8ba07ac3-6041-4be5-bc45-ef97ff5e17c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccc34ad7-d461-4e13-9920-3059409c3349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}