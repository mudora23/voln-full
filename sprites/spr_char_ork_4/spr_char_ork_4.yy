{
    "id": "e9cbdad1-a38d-4df7-8cdb-22250e219e22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1997,
    "bbox_left": 4,
    "bbox_right": 1111,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8ab92cc-501f-4710-ba74-d5ac94e5faaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cbdad1-a38d-4df7-8cdb-22250e219e22",
            "compositeImage": {
                "id": "52a2f12e-c2bc-4f54-93b1-e9bcbd26596a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8ab92cc-501f-4710-ba74-d5ac94e5faaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31dbc124-6cbf-4866-8a31-29bd5d0b2b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8ab92cc-501f-4710-ba74-d5ac94e5faaa",
                    "LayerId": "b44eaffb-4524-431b-aef1-635ec354aa65"
                }
            ]
        },
        {
            "id": "a2fbb518-25df-4b20-af70-991c924e9f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cbdad1-a38d-4df7-8cdb-22250e219e22",
            "compositeImage": {
                "id": "92e5bf02-b97a-4455-827c-8628340be8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2fbb518-25df-4b20-af70-991c924e9f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13566b22-b3df-4607-820b-31a9d6a72e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2fbb518-25df-4b20-af70-991c924e9f95",
                    "LayerId": "b44eaffb-4524-431b-aef1-635ec354aa65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "b44eaffb-4524-431b-aef1-635ec354aa65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9cbdad1-a38d-4df7-8cdb-22250e219e22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}