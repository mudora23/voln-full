{
    "id": "6e1ef0be-a4de-48ae-8aaa-738267f530c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_left_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 1,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2ec957d-f518-4daf-abca-097b30b11585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e1ef0be-a4de-48ae-8aaa-738267f530c5",
            "compositeImage": {
                "id": "389416ad-84ba-4b5d-a263-cd1a043e0a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ec957d-f518-4daf-abca-097b30b11585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2394bdb-0023-48f8-8b87-1931037b0e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ec957d-f518-4daf-abca-097b30b11585",
                    "LayerId": "5dfe9c97-decb-41ac-9eef-df4f37d96f0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 116,
    "layers": [
        {
            "id": "5dfe9c97-decb-41ac-9eef-df4f37d96f0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e1ef0be-a4de-48ae-8aaa-738267f530c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 21,
    "yorig": 94
}