{
    "id": "1ea879b2-225f-4808-886e-6873dc1350ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_right_seemless_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12205ba3-6d6a-4e32-a9d9-49edabcfec64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ea879b2-225f-4808-886e-6873dc1350ff",
            "compositeImage": {
                "id": "7a25336e-1340-4c2c-b52c-23e9d75ba339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12205ba3-6d6a-4e32-a9d9-49edabcfec64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f62d984-0280-479a-a913-5a77016dd802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12205ba3-6d6a-4e32-a9d9-49edabcfec64",
                    "LayerId": "09692eb8-597e-4e48-a7e1-5fab9a688092"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "09692eb8-597e-4e48-a7e1-5fab9a688092",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ea879b2-225f-4808-886e-6873dc1350ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 12,
    "yorig": 14
}