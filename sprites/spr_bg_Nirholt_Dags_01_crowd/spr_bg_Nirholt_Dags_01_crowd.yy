{
    "id": "18dae48f-51a3-4e32-81e1-bbb472df8d5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_Dags_01_crowd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f0a676e-e277-4d2b-a7c6-fb7d34a85cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18dae48f-51a3-4e32-81e1-bbb472df8d5a",
            "compositeImage": {
                "id": "22d33c95-3455-4cac-9321-99a81756e64f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f0a676e-e277-4d2b-a7c6-fb7d34a85cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55aeb24c-30ac-413f-979c-0c05c08b3e53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f0a676e-e277-4d2b-a7c6-fb7d34a85cb9",
                    "LayerId": "5c492604-a2e1-406a-a279-13b1ced3d5cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "5c492604-a2e1-406a-a279-13b1ced3d5cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18dae48f-51a3-4e32-81e1-bbb472df8d5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}