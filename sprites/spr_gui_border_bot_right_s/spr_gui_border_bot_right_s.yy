{
    "id": "32e65a28-877f-4c93-8d45-d790d80f466c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_right_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 113,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60b4a71e-a948-44cf-96d3-174ee0236805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32e65a28-877f-4c93-8d45-d790d80f466c",
            "compositeImage": {
                "id": "b6c9f292-e6e6-44e1-a389-725f3e63e604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b4a71e-a948-44cf-96d3-174ee0236805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ec0f6b-f812-423e-bc02-eba3b7f9e441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b4a71e-a948-44cf-96d3-174ee0236805",
                    "LayerId": "7acae012-b6a9-46f4-a1a4-f58396d50a1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 116,
    "layers": [
        {
            "id": "7acae012-b6a9-46f4-a1a4-f58396d50a1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32e65a28-877f-4c93-8d45-d790d80f466c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 94,
    "yorig": 94
}