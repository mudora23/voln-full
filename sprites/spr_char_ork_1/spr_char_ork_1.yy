{
    "id": "53b95e0f-5dc2-4a63-a01e-2e4753672216",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2001,
    "bbox_left": 20,
    "bbox_right": 1229,
    "bbox_top": 130,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52fb4e88-1e2f-408a-a550-e64d96e9b2ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53b95e0f-5dc2-4a63-a01e-2e4753672216",
            "compositeImage": {
                "id": "fe3bb86c-c2de-4635-affa-5859c2e3f58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52fb4e88-1e2f-408a-a550-e64d96e9b2ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a50109b-f633-4e0f-a689-519dac5b6f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52fb4e88-1e2f-408a-a550-e64d96e9b2ce",
                    "LayerId": "801f4df5-26cb-4587-beb2-d8e2d62b02b9"
                }
            ]
        },
        {
            "id": "7388a976-73d3-400e-b584-89718c930617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53b95e0f-5dc2-4a63-a01e-2e4753672216",
            "compositeImage": {
                "id": "2f665c9c-5121-437f-a09f-a5eedf2fe282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7388a976-73d3-400e-b584-89718c930617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9574d4b-19a6-4b28-9b4f-66e5cd910615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7388a976-73d3-400e-b584-89718c930617",
                    "LayerId": "801f4df5-26cb-4587-beb2-d8e2d62b02b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2004,
    "layers": [
        {
            "id": "801f4df5-26cb-4587-beb2-d8e2d62b02b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53b95e0f-5dc2-4a63-a01e-2e4753672216",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1230,
    "xorig": 0,
    "yorig": 0
}