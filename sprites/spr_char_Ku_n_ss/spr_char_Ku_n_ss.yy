{
    "id": "b1f34bd5-8206-4289-9ccb-2f38735711bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ku_n_ss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1721,
    "bbox_left": 128,
    "bbox_right": 991,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07423f6b-19b9-4603-a907-27963b0feb23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f34bd5-8206-4289-9ccb-2f38735711bd",
            "compositeImage": {
                "id": "359234a4-10d9-4c52-b630-1409e76022f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07423f6b-19b9-4603-a907-27963b0feb23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ca1d89c-6110-481d-a3da-6333a0382877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07423f6b-19b9-4603-a907-27963b0feb23",
                    "LayerId": "8f958ac5-bb5a-4d5f-99bd-2c0cbdafee85"
                }
            ]
        },
        {
            "id": "0a4e3487-454c-4499-b9d5-579bb795b3f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f34bd5-8206-4289-9ccb-2f38735711bd",
            "compositeImage": {
                "id": "79ddf798-9c94-480f-b0ba-4b876468f4ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a4e3487-454c-4499-b9d5-579bb795b3f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "754cd33c-bba2-40d1-9c4e-0cced7f129f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a4e3487-454c-4499-b9d5-579bb795b3f0",
                    "LayerId": "8f958ac5-bb5a-4d5f-99bd-2c0cbdafee85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1779,
    "layers": [
        {
            "id": "8f958ac5-bb5a-4d5f-99bd-2c0cbdafee85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1f34bd5-8206-4289-9ccb-2f38735711bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1086,
    "xorig": 0,
    "yorig": 0
}