{
    "id": "762af222-3443-4abc-a994-206bb6f34c0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_orange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96fe4df1-5543-48c2-b2e3-4a5ea1dd3334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "762af222-3443-4abc-a994-206bb6f34c0c",
            "compositeImage": {
                "id": "149bec65-246e-41fe-9c06-dc81bfbce14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96fe4df1-5543-48c2-b2e3-4a5ea1dd3334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdb8702a-6a69-48e3-9f3f-29494c624b19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96fe4df1-5543-48c2-b2e3-4a5ea1dd3334",
                    "LayerId": "b2e8141b-f19c-4a35-83c6-69296d44ecc2"
                }
            ]
        },
        {
            "id": "9692fd0a-69fc-4b1d-8d6f-e05959fe6c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "762af222-3443-4abc-a994-206bb6f34c0c",
            "compositeImage": {
                "id": "47b828df-ef86-4193-a3a1-4ca5efaa30d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9692fd0a-69fc-4b1d-8d6f-e05959fe6c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882fc29f-666a-48a5-ad12-aea52f66986c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9692fd0a-69fc-4b1d-8d6f-e05959fe6c5c",
                    "LayerId": "b2e8141b-f19c-4a35-83c6-69296d44ecc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "b2e8141b-f19c-4a35-83c6-69296d44ecc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "762af222-3443-4abc-a994-206bb6f34c0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}