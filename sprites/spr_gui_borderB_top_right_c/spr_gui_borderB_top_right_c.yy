{
    "id": "91a19270-b69e-4bd4-9e1f-dd70c6d97bd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_top_right_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25a487e1-cb6a-4d1e-8fe0-6694d5562be0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a19270-b69e-4bd4-9e1f-dd70c6d97bd3",
            "compositeImage": {
                "id": "f36d749b-bee1-4f62-88e4-fddb4d394732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a487e1-cb6a-4d1e-8fe0-6694d5562be0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6434cd29-6025-40e5-8242-e04d2b1c602f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a487e1-cb6a-4d1e-8fe0-6694d5562be0",
                    "LayerId": "f1aaeb70-32ab-4006-a391-0507e71a9070"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "f1aaeb70-32ab-4006-a391-0507e71a9070",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91a19270-b69e-4bd4-9e1f-dd70c6d97bd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 119,
    "yorig": 15
}