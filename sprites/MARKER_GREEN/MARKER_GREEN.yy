{
    "id": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MARKER_GREEN",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56079ee6-0ad9-4e07-9281-563591fa7da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
            "compositeImage": {
                "id": "41ffa00b-b286-4726-8d60-802c9aea80a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56079ee6-0ad9-4e07-9281-563591fa7da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d934d1-d2b7-4e1a-825b-7bdcf7d60c89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56079ee6-0ad9-4e07-9281-563591fa7da8",
                    "LayerId": "3b2ef4c4-dd19-47c1-b8a6-d62ebe5b2fcd"
                }
            ]
        },
        {
            "id": "e75218d1-3b49-4b28-9454-29a95606a3ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
            "compositeImage": {
                "id": "8e89fba0-ef72-43a8-be3f-eb1230689a10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75218d1-3b49-4b28-9454-29a95606a3ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9f643a3-5c3b-4727-90a7-8f63b207a214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75218d1-3b49-4b28-9454-29a95606a3ab",
                    "LayerId": "3b2ef4c4-dd19-47c1-b8a6-d62ebe5b2fcd"
                }
            ]
        },
        {
            "id": "98b05eb5-5dae-4b70-86b6-24948b56f6de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
            "compositeImage": {
                "id": "65958baa-2c63-49f4-bc3d-a4687e15ae63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b05eb5-5dae-4b70-86b6-24948b56f6de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80185cf3-186a-4805-9134-bccf7fc840e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b05eb5-5dae-4b70-86b6-24948b56f6de",
                    "LayerId": "3b2ef4c4-dd19-47c1-b8a6-d62ebe5b2fcd"
                }
            ]
        },
        {
            "id": "063f1f28-aaef-43fb-9665-fe3695a596ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
            "compositeImage": {
                "id": "795fe017-725c-4d80-b9fb-2e8d1d7d9b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063f1f28-aaef-43fb-9665-fe3695a596ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a477e9-1b2a-4e9d-9ef3-dc4c7da2db7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063f1f28-aaef-43fb-9665-fe3695a596ea",
                    "LayerId": "3b2ef4c4-dd19-47c1-b8a6-d62ebe5b2fcd"
                }
            ]
        },
        {
            "id": "420ec268-5d54-4fe0-acc0-1892755b7401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
            "compositeImage": {
                "id": "619b4485-495c-4cc6-8c18-022eaa078092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "420ec268-5d54-4fe0-acc0-1892755b7401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc8c90c-bce9-4671-8614-ddde37f01fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "420ec268-5d54-4fe0-acc0-1892755b7401",
                    "LayerId": "3b2ef4c4-dd19-47c1-b8a6-d62ebe5b2fcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "3b2ef4c4-dd19-47c1-b8a6-d62ebe5b2fcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "338c8a27-cf83-4bb3-a378-6cc1e5d2c084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 0
}