{
    "id": "26c9232f-9803-41db-aafe-75a050a33a2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Ruins_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dee3fcf1-9409-47d8-b5e5-64e8e893b555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26c9232f-9803-41db-aafe-75a050a33a2f",
            "compositeImage": {
                "id": "08d0acd1-bd5a-4dc1-9ede-ff4054babf81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee3fcf1-9409-47d8-b5e5-64e8e893b555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbdbea8-bc84-4b53-91f4-0ebc7c2a475d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee3fcf1-9409-47d8-b5e5-64e8e893b555",
                    "LayerId": "96759adb-ace5-4c2e-9512-ea36caeb5cda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "96759adb-ace5-4c2e-9512-ea36caeb5cda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26c9232f-9803-41db-aafe-75a050a33a2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}