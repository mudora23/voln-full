{
    "id": "e47196a9-2fd0-43f6-ba49-8c0d91273926",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_imp_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1519,
    "bbox_left": 0,
    "bbox_right": 1184,
    "bbox_top": 95,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6ae34de-a3d9-4885-9f77-3e682cd12791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47196a9-2fd0-43f6-ba49-8c0d91273926",
            "compositeImage": {
                "id": "c9948562-3ee8-4d28-a9bc-f08aae07afd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ae34de-a3d9-4885-9f77-3e682cd12791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a4ac79-7da3-435c-adb8-c1aa99a1e143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ae34de-a3d9-4885-9f77-3e682cd12791",
                    "LayerId": "acce1ef5-10d5-4abc-884b-13df55b70cc7"
                }
            ]
        },
        {
            "id": "2dfeeb5a-7db0-42a6-9ab6-62a512f2d39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47196a9-2fd0-43f6-ba49-8c0d91273926",
            "compositeImage": {
                "id": "7b99dec4-2904-4a77-b49b-a5f870cb22bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dfeeb5a-7db0-42a6-9ab6-62a512f2d39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9a61b1-2dc6-4764-868e-d78ffe70d0ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dfeeb5a-7db0-42a6-9ab6-62a512f2d39a",
                    "LayerId": "acce1ef5-10d5-4abc-884b-13df55b70cc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1520,
    "layers": [
        {
            "id": "acce1ef5-10d5-4abc-884b-13df55b70cc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e47196a9-2fd0-43f6-ba49-8c0d91273926",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1276,
    "xorig": 0,
    "yorig": 0
}