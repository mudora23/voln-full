{
    "id": "7eadc22c-7fcc-4ae2-91df-8b88bfa660d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_polygon_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c7a03e5-353f-44a9-80ad-b49057a5deea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eadc22c-7fcc-4ae2-91df-8b88bfa660d9",
            "compositeImage": {
                "id": "5ba520c2-4939-41c1-bc3d-0ae407c4d439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7a03e5-353f-44a9-80ad-b49057a5deea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a67e703a-d028-4119-a6c3-d39671b5e276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7a03e5-353f-44a9-80ad-b49057a5deea",
                    "LayerId": "beabadce-0250-43f0-a7c5-f27ef6cd99ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "beabadce-0250-43f0-a7c5-f27ef6cd99ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eadc22c-7fcc-4ae2-91df-8b88bfa660d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 35
}