{
    "id": "7c6d3d26-c578-4e1e-b854-136cf322eb0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_wound",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 3,
    "bbox_right": 45,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f39eb44c-bbe1-4fd8-a5df-9cc35ac37e01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6d3d26-c578-4e1e-b854-136cf322eb0f",
            "compositeImage": {
                "id": "40d87e65-f08c-4f3e-89c9-3729d89b6c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39eb44c-bbe1-4fd8-a5df-9cc35ac37e01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c626eba-5a2c-4759-998c-9fedd4ced71c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39eb44c-bbe1-4fd8-a5df-9cc35ac37e01",
                    "LayerId": "39a35626-ffd8-4e0c-9282-5e50721383b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "39a35626-ffd8-4e0c-9282-5e50721383b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c6d3d26-c578-4e1e-b854-136cf322eb0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}