{
    "id": "123c3470-379e-4e0e-9c53-05b35aa8d1ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_bodyguard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 3,
    "bbox_right": 46,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd1a1ab6-453b-4cb1-a7cc-b71a3719be4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123c3470-379e-4e0e-9c53-05b35aa8d1ff",
            "compositeImage": {
                "id": "1b6a42cc-bfa6-4332-b0d8-2bbedbe6c00f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1a1ab6-453b-4cb1-a7cc-b71a3719be4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab0181ae-861a-4bdc-98ff-1583959daf76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1a1ab6-453b-4cb1-a7cc-b71a3719be4d",
                    "LayerId": "c6893a64-7ea5-4738-9245-97eb336bd061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "c6893a64-7ea5-4738-9245-97eb336bd061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "123c3470-379e-4e0e-9c53-05b35aa8d1ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}