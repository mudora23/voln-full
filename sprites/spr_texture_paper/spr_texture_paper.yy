{
    "id": "9650f4eb-b412-4169-9c36-895e93436af1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_texture_paper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b5e814a-d24e-4ce4-945c-2e8c73cae2e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "268a4d6d-3025-4861-9524-65365d6412d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5e814a-d24e-4ce4-945c-2e8c73cae2e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fd1aeb-3fb0-4800-a535-0b7fd85b8be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5e814a-d24e-4ce4-945c-2e8c73cae2e1",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "ee88cab5-ea6e-4c03-97a0-e41074ab3a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "d7aa8d8c-e4a5-449c-97e2-40441a23c0a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee88cab5-ea6e-4c03-97a0-e41074ab3a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e531027d-797d-4e6e-8c53-07d381810330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee88cab5-ea6e-4c03-97a0-e41074ab3a74",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "573de50b-96e3-40e0-8e79-1ed4516e1a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "58c0336c-da2d-4d93-9ac1-a1d8e8d0223d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573de50b-96e3-40e0-8e79-1ed4516e1a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21cf0e0b-3647-4cb0-b44f-80836db14f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573de50b-96e3-40e0-8e79-1ed4516e1a5d",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "875767f3-ab16-4635-a81b-02cb467ef7bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "4889abd9-4426-4ce1-b6b9-cfda51700a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875767f3-ab16-4635-a81b-02cb467ef7bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdd5bd3b-db96-48aa-99ad-e163e1129a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875767f3-ab16-4635-a81b-02cb467ef7bb",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "9c5a14bc-d627-4be1-95d7-557ff28fb171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "54d48fbf-dd1a-407e-8470-88d51fa0a78b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c5a14bc-d627-4be1-95d7-557ff28fb171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa4325f-3ad2-4bea-872e-8ca02e09abfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c5a14bc-d627-4be1-95d7-557ff28fb171",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "601c129f-a25d-4184-9bce-614227a7db07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "77e3a58b-6994-40cf-bbf8-d448584a17a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "601c129f-a25d-4184-9bce-614227a7db07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c321a11d-f918-4390-b885-e0ac7e774c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "601c129f-a25d-4184-9bce-614227a7db07",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "a5ece387-bdbc-46ce-8ffe-03e9e1cb9faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "1c143766-abc8-4d2b-a8eb-7f58153ba613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ece387-bdbc-46ce-8ffe-03e9e1cb9faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb057c4-3de8-413f-91be-0c36f10013e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ece387-bdbc-46ce-8ffe-03e9e1cb9faf",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "1e69a0e4-14cf-4be1-b22d-7628d8292547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "180b96b2-932f-4d6c-ae9b-16168451360b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e69a0e4-14cf-4be1-b22d-7628d8292547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e2af3b6-d540-4012-955f-8cef70fee95f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e69a0e4-14cf-4be1-b22d-7628d8292547",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "f3db8687-c97c-4331-b8c9-27a01f1e2238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "abf2c302-68e1-47bc-a2bf-660335a31efa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3db8687-c97c-4331-b8c9-27a01f1e2238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c466d6b-efa7-4caf-98e4-5051f1bf2fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3db8687-c97c-4331-b8c9-27a01f1e2238",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "bb2e525a-5a6c-4add-a247-c4a17838b5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "3c9cec35-604c-4cab-b0b6-11054fac0df3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2e525a-5a6c-4add-a247-c4a17838b5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde78edc-a54d-4eff-8a9f-0626c1394cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2e525a-5a6c-4add-a247-c4a17838b5ce",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "bb98bd69-4b93-4706-a05c-d11c6164c725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "2b3cb711-47e3-4bfa-9f40-7cfea49df81d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb98bd69-4b93-4706-a05c-d11c6164c725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac04d5fa-f901-4ee9-b43e-56c45f81e3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb98bd69-4b93-4706-a05c-d11c6164c725",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "ef3807d4-8f8b-4ddb-89b7-240f8ae7dc2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "565c38cb-9c46-47ef-83ec-9313df96c362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3807d4-8f8b-4ddb-89b7-240f8ae7dc2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831df334-a077-44d9-adf5-4957d3dd6821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3807d4-8f8b-4ddb-89b7-240f8ae7dc2a",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        },
        {
            "id": "18f8f3f0-5868-4503-ba66-e95a338b7ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "compositeImage": {
                "id": "ec36261a-817d-4c9d-962d-761762dc2b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f8f3f0-5868-4503-ba66-e95a338b7ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fdbf83-5045-459a-8fa2-2906d5d8cf48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f8f3f0-5868-4503-ba66-e95a338b7ed8",
                    "LayerId": "455bb663-0540-400e-ab33-b567ca808983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "455bb663-0540-400e-ab33-b567ca808983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9650f4eb-b412-4169-9c36-895e93436af1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}