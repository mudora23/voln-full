{
    "id": "c6c36780-f5f3-4211-b203-b4eeb802efae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_hide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 5,
    "bbox_right": 41,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "927ab98c-05f2-4f5a-87ec-aea50898cee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6c36780-f5f3-4211-b203-b4eeb802efae",
            "compositeImage": {
                "id": "b1b61bae-986f-494e-a781-c37520f44848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927ab98c-05f2-4f5a-87ec-aea50898cee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a603e520-7f2e-4347-8575-7ea1f236218c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927ab98c-05f2-4f5a-87ec-aea50898cee6",
                    "LayerId": "bcc7c248-70b9-4f97-b679-04baa9b2b01c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "bcc7c248-70b9-4f97-b679-04baa9b2b01c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6c36780-f5f3-4211-b203-b4eeb802efae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}