{
    "id": "fa277e3d-0f52-4d13-a891-f1cc159e65e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MARKER_NOT_VISITED",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73e9bbd9-201f-46ae-bf54-a9db5c0f884e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa277e3d-0f52-4d13-a891-f1cc159e65e2",
            "compositeImage": {
                "id": "03fb0bb4-a146-4f3f-a796-c79477d828fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e9bbd9-201f-46ae-bf54-a9db5c0f884e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fbe766f-b576-4102-850c-0c2a1b2e96f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e9bbd9-201f-46ae-bf54-a9db5c0f884e",
                    "LayerId": "cddbc639-af64-4f0b-81a6-1b119e9b849d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cddbc639-af64-4f0b-81a6-1b119e9b849d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa277e3d-0f52-4d13-a891-f1cc159e65e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}