{
    "id": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_choice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 313,
    "bbox_left": 0,
    "bbox_right": 1294,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b90ab8b-5c03-49b3-9dc4-08fd49f7657f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "01552db6-ad8a-460c-ba7a-860d7a778ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b90ab8b-5c03-49b3-9dc4-08fd49f7657f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e883e71b-84c4-491d-a6cd-8868913acbac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b90ab8b-5c03-49b3-9dc4-08fd49f7657f",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "17244c71-4c90-48af-9761-db5d1b32b9f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "72025548-16e1-44d8-8523-fdbeef7df33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17244c71-4c90-48af-9761-db5d1b32b9f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92500a06-16e2-46fb-aab5-366a6e027150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17244c71-4c90-48af-9761-db5d1b32b9f8",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "169655a7-75d3-4eda-a94e-d23d4b749c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "c44c7fcd-49d0-463a-a270-69f0f60a9302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "169655a7-75d3-4eda-a94e-d23d4b749c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5bcaf60-a8a1-4fe0-9e86-bd1082ddd975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "169655a7-75d3-4eda-a94e-d23d4b749c5f",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "84157a6d-43c9-44ea-855b-19044312c2a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "ab513444-1bfc-4f63-a345-3e831458d5ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84157a6d-43c9-44ea-855b-19044312c2a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9960a56f-ba70-4565-9b5b-d9413099b82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84157a6d-43c9-44ea-855b-19044312c2a8",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "f02f325b-a02a-4a7c-bfc7-0bff909d5423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "727c4e0e-4bc8-4aec-b3ea-714c3f5712dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02f325b-a02a-4a7c-bfc7-0bff909d5423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03d92e4e-94d8-46ed-b5d2-1c21521de847",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02f325b-a02a-4a7c-bfc7-0bff909d5423",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "63a7387e-909f-47a8-b844-718cf2d1bc33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "e26f8b09-894f-4846-8120-68c473561765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63a7387e-909f-47a8-b844-718cf2d1bc33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04305ce-d9ba-4f56-b024-2dcd23e96ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63a7387e-909f-47a8-b844-718cf2d1bc33",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "10308f4f-0d92-4d80-aaf4-f42b57166e13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "71d90cfa-d279-41f3-a5c1-4c1f651104d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10308f4f-0d92-4d80-aaf4-f42b57166e13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "479be42d-8ee2-43f4-8e30-b4ba6dd29ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10308f4f-0d92-4d80-aaf4-f42b57166e13",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "bf944f4b-6472-43ca-8a96-1cdd49c4713f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "0b700126-e490-4e5f-9b59-1792e638578e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf944f4b-6472-43ca-8a96-1cdd49c4713f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57cac82-56f7-4ba5-92b4-9c3d770a9411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf944f4b-6472-43ca-8a96-1cdd49c4713f",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "bcab6d1c-b24d-435e-a0eb-b4de60adac21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "d76d18a4-3514-426e-980a-12f98be765bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcab6d1c-b24d-435e-a0eb-b4de60adac21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e8cdc27-efb9-4743-ad7a-512224617480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcab6d1c-b24d-435e-a0eb-b4de60adac21",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        },
        {
            "id": "e69a3bcf-9683-400d-8a9b-e477d24fcf37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "compositeImage": {
                "id": "3c355fb1-ccf5-4a5e-9fb0-946023c79122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69a3bcf-9683-400d-8a9b-e477d24fcf37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c6e2be-53bc-49e8-936b-572f94c53bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69a3bcf-9683-400d-8a9b-e477d24fcf37",
                    "LayerId": "9c17376f-1292-487c-b71b-588f7652a670"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 314,
    "layers": [
        {
            "id": "9c17376f-1292-487c-b71b-588f7652a670",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3b7c060-d9a4-4cb5-a1b8-ee9b284b599f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1295,
    "xorig": 0,
    "yorig": 0
}