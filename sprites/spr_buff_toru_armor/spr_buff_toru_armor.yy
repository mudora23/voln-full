{
    "id": "ef8c2b29-b870-4a22-bbd3-43f751b7f9ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_toru_armor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "466b3521-6f74-408c-bd3c-5c44a96d2019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef8c2b29-b870-4a22-bbd3-43f751b7f9ad",
            "compositeImage": {
                "id": "83b76727-aedd-450b-9771-f63f9692dbca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "466b3521-6f74-408c-bd3c-5c44a96d2019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88533272-a601-47b5-ae5a-c7178c099817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "466b3521-6f74-408c-bd3c-5c44a96d2019",
                    "LayerId": "52b719a7-2591-4974-a1f3-9be1bcc2a838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "52b719a7-2591-4974-a1f3-9be1bcc2a838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef8c2b29-b870-4a22-bbd3-43f751b7f9ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}