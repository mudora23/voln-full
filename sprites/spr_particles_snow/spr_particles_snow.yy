{
    "id": "def5a970-b7ec-4eef-98c4-608516049727",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particles_snow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 751,
    "bbox_left": 89,
    "bbox_right": 710,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b354a09-814b-4ec6-80b8-6c803ddfa16b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "000c52a4-4397-47ab-adc6-5cdd6bb68cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b354a09-814b-4ec6-80b8-6c803ddfa16b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6162576-22ac-4271-b6ab-a4bcefc1c107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b354a09-814b-4ec6-80b8-6c803ddfa16b",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "798976dc-3b27-4f71-aaf2-36a20b507c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "4e2d746c-60be-42e5-9f03-72640b8f662d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798976dc-3b27-4f71-aaf2-36a20b507c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071d7d8a-bac9-4ad5-b77a-def368248dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798976dc-3b27-4f71-aaf2-36a20b507c7f",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "25243462-5ed7-4aaf-a4f9-cc2d2e8b2b35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "535041db-27b9-4302-a237-1477e251eb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25243462-5ed7-4aaf-a4f9-cc2d2e8b2b35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844d84fa-3b92-4d62-9543-50027efdbb70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25243462-5ed7-4aaf-a4f9-cc2d2e8b2b35",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "767e84f9-2ed1-4f14-8dc6-2b94fd955014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "2c89c3b2-afdf-4ef7-9d6c-fc994f6b7375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "767e84f9-2ed1-4f14-8dc6-2b94fd955014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94df1807-b646-4501-be87-6b4dd990d8aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "767e84f9-2ed1-4f14-8dc6-2b94fd955014",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "77c67d8f-da99-4ec7-84e9-b349dcd69ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "9e8e2b7e-cfa0-437a-a55b-42456b61f9a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77c67d8f-da99-4ec7-84e9-b349dcd69ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c455fb71-8888-4dea-af2f-a17dd8f99b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77c67d8f-da99-4ec7-84e9-b349dcd69ade",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "f0359541-9f89-4f76-bc87-1e0929e1f043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "6f2ec941-15b9-4d9b-a459-e56316306868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0359541-9f89-4f76-bc87-1e0929e1f043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa1d825-a61a-4ecf-96ac-225f4958873f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0359541-9f89-4f76-bc87-1e0929e1f043",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "86a82efb-487f-49f6-abf1-13054aedbcfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "87cc4249-9d51-4121-bea4-1c8c1e9ba741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86a82efb-487f-49f6-abf1-13054aedbcfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e705d8f-2328-49ea-b2e5-b7d6aac9eb03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86a82efb-487f-49f6-abf1-13054aedbcfa",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "67e73094-8475-428f-8099-6e53b4e13d40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "2f1adcd8-5d3b-4ce3-a78f-baacf04735e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e73094-8475-428f-8099-6e53b4e13d40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d3fa9e-86df-4375-a826-797cf4ad011b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e73094-8475-428f-8099-6e53b4e13d40",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "6190291d-5669-4bd0-848f-2c8cc117e8c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "9b4dd2c3-5114-4221-a57b-b62850842c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6190291d-5669-4bd0-848f-2c8cc117e8c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d4838c-954e-42e8-acd0-7877d6f9cef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6190291d-5669-4bd0-848f-2c8cc117e8c9",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "2a7c5303-5c23-44c7-a384-786bf6b9a15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "45f6c7e7-57ba-42a4-baa6-c0ade8d6b606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a7c5303-5c23-44c7-a384-786bf6b9a15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b320bbbe-eeb5-4063-97ba-b72a44610778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a7c5303-5c23-44c7-a384-786bf6b9a15f",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "68eb757c-d250-4678-87e2-9fe3376e3b41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "24cdc57f-3199-4692-b146-3042a025d105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68eb757c-d250-4678-87e2-9fe3376e3b41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33c3d4e0-2341-4c6c-8335-fd7e6c4a8662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68eb757c-d250-4678-87e2-9fe3376e3b41",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        },
        {
            "id": "9de62786-8dbd-4000-acda-9501ec42c110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "compositeImage": {
                "id": "56842e2e-db3c-4545-98e2-d5969df81373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de62786-8dbd-4000-acda-9501ec42c110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bd6c0c-c042-4d0f-9333-c20e4a01d2f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de62786-8dbd-4000-acda-9501ec42c110",
                    "LayerId": "f88e2698-6d01-422e-a58f-fdb503d826fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "f88e2698-6d01-422e-a58f-fdb503d826fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "def5a970-b7ec-4eef-98c4-608516049727",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}