{
    "id": "47225f11-7772-4a23-8cc1-2e90cfd9ace0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_spi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 4,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4305cc89-656c-4a95-a10d-a88ab6766977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47225f11-7772-4a23-8cc1-2e90cfd9ace0",
            "compositeImage": {
                "id": "4b428033-86f5-4e5e-8741-53ef316b1201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4305cc89-656c-4a95-a10d-a88ab6766977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452ae1ed-e691-4efa-a3dc-4141a4522b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4305cc89-656c-4a95-a10d-a88ab6766977",
                    "LayerId": "9124414b-2722-46a3-bfa1-6242151aa930"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9124414b-2722-46a3-bfa1-6242151aa930",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47225f11-7772-4a23-8cc1-2e90cfd9ace0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}