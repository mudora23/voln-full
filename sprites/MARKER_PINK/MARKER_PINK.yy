{
    "id": "8020f458-f113-421b-9618-86f3ffcf82c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MARKER_PINK",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd6143d9-52e8-4d81-a827-5e0db07ed236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8020f458-f113-421b-9618-86f3ffcf82c7",
            "compositeImage": {
                "id": "daa7c202-2d71-4101-95d3-975c2f0bfb34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd6143d9-52e8-4d81-a827-5e0db07ed236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136327d9-69d0-46f3-af41-eb8696cbf37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd6143d9-52e8-4d81-a827-5e0db07ed236",
                    "LayerId": "5d5e80ec-078a-4ad1-8580-445c4e693519"
                }
            ]
        },
        {
            "id": "15f5bb2f-a62b-4343-b998-f7eb16809145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8020f458-f113-421b-9618-86f3ffcf82c7",
            "compositeImage": {
                "id": "d684c785-e6fc-40cc-b70e-79f91ba25b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f5bb2f-a62b-4343-b998-f7eb16809145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad150623-5356-4701-954b-8f23ec83b65b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f5bb2f-a62b-4343-b998-f7eb16809145",
                    "LayerId": "5d5e80ec-078a-4ad1-8580-445c4e693519"
                }
            ]
        },
        {
            "id": "fb8603b8-4ee6-47c3-8333-650989c81501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8020f458-f113-421b-9618-86f3ffcf82c7",
            "compositeImage": {
                "id": "a1fcbd43-99f7-4996-af7f-71aa3c1cc237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb8603b8-4ee6-47c3-8333-650989c81501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d03b3b9-a129-4210-a54f-7a3f223d8d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb8603b8-4ee6-47c3-8333-650989c81501",
                    "LayerId": "5d5e80ec-078a-4ad1-8580-445c4e693519"
                }
            ]
        },
        {
            "id": "47bab41b-7c0b-4925-a654-1c56a535f77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8020f458-f113-421b-9618-86f3ffcf82c7",
            "compositeImage": {
                "id": "491a2673-3a5c-4768-993c-ac4d79ee4358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47bab41b-7c0b-4925-a654-1c56a535f77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d711fb7c-065b-4395-8189-6e025ec36718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47bab41b-7c0b-4925-a654-1c56a535f77d",
                    "LayerId": "5d5e80ec-078a-4ad1-8580-445c4e693519"
                }
            ]
        },
        {
            "id": "935c0d70-f666-435c-aea7-c0a8561ac1e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8020f458-f113-421b-9618-86f3ffcf82c7",
            "compositeImage": {
                "id": "7050cf0d-40b0-4ae3-a412-525f46891727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935c0d70-f666-435c-aea7-c0a8561ac1e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab893cd-7393-4fd9-b1f1-f3fa79becf5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935c0d70-f666-435c-aea7-c0a8561ac1e5",
                    "LayerId": "5d5e80ec-078a-4ad1-8580-445c4e693519"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "5d5e80ec-078a-4ad1-8580-445c4e693519",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8020f458-f113-421b-9618-86f3ffcf82c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 0
}