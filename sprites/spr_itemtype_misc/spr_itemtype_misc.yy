{
    "id": "d98fe904-b103-453f-8c03-f2a2c0215987",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_itemtype_misc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 8,
    "bbox_right": 56,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cc594d2-a14f-4a92-bf97-03555a7832c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d98fe904-b103-453f-8c03-f2a2c0215987",
            "compositeImage": {
                "id": "51fc736b-985e-4f77-a4a3-a504656be372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc594d2-a14f-4a92-bf97-03555a7832c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9f8bf1-2d54-40f8-b1d1-113a713071fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc594d2-a14f-4a92-bf97-03555a7832c2",
                    "LayerId": "15dff756-dc28-444b-9fde-3e3295576187"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "15dff756-dc28-444b-9fde-3e3295576187",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d98fe904-b103-453f-8c03-f2a2c0215987",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}