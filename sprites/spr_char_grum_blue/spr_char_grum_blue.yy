{
    "id": "ff9696d7-2273-493b-966a-bc431834f8b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc71c432-10a9-4250-b73c-2cfbb71001db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff9696d7-2273-493b-966a-bc431834f8b3",
            "compositeImage": {
                "id": "7985517f-023b-42de-a363-66856bc1a3f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc71c432-10a9-4250-b73c-2cfbb71001db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e05d2aa-e8b7-4d87-897b-6df3a15533a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc71c432-10a9-4250-b73c-2cfbb71001db",
                    "LayerId": "dc1d6fd2-b595-4e58-a9d8-c8ab6d942cc3"
                }
            ]
        },
        {
            "id": "79c82709-e7b7-4d47-8ebe-f43ac4c0c8fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff9696d7-2273-493b-966a-bc431834f8b3",
            "compositeImage": {
                "id": "3df5cde6-c2f1-4f05-a79c-c50afcad2d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c82709-e7b7-4d47-8ebe-f43ac4c0c8fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a864605c-d30c-4324-a9f8-dfb0259d7ce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c82709-e7b7-4d47-8ebe-f43ac4c0c8fa",
                    "LayerId": "dc1d6fd2-b595-4e58-a9d8-c8ab6d942cc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "dc1d6fd2-b595-4e58-a9d8-c8ab6d942cc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff9696d7-2273-493b-966a-bc431834f8b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}