{
    "id": "395b0ff5-813c-40c1-990a-bebdda66fd8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_stun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 2,
    "bbox_right": 46,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45b0d7dd-b9d8-4610-8cad-853e7e57ac19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "395b0ff5-813c-40c1-990a-bebdda66fd8d",
            "compositeImage": {
                "id": "b7e7c929-5a7c-48f6-9146-16ca5a59b2ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b0d7dd-b9d8-4610-8cad-853e7e57ac19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aec984a-62d6-41a4-ac1d-7d346c4f9321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b0d7dd-b9d8-4610-8cad-853e7e57ac19",
                    "LayerId": "57721023-08c8-41fb-9265-daef77141040"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "57721023-08c8-41fb-9265-daef77141040",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "395b0ff5-813c-40c1-990a-bebdda66fd8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}