{
    "id": "cb91fa93-45be-4314-8336-8a2e96dff855",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_violet_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ae3cea7-09ce-4bd3-858d-e0fb3eed0e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb91fa93-45be-4314-8336-8a2e96dff855",
            "compositeImage": {
                "id": "fedc1334-53d7-411f-9e18-d05ab088f164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae3cea7-09ce-4bd3-858d-e0fb3eed0e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ce1e89-7acd-47ae-b334-89c510e7e7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae3cea7-09ce-4bd3-858d-e0fb3eed0e81",
                    "LayerId": "46e1a683-f8f4-4a36-a2cc-8a86b8b1fc3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "46e1a683-f8f4-4a36-a2cc-8a86b8b1fc3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb91fa93-45be-4314-8336-8a2e96dff855",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}