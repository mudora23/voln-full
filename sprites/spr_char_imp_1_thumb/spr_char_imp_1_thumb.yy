{
    "id": "b5801bc7-aa92-45b9-b604-cdb5b4b1d68b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_imp_1_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65a1a9ce-9e74-4b8f-8e72-12458529b106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5801bc7-aa92-45b9-b604-cdb5b4b1d68b",
            "compositeImage": {
                "id": "55d381f7-7ac9-4a59-87f6-e4f8a1769b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a1a9ce-9e74-4b8f-8e72-12458529b106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515b5571-bad7-47b9-9cf6-407cba9b0f67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a1a9ce-9e74-4b8f-8e72-12458529b106",
                    "LayerId": "5d0260f0-e4a9-47ed-a79a-c78614efb093"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "5d0260f0-e4a9-47ed-a79a-c78614efb093",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5801bc7-aa92-45b9-b604-cdb5b4b1d68b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}