{
    "id": "e4ac833f-228e-4495-bfc7-6186882f0ffb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_ally",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5dcd389-372b-42f9-a595-8832aab51eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4ac833f-228e-4495-bfc7-6186882f0ffb",
            "compositeImage": {
                "id": "fa1a2717-8651-4c7f-ab24-da9564cdffb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5dcd389-372b-42f9-a595-8832aab51eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de33887a-eca4-48e6-b548-dd2b0a80ae76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5dcd389-372b-42f9-a595-8832aab51eb6",
                    "LayerId": "4929a563-39de-4b15-b39a-3f2655f2748f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "4929a563-39de-4b15-b39a-3f2655f2748f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4ac833f-228e-4495-bfc7-6186882f0ffb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}