{
    "id": "6b83339a-8397-4118-9bb3-e8ec6d647793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_top_right_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a96753d6-d30c-4718-8739-3fb1553a5bf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b83339a-8397-4118-9bb3-e8ec6d647793",
            "compositeImage": {
                "id": "e1d22341-2be9-4cf9-b8d1-bb6b9f8012a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a96753d6-d30c-4718-8739-3fb1553a5bf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4ea1fd-5f39-43c0-85e0-ef25ba4091c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a96753d6-d30c-4718-8739-3fb1553a5bf9",
                    "LayerId": "59dce66d-2d22-4a75-9a9e-2c659fd36c78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "59dce66d-2d22-4a75-9a9e-2c659fd36c78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b83339a-8397-4118-9bb3-e8ec6d647793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 121,
    "yorig": 13
}