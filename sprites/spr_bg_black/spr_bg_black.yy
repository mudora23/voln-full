{
    "id": "70eabf68-d713-443a-b5e2-12c11f8a8b20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f191b1d4-ae5c-479a-b42f-9f51922fe728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70eabf68-d713-443a-b5e2-12c11f8a8b20",
            "compositeImage": {
                "id": "24dc8ab2-204c-48a6-84a5-600e6127238c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f191b1d4-ae5c-479a-b42f-9f51922fe728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7476bc6-0f5f-43cd-bedb-18246ca10f0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f191b1d4-ae5c-479a-b42f-9f51922fe728",
                    "LayerId": "7d13c41c-24e4-4123-a172-cce69ce89acd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "7d13c41c-24e4-4123-a172-cce69ce89acd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70eabf68-d713-443a-b5e2-12c11f8a8b20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}