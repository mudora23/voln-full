{
    "id": "b5a7248b-9d0e-4462-af89-436510c98090",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_redpink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63af8378-aca8-4edb-9f78-9d2c78a004c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5a7248b-9d0e-4462-af89-436510c98090",
            "compositeImage": {
                "id": "e8e179dd-8078-4729-b62c-efa13d8d3a0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63af8378-aca8-4edb-9f78-9d2c78a004c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a20075-f19d-4d14-8753-5730b21a8cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63af8378-aca8-4edb-9f78-9d2c78a004c6",
                    "LayerId": "224165ee-ce60-4c21-9ee7-029d82ba1c42"
                }
            ]
        },
        {
            "id": "9d1840c5-a158-4db8-813d-e456126c1c1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5a7248b-9d0e-4462-af89-436510c98090",
            "compositeImage": {
                "id": "9d462ac3-f961-4066-9bc3-dcbd69d7e03f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d1840c5-a158-4db8-813d-e456126c1c1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef648350-b972-44b8-9ee4-2d284f8a6456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d1840c5-a158-4db8-813d-e456126c1c1d",
                    "LayerId": "224165ee-ce60-4c21-9ee7-029d82ba1c42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "224165ee-ce60-4c21-9ee7-029d82ba1c42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5a7248b-9d0e-4462-af89-436510c98090",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}