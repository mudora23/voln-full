{
    "id": "f9f6d8bf-63cc-453e-a315-0af0d5aeeef9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_3_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 14,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21888f14-e71e-49aa-9a83-bebc9d2d7d5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9f6d8bf-63cc-453e-a315-0af0d5aeeef9",
            "compositeImage": {
                "id": "610f20cb-a4eb-4da6-9b10-39e6a6b7bae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21888f14-e71e-49aa-9a83-bebc9d2d7d5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fe2ca43-bc1a-43a3-831f-2b1d47d21c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21888f14-e71e-49aa-9a83-bebc9d2d7d5f",
                    "LayerId": "9e438c98-de23-4eca-bcde-365d8eb1f57b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "9e438c98-de23-4eca-bcde-365d8eb1f57b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9f6d8bf-63cc-453e-a315-0af0d5aeeef9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}