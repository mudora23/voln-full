{
    "id": "c86b78e1-b0fe-4ec9-aeaf-6112d9d16c6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_3_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00ed887b-e253-46d7-8a37-334945ecaba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c86b78e1-b0fe-4ec9-aeaf-6112d9d16c6a",
            "compositeImage": {
                "id": "4199af63-560f-40aa-8a9b-6e0ec395f8fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00ed887b-e253-46d7-8a37-334945ecaba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b613f23-9ad4-4f4b-a483-e088cd758a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00ed887b-e253-46d7-8a37-334945ecaba3",
                    "LayerId": "b4336559-b0e4-4456-b09a-0bf7a47dfdb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "b4336559-b0e4-4456-b09a-0bf7a47dfdb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c86b78e1-b0fe-4ec9-aeaf-6112d9d16c6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}