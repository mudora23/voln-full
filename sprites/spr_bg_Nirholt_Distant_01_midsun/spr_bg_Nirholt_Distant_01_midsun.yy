{
    "id": "2ec09682-b229-4b73-88d8-e37d97cf4994",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_Distant_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7f293ab-10a4-401b-ad27-3d5832983825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ec09682-b229-4b73-88d8-e37d97cf4994",
            "compositeImage": {
                "id": "ca5a215f-487a-4a5d-ae88-1eb234bf3011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f293ab-10a4-401b-ad27-3d5832983825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fb6ead-bed1-4e37-bc83-9f41eeff97fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f293ab-10a4-401b-ad27-3d5832983825",
                    "LayerId": "5a83f4b8-9eee-4b25-834a-ddaf1c42ddfa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "5a83f4b8-9eee-4b25-834a-ddaf1c42ddfa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ec09682-b229-4b73-88d8-e37d97cf4994",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}