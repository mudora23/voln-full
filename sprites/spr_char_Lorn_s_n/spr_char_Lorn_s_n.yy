{
    "id": "7e08f719-2c6c-410b-a6fa-a8d6456eadc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Lorn_s_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1999,
    "bbox_left": 194,
    "bbox_right": 1010,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5ba0e3a-8cf7-491b-9817-9c5e3a874007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e08f719-2c6c-410b-a6fa-a8d6456eadc7",
            "compositeImage": {
                "id": "9a47d378-e1e5-4a58-ae59-65e85c21f5cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5ba0e3a-8cf7-491b-9817-9c5e3a874007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7680f00b-e247-4139-8ad5-d8012d0fbe65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ba0e3a-8cf7-491b-9817-9c5e3a874007",
                    "LayerId": "2580e08f-2410-43bf-93e8-111c59f323b8"
                }
            ]
        },
        {
            "id": "2da6d182-aefa-4341-8c37-dcd94c659607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e08f719-2c6c-410b-a6fa-a8d6456eadc7",
            "compositeImage": {
                "id": "3f6307a4-b89a-4757-b6c9-8a72b3fb2a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da6d182-aefa-4341-8c37-dcd94c659607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a961b69c-3bcb-48cb-afba-5c8912531205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da6d182-aefa-4341-8c37-dcd94c659607",
                    "LayerId": "2580e08f-2410-43bf-93e8-111c59f323b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "2580e08f-2410-43bf-93e8-111c59f323b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e08f719-2c6c-410b-a6fa-a8d6456eadc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}