{
    "id": "0f18a78c-03b7-42ce-958d-32b8a36e2e9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_1b_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 17,
    "bbox_right": 256,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b74ac64-3002-4c39-8965-e080f037288f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f18a78c-03b7-42ce-958d-32b8a36e2e9e",
            "compositeImage": {
                "id": "5abf6c10-ebbe-40e3-a29c-58ffe0f1a98a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b74ac64-3002-4c39-8965-e080f037288f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7cf8fab-b3b4-4c3a-8305-49db50ac7e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b74ac64-3002-4c39-8965-e080f037288f",
                    "LayerId": "773fb24d-5a31-475f-94ba-78697fcdefdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "773fb24d-5a31-475f-94ba-78697fcdefdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f18a78c-03b7-42ce-958d-32b8a36e2e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}