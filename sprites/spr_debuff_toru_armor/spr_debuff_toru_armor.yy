{
    "id": "daad2096-3643-4988-a892-8844feb812df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_toru_armor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 2,
    "bbox_right": 48,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70e3166d-0ca8-4437-821a-6afedc13195d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daad2096-3643-4988-a892-8844feb812df",
            "compositeImage": {
                "id": "b1b835fd-6005-4e1a-830f-1da50742a099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e3166d-0ca8-4437-821a-6afedc13195d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354f9139-32ec-47f8-9d84-0167573da10b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e3166d-0ca8-4437-821a-6afedc13195d",
                    "LayerId": "b2eddd23-e6d1-4af8-bb59-7d4530dc19bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "b2eddd23-e6d1-4af8-bb59-7d4530dc19bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daad2096-3643-4988-a892-8844feb812df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}