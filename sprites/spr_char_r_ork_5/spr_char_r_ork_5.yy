{
    "id": "4cf4ddcf-7b7b-4875-8421-c98d4edd513e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1997,
    "bbox_left": 0,
    "bbox_right": 1111,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a0a256d-5345-4967-9d4d-8e6aa238ad54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cf4ddcf-7b7b-4875-8421-c98d4edd513e",
            "compositeImage": {
                "id": "58375144-4062-4738-b421-23dab6691d63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0a256d-5345-4967-9d4d-8e6aa238ad54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1449206-dd4f-40e7-85a3-58885d2de868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0a256d-5345-4967-9d4d-8e6aa238ad54",
                    "LayerId": "bfb1d147-e1ee-4262-890c-83911fcebb2f"
                }
            ]
        },
        {
            "id": "4514814d-d741-4418-8c99-21b0ab19692e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cf4ddcf-7b7b-4875-8421-c98d4edd513e",
            "compositeImage": {
                "id": "8612ea00-76d7-4aff-9688-51c958563662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4514814d-d741-4418-8c99-21b0ab19692e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb4b51ee-525e-46d1-8b71-a5617feb133b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4514814d-d741-4418-8c99-21b0ab19692e",
                    "LayerId": "bfb1d147-e1ee-4262-890c-83911fcebb2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "bfb1d147-e1ee-4262-890c-83911fcebb2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cf4ddcf-7b7b-4875-8421-c98d4edd513e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}