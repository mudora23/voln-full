{
    "id": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "icon_control",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb5ca168-6153-4595-ac98-be7b1c7f9623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
            "compositeImage": {
                "id": "cba44c44-2433-441a-b71f-e8d0be100f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb5ca168-6153-4595-ac98-be7b1c7f9623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06ecdac-29a8-47e0-a6ad-c83c769b8900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb5ca168-6153-4595-ac98-be7b1c7f9623",
                    "LayerId": "f6cddc91-9eba-476e-a692-ee524ec1f97c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f6cddc91-9eba-476e-a692-ee524ec1f97c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}