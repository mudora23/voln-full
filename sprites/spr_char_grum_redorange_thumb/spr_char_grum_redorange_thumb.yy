{
    "id": "77bc29b2-0a58-4a7d-84b8-a1bc6772f520",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_redorange_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d554645-6278-4dfd-b522-7bc3820c6a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77bc29b2-0a58-4a7d-84b8-a1bc6772f520",
            "compositeImage": {
                "id": "c7126c05-478f-4844-ba43-696b293d399c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d554645-6278-4dfd-b522-7bc3820c6a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15fa9d68-c94b-4916-9552-f9b1b58cc085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d554645-6278-4dfd-b522-7bc3820c6a4c",
                    "LayerId": "3e4cbebc-7ba7-4f0b-b631-872a5e01938d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "3e4cbebc-7ba7-4f0b-b631-872a5e01938d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77bc29b2-0a58-4a7d-84b8-a1bc6772f520",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}