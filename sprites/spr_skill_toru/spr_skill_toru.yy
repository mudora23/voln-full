{
    "id": "1a0b7072-36ce-4343-a8c6-f638d0fedebf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_toru",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27417cab-8b80-4690-96c1-f59cf67a0f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a0b7072-36ce-4343-a8c6-f638d0fedebf",
            "compositeImage": {
                "id": "eca05c3d-f72a-4605-9a04-c4bb8ab8266c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27417cab-8b80-4690-96c1-f59cf67a0f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91113c3a-60db-48fe-bab9-ae6f6fcf7561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27417cab-8b80-4690-96c1-f59cf67a0f0c",
                    "LayerId": "489b74b3-bcc8-49e5-b0f5-fc4f2c305b72"
                },
                {
                    "id": "2a180f90-0078-445d-84b7-ca73d0a988e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27417cab-8b80-4690-96c1-f59cf67a0f0c",
                    "LayerId": "d96a7163-7429-4e6a-8ff9-b6489d2c7756"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "d96a7163-7429-4e6a-8ff9-b6489d2c7756",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a0b7072-36ce-4343-a8c6-f638d0fedebf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "489b74b3-bcc8-49e5-b0f5-fc4f2c305b72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a0b7072-36ce-4343-a8c6-f638d0fedebf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}