{
    "id": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MARKER_WHITE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "511f3a9a-353c-409a-abe5-9d9bc7c40d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
            "compositeImage": {
                "id": "46a2ad07-d2b4-463b-8767-c48e6efab1ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511f3a9a-353c-409a-abe5-9d9bc7c40d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31edb79b-4c29-48a1-bd4b-e73c367c4160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511f3a9a-353c-409a-abe5-9d9bc7c40d72",
                    "LayerId": "7c32e656-a7f2-414d-bbf3-4040bb157584"
                }
            ]
        },
        {
            "id": "24003028-130a-4b5d-84a8-6f7cf7d45cb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
            "compositeImage": {
                "id": "a6df5661-25d0-4cb8-b715-37f7857e4539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24003028-130a-4b5d-84a8-6f7cf7d45cb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900285fd-e2b0-4144-b537-b374ce4b5d7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24003028-130a-4b5d-84a8-6f7cf7d45cb6",
                    "LayerId": "7c32e656-a7f2-414d-bbf3-4040bb157584"
                }
            ]
        },
        {
            "id": "d814d09d-2589-403e-a411-47365dd7878b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
            "compositeImage": {
                "id": "1a9b483c-9c99-4225-a49b-2d918e4c21d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d814d09d-2589-403e-a411-47365dd7878b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb1b579-00a9-498e-b44e-e029168996e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d814d09d-2589-403e-a411-47365dd7878b",
                    "LayerId": "7c32e656-a7f2-414d-bbf3-4040bb157584"
                }
            ]
        },
        {
            "id": "1fffc7c3-b049-4cfe-a299-a24006a4c210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
            "compositeImage": {
                "id": "8dec328e-04d2-413a-bf4a-313b2734f025",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fffc7c3-b049-4cfe-a299-a24006a4c210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7471b932-6f65-453e-9085-a6b0cc1f3e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fffc7c3-b049-4cfe-a299-a24006a4c210",
                    "LayerId": "7c32e656-a7f2-414d-bbf3-4040bb157584"
                }
            ]
        },
        {
            "id": "f906bc93-03c9-44ee-b66e-609a44ea2830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
            "compositeImage": {
                "id": "c0803cef-28cb-4ad3-8457-e377fb560520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f906bc93-03c9-44ee-b66e-609a44ea2830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c37369c-2d17-4e1b-af1e-875b3fb3258a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f906bc93-03c9-44ee-b66e-609a44ea2830",
                    "LayerId": "7c32e656-a7f2-414d-bbf3-4040bb157584"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "7c32e656-a7f2-414d-bbf3-4040bb157584",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75b1d189-e5e4-4f7a-b673-491cb04d1b80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 0
}