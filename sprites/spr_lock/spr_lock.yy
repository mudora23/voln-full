{
    "id": "04d9b296-6a79-45be-865f-8eddd658a971",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 16,
    "bbox_right": 86,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15464e93-3bd6-4188-b475-c92b1201a9b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d9b296-6a79-45be-865f-8eddd658a971",
            "compositeImage": {
                "id": "83b532b5-e56a-482a-84f4-2633f6f79faf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15464e93-3bd6-4188-b475-c92b1201a9b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3430686e-b136-498e-a9aa-5eacfc8cd558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15464e93-3bd6-4188-b475-c92b1201a9b6",
                    "LayerId": "ccc8b1c6-12f3-4438-851e-834d89525770"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ccc8b1c6-12f3-4438-851e-834d89525770",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04d9b296-6a79-45be-865f-8eddd658a971",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}