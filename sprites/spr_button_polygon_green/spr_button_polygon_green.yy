{
    "id": "1e0679fd-eab8-4459-b551-c678f6d841d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_polygon_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fac73c30-4c97-4aea-a72c-de16242cdf6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e0679fd-eab8-4459-b551-c678f6d841d5",
            "compositeImage": {
                "id": "beb2e74c-ca72-4e6c-b4d4-7a67c1c3d973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac73c30-4c97-4aea-a72c-de16242cdf6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fe7b3e4-80ad-4d4e-96e1-d8926f9f4516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac73c30-4c97-4aea-a72c-de16242cdf6f",
                    "LayerId": "b0332cf3-8ef9-4ed1-a7f3-f7a9370fc8b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "b0332cf3-8ef9-4ed1-a7f3-f7a9370fc8b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e0679fd-eab8-4459-b551-c678f6d841d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 35
}