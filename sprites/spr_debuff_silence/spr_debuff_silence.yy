{
    "id": "8f76327c-c349-49b0-bdb8-11cf562ca8a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_silence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 2,
    "bbox_right": 47,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5856b8d7-da96-49d2-8544-c03a46502d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f76327c-c349-49b0-bdb8-11cf562ca8a7",
            "compositeImage": {
                "id": "39c8f0f6-6659-410b-af44-5325c19676b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5856b8d7-da96-49d2-8544-c03a46502d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dcbc948-cc10-418b-b8c1-8162a958b9eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5856b8d7-da96-49d2-8544-c03a46502d95",
                    "LayerId": "edc5e3d3-6ca1-4831-b9dd-0953d1825a20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "edc5e3d3-6ca1-4831-b9dd-0953d1825a20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f76327c-c349-49b0-bdb8-11cf562ca8a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}