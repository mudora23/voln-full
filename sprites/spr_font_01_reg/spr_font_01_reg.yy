{
    "id": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_reg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a61575a-ebf4-40ef-9174-acef4f78a761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "3a169d58-9e3e-4567-a400-e713272afe08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a61575a-ebf4-40ef-9174-acef4f78a761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff871ffa-c665-4b05-90e4-0250b1c6d7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a61575a-ebf4-40ef-9174-acef4f78a761",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b73ef740-ca87-42ef-94a0-4743661b43e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a61575a-ebf4-40ef-9174-acef4f78a761",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "1294195c-5d15-4fa7-902a-76169314a8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5249132f-ec83-489d-a197-4667f27fd5bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1294195c-5d15-4fa7-902a-76169314a8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3ee2c9-7228-47b3-a0a4-166d4f053bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1294195c-5d15-4fa7-902a-76169314a8c0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "fff228b5-b38c-4bdd-bc77-3d899fe4a15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1294195c-5d15-4fa7-902a-76169314a8c0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "5222a9c0-1fa1-421f-a1c2-6db34d91c219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "81210a3a-885f-4e05-ad2d-02421504ad36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5222a9c0-1fa1-421f-a1c2-6db34d91c219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d54208e-d675-4ae4-8a86-6caa55f53045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5222a9c0-1fa1-421f-a1c2-6db34d91c219",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1da3e1e2-09e4-4f5a-9865-4e4cf338bc8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5222a9c0-1fa1-421f-a1c2-6db34d91c219",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "83d48ed7-f1ec-4e2d-a46b-3b625e18cb2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a0c867a4-8a9e-439d-9d00-0074c2e9c51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d48ed7-f1ec-4e2d-a46b-3b625e18cb2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72249ae-d6e2-4316-8364-b4323e62eb74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d48ed7-f1ec-4e2d-a46b-3b625e18cb2f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "a3c0b3f2-8518-427e-b459-e5d7e4853249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d48ed7-f1ec-4e2d-a46b-3b625e18cb2f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "cf175552-f699-4b53-8cee-565d09e4e491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "93828099-e9e7-463f-a268-33ce7091978b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf175552-f699-4b53-8cee-565d09e4e491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b07dd200-f4af-48a9-a354-f424158d796d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf175552-f699-4b53-8cee-565d09e4e491",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "eef7d03d-74ae-4bda-8bdf-d0e9768882b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf175552-f699-4b53-8cee-565d09e4e491",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "be4a504a-98d5-423d-98b9-ca339742be71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "960f01c3-9f0a-4f48-966b-4aaa0f42abf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4a504a-98d5-423d-98b9-ca339742be71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b65d69-6c92-4178-a2cb-02f3132bb538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4a504a-98d5-423d-98b9-ca339742be71",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "51f18a87-621d-461b-ad14-ccd50a8fcaf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4a504a-98d5-423d-98b9-ca339742be71",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "5f6ec4bd-ab74-4e37-ab07-441c5a9b0c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c34c38f2-08ce-4121-adbb-deec773abe9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6ec4bd-ab74-4e37-ab07-441c5a9b0c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe01fdfa-1550-498b-a092-567c9056fb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6ec4bd-ab74-4e37-ab07-441c5a9b0c01",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1cda8ca3-ca27-4eba-8206-74dd1859d304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6ec4bd-ab74-4e37-ab07-441c5a9b0c01",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "bc633d7c-fa7f-420b-acd4-7d180572be0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d118d8ef-03ee-416a-9327-b98a25e853a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc633d7c-fa7f-420b-acd4-7d180572be0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67108b36-0bee-467c-9658-0680ffea5855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc633d7c-fa7f-420b-acd4-7d180572be0e",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "0f74f750-d8ee-4061-86ef-003b33cce149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc633d7c-fa7f-420b-acd4-7d180572be0e",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "290fa26b-f92e-4d42-87a4-d4627044b95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "057aaf7b-351d-484c-893b-6aa0005177c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290fa26b-f92e-4d42-87a4-d4627044b95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5783dce5-a3b8-4371-8200-32978217720d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290fa26b-f92e-4d42-87a4-d4627044b95b",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1dd9948e-3cd9-404a-b7da-fa92d95ed258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290fa26b-f92e-4d42-87a4-d4627044b95b",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "ccf86dc9-7da4-473d-8d32-78bcbbaeeb77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a2647184-80ee-4948-8633-46c172bca246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf86dc9-7da4-473d-8d32-78bcbbaeeb77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46f431df-dc9d-4754-8a43-d228648b43df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf86dc9-7da4-473d-8d32-78bcbbaeeb77",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "fc8d390a-a3b9-4fd4-a933-3fc295082764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf86dc9-7da4-473d-8d32-78bcbbaeeb77",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "7ff5a9f5-e7cf-456b-a1ad-e0c4eea115d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "355dd3c0-09d0-4314-ad86-6b2ca49fe53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ff5a9f5-e7cf-456b-a1ad-e0c4eea115d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed04a49b-a467-49c7-b4d3-7cee83258fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff5a9f5-e7cf-456b-a1ad-e0c4eea115d6",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4c3b1a4d-3b60-4e67-b10e-ccec8faeecad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff5a9f5-e7cf-456b-a1ad-e0c4eea115d6",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "8a03fad8-b809-4b1f-861e-121a4c8f9865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "4cd0e6d3-4691-4afc-a62b-14e95ec96679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a03fad8-b809-4b1f-861e-121a4c8f9865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c004f256-8f9d-444a-8bf2-89813411da8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a03fad8-b809-4b1f-861e-121a4c8f9865",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "0669f57b-d2cf-41c0-a94e-436a17ae6fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a03fad8-b809-4b1f-861e-121a4c8f9865",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a0a778eb-2871-4b5e-8e0d-99648d1e2a29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "19beaf61-f4f0-4776-9c9b-8a1e543ba53c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0a778eb-2871-4b5e-8e0d-99648d1e2a29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0704ca32-c442-4831-8d54-281e15d4bf38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0a778eb-2871-4b5e-8e0d-99648d1e2a29",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "f55e1a26-e9a1-4226-8709-2dc6e2a8800f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0a778eb-2871-4b5e-8e0d-99648d1e2a29",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "df8f908b-3cfb-482b-829c-9e0d75cfec41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "de881860-69f2-4ba7-920c-367b63f217f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df8f908b-3cfb-482b-829c-9e0d75cfec41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b190da-f102-4772-8195-7e8e51cb0747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df8f908b-3cfb-482b-829c-9e0d75cfec41",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "d628236c-2fce-452a-9488-5b6252ceef5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df8f908b-3cfb-482b-829c-9e0d75cfec41",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3f09125a-4bb6-413f-9eca-1195221ee07f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "882d3b14-845c-46f3-bd8e-1fbebbfb5a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f09125a-4bb6-413f-9eca-1195221ee07f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ffdfa8d-e6ae-4226-9d0f-b497fcfed1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f09125a-4bb6-413f-9eca-1195221ee07f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1b1de720-2e0c-415d-b559-ee5cad8beb80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f09125a-4bb6-413f-9eca-1195221ee07f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3491334b-ad62-4a7c-88f0-5bf1319558f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5f11359d-7fd5-4bbe-b299-27833d97d935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3491334b-ad62-4a7c-88f0-5bf1319558f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f5e1029-8974-4460-aaa2-1aa4787153f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3491334b-ad62-4a7c-88f0-5bf1319558f0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "619ec331-f250-46fd-a913-a92591461065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3491334b-ad62-4a7c-88f0-5bf1319558f0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "aa60a752-27e1-483d-b3c2-e4b06533acd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "6efc013b-7e84-46af-9f44-99b2fff91ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa60a752-27e1-483d-b3c2-e4b06533acd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340b98ba-9c3e-4ac7-abaa-d0c1fd7e3d45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa60a752-27e1-483d-b3c2-e4b06533acd4",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1719ab1b-1a04-457d-9165-1c14cbb2fcc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa60a752-27e1-483d-b3c2-e4b06533acd4",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "61bd6106-62f3-4da3-b672-91a8a5d8fb88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "e25ee41a-55ff-4d09-b702-a608b465ddd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61bd6106-62f3-4da3-b672-91a8a5d8fb88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "603d5f7c-480b-43e0-bdf7-21356c020e61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61bd6106-62f3-4da3-b672-91a8a5d8fb88",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1d2569e4-58f4-414b-9612-b6d30d60a7a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61bd6106-62f3-4da3-b672-91a8a5d8fb88",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "79cd63c8-160f-4d40-9add-c41edc6aefa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "798d6bda-0555-4e43-bb2f-6cd3ba500f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79cd63c8-160f-4d40-9add-c41edc6aefa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afdff6bb-6531-4530-804f-270d76e47df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79cd63c8-160f-4d40-9add-c41edc6aefa5",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "832c42a8-29d2-4092-9f35-be678d99d2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79cd63c8-160f-4d40-9add-c41edc6aefa5",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "36c56b90-6566-4575-8a90-9a9d93061a4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a7a07718-d2bd-43a0-b8fb-a2c709248fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c56b90-6566-4575-8a90-9a9d93061a4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330bfffd-d800-45c4-aa55-b05c30ed3553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c56b90-6566-4575-8a90-9a9d93061a4b",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "242d549f-780b-4240-b545-7b89df4c6dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c56b90-6566-4575-8a90-9a9d93061a4b",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "69421d88-7996-4005-888f-323e57eb9a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "fb807f84-d0ea-4b6e-8f0c-07d8db8df9e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69421d88-7996-4005-888f-323e57eb9a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68cda7a0-3f4d-464f-beab-4a05bb140f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69421d88-7996-4005-888f-323e57eb9a14",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9b95ad9e-7263-4a10-83ee-c524e03c9515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69421d88-7996-4005-888f-323e57eb9a14",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "10eec9f6-a5ac-4c5f-9db1-ef0a55051e3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "ed9a913d-eee4-41ee-9ec5-368cac6b284c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10eec9f6-a5ac-4c5f-9db1-ef0a55051e3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7d50a5-2491-492c-86a6-43d17bbe59a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10eec9f6-a5ac-4c5f-9db1-ef0a55051e3b",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "6014df41-2ce4-4ba1-bdbb-72ba3f1be605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10eec9f6-a5ac-4c5f-9db1-ef0a55051e3b",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "6b12d760-6b5f-4721-9159-0baee1540b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "15aaa689-2c5b-4a94-8c6e-99a8df3b3005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b12d760-6b5f-4721-9159-0baee1540b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098e71c7-da71-45b1-a482-9685af220812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b12d760-6b5f-4721-9159-0baee1540b3f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "31e5ee25-ba54-44ad-9def-5402093c4679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b12d760-6b5f-4721-9159-0baee1540b3f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2397ba11-7900-4936-9948-219e98b8ae70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "9c6d4251-053c-489b-8f6a-b86fe2b5f1ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2397ba11-7900-4936-9948-219e98b8ae70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bac5552-daf0-4646-967e-86ebc5d937d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2397ba11-7900-4936-9948-219e98b8ae70",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "070a1602-5d9a-4d4b-9dec-1bfe1a98eec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2397ba11-7900-4936-9948-219e98b8ae70",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "d92f1134-3f26-4a73-8234-90f15796a5c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "873ea357-e98e-42f9-9a4c-116a6707c91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d92f1134-3f26-4a73-8234-90f15796a5c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515be3c7-ac9c-4598-8e9c-c0d3b3767e46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d92f1134-3f26-4a73-8234-90f15796a5c5",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "7f6989b6-5859-4678-a79d-8fb034ffdd96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d92f1134-3f26-4a73-8234-90f15796a5c5",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "54260f27-97a1-46fe-8b4f-7936cca790c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "70cdafd4-0cfb-4a0c-a084-8630934bc994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54260f27-97a1-46fe-8b4f-7936cca790c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7337a7ba-09c0-4117-86bf-f7afdab6844b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54260f27-97a1-46fe-8b4f-7936cca790c7",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "168ca95e-a039-4253-a18a-102c376dec97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54260f27-97a1-46fe-8b4f-7936cca790c7",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "4d89772f-af24-4dca-a40b-472e1916af90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5c866fc7-270a-4e59-b3a9-b6671e2dd839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d89772f-af24-4dca-a40b-472e1916af90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c76ef02-5ad0-41ab-acc2-8904210cadf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d89772f-af24-4dca-a40b-472e1916af90",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "aee4c5aa-1f31-4b61-bc78-7a3a8075e889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d89772f-af24-4dca-a40b-472e1916af90",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a00ce849-7f4b-497d-a756-ea4fe2236547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "2141205e-8730-4c3b-a0d0-1940e2ad07e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00ce849-7f4b-497d-a756-ea4fe2236547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d04494-3430-4f73-bc31-a1a02407d16d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00ce849-7f4b-497d-a756-ea4fe2236547",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "3424ad51-39be-4753-91b6-62a4386bfb76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00ce849-7f4b-497d-a756-ea4fe2236547",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "4e4ff462-9c9a-4804-b212-fca95577a053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "cbcd2f40-9041-4f78-973c-713f98647e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e4ff462-9c9a-4804-b212-fca95577a053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e724aa-b1ee-4fa8-86a5-2275ef87fa89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4ff462-9c9a-4804-b212-fca95577a053",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "e87043b2-d4b7-4a51-97c2-b6b328df1187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4ff462-9c9a-4804-b212-fca95577a053",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "28f0eb9f-2138-4945-9e38-7ac3342a2240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a9199ad4-5e35-49ce-b7b7-ae6f6c789669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f0eb9f-2138-4945-9e38-7ac3342a2240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad072f5-cccd-4e2e-a751-dbf26e09729c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f0eb9f-2138-4945-9e38-7ac3342a2240",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ead69212-33e6-4f2e-8f03-8ccca8bfcce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f0eb9f-2138-4945-9e38-7ac3342a2240",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a11b1cf8-4d3d-47e9-bd6f-8fe18b8866c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "75e055d1-7277-4a57-a1a0-1f3a671d4b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a11b1cf8-4d3d-47e9-bd6f-8fe18b8866c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd9c9aa-f287-4095-9adc-a14bc738b71d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a11b1cf8-4d3d-47e9-bd6f-8fe18b8866c0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1a276871-07d4-47b1-8f58-180f9b3c141f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a11b1cf8-4d3d-47e9-bd6f-8fe18b8866c0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "d868a476-2694-4757-971a-0735bf56c986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "1417a98c-fd77-4f7e-a386-a4446160a970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d868a476-2694-4757-971a-0735bf56c986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f769b20-bd5f-46b6-bdd9-b3eea27d9256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d868a476-2694-4757-971a-0735bf56c986",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "223f462c-be0c-494d-a678-b40d1be1d9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d868a476-2694-4757-971a-0735bf56c986",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "9264a483-6364-4e0d-b84e-feab070c4a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "b2ed0cd4-f7c0-44bb-85ca-457da615fd41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9264a483-6364-4e0d-b84e-feab070c4a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a345f2-8ef9-42e9-9f09-1d14aea2a1d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9264a483-6364-4e0d-b84e-feab070c4a94",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "7421a56f-2abe-499a-9b83-f6207e1db58a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9264a483-6364-4e0d-b84e-feab070c4a94",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "fe99bed3-3242-4c59-ac52-0aa33db93ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "1cc3c2ee-f516-478b-b3a4-919a453904a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe99bed3-3242-4c59-ac52-0aa33db93ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3268b336-0d39-4d00-a18b-3dcb9237b821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe99bed3-3242-4c59-ac52-0aa33db93ae8",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "215f1bfd-a82e-4614-93db-e0b7487d508c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe99bed3-3242-4c59-ac52-0aa33db93ae8",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "ccdc9ba1-a682-466a-b51b-82e59c7827f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "ee9df8d8-e246-4900-8f5a-a20c2dc78c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccdc9ba1-a682-466a-b51b-82e59c7827f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d225c7eb-63b2-43d9-977d-74b322e08c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccdc9ba1-a682-466a-b51b-82e59c7827f2",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9a23d5ee-3b98-4741-9561-d2ebcf14a853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccdc9ba1-a682-466a-b51b-82e59c7827f2",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "d5255778-edd3-4fef-8a09-c4ad04ef4a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "b4eefc7b-917d-4c3e-b512-eb4eb12ee8ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5255778-edd3-4fef-8a09-c4ad04ef4a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47e5f6bb-7b33-476c-80d7-bceba89acc2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5255778-edd3-4fef-8a09-c4ad04ef4a82",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "06b86210-09ea-4d33-abf8-9b7f2ec9ed29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5255778-edd3-4fef-8a09-c4ad04ef4a82",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "93dce1b5-53c7-45e3-98d9-596fc1a6b0c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "e89df2a8-5131-4d73-9262-c12d0e18a811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93dce1b5-53c7-45e3-98d9-596fc1a6b0c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d36e957-f85e-4a68-b45d-d109835c9610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93dce1b5-53c7-45e3-98d9-596fc1a6b0c8",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "62655137-ce18-4e2d-9e6b-c3ea7549c505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93dce1b5-53c7-45e3-98d9-596fc1a6b0c8",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "d39bad3d-f36d-4b68-9a7e-6c8fd0250dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "ea56b799-207b-4703-8764-553d615094cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39bad3d-f36d-4b68-9a7e-6c8fd0250dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ec4f3de-b7ef-48a7-a676-b6c1bb8fbefc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39bad3d-f36d-4b68-9a7e-6c8fd0250dce",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "f2cb6a15-4149-4c12-9546-c39222d2cc63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39bad3d-f36d-4b68-9a7e-6c8fd0250dce",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a6caa9d1-116c-4a87-ab1b-46b3d79237e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5d9ea22f-4a92-497c-8724-1b508ecca09b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6caa9d1-116c-4a87-ab1b-46b3d79237e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf1abd5-ffd4-430c-8c07-fe6b6e70dcd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6caa9d1-116c-4a87-ab1b-46b3d79237e5",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9190c019-e4b1-4e09-81e5-47d38ca34d36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6caa9d1-116c-4a87-ab1b-46b3d79237e5",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "e090b472-104a-4478-8bf8-60f984df29f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "e38738f4-fd40-487e-9caf-a0465337617d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e090b472-104a-4478-8bf8-60f984df29f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e0fcdf-ae97-431c-9eb3-9daf1d3d34c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e090b472-104a-4478-8bf8-60f984df29f6",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9534beaf-7eb9-439d-b67f-d7b23fa9dece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e090b472-104a-4478-8bf8-60f984df29f6",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "1777f0b2-c477-4e8e-b680-58443a36c2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "62892f22-121c-4283-bd7d-c4b3f4fd38bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1777f0b2-c477-4e8e-b680-58443a36c2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d39967-7de5-4e98-ab31-cd402a9b1785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1777f0b2-c477-4e8e-b680-58443a36c2d8",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "604e60ba-d477-4f81-8fc5-a65ba3fd0244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1777f0b2-c477-4e8e-b680-58443a36c2d8",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "80b5fe92-d8f7-4979-8138-33267a25ccbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "247c7892-c22d-4490-8c84-49395795d99a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b5fe92-d8f7-4979-8138-33267a25ccbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc32eb58-1e21-4a70-84d1-7ecb23dc9719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b5fe92-d8f7-4979-8138-33267a25ccbf",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4d8f6834-d7be-4982-85f1-8d6d8d46e5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b5fe92-d8f7-4979-8138-33267a25ccbf",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "96258e6c-0ea9-4828-9f3a-ad18eea8f4bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c7d9a62a-157d-4856-96ea-a72cf11fa6e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96258e6c-0ea9-4828-9f3a-ad18eea8f4bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf721f5-2e14-4b18-a194-45472a8cbcf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96258e6c-0ea9-4828-9f3a-ad18eea8f4bd",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1f0ea330-687b-4673-ab9f-0b77c56027f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96258e6c-0ea9-4828-9f3a-ad18eea8f4bd",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "ad8dfdc3-97ec-4fa9-b819-2732c2862568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "3e4a825b-6582-4004-ad4a-57a6f08b6208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8dfdc3-97ec-4fa9-b819-2732c2862568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f62d005-56da-40a0-90b3-8edfc629b56b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8dfdc3-97ec-4fa9-b819-2732c2862568",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "39f823a7-5cee-4df4-b904-4e14ae8132eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8dfdc3-97ec-4fa9-b819-2732c2862568",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "26a551b6-f3a0-4681-b1db-d8a85c43fce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "bee07450-9bbf-4816-931e-178bf84bf859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a551b6-f3a0-4681-b1db-d8a85c43fce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b544eae3-9914-4a2e-8c83-8a04b2566dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a551b6-f3a0-4681-b1db-d8a85c43fce0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "5a22d06a-4db2-4f7a-9a48-9983b3ffa457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a551b6-f3a0-4681-b1db-d8a85c43fce0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "93fd2b0b-abc5-412f-99ae-3ae35253b49d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "db924bd6-5c52-476e-8179-80fe79892a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93fd2b0b-abc5-412f-99ae-3ae35253b49d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94245001-cc12-4e75-8e99-1e60035aef80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93fd2b0b-abc5-412f-99ae-3ae35253b49d",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4e595ad2-1c46-48c6-8bec-c6e825724ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93fd2b0b-abc5-412f-99ae-3ae35253b49d",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "cf0f5f0f-6496-455f-96a3-12b6abb66dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "f1e14ab6-f326-4e59-a516-6a959d3b9da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf0f5f0f-6496-455f-96a3-12b6abb66dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634caa36-467a-4571-81aa-14d5daf862bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf0f5f0f-6496-455f-96a3-12b6abb66dca",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "eca2047f-2d3e-4d6b-9ff0-e1637e681890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf0f5f0f-6496-455f-96a3-12b6abb66dca",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "6aaeb690-4608-4ddc-ac84-800bdda03ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "e552f364-b7b4-4476-8569-59a5529f52c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aaeb690-4608-4ddc-ac84-800bdda03ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee02f7f8-4eaf-41e9-8b9e-2e84ec82818f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aaeb690-4608-4ddc-ac84-800bdda03ad0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "5527ac1e-5f16-46cb-a3ec-c5d217aad15b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aaeb690-4608-4ddc-ac84-800bdda03ad0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "8148a5e1-65c6-4702-b236-a79185e00b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "3508aefa-7286-4cba-b227-30e70cd650af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8148a5e1-65c6-4702-b236-a79185e00b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f7bfbc-9dd4-49be-be2f-f4a0334348ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8148a5e1-65c6-4702-b236-a79185e00b0d",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "691a3e8b-80c3-4b98-baa9-d48a0d1fd7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8148a5e1-65c6-4702-b236-a79185e00b0d",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "189155a6-7fcb-4419-b580-bb0068d5b8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "051b10b2-c952-4170-8e2c-17602d6c1e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189155a6-7fcb-4419-b580-bb0068d5b8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a16cc5-a138-4d71-85a8-2b70376552f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189155a6-7fcb-4419-b580-bb0068d5b8ec",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "56537f19-9cf3-4af2-aae1-70110261e0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189155a6-7fcb-4419-b580-bb0068d5b8ec",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "ebed1a8e-d82b-464e-85db-3542682cbeff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c4003fa9-ed5c-4cc1-af14-32de86bbc26d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebed1a8e-d82b-464e-85db-3542682cbeff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5ebbf3-9060-46f2-9a21-9566f5486b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebed1a8e-d82b-464e-85db-3542682cbeff",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "73e3a939-929c-49c2-a6f0-9ad574167799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebed1a8e-d82b-464e-85db-3542682cbeff",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "aa7d8120-9072-4ae4-b2b3-e0e909932206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "6093265b-ad99-400c-bc9e-a84402d2235c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7d8120-9072-4ae4-b2b3-e0e909932206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1b1fec-30ee-4dfd-a15b-eecc4375bdce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7d8120-9072-4ae4-b2b3-e0e909932206",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "7f966911-fb02-4959-9318-ce9606124994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7d8120-9072-4ae4-b2b3-e0e909932206",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3ee68c9e-7b7d-4895-bdf5-d5fb34d53164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "0eaeeda9-88a8-408f-9cf5-c55f60400304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee68c9e-7b7d-4895-bdf5-d5fb34d53164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa3b320-6bdd-49a9-97c7-d5e155708d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee68c9e-7b7d-4895-bdf5-d5fb34d53164",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "cd18c89a-a6ee-463e-bcf6-f0bf811c2f3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee68c9e-7b7d-4895-bdf5-d5fb34d53164",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "300f2c5f-db44-490b-9284-bdcd5e9945e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "9f909400-9ba8-4dc8-9b0a-76f44c7830c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300f2c5f-db44-490b-9284-bdcd5e9945e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a085524-482f-481f-9eb8-4c80173b6e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300f2c5f-db44-490b-9284-bdcd5e9945e3",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "5df2ca28-2f00-48c2-ba17-374b74c16364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300f2c5f-db44-490b-9284-bdcd5e9945e3",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "026f08a9-c3dd-4c86-8ae5-8faaab1aed19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "7b806eed-838d-4264-b6f4-ac6c6ee82e1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026f08a9-c3dd-4c86-8ae5-8faaab1aed19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8dce6db-b1b7-41aa-8d5d-42da43206a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026f08a9-c3dd-4c86-8ae5-8faaab1aed19",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9ce1e73d-5f4d-44d9-b3d0-40b798901e7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026f08a9-c3dd-4c86-8ae5-8faaab1aed19",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "34836882-e819-4a18-8e24-2f5e21c45d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "1e75259c-2aaf-438d-94b6-727093de6417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34836882-e819-4a18-8e24-2f5e21c45d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "346877c6-4bcc-4dd7-b6d2-a9b6fde0fbae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34836882-e819-4a18-8e24-2f5e21c45d41",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b17c96fb-80a0-4f88-8e63-0c3993cad239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34836882-e819-4a18-8e24-2f5e21c45d41",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "07f235be-19a2-4bbd-9816-5db161f59099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "415972fe-a679-463f-9f56-57ddd4708055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f235be-19a2-4bbd-9816-5db161f59099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e72b50-8413-4ef9-947a-64f0e625c691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f235be-19a2-4bbd-9816-5db161f59099",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4c1ef835-1253-4613-8953-6231f93c4a7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f235be-19a2-4bbd-9816-5db161f59099",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "c2620771-9256-41d2-b066-0f3177c6a8fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "ce496976-5907-436e-a924-65d4a8a45678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2620771-9256-41d2-b066-0f3177c6a8fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f44f12a-d074-458d-81bf-c78ffaf09436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2620771-9256-41d2-b066-0f3177c6a8fd",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "5ebe5e17-9007-4cde-93e5-08014e8730af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2620771-9256-41d2-b066-0f3177c6a8fd",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2f3d6078-d1af-453e-9583-b9152f06a57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "e8acd45c-5ab7-459d-af41-d57ea84a70d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3d6078-d1af-453e-9583-b9152f06a57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23017da3-de33-4f42-9c2e-1d9409e56920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3d6078-d1af-453e-9583-b9152f06a57b",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "23faa56e-f7dc-45ed-be86-6236aee84431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3d6078-d1af-453e-9583-b9152f06a57b",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f26302f8-781d-43b8-b314-9f0ea892e781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "ce796064-9eb3-419c-9658-f1eac78c9049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f26302f8-781d-43b8-b314-9f0ea892e781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5031c71-929c-498e-b4e7-489e7fd6e33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26302f8-781d-43b8-b314-9f0ea892e781",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "e5bd91c9-8521-4063-82c6-0d6347cba992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26302f8-781d-43b8-b314-9f0ea892e781",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "9920d555-91a1-477a-b865-d1f6619d6d9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c37b0907-5c97-47db-afa0-d47c032ed9c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9920d555-91a1-477a-b865-d1f6619d6d9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "603eddc3-32a1-4d9b-babe-8e2c2110f087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9920d555-91a1-477a-b865-d1f6619d6d9c",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ab5db92e-07c7-4ebc-9d7c-c043334776a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9920d555-91a1-477a-b865-d1f6619d6d9c",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f1d4c730-2add-48af-b38c-e3f41df0a0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "9c4014e6-b698-48df-9372-c15ff7fd346c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d4c730-2add-48af-b38c-e3f41df0a0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f059f2d5-e109-45b0-8d9d-db305ba07409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d4c730-2add-48af-b38c-e3f41df0a0cd",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "d91077c1-d880-45b6-b802-3c12c754ceca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d4c730-2add-48af-b38c-e3f41df0a0cd",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f05e950e-68d1-4df6-b17f-1a0d5603f48a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "72d12363-d54f-49e5-a485-ee79f85ccbf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05e950e-68d1-4df6-b17f-1a0d5603f48a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f7afed5-4c6d-47a6-9bad-5565c9e78638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05e950e-68d1-4df6-b17f-1a0d5603f48a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "854f5a3a-498a-4714-97a2-c2393555787e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05e950e-68d1-4df6-b17f-1a0d5603f48a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "45eee6f5-2557-414b-a22e-a0ab1f5d8058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "25b6ed52-0b3a-4d4a-a9e8-e761a5c7874f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45eee6f5-2557-414b-a22e-a0ab1f5d8058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f10b19b1-25c3-4dad-a07b-935dbb40f060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45eee6f5-2557-414b-a22e-a0ab1f5d8058",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "0edc0847-9dd3-4f1a-a343-8429b978e46e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45eee6f5-2557-414b-a22e-a0ab1f5d8058",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2af649c5-80d5-4394-adc1-eb330a0f1839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d2c75d68-d5b0-4b51-a3b1-7caa170e6180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2af649c5-80d5-4394-adc1-eb330a0f1839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6d2811-14a1-42d3-bd2f-9c067eb150c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2af649c5-80d5-4394-adc1-eb330a0f1839",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "2fe6e5ff-70a1-4381-8894-e63bcb3364ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2af649c5-80d5-4394-adc1-eb330a0f1839",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "20063149-6fe5-4b5d-99dc-cc3eed8f354d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "492ffbca-e301-44ba-90ae-780d9ad5952b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20063149-6fe5-4b5d-99dc-cc3eed8f354d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1445bd22-05e5-419e-8273-12b44b6b403f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20063149-6fe5-4b5d-99dc-cc3eed8f354d",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "fee90b80-32ce-4235-87bc-21b4a2e19428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20063149-6fe5-4b5d-99dc-cc3eed8f354d",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a5d0989b-834d-4483-89d2-c6e92411c3ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "0df93535-0122-4ce4-a64e-f73187d1e05a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d0989b-834d-4483-89d2-c6e92411c3ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38d970c2-947e-4cea-ab03-1df80e32fcb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d0989b-834d-4483-89d2-c6e92411c3ee",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ec62a40f-8f4f-40a7-b835-9f15005c9b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d0989b-834d-4483-89d2-c6e92411c3ee",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "aa6bd4a1-0183-4dd6-89a2-37ac6c367b14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c3b6329c-e3e1-4f66-bfe1-ab8f9730bf22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6bd4a1-0183-4dd6-89a2-37ac6c367b14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17150b10-93a3-4f3f-a3f3-bc93dc3f2019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6bd4a1-0183-4dd6-89a2-37ac6c367b14",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "0cf43156-dc30-477e-a059-c4d8a5f616de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6bd4a1-0183-4dd6-89a2-37ac6c367b14",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a0b38e1c-3756-4357-8a1b-47b5b53bfa46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c53d1829-7001-4b21-80cc-111334137843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b38e1c-3756-4357-8a1b-47b5b53bfa46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e50486dc-75e5-4a35-a49d-9d204484dd2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b38e1c-3756-4357-8a1b-47b5b53bfa46",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b840698e-74fa-492e-b9be-76d46bf8d288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b38e1c-3756-4357-8a1b-47b5b53bfa46",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "65884724-0f95-4c41-a5fe-aa46576216d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "09f1a43b-4a84-4999-a2e3-c0a68169ac62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65884724-0f95-4c41-a5fe-aa46576216d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0405928f-ee07-48fb-9e0c-fc130ad402a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65884724-0f95-4c41-a5fe-aa46576216d9",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "da8f55d0-38ce-4a44-adb6-7ddaff3c1315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65884724-0f95-4c41-a5fe-aa46576216d9",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "7b27b43f-4d39-4cc9-99de-bf7fc44dce22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "317655fb-f2d4-4c16-bd48-a8be731a6cee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b27b43f-4d39-4cc9-99de-bf7fc44dce22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07338aca-08ec-4ae5-8f14-dfca9f5cb9c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b27b43f-4d39-4cc9-99de-bf7fc44dce22",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1b9efa2e-da53-4e67-8b06-32588fcecbed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b27b43f-4d39-4cc9-99de-bf7fc44dce22",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "c74cf12d-cef4-43fa-a955-df378709936a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "454977b4-ec3c-4c53-86fa-b374e30837d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74cf12d-cef4-43fa-a955-df378709936a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9aee46f-81f8-4c87-a9c2-6be751543f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74cf12d-cef4-43fa-a955-df378709936a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "5f1a8929-0a2b-402c-90ec-c33818de00a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74cf12d-cef4-43fa-a955-df378709936a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "88cf9d88-e2f6-49fb-8f8c-763f53294f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "1a0432bb-67f9-4fc1-88eb-b207a7926014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cf9d88-e2f6-49fb-8f8c-763f53294f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da24011b-2a13-4b9e-81e6-52abb4d26101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cf9d88-e2f6-49fb-8f8c-763f53294f85",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "3a08a9f4-6de3-40d1-b856-b9009dd9fef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cf9d88-e2f6-49fb-8f8c-763f53294f85",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f61a72fc-1307-47bb-b025-de9a90cec680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "39f1a5c6-e757-4849-b863-08904281466c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f61a72fc-1307-47bb-b025-de9a90cec680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26528ae-dff6-4000-9fc8-447be0f67a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f61a72fc-1307-47bb-b025-de9a90cec680",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "5036bae7-2aa1-4b72-879a-d3ef917018c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f61a72fc-1307-47bb-b025-de9a90cec680",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "1b26a1ff-b72b-487c-8e38-946e62d8522b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "287ef054-d1ad-408e-924d-960c7993833c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b26a1ff-b72b-487c-8e38-946e62d8522b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73580ef-f4c4-4816-8998-4668c884cddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b26a1ff-b72b-487c-8e38-946e62d8522b",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ad72abe4-2e85-4d56-9b73-a349f936ba4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b26a1ff-b72b-487c-8e38-946e62d8522b",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "673aabe6-6298-492e-aed8-a822e616bceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d719c987-5eec-4789-9227-9129666b418d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673aabe6-6298-492e-aed8-a822e616bceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96eca6e5-d2ab-4f2d-9f4f-5bd86211e55a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673aabe6-6298-492e-aed8-a822e616bceb",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "77b50aa0-a674-4ae2-8244-979cac28951a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673aabe6-6298-492e-aed8-a822e616bceb",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "5db7a01b-640f-499c-8418-761debce5fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "34cf79b6-d3c2-47a4-921e-0142276dbae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db7a01b-640f-499c-8418-761debce5fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b5926a9-94bc-4583-8b2a-e51dbfba9a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db7a01b-640f-499c-8418-761debce5fd4",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ff6fd16d-7d48-4596-a38f-8ae3903e1f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db7a01b-640f-499c-8418-761debce5fd4",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "63440345-199e-408a-a158-d7514002dbbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "54d27b10-e7a2-464b-98ca-ad7a364093ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63440345-199e-408a-a158-d7514002dbbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3712f022-0941-448c-8ff6-7a8e180bfa84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63440345-199e-408a-a158-d7514002dbbe",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ff32eba2-de2a-400d-b44e-3917e89a7ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63440345-199e-408a-a158-d7514002dbbe",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a4ff8ecd-9fcd-42cf-ab03-d59113f2ac55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "0c1c7f4f-730b-4225-957e-e35b3acf542b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ff8ecd-9fcd-42cf-ab03-d59113f2ac55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87a9e8b3-487d-47fb-9040-94e62dd8742b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ff8ecd-9fcd-42cf-ab03-d59113f2ac55",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "ba28d4e7-feb8-46a2-8176-9376621586b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ff8ecd-9fcd-42cf-ab03-d59113f2ac55",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3ef5b7cf-1d9e-4941-8141-41762a77fd2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "86da1e03-7a43-4c1f-9d72-8c8d0f747098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ef5b7cf-1d9e-4941-8141-41762a77fd2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ab18ed4-893d-42cb-8d75-9db4ecf299ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ef5b7cf-1d9e-4941-8141-41762a77fd2a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "d20c290e-f535-4591-88b9-eefdf5844f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ef5b7cf-1d9e-4941-8141-41762a77fd2a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "65bc058c-2ce8-4213-84e4-384e084dab93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "8b8b2517-b2eb-4b88-b149-7639c6a9f751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65bc058c-2ce8-4213-84e4-384e084dab93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "341e43ab-8e49-43d7-9648-db107ec36d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bc058c-2ce8-4213-84e4-384e084dab93",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "7193e880-036c-46fd-a389-ac7e2cfa90d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bc058c-2ce8-4213-84e4-384e084dab93",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "119578ed-168b-499b-beaf-ce05c9359d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "54a72aac-89d5-486b-be6a-5214f4824a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "119578ed-168b-499b-beaf-ce05c9359d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1498347f-26e7-4148-98cc-89e892395ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "119578ed-168b-499b-beaf-ce05c9359d56",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "2709f2b0-245e-4603-9f9b-76e435fb57e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "119578ed-168b-499b-beaf-ce05c9359d56",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "4eb609ac-3bf9-4fd3-b543-e5c9dd328140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "7e4078db-cdc3-4287-9846-a9bdbffac312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb609ac-3bf9-4fd3-b543-e5c9dd328140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bb4cd33-d57b-4988-9b3d-f7398fccf0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb609ac-3bf9-4fd3-b543-e5c9dd328140",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "c1a7973a-812b-46b1-bc20-f86606ebe358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb609ac-3bf9-4fd3-b543-e5c9dd328140",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "0c882f7f-39d5-4929-a5d0-589a71509a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "8c6797c2-f9f3-45be-8326-34d1cc2cb4af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c882f7f-39d5-4929-a5d0-589a71509a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4e5670-735b-4f16-899f-13c9941f304e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c882f7f-39d5-4929-a5d0-589a71509a8e",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1cc6e1c5-2904-44a6-b1ef-5473e9c71ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c882f7f-39d5-4929-a5d0-589a71509a8e",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3fc55300-0abf-4145-b6a4-285daba14013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d1433894-9ae8-4ee6-8123-9ad91b1650f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc55300-0abf-4145-b6a4-285daba14013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebfa7a49-bf5f-4add-804c-e88a17fe475c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc55300-0abf-4145-b6a4-285daba14013",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "e80a1771-5d03-4911-96fc-a3e12de0d260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc55300-0abf-4145-b6a4-285daba14013",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "8372c516-ef44-45d6-ad67-0e2c59932599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "4c200a95-8475-4daa-8ff5-0a2a46119be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8372c516-ef44-45d6-ad67-0e2c59932599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "869bde13-0002-490e-8be0-bd43266edc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8372c516-ef44-45d6-ad67-0e2c59932599",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "57ac1ef2-144d-4b4a-839d-530b8d89873b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8372c516-ef44-45d6-ad67-0e2c59932599",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "98995c16-99f1-4759-aecf-dd762527a960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "4b00720a-6cee-4e12-96db-91748459e5e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98995c16-99f1-4759-aecf-dd762527a960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "433ae59b-b092-4ab6-8e1b-b639b8272bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98995c16-99f1-4759-aecf-dd762527a960",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "059d41d0-980f-4e4a-8177-1121663c64c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98995c16-99f1-4759-aecf-dd762527a960",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "e3b6ae75-ab31-4fe3-b72d-889661430fe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c23d2a8f-8d2b-49a5-98d2-85da6d38f3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b6ae75-ab31-4fe3-b72d-889661430fe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afb03d8d-cf7e-4d6f-81a7-19320fac0358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b6ae75-ab31-4fe3-b72d-889661430fe0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4c7d7275-0eab-414c-88b3-8d9835d01dd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b6ae75-ab31-4fe3-b72d-889661430fe0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2ba90622-b735-42cc-bddf-10d8a93ef903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "397c5c2f-cf4f-4cf1-87db-308c4a4ac413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba90622-b735-42cc-bddf-10d8a93ef903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5717b97-5725-474a-8e68-ff2d0e393a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba90622-b735-42cc-bddf-10d8a93ef903",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "cbedc81b-6c1e-4fcc-8d7e-08877a22df9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba90622-b735-42cc-bddf-10d8a93ef903",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f5102042-11a8-4bf6-8f64-bdf31a8a9dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "74bba086-d077-407c-b036-b7db013ebff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5102042-11a8-4bf6-8f64-bdf31a8a9dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30d78e7-0e55-40ed-8077-f9b73cdff08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5102042-11a8-4bf6-8f64-bdf31a8a9dd4",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "d2a312ce-94c4-4f40-bf25-15fcd42aec47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5102042-11a8-4bf6-8f64-bdf31a8a9dd4",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "d8581ad8-b068-4371-a206-36c5753c886e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "9ed7fb89-5e0b-4f97-a022-a8d4203aee99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8581ad8-b068-4371-a206-36c5753c886e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475501cd-19ce-46a0-a521-98ff4afef1ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8581ad8-b068-4371-a206-36c5753c886e",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "3014e451-83c8-4810-8930-f021cd93e1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8581ad8-b068-4371-a206-36c5753c886e",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "55956b4e-de7e-4102-9f42-1a4c3223e226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "b26cc2bc-38f7-49c4-80f1-39e3aaf080a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55956b4e-de7e-4102-9f42-1a4c3223e226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "292794c7-87be-4c27-a8e5-2d1fc1e0c554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55956b4e-de7e-4102-9f42-1a4c3223e226",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "f3066920-0f60-472e-8b3d-4ae11155c3cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55956b4e-de7e-4102-9f42-1a4c3223e226",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "43496582-a635-4683-9a94-ec998fe1738d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c2b3ed4c-1019-4780-a904-0a15ef83c40a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43496582-a635-4683-9a94-ec998fe1738d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fed251b-ee0d-4c0d-b502-41dbae79c122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43496582-a635-4683-9a94-ec998fe1738d",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "81588722-ed16-484c-8b84-b4a47df13dd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43496582-a635-4683-9a94-ec998fe1738d",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "fc754427-dfcf-49fc-ad23-3c2d327475e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a00c83b9-fd9c-46c5-9941-2031d2217d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc754427-dfcf-49fc-ad23-3c2d327475e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d1f6fa-4636-44c2-ac5d-9129b231859a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc754427-dfcf-49fc-ad23-3c2d327475e0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "68b48d15-12c6-41e4-8e0d-eeebff8c975d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc754427-dfcf-49fc-ad23-3c2d327475e0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3997c21a-6a9c-4a1f-ad7d-13fff0b3d2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "b97e2455-3836-4f21-bef2-03bf9b6c893d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3997c21a-6a9c-4a1f-ad7d-13fff0b3d2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3ed9952-1d9a-4ffc-ba76-05ecfaff5465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3997c21a-6a9c-4a1f-ad7d-13fff0b3d2d0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "2b134950-ef98-4409-ae93-e1fce52e0bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3997c21a-6a9c-4a1f-ad7d-13fff0b3d2d0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "b8a84e82-6aab-45c9-9a4c-b8ec0b627bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "84bd0106-525c-4652-acb4-8b4d0cf4a661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a84e82-6aab-45c9-9a4c-b8ec0b627bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dab6eeeb-2655-4cf1-8ed2-35ed80838ea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a84e82-6aab-45c9-9a4c-b8ec0b627bc8",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "28a57e0e-2899-4e46-ad23-377cbff261d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a84e82-6aab-45c9-9a4c-b8ec0b627bc8",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "5861bde8-0e50-402a-b7c3-d4e3c323fd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c6906d78-46d7-464a-b24f-cd89247e5abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5861bde8-0e50-402a-b7c3-d4e3c323fd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45506019-3c29-4be6-ae6e-2c27b522db7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5861bde8-0e50-402a-b7c3-d4e3c323fd2c",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "01a77c76-e9eb-4bf1-9cc7-895c1c92babc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5861bde8-0e50-402a-b7c3-d4e3c323fd2c",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "bc9fef43-c7de-4285-a865-ac8d30c2a535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "2a00bcf5-c155-432f-b87a-051e9d594c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc9fef43-c7de-4285-a865-ac8d30c2a535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f86e0fe-ecca-43b2-b3de-55d358912524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9fef43-c7de-4285-a865-ac8d30c2a535",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "172d11ba-f892-41a3-995b-4c4878282cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9fef43-c7de-4285-a865-ac8d30c2a535",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f6943794-45c2-49cc-a1ed-e26b9322b75a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "03456417-e51d-4929-a556-525583a80648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6943794-45c2-49cc-a1ed-e26b9322b75a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "460b7803-fc53-479d-ae30-ca316a9274d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6943794-45c2-49cc-a1ed-e26b9322b75a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b0d58b0b-45e6-457a-9176-5ce16ce50bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6943794-45c2-49cc-a1ed-e26b9322b75a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "1ff709e9-21aa-4d03-b840-3b70e96237af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "8e1f7547-3ff1-46d3-95d3-a88946f04471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ff709e9-21aa-4d03-b840-3b70e96237af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a330d76-c7e2-4bfe-9aa9-9130e4f680b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff709e9-21aa-4d03-b840-3b70e96237af",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "e18802a7-1648-451e-890b-7d2c486d0ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff709e9-21aa-4d03-b840-3b70e96237af",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "c492b90d-dd01-4afc-b41f-51e9644a147f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "2a08e5ab-d890-460a-b174-7b5c4f3cfa77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c492b90d-dd01-4afc-b41f-51e9644a147f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7387ac71-50c1-4353-ac22-c5825296f159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c492b90d-dd01-4afc-b41f-51e9644a147f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "af2380ae-8c44-4a65-be65-50bfb03188ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c492b90d-dd01-4afc-b41f-51e9644a147f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "c7ad789f-e1c0-42cc-bb17-0c4034d6af41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "6adeb8c1-6909-4b84-9d5d-ff2302344ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ad789f-e1c0-42cc-bb17-0c4034d6af41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b5286e9-d24f-4fb3-bca4-cac8c3cc0479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ad789f-e1c0-42cc-bb17-0c4034d6af41",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "42c39538-9c50-480d-9d0e-4cc1c7b1c6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ad789f-e1c0-42cc-bb17-0c4034d6af41",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "c20a69ad-97c8-4f76-98d1-1d37302f35cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "75c610f8-e8b7-494c-ad26-51c2a1a427f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c20a69ad-97c8-4f76-98d1-1d37302f35cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d597aebf-5ca3-456c-b6ac-37672e49cca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20a69ad-97c8-4f76-98d1-1d37302f35cb",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "a3caa37e-e85f-4c3b-9207-31758db12b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20a69ad-97c8-4f76-98d1-1d37302f35cb",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f7d76d7a-e820-4283-81d6-89f41bc2adb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "53b8a250-2be5-4960-9c23-ab514da2c030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7d76d7a-e820-4283-81d6-89f41bc2adb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7aca47-5a56-4f4f-8b66-e4e58f4a02d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d76d7a-e820-4283-81d6-89f41bc2adb1",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b830798d-5715-479b-9669-eb6c00f63cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d76d7a-e820-4283-81d6-89f41bc2adb1",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "b17ce6bc-33bc-4009-a8ad-73a78695f7f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "3f9e05dd-4135-482f-816d-fb40bcdf31c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b17ce6bc-33bc-4009-a8ad-73a78695f7f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1421188b-f557-4cc9-a796-0389d9559415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17ce6bc-33bc-4009-a8ad-73a78695f7f0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "8db5fe53-fb1f-400a-88a0-4fc7eb72f01f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17ce6bc-33bc-4009-a8ad-73a78695f7f0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "b306f638-ad6b-4a48-8a7d-cb6432f8f254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "31a1c35c-f5a3-414d-b42f-6b48bf34dd37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b306f638-ad6b-4a48-8a7d-cb6432f8f254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b679e4d6-074c-48e2-b5be-368f8c9d2a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b306f638-ad6b-4a48-8a7d-cb6432f8f254",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1a6bf7e2-b98a-4cbe-a79f-42503cb9a024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b306f638-ad6b-4a48-8a7d-cb6432f8f254",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "8de95122-d11b-4379-83e5-6c992d8a434a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5264750c-2dca-4971-a0b9-02dce84abb17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de95122-d11b-4379-83e5-6c992d8a434a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "979caf2b-1dc8-4025-ac97-dda4489ee7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de95122-d11b-4379-83e5-6c992d8a434a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "a5ecf796-23dc-4593-8309-2019031ae213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de95122-d11b-4379-83e5-6c992d8a434a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "491bdbea-8728-4c7d-a01f-66bd64f50ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "c79a0dc1-5d5f-4ea7-a702-c2d1c6d845c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "491bdbea-8728-4c7d-a01f-66bd64f50ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1106e520-fc1b-4718-a57e-0ccdab1ee7de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "491bdbea-8728-4c7d-a01f-66bd64f50ba4",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "8e5c27bc-0172-4481-8af8-22903294f316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "491bdbea-8728-4c7d-a01f-66bd64f50ba4",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "5011642d-2c86-45a9-844b-8b561f96b2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "13760bec-8beb-44d1-b0d5-3645b045ef66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5011642d-2c86-45a9-844b-8b561f96b2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "462d677f-7904-42af-b24a-de1e5b04473b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5011642d-2c86-45a9-844b-8b561f96b2d9",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "14b10fde-b75a-4942-a97d-87c7c2d4278f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5011642d-2c86-45a9-844b-8b561f96b2d9",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "9690a3ee-0b7b-465e-9b68-ecbd07b2d09a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "30d426ed-cd39-4630-ae60-954efd9e8ded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9690a3ee-0b7b-465e-9b68-ecbd07b2d09a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3894406a-5b37-4a15-91ff-035a749bfcd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9690a3ee-0b7b-465e-9b68-ecbd07b2d09a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "3268a293-8ad3-4d52-b0ed-cdafae352378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9690a3ee-0b7b-465e-9b68-ecbd07b2d09a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f833e38e-ccfb-4936-afe1-0864bd0edfda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a93171a6-9391-4382-983a-d187085cdafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f833e38e-ccfb-4936-afe1-0864bd0edfda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a758c7d-1572-47b7-85fe-1bff6d214f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f833e38e-ccfb-4936-afe1-0864bd0edfda",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "c97e4a2a-407e-40b9-abd3-8ba0e878dc57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f833e38e-ccfb-4936-afe1-0864bd0edfda",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "163948d3-8b71-4c1e-8e54-0c7e8c341c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "17c1773a-28cb-48d1-b59f-692f0464cb22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163948d3-8b71-4c1e-8e54-0c7e8c341c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119c8248-3998-4e3b-b4a0-a0edf5a1b1ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163948d3-8b71-4c1e-8e54-0c7e8c341c96",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "94e63888-d9f5-4189-ada2-afc2b91f680f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163948d3-8b71-4c1e-8e54-0c7e8c341c96",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "27cd7d20-282e-4838-9376-6b92d9413094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "183d3f02-6636-4665-b80e-d3f64c49e9df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27cd7d20-282e-4838-9376-6b92d9413094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30527b40-e8c6-4e40-a770-6feeed5133af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27cd7d20-282e-4838-9376-6b92d9413094",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "8a4b4e6d-a06e-402d-9a20-867864de018d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27cd7d20-282e-4838-9376-6b92d9413094",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "47dee710-e724-43d0-94c7-c4112d44941c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "e1fff069-e00f-4212-a6ac-ee1b7da22529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47dee710-e724-43d0-94c7-c4112d44941c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27180852-4901-4971-a1be-510ff4ea6a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47dee710-e724-43d0-94c7-c4112d44941c",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b0d0ac3d-ebf2-4b64-b0cb-0e08599b10a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47dee710-e724-43d0-94c7-c4112d44941c",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "39c3ba3e-b74f-4dc9-8dd7-ac1ee8a72115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "41c92baa-a11c-4778-a208-eb06854e98c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c3ba3e-b74f-4dc9-8dd7-ac1ee8a72115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32e0bee0-48f0-4307-8479-59618e80db4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c3ba3e-b74f-4dc9-8dd7-ac1ee8a72115",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "0842edf8-57e6-467c-813c-a73855a81196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c3ba3e-b74f-4dc9-8dd7-ac1ee8a72115",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "3cc39335-ada0-4ee2-84ab-ddecba1914ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "75f14e5e-8646-41e1-b5ad-acdbef7edc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc39335-ada0-4ee2-84ab-ddecba1914ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5397bae-d5d6-497e-a973-acc782c55f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc39335-ada0-4ee2-84ab-ddecba1914ae",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "16ed2d70-d20e-4242-9921-f3dec7f39779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc39335-ada0-4ee2-84ab-ddecba1914ae",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "70f80803-41f7-4127-b37c-479fdcaf8795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "8edb1fb2-c1ed-4777-b9ec-334ec417d3bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70f80803-41f7-4127-b37c-479fdcaf8795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "309f8807-c037-49c9-bc9e-7b7b56b18955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70f80803-41f7-4127-b37c-479fdcaf8795",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "8e21bb75-3f90-49f4-b43a-3c8a853e4630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70f80803-41f7-4127-b37c-479fdcaf8795",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "85948732-3b2a-49d5-991e-ed40f0691e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "de68c47e-51de-40e9-82de-ac3ccbbd4a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85948732-3b2a-49d5-991e-ed40f0691e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccba99fa-1f74-4b5b-9416-727a9a175140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85948732-3b2a-49d5-991e-ed40f0691e76",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "e32624a7-0376-4f15-acd6-f932ee517ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85948732-3b2a-49d5-991e-ed40f0691e76",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "d55e9f4f-85f8-4b88-80d7-4ec12e33e63e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5e62fb1c-c9fd-404f-9347-52a0fe1198b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d55e9f4f-85f8-4b88-80d7-4ec12e33e63e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316629e0-8ae9-4816-a28a-793dafc0871c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55e9f4f-85f8-4b88-80d7-4ec12e33e63e",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "62363783-febe-4cbd-883f-dfc570c3a6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55e9f4f-85f8-4b88-80d7-4ec12e33e63e",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "8ba678d4-69ee-4e20-a340-8cc1d44fc215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "8f2dbe93-fd2f-4705-acf0-f1292a71e70a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba678d4-69ee-4e20-a340-8cc1d44fc215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547d3d8e-94fd-4d29-9ad1-6b9851a66f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba678d4-69ee-4e20-a340-8cc1d44fc215",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9fa17a18-b42e-47df-bade-b79ea9a69238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba678d4-69ee-4e20-a340-8cc1d44fc215",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "9a9ec3ae-8cb3-4c59-ae12-2abe6a1cce01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5ea0a89c-ca0f-4aae-9026-6e3f75821ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9ec3ae-8cb3-4c59-ae12-2abe6a1cce01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2593d51d-4c83-4745-a649-a030f3340af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9ec3ae-8cb3-4c59-ae12-2abe6a1cce01",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "d72c39fd-6dba-48c1-887b-0163fb52a958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9ec3ae-8cb3-4c59-ae12-2abe6a1cce01",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "cac28235-a5f8-459a-9821-edcadc58464f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "bb596ef4-edd3-4d30-beab-acd18953a5dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cac28235-a5f8-459a-9821-edcadc58464f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ebba395-78e7-4834-8d9c-c917f12c377b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac28235-a5f8-459a-9821-edcadc58464f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "c5527c35-8ab3-426d-b247-80c0f658eaa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac28235-a5f8-459a-9821-edcadc58464f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "25fd1a9b-d194-4d31-b773-880eac1f49ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d91a5d3c-b680-4df7-ad48-b718b16ca330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25fd1a9b-d194-4d31-b773-880eac1f49ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442b5d66-45aa-4d59-8182-a0f8904a5705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25fd1a9b-d194-4d31-b773-880eac1f49ba",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "917b35ff-b8a7-4c09-a394-c113377bb6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25fd1a9b-d194-4d31-b773-880eac1f49ba",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "f992881a-073f-48da-94ea-15f33118ec69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "49035e84-a27c-4ee3-b067-f924d10626cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f992881a-073f-48da-94ea-15f33118ec69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b53187f-0a03-4e6d-8b6a-54e2183ca5c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f992881a-073f-48da-94ea-15f33118ec69",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "a9722129-173c-49a4-8cf8-9c1ab089dd9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f992881a-073f-48da-94ea-15f33118ec69",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "8410674f-feb3-469d-8172-5a125d85653f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "15942d2d-8352-4a02-8ed1-ca0c04953a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8410674f-feb3-469d-8172-5a125d85653f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57554d4-ea48-49ac-9707-b2109f0aa4d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8410674f-feb3-469d-8172-5a125d85653f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "1d67ae7a-039f-4c28-9392-5339ce41004d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8410674f-feb3-469d-8172-5a125d85653f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "e824c616-c515-4510-b19a-44dd9137da22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "919b342a-4c02-4693-b973-de728ba1e8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e824c616-c515-4510-b19a-44dd9137da22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a8bca2-63cd-4ab3-a839-b6adad2a9e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e824c616-c515-4510-b19a-44dd9137da22",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "0f0d392e-55bb-432c-bf72-051954657232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e824c616-c515-4510-b19a-44dd9137da22",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "6f3485cb-27ec-465e-bccb-ce46fd00bd87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d34dc1ea-d3db-442a-9c94-3b2bb6418920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3485cb-27ec-465e-bccb-ce46fd00bd87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b825332d-f841-4528-acbf-6e840c273f8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3485cb-27ec-465e-bccb-ce46fd00bd87",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "f0098dd0-b386-4c7e-a3eb-6f47944b5523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3485cb-27ec-465e-bccb-ce46fd00bd87",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a9392770-a547-4730-95d0-dd8ff4925c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "3e489c1e-c0d7-4f52-8ad8-73d42fe6add6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9392770-a547-4730-95d0-dd8ff4925c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5f9266e-d058-4b38-bee7-61f3e3c97e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9392770-a547-4730-95d0-dd8ff4925c97",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "6532e114-9645-4f62-bb8e-c551e634ed5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9392770-a547-4730-95d0-dd8ff4925c97",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "541eba7f-919c-44a8-a3d8-32b82a63e2e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "5bdd6b88-dfa1-4943-b66c-b5fe12701362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "541eba7f-919c-44a8-a3d8-32b82a63e2e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed6167cc-b9f9-4873-8dfc-2ffb8ca99194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "541eba7f-919c-44a8-a3d8-32b82a63e2e8",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "da74bb70-ef28-44ef-be94-3012c5551ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "541eba7f-919c-44a8-a3d8-32b82a63e2e8",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "01cabece-1816-4b33-ae00-8844f880c2ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "59b04d95-5fba-4777-b4ae-63aec9a46f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01cabece-1816-4b33-ae00-8844f880c2ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a37784-3565-47ac-9597-ec146a942f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cabece-1816-4b33-ae00-8844f880c2ff",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "3815a80c-b250-444d-a920-6a17a03ebb14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cabece-1816-4b33-ae00-8844f880c2ff",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "145ec710-1548-420d-a69e-133e3fb28a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "7288d5fc-e18b-44e3-b267-ebe74985ffe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "145ec710-1548-420d-a69e-133e3fb28a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8065150b-3dc6-474d-8dfd-396f2d2c1b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145ec710-1548-420d-a69e-133e3fb28a4f",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "354608f8-dfef-44dc-ab6d-7d6379bbbd22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145ec710-1548-420d-a69e-133e3fb28a4f",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "1856639f-d6c3-42a2-9f7d-81df9bf10fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "a9a999ea-b4e7-43fc-a6d8-455879baba29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1856639f-d6c3-42a2-9f7d-81df9bf10fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f87d98-754d-41d0-9c91-5e02adab3750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1856639f-d6c3-42a2-9f7d-81df9bf10fbe",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "986731f4-8865-44b5-8282-03f3b1410a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1856639f-d6c3-42a2-9f7d-81df9bf10fbe",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "4dd70f1d-0a0f-4eb9-8671-1cf4a3b89948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "0968235e-73c3-418c-b150-3c0ec2180a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd70f1d-0a0f-4eb9-8671-1cf4a3b89948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41007694-fe30-4f54-bcfe-237d58b2d663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd70f1d-0a0f-4eb9-8671-1cf4a3b89948",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "c2bced03-1715-42ec-b45e-2bd8418c52d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd70f1d-0a0f-4eb9-8671-1cf4a3b89948",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2a6cee55-a9fc-4779-832f-03270c0c4a15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "32c76bfa-c15a-4914-bc0d-afd615a2ff8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a6cee55-a9fc-4779-832f-03270c0c4a15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604de945-e35f-41da-8ec5-5473789ac92a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a6cee55-a9fc-4779-832f-03270c0c4a15",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "8dc3ebe4-1b4d-4167-9dd2-74e9797fd2a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a6cee55-a9fc-4779-832f-03270c0c4a15",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "c74caa3c-1fe5-4d91-bd54-2c3ff6d253a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "20c16e9c-ab50-4d79-8aa5-3524aaec557e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74caa3c-1fe5-4d91-bd54-2c3ff6d253a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6960249-1002-49b4-b7cd-f76608c63744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74caa3c-1fe5-4d91-bd54-2c3ff6d253a7",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "093214f0-d9df-48d3-b96e-fb5811d69e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74caa3c-1fe5-4d91-bd54-2c3ff6d253a7",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "73e0bc00-9306-4a1a-ad42-8b819115acd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d36256d4-dc47-419c-9559-dfb9ef102839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e0bc00-9306-4a1a-ad42-8b819115acd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "783ab7dd-c5f7-43e5-aa16-8224486a9413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e0bc00-9306-4a1a-ad42-8b819115acd2",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "952b272b-ea23-4dc9-be0e-a05883e18f76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e0bc00-9306-4a1a-ad42-8b819115acd2",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "e0f0d30b-e342-4b2e-b1f5-fd15c471d5d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "2bd1830e-423b-457f-8cbd-689d657c3223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f0d30b-e342-4b2e-b1f5-fd15c471d5d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b104112-b855-457c-b4af-d5f62dbd8787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f0d30b-e342-4b2e-b1f5-fd15c471d5d0",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "c3f06b02-e4de-4442-9ad8-ae58384d11c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f0d30b-e342-4b2e-b1f5-fd15c471d5d0",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "edd6052e-d079-460c-ac27-e524206247f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "93563c2f-f877-493a-a922-0ba4bb07d392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd6052e-d079-460c-ac27-e524206247f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced82cc7-7db9-4aba-b6bd-b22f5f13899e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd6052e-d079-460c-ac27-e524206247f2",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4fe17edf-f642-44f5-9e86-4a8fc44ddad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd6052e-d079-460c-ac27-e524206247f2",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2b8409f3-aadf-44dd-91e3-ff049a9b48a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "d199917f-5fe1-43dd-acf0-dc1f56d98130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8409f3-aadf-44dd-91e3-ff049a9b48a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ff2298-0cea-4409-ac47-8184de76bd92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8409f3-aadf-44dd-91e3-ff049a9b48a7",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "9376a56a-987f-4176-84ec-e8d28f85cfa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8409f3-aadf-44dd-91e3-ff049a9b48a7",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "9b625d94-e0dc-4b46-8bcb-a97b65f8d1b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "7595e8e6-1268-45a7-8608-1f0300d344d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b625d94-e0dc-4b46-8bcb-a97b65f8d1b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7e9302-9d90-4680-8501-eb30b641fdb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b625d94-e0dc-4b46-8bcb-a97b65f8d1b2",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "f8968efe-c439-4478-a9e5-f4217ca2038a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b625d94-e0dc-4b46-8bcb-a97b65f8d1b2",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "a87d7310-5ae5-465f-bca0-4cc6e5ca320c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "ef3a93a8-9191-46b6-94b5-dbe41c4c0716",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87d7310-5ae5-465f-bca0-4cc6e5ca320c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8cce64-9013-4c7a-b63a-359acbf3a3dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87d7310-5ae5-465f-bca0-4cc6e5ca320c",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "986761df-df2a-4ed6-ab34-e0d65c4c07ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87d7310-5ae5-465f-bca0-4cc6e5ca320c",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "2e8615b2-f512-4618-9f79-f7dd13fd0f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "50234037-78b7-47df-8f30-26591c553875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8615b2-f512-4618-9f79-f7dd13fd0f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc086d0-9cae-48a5-afce-76e0cb2702a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8615b2-f512-4618-9f79-f7dd13fd0f3a",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "8f8f6ce9-3f19-4663-bcf8-5445bbb3a952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8615b2-f512-4618-9f79-f7dd13fd0f3a",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "082a8f16-065d-482a-a82f-82e5fd9bc6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "4f961255-b53c-474e-bd5d-c56f21a9a004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082a8f16-065d-482a-a82f-82e5fd9bc6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16bb96b-a358-4f60-8067-55c28fd0f5a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082a8f16-065d-482a-a82f-82e5fd9bc6a1",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "e6a4847b-1636-42e6-bed4-54009a742bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082a8f16-065d-482a-a82f-82e5fd9bc6a1",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "13fc98df-5d75-4cdc-8c76-b3b65ccb1586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "b3a8a0cf-8527-47bc-9c8a-cb2ee97edc7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13fc98df-5d75-4cdc-8c76-b3b65ccb1586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8c2d3d-0a7a-46f0-98fb-059c81ccbe31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fc98df-5d75-4cdc-8c76-b3b65ccb1586",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "b4c27d62-d657-4e8a-a3eb-e57ca2da4cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fc98df-5d75-4cdc-8c76-b3b65ccb1586",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        },
        {
            "id": "da024ff6-906e-4383-a37d-b7c2358e7bb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "compositeImage": {
                "id": "f32758b8-347f-4b71-9934-20dced5d0f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da024ff6-906e-4383-a37d-b7c2358e7bb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda2537d-591f-42b7-91d5-e7cac407e670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da024ff6-906e-4383-a37d-b7c2358e7bb4",
                    "LayerId": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a"
                },
                {
                    "id": "4d876772-1397-4806-8ed5-0d3a9acc42fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da024ff6-906e-4383-a37d-b7c2358e7bb4",
                    "LayerId": "419c54e7-0177-44d6-b3ff-3dcdf31f433d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "c0cc44b7-8212-4a2f-a4bd-051c128d8c7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "419c54e7-0177-44d6-b3ff-3dcdf31f433d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d03e2cf2-5efa-4dc0-82e2-d6f793d0fa5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 1,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}