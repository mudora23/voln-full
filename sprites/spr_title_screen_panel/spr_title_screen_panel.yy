{
    "id": "d94e16d4-4b58-455e-b66d-1039d6cc5f0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title_screen_panel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 755,
    "bbox_left": 68,
    "bbox_right": 908,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea4eb560-9eea-4380-a439-8aeca6f59884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d94e16d4-4b58-455e-b66d-1039d6cc5f0b",
            "compositeImage": {
                "id": "9babdf48-fd25-43ca-b5b8-8a9b79b9f519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea4eb560-9eea-4380-a439-8aeca6f59884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70706335-48e4-4b8f-acb2-350cdf16303c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea4eb560-9eea-4380-a439-8aeca6f59884",
                    "LayerId": "094584ab-d5fe-4d92-be26-9b84863ee2a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 756,
    "layers": [
        {
            "id": "094584ab-d5fe-4d92-be26-9b84863ee2a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d94e16d4-4b58-455e-b66d-1039d6cc5f0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 977,
    "xorig": 488,
    "yorig": 755
}