{
    "id": "243cf1c7-cf78-4175-a70f-12f4209671dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MARKER_RED",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cba23fd-009a-4f94-b874-e88016e711d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243cf1c7-cf78-4175-a70f-12f4209671dd",
            "compositeImage": {
                "id": "508aa17b-96b1-4615-857e-741ae7cf0a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cba23fd-009a-4f94-b874-e88016e711d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c70b69-1c0d-4ebb-84c3-4c35cc330394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cba23fd-009a-4f94-b874-e88016e711d1",
                    "LayerId": "db919b61-9434-4e96-a01a-8fddc00847f1"
                }
            ]
        },
        {
            "id": "be94002a-2f14-493c-b000-0166e5969df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243cf1c7-cf78-4175-a70f-12f4209671dd",
            "compositeImage": {
                "id": "c6658aed-e108-4865-852b-9bab99cb1168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be94002a-2f14-493c-b000-0166e5969df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66bcfc7b-53d0-42e1-811f-8fd7d8b167a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be94002a-2f14-493c-b000-0166e5969df7",
                    "LayerId": "db919b61-9434-4e96-a01a-8fddc00847f1"
                }
            ]
        },
        {
            "id": "928c2ad5-c6c9-4263-9b4c-36e037699ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243cf1c7-cf78-4175-a70f-12f4209671dd",
            "compositeImage": {
                "id": "72a275bd-1e9f-4a52-8d32-8b1359c79530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928c2ad5-c6c9-4263-9b4c-36e037699ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe6cbb4-4078-4fe5-8704-c11463d8b8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928c2ad5-c6c9-4263-9b4c-36e037699ea2",
                    "LayerId": "db919b61-9434-4e96-a01a-8fddc00847f1"
                }
            ]
        },
        {
            "id": "c5489247-379c-49ec-8dc2-c9b5ea4da07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243cf1c7-cf78-4175-a70f-12f4209671dd",
            "compositeImage": {
                "id": "7eda4930-af27-4ad1-97f3-09350c243b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5489247-379c-49ec-8dc2-c9b5ea4da07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8079170-ba89-4030-8809-e230fd4fbc78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5489247-379c-49ec-8dc2-c9b5ea4da07d",
                    "LayerId": "db919b61-9434-4e96-a01a-8fddc00847f1"
                }
            ]
        },
        {
            "id": "51c27a8a-1872-4718-bb49-fecd40e8f353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243cf1c7-cf78-4175-a70f-12f4209671dd",
            "compositeImage": {
                "id": "5189638c-4da4-4260-b7f2-148f37c6a30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51c27a8a-1872-4718-bb49-fecd40e8f353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f21e707c-a354-4a48-846c-82bb3bebb8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51c27a8a-1872-4718-bb49-fecd40e8f353",
                    "LayerId": "db919b61-9434-4e96-a01a-8fddc00847f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "db919b61-9434-4e96-a01a-8fddc00847f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243cf1c7-cf78-4175-a70f-12f4209671dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 0
}