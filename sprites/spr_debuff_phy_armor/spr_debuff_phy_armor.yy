{
    "id": "cd86aae8-5471-4c76-916e-717a660563e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_phy_armor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 2,
    "bbox_right": 48,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2442a849-98a6-46ea-baec-2a273c481ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd86aae8-5471-4c76-916e-717a660563e7",
            "compositeImage": {
                "id": "9abdf5fb-54a9-4894-8dbb-f6f7fca12d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2442a849-98a6-46ea-baec-2a273c481ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5357e77c-a91c-4279-b69a-4c1b6a1f35de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2442a849-98a6-46ea-baec-2a273c481ff7",
                    "LayerId": "f2a554f3-8381-4722-82bd-8b47a956c744"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "f2a554f3-8381-4722-82bd-8b47a956c744",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd86aae8-5471-4c76-916e-717a660563e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}