{
    "id": "b1a57c65-7409-4da1-a262-904c2b481265",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 753,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bee72ff-5e21-481e-8f62-7538a4595cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1a57c65-7409-4da1-a262-904c2b481265",
            "compositeImage": {
                "id": "4eda4f9a-5c9b-4313-a9e4-1b5995a383cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bee72ff-5e21-481e-8f62-7538a4595cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb75273-5a92-4ccb-8f89-da9971a249a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bee72ff-5e21-481e-8f62-7538a4595cda",
                    "LayerId": "31e092d5-6b43-42e2-b6e0-cfb0e7c120ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 754,
    "layers": [
        {
            "id": "31e092d5-6b43-42e2-b6e0-cfb0e7c120ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1a57c65-7409-4da1-a262-904c2b481265",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}