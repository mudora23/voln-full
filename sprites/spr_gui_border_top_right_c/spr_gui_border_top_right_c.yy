{
    "id": "a34bd62f-96d7-466f-a985-abd67554f704",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_right_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 142,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbb91e46-f418-4a08-b8ae-080a9408d6fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a34bd62f-96d7-466f-a985-abd67554f704",
            "compositeImage": {
                "id": "c69b0019-af4d-4eb3-b4f3-43c82b051a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbb91e46-f418-4a08-b8ae-080a9408d6fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6d59cc-cff2-4e98-a8d6-b6ada9961345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb91e46-f418-4a08-b8ae-080a9408d6fa",
                    "LayerId": "77cc0489-d4d1-4683-900a-2839ef795082"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "77cc0489-d4d1-4683-900a-2839ef795082",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a34bd62f-96d7-466f-a985-abd67554f704",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 143,
    "xorig": 118,
    "yorig": 24
}