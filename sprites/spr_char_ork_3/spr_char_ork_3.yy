{
    "id": "76e3258c-d633-475a-a92f-09562ad6f15f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2003,
    "bbox_left": 0,
    "bbox_right": 938,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "940feb9e-4cb8-47c8-9bd3-169999e97db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e3258c-d633-475a-a92f-09562ad6f15f",
            "compositeImage": {
                "id": "b1d9732f-54e9-426d-8f67-8213881818f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940feb9e-4cb8-47c8-9bd3-169999e97db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f4cf10f-66e7-439d-8118-ec306dc084f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940feb9e-4cb8-47c8-9bd3-169999e97db9",
                    "LayerId": "f6f95ef2-1e0f-4f9a-8ad9-708c1308983f"
                }
            ]
        },
        {
            "id": "050d8156-f9cf-44ff-b1ba-6c87c30f1953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e3258c-d633-475a-a92f-09562ad6f15f",
            "compositeImage": {
                "id": "040ec964-8937-48fc-a43b-ff04c8c42125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "050d8156-f9cf-44ff-b1ba-6c87c30f1953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71599b23-1647-4111-ae09-ddd7aacccdab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "050d8156-f9cf-44ff-b1ba-6c87c30f1953",
                    "LayerId": "f6f95ef2-1e0f-4f9a-8ad9-708c1308983f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2004,
    "layers": [
        {
            "id": "f6f95ef2-1e0f-4f9a-8ad9-708c1308983f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76e3258c-d633-475a-a92f-09562ad6f15f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 939,
    "xorig": 0,
    "yorig": 0
}