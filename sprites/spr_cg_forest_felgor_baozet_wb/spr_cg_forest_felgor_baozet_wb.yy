{
    "id": "2df4d82b-2f6c-4c5a-a630-ce2dba23d51c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cg_forest_felgor_baozet_wb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1949,
    "bbox_left": 0,
    "bbox_right": 2799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "612e5d7f-7a71-4ac9-9c15-6f8b611ca02c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2df4d82b-2f6c-4c5a-a630-ce2dba23d51c",
            "compositeImage": {
                "id": "f0d17205-a3aa-4edd-ac39-01da652a96e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612e5d7f-7a71-4ac9-9c15-6f8b611ca02c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a517e8-303a-4ab7-9e57-1e8e77a42df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612e5d7f-7a71-4ac9-9c15-6f8b611ca02c",
                    "LayerId": "c0eb001c-cbf8-465c-ae0b-7d7829765281"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1950,
    "layers": [
        {
            "id": "c0eb001c-cbf8-465c-ae0b-7d7829765281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2df4d82b-2f6c-4c5a-a630-ce2dba23d51c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2800,
    "xorig": 0,
    "yorig": 0
}