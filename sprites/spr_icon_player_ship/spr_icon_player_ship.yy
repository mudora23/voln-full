{
    "id": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_player_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 6,
    "bbox_right": 43,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bbef4b4-de2a-46a6-b062-8404792411fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "dd6ec751-ef4a-43b8-a534-404acab38476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbef4b4-de2a-46a6-b062-8404792411fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "313e4a4c-256d-4d01-985f-987a0a9268a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbef4b4-de2a-46a6-b062-8404792411fd",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "e4fd654e-d377-493b-a9e4-0a9b772d5163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "639a528c-e657-475c-b91b-051a54096e49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4fd654e-d377-493b-a9e4-0a9b772d5163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac900fde-2f8b-4692-9bc6-9fd314f7c482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4fd654e-d377-493b-a9e4-0a9b772d5163",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "245669f1-ab58-4c4d-8814-8f40ed9382a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "54d2ed2b-1d5f-4eb9-b38a-eec65b2efe1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245669f1-ab58-4c4d-8814-8f40ed9382a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a64a66-d804-4c40-96f6-9a1cee68e13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245669f1-ab58-4c4d-8814-8f40ed9382a6",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "e1412a35-a12d-429e-8ba6-ac1d5c6b6949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "c2fb0ad6-1982-46e7-90e9-dc68a248ca08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1412a35-a12d-429e-8ba6-ac1d5c6b6949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca68f6d-3265-4efd-8748-b09836412def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1412a35-a12d-429e-8ba6-ac1d5c6b6949",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "13243bcf-cd58-440d-b8f4-0278cb97e289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "bc61066f-e940-4640-bb64-6c8d614647e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13243bcf-cd58-440d-b8f4-0278cb97e289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf33aad-607a-4c51-868b-4e8ed6d893b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13243bcf-cd58-440d-b8f4-0278cb97e289",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "34f1c44c-dbdb-453c-87c1-2abf8e645c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "e0689204-1e10-4cfc-a9e1-ea8dc31187dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f1c44c-dbdb-453c-87c1-2abf8e645c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f381a5f5-1e06-4bed-b23f-b226c3a51816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f1c44c-dbdb-453c-87c1-2abf8e645c6a",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "6da45a03-dea2-428d-b690-34770a10ca5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "6daebbfb-a92d-4126-8ee7-09fd236511c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6da45a03-dea2-428d-b690-34770a10ca5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8da1a81-6322-4390-95f6-14a94e461682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6da45a03-dea2-428d-b690-34770a10ca5e",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "9177dbd9-003d-4fff-895c-1e08e1481b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "d126b8a2-dd47-46de-a0b7-296ca2dd8532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9177dbd9-003d-4fff-895c-1e08e1481b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ce0e8d5-4495-44c7-b655-47e3aa0649a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9177dbd9-003d-4fff-895c-1e08e1481b4f",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "b712f404-b49d-4243-969e-9dbae7a65cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "4a00fa4b-ac54-4d25-8aaa-7544cabaa8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b712f404-b49d-4243-969e-9dbae7a65cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9604941f-3d43-4351-99f3-b25857b943d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b712f404-b49d-4243-969e-9dbae7a65cdd",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "cc3fa137-5941-4ca4-bf64-576af4fb7a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "2f94e420-5def-454e-a3a3-b0d56e1030cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc3fa137-5941-4ca4-bf64-576af4fb7a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091bc78f-c814-4b84-8631-2439bb331421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc3fa137-5941-4ca4-bf64-576af4fb7a8c",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "ebf0ce26-1df6-4ffb-a952-ca91d699599d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "dc5478a0-f30a-49c5-bf56-b925b422917d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf0ce26-1df6-4ffb-a952-ca91d699599d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "238f9246-497d-4413-97c9-4350a28fb0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf0ce26-1df6-4ffb-a952-ca91d699599d",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "46306f6f-cf5d-42e0-bb49-60281786771f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "2bd6f3e0-a51a-4489-9203-b776dde71e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46306f6f-cf5d-42e0-bb49-60281786771f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36894e1f-6d15-42ec-94da-18270e4c52d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46306f6f-cf5d-42e0-bb49-60281786771f",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "e4f1c96a-236c-4aae-a328-270771466c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "1ec06154-b140-4831-bb69-f58ac8e6a07e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f1c96a-236c-4aae-a328-270771466c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f1346d-1ba4-411d-8b2e-f0586fe75934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f1c96a-236c-4aae-a328-270771466c01",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "cf5f7f92-1698-4d83-90ff-f13c5bbe42d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "76cc9cdf-b05a-4ceb-8fd6-443d7a314152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf5f7f92-1698-4d83-90ff-f13c5bbe42d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "682c754e-1ba2-4523-9ab2-2e842ff6e276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf5f7f92-1698-4d83-90ff-f13c5bbe42d4",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "2d19890f-5002-4502-8df1-3a5622225c40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "dd1add67-4890-4744-adf3-6d3a6b182833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d19890f-5002-4502-8df1-3a5622225c40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8941e2-ac2e-4962-bb6f-a2d5919c7abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d19890f-5002-4502-8df1-3a5622225c40",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "15756667-8d14-4f80-b668-af1c8226c512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "36533196-b723-40f1-a67d-8b5e12d1c6a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15756667-8d14-4f80-b668-af1c8226c512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b65fa1fc-798e-471c-9bce-c2b431bef056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15756667-8d14-4f80-b668-af1c8226c512",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "fb677255-3c76-4ae1-bc72-1a36cb0b3372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "00564d38-c36c-4fb8-b68d-f6720fae4f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb677255-3c76-4ae1-bc72-1a36cb0b3372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4dcc7d0-09cd-41be-8bdb-5a62368c2842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb677255-3c76-4ae1-bc72-1a36cb0b3372",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "f71cac7b-61f1-48a2-930a-ed9e447bb280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "33893065-fc4c-40d3-b2df-58acef60f1d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71cac7b-61f1-48a2-930a-ed9e447bb280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880ecf55-5d00-4b0f-806f-0b64f53e5963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71cac7b-61f1-48a2-930a-ed9e447bb280",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "c196991e-71d9-49e7-88af-2e7836839b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "119ce866-ba06-4f93-b421-efc88d86e1e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c196991e-71d9-49e7-88af-2e7836839b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce759c1-7601-4946-a3b2-66e8eab605e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c196991e-71d9-49e7-88af-2e7836839b8c",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "096b97bd-1b9a-4bea-8be1-9461e4294cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "c892d289-fbf9-4e17-a90c-c6e6d4124d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "096b97bd-1b9a-4bea-8be1-9461e4294cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c37768d2-d290-40b3-964b-70d1f1642d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "096b97bd-1b9a-4bea-8be1-9461e4294cd0",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "e40c45a0-98d8-47a7-b399-dd1499e4ee8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "04d09d62-b088-49d8-9609-c199d751796c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e40c45a0-98d8-47a7-b399-dd1499e4ee8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdb2c6da-c3e6-4da1-a2a1-1c93035f6f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e40c45a0-98d8-47a7-b399-dd1499e4ee8f",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "25294220-31e5-4481-b83e-6994098c2a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "38c6a90c-ac56-4285-ae30-c7f3a8fd094c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25294220-31e5-4481-b83e-6994098c2a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c027e0b-ab50-4a68-bd87-51df1efbbaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25294220-31e5-4481-b83e-6994098c2a7c",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "db76eba7-beb5-49aa-a72d-34103e309397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "ca770b34-87fa-41d7-bd52-e3675ce0fbbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db76eba7-beb5-49aa-a72d-34103e309397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b209424-40b4-4bf9-b1f5-f2751df5410a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db76eba7-beb5-49aa-a72d-34103e309397",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        },
        {
            "id": "f98dec04-2ea2-45f9-ba44-e0067fb8b93e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "compositeImage": {
                "id": "c332ea6e-ee6d-49bf-b7ad-ca2538115652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98dec04-2ea2-45f9-ba44-e0067fb8b93e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f37dd18-aa15-4f45-9b4b-f0da33b8d938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98dec04-2ea2-45f9-ba44-e0067fb8b93e",
                    "LayerId": "14011c30-90dd-429a-8e0b-873e3869c008"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "14011c30-90dd-429a-8e0b-873e3869c008",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "392054c4-7eaa-40f8-ab4d-859791f34e0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 35
}