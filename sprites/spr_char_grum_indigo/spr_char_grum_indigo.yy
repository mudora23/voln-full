{
    "id": "a67dfb6b-467e-4df6-946b-09603a93634c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_indigo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7389d33b-cee6-4607-9095-7b1101d2c2dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67dfb6b-467e-4df6-946b-09603a93634c",
            "compositeImage": {
                "id": "b306b392-bfab-494a-a5ec-c85f351d6e81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7389d33b-cee6-4607-9095-7b1101d2c2dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511c886a-61e5-4726-9b9c-2ce186e29a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7389d33b-cee6-4607-9095-7b1101d2c2dc",
                    "LayerId": "cffe0372-a209-4d7f-854e-f87433059d44"
                }
            ]
        },
        {
            "id": "8372b68e-45a3-4808-bb81-c21fc359f566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67dfb6b-467e-4df6-946b-09603a93634c",
            "compositeImage": {
                "id": "f9682b24-3fca-436b-992b-310f072b99ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8372b68e-45a3-4808-bb81-c21fc359f566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d026c3a7-6dea-4ea7-a7bd-597c45c160c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8372b68e-45a3-4808-bb81-c21fc359f566",
                    "LayerId": "cffe0372-a209-4d7f-854e-f87433059d44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "cffe0372-a209-4d7f-854e-f87433059d44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a67dfb6b-467e-4df6-946b-09603a93634c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}