{
    "id": "b5a07599-5379-4424-8326-9cab377cd36d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_player_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 14,
    "bbox_right": 50,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4298b57-e09f-4723-b882-03d7ab0dc4b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5a07599-5379-4424-8326-9cab377cd36d",
            "compositeImage": {
                "id": "db3e3f2d-8092-4069-926f-8aa9e70be360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4298b57-e09f-4723-b882-03d7ab0dc4b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff6269cd-edd9-4b3f-ae6e-e963daca4711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4298b57-e09f-4723-b882-03d7ab0dc4b8",
                    "LayerId": "3cc22b72-4e54-4a21-94c0-f2709a2c9465"
                }
            ]
        },
        {
            "id": "79fb9124-aff9-4a6b-8edc-0e2842fa0943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5a07599-5379-4424-8326-9cab377cd36d",
            "compositeImage": {
                "id": "60c4fa1e-e9b7-4fbf-ba20-92e4a70c0f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79fb9124-aff9-4a6b-8edc-0e2842fa0943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c0ba32-daae-4958-b008-36981767718f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79fb9124-aff9-4a6b-8edc-0e2842fa0943",
                    "LayerId": "3cc22b72-4e54-4a21-94c0-f2709a2c9465"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "3cc22b72-4e54-4a21-94c0-f2709a2c9465",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5a07599-5379-4424-8326-9cab377cd36d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}