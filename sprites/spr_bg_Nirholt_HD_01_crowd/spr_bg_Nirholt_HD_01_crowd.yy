{
    "id": "125ad2c3-2b35-4fef-979c-cf81fbd91e9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_HD_01_crowd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4639e591-551d-478f-a91c-fba9dc2181c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "125ad2c3-2b35-4fef-979c-cf81fbd91e9b",
            "compositeImage": {
                "id": "28ae90aa-705d-4b08-bf3d-a4b1783ba997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4639e591-551d-478f-a91c-fba9dc2181c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c47edc3-d9b4-4467-bc74-fcd0e44e7618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4639e591-551d-478f-a91c-fba9dc2181c7",
                    "LayerId": "e405a162-6742-42e2-bf4f-ff2dfc2746d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "e405a162-6742-42e2-bf4f-ff2dfc2746d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "125ad2c3-2b35-4fef-979c-cf81fbd91e9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}