{
    "id": "b4a2d83b-7406-40d0-84ae-8ab068c2a12c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 5,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29a35b90-19cf-464a-a5fd-a9c2ecef5185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4a2d83b-7406-40d0-84ae-8ab068c2a12c",
            "compositeImage": {
                "id": "0f61c2e4-637b-4ce3-993d-6e34a625bc59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a35b90-19cf-464a-a5fd-a9c2ecef5185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822c2b45-cb9d-4bc8-bfb3-1238ad68f94c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a35b90-19cf-464a-a5fd-a9c2ecef5185",
                    "LayerId": "90dd024a-918a-4e1f-8327-4a437154b977"
                }
            ]
        },
        {
            "id": "53d3dbd5-91c9-4767-80a2-b1a2d3611c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4a2d83b-7406-40d0-84ae-8ab068c2a12c",
            "compositeImage": {
                "id": "0a43784c-af5f-4fc3-bcab-4c84f7f33a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d3dbd5-91c9-4767-80a2-b1a2d3611c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb3ab212-3fce-4f82-a79f-219dd004ac0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d3dbd5-91c9-4767-80a2-b1a2d3611c6e",
                    "LayerId": "90dd024a-918a-4e1f-8327-4a437154b977"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "90dd024a-918a-4e1f-8327-4a437154b977",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4a2d83b-7406-40d0-84ae-8ab068c2a12c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}