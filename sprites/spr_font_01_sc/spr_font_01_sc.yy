{
    "id": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_sc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f34720b-41cf-426b-9ca6-dd5c761a03ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "3ba89b02-b3f5-4cd1-9831-dc810c058f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f34720b-41cf-426b-9ca6-dd5c761a03ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db157e2a-ce5b-4c30-ae60-52f8bcaeb099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f34720b-41cf-426b-9ca6-dd5c761a03ad",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "0e8583a5-4eef-40fd-906a-b58850fcee4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "9e403ae3-e611-4575-8f03-28e894d99be0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e8583a5-4eef-40fd-906a-b58850fcee4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a749fda-a00a-4d76-b341-7b84934076fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e8583a5-4eef-40fd-906a-b58850fcee4e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e1da90eb-f21d-488e-9bca-5468b79bdd1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "7323722c-9d5b-42c0-9a1f-da437189fb22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1da90eb-f21d-488e-9bca-5468b79bdd1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f835e9-7f4c-4837-bd9b-7284d351ae21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1da90eb-f21d-488e-9bca-5468b79bdd1b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "7fa2ef76-3f30-4bd8-946e-1a0a3bb244f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "1ae598a6-a0f0-41f5-a6f0-daf5d490049f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa2ef76-3f30-4bd8-946e-1a0a3bb244f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0812121b-9a45-4d70-b5e2-93bc37b5709c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa2ef76-3f30-4bd8-946e-1a0a3bb244f4",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "cf6f4316-7a2c-4d2c-8f0f-b8d1825210e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f61cd80d-1989-4e44-8ad7-4bf519cfa69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf6f4316-7a2c-4d2c-8f0f-b8d1825210e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8259af19-8781-4770-ac9d-ba06b3cb4db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6f4316-7a2c-4d2c-8f0f-b8d1825210e5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "2ae19674-5aef-4d90-bbfb-a415dbb83d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "1f2b7b70-d64d-4fb2-bc25-e7d4b54325c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae19674-5aef-4d90-bbfb-a415dbb83d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3a7b43-14d1-42b4-a035-308c5734db95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae19674-5aef-4d90-bbfb-a415dbb83d8d",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "f0df270f-70f2-4824-adcb-338adb9128dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "8ea6d280-254d-49b7-9983-d8fc96aee44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0df270f-70f2-4824-adcb-338adb9128dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a7834a-29cf-444c-891c-e12ec15a9b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0df270f-70f2-4824-adcb-338adb9128dd",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e9ae1f57-bd82-48a6-aa0d-ac75131c90c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "aeb257f7-7da0-45ec-9079-603f3f550df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ae1f57-bd82-48a6-aa0d-ac75131c90c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d3016f-7db4-4cf5-8347-daa538684a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ae1f57-bd82-48a6-aa0d-ac75131c90c3",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "92db21fe-570f-4f58-b2f5-2543048561db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "b987b876-af15-4931-b814-f15a47193c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92db21fe-570f-4f58-b2f5-2543048561db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f6350c-907e-40c5-9312-b17c0eb9d2ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92db21fe-570f-4f58-b2f5-2543048561db",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "a1414373-ff93-4910-be03-c3e13fc69452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f6c1121c-cc8d-4cf0-8e7c-c5bcf929039f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1414373-ff93-4910-be03-c3e13fc69452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80dcad8b-199b-4e06-a18f-46ce1909322e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1414373-ff93-4910-be03-c3e13fc69452",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "1a93c323-471f-4989-87d3-df99c11623fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "a6367d95-d415-41a1-937c-8538e8970bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a93c323-471f-4989-87d3-df99c11623fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9337377d-8b8e-4097-a688-d62303aa1e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a93c323-471f-4989-87d3-df99c11623fb",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "95c45814-d4ef-40a8-8f97-1642357d879a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "29171d98-413f-4b97-82c9-63c4d4147ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95c45814-d4ef-40a8-8f97-1642357d879a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e229805-9ea5-43ee-b6c8-40542fe3fddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95c45814-d4ef-40a8-8f97-1642357d879a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "f2149417-7352-49c2-ad02-b8ffa4fa25cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c83d3151-1b45-4739-bb7b-c4224a04b762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2149417-7352-49c2-ad02-b8ffa4fa25cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2225d83-c97c-47e0-b659-396d0360eb5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2149417-7352-49c2-ad02-b8ffa4fa25cd",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d6015de5-4487-430a-af80-3589b3ed73a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c0364afd-a38a-4e5f-b1a2-8e54b1dba735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6015de5-4487-430a-af80-3589b3ed73a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbcf646-be9c-49bd-aaa7-3cf32cd38351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6015de5-4487-430a-af80-3589b3ed73a9",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "96519ae7-f249-48c5-8b22-550d6b57a386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "acab2a09-0190-4b84-8b66-4c9818f00842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96519ae7-f249-48c5-8b22-550d6b57a386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e24592-d2dc-46a0-8833-dbd9597c2ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96519ae7-f249-48c5-8b22-550d6b57a386",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4ca832c4-b78d-494d-a86d-fde626dfd9b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "ba4aaaf4-acc4-4d68-8aca-66b402091c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ca832c4-b78d-494d-a86d-fde626dfd9b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee46d6c9-3b6d-404f-bffb-7e2d1590c579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ca832c4-b78d-494d-a86d-fde626dfd9b7",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "77f1ec3c-324f-4528-a45e-ec7a4d396c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f71d7244-887b-4754-9b3c-47f54f39e072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f1ec3c-324f-4528-a45e-ec7a4d396c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8712edee-ae02-4cfe-9c3a-2da30f2aa909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f1ec3c-324f-4528-a45e-ec7a4d396c92",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "24072bfd-3265-4171-a574-d88cc527643c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "ec1ac754-c26b-4c11-8eec-600301cd721e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24072bfd-3265-4171-a574-d88cc527643c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5424c05-fc4d-4fb3-b10a-aeb376ac691d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24072bfd-3265-4171-a574-d88cc527643c",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "01461031-396b-47fb-b16d-0772692e2e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "b62f8d4e-c2e4-4fce-952b-763b2d40dfbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01461031-396b-47fb-b16d-0772692e2e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e8343db-8a9c-4902-8936-dd7a6a8d3074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01461031-396b-47fb-b16d-0772692e2e62",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4e4921b4-4b03-4c1b-8a7d-33c798c48b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "ff0943f6-fd9d-4b20-a481-a8de9c8ce731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e4921b4-4b03-4c1b-8a7d-33c798c48b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b8ade1-e461-4cd8-97a9-6fa8facd163f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4921b4-4b03-4c1b-8a7d-33c798c48b3b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "87a269ae-503b-4545-ad8e-3445d0c7cd35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "9441fcc7-4397-42bc-b60a-392a36dab35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87a269ae-503b-4545-ad8e-3445d0c7cd35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e56457f-5c00-429d-a123-b563f6edd249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87a269ae-503b-4545-ad8e-3445d0c7cd35",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e710fbbf-2e4d-4ba4-b2d4-c7ff5b89e50a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "2f7a5d4d-150e-4e7b-aaf6-0e07e84e3da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e710fbbf-2e4d-4ba4-b2d4-c7ff5b89e50a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbca7252-9fe9-4665-9678-44cd2cf5e921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e710fbbf-2e4d-4ba4-b2d4-c7ff5b89e50a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "57ced4e5-befa-47ac-8461-cbebece1d598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "982dd259-9798-42ac-98cb-6e31014bed5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57ced4e5-befa-47ac-8461-cbebece1d598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee1810b9-6475-41c9-9601-3326dcd5b6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57ced4e5-befa-47ac-8461-cbebece1d598",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "367774a0-ef4a-444f-bcf7-434c97f0274b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "5dd3fffa-8aeb-4556-aa0d-d7e44455d38e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "367774a0-ef4a-444f-bcf7-434c97f0274b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f3700f3-4ce2-4360-95a0-b39a49c6056e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "367774a0-ef4a-444f-bcf7-434c97f0274b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d472c307-5352-4172-8491-209b04f34c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "6a4a5019-d64b-4755-95d6-2a8a7f9645f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d472c307-5352-4172-8491-209b04f34c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f8c38f1-9edb-4d5e-b0bc-b20e35b76085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d472c307-5352-4172-8491-209b04f34c38",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "beda8aeb-b687-4059-af7b-74952a1a883e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f0739acf-24fb-41c5-a0f9-b589612bbd86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beda8aeb-b687-4059-af7b-74952a1a883e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2660140-25ff-4f1d-8a4a-99a122b74bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beda8aeb-b687-4059-af7b-74952a1a883e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "8d3ec2bd-f489-4e04-8d9b-4c1e014f53aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "7b341486-0467-4c25-bbd3-f757c85f60b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3ec2bd-f489-4e04-8d9b-4c1e014f53aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25379c61-f7d7-4a91-bb88-bdf0d6fe26ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3ec2bd-f489-4e04-8d9b-4c1e014f53aa",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "8af50eb4-2072-45d9-88e7-0aeeedd1cc63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "3fe7a73e-43cb-4f1e-8c4a-18e29ce29431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af50eb4-2072-45d9-88e7-0aeeedd1cc63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2acc9e-077b-4fcd-a6c2-ef069f1ef2e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af50eb4-2072-45d9-88e7-0aeeedd1cc63",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "6a51270b-9b1b-4e44-927e-1e05393afd1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "94500826-2887-4a33-bfba-9155fdfbab33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a51270b-9b1b-4e44-927e-1e05393afd1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88588ed9-9289-42a6-8c4a-811c7aabbe6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a51270b-9b1b-4e44-927e-1e05393afd1e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "16a2e617-e557-451d-9c59-2a46a5b00eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "b34a28d7-3961-41d2-a246-4f88379787e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a2e617-e557-451d-9c59-2a46a5b00eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25fa79e-81f3-465a-8447-bf1d98155770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a2e617-e557-451d-9c59-2a46a5b00eb6",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "08f8cf13-a154-4991-91a0-99f0428d9d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "073402c3-d0ca-456a-a397-5a44150bcafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f8cf13-a154-4991-91a0-99f0428d9d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e552dce9-102f-43e4-8bef-d85c49089d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f8cf13-a154-4991-91a0-99f0428d9d42",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "a4b06fd2-3800-425d-8248-a1bf1756f506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "4bed10ca-6ef5-4d4b-a8cc-ea37a554eb4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b06fd2-3800-425d-8248-a1bf1756f506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf53ae2-2f47-48af-ab5e-fc3126058045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b06fd2-3800-425d-8248-a1bf1756f506",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "b7fa721d-0092-4dfb-a364-d7a00df6d79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "53d09747-14e2-47ed-a728-84e827cb5757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7fa721d-0092-4dfb-a364-d7a00df6d79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f3e4ea-e796-4b74-909e-c908d2f0b9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7fa721d-0092-4dfb-a364-d7a00df6d79b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "1f4121a9-891e-4ee9-83b5-6c02cf731628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "4473fa0e-4685-4466-a9e9-ad1d2e59c78a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f4121a9-891e-4ee9-83b5-6c02cf731628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f76395-df8e-4214-8346-ea32cb3b3454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f4121a9-891e-4ee9-83b5-6c02cf731628",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "a6a20818-e33f-4e76-ae54-40725ec9a62d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "41e4f05b-90ef-4f8d-b754-d84caf67074b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a20818-e33f-4e76-ae54-40725ec9a62d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9f9fa7b-6bba-43f0-bbdd-455fa4ed9285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a20818-e33f-4e76-ae54-40725ec9a62d",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "45c37c4a-779f-460c-b58a-c33cfb59ef38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "0a30e66b-f52a-42a7-a6fb-a0c04e8bc806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c37c4a-779f-460c-b58a-c33cfb59ef38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88f1f704-8dca-4900-84d1-2ef896ca3f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c37c4a-779f-460c-b58a-c33cfb59ef38",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e2b94a22-e491-42ea-adf1-f488fce369d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "e8a9634d-f633-4cd6-b1d3-c945615f1e15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b94a22-e491-42ea-adf1-f488fce369d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40869c58-ae84-441c-b860-c2b6fde7e82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b94a22-e491-42ea-adf1-f488fce369d5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "06d0448e-1e87-43d5-b38a-90bfdd9f5bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "32f05773-313c-4618-8ad6-a80826851d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d0448e-1e87-43d5-b38a-90bfdd9f5bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03a4cd86-0363-4858-92c5-dacd2909c4cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d0448e-1e87-43d5-b38a-90bfdd9f5bf5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "cafa9d7b-2201-4558-ba20-23f11db26f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "973a3aa3-3424-49a4-8890-43ab44ede6ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafa9d7b-2201-4558-ba20-23f11db26f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99fccde-aa2e-490e-86c8-e9eb6216093f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafa9d7b-2201-4558-ba20-23f11db26f12",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "9a9850ce-abeb-49b7-88ab-a503a80e5563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "a7b75891-6d31-496e-be77-af9e4958e889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9850ce-abeb-49b7-88ab-a503a80e5563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c599511-5ed1-43a9-9c6d-395dacb1d6a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9850ce-abeb-49b7-88ab-a503a80e5563",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "90413879-addc-4337-ab6a-60aa69d92e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "16536430-6b62-4b41-be06-4431f2551cd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90413879-addc-4337-ab6a-60aa69d92e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ef2ff1-36f1-4730-aae2-1c22df405dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90413879-addc-4337-ab6a-60aa69d92e5a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "7824084e-500c-4466-9a1f-f74158a93023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "817f29e4-c0a9-43ec-a828-ed659561ddf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7824084e-500c-4466-9a1f-f74158a93023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cedce95-3218-4dcb-abde-22e5d6059a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7824084e-500c-4466-9a1f-f74158a93023",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "f0d17b0d-17e1-495a-b411-2d80bb1d62c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "4ae2d879-c43a-409b-8fef-31848e4d8665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d17b0d-17e1-495a-b411-2d80bb1d62c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1299634-1deb-48b3-9c73-85c84808e79a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d17b0d-17e1-495a-b411-2d80bb1d62c2",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e738c326-365e-410f-b115-d66cf2488731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "86315a34-3b10-49d2-a27d-a3ba649e14d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e738c326-365e-410f-b115-d66cf2488731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c051151-cb06-4415-9fd7-0c1ba89ed850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e738c326-365e-410f-b115-d66cf2488731",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "eb2926f3-a588-4c38-b552-3682ac67a909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "9cb268bd-561a-4b7f-b7d8-00d84b3f8794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb2926f3-a588-4c38-b552-3682ac67a909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c411842-187a-47c6-b563-5de1bfcf5df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb2926f3-a588-4c38-b552-3682ac67a909",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "b16c9d09-12d4-43c3-a441-7e699a450283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "56eb39dd-06ed-4efd-8205-ceafa52cc4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b16c9d09-12d4-43c3-a441-7e699a450283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0201f57-f546-4c6e-8ed3-9ad149363eb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b16c9d09-12d4-43c3-a441-7e699a450283",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "32a7efbd-8da5-4a57-9247-8e70eb667651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "9b110716-ea23-4021-8f17-ccf25eb0d42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a7efbd-8da5-4a57-9247-8e70eb667651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d165a443-d517-4662-9905-f348ec575b2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a7efbd-8da5-4a57-9247-8e70eb667651",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "37656894-da18-4e91-8872-abfa054b2e47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "039eeea9-099f-46bc-8b68-80ac292b40f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37656894-da18-4e91-8872-abfa054b2e47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e4a13b-ba38-4226-8d46-03c49870fcb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37656894-da18-4e91-8872-abfa054b2e47",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "004fa6db-e62a-434a-a5f1-ce74c373fb63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "a3b0215e-90fd-4c98-90f8-5c8e56e289c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "004fa6db-e62a-434a-a5f1-ce74c373fb63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0411b461-e218-45d2-be4d-6ee1691cf3a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "004fa6db-e62a-434a-a5f1-ce74c373fb63",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "ee097bd5-856f-4fae-8ca3-d7c22435ae63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "447121fa-8a22-4ba8-b96a-671b2fba80b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee097bd5-856f-4fae-8ca3-d7c22435ae63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ec8e653-46b1-430c-a41d-1605f6d316ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee097bd5-856f-4fae-8ca3-d7c22435ae63",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "ab085d25-d6ec-4257-8f39-d751233cbd3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "7cadb677-63eb-4e92-a9bb-da992a498fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab085d25-d6ec-4257-8f39-d751233cbd3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b7a519-542c-428a-b5bd-f2379b802653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab085d25-d6ec-4257-8f39-d751233cbd3c",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "269666d1-a907-434e-97d2-ba69e7e5949a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "e1526b6d-06d8-4d43-90bc-e8f20000c041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269666d1-a907-434e-97d2-ba69e7e5949a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a2df61-7bce-499c-a5fe-e250217cfb2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269666d1-a907-434e-97d2-ba69e7e5949a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "ef04c8e0-4c05-460f-8f34-eb9ebb4161cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "19d7624c-840c-4781-8488-9ac4c4a62498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef04c8e0-4c05-460f-8f34-eb9ebb4161cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919bb93f-d987-49a7-97a6-f1971f00ad52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef04c8e0-4c05-460f-8f34-eb9ebb4161cd",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "ca2b8f18-4c52-4819-833b-13993f7823b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "8db962f0-7c60-447a-ae1e-000282983aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2b8f18-4c52-4819-833b-13993f7823b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc501d68-fa20-4f39-a441-9f96eda7f095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2b8f18-4c52-4819-833b-13993f7823b9",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "955c2646-e690-4100-b760-0df51084a82f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f350c595-d7ae-4409-b177-a01333218a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955c2646-e690-4100-b760-0df51084a82f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02b72402-5e0b-4684-9dfc-5f5db3df024a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955c2646-e690-4100-b760-0df51084a82f",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "1045f81b-3eff-406e-a92b-4820c4a0471b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "16890547-76dd-4f5e-92b1-652d029f15dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1045f81b-3eff-406e-a92b-4820c4a0471b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47717eeb-5ead-4d56-be42-95f2cead13df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1045f81b-3eff-406e-a92b-4820c4a0471b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4a20fcec-13a0-40a6-b03c-76b8747d2f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f2c9c025-8118-44f3-9abc-123b676f0da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a20fcec-13a0-40a6-b03c-76b8747d2f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "738b0d88-fa5c-47ee-8e39-54aee3e0707b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a20fcec-13a0-40a6-b03c-76b8747d2f5a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "6487d138-9fd7-4983-bb83-668b6dea01b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "8d594f55-8550-482d-92d2-13d35448fdc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6487d138-9fd7-4983-bb83-668b6dea01b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f64693a3-4f2a-44f5-9864-f010c8e2a860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6487d138-9fd7-4983-bb83-668b6dea01b8",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d904b171-b05b-471e-b405-c22df275313a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "dbc798be-51ff-48ef-9758-d0ba8961fdb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d904b171-b05b-471e-b405-c22df275313a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb7a3ab7-f4e1-4010-82fb-f0051baa2788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d904b171-b05b-471e-b405-c22df275313a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "1c439894-f19e-46ac-99f5-1c0f49191a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "d9a4356f-3f6f-4317-adf5-390c965b6d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c439894-f19e-46ac-99f5-1c0f49191a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e90007-908e-42c2-97f9-fb0584c713df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c439894-f19e-46ac-99f5-1c0f49191a00",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d6c55350-343d-4ffe-ab0e-c489efac0149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "25f6b6ae-bfd6-4d76-8ba1-72d34c60dc48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c55350-343d-4ffe-ab0e-c489efac0149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05b8ebb-8356-482f-b163-9149cee9505d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c55350-343d-4ffe-ab0e-c489efac0149",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "31171ad7-8ca6-493a-a1c3-71ea06e77f07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "f1b3dbc4-d2e3-4823-b97a-e08caa98e943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31171ad7-8ca6-493a-a1c3-71ea06e77f07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c1408f-976b-4489-9b03-eb9838d73f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31171ad7-8ca6-493a-a1c3-71ea06e77f07",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "6abe6b89-90af-4332-9cb2-39673f5343b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "545a12fc-8fef-4070-bc7c-c0e4f29647b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abe6b89-90af-4332-9cb2-39673f5343b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b1a527c-5edf-4fe6-af22-9d686f8fc694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abe6b89-90af-4332-9cb2-39673f5343b5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "b71c9cda-994f-48c9-adba-2f65f8b77d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "176e0c32-33d8-4ac4-9124-eb5cfbd108ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71c9cda-994f-48c9-adba-2f65f8b77d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b5e56c-f50c-48df-adef-41fd0adbe579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71c9cda-994f-48c9-adba-2f65f8b77d52",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e5e14a11-72f7-49d9-b9dc-2d08b60b6360",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "bdd50f3d-c879-4c54-9822-1441e86ee798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e14a11-72f7-49d9-b9dc-2d08b60b6360",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa676a4-d94f-40c6-8822-c906a45d1f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e14a11-72f7-49d9-b9dc-2d08b60b6360",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d17b7b02-db22-44fe-b2d1-7951b06abab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "2ae13362-d891-4678-bfd3-0f14fa5e7b31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d17b7b02-db22-44fe-b2d1-7951b06abab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31aa12e7-5d6e-4cf5-ba10-dc71c23ebce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d17b7b02-db22-44fe-b2d1-7951b06abab2",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "42a337c4-4217-4323-bdf5-e80d6e32d189",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "81cba904-d1a2-47e3-b4ab-420970495f78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a337c4-4217-4323-bdf5-e80d6e32d189",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba52c04-643f-4214-ab2c-156096eb8bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a337c4-4217-4323-bdf5-e80d6e32d189",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4c24dc30-62bc-4d86-9fa2-57c8ee84c402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c42cf6e3-37ba-44f4-80c3-5ae61a35e9a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c24dc30-62bc-4d86-9fa2-57c8ee84c402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ceed13-47d4-4d73-ab65-6ac7852df27d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c24dc30-62bc-4d86-9fa2-57c8ee84c402",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d0fd9fba-bd2f-4fd4-8ea7-495b988805b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "5a023947-9ff2-468c-94ae-200c922fa962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0fd9fba-bd2f-4fd4-8ea7-495b988805b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4598011-b31c-4d16-a60a-fb25b0003c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0fd9fba-bd2f-4fd4-8ea7-495b988805b0",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "04fb847f-fdbf-49a7-bf83-383eceb5d457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "87b7bdf9-9711-4a48-a38a-6d2f30e4c3d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04fb847f-fdbf-49a7-bf83-383eceb5d457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2907cb-e1a7-4907-9794-a7fd830c0e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04fb847f-fdbf-49a7-bf83-383eceb5d457",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "87b7e768-5879-4f4b-bb6e-b633c0cfca29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "5dc5bb5f-6965-4cb7-93f8-5a3acd42595b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b7e768-5879-4f4b-bb6e-b633c0cfca29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd5e99c-c145-46a8-9f63-10841af9991a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b7e768-5879-4f4b-bb6e-b633c0cfca29",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "5ee0ecde-23a5-4b14-9d49-923a166eeacc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c3ffb4be-56c2-45b8-86be-6c46ab8993e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee0ecde-23a5-4b14-9d49-923a166eeacc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d39d99-7464-478c-9220-58907f6c701d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee0ecde-23a5-4b14-9d49-923a166eeacc",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "93a01eb6-e34a-4a85-ae90-81cfaae8444e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "09b46dc1-1b1f-492e-ad29-961265c3b5a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a01eb6-e34a-4a85-ae90-81cfaae8444e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d05fa733-3ab7-41ca-bd1b-7a2e16d852b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a01eb6-e34a-4a85-ae90-81cfaae8444e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "154f31df-052a-49b3-acb1-34bbf9a47ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c9a2262a-bbf1-419c-9e83-ee961f6a7754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "154f31df-052a-49b3-acb1-34bbf9a47ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b0f3f2d-70c8-4827-843e-5f866b367d7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "154f31df-052a-49b3-acb1-34bbf9a47ce3",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "bd8d66f4-f488-4916-b5d2-d35b3e0a2098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "1fe13ce2-9523-4c76-9575-b8c272f40d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8d66f4-f488-4916-b5d2-d35b3e0a2098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664a2b11-ced9-49c0-8b55-72805ab7bd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8d66f4-f488-4916-b5d2-d35b3e0a2098",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "5d28344d-50a3-4abb-b3d2-5913f110dca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "787415cb-5f7d-4a9d-a055-40df37975537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d28344d-50a3-4abb-b3d2-5913f110dca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bacf58e5-77a5-43f0-9e4e-2f8418f4a153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d28344d-50a3-4abb-b3d2-5913f110dca2",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "37f12af8-1fa6-439a-b11a-c28c62f5e01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "a30e50b4-ddf8-4f0b-8b0d-dc055d1994bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f12af8-1fa6-439a-b11a-c28c62f5e01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0419b2-b4c6-444e-b56a-7d77e3d3143c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f12af8-1fa6-439a-b11a-c28c62f5e01c",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "1924fc85-7891-423e-a8a2-7809ddce7c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "96862748-8b99-4459-90ec-f3f678fe16dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1924fc85-7891-423e-a8a2-7809ddce7c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a54c1e3d-fda8-406e-8e06-35b6c1feecf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1924fc85-7891-423e-a8a2-7809ddce7c04",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c56cffe1-39da-47f1-8324-c11705e9c162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "27057887-bbe7-4a06-b197-a6c1e3ca252c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c56cffe1-39da-47f1-8324-c11705e9c162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a448aa3-9f71-45e8-ad77-e4f895501d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c56cffe1-39da-47f1-8324-c11705e9c162",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "00e90c38-1863-4fc9-aeec-1b822ad97c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "e1aa5166-e412-4c6a-afbf-d2717fe35bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00e90c38-1863-4fc9-aeec-1b822ad97c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5387696-8627-4240-83f5-3b3cb0c67bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00e90c38-1863-4fc9-aeec-1b822ad97c84",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "14a85e1e-45d9-4e07-afc2-3976c4713e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "66cf9153-25ed-43db-a065-fd09fe2f95c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14a85e1e-45d9-4e07-afc2-3976c4713e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4096f42a-054f-4a33-9394-fbafd156c119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14a85e1e-45d9-4e07-afc2-3976c4713e1f",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "5d177441-0d4a-4e60-a2aa-7b3d5511becd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "be1fe215-bcc2-414e-922c-f11e69205ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d177441-0d4a-4e60-a2aa-7b3d5511becd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa643d0-38b7-4813-8be9-d05fdfe0a971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d177441-0d4a-4e60-a2aa-7b3d5511becd",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "693e723c-4788-4f3e-a1eb-2e1d9d8b127f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "fc1d1736-5d03-4e2d-af9f-1f89a7b4e325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693e723c-4788-4f3e-a1eb-2e1d9d8b127f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e53ce045-479a-4ae4-8c46-55c972551152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693e723c-4788-4f3e-a1eb-2e1d9d8b127f",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "5313878b-e661-4dfe-9c4c-38187cf1d97b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c29b7d06-e9ee-497b-8362-6e030d3a18e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5313878b-e661-4dfe-9c4c-38187cf1d97b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "197627a9-afac-4723-8317-177f83521bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5313878b-e661-4dfe-9c4c-38187cf1d97b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d1fdfb9b-ded2-49ef-9368-8e958d20f806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "75b68a09-c497-463f-9299-43cb36449b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1fdfb9b-ded2-49ef-9368-8e958d20f806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba28a776-fa2d-4fd8-979a-c23eeb794ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1fdfb9b-ded2-49ef-9368-8e958d20f806",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c12ce078-d8aa-4948-bc64-5e3f99ad41e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "76b02e64-b559-4aee-9814-25905e2a9c28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12ce078-d8aa-4948-bc64-5e3f99ad41e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6c95b9-f645-4389-9cbf-344f411db707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12ce078-d8aa-4948-bc64-5e3f99ad41e4",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "735973ec-a566-4c42-8cfe-e1fb59fe04fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "12050d6c-13df-4270-a80f-f2438e1f7ba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "735973ec-a566-4c42-8cfe-e1fb59fe04fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db67778-bd83-4614-b83c-5c424c0b0e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "735973ec-a566-4c42-8cfe-e1fb59fe04fc",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c6e771a2-269f-4d3e-8cc7-ee61c256ca25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "e83667dd-8ac3-472f-b4f0-58b34459b6ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e771a2-269f-4d3e-8cc7-ee61c256ca25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abc0336-ab69-4094-ac08-5c8ff8b4f646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e771a2-269f-4d3e-8cc7-ee61c256ca25",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "5fbe9bfb-f2ce-4b17-9e7b-70736762eef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "5743ab5b-5f3b-40f0-b369-fdf791172289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbe9bfb-f2ce-4b17-9e7b-70736762eef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a39b198-3061-42e6-9973-25504b02434c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbe9bfb-f2ce-4b17-9e7b-70736762eef7",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "edba9d4f-f477-4f89-b599-3d117bf52164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "35e6376e-3c3f-4e11-896f-c1eed8a56b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edba9d4f-f477-4f89-b599-3d117bf52164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf9de4c-3da8-4c21-983a-b92f50027176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edba9d4f-f477-4f89-b599-3d117bf52164",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d14a35ad-2bc2-4ed6-9ce2-60e68b430bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "3888a121-c5de-44e7-ab49-630f76559815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d14a35ad-2bc2-4ed6-9ce2-60e68b430bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f7bf11a-663a-47e2-a956-17561d748214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d14a35ad-2bc2-4ed6-9ce2-60e68b430bb8",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "7cc9376e-2187-4f9a-8cc4-bac886ea43d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "504c4e63-910f-427f-b6e8-55d413c69947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc9376e-2187-4f9a-8cc4-bac886ea43d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8fba48-d577-4950-b80a-452106d1c935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc9376e-2187-4f9a-8cc4-bac886ea43d5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "bcc5dce8-fd65-4e83-967a-b12a596f8d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "5e34c886-aaa0-4a44-bf6f-0462a03099f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcc5dce8-fd65-4e83-967a-b12a596f8d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844b0157-8638-4c8c-8c5b-c8f6dc529bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcc5dce8-fd65-4e83-967a-b12a596f8d1f",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "098015db-9e83-4943-b4da-dde604df91f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "5c668040-f626-4e6e-a9a0-a21c32aa0714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098015db-9e83-4943-b4da-dde604df91f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e9bc4ee-afb1-44b5-afe4-08188e918785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098015db-9e83-4943-b4da-dde604df91f7",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "44436045-c373-411f-8b4e-f6fb008244b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "55e47e59-c4ca-4f1d-8d9f-2c2c932954cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44436045-c373-411f-8b4e-f6fb008244b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f1db60-db02-40da-80b8-68ff9336be20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44436045-c373-411f-8b4e-f6fb008244b6",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "13ebda6c-c2a7-4104-8d49-f5cdd04009ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "fcd58cc5-25bd-43b1-93c6-6ccb636ddc0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ebda6c-c2a7-4104-8d49-f5cdd04009ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8a8182-4802-4d66-95b7-1a8a2e3515f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ebda6c-c2a7-4104-8d49-f5cdd04009ba",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "bc108e27-c92a-4df7-8ef3-c7738a6cea21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "9689454f-0ab7-4d41-a5b7-813495d5ea99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc108e27-c92a-4df7-8ef3-c7738a6cea21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0073a9a-d89e-48ba-be6c-9c541134d173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc108e27-c92a-4df7-8ef3-c7738a6cea21",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "9f5e3355-8995-4b67-b3ef-15112f69ba56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "2130d0f3-15d5-4789-be23-a65c3b51afe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5e3355-8995-4b67-b3ef-15112f69ba56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5449a079-1ebc-47ea-8bf9-de7c6007c48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5e3355-8995-4b67-b3ef-15112f69ba56",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "2fd76a0f-0eee-4a4c-b764-499fe2ca7dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "bc3456e2-1c2f-4d50-a57e-c9e74cc5ba03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd76a0f-0eee-4a4c-b764-499fe2ca7dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae872774-9d70-4cf7-8a41-8608f29dfdc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd76a0f-0eee-4a4c-b764-499fe2ca7dbd",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "6beef716-9d11-4f08-9ca9-95ab3100dde9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "882e76f9-d3d6-47d4-af28-360d8323c31f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6beef716-9d11-4f08-9ca9-95ab3100dde9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ca3120-f9d6-40ed-8117-4a408c2f594f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6beef716-9d11-4f08-9ca9-95ab3100dde9",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "2b9db9ec-9b49-4441-a441-c0af79b517a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "a008ce99-7712-447f-95e0-15ae15458f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9db9ec-9b49-4441-a441-c0af79b517a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8849be-91e4-4450-8318-610850aa57f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9db9ec-9b49-4441-a441-c0af79b517a5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "8c8683b0-d141-4192-9b5e-da78b0312fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "b3dc0741-a155-4aee-b92c-69306e39f8b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8683b0-d141-4192-9b5e-da78b0312fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8e368f-6a5d-4256-83c5-5e7432fa4ace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8683b0-d141-4192-9b5e-da78b0312fe8",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "60373c6d-29bb-46ed-ae35-09303d024f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "e446a1df-5643-4a50-a170-f4ef495cdf46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60373c6d-29bb-46ed-ae35-09303d024f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd027ba2-c172-4628-a1b8-441abf88b0d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60373c6d-29bb-46ed-ae35-09303d024f05",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c8c36da3-344c-4f67-9809-00805fb9911f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "4216280f-a5d0-4572-a1d9-9895e0de028f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c36da3-344c-4f67-9809-00805fb9911f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8286ee-51eb-4b9d-8752-54ff153b4634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c36da3-344c-4f67-9809-00805fb9911f",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d1d66ed8-997c-48c7-be1b-dfe331049ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "4cb92fbb-04d7-48f6-af98-e55594390419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1d66ed8-997c-48c7-be1b-dfe331049ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "447280e4-540d-46ce-bc87-6d78162d6cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1d66ed8-997c-48c7-be1b-dfe331049ce8",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "ff2100c9-0f5a-4e4b-8811-b5dc93562958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "2ef6231c-59be-4bcb-b750-159ac6add628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff2100c9-0f5a-4e4b-8811-b5dc93562958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c799e548-6236-41e3-ab63-aad2a8d5237b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff2100c9-0f5a-4e4b-8811-b5dc93562958",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "03f69643-8b66-4e39-9c49-45bc7cf470f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "07af40d5-c156-401e-a466-6be56f8ef5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f69643-8b66-4e39-9c49-45bc7cf470f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac23f7c-a010-4965-aa85-1a277778492d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f69643-8b66-4e39-9c49-45bc7cf470f1",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "5b1a3aed-2916-430a-897a-36450cb717ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "bb169cda-07c8-417c-aa48-3f829ff65adb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1a3aed-2916-430a-897a-36450cb717ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80a39735-4988-4ac1-b530-27bb0790e397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1a3aed-2916-430a-897a-36450cb717ab",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "36ba1674-69a3-4e98-a681-aab9627324f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "25747e2e-889e-4ede-a0d0-671bf60f3017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ba1674-69a3-4e98-a681-aab9627324f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d76d21a0-c5c6-4d01-b825-6aed3d598200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ba1674-69a3-4e98-a681-aab9627324f9",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "d1063dc4-d456-4e94-83dc-2261a54bf834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c4d025b0-3b38-4c24-b4d8-845996f61b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1063dc4-d456-4e94-83dc-2261a54bf834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f910723-cd05-47d6-8ec8-78b0ff66d1d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1063dc4-d456-4e94-83dc-2261a54bf834",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c8785bfe-8bb5-4327-b622-d52dc12d740b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "09d0ea77-0843-4611-a919-ba126c91bd24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8785bfe-8bb5-4327-b622-d52dc12d740b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "773a6c18-d44d-460a-ab5a-6a5b15508183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8785bfe-8bb5-4327-b622-d52dc12d740b",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "33122733-9eff-4214-b33b-b8056815aef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "77aef92e-eb72-4e07-858f-74feba10141c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33122733-9eff-4214-b33b-b8056815aef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb4b7f78-b363-4af7-b95f-c6f66379e820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33122733-9eff-4214-b33b-b8056815aef7",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "cdca2f66-720b-4daf-be67-a305151161bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "84f15d9a-56a3-473d-b59c-a2a058419097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdca2f66-720b-4daf-be67-a305151161bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab91822-1c2c-4477-bf5d-76afc4589925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdca2f66-720b-4daf-be67-a305151161bf",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "159fb7cf-cf6a-4da8-93dc-3a68f276d9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "4636fcb3-0387-4512-9315-5d31d8fbc298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "159fb7cf-cf6a-4da8-93dc-3a68f276d9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aeedd39-af82-4543-b366-baad6fd592d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159fb7cf-cf6a-4da8-93dc-3a68f276d9e7",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "7796294f-eff3-44b0-9882-f7b2c61cc019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "0db2c0e7-1010-409e-adfe-8c041caf2629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7796294f-eff3-44b0-9882-f7b2c61cc019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b11d5d-3248-4758-8b77-88aed3e78229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7796294f-eff3-44b0-9882-f7b2c61cc019",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "3df7cc68-a0f4-4aae-b57e-223d7dc223b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "99a7bbeb-25d9-4825-b34c-6433c8431f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df7cc68-a0f4-4aae-b57e-223d7dc223b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db3e016f-00ce-464f-9baa-537a4c1401b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df7cc68-a0f4-4aae-b57e-223d7dc223b6",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c6000ec7-f74a-4f44-93b3-f926a9ced6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c2b443c0-ba43-491f-8994-a10425c599ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6000ec7-f74a-4f44-93b3-f926a9ced6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d17d7cbd-b206-4911-9869-45b74efa4bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6000ec7-f74a-4f44-93b3-f926a9ced6f7",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "828e2ca8-bfbf-4c64-991b-8252a2de74be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "eb7e427a-8208-4a2d-8fe4-a06b04577f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "828e2ca8-bfbf-4c64-991b-8252a2de74be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ef03b9-da28-4e93-80aa-043d2ea2db91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "828e2ca8-bfbf-4c64-991b-8252a2de74be",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "50c51804-2e4e-458b-a3fa-e69442943dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "d3f224a2-e6a9-4715-bba4-50293e87bcde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c51804-2e4e-458b-a3fa-e69442943dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efef52c0-880c-4c23-8a3c-f5dd870aa5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c51804-2e4e-458b-a3fa-e69442943dc2",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "9b2eb624-125f-455b-9486-d8b5bdf80d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "1769184c-c682-4d08-ab0e-09173f31d953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2eb624-125f-455b-9486-d8b5bdf80d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e025d7-18fe-46d0-944c-01b244dfead2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2eb624-125f-455b-9486-d8b5bdf80d35",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "8edf72ed-84d0-4bf8-a039-02c2aa88d909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "57b1fa09-fd98-4f42-ba04-721bff637694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edf72ed-84d0-4bf8-a039-02c2aa88d909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cce5d43b-18b7-46a1-b3c4-07fbc3bc7c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edf72ed-84d0-4bf8-a039-02c2aa88d909",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "fdedd127-cccf-442b-8f02-689f746a89e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "cb8ff112-022a-439f-b555-ce8a58b7f1c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdedd127-cccf-442b-8f02-689f746a89e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d643c7-8d45-4a3d-8b53-3d5fd79fd6c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdedd127-cccf-442b-8f02-689f746a89e0",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4b6e7397-c24e-4e06-ac1e-341d3d69432c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "a56a9db0-2d67-4f7d-9330-327abc1f3677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6e7397-c24e-4e06-ac1e-341d3d69432c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f781b6-185f-49d6-b7bb-105955d9eb9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6e7397-c24e-4e06-ac1e-341d3d69432c",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "a533f30a-f6ec-43ce-82f7-a803814f314a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "cac48089-41f6-4d49-98af-93c817ef9e75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a533f30a-f6ec-43ce-82f7-a803814f314a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd459a9-c64c-439c-83f2-b104c837ed9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a533f30a-f6ec-43ce-82f7-a803814f314a",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c7c1772f-6b8d-4e56-be16-82e878af2c7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "39cb484e-85f5-4d4f-b2ec-a20ac9f3e3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7c1772f-6b8d-4e56-be16-82e878af2c7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "191dc2c6-e470-42a9-a151-862670c1d5b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7c1772f-6b8d-4e56-be16-82e878af2c7e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "2436a3b1-ac4d-4b0a-85c5-07446744b9f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "d32b959b-e9eb-43bc-a921-2a4e6611b6a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2436a3b1-ac4d-4b0a-85c5-07446744b9f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78338ce8-e65f-4045-9b85-96860313d08e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2436a3b1-ac4d-4b0a-85c5-07446744b9f5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "19a058ab-335b-4bc0-996f-50192427e703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "89eeaf05-2534-4aca-8440-1d4d6e50b11e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a058ab-335b-4bc0-996f-50192427e703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20718d61-2799-45ad-af73-8dc0a9547698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a058ab-335b-4bc0-996f-50192427e703",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "99c7a2c4-091d-42a9-b8ba-c868324324d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "24771cb6-3b9c-4686-a0e6-bba2db7fca0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c7a2c4-091d-42a9-b8ba-c868324324d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4d0b0f-8d22-4923-a65f-dd4f67eff982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c7a2c4-091d-42a9-b8ba-c868324324d3",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "233f31cd-1ebe-4a8a-acea-7ea7dfa93928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c949b63c-ce03-4937-871d-bc2a2f13ae76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233f31cd-1ebe-4a8a-acea-7ea7dfa93928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3769f05a-7b57-47ab-8b35-e87c6625f22e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233f31cd-1ebe-4a8a-acea-7ea7dfa93928",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "62c031bf-264a-4e62-b491-8b918dce9c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "2e12c4b7-2349-494b-9b54-328d748a287e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62c031bf-264a-4e62-b491-8b918dce9c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2914a86-d9aa-4244-b6d5-2295db5d599e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62c031bf-264a-4e62-b491-8b918dce9c6e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4086632a-f6f9-4607-80cd-b4d5e86b1e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "c86b14ba-55da-41bb-a3f5-e219ac28e4af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4086632a-f6f9-4607-80cd-b4d5e86b1e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180700dc-cd34-4028-95d6-63949fca9274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4086632a-f6f9-4607-80cd-b4d5e86b1e75",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "fe97f145-f746-452b-b441-8e4ae749ac13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "7ea90908-9cae-4979-8cca-b92bc8c536c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe97f145-f746-452b-b441-8e4ae749ac13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9251343b-d471-48e7-841e-1dd46bdff73c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe97f145-f746-452b-b441-8e4ae749ac13",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e4bff6a6-30b7-476f-b4ca-cda617e3537d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "ccb9a5e3-acfd-4062-a884-0701a489cd9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bff6a6-30b7-476f-b4ca-cda617e3537d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5094bdb9-8ef7-4cd7-b8ba-64b8bfd41510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bff6a6-30b7-476f-b4ca-cda617e3537d",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e3c5a254-51fd-4fc5-b586-696661e15d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "94595e90-00d5-4e08-9c39-486a674e986e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c5a254-51fd-4fc5-b586-696661e15d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d053bcd-c54a-474a-9828-f9fed8340215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c5a254-51fd-4fc5-b586-696661e15d5e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "3521ee06-284f-4e02-abd7-a3fc4b8549ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "8b3fcab2-a6f0-484c-b005-c21e4227a356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3521ee06-284f-4e02-abd7-a3fc4b8549ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ad2203-a790-471f-94a8-dbade3652957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3521ee06-284f-4e02-abd7-a3fc4b8549ec",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "c23ead45-12c6-4116-aa36-f49a7d5e99db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "86089bd8-6461-479c-99ac-87ca5d0d510a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23ead45-12c6-4116-aa36-f49a7d5e99db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad96694-4b87-4e1f-bda1-cc673c91762f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23ead45-12c6-4116-aa36-f49a7d5e99db",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "46ef7618-dd0a-4351-8f18-daedc225be47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "1a49a7b4-14da-4c99-8d30-e8e5598ea95d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ef7618-dd0a-4351-8f18-daedc225be47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f993e7ba-a3b5-4674-959e-62697c9ad0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ef7618-dd0a-4351-8f18-daedc225be47",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "63dde3dc-47a8-40b2-84e5-03eeff550f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "390cc0fc-8614-4751-acd3-1cfcf6dfdb10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63dde3dc-47a8-40b2-84e5-03eeff550f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "492c48a8-40a0-495c-9766-19c054a18623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63dde3dc-47a8-40b2-84e5-03eeff550f75",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "e08c4e56-df21-4b2f-ab31-8497ac992f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "6b7729ab-8960-4ac9-8ab3-fd97c96e297d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08c4e56-df21-4b2f-ab31-8497ac992f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be28a5b-99bd-4193-95ee-e4a5633e26fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08c4e56-df21-4b2f-ab31-8497ac992f6e",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "3b0c885b-39cc-46cd-861e-92917efeb1a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "916076ea-9b78-49dd-be41-179bad2a685d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b0c885b-39cc-46cd-861e-92917efeb1a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e645783c-81c9-4351-9926-6a5d1cdc3098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b0c885b-39cc-46cd-861e-92917efeb1a8",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "a3af3c2a-9cc4-4a1c-8f6f-3918d29eb048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "25b28404-010b-485c-89df-f036d59faeb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3af3c2a-9cc4-4a1c-8f6f-3918d29eb048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf42c64-560e-4a45-8882-722ae58b9584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3af3c2a-9cc4-4a1c-8f6f-3918d29eb048",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "4c96513c-8204-4ccd-a9a7-5ef7fa419f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "fa085342-0358-4f12-a16f-fd85b5593ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c96513c-8204-4ccd-a9a7-5ef7fa419f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02101be2-f458-44da-a7b2-07f6652853e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c96513c-8204-4ccd-a9a7-5ef7fa419f66",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "80841a6c-2470-4036-84ec-1b13e8621ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "941d436c-0474-4792-bff2-971a885cb674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80841a6c-2470-4036-84ec-1b13e8621ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7500b9b1-a2aa-4af1-a294-848a7ce4a7a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80841a6c-2470-4036-84ec-1b13e8621ea1",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "07adddcf-f8aa-4999-a5e2-fdd8337faa56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "815ae0f8-b6ae-4044-8979-288b00223099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07adddcf-f8aa-4999-a5e2-fdd8337faa56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1a2968-5c47-44cd-ac1f-601c2ac1dc4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07adddcf-f8aa-4999-a5e2-fdd8337faa56",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        },
        {
            "id": "872d83bd-aa9a-472e-94fc-28beef22e7a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "compositeImage": {
                "id": "84a3a510-e7d0-4528-b7ab-1ed0f92aa152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "872d83bd-aa9a-472e-94fc-28beef22e7a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5920da33-b24a-4751-b274-e2827cdcb09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "872d83bd-aa9a-472e-94fc-28beef22e7a5",
                    "LayerId": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "3db4b074-34d9-47c3-bfca-5b1c45d2d6b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19589a3f-e8a3-403a-a146-f1f996a6aa92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}