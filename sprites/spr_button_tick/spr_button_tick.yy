{
    "id": "f0c30603-1ab9-4e00-9939-a0bd6c79814d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_tick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 5,
    "bbox_right": 58,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "136798a1-661f-45be-8dc8-a0dc562296e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0c30603-1ab9-4e00-9939-a0bd6c79814d",
            "compositeImage": {
                "id": "eb921dcc-38c3-4676-b14e-7cb4bb95f1ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136798a1-661f-45be-8dc8-a0dc562296e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "018dcf8f-ec54-488b-9f8a-d74aa7fc822a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136798a1-661f-45be-8dc8-a0dc562296e3",
                    "LayerId": "16dbca20-c7d8-4ccb-8036-a3ba5a698165"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "16dbca20-c7d8-4ccb-8036-a3ba5a698165",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0c30603-1ab9-4e00-9939-a0bd6c79814d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}