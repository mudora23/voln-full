{
    "id": "4f7780b7-1817-4f45-b767-f57f74bddac4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_doublehit1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df312773-f49f-4be4-893f-08e392ab2f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7780b7-1817-4f45-b767-f57f74bddac4",
            "compositeImage": {
                "id": "aae31a8b-e03a-4fba-b610-6c62c3d7ffae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df312773-f49f-4be4-893f-08e392ab2f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a965934-1721-4c03-9587-36ea374ff6c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df312773-f49f-4be4-893f-08e392ab2f8e",
                    "LayerId": "a445d4b7-5d4b-49aa-b867-580ae7a973fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "a445d4b7-5d4b-49aa-b867-580ae7a973fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f7780b7-1817-4f45-b767-f57f74bddac4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}