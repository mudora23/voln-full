{
    "id": "8288be2e-8f75-4e39-b122-c2afb121f06d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_toru_int",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdf44f31-b748-49b2-a9af-94839fe2660c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8288be2e-8f75-4e39-b122-c2afb121f06d",
            "compositeImage": {
                "id": "a1f43786-ac79-4a4b-b9f9-22d374ff358f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf44f31-b748-49b2-a9af-94839fe2660c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac61d768-50de-4d4c-861b-21497e8e9408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf44f31-b748-49b2-a9af-94839fe2660c",
                    "LayerId": "0a6335ba-400d-41bf-b44e-6f8985db3934"
                },
                {
                    "id": "517960cd-ac7d-421b-b26a-eaf3f8193b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf44f31-b748-49b2-a9af-94839fe2660c",
                    "LayerId": "27f88b7a-07bc-4ec1-b6d2-5e7ad15af317"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "0a6335ba-400d-41bf-b44e-6f8985db3934",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8288be2e-8f75-4e39-b122-c2afb121f06d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "27f88b7a-07bc-4ec1-b6d2-5e7ad15af317",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8288be2e-8f75-4e39-b122-c2afb121f06d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}