{
    "id": "31463508-4da4-4d99-a7c0-b198cab3924b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1970,
    "bbox_left": 0,
    "bbox_right": 1141,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3826508c-88b8-4b88-a08b-adeaf0af763e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31463508-4da4-4d99-a7c0-b198cab3924b",
            "compositeImage": {
                "id": "0b82162a-934a-4505-bd12-5396cc095b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3826508c-88b8-4b88-a08b-adeaf0af763e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3658d58-e109-4f76-94c6-c7e71e1d9677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3826508c-88b8-4b88-a08b-adeaf0af763e",
                    "LayerId": "ed66632b-02a2-419d-bb00-28ed732ef5c3"
                }
            ]
        },
        {
            "id": "eaac2e37-2c44-48a4-b731-6e72145f2a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31463508-4da4-4d99-a7c0-b198cab3924b",
            "compositeImage": {
                "id": "30f30557-f844-4601-99b5-387d965f7959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaac2e37-2c44-48a4-b731-6e72145f2a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c9389ce-389b-4f49-ae59-84967ea70edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaac2e37-2c44-48a4-b731-6e72145f2a35",
                    "LayerId": "ed66632b-02a2-419d-bb00-28ed732ef5c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1971,
    "layers": [
        {
            "id": "ed66632b-02a2-419d-bb00-28ed732ef5c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31463508-4da4-4d99-a7c0-b198cab3924b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1142,
    "xorig": 0,
    "yorig": 0
}