{
    "id": "60bb8be8-8e3a-46f6-b90c-b24f4e7d919d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_top_seemless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3550838a-0244-444b-b202-416d1da6d4dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60bb8be8-8e3a-46f6-b90c-b24f4e7d919d",
            "compositeImage": {
                "id": "7e008614-595a-44bf-bd55-8983a1a3b1c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3550838a-0244-444b-b202-416d1da6d4dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d3d566-994e-41fb-813a-7b21bb9f7f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3550838a-0244-444b-b202-416d1da6d4dc",
                    "LayerId": "1ac7bc9b-dad6-45aa-8f4b-c3f816ca41ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "1ac7bc9b-dad6-45aa-8f4b-c3f816ca41ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60bb8be8-8e3a-46f6-b90c-b24f4e7d919d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}