{
    "id": "7d8f2461-c4d3-4c12-9b5f-9f3fdc20acb3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1997,
    "bbox_left": 4,
    "bbox_right": 1111,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bb1267f-e8ef-444c-85bb-cfcea95a1b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d8f2461-c4d3-4c12-9b5f-9f3fdc20acb3",
            "compositeImage": {
                "id": "057e4a1e-c7cb-4c34-8e8b-4a991dfb40ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb1267f-e8ef-444c-85bb-cfcea95a1b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0d31bd2-2069-4b27-8b87-f196420ddf95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb1267f-e8ef-444c-85bb-cfcea95a1b27",
                    "LayerId": "9d1cd01e-8c84-41a7-a739-c77fcc2890c5"
                }
            ]
        },
        {
            "id": "16508124-c96e-4ddf-9b56-5aaf0a70865c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d8f2461-c4d3-4c12-9b5f-9f3fdc20acb3",
            "compositeImage": {
                "id": "f8beeb68-b811-4ddf-9079-6955fe479078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16508124-c96e-4ddf-9b56-5aaf0a70865c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8194cf3-c6b8-48cc-a2a4-c6b5da338963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16508124-c96e-4ddf-9b56-5aaf0a70865c",
                    "LayerId": "9d1cd01e-8c84-41a7-a739-c77fcc2890c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "9d1cd01e-8c84-41a7-a739-c77fcc2890c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d8f2461-c4d3-4c12-9b5f-9f3fdc20acb3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}