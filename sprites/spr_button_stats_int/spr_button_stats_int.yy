{
    "id": "208a6906-02a2-4b79-889c-793ed99fee09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_int",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb6d0b4b-0374-4cfd-be8b-ae835abfdf4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "208a6906-02a2-4b79-889c-793ed99fee09",
            "compositeImage": {
                "id": "36dadf50-6e97-4e36-a387-f2f39a52accb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6d0b4b-0374-4cfd-be8b-ae835abfdf4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc1479e-4295-4acb-a542-9bcec0f6b3ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6d0b4b-0374-4cfd-be8b-ae835abfdf4e",
                    "LayerId": "8ad87ce5-7065-4785-9dd1-507b7a3c5798"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8ad87ce5-7065-4785-9dd1-507b7a3c5798",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "208a6906-02a2-4b79-889c-793ed99fee09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}