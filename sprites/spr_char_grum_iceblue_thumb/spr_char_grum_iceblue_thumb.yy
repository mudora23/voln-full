{
    "id": "c878f6f7-2416-47b6-b5e1-9ec4985c6394",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_iceblue_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5de41533-1b69-4e96-bf0d-4976accc74a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c878f6f7-2416-47b6-b5e1-9ec4985c6394",
            "compositeImage": {
                "id": "e2a3d323-acaa-47dd-a028-592af0fa7e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de41533-1b69-4e96-bf0d-4976accc74a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcefe2db-667f-453d-816f-f347728a9bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de41533-1b69-4e96-bf0d-4976accc74a8",
                    "LayerId": "fabdc432-5ecf-4434-99da-3a3c46605ed9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "fabdc432-5ecf-4434-99da-3a3c46605ed9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c878f6f7-2416-47b6-b5e1-9ec4985c6394",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}