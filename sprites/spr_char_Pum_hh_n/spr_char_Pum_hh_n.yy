{
    "id": "d11380b3-e775-4b00-8368-776858dea8df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Pum_hh_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2112,
    "bbox_left": 295,
    "bbox_right": 1171,
    "bbox_top": 62,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ed4a385-8e9e-4f21-a97a-7029cbbf98b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d11380b3-e775-4b00-8368-776858dea8df",
            "compositeImage": {
                "id": "568d25c5-7126-419f-a7e1-29acb9479996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed4a385-8e9e-4f21-a97a-7029cbbf98b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d574a8d4-0c41-40b8-8cb2-4c950e1a7b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed4a385-8e9e-4f21-a97a-7029cbbf98b8",
                    "LayerId": "32143d2c-fbd3-47a7-83ce-24a116c5752c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2250,
    "layers": [
        {
            "id": "32143d2c-fbd3-47a7-83ce-24a116c5752c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d11380b3-e775-4b00-8368-776858dea8df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1400,
    "xorig": 0,
    "yorig": 0
}