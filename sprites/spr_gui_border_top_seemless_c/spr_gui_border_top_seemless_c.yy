{
    "id": "e1b5e6d1-eec1-4df3-9af0-bb8c641e207f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffb94b53-31b4-47af-94f8-e048d81385d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b5e6d1-eec1-4df3-9af0-bb8c641e207f",
            "compositeImage": {
                "id": "d7ccc60c-b3b5-4204-9aad-d623cc45505b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb94b53-31b4-47af-94f8-e048d81385d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be81c3c5-d595-420d-a590-d93146f31cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb94b53-31b4-47af-94f8-e048d81385d5",
                    "LayerId": "dc8264c3-5bb4-442e-a7f5-34ff7f799531"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "dc8264c3-5bb4-442e-a7f5-34ff7f799531",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1b5e6d1-eec1-4df3-9af0-bb8c641e207f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 5
}