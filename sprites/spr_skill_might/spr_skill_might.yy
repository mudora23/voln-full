{
    "id": "6c227ad3-691f-4aea-932f-5359d7993778",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_might",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71a7af59-86e2-4118-92fd-232740e35e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c227ad3-691f-4aea-932f-5359d7993778",
            "compositeImage": {
                "id": "7a823bad-cb60-4beb-8cbc-9280bf81ad79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a7af59-86e2-4118-92fd-232740e35e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43e4260-9c13-469f-89d0-dcf7432079d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a7af59-86e2-4118-92fd-232740e35e42",
                    "LayerId": "d09c69e2-287c-48a5-b35e-c5541604d8e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "d09c69e2-287c-48a5-b35e-c5541604d8e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c227ad3-691f-4aea-932f-5359d7993778",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}