{
    "id": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_reg_bo_it",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2268f345-e854-43c4-848d-800991500ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "82cac4ac-c0b9-4afa-8efe-c3575a8e357a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2268f345-e854-43c4-848d-800991500ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bacb1fd-a685-4718-928b-aaeef06ecf26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2268f345-e854-43c4-848d-800991500ad8",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "6c93814b-f27e-417b-b471-aba8e6f7d49f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4d50cf82-a2e4-4485-8904-43f39d2e879b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c93814b-f27e-417b-b471-aba8e6f7d49f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a551f2-dae3-45e3-b2cf-f82fbc685b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c93814b-f27e-417b-b471-aba8e6f7d49f",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "be8f9e9f-eb71-4f11-b6cb-98eb49689d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f59f6710-4737-4f1b-a15d-ebbc7fa24e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8f9e9f-eb71-4f11-b6cb-98eb49689d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2133291-e358-4ab1-8ded-f69ee5abca3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8f9e9f-eb71-4f11-b6cb-98eb49689d11",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ef082543-44a6-4996-8240-ed815410193e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "67de5710-f2a5-4847-929c-4fb71109455f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef082543-44a6-4996-8240-ed815410193e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c35f58-4d78-4891-8bca-b9b55fc90fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef082543-44a6-4996-8240-ed815410193e",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "7f7016cc-5016-4bb1-b5a4-326765526d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f0a3b611-1835-4ceb-961d-52377292fa89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7016cc-5016-4bb1-b5a4-326765526d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b2be794-ce23-4328-a848-9dea6664af36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7016cc-5016-4bb1-b5a4-326765526d00",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "2adfe110-73b2-4f39-b4df-d181a280c29e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "8e3af028-8505-4a42-b1a5-3da7e20b3bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2adfe110-73b2-4f39-b4df-d181a280c29e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dd47278-7c0e-4101-b8a4-e22ee3319d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2adfe110-73b2-4f39-b4df-d181a280c29e",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ebae0e73-fdc0-400f-9b84-6ca4d3bd15ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "87f6f7ae-cc2f-44c2-bf8d-1081347c3491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebae0e73-fdc0-400f-9b84-6ca4d3bd15ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e28d1b-ef52-42d9-91bf-64116dd44f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebae0e73-fdc0-400f-9b84-6ca4d3bd15ad",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "cd57758d-8229-4ae5-abd2-0da0d3a42bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f743341e-6bce-469e-8031-c76b520868c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd57758d-8229-4ae5-abd2-0da0d3a42bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a436acae-5790-4047-b5d5-89818bd466a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd57758d-8229-4ae5-abd2-0da0d3a42bb3",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "3a3ffc32-0980-450a-93a8-e4530a4b8656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "fef36b3c-9c34-4a60-912f-95d923e506f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a3ffc32-0980-450a-93a8-e4530a4b8656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268847ea-f559-4e20-b490-1912a3e45740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a3ffc32-0980-450a-93a8-e4530a4b8656",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "39d2d8d5-8d26-4eb1-80fe-63f50f5ddca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "9722582d-5b9e-44e9-808b-2f6f59ba0771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39d2d8d5-8d26-4eb1-80fe-63f50f5ddca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3378124-a583-43b8-bd19-8dba8f8f87fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39d2d8d5-8d26-4eb1-80fe-63f50f5ddca5",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "1ff30204-d827-4234-9afc-5a04a66cef07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5d5fafdc-07b6-4db6-8edc-b240875c4fe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ff30204-d827-4234-9afc-5a04a66cef07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a7b4b0d-68b5-46e8-8b19-678af81cbf04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff30204-d827-4234-9afc-5a04a66cef07",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "30b87ea2-18aa-41c4-b660-114b1e962cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "83b15395-bdea-4b19-8872-f3162756efd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b87ea2-18aa-41c4-b660-114b1e962cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc64700-54a5-4baa-a19d-25110928adbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b87ea2-18aa-41c4-b660-114b1e962cef",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "8090a7c6-6d30-43f1-8304-2eda166ff413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "04629050-7603-4757-a522-e40c3b37a967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8090a7c6-6d30-43f1-8304-2eda166ff413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ab861f-446b-4eaa-a67a-a3c24e2ab865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8090a7c6-6d30-43f1-8304-2eda166ff413",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "c081a9aa-8f69-4a4d-8c42-101ca21e7ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "99643574-237e-4237-9808-f972f83c7537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c081a9aa-8f69-4a4d-8c42-101ca21e7ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445fb11f-5b68-4103-b47d-fbd41bb07733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c081a9aa-8f69-4a4d-8c42-101ca21e7ba1",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "8ab6261e-547e-47d6-99a2-34d5f0774c67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "2e4ef8d8-bb42-49bc-9bf2-f5e9ed052cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab6261e-547e-47d6-99a2-34d5f0774c67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "602ad63f-de03-4586-8509-0381dc9cfa51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab6261e-547e-47d6-99a2-34d5f0774c67",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "00aafbef-27df-4802-8a78-27749936da82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "44d3feca-62db-4af1-9466-bab6f2f3da9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00aafbef-27df-4802-8a78-27749936da82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d9caa60-8344-44b8-82cf-542529349595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00aafbef-27df-4802-8a78-27749936da82",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "4426600b-e879-4090-b8ca-dfc13d910140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "70ad9424-3ef7-4f3f-a2f3-887c400e48c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4426600b-e879-4090-b8ca-dfc13d910140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce892d53-393e-49c9-b58a-a0ff557c60e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4426600b-e879-4090-b8ca-dfc13d910140",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "110873ae-41de-400b-ab07-dc01ea7077ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "7df48171-36bf-495e-b4e2-9c2b3e669a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "110873ae-41de-400b-ab07-dc01ea7077ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df169437-b369-4b98-b1c5-35416d451ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "110873ae-41de-400b-ab07-dc01ea7077ad",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b765242e-7c10-4b29-85ef-7ccc9bdd77ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a8830662-7250-42f8-9dda-4827f2f1435f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b765242e-7c10-4b29-85ef-7ccc9bdd77ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e06034d-3884-4dbb-8ddf-c5eb2711d64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b765242e-7c10-4b29-85ef-7ccc9bdd77ed",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "84a29101-b2dd-402e-a046-b3c8bbf78ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "e4586ea2-a206-4212-bd99-d1ec1e4ba649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a29101-b2dd-402e-a046-b3c8bbf78ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae42543c-ed52-4f96-99bf-2847d23d8680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a29101-b2dd-402e-a046-b3c8bbf78ca8",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "70933690-8fd7-420a-b957-19e24ba123b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "3cf85c53-80e6-4b01-9e00-d374043c54c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70933690-8fd7-420a-b957-19e24ba123b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "204a385f-e554-4014-9002-853a6de17558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70933690-8fd7-420a-b957-19e24ba123b1",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "4c089731-f9ca-40c1-b87c-b4a2779fa098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "1f8d4ef5-8d93-465f-b691-66b9695d85b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c089731-f9ca-40c1-b87c-b4a2779fa098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "065ed1be-3eb6-455a-a376-d48475378233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c089731-f9ca-40c1-b87c-b4a2779fa098",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "cdae9fbf-3194-420d-9a95-ab07588f1020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "067901d2-4520-43ba-bf16-0750139b943f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdae9fbf-3194-420d-9a95-ab07588f1020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05cbda68-5cad-4da9-996a-d15889f0f1fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdae9fbf-3194-420d-9a95-ab07588f1020",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "83d949d3-f6c6-4078-bb7c-9cf3168cb338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5b39f684-dbb6-4210-9e83-d1937705b4f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d949d3-f6c6-4078-bb7c-9cf3168cb338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6710b796-696c-45c7-b2a2-5935d04917f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d949d3-f6c6-4078-bb7c-9cf3168cb338",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "51aedf00-1312-4891-ace7-9ca8c492b282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "961fb0a2-c2e2-4452-bd01-aff624e2596e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51aedf00-1312-4891-ace7-9ca8c492b282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fcd43e2-0f01-45bc-9a75-72b7cf2aad4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51aedf00-1312-4891-ace7-9ca8c492b282",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "17a582b2-ac84-48fc-b014-1dbcb31e5ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "ec50b209-e4f6-4003-b0f1-b4cb4235374d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a582b2-ac84-48fc-b014-1dbcb31e5ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334fb450-d9b1-4b89-8aeb-a3c481bc06de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a582b2-ac84-48fc-b014-1dbcb31e5ccb",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "e8502bbc-a692-407e-a26a-cd8a170cb5aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4ee03cae-7fb4-4cac-a850-a07d8d738aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8502bbc-a692-407e-a26a-cd8a170cb5aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90106903-6919-43fc-b729-f8c3aa8935ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8502bbc-a692-407e-a26a-cd8a170cb5aa",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "c35679cb-9516-4473-85e0-05f0e765633b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "20de7429-08ee-49fe-a6d7-9d70711f1a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c35679cb-9516-4473-85e0-05f0e765633b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60ac784-f7b6-442e-b542-881e04ecffe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c35679cb-9516-4473-85e0-05f0e765633b",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "8ba56eb0-694a-45f3-805f-8b35434ad6b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "34b4661d-b521-4ae1-ab16-bc5342f23173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba56eb0-694a-45f3-805f-8b35434ad6b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cbdd75b-2b82-4507-8a0e-79291f6f1531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba56eb0-694a-45f3-805f-8b35434ad6b0",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "00557c73-2f4d-4beb-a8f1-fed662d84dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "af182422-35a2-4b72-a61f-6f7fc1907a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00557c73-2f4d-4beb-a8f1-fed662d84dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "622aea8d-17a5-482d-9a1a-c858ba8363bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00557c73-2f4d-4beb-a8f1-fed662d84dd6",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "339406e8-bf04-4347-a0c0-3d5fd3fce345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "ca8993d6-85f8-40d8-b80b-84f6e1eff200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "339406e8-bf04-4347-a0c0-3d5fd3fce345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a2b792c-c81a-4425-9159-01266e3be4db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "339406e8-bf04-4347-a0c0-3d5fd3fce345",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "a316df9a-65f6-4dad-a004-1d3b22f78f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "fbeb7ca9-a65f-44c0-b3d2-5f21de0b2a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a316df9a-65f6-4dad-a004-1d3b22f78f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54feeb2-5502-454b-8cc5-fc7bbc3141e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a316df9a-65f6-4dad-a004-1d3b22f78f2f",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "099d5da6-7088-406f-83f1-fc0fff3db0ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "95f0a1e3-69ab-402b-beee-3e674761aa6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099d5da6-7088-406f-83f1-fc0fff3db0ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be1f792-220d-4129-a002-df35022b5a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099d5da6-7088-406f-83f1-fc0fff3db0ce",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "a1b80dab-5db6-48a2-8680-220a8b431f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "63a5e01a-3b38-4309-a21b-415bf3b2955b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1b80dab-5db6-48a2-8680-220a8b431f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d5db6e5-6a54-4318-95f1-1e523f788dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1b80dab-5db6-48a2-8680-220a8b431f04",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "3b4c8283-3b3a-4deb-a4be-7113eedcb2e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "30184bae-3d03-4ba4-bd91-a7594e9dcb7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4c8283-3b3a-4deb-a4be-7113eedcb2e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2a346c-8e45-4410-b604-5e72ca44cced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4c8283-3b3a-4deb-a4be-7113eedcb2e0",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "e932f243-cb1d-4ed3-9862-ae8c8de20362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "bd05f73c-e1d9-488d-8935-e69fbae1594d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e932f243-cb1d-4ed3-9862-ae8c8de20362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65c18a8-ac90-4758-83cb-52d7197b4130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e932f243-cb1d-4ed3-9862-ae8c8de20362",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "80761f72-2e30-4e4a-a5d4-a1bd26b69c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "c7f131f8-388b-42da-a26d-c42fe39c606a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80761f72-2e30-4e4a-a5d4-a1bd26b69c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cdd9758-bdc4-4cb2-b735-7b02105093cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80761f72-2e30-4e4a-a5d4-a1bd26b69c30",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "c093f62f-0db5-4d28-800a-2eeeb6adca13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "79a93bdd-236d-4b6b-8180-7c75a5a55d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c093f62f-0db5-4d28-800a-2eeeb6adca13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e768bb-5f8d-42b7-a4b7-51004f990aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c093f62f-0db5-4d28-800a-2eeeb6adca13",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ab944797-1afa-436e-ab09-d0c155c457b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "8ed0b5c9-d600-4eb4-934c-334dc751c29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab944797-1afa-436e-ab09-d0c155c457b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ad69e4-f98b-4251-9472-e9426bf92aa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab944797-1afa-436e-ab09-d0c155c457b9",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "35ac5edc-fc48-4f24-be36-bb435194a9b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "af165478-214a-4652-9971-d887246f2328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ac5edc-fc48-4f24-be36-bb435194a9b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67f670c-effc-42a2-9d27-f36921b01220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ac5edc-fc48-4f24-be36-bb435194a9b7",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ea547fd8-dc37-4d78-b001-e67dacdc1e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "18a25791-84bf-43ed-826b-1715b22a720f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea547fd8-dc37-4d78-b001-e67dacdc1e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7497e169-21d0-4f76-a2c4-d0283593d7d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea547fd8-dc37-4d78-b001-e67dacdc1e30",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "bec1eb03-3d63-4364-867e-b6e9a3a88d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4c4d05c9-82a9-44ad-a311-b73c5504f1b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bec1eb03-3d63-4364-867e-b6e9a3a88d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1660e945-0fd7-4918-a948-614e65e1278c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bec1eb03-3d63-4364-867e-b6e9a3a88d12",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "60874f1a-64f5-4529-ab99-b2ba51af887b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f9883fd2-1e65-4972-af7b-e64c8f56fbf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60874f1a-64f5-4529-ab99-b2ba51af887b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b451dfbc-7faf-45a0-ac2e-1eafd5892cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60874f1a-64f5-4529-ab99-b2ba51af887b",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "fd0bbfd5-c6b4-44ab-b3ea-76ee18657596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "7431e6eb-8d2c-4e9a-813c-42c91ddc42c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd0bbfd5-c6b4-44ab-b3ea-76ee18657596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c586a8b-22a1-4ab9-be05-93adc7e274d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd0bbfd5-c6b4-44ab-b3ea-76ee18657596",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "d091cfb9-c9be-4022-8399-290f3551f689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "9d24bafd-6df6-45c9-9649-f07b5bddfb8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d091cfb9-c9be-4022-8399-290f3551f689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d4c1ad5-32e6-4ba4-b9c1-14ac9586483f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d091cfb9-c9be-4022-8399-290f3551f689",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "345da2e1-95f9-404d-9952-22b74119cc24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "0d85e224-bfb8-4100-80c2-839407a958e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345da2e1-95f9-404d-9952-22b74119cc24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c04ee85-fb28-44a1-9c72-c42a14923803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345da2e1-95f9-404d-9952-22b74119cc24",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "841e7ba0-8f78-4603-acb9-216687b49950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "fe5223f8-3254-4ee0-a2d9-3a2529195648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841e7ba0-8f78-4603-acb9-216687b49950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a178ef-883e-4837-b816-59c14d7bbac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841e7ba0-8f78-4603-acb9-216687b49950",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "4546404e-56e9-44ba-ba36-0b05d6a317f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "ea911e6f-d528-487c-83b8-6b2266d4b9fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4546404e-56e9-44ba-ba36-0b05d6a317f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f2d33d-b70f-449f-9e66-c48f597a926e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4546404e-56e9-44ba-ba36-0b05d6a317f3",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b4644f3d-1da2-435e-82a8-adece496e235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4c7c8739-09d0-409a-a7f8-57c353208e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4644f3d-1da2-435e-82a8-adece496e235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13458ee1-e805-4060-b445-04fcf78e3804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4644f3d-1da2-435e-82a8-adece496e235",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "06131b8c-d644-4453-8514-ab1d699d3e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "8b885a30-194f-49f4-bf27-bf11b86a1ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06131b8c-d644-4453-8514-ab1d699d3e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69bcde3c-565a-4ecc-a3ad-038c11376697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06131b8c-d644-4453-8514-ab1d699d3e39",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "d827a134-95ad-4b89-ad68-6a5a6b0104ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4c9ad0eb-ab7b-4e60-b1ff-912b05cd632b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d827a134-95ad-4b89-ad68-6a5a6b0104ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a49e61-89a8-4a20-a705-a6c40b3855a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d827a134-95ad-4b89-ad68-6a5a6b0104ed",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "9ea3d434-42a1-45eb-afdb-cb0eac7d9aa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4a8d4500-5c26-4184-bc7a-5cff47fb5b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ea3d434-42a1-45eb-afdb-cb0eac7d9aa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a467437-bd0b-49b7-850d-d7b010419cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ea3d434-42a1-45eb-afdb-cb0eac7d9aa1",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "255dbf1d-e7cc-4181-99d6-fe2665767e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f913b0e3-6c6f-43ea-ae73-5477e4774032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255dbf1d-e7cc-4181-99d6-fe2665767e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e0afa6-c4e9-44b4-8306-6813d6947bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255dbf1d-e7cc-4181-99d6-fe2665767e54",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "24590a43-f9f7-4ce3-bf68-e60333605ec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "c5035f5a-3331-4863-a63d-f628093aeb5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24590a43-f9f7-4ce3-bf68-e60333605ec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36e638a-c2b1-4d34-8a34-d84de75c910b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24590a43-f9f7-4ce3-bf68-e60333605ec8",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "eefbb8ae-b112-4ffc-86dd-a8a23f4bfe52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "7fd6b00c-14be-4d1d-a9d3-ef95bbb88e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eefbb8ae-b112-4ffc-86dd-a8a23f4bfe52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0c66a2-f0e6-4710-8845-65a292fe9130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eefbb8ae-b112-4ffc-86dd-a8a23f4bfe52",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "9e1f589a-cd90-480f-ae1a-d2912989883c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "add924ef-d9c1-4e97-8088-bcc96b17d406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e1f589a-cd90-480f-ae1a-d2912989883c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60290447-fc4e-45bd-825d-29f3ba4ea7d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1f589a-cd90-480f-ae1a-d2912989883c",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "886b9f40-0988-4c41-ad35-2636284ae894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5784755a-8923-409f-ac6c-e30ce53b700b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886b9f40-0988-4c41-ad35-2636284ae894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1095ab99-5256-4d91-8978-3a8ccb5c95ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886b9f40-0988-4c41-ad35-2636284ae894",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b4a653fc-9ea0-4e04-8f5a-d7c308eaba75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "b9611fcd-bbce-4e32-bf6c-6742caa74f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a653fc-9ea0-4e04-8f5a-d7c308eaba75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d8e5c1b-206d-453a-9600-2997906fca30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a653fc-9ea0-4e04-8f5a-d7c308eaba75",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "e0e3d9e8-fbf3-4ec1-b035-a3fd18255012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "1d76069f-5b00-4a26-ac95-22a3d97d4d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e3d9e8-fbf3-4ec1-b035-a3fd18255012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86df4a73-2ae2-4e1f-bfb0-19099b6e7b97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e3d9e8-fbf3-4ec1-b035-a3fd18255012",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "41c39e75-5792-470f-afbd-d4df7680c44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "70bfef34-acd1-48cd-9b7f-ab42c45ad436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c39e75-5792-470f-afbd-d4df7680c44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0c1e29-5ce9-430c-a994-7c5648880f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c39e75-5792-470f-afbd-d4df7680c44b",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "df117342-07e1-4fe3-a491-2279b84f5970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "67ba6e6f-c9ac-47e6-b696-b74ca178e66f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df117342-07e1-4fe3-a491-2279b84f5970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68149405-3223-4ca7-936b-41a66091de64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df117342-07e1-4fe3-a491-2279b84f5970",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "7e187603-bb7c-4467-9fae-9514f53ff68b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "8ad1f2fc-f3c4-4d8f-89e7-fff057d7e061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e187603-bb7c-4467-9fae-9514f53ff68b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f52570d-dede-4836-ba16-009e34192515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e187603-bb7c-4467-9fae-9514f53ff68b",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "309855a0-5ca7-4613-bac1-300fb2fda3e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "fb4ee557-135b-4e11-b048-ba39fba64161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309855a0-5ca7-4613-bac1-300fb2fda3e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b57724c9-1638-4056-a24b-0018eec30ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309855a0-5ca7-4613-bac1-300fb2fda3e7",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "83cd6f7a-8aeb-41ce-9247-321a6fdc7abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "7de6381d-8e16-4c10-a099-47c326c4d545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83cd6f7a-8aeb-41ce-9247-321a6fdc7abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1355dc1-2860-434e-963f-d84057832d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83cd6f7a-8aeb-41ce-9247-321a6fdc7abc",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "2c9832e2-5b70-4175-bd23-1eb2087d2e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f00bcb56-0d02-4f2b-99f2-a76b779db9f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9832e2-5b70-4175-bd23-1eb2087d2e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61983290-95d1-48f6-9801-5bdc8eb857df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9832e2-5b70-4175-bd23-1eb2087d2e59",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "c912ade6-65b7-4b02-b436-b635a4698ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "2dc84fe1-cd5b-4f45-bdc9-02b5e19b4802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c912ade6-65b7-4b02-b436-b635a4698ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d29b9a-c9bc-4db4-9585-30b397e104d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c912ade6-65b7-4b02-b436-b635a4698ef6",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "19bdcc78-e4c2-415f-89ad-c368136288e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "c8e004f2-ecdc-4715-9cfd-2e76f9306dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19bdcc78-e4c2-415f-89ad-c368136288e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e2c390-d0ed-47a8-b22b-5ea76629e1c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19bdcc78-e4c2-415f-89ad-c368136288e0",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "954b754a-a9d1-4c6d-81c1-8b52ee1c26a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "77a0319e-c2fd-499c-ab53-e9e7113b8115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "954b754a-a9d1-4c6d-81c1-8b52ee1c26a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044c710a-9749-4e08-b815-fd2aae2ca207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "954b754a-a9d1-4c6d-81c1-8b52ee1c26a3",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "50f987e8-a4de-424f-9ca7-c80b074752c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "1ec16aff-80d7-4bec-8421-e0e94dc7769a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f987e8-a4de-424f-9ca7-c80b074752c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8df009-63b5-4ee1-a175-96364d81aea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f987e8-a4de-424f-9ca7-c80b074752c4",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "e351c5d1-cdab-493b-b426-6a83c4c9bbed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "8a3ecbdf-aecf-4dd1-892f-3c54f6821d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e351c5d1-cdab-493b-b426-6a83c4c9bbed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2cafd7-6640-428a-91b8-5ff66e023a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e351c5d1-cdab-493b-b426-6a83c4c9bbed",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b3ec35f2-47b2-4493-bfe7-f397e42073af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4ab50ac9-3eaf-40f0-905f-33820c60275a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ec35f2-47b2-4493-bfe7-f397e42073af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bff94bc-af42-4226-b3d8-2e14377433f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ec35f2-47b2-4493-bfe7-f397e42073af",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "4d03f4c3-6395-41a6-b497-5bc631c4586e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "88db6e25-c954-4725-9052-63f6272add13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d03f4c3-6395-41a6-b497-5bc631c4586e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49f41f36-713f-4b5f-b324-c24f3873ccb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d03f4c3-6395-41a6-b497-5bc631c4586e",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b7be2f0c-a426-4ddb-ac24-e43ba7bfebb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f4293462-ea29-4998-bfd6-aa5687585c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7be2f0c-a426-4ddb-ac24-e43ba7bfebb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff9f782-c50c-4cbc-94e0-a16bc4a47483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7be2f0c-a426-4ddb-ac24-e43ba7bfebb9",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "241f3361-0ac4-45f3-b5a6-bb99bc1ccc2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "e4a192c3-9290-44d3-87ea-15a4eb64124c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241f3361-0ac4-45f3-b5a6-bb99bc1ccc2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f3774a-2a3e-49cb-9290-f73b82e5c8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241f3361-0ac4-45f3-b5a6-bb99bc1ccc2e",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "71966a2d-eba0-491b-b048-1dbc052d582c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a331e34e-7fdc-4f7c-af9f-18f65583076c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71966a2d-eba0-491b-b048-1dbc052d582c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e23a609-6b41-4e8f-80f6-442407088ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71966a2d-eba0-491b-b048-1dbc052d582c",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ae03deac-2857-4c32-b4a0-43ba3c1fce72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "05c5cf9b-f9d8-40c7-8b90-f0bd19dce703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae03deac-2857-4c32-b4a0-43ba3c1fce72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25fe6d0-9d59-4dfc-aba8-a400af10f9b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae03deac-2857-4c32-b4a0-43ba3c1fce72",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "52933f3b-004f-4c68-a869-81539f3bd7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a5385e06-6c64-4a87-a4d7-31b379ba2626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52933f3b-004f-4c68-a869-81539f3bd7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10685080-bc5b-44a8-b7bf-5e71aae11f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52933f3b-004f-4c68-a869-81539f3bd7ea",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ff4e4d9b-fcd6-43cd-8cc9-a3b6b0d5418a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d9f9f25c-e329-465e-8521-64fc3ced221a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff4e4d9b-fcd6-43cd-8cc9-a3b6b0d5418a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be8fe0c3-185f-405a-9c8e-2f2e68dc6051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff4e4d9b-fcd6-43cd-8cc9-a3b6b0d5418a",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "0814c6f0-4d35-45f1-9450-363aac3943ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "36ffcd27-429c-46f8-a158-233b18ddb90c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0814c6f0-4d35-45f1-9450-363aac3943ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "320b4807-a16c-4144-b091-82d71b3dad72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0814c6f0-4d35-45f1-9450-363aac3943ef",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "77098de5-bea9-4b91-888c-5d01ddee5e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "ffdd7eaa-745e-42c1-9d95-59932edca562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77098de5-bea9-4b91-888c-5d01ddee5e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee39d52f-a0e6-4114-8955-eac0e808b091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77098de5-bea9-4b91-888c-5d01ddee5e16",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "519b0acb-b5d1-4666-8121-adf86ddae749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f2dff058-41a8-4c69-9ddd-28148eacf4e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "519b0acb-b5d1-4666-8121-adf86ddae749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834f96ac-1b18-4804-9d0a-e5c026bb2c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "519b0acb-b5d1-4666-8121-adf86ddae749",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "4715a19d-3547-4b27-acde-5ae991ba1868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "41e688fe-6af5-4b78-b552-93f41f0ddc13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4715a19d-3547-4b27-acde-5ae991ba1868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a817f2a0-3197-4e8c-80b7-19b68313d32f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4715a19d-3547-4b27-acde-5ae991ba1868",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "68346bdb-32ad-438b-86f3-9b2f8ce3be96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "b1665f9e-23d9-4ab9-ba44-1ca7548b314c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68346bdb-32ad-438b-86f3-9b2f8ce3be96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e00cfba1-457b-415c-a4a1-1b7600b41dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68346bdb-32ad-438b-86f3-9b2f8ce3be96",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "6a1fd9e2-226a-4b87-a479-d8b3cb4b1298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4ea6b1e8-eea9-4e3a-adb0-700b084d674c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a1fd9e2-226a-4b87-a479-d8b3cb4b1298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d919b54-626f-4a89-874d-0c88032ac3da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a1fd9e2-226a-4b87-a479-d8b3cb4b1298",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "fddce974-4664-4565-845a-70952ed5771d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "2bc856e5-355c-47bf-9e0a-67a19a8f72bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fddce974-4664-4565-845a-70952ed5771d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "980a7a22-5518-4850-b9d8-7e71b30704a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fddce974-4664-4565-845a-70952ed5771d",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "46471f26-ab0a-4d9d-95bd-5f2f1836ed35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "bffdce2c-5ad6-477b-8117-e3e375a47406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46471f26-ab0a-4d9d-95bd-5f2f1836ed35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc87358-eb4e-496e-b7de-5e130e8f6799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46471f26-ab0a-4d9d-95bd-5f2f1836ed35",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "9f399d15-ad77-42e0-8109-6bc7fb966776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "b13978ea-f330-403a-b240-480e6cd41e62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f399d15-ad77-42e0-8109-6bc7fb966776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a5e977-2a11-4a8f-a671-5ed30b41252d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f399d15-ad77-42e0-8109-6bc7fb966776",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "c163921e-27ad-4073-ba4b-bbc739d82325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "66d74098-075b-4797-98bf-c22703b7ff1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c163921e-27ad-4073-ba4b-bbc739d82325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f6879c-b304-48f6-a257-0f21fa273c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c163921e-27ad-4073-ba4b-bbc739d82325",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "17928658-22b9-40d9-b6a3-cd601c43e139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "76881c6c-f6db-450e-84a0-b3d824190191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17928658-22b9-40d9-b6a3-cd601c43e139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01e2216-8967-4a6a-903d-d4d5e0321cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17928658-22b9-40d9-b6a3-cd601c43e139",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "15356d71-db16-43df-bd06-38c041a70306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a022a2f0-43b6-4814-a504-b9fde861f873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15356d71-db16-43df-bd06-38c041a70306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf4bdb6-8dc3-477f-be5d-c258872e9383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15356d71-db16-43df-bd06-38c041a70306",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "d084fad2-5099-4866-910a-92ef33a7aa67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4799889c-acc8-4de2-a0f0-03d268809f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d084fad2-5099-4866-910a-92ef33a7aa67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cc6362-13fc-4435-bf30-512f1c24562c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d084fad2-5099-4866-910a-92ef33a7aa67",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "401f0c15-d331-4f97-aece-73daefc89a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "86e2a57a-0aaa-4b86-b7b9-f82c12a92f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401f0c15-d331-4f97-aece-73daefc89a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99c92098-ef3d-47d8-8c88-c50637c416e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401f0c15-d331-4f97-aece-73daefc89a41",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "452933e0-052f-4a4b-862d-2945ae938158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d14fadf7-0313-480e-a4f3-35a29aabaf5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "452933e0-052f-4a4b-862d-2945ae938158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "031ac17e-ce8d-4829-861b-1ea5d017651b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "452933e0-052f-4a4b-862d-2945ae938158",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "47c128b4-2c25-4815-9319-898948ae7200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "12f19028-91b8-4491-9438-0b04b864729a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47c128b4-2c25-4815-9319-898948ae7200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a67eff31-14f2-4dbf-a4aa-9b869532ab62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47c128b4-2c25-4815-9319-898948ae7200",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "3d4428b8-6b77-4761-bede-6684c25b4474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "8950ab33-64bb-4466-9a96-7dabdc70a3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4428b8-6b77-4761-bede-6684c25b4474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef271ce-f6ef-48ae-ad34-09c8b4806244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4428b8-6b77-4761-bede-6684c25b4474",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "96ed0e26-8ec7-4a55-838d-f4ec286dd308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5164c18f-a4b2-4bf1-b347-59d44e784707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ed0e26-8ec7-4a55-838d-f4ec286dd308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b152abad-870c-4fda-826c-5715a8fb3161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ed0e26-8ec7-4a55-838d-f4ec286dd308",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "eacefdef-0e72-4d8e-b2bb-8b42e4a21601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "2130ae43-273c-42c3-a8f3-32b49def5126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eacefdef-0e72-4d8e-b2bb-8b42e4a21601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aedc1ae-af49-4ca4-8a0d-66fb5667d2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eacefdef-0e72-4d8e-b2bb-8b42e4a21601",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "d99dda2b-bc52-407e-8874-95b6d2fcf0dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a39f07af-20b2-4560-bee9-cc4ae6208676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99dda2b-bc52-407e-8874-95b6d2fcf0dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66e9b2f-9995-4ccd-954a-f38e2bb97289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99dda2b-bc52-407e-8874-95b6d2fcf0dd",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "6c698d9a-7b65-4ac9-96f8-aeeb41a54992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "1012a77c-2616-4406-888b-d4ec03d2864b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c698d9a-7b65-4ac9-96f8-aeeb41a54992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e752ee5-ef06-4064-914d-0cf1d4875c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c698d9a-7b65-4ac9-96f8-aeeb41a54992",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ab4f5765-00b9-4ca6-8b49-3728cc6e981d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5eb5147f-f484-4fa7-99d6-71b4be25c9ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab4f5765-00b9-4ca6-8b49-3728cc6e981d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aabc26f-3afd-4187-bdde-53207628a657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab4f5765-00b9-4ca6-8b49-3728cc6e981d",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "421968d5-6147-4b75-b0e1-c0d9279a88f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "9d56c180-e49c-4153-8612-44c44b68c941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421968d5-6147-4b75-b0e1-c0d9279a88f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b776fdb-40be-489d-8b2c-61309a8d3029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421968d5-6147-4b75-b0e1-c0d9279a88f6",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "9d678439-04da-4f93-a17b-ab41ed786e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "2d014668-2fb7-4045-bd14-6b9805ac3fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d678439-04da-4f93-a17b-ab41ed786e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff69a1da-62b6-44ba-a2be-931938655c96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d678439-04da-4f93-a17b-ab41ed786e8d",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b948ec2c-c2e4-418a-88d7-2851966e3b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "f038b405-a024-462d-9214-3d4066b40e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b948ec2c-c2e4-418a-88d7-2851966e3b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c36e0fe-55ef-4a42-87d4-25632a8358ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b948ec2c-c2e4-418a-88d7-2851966e3b68",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "50be43db-07d0-4701-b245-18772bcf5ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "60ce6207-9860-4c70-b96b-64600872a944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50be43db-07d0-4701-b245-18772bcf5ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1593187-af4e-40e1-9845-06fe4f112e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50be43db-07d0-4701-b245-18772bcf5ddd",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "90992a2e-11a0-4726-942c-586f6bbf5509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4df13975-a43c-4aeb-8b8a-4e49b90a4e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90992a2e-11a0-4726-942c-586f6bbf5509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b502c8b6-444a-4571-a888-2d2cfb56b1c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90992a2e-11a0-4726-942c-586f6bbf5509",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "a7a6c031-1f4b-430f-aca6-e65d8f2d00da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "cc17b231-82a7-4928-9d7d-060f9d787352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a6c031-1f4b-430f-aca6-e65d8f2d00da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5165b1-8349-415c-acc8-4e3966ecd1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a6c031-1f4b-430f-aca6-e65d8f2d00da",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "1ceba3d0-7e7e-401a-8580-d92e137cb8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "4a75cebd-79e0-43eb-ac74-7d4b3c9f32a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ceba3d0-7e7e-401a-8580-d92e137cb8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d05109e6-4889-48ba-9dcd-05496900d544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ceba3d0-7e7e-401a-8580-d92e137cb8b3",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "a31fc933-e2a9-46ea-9f1f-80e03eb66ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "77044d17-cceb-489c-98fd-ef2b3627826e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31fc933-e2a9-46ea-9f1f-80e03eb66ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1feb90de-dd27-4553-96db-68986cf91baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31fc933-e2a9-46ea-9f1f-80e03eb66ba0",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "993c74b0-e47d-4c7d-9cd4-5194d4798db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "7bd05628-96a8-4e43-9d05-9788b652e801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "993c74b0-e47d-4c7d-9cd4-5194d4798db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7649d1c6-7cea-4650-90ae-437e7237a4ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "993c74b0-e47d-4c7d-9cd4-5194d4798db8",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "3a60a110-ecc0-4696-81b3-db8b874f9eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "ea1061b9-a11f-4535-af1f-6686623746f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a60a110-ecc0-4696-81b3-db8b874f9eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a229fa95-e0a9-4812-9388-c9c084beaae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a60a110-ecc0-4696-81b3-db8b874f9eb7",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "d96155a9-068a-4af9-a6be-762d5796a2c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "1b5f8927-d510-4d21-a1e5-c5846457ec03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96155a9-068a-4af9-a6be-762d5796a2c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2123bdad-b394-4c2d-b90b-e64f2639775b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96155a9-068a-4af9-a6be-762d5796a2c6",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "5b5fb6be-266e-4b14-acf9-23d21e19ff4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "bdabad61-a6b5-40cd-99d4-020e81cb3a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5fb6be-266e-4b14-acf9-23d21e19ff4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0594544f-9345-4889-9808-aea460fc5e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5fb6be-266e-4b14-acf9-23d21e19ff4a",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "35d3c7fd-3fb8-4c07-8bd2-4ea238469f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "73c4b4f4-dfd8-4714-a411-49073d7fdd20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d3c7fd-3fb8-4c07-8bd2-4ea238469f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cde78b2-c0d3-486c-9349-309c09bad8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d3c7fd-3fb8-4c07-8bd2-4ea238469f0c",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "d0b07cdb-3594-4c06-9ccb-76c321f26ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d43772f2-dcd3-4b4c-8d1b-05da1cbe577f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b07cdb-3594-4c06-9ccb-76c321f26ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1654bb-94b8-4773-978d-fff971d68b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b07cdb-3594-4c06-9ccb-76c321f26ecc",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "c46ba1a5-aaf3-4915-9c82-8cfc9d555258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "ba4edf02-304c-4042-b5b7-182209a12b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46ba1a5-aaf3-4915-9c82-8cfc9d555258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "297ad9c1-2deb-4412-8dab-c3df97180b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46ba1a5-aaf3-4915-9c82-8cfc9d555258",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "bcf107fe-7351-4108-9c9e-496d2d0e1267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5532592a-20f8-462b-a7a8-e53d41d2898d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf107fe-7351-4108-9c9e-496d2d0e1267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cdf82da-3738-4b47-a353-536f0092aea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf107fe-7351-4108-9c9e-496d2d0e1267",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "f9b22fb4-0940-4bac-b50d-c3b1e4150996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "e48db174-e366-4fb9-b7ac-816593f6ab71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b22fb4-0940-4bac-b50d-c3b1e4150996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f453c2dc-3844-4912-819b-7d793917ec81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b22fb4-0940-4bac-b50d-c3b1e4150996",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "154d91ed-ecd6-4398-a684-ee82b5837e84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "656581c4-426c-4068-b290-1a5c87d359c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "154d91ed-ecd6-4398-a684-ee82b5837e84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff13b637-e0b4-4f70-8c35-e969ac4d49f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "154d91ed-ecd6-4398-a684-ee82b5837e84",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ad58b32f-e4ea-42e6-b552-43d73c5bf461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "e5d6638d-9b01-4630-a001-69e92143442c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad58b32f-e4ea-42e6-b552-43d73c5bf461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa3c6fd-9907-48de-8bf6-bf4de8b38d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad58b32f-e4ea-42e6-b552-43d73c5bf461",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "745a3eec-f14e-4467-88e5-66c1c7823875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "463b0d32-3ad0-48d0-bffe-5476aa41445b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "745a3eec-f14e-4467-88e5-66c1c7823875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03061f39-c995-40ac-af40-cd0e6c178302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "745a3eec-f14e-4467-88e5-66c1c7823875",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "e0891870-4b14-48b0-9911-243a7d140780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d23a173e-5348-420e-bd11-2c4b953933f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0891870-4b14-48b0-9911-243a7d140780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9635f52-6df1-46df-88ac-c1c7c8845d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0891870-4b14-48b0-9911-243a7d140780",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "54fb8f36-5e3a-4e37-81cf-33f95681364f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d10c050c-99b9-4b9a-addf-609ed7946bc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54fb8f36-5e3a-4e37-81cf-33f95681364f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2705ba8d-030f-42db-8bb1-65b3b09a5f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54fb8f36-5e3a-4e37-81cf-33f95681364f",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "4a6f3d55-bdf9-49ba-80a1-a731b39e34db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "908e7240-4753-4e3f-9e7b-74a854b7095d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6f3d55-bdf9-49ba-80a1-a731b39e34db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0810e2-a5f6-48fa-a270-18a55689ce82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6f3d55-bdf9-49ba-80a1-a731b39e34db",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ca71fd1e-ef1d-4454-9142-e38a262d908d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "e71a8d2d-f907-436f-aaa3-f422fcd07c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca71fd1e-ef1d-4454-9142-e38a262d908d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf4e124-0336-4e86-b3f7-3f23b9f0370f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca71fd1e-ef1d-4454-9142-e38a262d908d",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "350ed227-b74e-400f-9e9a-039ee74ec590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5840aa03-bbe8-4719-bab3-b3cdaaf2a0bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350ed227-b74e-400f-9e9a-039ee74ec590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdea0f98-4657-4f4f-a721-73d1f2df510f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350ed227-b74e-400f-9e9a-039ee74ec590",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "f0779285-aed7-4715-9da1-7278c0914b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "572a5687-8f1a-4e68-80e5-675c606159f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0779285-aed7-4715-9da1-7278c0914b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c61d7bc-4e21-4717-9de7-097461dc7412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0779285-aed7-4715-9da1-7278c0914b66",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "71fd3902-3285-495f-a936-1a80982f6677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "daca7d35-b290-4f9a-a232-78ed8e6d5dc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71fd3902-3285-495f-a936-1a80982f6677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29961300-bf09-43c0-937a-e93b78372a49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71fd3902-3285-495f-a936-1a80982f6677",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "0daa7ee1-5ead-4d3f-aef2-fa54b7a1a4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "714e6065-de03-4862-843a-4d3fc26894d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0daa7ee1-5ead-4d3f-aef2-fa54b7a1a4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d64beb-652f-4cfe-adbe-5903fd93f774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0daa7ee1-5ead-4d3f-aef2-fa54b7a1a4ee",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "f2c528f3-5269-4faf-afb1-793ad96fd98a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a035fc69-207a-4aa9-8d21-44aa591b4159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c528f3-5269-4faf-afb1-793ad96fd98a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f87e3168-5ef4-44f8-8ba6-d5617791154a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c528f3-5269-4faf-afb1-793ad96fd98a",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "6665552d-5fec-44af-a30f-0e4da585b27c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "6d11939a-d924-4da5-85ce-a088c1c4a174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6665552d-5fec-44af-a30f-0e4da585b27c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d9350f-ab36-48c2-aa78-5b2c1db86e14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6665552d-5fec-44af-a30f-0e4da585b27c",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "866f16d4-0cb8-4c54-b5ef-d7caa3796643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "a98f2afc-9f78-413d-820d-dbf4239d848e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866f16d4-0cb8-4c54-b5ef-d7caa3796643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f3b4f9e-febf-45b3-944f-71b8fe90e1f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866f16d4-0cb8-4c54-b5ef-d7caa3796643",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "b846512b-2f78-42e5-b11b-c8e32d334f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d71afd9a-a1fb-4a48-8343-ef26392e7b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b846512b-2f78-42e5-b11b-c8e32d334f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6753f4-dcf0-4e21-9d1c-94273bca1e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b846512b-2f78-42e5-b11b-c8e32d334f57",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "ca322a1d-af6e-4f6b-91e3-37372ef05f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "5554349b-a461-440a-a7df-406e46ffd0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca322a1d-af6e-4f6b-91e3-37372ef05f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08da9789-b791-4586-b750-b7b77bafa71d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca322a1d-af6e-4f6b-91e3-37372ef05f84",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "1bd9c5ad-5a57-4a3b-b5e9-416e19386115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "d889a176-f82b-4527-ae0e-2a475b1a6a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd9c5ad-5a57-4a3b-b5e9-416e19386115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f668f676-c016-45b8-bd2b-1d5a212bdcff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd9c5ad-5a57-4a3b-b5e9-416e19386115",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "cf2b2bb5-b270-4a90-bd6e-36ca79258b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "3bfdd7eb-0b03-4fc2-9832-0294a4bec001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf2b2bb5-b270-4a90-bd6e-36ca79258b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5166fc-07a2-499a-8ec5-550053f5ca38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf2b2bb5-b270-4a90-bd6e-36ca79258b72",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "45fc4995-297b-4dda-81aa-1087f8231dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "dab95467-dc42-491f-b515-52931c6dc909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45fc4995-297b-4dda-81aa-1087f8231dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d655765-c546-49c8-aec8-37179d34f63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45fc4995-297b-4dda-81aa-1087f8231dbe",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "da1e6601-950f-4a60-92e5-2ed7efc25b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "77a9dcdb-06db-4654-b13f-6feb891fdf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da1e6601-950f-4a60-92e5-2ed7efc25b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c253451-a590-49e0-9fdd-775151857797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da1e6601-950f-4a60-92e5-2ed7efc25b5f",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "9d385ffb-f2d2-46f2-8114-e8970cc57351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "27e4b02e-c4e5-482d-860c-70ab9acd8c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d385ffb-f2d2-46f2-8114-e8970cc57351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb67c7a0-99d1-42a8-af16-8340a6109be9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d385ffb-f2d2-46f2-8114-e8970cc57351",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "f2d0203f-0f78-42f7-8fe3-f3c769829a7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "bf1b741c-43ee-4a20-ae59-3c2c8df91def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d0203f-0f78-42f7-8fe3-f3c769829a7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491a944b-e5c5-4945-978d-ddf8ae45d00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d0203f-0f78-42f7-8fe3-f3c769829a7e",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "7d2c1795-38e4-438c-a2cf-2f368ca174bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "644750f6-16fe-418c-884a-c98e1e4989ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2c1795-38e4-438c-a2cf-2f368ca174bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2cb7bac-da8a-4a35-b893-a058318df689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2c1795-38e4-438c-a2cf-2f368ca174bc",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "7d76e3f1-5cca-415e-8432-c50344cbed6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "e4f81d08-70ec-4377-8e3f-639a8a692e2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d76e3f1-5cca-415e-8432-c50344cbed6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea55d83-0aa7-424c-afb6-d9ba3beab80d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d76e3f1-5cca-415e-8432-c50344cbed6a",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "bb32657f-8fb3-41da-a25a-ccc46feec9fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "b9485642-47dd-4ddf-a0a9-6ea7b0878c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb32657f-8fb3-41da-a25a-ccc46feec9fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05e11d4-e343-4726-9a95-07aa74100302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb32657f-8fb3-41da-a25a-ccc46feec9fb",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "32c43c73-67e2-494f-9ee6-03d86f0fc189",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "6da2bac4-fa62-4c05-acd8-0ee0d6d49431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c43c73-67e2-494f-9ee6-03d86f0fc189",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d9c7f0-fa60-498c-b736-bf5346e5739b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c43c73-67e2-494f-9ee6-03d86f0fc189",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "a05675b0-f410-4b7d-ab7e-17d1e2fb75f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "12a3c380-a40c-41fd-b89e-844e1fb87688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a05675b0-f410-4b7d-ab7e-17d1e2fb75f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f2a4f27-4ef5-4bb3-a3a1-6a6bc3457e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a05675b0-f410-4b7d-ab7e-17d1e2fb75f2",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        },
        {
            "id": "33e40f17-f1d1-44f3-83e5-67e379e98f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "compositeImage": {
                "id": "81b919da-9fa8-4797-bcef-1a81e601f301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e40f17-f1d1-44f3-83e5-67e379e98f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1cdd5ac-3779-457e-bc56-684f18b4f379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e40f17-f1d1-44f3-83e5-67e379e98f9c",
                    "LayerId": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "1201bc1d-8bb9-480d-8904-2eb6a2e7a87d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a8eceab-aaef-48f9-a8c8-a03db8358235",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}