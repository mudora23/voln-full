{
    "id": "53d75eb8-9777-4b17-a504-2b6d6f70b76a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_left_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e166eda-d6a6-4a6b-81bd-8b6c524ec3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53d75eb8-9777-4b17-a504-2b6d6f70b76a",
            "compositeImage": {
                "id": "29e5d1b9-2d71-4cbd-9aaf-b4eb255b604c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e166eda-d6a6-4a6b-81bd-8b6c524ec3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc214ba8-8787-4dd8-8d07-17cb4ba34e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e166eda-d6a6-4a6b-81bd-8b6c524ec3e0",
                    "LayerId": "e3dcf96f-647d-4fbd-8b9f-8292b3664cb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 145,
    "layers": [
        {
            "id": "e3dcf96f-647d-4fbd-8b9f-8292b3664cb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53d75eb8-9777-4b17-a504-2b6d6f70b76a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 26,
    "yorig": 119
}