{
    "id": "0782929e-cb71-40a8-bb6d-4bcf8aaa4af6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_violet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b214d4c1-80d7-4413-8c16-b33ac5c655d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0782929e-cb71-40a8-bb6d-4bcf8aaa4af6",
            "compositeImage": {
                "id": "7090951c-fd5f-45c4-84a6-b189dae50a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b214d4c1-80d7-4413-8c16-b33ac5c655d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b03cee-69eb-4fc9-9fd2-4acce6f9878f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b214d4c1-80d7-4413-8c16-b33ac5c655d5",
                    "LayerId": "076e86e6-bff5-4906-940d-aa526e11768d"
                }
            ]
        },
        {
            "id": "e5b08c25-42cd-4e37-b52b-f856345045a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0782929e-cb71-40a8-bb6d-4bcf8aaa4af6",
            "compositeImage": {
                "id": "9c2bb6c4-04e5-4f41-bd9b-396ea8335f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b08c25-42cd-4e37-b52b-f856345045a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0bb1468-5310-4e47-bf0b-ee8f37952aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b08c25-42cd-4e37-b52b-f856345045a5",
                    "LayerId": "076e86e6-bff5-4906-940d-aa526e11768d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "076e86e6-bff5-4906-940d-aa526e11768d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0782929e-cb71-40a8-bb6d-4bcf8aaa4af6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}