{
    "id": "022beb6e-1c8f-43d3-bb6d-5d40b0ba62f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Cave_Interior_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5b953c3-8ac7-4e9f-96c1-dddbba45872b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "022beb6e-1c8f-43d3-bb6d-5d40b0ba62f5",
            "compositeImage": {
                "id": "630ff900-e12a-477e-857b-ef1a2373fb43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b953c3-8ac7-4e9f-96c1-dddbba45872b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785a596a-8717-45ca-a5d5-33a5dec8a96a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b953c3-8ac7-4e9f-96c1-dddbba45872b",
                    "LayerId": "1a86fc39-7a54-48d7-83fd-eb1492c3b104"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "1a86fc39-7a54-48d7-83fd-eb1492c3b104",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "022beb6e-1c8f-43d3-bb6d-5d40b0ba62f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}