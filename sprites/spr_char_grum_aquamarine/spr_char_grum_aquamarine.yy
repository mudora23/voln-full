{
    "id": "c7051efd-1a60-4240-9de3-b3db4c6f7db9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_aquamarine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "833f5591-76ba-42e7-b020-e7ac857a878a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7051efd-1a60-4240-9de3-b3db4c6f7db9",
            "compositeImage": {
                "id": "f6bde998-283c-4214-b736-974dff6c4105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833f5591-76ba-42e7-b020-e7ac857a878a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30995d0-7991-4282-a57b-aba77f620968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833f5591-76ba-42e7-b020-e7ac857a878a",
                    "LayerId": "71e5f7a6-cd62-4cd9-9d83-1a04e3cbee43"
                }
            ]
        },
        {
            "id": "2749f3fe-f20c-4c61-bc92-ac54a1a889db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7051efd-1a60-4240-9de3-b3db4c6f7db9",
            "compositeImage": {
                "id": "eac78e76-c66f-41a7-a5d1-2bd74fb58834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2749f3fe-f20c-4c61-bc92-ac54a1a889db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c56e7b-f8b2-46c2-aa0b-6bb4db9a33fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2749f3fe-f20c-4c61-bc92-ac54a1a889db",
                    "LayerId": "71e5f7a6-cd62-4cd9-9d83-1a04e3cbee43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "71e5f7a6-cd62-4cd9-9d83-1a04e3cbee43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7051efd-1a60-4240-9de3-b3db4c6f7db9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}