{
    "id": "7ae31613-604a-4018-9fe8-194419513359",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Ku_n_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1721,
    "bbox_left": 128,
    "bbox_right": 991,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4a01541-6f63-4ccf-8338-411da8f1fde4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ae31613-604a-4018-9fe8-194419513359",
            "compositeImage": {
                "id": "71f540cc-05bd-40af-af04-c1c98d9c5c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a01541-6f63-4ccf-8338-411da8f1fde4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "230f64e0-6e44-4069-95de-2b31979ed805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a01541-6f63-4ccf-8338-411da8f1fde4",
                    "LayerId": "eccb9ff3-bc6f-46ed-9ffc-660e6e63c553"
                }
            ]
        },
        {
            "id": "33ee72e4-5014-430a-be1c-1826d728c7d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ae31613-604a-4018-9fe8-194419513359",
            "compositeImage": {
                "id": "7946029f-8a1e-4946-9fbb-358cb9f6bd7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ee72e4-5014-430a-be1c-1826d728c7d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "590ae529-80c9-4061-ae77-ff2e1718f223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ee72e4-5014-430a-be1c-1826d728c7d7",
                    "LayerId": "eccb9ff3-bc6f-46ed-9ffc-660e6e63c553"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1779,
    "layers": [
        {
            "id": "eccb9ff3-bc6f-46ed-9ffc-660e6e63c553",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ae31613-604a-4018-9fe8-194419513359",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1086,
    "xorig": 0,
    "yorig": 0
}