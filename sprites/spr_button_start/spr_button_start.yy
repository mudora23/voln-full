{
    "id": "103c67ea-f99e-4db7-a854-c080939e70cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2970412b-70b3-44ea-8336-d1b12ba7259d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "103c67ea-f99e-4db7-a854-c080939e70cd",
            "compositeImage": {
                "id": "cda1f0d7-6fea-4e27-855c-579fc9923ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2970412b-70b3-44ea-8336-d1b12ba7259d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed2688d9-758a-470b-aaef-7df94b313d0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2970412b-70b3-44ea-8336-d1b12ba7259d",
                    "LayerId": "7809cc55-adad-4e6b-a049-68b77f7d401f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "7809cc55-adad-4e6b-a049-68b77f7d401f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "103c67ea-f99e-4db7-a854-c080939e70cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 75
}