{
    "id": "e713a425-3ef9-4d7b-ab8e-c51f75528a35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_itemtype_food",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 3,
    "bbox_right": 58,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ded8f8e-4c45-4b5f-b7d8-090442a42789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e713a425-3ef9-4d7b-ab8e-c51f75528a35",
            "compositeImage": {
                "id": "38a21a54-8d39-49bb-bcc5-52b9016e742f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ded8f8e-4c45-4b5f-b7d8-090442a42789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c45c6f-ab61-40e9-9205-27fd2bdca55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ded8f8e-4c45-4b5f-b7d8-090442a42789",
                    "LayerId": "fdd8e79b-a6bf-40e7-9966-3ac86ad47441"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fdd8e79b-a6bf-40e7-9966-3ac86ad47441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e713a425-3ef9-4d7b-ab8e-c51f75528a35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}