{
    "id": "a5466a45-0600-4e8c-939d-9eeea012617f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_teal_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f904fa0-0321-4c8b-b2a8-dc631c33e5cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5466a45-0600-4e8c-939d-9eeea012617f",
            "compositeImage": {
                "id": "1f2517b5-dd0d-40d6-b9fd-1a8859a3ddaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f904fa0-0321-4c8b-b2a8-dc631c33e5cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2121254d-8673-42bc-b30d-f3bdc3510229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f904fa0-0321-4c8b-b2a8-dc631c33e5cd",
                    "LayerId": "b7f8ab95-f215-4cf0-8b0f-c7673340cc95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "b7f8ab95-f215-4cf0-8b0f-c7673340cc95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5466a45-0600-4e8c-939d-9eeea012617f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}