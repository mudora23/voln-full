{
    "id": "2d704fd2-34d5-4c6d-8501-7b9b8b4058d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_aquamarine_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3094922-ea52-47ba-aa19-ce44b587c851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d704fd2-34d5-4c6d-8501-7b9b8b4058d4",
            "compositeImage": {
                "id": "eabb3517-2787-4c8f-a636-08bd6a69336f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3094922-ea52-47ba-aa19-ce44b587c851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e172a0-991d-4834-b18a-5895346c9395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3094922-ea52-47ba-aa19-ce44b587c851",
                    "LayerId": "070d6d24-7f49-4e23-8bb5-2ba3a8262297"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "070d6d24-7f49-4e23-8bb5-2ba3a8262297",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d704fd2-34d5-4c6d-8501-7b9b8b4058d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}