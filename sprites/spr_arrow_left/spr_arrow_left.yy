{
    "id": "4e92f4fd-8e66-4e23-bea9-2c8694e0b2ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 3,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d95e48e-de68-43b7-a984-978a8332d07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e92f4fd-8e66-4e23-bea9-2c8694e0b2ac",
            "compositeImage": {
                "id": "3a121fbd-8551-4c41-8f0d-6a9a4b0c647e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d95e48e-de68-43b7-a984-978a8332d07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a3bdd8-ef21-4d3f-8657-f5a725114065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d95e48e-de68-43b7-a984-978a8332d07b",
                    "LayerId": "a4561fcd-574f-4350-a97c-b32bfc21a343"
                }
            ]
        },
        {
            "id": "1dfa82a6-e998-49e2-aee4-ec5de68607a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e92f4fd-8e66-4e23-bea9-2c8694e0b2ac",
            "compositeImage": {
                "id": "e67df406-3370-4ac4-8c68-e8382ae71f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfa82a6-e998-49e2-aee4-ec5de68607a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b89dea-66ce-403e-8f82-d566a00f3292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfa82a6-e998-49e2-aee4-ec5de68607a2",
                    "LayerId": "a4561fcd-574f-4350-a97c-b32bfc21a343"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "a4561fcd-574f-4350-a97c-b32bfc21a343",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e92f4fd-8e66-4e23-bea9-2c8694e0b2ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}