{
    "id": "032bb1b8-af91-4b8f-b999-606f6802b285",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Bal_r_ls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2210,
    "bbox_left": 80,
    "bbox_right": 1031,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78f8bffe-07ae-4877-ba1f-6b8c62b0c8bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "032bb1b8-af91-4b8f-b999-606f6802b285",
            "compositeImage": {
                "id": "9c335721-bb3e-42b8-aa9b-f5784675c010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f8bffe-07ae-4877-ba1f-6b8c62b0c8bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6daf5d0-5ab1-4e3e-9f52-0b86b9892958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f8bffe-07ae-4877-ba1f-6b8c62b0c8bc",
                    "LayerId": "f7ff6fb6-099b-44f9-af10-7d7fe97213c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2228,
    "layers": [
        {
            "id": "f7ff6fb6-099b-44f9-af10-7d7fe97213c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "032bb1b8-af91-4b8f-b999-606f6802b285",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1180,
    "xorig": 0,
    "yorig": 0
}