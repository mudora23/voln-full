{
    "id": "ca1d05be-ab6b-4d5b-a229-43f78f57b475",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1931,
    "bbox_left": 0,
    "bbox_right": 742,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d325848-3897-4235-848f-ca0a0e451f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca1d05be-ab6b-4d5b-a229-43f78f57b475",
            "compositeImage": {
                "id": "b4eabd35-15f0-41ce-8702-bc54ab6dcb1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d325848-3897-4235-848f-ca0a0e451f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9d81de-d200-41af-bafd-20cea340e0ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d325848-3897-4235-848f-ca0a0e451f0c",
                    "LayerId": "2520259e-709e-4a04-b79c-0270b2b62351"
                }
            ]
        },
        {
            "id": "8d2a9181-3e45-44c8-bb8d-8f64bb9273cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca1d05be-ab6b-4d5b-a229-43f78f57b475",
            "compositeImage": {
                "id": "544f7a7e-eb29-4cfb-836f-3829b65c60c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d2a9181-3e45-44c8-bb8d-8f64bb9273cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e3f10e-9ef6-4ed8-9cd6-8ed10c2525f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d2a9181-3e45-44c8-bb8d-8f64bb9273cf",
                    "LayerId": "2520259e-709e-4a04-b79c-0270b2b62351"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2004,
    "layers": [
        {
            "id": "2520259e-709e-4a04-b79c-0270b2b62351",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca1d05be-ab6b-4d5b-a229-43f78f57b475",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 762,
    "xorig": 0,
    "yorig": 0
}