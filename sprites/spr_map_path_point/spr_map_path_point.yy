{
    "id": "6e79c277-97ca-4e80-82bd-655a1a09667d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_path_point",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 13,
    "bbox_right": 23,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d99546af-6ccd-43f7-8e6b-74acb77509e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e79c277-97ca-4e80-82bd-655a1a09667d",
            "compositeImage": {
                "id": "5ea63d94-4e1e-42a4-b292-7f9bc8e6bc86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99546af-6ccd-43f7-8e6b-74acb77509e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35607ade-8356-4e49-bbfb-e9623a1c6e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99546af-6ccd-43f7-8e6b-74acb77509e2",
                    "LayerId": "eb4e4750-ae12-47dc-9915-a69f03910017"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eb4e4750-ae12-47dc-9915-a69f03910017",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e79c277-97ca-4e80-82bd-655a1a09667d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 16
}