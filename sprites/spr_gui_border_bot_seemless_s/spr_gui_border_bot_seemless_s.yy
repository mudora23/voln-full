{
    "id": "47a50edf-1276-4808-9fa5-84163330fefa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_seemless_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dcf4442-312d-41d1-8599-41894ebb2152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a50edf-1276-4808-9fa5-84163330fefa",
            "compositeImage": {
                "id": "ff33a6b8-c5ba-4d8e-8292-87120efcdc54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dcf4442-312d-41d1-8599-41894ebb2152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2312ad28-d142-4d12-a723-852e41011091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dcf4442-312d-41d1-8599-41894ebb2152",
                    "LayerId": "040303d5-58c2-462d-b002-b8cb42d7f4b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "040303d5-58c2-462d-b002-b8cb42d7f4b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47a50edf-1276-4808-9fa5-84163330fefa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 11
}