{
    "id": "f993eaf5-9597-4fa5-a898-692cdf219c0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_scroll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f69042c1-1d23-4cb1-a26e-3e9bdccca6bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f993eaf5-9597-4fa5-a898-692cdf219c0f",
            "compositeImage": {
                "id": "b0393b4f-8164-4bc9-aebd-37c34e782cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f69042c1-1d23-4cb1-a26e-3e9bdccca6bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91b2233-dc3a-4203-928d-6d4406bb32b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69042c1-1d23-4cb1-a26e-3e9bdccca6bb",
                    "LayerId": "6e6a127f-2bd4-47a4-bfef-befc389543d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "6e6a127f-2bd4-47a4-bfef-befc389543d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f993eaf5-9597-4fa5-a898-692cdf219c0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}