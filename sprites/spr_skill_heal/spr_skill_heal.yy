{
    "id": "7acda7d1-bb74-4597-9208-2d968ad848c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_heal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c1a8a09-2ba2-446c-ae8a-4a97e3ab6fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7acda7d1-bb74-4597-9208-2d968ad848c3",
            "compositeImage": {
                "id": "865289d0-81c0-4846-b104-33a6fc7807dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c1a8a09-2ba2-446c-ae8a-4a97e3ab6fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31566d72-2f3d-4932-8d55-e934b00946c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c1a8a09-2ba2-446c-ae8a-4a97e3ab6fc4",
                    "LayerId": "771fc3b1-e8df-4fa9-b450-e34612e09319"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "771fc3b1-e8df-4fa9-b450-e34612e09319",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7acda7d1-bb74-4597-9208-2d968ad848c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}