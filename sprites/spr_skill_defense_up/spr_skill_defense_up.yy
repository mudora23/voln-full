{
    "id": "17005755-840d-4bf0-a596-acb04541ba10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_defense_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 197,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70cb3a58-4a48-4177-8f7b-390bec52efa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17005755-840d-4bf0-a596-acb04541ba10",
            "compositeImage": {
                "id": "c66cd8d8-52cb-4f28-88c8-23fc32880c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70cb3a58-4a48-4177-8f7b-390bec52efa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d23a065-2530-4ea3-9e0c-2f4b33fcb420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70cb3a58-4a48-4177-8f7b-390bec52efa7",
                    "LayerId": "6ac57711-1398-48a4-b939-4b0f9d6c11d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "6ac57711-1398-48a4-b939-4b0f9d6c11d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17005755-840d-4bf0-a596-acb04541ba10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}