{
    "id": "cd907418-8476-4a00-8b85-3106bd0c5399",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_5_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91ce3fe0-5a23-47be-9cd4-66a93f6a3c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd907418-8476-4a00-8b85-3106bd0c5399",
            "compositeImage": {
                "id": "5c1ddab3-d62c-4d27-953a-7ff5ec4ad068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ce3fe0-5a23-47be-9cd4-66a93f6a3c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5cfb4a4-1900-492f-b834-74dca82651d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ce3fe0-5a23-47be-9cd4-66a93f6a3c63",
                    "LayerId": "e1453e08-decd-4d48-b35a-90ab28885c7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "e1453e08-decd-4d48-b35a-90ab28885c7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd907418-8476-4a00-8b85-3106bd0c5399",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}