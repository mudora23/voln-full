{
    "id": "b379c3e1-3875-4463-8a70-bd6adf34e60a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Zeyd_f_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1930,
    "bbox_left": 204,
    "bbox_right": 929,
    "bbox_top": 107,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbe8c97b-b4ba-49c6-904d-344ed865d5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b379c3e1-3875-4463-8a70-bd6adf34e60a",
            "compositeImage": {
                "id": "fd2b1265-6e03-43fb-a636-3c29d1917b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe8c97b-b4ba-49c6-904d-344ed865d5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf4fbef-e267-4295-8f54-2c02a97f65a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe8c97b-b4ba-49c6-904d-344ed865d5e8",
                    "LayerId": "7895d8b8-70c6-4234-b5cf-68d3b61e0978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "7895d8b8-70c6-4234-b5cf-68d3b61e0978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b379c3e1-3875-4463-8a70-bd6adf34e60a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}