{
    "id": "0a2be541-981a-445a-b5ba-1d5dec6e17d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_overlay_multiply",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 609,
    "bbox_left": 0,
    "bbox_right": 1100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ba4723d-6245-4c2b-a03f-55e0e9b9d6e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2be541-981a-445a-b5ba-1d5dec6e17d9",
            "compositeImage": {
                "id": "96ed777b-a818-4eda-8c02-64310fbbd767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba4723d-6245-4c2b-a03f-55e0e9b9d6e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b3623a8-4d4b-474a-8b8a-f8adcf421440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba4723d-6245-4c2b-a03f-55e0e9b9d6e2",
                    "LayerId": "1fd2b8c1-ac86-48f3-ab62-3364ea75b37e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 610,
    "layers": [
        {
            "id": "1fd2b8c1-ac86-48f3-ab62-3364ea75b37e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a2be541-981a-445a-b5ba-1d5dec6e17d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1101,
    "xorig": 0,
    "yorig": 0
}