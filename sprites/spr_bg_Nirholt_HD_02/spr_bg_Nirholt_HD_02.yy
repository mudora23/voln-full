{
    "id": "fcc72587-36a0-4e10-b63d-59ea8aa7e4d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_HD_02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a94620e-804e-4411-b4da-afebaaf4f0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcc72587-36a0-4e10-b63d-59ea8aa7e4d2",
            "compositeImage": {
                "id": "c1be3fb6-dd2c-4580-86c7-309924f2107b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a94620e-804e-4411-b4da-afebaaf4f0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2be613-1ed8-4773-b216-9d0e40639560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a94620e-804e-4411-b4da-afebaaf4f0cd",
                    "LayerId": "e7836b87-6b89-49ba-afd9-1aeeed123551"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "e7836b87-6b89-49ba-afd9-1aeeed123551",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcc72587-36a0-4e10-b63d-59ea8aa7e4d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}