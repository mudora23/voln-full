{
    "id": "ce1457fa-b196-4b78-81a4-968f7430fee1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_might_str",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70b86e59-c401-4932-a073-5c2090dd7c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce1457fa-b196-4b78-81a4-968f7430fee1",
            "compositeImage": {
                "id": "36fdd5c9-def2-45d2-9f2d-d02dfb0dd3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b86e59-c401-4932-a073-5c2090dd7c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aee113d-1fe3-4807-8d28-27590b4e55fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b86e59-c401-4932-a073-5c2090dd7c48",
                    "LayerId": "a81f0901-6c2c-4242-866a-44ca71fa2167"
                },
                {
                    "id": "e1b1f7fb-4f7b-4292-b048-0a6bbe2013b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b86e59-c401-4932-a073-5c2090dd7c48",
                    "LayerId": "4a06b0ca-0e3a-4579-9163-4fc0d2bdbdba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "4a06b0ca-0e3a-4579-9163-4fc0d2bdbdba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce1457fa-b196-4b78-81a4-968f7430fee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a81f0901-6c2c-4242-866a-44ca71fa2167",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce1457fa-b196-4b78-81a4-968f7430fee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}