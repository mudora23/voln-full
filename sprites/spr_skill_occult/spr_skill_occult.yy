{
    "id": "ff670d03-55e0-44a8-90be-15f49a204ef5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_occult",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f285e7b-eb5b-49d5-8670-c7745279c30a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff670d03-55e0-44a8-90be-15f49a204ef5",
            "compositeImage": {
                "id": "b8a01830-7ee9-4475-8251-c4775883dcc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f285e7b-eb5b-49d5-8670-c7745279c30a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efe2caf6-c337-4038-8219-18ef32049288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f285e7b-eb5b-49d5-8670-c7745279c30a",
                    "LayerId": "724d0035-81ed-48a2-8613-c8b06b3ed020"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "724d0035-81ed-48a2-8613-c8b06b3ed020",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff670d03-55e0-44a8-90be-15f49a204ef5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}