{
    "id": "55465734-9d5e-4840-a634-43fe1302fb05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_top_left_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c30173c-c24e-410b-bc1e-a4baf8e84b04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55465734-9d5e-4840-a634-43fe1302fb05",
            "compositeImage": {
                "id": "2c2f46f0-0bc8-481e-b04c-b0f1921c24d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c30173c-c24e-410b-bc1e-a4baf8e84b04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a042f3e-acc5-4bb8-8e96-2ea2e134ce74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c30173c-c24e-410b-bc1e-a4baf8e84b04",
                    "LayerId": "283ab3e9-8550-4133-9982-2db569646110"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "283ab3e9-8550-4133-9982-2db569646110",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55465734-9d5e-4840-a634-43fe1302fb05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 13,
    "yorig": 13
}