{
    "id": "dff6ec7a-0f63-43e0-91cf-c39dcd0ef99c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_str",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "221174a0-888a-4426-94d5-eb7adccb9d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dff6ec7a-0f63-43e0-91cf-c39dcd0ef99c",
            "compositeImage": {
                "id": "cc8fc70a-266d-4787-b847-fd531023fb44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221174a0-888a-4426-94d5-eb7adccb9d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604e7dab-a05f-4aae-97b2-113ce416c13b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221174a0-888a-4426-94d5-eb7adccb9d62",
                    "LayerId": "f712e21f-e606-474a-b7cc-745c89c2cd2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "f712e21f-e606-474a-b7cc-745c89c2cd2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dff6ec7a-0f63-43e0-91cf-c39dcd0ef99c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}