{
    "id": "aa21522f-26d7-40d5-a4bc-eb08530d1825",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_dex",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2bd6c98-caf1-4561-afe9-8c27c0c8531b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa21522f-26d7-40d5-a4bc-eb08530d1825",
            "compositeImage": {
                "id": "542c858f-5964-4041-aa0e-451b1bb35c7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2bd6c98-caf1-4561-afe9-8c27c0c8531b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c981b202-cc18-40a3-b036-4d05c8425f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2bd6c98-caf1-4561-afe9-8c27c0c8531b",
                    "LayerId": "d0bcf59b-a890-453e-95e3-9f76d98f4a95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "d0bcf59b-a890-453e-95e3-9f76d98f4a95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa21522f-26d7-40d5-a4bc-eb08530d1825",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}