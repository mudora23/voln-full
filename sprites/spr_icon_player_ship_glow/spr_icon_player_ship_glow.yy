{
    "id": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_player_ship_glow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 1,
    "bbox_right": 48,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2fd3526-fbb5-4eaf-8e5e-72b586fbe7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "e9b18afc-f8d2-48b0-94a8-4cd0d009c619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2fd3526-fbb5-4eaf-8e5e-72b586fbe7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb2a4bce-212e-4c30-8784-7cb9e16ecdfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2fd3526-fbb5-4eaf-8e5e-72b586fbe7ea",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "0386496a-cff1-4bf0-a4e6-6e50b0849666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "e3a4ea15-808d-4a2a-8cb3-238636291f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0386496a-cff1-4bf0-a4e6-6e50b0849666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd5b217-15d7-4491-a4a3-67a5944e69e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0386496a-cff1-4bf0-a4e6-6e50b0849666",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "de92b021-b292-4b43-a5cd-c89088c1d82e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "a75eb276-871b-4501-b13b-a0e40b27c446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de92b021-b292-4b43-a5cd-c89088c1d82e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af1faae-0ac5-4271-a1c5-48cc5a0b5114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de92b021-b292-4b43-a5cd-c89088c1d82e",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "795b62ff-d6b6-4f23-9169-239f4266589a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "911635e8-1dbe-485c-891f-333b3b5408c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "795b62ff-d6b6-4f23-9169-239f4266589a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e2055d-5527-4448-b590-682a541d5968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "795b62ff-d6b6-4f23-9169-239f4266589a",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "8810a311-0d65-4880-a5f4-b09cf6eb73b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "d26fac71-1f3b-4164-a822-e21113901531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8810a311-0d65-4880-a5f4-b09cf6eb73b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c33e1e8-893b-46b2-9842-27df671c6bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8810a311-0d65-4880-a5f4-b09cf6eb73b8",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "f578196f-3481-41c0-a8ae-386b00500f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "ae3ea4d3-8c1d-45a0-9620-54f2621707d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f578196f-3481-41c0-a8ae-386b00500f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34995024-96f3-4b37-92e7-da3264459079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f578196f-3481-41c0-a8ae-386b00500f72",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "dd768e26-175e-44f9-b84a-cdaac38a3e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "41c8958e-ab59-4bfc-af48-fdd8dbdf57e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd768e26-175e-44f9-b84a-cdaac38a3e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efaebd9-9067-47d7-a48e-c4691f06624a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd768e26-175e-44f9-b84a-cdaac38a3e88",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "666f2415-bab9-45ff-bedd-cd933bfab896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "e5396b4c-6592-48f2-8f20-7ff0852d76b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666f2415-bab9-45ff-bedd-cd933bfab896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a26b40a-2cc5-4cbf-aaa9-448ed18cf820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666f2415-bab9-45ff-bedd-cd933bfab896",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "4f61fa69-71f4-4873-8d0f-e688310014bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "315ad613-5bfd-4d1e-8629-53f7676d58eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f61fa69-71f4-4873-8d0f-e688310014bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21332da0-6c91-4836-a79a-fad86afe2362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f61fa69-71f4-4873-8d0f-e688310014bc",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "a33d8690-e8d6-486d-a755-77f38ae98503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "8be980c3-3692-478e-a4ce-b4ca1f73b0ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33d8690-e8d6-486d-a755-77f38ae98503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b1b37cd-4b23-4f89-a273-a063bdd89c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33d8690-e8d6-486d-a755-77f38ae98503",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "b4837942-7ae7-45e7-8856-d702985da1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "60fcc21d-d912-418e-b387-b041bf88ca76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4837942-7ae7-45e7-8856-d702985da1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f64967d-6dc3-4b20-b8be-45b15307b0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4837942-7ae7-45e7-8856-d702985da1ea",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "ab7a1179-67e5-47cb-81f4-532ce9f1f9b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "6398a8c6-5759-4642-82c2-81d679601baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab7a1179-67e5-47cb-81f4-532ce9f1f9b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2585cc8-0537-4fd9-842e-c356b91d7b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab7a1179-67e5-47cb-81f4-532ce9f1f9b9",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "c6a808b1-28d3-48c6-9fe4-5c85f1c8956c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "175591c1-42ff-4fe2-adfe-51bf34ecd446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a808b1-28d3-48c6-9fe4-5c85f1c8956c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba39e3e3-1cad-4b82-a7ce-c3fcb9948de0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a808b1-28d3-48c6-9fe4-5c85f1c8956c",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "1a795cd8-cc49-4376-a40c-57f821b275a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "5303fd72-8879-4154-b79a-680beb38f735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a795cd8-cc49-4376-a40c-57f821b275a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "389a27fe-347c-4d9d-9116-bc57ba6d6b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a795cd8-cc49-4376-a40c-57f821b275a2",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "e559bf99-c536-4fd9-b1d6-65739d5621c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "c8c114b3-5b63-452a-b81c-9aa42f33471a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e559bf99-c536-4fd9-b1d6-65739d5621c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6736de51-d17e-4223-995e-4e2857e0ad05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e559bf99-c536-4fd9-b1d6-65739d5621c4",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "4cdb2a16-2274-4600-9d9b-0fc69d77c7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "ce696990-9d95-4d87-81d7-fa5698b047a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cdb2a16-2274-4600-9d9b-0fc69d77c7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129e1071-9308-41c6-99f4-c9d7e9364af1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdb2a16-2274-4600-9d9b-0fc69d77c7d2",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "60224266-b4ae-4ea4-8834-c7d5420cbe13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "645e7450-55f5-418f-a24e-578a1c5340e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60224266-b4ae-4ea4-8834-c7d5420cbe13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d49b70-814f-45d2-b54a-5f8366375083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60224266-b4ae-4ea4-8834-c7d5420cbe13",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "d236b394-859e-4f65-ac73-f1d22e3d3e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "adcde74a-ea6c-408d-a8b6-4b99c1a70bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d236b394-859e-4f65-ac73-f1d22e3d3e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec3db30-fa73-4d58-9e0f-f217bca7bb54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d236b394-859e-4f65-ac73-f1d22e3d3e2d",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "ca61420c-b017-4095-bde5-bff622520ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "aaee7354-691c-45ea-b7bb-a3baf8e424fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca61420c-b017-4095-bde5-bff622520ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eec69b9-6232-4694-96bd-dda6c64408c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca61420c-b017-4095-bde5-bff622520ac1",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "d6193fbd-7338-4be6-97f3-fac489ecc82f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "cfc569d6-ed6e-43a2-aff3-5f2ea3b9a415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6193fbd-7338-4be6-97f3-fac489ecc82f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e15eca0-5339-4288-9c95-05b89ba2c90c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6193fbd-7338-4be6-97f3-fac489ecc82f",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "9892827b-2480-469c-beb3-2eeb6be11944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "19371d52-120e-47e3-88c3-558dba4362ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9892827b-2480-469c-beb3-2eeb6be11944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38ad9bd-5834-4149-9d50-e406b874d22c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9892827b-2480-469c-beb3-2eeb6be11944",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "c4db052d-c34c-43bc-a691-42ba8207845c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "f8567aed-6c09-43c9-9e2d-337fbcddaef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4db052d-c34c-43bc-a691-42ba8207845c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a76669-7b56-40c5-9a41-c5e8bf040b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4db052d-c34c-43bc-a691-42ba8207845c",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "0debec40-552a-491c-84af-e53674fd734f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "c3b53e24-7589-4473-a191-cb5922d520d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0debec40-552a-491c-84af-e53674fd734f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "010ad8f5-2019-47f1-a574-b8827b626282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0debec40-552a-491c-84af-e53674fd734f",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        },
        {
            "id": "6e342c4a-0d47-4e13-b0f7-6bdc2f9118b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "compositeImage": {
                "id": "ae85f797-f996-42b7-a96d-875edf5a093d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e342c4a-0d47-4e13-b0f7-6bdc2f9118b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a9e34c-1d2c-4b99-9724-bd00e3f3ef01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e342c4a-0d47-4e13-b0f7-6bdc2f9118b4",
                    "LayerId": "307d219a-7506-48bd-b2c7-bdf124521727"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "307d219a-7506-48bd-b2c7-bdf124521727",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4f9cbc9-dc00-4881-970d-d920e97c64c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 35
}