{
    "id": "3fa3eddb-feff-47be-bb8c-e496ed0743a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_yellowgreen_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3b67bcd-17cc-43fb-8507-9c64a93cc996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fa3eddb-feff-47be-bb8c-e496ed0743a3",
            "compositeImage": {
                "id": "cf4d6d75-2537-44c3-94ba-1ac6556b159d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b67bcd-17cc-43fb-8507-9c64a93cc996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49d9184-b12b-445e-98e3-282ff996455c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b67bcd-17cc-43fb-8507-9c64a93cc996",
                    "LayerId": "68375d34-5e6c-4019-9f9c-c9599a52c0b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "68375d34-5e6c-4019-9f9c-c9599a52c0b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fa3eddb-feff-47be-bb8c-e496ed0743a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}