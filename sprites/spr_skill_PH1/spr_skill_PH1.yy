{
    "id": "99c1fb06-0c4d-48ab-965a-e3cb1db6055f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_PH1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "562e6ab7-96b8-4e9e-83fb-ff77893277ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99c1fb06-0c4d-48ab-965a-e3cb1db6055f",
            "compositeImage": {
                "id": "00d3e168-c29f-4c62-a909-ece7c557af1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "562e6ab7-96b8-4e9e-83fb-ff77893277ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "196dface-4b13-4899-9e5f-21a265a73b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "562e6ab7-96b8-4e9e-83fb-ff77893277ba",
                    "LayerId": "4086af43-6bd5-49b2-9323-241810559c84"
                },
                {
                    "id": "830cd9e9-234f-4c79-92e4-d867c79cc277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "562e6ab7-96b8-4e9e-83fb-ff77893277ba",
                    "LayerId": "2d2671d5-3b71-4e6e-902b-ab7ae9f8aafe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "4086af43-6bd5-49b2-9323-241810559c84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99c1fb06-0c4d-48ab-965a-e3cb1db6055f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2d2671d5-3b71-4e6e-902b-ab7ae9f8aafe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99c1fb06-0c4d-48ab-965a-e3cb1db6055f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}