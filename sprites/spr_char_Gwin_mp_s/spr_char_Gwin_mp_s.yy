{
    "id": "cf00f61e-43f2-40d9-8332-345015684533",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Gwin_mp_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1950,
    "bbox_left": 21,
    "bbox_right": 1158,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87dc9e12-5914-473b-be0f-659c43cc66af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf00f61e-43f2-40d9-8332-345015684533",
            "compositeImage": {
                "id": "6b48b2b9-a280-49b2-9399-70eea24e8334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87dc9e12-5914-473b-be0f-659c43cc66af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4002fd76-0db2-41c5-b481-9b234b54d1df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87dc9e12-5914-473b-be0f-659c43cc66af",
                    "LayerId": "42b5670c-7491-4c80-8143-f84ca1fd8085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "42b5670c-7491-4c80-8143-f84ca1fd8085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf00f61e-43f2-40d9-8332-345015684533",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1244,
    "xorig": 0,
    "yorig": 0
}