{
    "id": "721d773c-6bb7-4c87-a23b-54158b6e4a2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Yim_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1985,
    "bbox_left": 153,
    "bbox_right": 1546,
    "bbox_top": 99,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce432a00-eeb7-425b-b340-67b78a3ef052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "721d773c-6bb7-4c87-a23b-54158b6e4a2d",
            "compositeImage": {
                "id": "7a4cea75-5459-42a0-99fd-bda38d76f4ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce432a00-eeb7-425b-b340-67b78a3ef052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c24778f7-5874-4be3-a15f-b58e8e99d27c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce432a00-eeb7-425b-b340-67b78a3ef052",
                    "LayerId": "d7ab6303-dfe2-4814-b155-0ae7cfffaf83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2058,
    "layers": [
        {
            "id": "d7ab6303-dfe2-4814-b155-0ae7cfffaf83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "721d773c-6bb7-4c87-a23b-54158b6e4a2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1781,
    "xorig": 0,
    "yorig": 0
}