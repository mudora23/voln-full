{
    "id": "e55bb344-b12e-4a4e-9537-b23e565db673",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_right_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "426d6192-1e7b-402a-8fb3-4f32da5b3327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e55bb344-b12e-4a4e-9537-b23e565db673",
            "compositeImage": {
                "id": "9af2619a-5e9e-4baf-8474-ae1710d04904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426d6192-1e7b-402a-8fb3-4f32da5b3327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591c9903-7b47-4703-b369-a7409f6978c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426d6192-1e7b-402a-8fb3-4f32da5b3327",
                    "LayerId": "57e466e0-e67a-437a-8a37-291321c27ccb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "57e466e0-e67a-437a-8a37-291321c27ccb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e55bb344-b12e-4a4e-9537-b23e565db673",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 11,
    "yorig": 18
}