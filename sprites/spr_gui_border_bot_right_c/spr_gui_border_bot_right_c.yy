{
    "id": "7e59cdb5-d216-4ce8-bf15-3f7abdd8a511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_right_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77dcd84d-2db8-4b42-82df-1821169ac793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e59cdb5-d216-4ce8-bf15-3f7abdd8a511",
            "compositeImage": {
                "id": "e36a7350-aacf-47d7-8bbd-00ebda788002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77dcd84d-2db8-4b42-82df-1821169ac793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b229a42-8ad1-4bf8-b9e6-4a6fb77ecddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77dcd84d-2db8-4b42-82df-1821169ac793",
                    "LayerId": "15022c6a-cbd7-42e1-bc2c-81eadac65c5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 145,
    "layers": [
        {
            "id": "15022c6a-cbd7-42e1-bc2c-81eadac65c5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e59cdb5-d216-4ce8-bf15-3f7abdd8a511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 119,
    "yorig": 119
}