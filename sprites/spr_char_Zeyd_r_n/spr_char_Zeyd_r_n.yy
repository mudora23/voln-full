{
    "id": "215226a7-8acf-4500-b71e-1a486326c375",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Zeyd_r_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1930,
    "bbox_left": 204,
    "bbox_right": 929,
    "bbox_top": 107,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "110ba2f9-ad9e-4f4d-9281-2fefe4ba6547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "215226a7-8acf-4500-b71e-1a486326c375",
            "compositeImage": {
                "id": "13054be1-ea39-4171-a4f2-806a1f9ec938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "110ba2f9-ad9e-4f4d-9281-2fefe4ba6547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5507e9fd-f777-4d2b-a170-bf3e3e4eccda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "110ba2f9-ad9e-4f4d-9281-2fefe4ba6547",
                    "LayerId": "665bee7e-1b37-4220-927b-9df13c3d841f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "665bee7e-1b37-4220-927b-9df13c3d841f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "215226a7-8acf-4500-b71e-1a486326c375",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}