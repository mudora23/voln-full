{
    "id": "680d2eee-7b11-494e-8278-e110997db7f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_anon_male_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1779,
    "bbox_left": 147,
    "bbox_right": 944,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06522555-6d13-4ee6-9784-650ba69b3209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "680d2eee-7b11-494e-8278-e110997db7f4",
            "compositeImage": {
                "id": "6013d6bb-0d0a-488c-acab-fc90c4f695ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06522555-6d13-4ee6-9784-650ba69b3209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe59cfb-7a10-4fc6-8e94-acec1a1f7934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06522555-6d13-4ee6-9784-650ba69b3209",
                    "LayerId": "f97afa58-4992-4f44-a1ab-2135e4b5ffe4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1780,
    "layers": [
        {
            "id": "f97afa58-4992-4f44-a1ab-2135e4b5ffe4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "680d2eee-7b11-494e-8278-e110997db7f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1110,
    "xorig": 0,
    "yorig": 0
}