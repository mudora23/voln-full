{
    "id": "b0c5f999-b279-4236-a905-23726225fe5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_seemless_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b598c22-182a-4ebe-9997-d631f2ccaa47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0c5f999-b279-4236-a905-23726225fe5d",
            "compositeImage": {
                "id": "3ef92461-0aec-4aa2-875d-14f778dfc414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b598c22-182a-4ebe-9997-d631f2ccaa47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b13322-c92c-4658-9240-cd7e30c9090d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b598c22-182a-4ebe-9997-d631f2ccaa47",
                    "LayerId": "36781027-234e-4f1a-8557-ecd60d1761a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "36781027-234e-4f1a-8557-ecd60d1761a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0c5f999-b279-4236-a905-23726225fe5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 6
}