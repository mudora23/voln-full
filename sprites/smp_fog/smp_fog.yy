{
    "id": "4b121e67-c963-415f-a120-3a0deb1a419f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "smp_fog",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 125,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8b601b9-e9d2-4a5e-9db8-07645a1e0073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b121e67-c963-415f-a120-3a0deb1a419f",
            "compositeImage": {
                "id": "e1130eec-75e3-41d2-a53d-0ea22857e49e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8b601b9-e9d2-4a5e-9db8-07645a1e0073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6a6cf4-7455-4934-a771-27344c4ff0d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8b601b9-e9d2-4a5e-9db8-07645a1e0073",
                    "LayerId": "fcd369ff-d874-49ee-b0fc-3336f76608e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "fcd369ff-d874-49ee-b0fc-3336f76608e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b121e67-c963-415f-a120-3a0deb1a419f",
            "blendMode": 1,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}