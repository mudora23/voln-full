{
    "id": "b6bb0bd8-551f-4679-84b5-1e98df142bd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_right_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 0,
    "bbox_right": 115,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ec68909-883e-4987-9f6e-098888f4b8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6bb0bd8-551f-4679-84b5-1e98df142bd4",
            "compositeImage": {
                "id": "916ec8f2-48e1-4375-a173-176173b88da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec68909-883e-4987-9f6e-098888f4b8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c84e0a-b95b-4e6a-80cc-51f6095afe8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec68909-883e-4987-9f6e-098888f4b8b7",
                    "LayerId": "2fbdfd9a-3410-4ec1-bd34-29499565d407"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "2fbdfd9a-3410-4ec1-bd34-29499565d407",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6bb0bd8-551f-4679-84b5-1e98df142bd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 94,
    "yorig": 21
}