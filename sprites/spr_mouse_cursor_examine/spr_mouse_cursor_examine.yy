{
    "id": "82050861-3bfc-4a4a-be3d-569d0ca94eec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse_cursor_examine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 7,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ae36508-f955-47d6-b55f-99cdf2ec4034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82050861-3bfc-4a4a-be3d-569d0ca94eec",
            "compositeImage": {
                "id": "71f8a47d-7607-45bc-aac4-1978dad8a6d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae36508-f955-47d6-b55f-99cdf2ec4034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7786593c-da87-4f32-ba26-1de97803c864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae36508-f955-47d6-b55f-99cdf2ec4034",
                    "LayerId": "7c839b8e-8f76-4dfa-9d4d-1f5b339b3cbc"
                }
            ]
        },
        {
            "id": "5a11ff88-06b1-40df-b25d-a7e28c98ae37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82050861-3bfc-4a4a-be3d-569d0ca94eec",
            "compositeImage": {
                "id": "76edd20f-2fb6-4b27-93cb-89970f2e7ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a11ff88-06b1-40df-b25d-a7e28c98ae37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ed6e0d-74ca-4480-9897-21111309960a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a11ff88-06b1-40df-b25d-a7e28c98ae37",
                    "LayerId": "7c839b8e-8f76-4dfa-9d4d-1f5b339b3cbc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "7c839b8e-8f76-4dfa-9d4d-1f5b339b3cbc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82050861-3bfc-4a4a-be3d-569d0ca94eec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 12,
    "yorig": 79
}