{
    "id": "8d0e0aa5-b845-4554-b8ad-fb740d3357ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_mini_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56b44a04-02a0-42d9-827c-58c59bce5829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d0e0aa5-b845-4554-b8ad-fb740d3357ea",
            "compositeImage": {
                "id": "53b53614-8e8f-436b-88dd-487570d29776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56b44a04-02a0-42d9-827c-58c59bce5829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee7f1c2-6694-477c-a479-7cfad1b4fcbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56b44a04-02a0-42d9-827c-58c59bce5829",
                    "LayerId": "fa0dc3af-4387-46c2-844b-cdc1fc3da6de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "fa0dc3af-4387-46c2-844b-cdc1fc3da6de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d0e0aa5-b845-4554-b8ad-fb740d3357ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}