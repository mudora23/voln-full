{
    "id": "535afa04-2407-4734-9cc4-b34f76fe8691",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_speed_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 180,
    "bbox_left": 1,
    "bbox_right": 199,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f936bb-04d1-499a-bdec-fae9c983e3e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "535afa04-2407-4734-9cc4-b34f76fe8691",
            "compositeImage": {
                "id": "6143a097-b2f1-46c6-bd45-9624f230f17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f936bb-04d1-499a-bdec-fae9c983e3e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe80e2f0-2f15-4ed4-b7d4-fc28ce9a5070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f936bb-04d1-499a-bdec-fae9c983e3e6",
                    "LayerId": "3a2e32f1-2dff-4dda-950d-8ec9db0a345a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "3a2e32f1-2dff-4dda-950d-8ec9db0a345a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "535afa04-2407-4734-9cc4-b34f76fe8691",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}