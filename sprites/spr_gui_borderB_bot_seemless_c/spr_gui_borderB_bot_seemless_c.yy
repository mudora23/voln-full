{
    "id": "f92fd6e2-7010-4958-ad38-a0aec0ab93a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_bot_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad2bc668-9e75-46ba-bdec-5d372d7a9688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f92fd6e2-7010-4958-ad38-a0aec0ab93a8",
            "compositeImage": {
                "id": "083054ea-310e-49dc-8ff2-970613d91c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2bc668-9e75-46ba-bdec-5d372d7a9688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a6902e-82b0-479d-a72a-8b26f876181f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2bc668-9e75-46ba-bdec-5d372d7a9688",
                    "LayerId": "0d3a6e07-374b-43dc-8135-58b1f695af0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "0d3a6e07-374b-43dc-8135-58b1f695af0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f92fd6e2-7010-4958-ad38-a0aec0ab93a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 3
}