{
    "id": "0f71b165-ea0c-443c-ba76-c376ca979745",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_thunderstorm1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 10,
    "bbox_right": 89,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d5951e-5d07-46a7-9b2b-9e893e1e6d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f71b165-ea0c-443c-ba76-c376ca979745",
            "compositeImage": {
                "id": "e133551b-a2b6-43b6-a14a-f0649290adeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d5951e-5d07-46a7-9b2b-9e893e1e6d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "971a3792-9fb2-4367-a0a6-cb7bd945f445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d5951e-5d07-46a7-9b2b-9e893e1e6d72",
                    "LayerId": "9f356542-ba55-4538-b943-c595fe5c700a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9f356542-ba55-4538-b943-c595fe5c700a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f71b165-ea0c-443c-ba76-c376ca979745",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}