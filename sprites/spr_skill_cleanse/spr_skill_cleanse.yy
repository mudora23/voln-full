{
    "id": "ae04aece-e885-4cfe-a799-7dbf43ed92ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_cleanse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 194,
    "bbox_left": 5,
    "bbox_right": 194,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03faafb8-709c-46a1-b076-fdcfb7d2dd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae04aece-e885-4cfe-a799-7dbf43ed92ac",
            "compositeImage": {
                "id": "0f18f5d3-5a19-4009-802c-334576772889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03faafb8-709c-46a1-b076-fdcfb7d2dd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ad4cbe1-34f4-45bd-8646-0c6add42ff60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03faafb8-709c-46a1-b076-fdcfb7d2dd19",
                    "LayerId": "b068b3cc-ec5d-4fc7-9090-2713f52e48e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "b068b3cc-ec5d-4fc7-9090-2713f52e48e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae04aece-e885-4cfe-a799-7dbf43ed92ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}