{
    "id": "50836aaa-1641-4eed-8ef2-da0b6a2831cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_top_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6ca830e-cb1e-4b2b-a761-c097cbb709e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50836aaa-1641-4eed-8ef2-da0b6a2831cf",
            "compositeImage": {
                "id": "961c50e4-ed27-41ac-b0bd-cddb5151758d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ca830e-cb1e-4b2b-a761-c097cbb709e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c35b6a75-8f75-4850-85ee-f5befaf3785f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ca830e-cb1e-4b2b-a761-c097cbb709e8",
                    "LayerId": "3ecbe8c8-dc3b-4cb7-84a6-eca7ac253c98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "3ecbe8c8-dc3b-4cb7-84a6-eca7ac253c98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50836aaa-1641-4eed-8ef2-da0b6a2831cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 19,
    "yorig": 11
}