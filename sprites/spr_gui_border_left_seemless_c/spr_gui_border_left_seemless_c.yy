{
    "id": "5421b02f-bd62-4b98-960c-027fa9217129",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_left_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59db22b2-ce51-4dc0-9ea3-c0461d15b7ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5421b02f-bd62-4b98-960c-027fa9217129",
            "compositeImage": {
                "id": "b4337677-69b2-49ac-b162-ea27c79c4d87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59db22b2-ce51-4dc0-9ea3-c0461d15b7ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0b4345-9cfb-4819-893c-5d3650791707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59db22b2-ce51-4dc0-9ea3-c0461d15b7ac",
                    "LayerId": "56ee34cd-9d3a-4128-afb1-b8349492d810"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "56ee34cd-9d3a-4128-afb1-b8349492d810",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5421b02f-bd62-4b98-960c-027fa9217129",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 6,
    "yorig": 19
}