{
    "id": "9b10a253-b5ce-4863-8aa7-e6a7ab56212a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_2b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1970,
    "bbox_left": 0,
    "bbox_right": 1214,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1ed68e9-ca41-4ba7-92db-165011bba6c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b10a253-b5ce-4863-8aa7-e6a7ab56212a",
            "compositeImage": {
                "id": "693adc44-544d-45d7-a0f8-5e9aaa07f643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ed68e9-ca41-4ba7-92db-165011bba6c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7d40cd-590a-43cc-be65-6e4490ad39f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ed68e9-ca41-4ba7-92db-165011bba6c5",
                    "LayerId": "ade30924-8591-4dc1-b3b6-411a376d7699"
                }
            ]
        },
        {
            "id": "9df17b01-3cf0-429f-b0f0-5b634bc6f471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b10a253-b5ce-4863-8aa7-e6a7ab56212a",
            "compositeImage": {
                "id": "f29f5cf8-e87a-4979-8894-b5a2a20b2440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df17b01-3cf0-429f-b0f0-5b634bc6f471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c48d8f37-72f4-44df-b869-93293e130aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df17b01-3cf0-429f-b0f0-5b634bc6f471",
                    "LayerId": "ade30924-8591-4dc1-b3b6-411a376d7699"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1971,
    "layers": [
        {
            "id": "ade30924-8591-4dc1-b3b6-411a376d7699",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b10a253-b5ce-4863-8aa7-e6a7ab56212a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1219,
    "xorig": 0,
    "yorig": 0
}