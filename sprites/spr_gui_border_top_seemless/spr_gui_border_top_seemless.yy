{
    "id": "46a3f8c3-f18c-4149-ad30-22a29428ac7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_seemless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1bb5fc4-d3c2-4a27-81f9-a302a0d9ce79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a3f8c3-f18c-4149-ad30-22a29428ac7e",
            "compositeImage": {
                "id": "d4bfbed7-6fe6-4ed0-a1e0-df7ede0cc8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1bb5fc4-d3c2-4a27-81f9-a302a0d9ce79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f6d74f6-6d53-4c97-9392-06244611b3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1bb5fc4-d3c2-4a27-81f9-a302a0d9ce79",
                    "LayerId": "b833e241-d954-4bcc-b06c-d6036361686b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "b833e241-d954-4bcc-b06c-d6036361686b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46a3f8c3-f18c-4149-ad30-22a29428ac7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 7
}