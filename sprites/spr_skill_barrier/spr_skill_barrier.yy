{
    "id": "d1477fcf-704f-4b26-a4f1-1801fe56af7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_barrier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 198,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea066058-36ea-4729-b457-b4356fff5b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1477fcf-704f-4b26-a4f1-1801fe56af7c",
            "compositeImage": {
                "id": "582f4c20-d361-4783-b493-07cbf8f6e1c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea066058-36ea-4729-b457-b4356fff5b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9acd686-f541-4198-821f-737b080e4f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea066058-36ea-4729-b457-b4356fff5b5b",
                    "LayerId": "084a7ea9-a7cd-4e92-8255-aa323556b380"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "084a7ea9-a7cd-4e92-8255-aa323556b380",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1477fcf-704f-4b26-a4f1-1801fe56af7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}