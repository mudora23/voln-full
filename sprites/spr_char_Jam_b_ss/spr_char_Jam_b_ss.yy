{
    "id": "873f5a90-ee2a-496b-8683-c00c126530f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Jam_b_ss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1829,
    "bbox_left": 327,
    "bbox_right": 737,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1719d4c4-6b51-436a-a7a5-1091d7e01eb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873f5a90-ee2a-496b-8683-c00c126530f5",
            "compositeImage": {
                "id": "c3331544-fc97-4365-80e3-2ffc2458cbc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1719d4c4-6b51-436a-a7a5-1091d7e01eb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4b362b-cf8e-4165-9b59-80be693eae00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1719d4c4-6b51-436a-a7a5-1091d7e01eb9",
                    "LayerId": "41271676-2cc1-4581-b879-9566427130a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1902,
    "layers": [
        {
            "id": "41271676-2cc1-4581-b879-9566427130a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "873f5a90-ee2a-496b-8683-c00c126530f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1060,
    "xorig": 0,
    "yorig": 0
}