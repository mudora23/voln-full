{
    "id": "afe989f9-9eb4-4de7-bd43-73bd6b7a6ed5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_redpink_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9f91ddb-3660-42e5-803c-709476b0e1d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe989f9-9eb4-4de7-bd43-73bd6b7a6ed5",
            "compositeImage": {
                "id": "ac7c2609-6479-4d5f-a98e-b618b84f6e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f91ddb-3660-42e5-803c-709476b0e1d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e43edc-9a15-4bde-a9da-9a8378a99db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f91ddb-3660-42e5-803c-709476b0e1d8",
                    "LayerId": "fb60b499-60a5-46d1-955b-19f292e43b91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "fb60b499-60a5-46d1-955b-19f292e43b91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afe989f9-9eb4-4de7-bd43-73bd6b7a6ed5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}