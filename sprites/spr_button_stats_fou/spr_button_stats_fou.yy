{
    "id": "d4e53255-3763-4fff-9ac0-4ca8bdb40f14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_fou",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94439c9b-0801-4816-84b0-7d0f9da69412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4e53255-3763-4fff-9ac0-4ca8bdb40f14",
            "compositeImage": {
                "id": "39763f1d-4f99-4f40-ad30-2d49b7aed2d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94439c9b-0801-4816-84b0-7d0f9da69412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecab9722-59a7-4171-9069-6dc2322ad912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94439c9b-0801-4816-84b0-7d0f9da69412",
                    "LayerId": "1578e075-0bd6-4170-a904-dc183e700337"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "1578e075-0bd6-4170-a904-dc183e700337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4e53255-3763-4fff-9ac0-4ca8bdb40f14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}