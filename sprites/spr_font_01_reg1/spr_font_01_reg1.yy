{
    "id": "7c6b16b6-2034-4689-89f0-5bf972927026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_reg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a113bb1a-563d-4598-8b6b-31e5dfcd4911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7b00e0f3-c5dd-4441-86c3-045c4e6f463a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a113bb1a-563d-4598-8b6b-31e5dfcd4911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658073b2-76bb-40ae-8191-a8d324751884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a113bb1a-563d-4598-8b6b-31e5dfcd4911",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "13824077-d2b3-4f6d-9d8b-2ba38f3cef9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a113bb1a-563d-4598-8b6b-31e5dfcd4911",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "dd247df6-9aa5-4d99-bc6f-08d3e714304f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "427bdaf9-70fd-4653-836c-30c4f58d7916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd247df6-9aa5-4d99-bc6f-08d3e714304f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a31a65-0b2d-474b-a73e-a74caca60be4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd247df6-9aa5-4d99-bc6f-08d3e714304f",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "83e90e0c-36af-4f75-9844-f23f8601a46b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd247df6-9aa5-4d99-bc6f-08d3e714304f",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "88106929-ebab-4e3a-b52d-67ac1777366d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "078ffccc-80aa-49aa-abdc-d88c39643350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88106929-ebab-4e3a-b52d-67ac1777366d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7be3a6-37b5-4910-9371-29f99b264289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88106929-ebab-4e3a-b52d-67ac1777366d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "18b69e80-1189-4519-8a67-17c4efb2e58b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88106929-ebab-4e3a-b52d-67ac1777366d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "01c436c6-4871-4fc2-905b-8e09a12c5b79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "04554f29-a4c2-4b65-ac39-6ee1abf695f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c436c6-4871-4fc2-905b-8e09a12c5b79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822ca3f2-4ae7-4f08-9fb3-f79a82ec0b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c436c6-4871-4fc2-905b-8e09a12c5b79",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d8fa2b05-4f03-4224-b876-edf19681c6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c436c6-4871-4fc2-905b-8e09a12c5b79",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7ad59721-5e19-4cb5-94a1-e0d7fc488f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "6e8541de-ef5f-44fb-8531-390035bf506e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad59721-5e19-4cb5-94a1-e0d7fc488f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4214c64f-0c80-400c-bd2f-2676f8578182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad59721-5e19-4cb5-94a1-e0d7fc488f79",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "30a5b9fb-2259-4995-9829-d419152c7f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad59721-5e19-4cb5-94a1-e0d7fc488f79",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "12e0194f-066d-4ce0-9c0d-e72302afd201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "b3372dae-0350-4156-a528-83c1eb601f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e0194f-066d-4ce0-9c0d-e72302afd201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b3585d-327d-44eb-8c94-853b4ab35b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e0194f-066d-4ce0-9c0d-e72302afd201",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d3dd6df5-9927-4bee-8fb1-feb90e191ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e0194f-066d-4ce0-9c0d-e72302afd201",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "de30d11b-c65a-409d-b333-960fee408008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c84a6264-75f3-43ce-a50f-3bd6f015cc8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de30d11b-c65a-409d-b333-960fee408008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86ba941-9160-45e4-a949-79cae069a57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de30d11b-c65a-409d-b333-960fee408008",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "a46f54e4-0e0a-470c-ab06-bcc4a7c9e9b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de30d11b-c65a-409d-b333-960fee408008",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "ab85612e-f705-40b1-b621-ae4234d7cd0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "9da79559-f0f3-462a-a857-b035cde9e5a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab85612e-f705-40b1-b621-ae4234d7cd0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bac6052-1d4f-4f50-9508-e987007bd9d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab85612e-f705-40b1-b621-ae4234d7cd0c",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "757e1655-1f7f-4f3f-b22f-b0c71608a714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab85612e-f705-40b1-b621-ae4234d7cd0c",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7921bf49-13bf-4fda-ba26-0d1a07d4d67b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "066a0654-ebd2-48d8-8dd3-2875070c24e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7921bf49-13bf-4fda-ba26-0d1a07d4d67b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76faed01-290e-4c73-80ce-1aadde3e12d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7921bf49-13bf-4fda-ba26-0d1a07d4d67b",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "12645dcc-f6b5-489f-a8cc-63d11f23dfee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7921bf49-13bf-4fda-ba26-0d1a07d4d67b",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "66c2317b-582b-42ea-91b5-f0ecb0ee35ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "308142a7-5a28-4e51-9635-9b1543b67cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c2317b-582b-42ea-91b5-f0ecb0ee35ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e189ddf-f0a4-40ac-8e87-eab3ebf4d04f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c2317b-582b-42ea-91b5-f0ecb0ee35ea",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "01125199-25a1-44d5-8ca0-f53497779bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c2317b-582b-42ea-91b5-f0ecb0ee35ea",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "ee5634de-6995-41f1-81e6-165dfd7b1631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7d28be07-fafe-45f6-929c-342b8e5bf0d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee5634de-6995-41f1-81e6-165dfd7b1631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9635b6d8-49db-4f29-b959-304946a0a341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee5634de-6995-41f1-81e6-165dfd7b1631",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "17109c07-8e40-4679-9119-1e33dda55b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee5634de-6995-41f1-81e6-165dfd7b1631",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e2625a52-44fc-4a57-84c2-3554db228299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "5977f7f9-ad30-4f16-8ae3-486ac13dfe61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2625a52-44fc-4a57-84c2-3554db228299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3a82b6-4ce3-4fb7-9ee1-46a3404dffef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2625a52-44fc-4a57-84c2-3554db228299",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "68cfcb40-835a-4e24-b3ba-6d8f01fbcd71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2625a52-44fc-4a57-84c2-3554db228299",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b5292e6e-7773-4246-b4fb-a6f396e42b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "6d30f05a-7d41-4f06-9b7f-6c24529b2052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5292e6e-7773-4246-b4fb-a6f396e42b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969de627-560d-4235-8b21-847454447eba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5292e6e-7773-4246-b4fb-a6f396e42b31",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "a7c6ef98-9865-4a5d-88cf-474362437692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5292e6e-7773-4246-b4fb-a6f396e42b31",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "c215a2f8-8e8a-4a75-ae21-9bf23d618bf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7489624c-84c9-46a9-8a09-f276503abedd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c215a2f8-8e8a-4a75-ae21-9bf23d618bf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a6f218-60c6-4d6c-b2f3-396dbd473bc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c215a2f8-8e8a-4a75-ae21-9bf23d618bf3",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "60c3cfa4-289f-4bfa-bd73-d77759879fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c215a2f8-8e8a-4a75-ae21-9bf23d618bf3",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b02b475d-04ad-45e0-a338-c955e9505b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7913ae1f-ae48-49ac-9933-979ac5a1052f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b02b475d-04ad-45e0-a338-c955e9505b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd947ef4-1ead-464f-ac47-c50c59e5ccff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b02b475d-04ad-45e0-a338-c955e9505b0f",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0a98a22b-c961-4485-852f-d3a688c4364d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b02b475d-04ad-45e0-a338-c955e9505b0f",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "56273a75-c508-4c80-b6fb-1dadbaa798c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "65d56d36-ac34-4837-8b95-1ba3820c9d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56273a75-c508-4c80-b6fb-1dadbaa798c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564ccf2b-cfa9-45a9-8b21-e74e1beb7c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56273a75-c508-4c80-b6fb-1dadbaa798c5",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "be5682f6-e369-442f-a70b-85946a95c254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56273a75-c508-4c80-b6fb-1dadbaa798c5",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "56ec1756-d785-48b3-8e4c-8a0f74609b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "3b6f6689-e269-4d4b-ab29-a6b16c0229a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ec1756-d785-48b3-8e4c-8a0f74609b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4110c76-76fa-4699-914e-fa43d586601e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ec1756-d785-48b3-8e4c-8a0f74609b1d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "668d1db3-06c7-4adf-93c9-46c17d72a658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ec1756-d785-48b3-8e4c-8a0f74609b1d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "019525f8-1918-451d-811a-6d9116404381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "5b629cd9-ab4c-4c2e-9cce-4823f6f2ae40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019525f8-1918-451d-811a-6d9116404381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802d284e-7074-4920-b7ad-02d7cab9e4ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019525f8-1918-451d-811a-6d9116404381",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "753a481e-32d4-45a3-815f-6a703517e5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019525f8-1918-451d-811a-6d9116404381",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "1610cafb-f1ee-4125-924e-178588e09da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "3726b60d-de68-4cc3-8530-fc0561307971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1610cafb-f1ee-4125-924e-178588e09da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04e25609-ec76-4642-b1b2-1b6b91399be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1610cafb-f1ee-4125-924e-178588e09da4",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "54be64b7-b410-4fbb-9255-481b21767e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1610cafb-f1ee-4125-924e-178588e09da4",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "c1ed460a-5101-4a36-97b8-b4c05295e157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "30766907-4ce2-420b-b549-2c3622ffbc18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ed460a-5101-4a36-97b8-b4c05295e157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205ab9a4-ffe9-4903-b34c-954483aeab72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ed460a-5101-4a36-97b8-b4c05295e157",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "fac70511-a327-44ae-9741-373baffb1480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ed460a-5101-4a36-97b8-b4c05295e157",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "d879c8f3-0088-4c20-be3e-80c4abf5523e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0d82982b-c81e-4ab2-8d2d-df9845eed484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d879c8f3-0088-4c20-be3e-80c4abf5523e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4a77ea-ed10-4fca-b71b-acd84ab1a4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d879c8f3-0088-4c20-be3e-80c4abf5523e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c2b04e6e-f9b7-4516-a792-08a72e9833ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d879c8f3-0088-4c20-be3e-80c4abf5523e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "f119fd54-5040-49e6-8b15-8bb70410c83e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "70a405ea-5d20-43dc-9d0e-0e8a2662ca0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f119fd54-5040-49e6-8b15-8bb70410c83e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1999a93a-8360-4c41-9a57-8d6974a5fb5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f119fd54-5040-49e6-8b15-8bb70410c83e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "3d2065fa-783c-4da6-9498-b3020ece55d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f119fd54-5040-49e6-8b15-8bb70410c83e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "772e4e4d-1484-41f9-b47b-f2cf370a7045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "1b7be1bf-4bab-446d-b0b7-131d9bd1c664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772e4e4d-1484-41f9-b47b-f2cf370a7045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "146b66b9-7291-42a9-ab89-7f7115314db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772e4e4d-1484-41f9-b47b-f2cf370a7045",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "ca73bf52-ea8d-45b5-80a1-dbb686202812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772e4e4d-1484-41f9-b47b-f2cf370a7045",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "5de39eef-fc44-40a9-b546-d2ebbd0910af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "416137dc-1d60-4cff-a2fc-bcf3f748cee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de39eef-fc44-40a9-b546-d2ebbd0910af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "384d56b2-ca7a-4023-a1a9-6433e0c5ed5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de39eef-fc44-40a9-b546-d2ebbd0910af",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "77a91727-0b5b-4a14-bf97-7c5ef07307af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de39eef-fc44-40a9-b546-d2ebbd0910af",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7800f10e-47a2-4336-98d5-333aca6bb026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "b704ab76-3b83-4076-857f-1648c47bcbb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7800f10e-47a2-4336-98d5-333aca6bb026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc2b68e5-ef86-43f5-aea7-41d5f4cc76be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7800f10e-47a2-4336-98d5-333aca6bb026",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c6707506-4d51-4068-9b2f-a86e04208db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7800f10e-47a2-4336-98d5-333aca6bb026",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "51aa7394-19ae-404e-91d4-600939159300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "db938670-4a67-4b2a-83d2-403b32f4c1b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51aa7394-19ae-404e-91d4-600939159300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c3f77b-8d57-45a4-a3a5-7576ecf38b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51aa7394-19ae-404e-91d4-600939159300",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "7f85b131-c516-42de-95fd-8b7cf0712935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51aa7394-19ae-404e-91d4-600939159300",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "5a0e69f9-cb23-4ef4-9bfb-d98aed4a1eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "bc807302-8e83-4cbe-b58a-8ff25114b2b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0e69f9-cb23-4ef4-9bfb-d98aed4a1eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d3e9f6-cd5f-4a75-a5a4-3587f73ca5ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0e69f9-cb23-4ef4-9bfb-d98aed4a1eb1",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "2134a712-dfde-4652-a97e-1dd6960f0267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0e69f9-cb23-4ef4-9bfb-d98aed4a1eb1",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "8f0704a3-da56-48ed-a8e5-488639dc2af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "ca4d0b10-d42c-4915-89e9-5d35fefe6a32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0704a3-da56-48ed-a8e5-488639dc2af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078feb97-b1df-4ced-8460-7ad7fc6d0b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0704a3-da56-48ed-a8e5-488639dc2af5",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d64bc100-e566-4b84-aee5-08e202983968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0704a3-da56-48ed-a8e5-488639dc2af5",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "60d2f8ee-0846-40e5-beda-b76fdd47f58e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "d152e9c3-ceee-4a20-bb1e-d6ccdc01e96a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d2f8ee-0846-40e5-beda-b76fdd47f58e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb9d1c6-98b8-4f73-a79b-4b5822b2ed65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d2f8ee-0846-40e5-beda-b76fdd47f58e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "1ad51099-8fa7-4a88-b739-dd34ffe171e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d2f8ee-0846-40e5-beda-b76fdd47f58e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a0c13987-8faa-4363-aa61-884e57219b7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "af002e33-fffa-4366-88fb-639613a5a137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c13987-8faa-4363-aa61-884e57219b7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47418d27-6af4-4f89-8120-3a9d36568033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c13987-8faa-4363-aa61-884e57219b7c",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0d852d79-40a2-4231-902c-b641c60936b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c13987-8faa-4363-aa61-884e57219b7c",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e2aa927b-a335-421f-80ec-13853647e495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "72ab4f9b-97e6-42b6-a326-05ff48ea3829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2aa927b-a335-421f-80ec-13853647e495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b97b226-73e6-4831-b9c3-d0cdccec28dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2aa927b-a335-421f-80ec-13853647e495",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0e223fb6-f046-4593-880b-501533397e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2aa927b-a335-421f-80ec-13853647e495",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "d18a00ae-1759-4b78-a4a6-1147f7d984c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c5a014fa-bc4f-48df-9b5c-9373243ef9a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18a00ae-1759-4b78-a4a6-1147f7d984c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553447f4-584c-42ee-8111-a6c062dba189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18a00ae-1759-4b78-a4a6-1147f7d984c5",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "5a57acd2-ca57-43cd-a154-e152a4149306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18a00ae-1759-4b78-a4a6-1147f7d984c5",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "3801a27d-468b-4bb5-be14-5ad1afd1ad46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c2ddeaeb-dff5-4e8d-a370-31369de73674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3801a27d-468b-4bb5-be14-5ad1afd1ad46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b67ce28-198b-492a-8d15-877893c7cc71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3801a27d-468b-4bb5-be14-5ad1afd1ad46",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "bda1486f-9ff5-422f-8cf4-afa42e72e51a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3801a27d-468b-4bb5-be14-5ad1afd1ad46",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "3299c07c-8896-4c8b-ad84-eba2b7f7ac16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c305ffff-014c-47de-bb6f-73538cebf57d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3299c07c-8896-4c8b-ad84-eba2b7f7ac16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6a9419a-c415-4bcc-9cd3-5795e0020d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3299c07c-8896-4c8b-ad84-eba2b7f7ac16",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "2f470b2e-52ef-4792-9684-363d8a3ec69f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3299c07c-8896-4c8b-ad84-eba2b7f7ac16",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "082ae334-f5db-4d12-b221-5406d99caf6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f63c1b42-56e4-434e-8f7a-3861fdb54d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082ae334-f5db-4d12-b221-5406d99caf6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1771960-5fc5-45c1-b52b-8be85e492bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082ae334-f5db-4d12-b221-5406d99caf6a",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "4b2478ae-7553-49a7-a09b-3512c389775f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082ae334-f5db-4d12-b221-5406d99caf6a",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e0551b35-05b5-4233-bd5c-7169f1d3d91b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "ce19af04-7856-42c7-99ed-1efddeff0d1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0551b35-05b5-4233-bd5c-7169f1d3d91b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af7c077-905c-455a-915f-80a9b9993ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0551b35-05b5-4233-bd5c-7169f1d3d91b",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "6ae925c0-4302-4e05-9513-ad1fe20fbaac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0551b35-05b5-4233-bd5c-7169f1d3d91b",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "11d0a5eb-e4d0-4b60-87f5-3ebe1428c021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "dd9311be-dea6-4b3f-95cb-c0f3930c28df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11d0a5eb-e4d0-4b60-87f5-3ebe1428c021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e40c493c-66b9-48e7-b584-ef3e8045d93a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d0a5eb-e4d0-4b60-87f5-3ebe1428c021",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "faf2f185-40b3-4a4e-bb19-5a8f1e79e371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d0a5eb-e4d0-4b60-87f5-3ebe1428c021",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "0f58171a-3ce5-453a-ae8b-bfefa7eefabb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0618044f-067c-4ddb-bba8-e74b68e82f74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f58171a-3ce5-453a-ae8b-bfefa7eefabb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a63d8a03-d082-411b-89fd-8c2ad5b66949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f58171a-3ce5-453a-ae8b-bfefa7eefabb",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "79c9833b-23a6-4860-ab8b-d135a444cd3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f58171a-3ce5-453a-ae8b-bfefa7eefabb",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "5f7f9f84-a9bf-4cec-805f-98a32d2e5cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0a751c56-aebb-408e-a9b1-bf18a6e9532b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7f9f84-a9bf-4cec-805f-98a32d2e5cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed835b88-f072-476e-a2b3-e77400879541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7f9f84-a9bf-4cec-805f-98a32d2e5cd8",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "1e890c59-4077-4a69-9bfa-63a9326f4f4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7f9f84-a9bf-4cec-805f-98a32d2e5cd8",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a402ec9a-0a38-41d7-a4f6-6fed9b594d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "1e31b794-06ea-4ca0-a53e-2a3052e9ab6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a402ec9a-0a38-41d7-a4f6-6fed9b594d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff60280-507f-496b-9fa4-27109b30c9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a402ec9a-0a38-41d7-a4f6-6fed9b594d30",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "20470923-3d7a-46e5-9b3e-bd50ac925dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a402ec9a-0a38-41d7-a4f6-6fed9b594d30",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "321f78ad-9ed4-4d45-84e3-06b1ef7360de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "9c995476-fb61-4429-ba4e-04d675d7f465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321f78ad-9ed4-4d45-84e3-06b1ef7360de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e807a38-c96b-42fd-b8b3-5176c4c7986f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321f78ad-9ed4-4d45-84e3-06b1ef7360de",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "1a4a23a4-701a-40d2-973f-bdd1e9d5c04a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321f78ad-9ed4-4d45-84e3-06b1ef7360de",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9598cd77-c51d-4635-921d-e0d2f0ee5f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "a4b33c7c-71e1-44dc-aeb3-8c111291740b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9598cd77-c51d-4635-921d-e0d2f0ee5f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ba0154-b747-4839-855f-0a3e804a6b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9598cd77-c51d-4635-921d-e0d2f0ee5f14",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "de02b48e-67d5-492d-ae59-bd0ec45cc363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9598cd77-c51d-4635-921d-e0d2f0ee5f14",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "6df1685f-1bd0-43c3-b3b6-aa902d828f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c452da3d-a6c5-4d66-80ab-3092e8f54c28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df1685f-1bd0-43c3-b3b6-aa902d828f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c1d995-cfb1-4bbb-848e-4242f15fb2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df1685f-1bd0-43c3-b3b6-aa902d828f2f",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "170d73c6-8f9a-433d-8eca-a9d0bb76080b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df1685f-1bd0-43c3-b3b6-aa902d828f2f",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "565f081e-01a7-4675-ac2c-54a40919773a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "2d9e4f09-8fac-4287-836f-68ec318cd33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565f081e-01a7-4675-ac2c-54a40919773a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "488ffcfd-c23b-44d3-ac3b-637e27fb7948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565f081e-01a7-4675-ac2c-54a40919773a",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "1128d840-936f-45ae-b245-8e6f87a60251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565f081e-01a7-4675-ac2c-54a40919773a",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7ee4e00a-a177-490e-94b6-552d863f0a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "5fd3b157-1721-419a-9122-77248ae86377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ee4e00a-a177-490e-94b6-552d863f0a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5e37f6-a2a4-4262-88bd-b7be2028f827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee4e00a-a177-490e-94b6-552d863f0a8b",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "6ad86e1b-bc7e-47c3-8346-26f2c095dca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee4e00a-a177-490e-94b6-552d863f0a8b",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "1f03f974-6609-441f-8912-94b93696e5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "8031a07e-dc08-453b-b830-98732fa733eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f03f974-6609-441f-8912-94b93696e5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3321e942-9010-4dd0-ae7f-f56d7d3459a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f03f974-6609-441f-8912-94b93696e5d9",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "65d3eff0-923d-4193-8edc-5019f6371103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f03f974-6609-441f-8912-94b93696e5d9",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "fa3a7053-b8a7-4698-8370-56a86869209f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "79cc3575-be18-4980-a52d-3bcadb8eb5c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa3a7053-b8a7-4698-8370-56a86869209f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f61fd77-b3ec-4bfa-8a01-16e944ca940e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa3a7053-b8a7-4698-8370-56a86869209f",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0f3794e2-f2e7-493d-a2a0-ed5bd0027976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa3a7053-b8a7-4698-8370-56a86869209f",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "26c13e75-6e06-4627-908e-8aa1fe049b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "72a59478-c42e-4020-8cd0-4525c9f681b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c13e75-6e06-4627-908e-8aa1fe049b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7720a5c9-bc4c-455b-9bd8-74ba41ba5a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c13e75-6e06-4627-908e-8aa1fe049b1f",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "517c0140-f2c8-4316-a9da-dac982ccca96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c13e75-6e06-4627-908e-8aa1fe049b1f",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e7fd9a4f-f54b-4a39-bb8d-233549f25c9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0594df10-6344-4d28-ba82-71e98383a506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7fd9a4f-f54b-4a39-bb8d-233549f25c9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354bc046-15f1-4373-b613-6d55274ad1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fd9a4f-f54b-4a39-bb8d-233549f25c9d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "5d3daeb9-1b1a-4a5b-a121-c28407bcd623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fd9a4f-f54b-4a39-bb8d-233549f25c9d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "dc2df3dc-c072-43d0-a25f-5399a377685e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "bbc16529-52a4-4bc7-9aef-be65a9958aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc2df3dc-c072-43d0-a25f-5399a377685e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "069f5a1d-2ef7-4951-a64d-5123490171b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2df3dc-c072-43d0-a25f-5399a377685e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "52fe3a7f-3b30-4bbf-ac38-443f3b30737b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2df3dc-c072-43d0-a25f-5399a377685e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "cb83f0ab-c5f1-4fa4-99b6-e41994d83214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7b3cdda9-1dbc-4549-b700-896435a3396c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb83f0ab-c5f1-4fa4-99b6-e41994d83214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e81b76f-ee28-42c6-a221-2db89a21360d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb83f0ab-c5f1-4fa4-99b6-e41994d83214",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "2649bce2-1abf-444a-bb03-50e064699fc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb83f0ab-c5f1-4fa4-99b6-e41994d83214",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e4158118-04e0-434b-8209-2f28f0ef7da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "ea938ed6-e818-4c8f-9c9d-b68793b49f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4158118-04e0-434b-8209-2f28f0ef7da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6eccaa1-a47e-4dcb-a28c-1560c0af7518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4158118-04e0-434b-8209-2f28f0ef7da3",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "ac7fb3ea-5b1d-435f-a894-f537da8fc3d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4158118-04e0-434b-8209-2f28f0ef7da3",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "4fc3c77b-a901-47cf-8fef-0208c796a2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "97b4c066-57ed-4064-ab97-bc25f0df1773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fc3c77b-a901-47cf-8fef-0208c796a2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4fe12b6-08bd-4793-b219-3bc4210eb824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc3c77b-a901-47cf-8fef-0208c796a2d8",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "cae3a539-6880-4c50-bc96-dab9cdc1cf68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc3c77b-a901-47cf-8fef-0208c796a2d8",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "d67392f2-1f9e-4458-a401-0eea3890d054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "ee083ac3-5731-4970-ad7a-b7ef3b17fbc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d67392f2-1f9e-4458-a401-0eea3890d054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06315fa-bbb2-4833-af81-ee7628c80b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d67392f2-1f9e-4458-a401-0eea3890d054",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "a207678e-475d-4492-b1d6-4d47fa24b935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d67392f2-1f9e-4458-a401-0eea3890d054",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "fedf8bea-3048-4cb2-9f55-8ccde3107bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "3a2879a3-458a-4659-b30a-179ace4ed487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedf8bea-3048-4cb2-9f55-8ccde3107bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d1f470-cc3f-4608-9517-bf73cec2897e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedf8bea-3048-4cb2-9f55-8ccde3107bda",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "24d8bf7a-40df-4b6f-afc4-2abf69ec2c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedf8bea-3048-4cb2-9f55-8ccde3107bda",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9de0e88b-730b-4ade-af89-2597e5997263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "80fd34bf-8b84-44e6-81d6-f9b917a0abf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de0e88b-730b-4ade-af89-2597e5997263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984c9c2e-d469-4550-a607-44683080bf62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de0e88b-730b-4ade-af89-2597e5997263",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "8129286f-d5c5-4074-838e-93a6403cbf36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de0e88b-730b-4ade-af89-2597e5997263",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b238e951-769c-4314-b9a1-97548df83fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "5c964fe7-e419-4437-ae75-b56fa5789e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b238e951-769c-4314-b9a1-97548df83fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8786ba9-04c3-475c-92fe-19094532f447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b238e951-769c-4314-b9a1-97548df83fef",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d6b5167c-55b9-4b5c-8507-11fcf59b6f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b238e951-769c-4314-b9a1-97548df83fef",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "80bf2637-b942-4023-9b50-b5f80156c984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "887a43e8-ee08-468e-9560-eb4a6eadd6ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80bf2637-b942-4023-9b50-b5f80156c984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7f3f9a-52d3-49ef-9dcc-3d5ab6c8dc88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80bf2637-b942-4023-9b50-b5f80156c984",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "b7fb64e8-b2e8-4474-8f29-76924fc3adb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80bf2637-b942-4023-9b50-b5f80156c984",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "12455b86-8417-4541-9542-03da05de0abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "a789e6b7-6226-49a9-9f7a-e0704bc5f5bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12455b86-8417-4541-9542-03da05de0abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8207227b-1621-4fb4-b1c5-8cb73923afde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12455b86-8417-4541-9542-03da05de0abd",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "4c1f7180-f300-4502-99f8-844f86ea656b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12455b86-8417-4541-9542-03da05de0abd",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a7475495-0b9c-49c4-82e5-8b2d2fe76c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f0bf8e96-0a8c-4c2a-ac14-2bee88a7f60e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7475495-0b9c-49c4-82e5-8b2d2fe76c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9fe4404-08b1-490b-909f-f6b5de7f717b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7475495-0b9c-49c4-82e5-8b2d2fe76c98",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "9ab95ccc-2606-42c8-8597-e780830034e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7475495-0b9c-49c4-82e5-8b2d2fe76c98",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "69a95f18-a5fc-4067-a994-c0bb1693d0d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "fc8d3516-00c2-457c-981d-ff1aa7c38f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a95f18-a5fc-4067-a994-c0bb1693d0d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ab0e35-1a35-4908-9675-a0ba62b19209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a95f18-a5fc-4067-a994-c0bb1693d0d9",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "3dd4db83-336b-4c93-ae50-efb1896a401e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a95f18-a5fc-4067-a994-c0bb1693d0d9",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "c64b3fa3-c246-44da-95bb-be6d6a174f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "ba247b4d-ac42-4650-bfa0-736f75b1329d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64b3fa3-c246-44da-95bb-be6d6a174f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f76f8c75-9c26-4ed7-84f0-38535cdde2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64b3fa3-c246-44da-95bb-be6d6a174f50",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "119563cd-b84c-43a9-a2f4-750763d9380b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64b3fa3-c246-44da-95bb-be6d6a174f50",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "688dc681-69ab-4c30-bbce-9125ba5d1fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7307cc25-5a9e-4303-b9f3-d68b64d45ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688dc681-69ab-4c30-bbce-9125ba5d1fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec65bae-b4de-4bcf-b3e2-ec4edff4c3d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688dc681-69ab-4c30-bbce-9125ba5d1fea",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "dd9ee96c-c491-4d8e-b00f-bb954e2db7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688dc681-69ab-4c30-bbce-9125ba5d1fea",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "908801a7-6442-47b2-aece-97dbcc0b3d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "4179b44d-d2fc-4864-8664-e7b39e10b5e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "908801a7-6442-47b2-aece-97dbcc0b3d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b480ac2-13e6-48b1-a5f3-968278da885c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908801a7-6442-47b2-aece-97dbcc0b3d22",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "01bb7344-b365-4233-ac12-901b7d59d364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908801a7-6442-47b2-aece-97dbcc0b3d22",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9ad473f9-bf99-4f5e-82a8-ad6277732f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "2043d169-f645-4988-83c1-b5c36de28396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad473f9-bf99-4f5e-82a8-ad6277732f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1dc9f21-d3cd-4741-a49b-97e45478d2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad473f9-bf99-4f5e-82a8-ad6277732f6d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "2491efbf-cb7e-491d-894e-6052484fc2a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad473f9-bf99-4f5e-82a8-ad6277732f6d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "5260992e-e69b-4dab-896a-28fd4258795d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "79317832-bbd2-4991-a30c-1e40121ade6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5260992e-e69b-4dab-896a-28fd4258795d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42354a7c-7b43-4a2e-b803-4e7eb4d4699b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5260992e-e69b-4dab-896a-28fd4258795d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "96967e9d-51d2-436f-a7f3-53bbe90754f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5260992e-e69b-4dab-896a-28fd4258795d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7787d3b1-d1b3-4dd8-b368-0418522a390d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "33aa8601-1eeb-462e-bfcf-45387d4b0c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7787d3b1-d1b3-4dd8-b368-0418522a390d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119d07fd-487a-4b2d-858a-848f77adab84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7787d3b1-d1b3-4dd8-b368-0418522a390d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "dfdb442b-c9ad-470f-bb97-0952205c5ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7787d3b1-d1b3-4dd8-b368-0418522a390d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "76c9ffea-2ab1-4ccd-b19d-d0c40d9ad0a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "d4bdee08-cc93-4728-9b19-0d8b3e7687da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c9ffea-2ab1-4ccd-b19d-d0c40d9ad0a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4203004b-41a2-4569-b255-6d1061d035b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c9ffea-2ab1-4ccd-b19d-d0c40d9ad0a3",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "e3f4596f-85e0-437d-88ef-e4aa5c83223a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c9ffea-2ab1-4ccd-b19d-d0c40d9ad0a3",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a25ba341-0dfd-433c-81e9-20af30fd3313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "80cb8339-8108-44d0-9fb2-7c28c9f9ca94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25ba341-0dfd-433c-81e9-20af30fd3313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675df8cc-c3ff-4d13-84c5-77952cace74f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25ba341-0dfd-433c-81e9-20af30fd3313",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "b4c8f147-4c62-401e-acff-3e77b4f4c008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25ba341-0dfd-433c-81e9-20af30fd3313",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7ae6e0ae-34c1-484a-a4ff-d0874b5119d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "afb99c53-bcd9-4d92-8cbf-1b568222043c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae6e0ae-34c1-484a-a4ff-d0874b5119d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73543bf-a307-44b5-8dc0-715abbc31167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae6e0ae-34c1-484a-a4ff-d0874b5119d4",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "24bb377f-a46c-4b3e-ab12-ca9644616165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae6e0ae-34c1-484a-a4ff-d0874b5119d4",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "1457ddb9-bed4-4c2d-8994-475da21df6e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "b69a2424-b430-4199-8549-8c55ddcc4d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1457ddb9-bed4-4c2d-8994-475da21df6e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3a276b-b220-4a1f-9980-6b48bb11ade8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1457ddb9-bed4-4c2d-8994-475da21df6e8",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c1e450fa-52c8-48ed-b694-9370210213d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1457ddb9-bed4-4c2d-8994-475da21df6e8",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "0dc13333-e42b-469e-bd39-ab786f177a84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "3da0a0b1-b23f-4afc-a57c-28c013f20ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc13333-e42b-469e-bd39-ab786f177a84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cb6933d-9f04-4636-869e-c3ef9c808d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc13333-e42b-469e-bd39-ab786f177a84",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "b7671c71-e70b-49d9-bdad-c035ebf84f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc13333-e42b-469e-bd39-ab786f177a84",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9467a6a5-50b8-4257-82d3-d6d6830b9c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0e5e5728-9922-4bde-a57a-3261da30ec77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9467a6a5-50b8-4257-82d3-d6d6830b9c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78164cc3-e506-4d9f-bcd3-88a7993131df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9467a6a5-50b8-4257-82d3-d6d6830b9c87",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "8e59120e-40dc-49c0-8f2b-1c0ef52ef8a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9467a6a5-50b8-4257-82d3-d6d6830b9c87",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "71545cc1-220d-4fa6-a904-62f37bc8a2de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "e36c7667-3e05-4004-a356-29e10cce71af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71545cc1-220d-4fa6-a904-62f37bc8a2de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c80cb6a3-e52d-4dd8-8a6d-89a4b457c599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71545cc1-220d-4fa6-a904-62f37bc8a2de",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "241c4e8e-d371-41f4-bfef-87408a87581b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71545cc1-220d-4fa6-a904-62f37bc8a2de",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e2eb0df7-4bb8-493b-acc9-3f98689a483e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "25df975b-0886-4fc4-bbec-c79873a6cff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2eb0df7-4bb8-493b-acc9-3f98689a483e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0fff9f6-1808-48be-af81-66a71339c403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2eb0df7-4bb8-493b-acc9-3f98689a483e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "5c5dbc92-6b91-4a89-8110-5f27b3532b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2eb0df7-4bb8-493b-acc9-3f98689a483e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b2a68491-2137-4cb8-b248-3017b7e73a31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "95febc05-0f14-4805-bd0f-495bc450dd21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a68491-2137-4cb8-b248-3017b7e73a31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89dfeab3-386f-400b-90a7-997ec3668d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a68491-2137-4cb8-b248-3017b7e73a31",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "fec71e9a-7148-4c44-843b-27d20f4e9d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a68491-2137-4cb8-b248-3017b7e73a31",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "cba56a22-318a-46e1-961f-342e6fa1ef61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "e52148c7-e496-4d1e-a0f8-a1359a5855b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba56a22-318a-46e1-961f-342e6fa1ef61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c235e8c-e11f-4c0a-8dc9-3901a4cab55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba56a22-318a-46e1-961f-342e6fa1ef61",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "405e40ca-2f92-4df2-98a4-1b1299822bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba56a22-318a-46e1-961f-342e6fa1ef61",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "df1582ad-9d29-4a55-aed0-26571f9523d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0c1f9de3-62d3-4182-aeb2-67cfb6a9fea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df1582ad-9d29-4a55-aed0-26571f9523d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d261bbb-2c0d-43df-a9c9-23f11ac18bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df1582ad-9d29-4a55-aed0-26571f9523d9",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "293357c2-26a2-4faa-b25a-a18e7ed1638e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df1582ad-9d29-4a55-aed0-26571f9523d9",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "ae264fdc-69de-43d1-a32d-d597cec022e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7b799eef-f870-47a6-8474-f6d5957b9f85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae264fdc-69de-43d1-a32d-d597cec022e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ff65f7-3c3d-4c47-a7ff-630442130b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae264fdc-69de-43d1-a32d-d597cec022e0",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "15f046df-f3c7-4900-b37f-56de5a3154eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae264fdc-69de-43d1-a32d-d597cec022e0",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "5474d227-0916-472c-a356-8a706ec24d09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "40225306-88ac-44ea-8804-bf5fb8526fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5474d227-0916-472c-a356-8a706ec24d09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "652788a9-25d2-4b26-b728-ad1914e49001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5474d227-0916-472c-a356-8a706ec24d09",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "2692e184-a09a-4a4b-821c-3cae1c3610d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5474d227-0916-472c-a356-8a706ec24d09",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9401a960-540e-4898-a920-7c0f27c82b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "fac5b686-5e20-467e-baa1-cf4073bb3a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9401a960-540e-4898-a920-7c0f27c82b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd937744-4267-4863-830b-531a10b91eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9401a960-540e-4898-a920-7c0f27c82b22",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d26310bf-2b55-455a-8cc3-a0d8f1a19386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9401a960-540e-4898-a920-7c0f27c82b22",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "86aca1dc-0e20-4802-bc51-43f097c77a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0d8330f7-a5fa-420a-a7ca-7212cbab3dbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86aca1dc-0e20-4802-bc51-43f097c77a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00117f8-23d3-4256-b096-a7ce66466b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86aca1dc-0e20-4802-bc51-43f097c77a8e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "b7513e62-3e84-452a-9d27-3f3548d7469a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86aca1dc-0e20-4802-bc51-43f097c77a8e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7ed9ec20-87c6-4f20-80bc-6b42f0aee9a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "cc64199b-cb81-4474-a8e9-1a7c1b39543d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed9ec20-87c6-4f20-80bc-6b42f0aee9a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4624562-f4a5-4738-ad88-901e20e032aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed9ec20-87c6-4f20-80bc-6b42f0aee9a4",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "58b57746-dcc5-4aab-9e79-01a1fae24afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed9ec20-87c6-4f20-80bc-6b42f0aee9a4",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "754c382c-23f6-47d5-8969-a80e1da5d3a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "85898c16-ae2b-411a-a73f-2156f9f5eb84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "754c382c-23f6-47d5-8969-a80e1da5d3a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42ae97c-638f-45ef-8ad7-20d1425d9dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "754c382c-23f6-47d5-8969-a80e1da5d3a0",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "f81ee8b7-219b-4615-94e2-01b81f2b34de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "754c382c-23f6-47d5-8969-a80e1da5d3a0",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "05420549-5f78-4513-8c41-e8f36e5f1dc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f834b1be-93a1-40cb-b959-f7eadef2762c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05420549-5f78-4513-8c41-e8f36e5f1dc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412c594d-f80a-4ce8-ae0b-58d858fda677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05420549-5f78-4513-8c41-e8f36e5f1dc1",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "f5745a82-2294-431c-add1-3d759cf35f9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05420549-5f78-4513-8c41-e8f36e5f1dc1",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "40697d1d-2cfe-4c51-b7ea-58ba34431015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "b9ef7233-1596-4608-949e-f1ed3307a6ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40697d1d-2cfe-4c51-b7ea-58ba34431015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b3d132-be3e-499a-a5b1-17f014945e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40697d1d-2cfe-4c51-b7ea-58ba34431015",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "98ac8249-4fe5-4df7-9b4a-acd64867446a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40697d1d-2cfe-4c51-b7ea-58ba34431015",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "773a1bb2-d861-4fd5-bc4a-51d0e1646207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "366c0533-6748-407d-87f2-4ca4688df89d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "773a1bb2-d861-4fd5-bc4a-51d0e1646207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5d7c8e0-9d8f-4171-8998-30c3b2822c64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773a1bb2-d861-4fd5-bc4a-51d0e1646207",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "4d9aa299-5efc-4fe8-95c4-1b6dbda49397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773a1bb2-d861-4fd5-bc4a-51d0e1646207",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7ef14821-527a-4652-8386-2f1c42ab0b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f79a74a1-46d7-4138-b1f3-fb78bd513541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef14821-527a-4652-8386-2f1c42ab0b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2b6b6f-b547-45d7-bfad-43fda360fc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef14821-527a-4652-8386-2f1c42ab0b4c",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "5dd3e513-19ad-47b9-bd59-55f59e88b644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef14821-527a-4652-8386-2f1c42ab0b4c",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a064d63b-a854-4118-8ed4-b686409e0585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "d151f48e-e4a9-43bb-98da-3dd1e4f0bc48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a064d63b-a854-4118-8ed4-b686409e0585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22b64981-b4e9-4c12-ada4-0c8ac30cb8a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a064d63b-a854-4118-8ed4-b686409e0585",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c8eaa0ab-c00a-4e6a-b86a-01be83f0e541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a064d63b-a854-4118-8ed4-b686409e0585",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "cd45fd88-725c-465e-b681-5c5708778bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f6eea7b0-cea2-45ab-a923-dd512398c4cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd45fd88-725c-465e-b681-5c5708778bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eff756c-a0f7-42c2-8e42-4a37d5b2d9bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd45fd88-725c-465e-b681-5c5708778bab",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "1892f5ff-e15d-43a1-bd5f-e359149c960b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd45fd88-725c-465e-b681-5c5708778bab",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e121ba4b-a552-4bcf-a6da-2c9ab2caf6e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "55371e39-b66d-4b93-865d-0dae6cefa3b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e121ba4b-a552-4bcf-a6da-2c9ab2caf6e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be22580-9e96-48be-aa2f-46ab27e279cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e121ba4b-a552-4bcf-a6da-2c9ab2caf6e6",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "05fc1d60-ce2e-447b-a740-4d7eaf940edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e121ba4b-a552-4bcf-a6da-2c9ab2caf6e6",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e393b069-9100-4efa-b449-d0f2cea1f776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7702bd22-1182-43ec-b6a1-f8a54aeeacfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e393b069-9100-4efa-b449-d0f2cea1f776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd074b2-96dc-4e7d-a88d-390a9115b2e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e393b069-9100-4efa-b449-d0f2cea1f776",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "42e84bc2-1404-4474-807f-02b119cb8647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e393b069-9100-4efa-b449-d0f2cea1f776",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "1368c699-ee31-42d6-9ed8-f0902678e73b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "ae3ba080-cb17-4e5c-9591-5263ed3ec8aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1368c699-ee31-42d6-9ed8-f0902678e73b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e206dc06-8484-4f0b-9ccc-8ce059a6e45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1368c699-ee31-42d6-9ed8-f0902678e73b",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "a134845f-2197-4ce0-a9d4-0891fe74f537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1368c699-ee31-42d6-9ed8-f0902678e73b",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "1770b7d9-39a6-4a96-8afe-2d49687fc837",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "670cb2a3-08e9-48b6-b52b-d6d52588ff10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1770b7d9-39a6-4a96-8afe-2d49687fc837",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02416e9e-db9c-4d0f-86c0-54400eb0e3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1770b7d9-39a6-4a96-8afe-2d49687fc837",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "658773ac-6b2c-4532-bd55-1bb79b80e91b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1770b7d9-39a6-4a96-8afe-2d49687fc837",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "4b9cb99c-4e0a-453a-a276-989b1e5ef88e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "deb8355a-ff92-43b2-851d-dbb21dc0a6df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9cb99c-4e0a-453a-a276-989b1e5ef88e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b1168aa-5829-4dd2-85ff-3647ee7fd8ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9cb99c-4e0a-453a-a276-989b1e5ef88e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "e0f147ce-e314-4251-9131-2af2382259c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9cb99c-4e0a-453a-a276-989b1e5ef88e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "07cede07-cd67-4950-8dce-70b8e5e401d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "d2c9b779-c384-40ce-806c-f7bd64fd34ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07cede07-cd67-4950-8dce-70b8e5e401d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d40b8c-5d4f-4f0d-888c-2be5b6f2e8ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cede07-cd67-4950-8dce-70b8e5e401d8",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "ed2686cf-c9c5-4194-8ee2-03d2ce33d8c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cede07-cd67-4950-8dce-70b8e5e401d8",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "4f7445bc-e233-41a9-b68f-549867043b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "28c819f5-bc81-4c13-a128-3988af25b510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7445bc-e233-41a9-b68f-549867043b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "275bf827-0be0-4521-b8e7-b100b6ff9ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7445bc-e233-41a9-b68f-549867043b1c",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c0af5151-b1c0-421d-b89b-1edea5fbddea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7445bc-e233-41a9-b68f-549867043b1c",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7e060487-3e91-46b9-8d63-715aaab88906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "a2073ab5-7db5-4f9f-8d36-55261d553167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e060487-3e91-46b9-8d63-715aaab88906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9091d860-8ef3-49e4-a5cd-1d64cfe83a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e060487-3e91-46b9-8d63-715aaab88906",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "f7fb522d-9029-4686-a888-1d60bd866361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e060487-3e91-46b9-8d63-715aaab88906",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a00f0930-a103-4957-8ac8-b500247be32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "76ec4a21-c5af-4713-9c54-48dc7c0960ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00f0930-a103-4957-8ac8-b500247be32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60640b76-c861-4c9a-a32a-9aa0cb146aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00f0930-a103-4957-8ac8-b500247be32c",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "f6c32f33-6f85-4f7a-9cc4-0ddf6aee463b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00f0930-a103-4957-8ac8-b500247be32c",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "d01b0b35-fd1a-418a-9158-a2702ed5e1dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "73db8d64-789c-4aeb-a9b8-0d0cdfea0d87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01b0b35-fd1a-418a-9158-a2702ed5e1dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22176a1e-418b-4268-af0e-3a9ba7f2bbab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01b0b35-fd1a-418a-9158-a2702ed5e1dc",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "b4a0609d-d661-4a66-b8aa-8a22eaf90448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01b0b35-fd1a-418a-9158-a2702ed5e1dc",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "8b881344-0f68-41a9-972d-e9d407070a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "3c40b916-ae85-4b59-8c9e-29f25b24d616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b881344-0f68-41a9-972d-e9d407070a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d6c867-a000-48d8-8d05-73324b670bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b881344-0f68-41a9-972d-e9d407070a1f",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "49568918-c288-49ba-8500-6d1745b29058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b881344-0f68-41a9-972d-e9d407070a1f",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9a9bc91c-94b4-4cd8-a97f-f17916a01525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "d5f050cc-c6c0-4585-aaf8-33679a878c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9bc91c-94b4-4cd8-a97f-f17916a01525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ecc47ff-3e98-44d7-a6b1-614fa543647a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9bc91c-94b4-4cd8-a97f-f17916a01525",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "4574ca5a-b0e5-463b-8518-36bae6d3c942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9bc91c-94b4-4cd8-a97f-f17916a01525",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "dd0feb0b-7dfc-4b7c-a5a6-13c522b59bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "6cbb50ba-2823-4021-9686-7e0931a087aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd0feb0b-7dfc-4b7c-a5a6-13c522b59bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42834b72-12af-4be0-8956-d0d0111404bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd0feb0b-7dfc-4b7c-a5a6-13c522b59bef",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "206ab8e7-6486-42d8-8947-d064a3773a1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd0feb0b-7dfc-4b7c-a5a6-13c522b59bef",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "413383bd-0a44-44a9-a144-fe42ebd856d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "109820d6-4a81-4488-83dd-4a20b2821aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413383bd-0a44-44a9-a144-fe42ebd856d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "640de244-d22c-4200-a55f-a8b9e0de5e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413383bd-0a44-44a9-a144-fe42ebd856d9",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "41cd4cee-a964-479a-8b2c-f555530c4e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413383bd-0a44-44a9-a144-fe42ebd856d9",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b6e399fc-c020-412b-9ad4-ce4041f7edda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "54e16846-99b0-4ea6-ae42-e10e4d03c1be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e399fc-c020-412b-9ad4-ce4041f7edda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb51aad5-102c-4a5c-9cb4-953160269f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e399fc-c020-412b-9ad4-ce4041f7edda",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "56ed083e-bd72-488d-b8b0-067baf90908e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e399fc-c020-412b-9ad4-ce4041f7edda",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "4efd6a0c-b401-444f-9485-8d4c261593f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f9ae5c97-89f7-4474-8e4c-b979635eef73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4efd6a0c-b401-444f-9485-8d4c261593f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf827aaf-c572-4d0b-a323-b9433786b677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4efd6a0c-b401-444f-9485-8d4c261593f5",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "ca06df73-3451-431a-b955-9e626a47032d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4efd6a0c-b401-444f-9485-8d4c261593f5",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "109f52f4-fa1c-46b9-bac0-fa98a08c4623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "8e588381-57bd-46f4-891c-3142c417e473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109f52f4-fa1c-46b9-bac0-fa98a08c4623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6c5038-2958-4965-80bd-0319a5551ae8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109f52f4-fa1c-46b9-bac0-fa98a08c4623",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d16df95f-8e9b-4e50-ab8c-10678853c610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109f52f4-fa1c-46b9-bac0-fa98a08c4623",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "bd0388b4-074b-4914-a29c-79b0037c1949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "2f53dd1f-74d2-4e68-8d52-04938db6e2b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0388b4-074b-4914-a29c-79b0037c1949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b0fdd77-129d-4e6a-86e4-0939839bb8c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0388b4-074b-4914-a29c-79b0037c1949",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "51ad3fbd-d520-41c6-9ed4-df5cbfff90ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0388b4-074b-4914-a29c-79b0037c1949",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b9ed246e-f244-4adb-910a-30075e778ebc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "e896350e-c60d-40bd-9e5f-bf47344376a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ed246e-f244-4adb-910a-30075e778ebc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58bbe0c-ded6-4aa3-9e1d-a190835fb41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ed246e-f244-4adb-910a-30075e778ebc",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "3eb1bf53-96d3-4e16-a9a8-ef82a267c4bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ed246e-f244-4adb-910a-30075e778ebc",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "a3b62877-0cdc-43b9-8419-5cdcfb10d196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "23e568ca-12b6-41ef-af61-791ed3ad18da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b62877-0cdc-43b9-8419-5cdcfb10d196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48eb0f63-42bb-49c6-a0c4-c36a3fff74c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b62877-0cdc-43b9-8419-5cdcfb10d196",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "202665fd-0df5-46e2-947a-fe782cd62a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b62877-0cdc-43b9-8419-5cdcfb10d196",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "6bebfa7d-6684-43e5-aefd-7f8f759fb5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "e5fa8031-5346-4598-92cd-cd5f0a44d370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bebfa7d-6684-43e5-aefd-7f8f759fb5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea29579-2c73-45ca-81ea-2412838437d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bebfa7d-6684-43e5-aefd-7f8f759fb5b0",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "fdee9da6-36d9-4a03-ae54-1fcfe90107a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bebfa7d-6684-43e5-aefd-7f8f759fb5b0",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "749c5d38-0293-4f00-b699-f034ee650c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "dc74049f-e6e0-49eb-a744-84ae1c50135b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749c5d38-0293-4f00-b699-f034ee650c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eee08b4-c55c-42f6-a9ff-8688dc345891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749c5d38-0293-4f00-b699-f034ee650c02",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "3ab2760e-48ee-42a1-b9c6-c87143424734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749c5d38-0293-4f00-b699-f034ee650c02",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "6208a458-508d-4570-80c0-28c77a14bcec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "4bd48098-2bba-4675-a9f8-055416b0e2b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6208a458-508d-4570-80c0-28c77a14bcec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b88cffc-f5bc-47a1-a5df-f058b2dced0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6208a458-508d-4570-80c0-28c77a14bcec",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d8ae1ba9-00e4-4908-a44c-a5df2e432c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6208a458-508d-4570-80c0-28c77a14bcec",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "d540d273-e97f-4ec2-8a8c-740d8bbcb45d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "04e7cd6d-b6f6-40d1-96cd-2e135bf3524b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d540d273-e97f-4ec2-8a8c-740d8bbcb45d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e3760d-cb1c-4e6e-bb24-ae85aaf78d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d540d273-e97f-4ec2-8a8c-740d8bbcb45d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "5a7febcb-be6e-4e2a-87d1-99be3ebb9c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d540d273-e97f-4ec2-8a8c-740d8bbcb45d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "1e97587e-bcac-4397-9374-090b5fbbc27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f92927f0-5514-4364-9df3-3afccdfcdd08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e97587e-bcac-4397-9374-090b5fbbc27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e040f9-f600-4b73-aa81-1744953052ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e97587e-bcac-4397-9374-090b5fbbc27d",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "6af02bea-f681-44d2-94c5-316729f1b997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e97587e-bcac-4397-9374-090b5fbbc27d",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "5a2ab2ba-02b4-4654-82a4-cbdcbd802d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "fdace24b-3d78-45f9-a723-4813185b15bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2ab2ba-02b4-4654-82a4-cbdcbd802d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d61c3c2f-c800-4b7d-a0ac-34bdb63a81b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2ab2ba-02b4-4654-82a4-cbdcbd802d11",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "43fedd0d-30c5-487b-abfb-8726a3f60076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2ab2ba-02b4-4654-82a4-cbdcbd802d11",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "15945798-1550-4f68-899d-a5698eae2245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "9e10e298-d69a-4953-88c9-efc331473a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15945798-1550-4f68-899d-a5698eae2245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "914c5807-2108-4962-8b31-d574e2aef22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15945798-1550-4f68-899d-a5698eae2245",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "113895b9-8f22-41c7-903f-40c27682da17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15945798-1550-4f68-899d-a5698eae2245",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "ebb85799-ab39-4ce9-8f61-2f77ad250d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "fe3707b7-fa5c-4a3f-8d15-2d1545c3e841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb85799-ab39-4ce9-8f61-2f77ad250d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c25842-8397-4531-9806-79b361813077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb85799-ab39-4ce9-8f61-2f77ad250d38",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "9d15724f-0f92-4cee-b15a-1da8b202f2d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb85799-ab39-4ce9-8f61-2f77ad250d38",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "c7ce2d17-7ecb-47ff-8ea2-183d80e83289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "7d365767-12e3-47cb-8a99-310fb4afeca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ce2d17-7ecb-47ff-8ea2-183d80e83289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c98076-fad1-4c34-816f-d3a63b119729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ce2d17-7ecb-47ff-8ea2-183d80e83289",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "e069ad83-bf5a-4612-bae8-ef005688ad74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ce2d17-7ecb-47ff-8ea2-183d80e83289",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "117ab6c0-0dac-457f-b2d2-9dfa5293e7ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "89da88eb-af87-4d56-8b37-d728087e4ce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "117ab6c0-0dac-457f-b2d2-9dfa5293e7ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af814101-7c53-4fe0-af85-07a487604243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "117ab6c0-0dac-457f-b2d2-9dfa5293e7ed",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "70b3b691-d81a-470d-9ac1-e6cd2a5621da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "117ab6c0-0dac-457f-b2d2-9dfa5293e7ed",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "746aa047-fede-4dc2-8c1f-49e5405d02f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "8b2f40e1-857d-416a-8631-06335a7224ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "746aa047-fede-4dc2-8c1f-49e5405d02f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad703b9e-6a2b-498e-9805-e9250f4491ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "746aa047-fede-4dc2-8c1f-49e5405d02f8",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "8820f47b-530f-4a9e-99ba-5d09043b3421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "746aa047-fede-4dc2-8c1f-49e5405d02f8",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "928b7753-6440-49c1-a0f4-28cee16576b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "1ba19337-6652-4e3d-b110-9525215509ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928b7753-6440-49c1-a0f4-28cee16576b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307e9abc-7b7a-47ab-be8f-33937bd913e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928b7753-6440-49c1-a0f4-28cee16576b1",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "afeef4b8-30b3-4dad-88e6-8c99cab869b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928b7753-6440-49c1-a0f4-28cee16576b1",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "0c152cb5-9740-4a16-88c3-d5809e7c6510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "0d13b1cb-5d91-48c0-94b7-840b4369c97b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c152cb5-9740-4a16-88c3-d5809e7c6510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cdbcf9d-d40c-405e-8f04-4f28c058abe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c152cb5-9740-4a16-88c3-d5809e7c6510",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "bf7c850a-31f7-4e60-894e-c82f4c6c78b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c152cb5-9740-4a16-88c3-d5809e7c6510",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "98be33c0-afa5-4b60-b6a2-190ed49a9762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "021e1af3-3915-473e-b767-128f6793b987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98be33c0-afa5-4b60-b6a2-190ed49a9762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b48ff50-9878-432e-be36-7d14785acb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98be33c0-afa5-4b60-b6a2-190ed49a9762",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0d604037-0a7e-416f-bcf3-fbace706757e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98be33c0-afa5-4b60-b6a2-190ed49a9762",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "19ac4afc-1546-4595-800e-2655da83a578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "e21dc1bc-5c37-475c-a6be-d26ddfd0fb7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ac4afc-1546-4595-800e-2655da83a578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a155320c-b9d3-4822-a678-62aa7c6384aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ac4afc-1546-4595-800e-2655da83a578",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "1d085181-7e63-4bc3-a2df-0e77c001988e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ac4afc-1546-4595-800e-2655da83a578",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "c90031cd-9786-4b44-bc56-f133bf4832f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "71f97e0d-391f-4060-b2eb-978dc549ad02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c90031cd-9786-4b44-bc56-f133bf4832f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b37da4-e930-4c18-95cb-c51ce6a5ef4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c90031cd-9786-4b44-bc56-f133bf4832f2",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "d6caf717-282c-4235-87fa-9e1a117c90c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c90031cd-9786-4b44-bc56-f133bf4832f2",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e9fe7bf5-2129-4a11-9579-913e851d66ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "f615c2d0-be62-4780-9e00-f467fcda2c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fe7bf5-2129-4a11-9579-913e851d66ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d63ccb7-6ff2-471b-8dcf-b919a5a4db94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fe7bf5-2129-4a11-9579-913e851d66ed",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c513e7fb-7704-42e4-8e1a-1fab69ec3643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fe7bf5-2129-4a11-9579-913e851d66ed",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "3bfe0b16-0e22-4bcd-b1e2-25062110faf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c0f24494-b201-4394-a8fa-6383f83d7462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bfe0b16-0e22-4bcd-b1e2-25062110faf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8be58c21-a640-41c8-93f9-4d5d3f3da496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bfe0b16-0e22-4bcd-b1e2-25062110faf8",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "4cc14158-c26c-46ad-80e0-8ebe724aa215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bfe0b16-0e22-4bcd-b1e2-25062110faf8",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "fb7296bc-f4a5-462a-bc82-4c34a6eef0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "03c0e5a1-bfb7-43c9-9b3a-ec734f046b07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb7296bc-f4a5-462a-bc82-4c34a6eef0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0653df2-a220-453c-a6f5-0025234975c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7296bc-f4a5-462a-bc82-4c34a6eef0cd",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "48653034-e81c-4e8a-aa79-aa47cfe82ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7296bc-f4a5-462a-bc82-4c34a6eef0cd",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "d4abd3cd-35a7-4c25-b8b2-993b46fc8ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "32799760-2b3e-474f-9a64-22ecf5260890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4abd3cd-35a7-4c25-b8b2-993b46fc8ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efa2fc93-bfc2-45b6-8dc6-2153a8db049f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4abd3cd-35a7-4c25-b8b2-993b46fc8ea6",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "15553fb2-9c40-4e05-9fba-d54ecbbf7cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4abd3cd-35a7-4c25-b8b2-993b46fc8ea6",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "7e497ae3-ee49-4789-a96a-0cf4134c5f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "6ce5a76d-37d9-437b-99dc-db7a5e1d9743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e497ae3-ee49-4789-a96a-0cf4134c5f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a374e9-57d0-463f-a734-e6fe0a8155b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e497ae3-ee49-4789-a96a-0cf4134c5f45",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c23ca9b8-7699-4da1-8a5b-15cc8bd4f64e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e497ae3-ee49-4789-a96a-0cf4134c5f45",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "114575e9-eaab-42cf-8126-9488c2fed28e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "de2d533b-fb9f-46c9-81b5-e18c3f18e64f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114575e9-eaab-42cf-8126-9488c2fed28e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab84d147-89a0-4fef-9e84-99450ad7ca2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114575e9-eaab-42cf-8126-9488c2fed28e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "4ac32c0c-f95c-4481-b608-9479da214573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114575e9-eaab-42cf-8126-9488c2fed28e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "867d028c-3f59-42c0-a424-e66a800fe503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "2d85b3df-faad-4478-a512-d3b3ed13aa4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "867d028c-3f59-42c0-a424-e66a800fe503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8ed4d7-844e-481c-a2b6-33169e087a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "867d028c-3f59-42c0-a424-e66a800fe503",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "7b5dc6ef-366a-426c-a676-345c2ee52abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "867d028c-3f59-42c0-a424-e66a800fe503",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "4144b6e4-e155-477c-b299-e6e6a5c242a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "68f42db6-83cc-4422-8455-a2b2be1e120b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4144b6e4-e155-477c-b299-e6e6a5c242a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f74bcb5-8c07-4a1a-a8f8-a343072fa9ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4144b6e4-e155-477c-b299-e6e6a5c242a0",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "c5f04b97-ed7f-4256-a335-d564f374e3d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4144b6e4-e155-477c-b299-e6e6a5c242a0",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "8c35419f-ae61-4373-bb86-34033354794e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "16e1d63d-fab3-4578-a0db-e40169126f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c35419f-ae61-4373-bb86-34033354794e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c9ceb39-b7e6-4f03-870c-1c41ec1ae2f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c35419f-ae61-4373-bb86-34033354794e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0d60a6a8-1d08-4aa0-bca1-19f40ae3a14f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c35419f-ae61-4373-bb86-34033354794e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "16836d06-b753-4a49-866b-afbee75c3fe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "a38dae0a-17f7-4f9a-bedd-2d8baebee1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16836d06-b753-4a49-866b-afbee75c3fe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33988e32-0221-4784-9d63-c510720c10fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16836d06-b753-4a49-866b-afbee75c3fe4",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "40cd2d19-6f64-482a-91b6-f0bf9875007c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16836d06-b753-4a49-866b-afbee75c3fe4",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "8d456931-1f61-421b-8df7-66512ff624df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "c9eb1f33-03bd-4e38-a558-5eac9ad58989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d456931-1f61-421b-8df7-66512ff624df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17970551-4508-4a3d-8748-1dd082b8acc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d456931-1f61-421b-8df7-66512ff624df",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "77120309-1504-4433-b6d8-48ee17a181e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d456931-1f61-421b-8df7-66512ff624df",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "f3684956-1301-419a-871c-a863b74aed6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "24ea9103-f5dc-4b4c-8cbd-680bd15c7686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3684956-1301-419a-871c-a863b74aed6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52c4526-ebec-4b5e-960e-f1d720383d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3684956-1301-419a-871c-a863b74aed6b",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "585c1858-a58f-449a-9ecf-e40edc86c328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3684956-1301-419a-871c-a863b74aed6b",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "9bb5ba0d-2446-4ba8-8df8-299164c2540e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "b3be42b6-41d3-4f0f-9f65-9d00e498cb6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb5ba0d-2446-4ba8-8df8-299164c2540e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02b00610-bb9c-4ab6-b372-9e23d265e587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb5ba0d-2446-4ba8-8df8-299164c2540e",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "ba539a17-1236-4d0a-acd7-6c12030d2948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb5ba0d-2446-4ba8-8df8-299164c2540e",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "ce109e03-5447-4d5b-89f8-45f5609c72fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "fa3e78ef-d122-4a55-8a6d-a3f32ddc7237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce109e03-5447-4d5b-89f8-45f5609c72fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae1e10b-0e59-40b4-86b2-edf46c160107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce109e03-5447-4d5b-89f8-45f5609c72fa",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "13924102-2dfc-446f-b2b6-b335736a1fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce109e03-5447-4d5b-89f8-45f5609c72fa",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "b9d31be9-b6da-413f-8820-e74b11ca97b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "93bf5edf-8821-4f30-b6fc-9346d590efef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d31be9-b6da-413f-8820-e74b11ca97b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916831d4-0026-4686-9f7a-b584dfd5c559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d31be9-b6da-413f-8820-e74b11ca97b2",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "33c22c07-799e-4fc1-a775-32b444c76142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d31be9-b6da-413f-8820-e74b11ca97b2",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "57cc619c-136a-48cb-9825-61b8f4849ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "1f05cb80-2f37-4813-a30b-b7cf4ad7c80f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57cc619c-136a-48cb-9825-61b8f4849ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dad17a6-dbdb-413d-851c-230319134ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57cc619c-136a-48cb-9825-61b8f4849ff7",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "2d16773a-3146-476c-98d6-cd88bc1b991e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57cc619c-136a-48cb-9825-61b8f4849ff7",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "e285bdc9-dbf7-460f-b66b-b2091cb9b985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "9b56ce43-27e3-4e2a-9a9b-0a71dd0f40fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e285bdc9-dbf7-460f-b66b-b2091cb9b985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f95ce80-77b8-4112-8c51-d5a0e450a1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e285bdc9-dbf7-460f-b66b-b2091cb9b985",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "64293996-607b-4550-8f10-1af87dbbf69e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e285bdc9-dbf7-460f-b66b-b2091cb9b985",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "dca512c5-7817-4467-a96c-2763497613b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "804828c8-726a-4073-9789-20865fd35422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca512c5-7817-4467-a96c-2763497613b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2190003e-e3fd-4a16-b1fc-1a0534f460a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca512c5-7817-4467-a96c-2763497613b3",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "db094510-c65c-4000-ad48-f0b720aed2bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca512c5-7817-4467-a96c-2763497613b3",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        },
        {
            "id": "bc4a92c3-2d5e-4cad-9561-102182a9ba88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "compositeImage": {
                "id": "b6875002-cc36-4e4e-b85b-2bae32b8cf39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc4a92c3-2d5e-4cad-9561-102182a9ba88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5204f186-f845-4ac1-bd82-0084899843e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4a92c3-2d5e-4cad-9561-102182a9ba88",
                    "LayerId": "7c7eaf44-d9f2-474c-a223-5df90f97b087"
                },
                {
                    "id": "0008d4b6-18a0-4527-b0b1-a7326aa6bcf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4a92c3-2d5e-4cad-9561-102182a9ba88",
                    "LayerId": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "7c7eaf44-d9f2-474c-a223-5df90f97b087",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b6839c7f-947b-4c35-9c66-6f6eaa1e6043",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c6b16b6-2034-4689-89f0-5bf972927026",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 1,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}