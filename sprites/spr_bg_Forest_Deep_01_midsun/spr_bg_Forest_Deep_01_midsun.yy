{
    "id": "c5cc0b51-0ffc-4a94-bd3d-25f3965aad2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Deep_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b76c74e-072d-42cd-b358-88217eb61fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc0b51-0ffc-4a94-bd3d-25f3965aad2c",
            "compositeImage": {
                "id": "cb9619ba-7b85-432e-80f2-9d6e52282afb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b76c74e-072d-42cd-b358-88217eb61fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "519f25e7-0f6a-4c6a-84d9-db6cde3f76fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b76c74e-072d-42cd-b358-88217eb61fdb",
                    "LayerId": "d61c641d-58f8-447e-89b1-b1b8afe90bcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "d61c641d-58f8-447e-89b1-b1b8afe90bcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5cc0b51-0ffc-4a94-bd3d-25f3965aad2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}