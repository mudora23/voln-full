{
    "id": "ef49144e-2e9c-4b73-bd4e-631a3a42ffcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_primrose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffae258d-c6ff-48c3-9008-dc626c752641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef49144e-2e9c-4b73-bd4e-631a3a42ffcf",
            "compositeImage": {
                "id": "df455240-21cf-413a-98aa-5f20bd61080d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffae258d-c6ff-48c3-9008-dc626c752641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2e82b8-b705-45cb-a32c-a67dd2a57f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffae258d-c6ff-48c3-9008-dc626c752641",
                    "LayerId": "c719f9fd-e904-4e04-acae-3b4830f1e3a8"
                }
            ]
        },
        {
            "id": "e904a2a2-2548-44de-84b9-6e2b60119ab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef49144e-2e9c-4b73-bd4e-631a3a42ffcf",
            "compositeImage": {
                "id": "f3f92ed6-97f3-424a-9a95-ce23d3082be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e904a2a2-2548-44de-84b9-6e2b60119ab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b6dd1c-b5e1-4cdb-8d2c-7683d3721574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e904a2a2-2548-44de-84b9-6e2b60119ab9",
                    "LayerId": "c719f9fd-e904-4e04-acae-3b4830f1e3a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "c719f9fd-e904-4e04-acae-3b4830f1e3a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef49144e-2e9c-4b73-bd4e-631a3a42ffcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}