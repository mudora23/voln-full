{
    "id": "9a0b7271-4a4d-4a04-a218-36b8a33c2027",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_player_information",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 439,
    "bbox_left": 0,
    "bbox_right": 359,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2c05020-18e0-412e-b566-f8b16cd85c29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0b7271-4a4d-4a04-a218-36b8a33c2027",
            "compositeImage": {
                "id": "771ee0c7-fe68-455e-b19e-6173b906c9e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c05020-18e0-412e-b566-f8b16cd85c29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960f432d-6d2b-414c-b5c9-477c307c773f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c05020-18e0-412e-b566-f8b16cd85c29",
                    "LayerId": "84039929-3ebc-46a1-baf5-1cabe0d163ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 440,
    "layers": [
        {
            "id": "84039929-3ebc-46a1-baf5-1cabe0d163ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a0b7271-4a4d-4a04-a218-36b8a33c2027",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 360,
    "xorig": 0,
    "yorig": 0
}