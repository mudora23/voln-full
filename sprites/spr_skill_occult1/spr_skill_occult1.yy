{
    "id": "3c68ca90-03fa-4aaf-9e15-026d9c333b77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_occult1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8d687e9-89a0-493e-b0bd-fe78e7a9e542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c68ca90-03fa-4aaf-9e15-026d9c333b77",
            "compositeImage": {
                "id": "2ab5317a-e053-44a4-a2be-d25caa3a4659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d687e9-89a0-493e-b0bd-fe78e7a9e542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e160dabe-7dac-46a9-bb37-96c26f1d3f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d687e9-89a0-493e-b0bd-fe78e7a9e542",
                    "LayerId": "b3a8eb65-6852-4f68-991f-54f3fe4c0295"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "b3a8eb65-6852-4f68-991f-54f3fe4c0295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c68ca90-03fa-4aaf-9e15-026d9c333b77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}