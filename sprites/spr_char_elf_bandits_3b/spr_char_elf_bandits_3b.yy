{
    "id": "e345464f-b30a-46e1-8d66-6ce6e4ce9b93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_3b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1970,
    "bbox_left": 0,
    "bbox_right": 1141,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8af9b4fb-fafa-437c-87b1-0c728f8d36e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e345464f-b30a-46e1-8d66-6ce6e4ce9b93",
            "compositeImage": {
                "id": "2f1e2718-e972-4634-b951-5ea4a473ec1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af9b4fb-fafa-437c-87b1-0c728f8d36e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276e0a38-58da-4200-bc3f-d3e5a294bcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af9b4fb-fafa-437c-87b1-0c728f8d36e5",
                    "LayerId": "e5d654b4-267a-42c6-9401-7cfa70db64e7"
                }
            ]
        },
        {
            "id": "3cf63aed-aa86-4373-a79f-4be7ce57c27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e345464f-b30a-46e1-8d66-6ce6e4ce9b93",
            "compositeImage": {
                "id": "c003bdec-cb11-4afc-bb02-c62202c13132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf63aed-aa86-4373-a79f-4be7ce57c27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e42452-b88f-4190-8781-4a2104f04ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf63aed-aa86-4373-a79f-4be7ce57c27f",
                    "LayerId": "e5d654b4-267a-42c6-9401-7cfa70db64e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1971,
    "layers": [
        {
            "id": "e5d654b4-267a-42c6-9401-7cfa70db64e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e345464f-b30a-46e1-8d66-6ce6e4ce9b93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1142,
    "xorig": 0,
    "yorig": 0
}