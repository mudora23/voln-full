{
    "id": "725fdb29-61b1-4dfc-85fa-027b21746914",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_4_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a643fc32-feb5-4c92-98ff-0494f5db40c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "725fdb29-61b1-4dfc-85fa-027b21746914",
            "compositeImage": {
                "id": "c1d84cbf-4323-46f3-b5ee-e19592a9d500",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a643fc32-feb5-4c92-98ff-0494f5db40c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee180de-bff0-4afd-aee9-9c679d60844a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a643fc32-feb5-4c92-98ff-0494f5db40c4",
                    "LayerId": "6aae699c-eba2-4223-ac8d-d666f089a987"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "6aae699c-eba2-4223-ac8d-d666f089a987",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "725fdb29-61b1-4dfc-85fa-027b21746914",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}