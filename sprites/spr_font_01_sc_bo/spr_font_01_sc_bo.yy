{
    "id": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_sc_bo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2cf7719-46e3-45a0-961e-81a9116f575d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "47d10612-0db6-432b-90ef-ba870606568b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2cf7719-46e3-45a0-961e-81a9116f575d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc4f4acc-cac7-4ff1-b1d2-7bd25edbc5de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2cf7719-46e3-45a0-961e-81a9116f575d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "31c3b55a-ba45-4643-86f0-d017a50763b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1cf3724d-507b-4406-9c28-3f7e94c7c9d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c3b55a-ba45-4643-86f0-d017a50763b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c98659-dbf8-4ccb-ab9b-136ad3ccc6b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c3b55a-ba45-4643-86f0-d017a50763b4",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "95cf3296-c6a5-4bee-a486-901a68476c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "cfa607a3-1d1c-4b01-898a-3fe6aca6e9ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95cf3296-c6a5-4bee-a486-901a68476c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "530c2a97-ed24-4207-878d-9f8012c5c0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95cf3296-c6a5-4bee-a486-901a68476c10",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "bcb9341d-7c7f-4302-8e29-74797bb5d4f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "7db461b9-fb6f-404d-94ca-0a58c2395e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb9341d-7c7f-4302-8e29-74797bb5d4f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7761776e-eb09-4a93-b052-b81b2e4c4bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb9341d-7c7f-4302-8e29-74797bb5d4f7",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "9ebd5db1-050b-4e86-acba-1659e3ffc109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e5eecf67-903a-485f-94b5-b32e03d60c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebd5db1-050b-4e86-acba-1659e3ffc109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "103b5839-d65e-4ae2-8272-bfe0cfbae8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebd5db1-050b-4e86-acba-1659e3ffc109",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f7965f41-c3a2-4521-a531-ff6bced31e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "c60b391c-15d2-48ec-8f5d-ec6fec05b8c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7965f41-c3a2-4521-a531-ff6bced31e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44000e13-dd59-4821-ab3e-75ee0f84945e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7965f41-c3a2-4521-a531-ff6bced31e9e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f22e3ed1-56ab-49ce-a24e-fda19d9cd5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "15c9a644-bbe4-497b-8e77-aac9e494fe36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22e3ed1-56ab-49ce-a24e-fda19d9cd5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a28975e-3f32-4c8b-a879-6cbbe1a8038a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22e3ed1-56ab-49ce-a24e-fda19d9cd5ce",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "77224968-06a7-47a8-8a69-aaac487d886e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "a16f3188-a03e-4511-b554-cbfd8a22f2ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77224968-06a7-47a8-8a69-aaac487d886e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1abd69e4-270c-4857-811a-bbb1e239ae01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77224968-06a7-47a8-8a69-aaac487d886e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "75684713-545a-41bf-b926-bdedc76da524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "f3246515-7494-47ef-b922-1fb35dacf65b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75684713-545a-41bf-b926-bdedc76da524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8853e4e8-25b2-4760-a0e5-05e9b6044f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75684713-545a-41bf-b926-bdedc76da524",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "54765620-b02f-4ef3-a05a-d589ceda05b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "69b058a7-4baf-41d8-bab9-735ecb2d7a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54765620-b02f-4ef3-a05a-d589ceda05b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a43bf9d1-d86a-4077-827a-3cf12dce5185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54765620-b02f-4ef3-a05a-d589ceda05b2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "993e108e-46b9-4dde-b20e-7c3893df5c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e74ff3a6-4419-4675-8514-b50b6bdeff15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "993e108e-46b9-4dde-b20e-7c3893df5c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8147cb0-a2a0-4941-902e-b05b452854c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "993e108e-46b9-4dde-b20e-7c3893df5c96",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "bda16ab8-9456-4c0f-882f-cdfb9deb2830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "bd73e117-1ffc-4b16-bccf-cc2b14c43351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda16ab8-9456-4c0f-882f-cdfb9deb2830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474a3f55-5d0d-41c8-b9af-b621fc4dd0b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda16ab8-9456-4c0f-882f-cdfb9deb2830",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2c020a84-1a92-4afb-a72f-c5f0230b677c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "c3f3026a-c89d-49f1-bcec-d06e588ae9c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c020a84-1a92-4afb-a72f-c5f0230b677c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6623a587-be68-468f-8d5d-fb3bc6cc331f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c020a84-1a92-4afb-a72f-c5f0230b677c",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "921d3728-cb62-4187-ac68-3e89b36186d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e6e6f9e5-3ba3-460e-b0c4-efc55057f752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921d3728-cb62-4187-ac68-3e89b36186d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36805893-ebe8-4f98-9d8a-e054c28ce668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921d3728-cb62-4187-ac68-3e89b36186d7",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2ad997ea-bcc0-4e41-9a0a-a4217947ffbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "c09622bb-1004-4410-9f38-37a55fdb02f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad997ea-bcc0-4e41-9a0a-a4217947ffbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e530ffd-5ee3-4b1f-b89a-3edbc770e247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad997ea-bcc0-4e41-9a0a-a4217947ffbd",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "3c6be818-4d2d-4e9d-9578-a0fffb897ece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "aee3f10c-8a57-4f5d-9f2b-97a2f9d4873c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c6be818-4d2d-4e9d-9578-a0fffb897ece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0928748-db94-48cc-a8ec-c9b3526959fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c6be818-4d2d-4e9d-9578-a0fffb897ece",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "40a33831-53f5-4287-9a60-ca724eb6c341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "10faa46e-dca7-42e4-a4c3-b0b75f40b91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40a33831-53f5-4287-9a60-ca724eb6c341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3201ddb7-d6c4-44e6-a97d-3c818e08117a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40a33831-53f5-4287-9a60-ca724eb6c341",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "6b535884-ecbb-41b1-a091-f4e8ddde13d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "04d18143-ddca-4cad-a799-5660b7c48f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b535884-ecbb-41b1-a091-f4e8ddde13d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46934dc-f1b8-492c-94bb-ade47638f3cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b535884-ecbb-41b1-a091-f4e8ddde13d2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "099d0a07-a5bd-4f26-9905-238a8fa55d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "931125f7-6f3f-4cc6-af7d-8e6a08957357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099d0a07-a5bd-4f26-9905-238a8fa55d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44870262-6d7a-4b17-b0ba-f47084677606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099d0a07-a5bd-4f26-9905-238a8fa55d5a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "8bfd3861-1b5f-44dd-8080-59d0776283fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "f46e542e-51f2-4906-9924-e5e9dc1eafd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bfd3861-1b5f-44dd-8080-59d0776283fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b3bbf0-26a3-40e5-8fc5-169357e2a718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bfd3861-1b5f-44dd-8080-59d0776283fa",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "1b63d4da-40a7-46ac-9eb9-de29fa03767d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "31f086cb-71d7-4949-b2dc-9af27e29fb0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b63d4da-40a7-46ac-9eb9-de29fa03767d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "643711fe-c2b5-4951-9030-22ede106fa03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b63d4da-40a7-46ac-9eb9-de29fa03767d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "db3a7f1e-ea82-436e-8b47-2d6da695b338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "4a84afc1-b4e6-4ea1-9c0e-ca1a0a3bb3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3a7f1e-ea82-436e-8b47-2d6da695b338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2249ab79-2c54-4cbd-be2e-1b483335f480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3a7f1e-ea82-436e-8b47-2d6da695b338",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e17d2c41-cea1-4ad6-851a-1fd36ca5fa9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "5522b138-66a8-471e-9d73-6f82de8034ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e17d2c41-cea1-4ad6-851a-1fd36ca5fa9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6772c9a-a8dd-41b4-b806-1ad953431d34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e17d2c41-cea1-4ad6-851a-1fd36ca5fa9b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "58b78975-d39c-4fe6-bdcd-5a37501f8a9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9f9c63e3-2963-4092-a18b-472046bd15d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b78975-d39c-4fe6-bdcd-5a37501f8a9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cebb058-c96a-4bfb-aa06-86865f6c74d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b78975-d39c-4fe6-bdcd-5a37501f8a9a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "fddcb01e-dff3-46ac-877e-2a139bfe7002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "90534b8b-27e5-481c-8afb-660b024573c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fddcb01e-dff3-46ac-877e-2a139bfe7002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf9ebd0-3438-4e74-9225-96b85f6d7113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fddcb01e-dff3-46ac-877e-2a139bfe7002",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "ec052c2d-bce9-491f-96e3-bff195a727c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "3ebf7624-0f88-47da-8598-414738e244a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec052c2d-bce9-491f-96e3-bff195a727c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f0878b7-b71c-443d-b67d-4e8110333a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec052c2d-bce9-491f-96e3-bff195a727c2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e5222eca-1471-4602-9b5e-71c7875fc03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "15efa001-e4e3-4201-a231-753abad4ed6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5222eca-1471-4602-9b5e-71c7875fc03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85951844-73a6-40da-89bd-63fa44b7bff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5222eca-1471-4602-9b5e-71c7875fc03b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b5addeb1-d387-49fe-8e30-e34d3ac97663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "4daaa3e6-95bc-4fc4-bdd0-041effab49c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5addeb1-d387-49fe-8e30-e34d3ac97663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b9d04d9-aee5-4988-a2d5-d783fd937379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5addeb1-d387-49fe-8e30-e34d3ac97663",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "ea688826-16a1-4ea1-b93b-14e5f841a07e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "eb67e436-de88-4531-95f0-8dad2026f032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea688826-16a1-4ea1-b93b-14e5f841a07e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8234b04d-063a-461f-91e5-51e94875ea04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea688826-16a1-4ea1-b93b-14e5f841a07e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "4541e079-2c39-4c3e-b8c8-2f979425a972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "6d787313-8104-468e-9174-1d68bf31057e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4541e079-2c39-4c3e-b8c8-2f979425a972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4227c93c-7176-4185-ae8a-e57ccfd24837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4541e079-2c39-4c3e-b8c8-2f979425a972",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "5c94eece-aea2-446e-8675-40386c840c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "cb4bcd77-aabb-4383-bf24-06154268c1c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c94eece-aea2-446e-8675-40386c840c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "965d4a42-4a9c-4576-a532-f22bbd2aacbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c94eece-aea2-446e-8675-40386c840c96",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "5ed7d89f-bd45-41ba-a59c-3c5c47a6fc3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "aa09071b-69fd-4a8c-ba31-c6e9202a9361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ed7d89f-bd45-41ba-a59c-3c5c47a6fc3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47277ed-1cd4-41eb-b0be-dce6792d9111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ed7d89f-bd45-41ba-a59c-3c5c47a6fc3c",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "1528b3fa-960b-4ee1-98ca-bc7ad680964e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "6391e102-1d0d-433b-b3b9-6f4594cddf2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1528b3fa-960b-4ee1-98ca-bc7ad680964e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d57e271-7e52-432f-a86b-bb650d852f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1528b3fa-960b-4ee1-98ca-bc7ad680964e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "1c81b219-8143-475b-8ad5-fc7d54218057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "a0f8a451-1ca8-4b0b-9cb0-dcd1593fc594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c81b219-8143-475b-8ad5-fc7d54218057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59bcdf1b-1aaf-4b7e-a33b-664cdb96a65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c81b219-8143-475b-8ad5-fc7d54218057",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b7b915a7-7c08-4458-807c-5caa4c04df46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "a89c481a-eda0-4920-ac93-e72e36883475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7b915a7-7c08-4458-807c-5caa4c04df46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9cd97d-8b2f-410a-91c8-a27fe806ac0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7b915a7-7c08-4458-807c-5caa4c04df46",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "9bebbfc5-154c-4f07-bc1d-463bc4cd6da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1b3c69fb-c222-4d2d-941f-b12172065c63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bebbfc5-154c-4f07-bc1d-463bc4cd6da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8056c5-87c9-4208-b752-5dc12f6cf9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bebbfc5-154c-4f07-bc1d-463bc4cd6da2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "6411481e-b1bc-44e7-9e48-e59ce6c26432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "929b32dc-c261-4628-bc0f-fd016609be72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6411481e-b1bc-44e7-9e48-e59ce6c26432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172c5c64-edeb-4612-a66e-ff35d093f65a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6411481e-b1bc-44e7-9e48-e59ce6c26432",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "ac1f492c-d171-46f7-8e5d-bc115b2dd39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e2682358-a0fd-4357-af20-bdcf5af5229b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac1f492c-d171-46f7-8e5d-bc115b2dd39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4765e65-599c-4888-bc84-337250a4e610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1f492c-d171-46f7-8e5d-bc115b2dd39f",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "305f5e92-9c7c-48d1-a709-625c23eebe27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1d6eb8d0-66d4-4e80-8972-dbb10e06a34f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305f5e92-9c7c-48d1-a709-625c23eebe27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0227bb4c-c7a5-4e87-841f-e78e61a8ad5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305f5e92-9c7c-48d1-a709-625c23eebe27",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "494d7510-9fdd-499c-922b-5785841e909e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "d9137721-29d6-4f38-ba6d-0ae5356b9c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "494d7510-9fdd-499c-922b-5785841e909e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f9ef21-faad-4d7d-8a14-8b9f613e4f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "494d7510-9fdd-499c-922b-5785841e909e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b4dcc453-a249-4938-a029-0e99830e6c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "692200bc-1a48-4528-841a-dab31047f635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4dcc453-a249-4938-a029-0e99830e6c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9766cb48-6006-48ee-aec5-22e18a4a9b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4dcc453-a249-4938-a029-0e99830e6c47",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "6774f4fd-9363-4569-9bfa-c7404aee4f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1d493b3b-cd2b-466a-8dc0-4eaf72b5cdb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6774f4fd-9363-4569-9bfa-c7404aee4f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a954f2ad-1a97-4b8d-b1ba-4171d6d69565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6774f4fd-9363-4569-9bfa-c7404aee4f0f",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f0f4cb8c-43fd-4bfe-9bc6-5a77a92a5d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "a6915f6e-6f1b-49af-8b60-b17170cc8427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0f4cb8c-43fd-4bfe-9bc6-5a77a92a5d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ab3a15-d522-45b9-b320-865f4a4b4011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0f4cb8c-43fd-4bfe-9bc6-5a77a92a5d6d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "de52ba39-033d-4fa1-9881-5e587f31be48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "b799bac3-c905-472d-9945-bb18806c3913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de52ba39-033d-4fa1-9881-5e587f31be48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9aedd85-e2d3-4d93-ab9e-51308e40c47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de52ba39-033d-4fa1-9881-5e587f31be48",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "9f3e45fb-b5cc-4f3a-ab10-73f2be40d4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "dbdcc48f-9544-4ebe-961c-848d5b18f497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3e45fb-b5cc-4f3a-ab10-73f2be40d4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd87a0e-fe5b-4448-b915-863cb6a21762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3e45fb-b5cc-4f3a-ab10-73f2be40d4c9",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f2ed0d52-2b84-416f-9d3b-51670688ff33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9b44d9b4-8a53-4efe-80a2-3fe3e3f0e8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ed0d52-2b84-416f-9d3b-51670688ff33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c884e3-a0db-45a6-884d-ade5ec7172ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ed0d52-2b84-416f-9d3b-51670688ff33",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "5ce82d58-b6a9-4efd-b65f-f5d0bf44634a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "d1ecbd9a-5903-43ad-b094-e6936f4b7058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce82d58-b6a9-4efd-b65f-f5d0bf44634a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f9cc7a-a077-4ece-a1f2-21772bac16fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce82d58-b6a9-4efd-b65f-f5d0bf44634a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "1721d278-c296-48d1-9f0f-eaec1f5369d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "79701b8d-6551-499f-868d-e5ca1006dc82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1721d278-c296-48d1-9f0f-eaec1f5369d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb56eef-795f-4492-90a2-01527a28eeb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1721d278-c296-48d1-9f0f-eaec1f5369d8",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "3f49c61a-6116-458e-b3f2-9b184d448f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "2556d5b1-9de9-4269-8658-a4a365e3714a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f49c61a-6116-458e-b3f2-9b184d448f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee949282-aca1-478e-81d4-184b7ac9d3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f49c61a-6116-458e-b3f2-9b184d448f30",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f330a9a1-bcfa-4a82-9198-923f4f912820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "51e9abd3-765b-4f71-b76a-2a4134e08659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f330a9a1-bcfa-4a82-9198-923f4f912820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b07a2e46-72b4-4626-bc1b-d1e0990d25ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f330a9a1-bcfa-4a82-9198-923f4f912820",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2c34a69a-0eb2-4851-861f-fef4c9d41101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "2b320ef1-fac5-42e7-ac58-fcbf83bc6c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c34a69a-0eb2-4851-861f-fef4c9d41101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9abc9c-8ac9-417b-ad1c-f7f0604613a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c34a69a-0eb2-4851-861f-fef4c9d41101",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "338f634f-c863-4e5e-9984-0028e8760c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "4dd79ee4-cf9b-43fd-bd92-9faad5be5161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338f634f-c863-4e5e-9984-0028e8760c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25443715-6db8-452b-8818-ab5661837cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338f634f-c863-4e5e-9984-0028e8760c9e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "3752d053-c1d0-42df-96be-12a54716493c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "32bab1b6-4bbc-45ec-ab7f-399ba27bd019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3752d053-c1d0-42df-96be-12a54716493c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17cb382b-4819-40f6-bd33-7bfd391fe328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3752d053-c1d0-42df-96be-12a54716493c",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "991ffcbf-063a-4d25-9ee5-6c051a669c1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "eafff17a-6ccb-432e-a0a8-85d35d237408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "991ffcbf-063a-4d25-9ee5-6c051a669c1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c13302-8be8-48ac-9e39-b2c011e7a6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "991ffcbf-063a-4d25-9ee5-6c051a669c1b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "39a76af7-0bfa-43ec-bf45-c3f59ff57131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e0fb5574-4763-4c62-8653-dd600dd863b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39a76af7-0bfa-43ec-bf45-c3f59ff57131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc5b49eb-99ee-4de4-adee-dc9a86931859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39a76af7-0bfa-43ec-bf45-c3f59ff57131",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "341c3cc7-a964-449f-a957-2aa6dd896ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e292c170-5aa9-4493-8a62-003c31aaf147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "341c3cc7-a964-449f-a957-2aa6dd896ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8c4425-8e6d-4fe4-b97b-f2b76364428f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "341c3cc7-a964-449f-a957-2aa6dd896ca0",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f8ab321b-092b-433c-bd1c-c8eb42d35d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "ddce133d-0d33-4910-b045-ff4b48f84616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ab321b-092b-433c-bd1c-c8eb42d35d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b338b8e4-5b9c-4120-8356-f9de917098a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ab321b-092b-433c-bd1c-c8eb42d35d68",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "3e6bd384-3505-45fb-80f3-26dc43af1b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "91b0f68f-7a96-4621-a179-1f3ecd8dcfb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6bd384-3505-45fb-80f3-26dc43af1b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4431c997-f46d-4007-9b02-f0f2b59d8fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6bd384-3505-45fb-80f3-26dc43af1b52",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "dc513b8d-9617-45aa-9713-65041782073b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9eecf045-4d99-4658-8019-db24b9ca56e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc513b8d-9617-45aa-9713-65041782073b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eeedc6f-1d7b-4ab4-9421-27a82a75d160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc513b8d-9617-45aa-9713-65041782073b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2cebc148-0e16-48c5-b27e-bd72a6c23ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "cb702c18-c851-4710-bffc-ecad5c010ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cebc148-0e16-48c5-b27e-bd72a6c23ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a196f650-9a7c-4ede-84ca-b8f2038461cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cebc148-0e16-48c5-b27e-bd72a6c23ac0",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c9827909-2293-4181-949f-13acda4c0b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "aac6e2c5-bf52-4a71-a2d2-24eec957a61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9827909-2293-4181-949f-13acda4c0b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6d133e6-1db4-441a-9181-8df59a912d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9827909-2293-4181-949f-13acda4c0b38",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "eaf7fe62-6f2f-4c9c-be1a-818629b0e27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e9ac4525-8b30-4ad4-b469-12d99d9a0a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf7fe62-6f2f-4c9c-be1a-818629b0e27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafae801-7d22-4750-9d11-0833df046f32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf7fe62-6f2f-4c9c-be1a-818629b0e27f",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "831ffe98-ee7e-4554-8f89-a84dbdc4a5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "67fe8330-c565-4bd4-a00b-1844df92f929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "831ffe98-ee7e-4554-8f89-a84dbdc4a5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5177ef0-b4f0-4af7-8611-1c3faeef0f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "831ffe98-ee7e-4554-8f89-a84dbdc4a5df",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c3f1ba0d-f7a5-40dd-8072-9adbe3081241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "5c1c1821-b7f0-4785-8b0d-c8eed4a2f14e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f1ba0d-f7a5-40dd-8072-9adbe3081241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f074f04-26bf-44a0-ac96-cc99b7200559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f1ba0d-f7a5-40dd-8072-9adbe3081241",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "0ba3de9d-9a31-40ca-a30a-9cba30ac02ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "fcc0fd8b-8a3a-4b5c-b6a1-f72c5c9f9eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba3de9d-9a31-40ca-a30a-9cba30ac02ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c98bad-d5f8-439c-ac17-7134711b1c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba3de9d-9a31-40ca-a30a-9cba30ac02ab",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "6305bc54-1591-498e-99b1-162d2bfeb866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9e51e10b-b732-4195-8276-121f87f65809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6305bc54-1591-498e-99b1-162d2bfeb866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82f653a0-2664-4342-8601-278db967cc42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6305bc54-1591-498e-99b1-162d2bfeb866",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "8cba37ea-038a-4faf-b16d-24d3cfb9d5ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "0ce5199c-98c7-42ce-8472-c20b4a59ebc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cba37ea-038a-4faf-b16d-24d3cfb9d5ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10904c25-37f1-49c8-9ad6-7971c4901945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cba37ea-038a-4faf-b16d-24d3cfb9d5ef",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "298296cc-9605-4346-a92b-66a522f90a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1e3d3474-5dbb-4337-8bd0-19bc535253da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "298296cc-9605-4346-a92b-66a522f90a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c31f788-88ac-461a-9447-61008fc0b74a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298296cc-9605-4346-a92b-66a522f90a3b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "90e8d270-0e6f-4271-952a-9ee25c77a0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "d9297424-4c9a-4add-a3c9-bf5269eb743f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e8d270-0e6f-4271-952a-9ee25c77a0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "878792c9-ab57-4ce6-93de-e650c1b2685d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e8d270-0e6f-4271-952a-9ee25c77a0f6",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "6d0e3c14-3012-40a0-b9cc-723f65596b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e1e3ec89-8b2b-4b85-8eb3-434624947f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0e3c14-3012-40a0-b9cc-723f65596b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c4649a-3590-4db7-bdd6-84a531ee1f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0e3c14-3012-40a0-b9cc-723f65596b2d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "bfc2d084-3b5e-4b53-8371-30b07655b2a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "a68d4ccc-da7f-4fb3-8cc5-0f3bc356b40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfc2d084-3b5e-4b53-8371-30b07655b2a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e446f93a-0342-4671-8c6d-ed4b96b168d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfc2d084-3b5e-4b53-8371-30b07655b2a9",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2aa6f7c7-68be-4849-8ce4-e2c316cd9eec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "afd02961-b02a-4b48-aadf-4c4eee3c5908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa6f7c7-68be-4849-8ce4-e2c316cd9eec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86456e8-6c15-424a-9d7b-903af2719656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa6f7c7-68be-4849-8ce4-e2c316cd9eec",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "cae84314-563e-4a13-871d-1589b7a26782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "04952adf-9130-4dcc-b947-df60dc75cb28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cae84314-563e-4a13-871d-1589b7a26782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e447b2f-cff4-4a93-92cf-4a8c8979747d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cae84314-563e-4a13-871d-1589b7a26782",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "01d280b5-bbf7-4b56-9122-a510c8af5410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9b436898-f217-48f9-b46b-9cf26d511fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d280b5-bbf7-4b56-9122-a510c8af5410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c460dfae-b269-4961-ae7c-41e750252652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d280b5-bbf7-4b56-9122-a510c8af5410",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "742d0ac9-8159-47db-b383-2bb3348a914d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "5034bdd2-b7a0-4675-b71b-a987acfc3e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742d0ac9-8159-47db-b383-2bb3348a914d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "018550fd-0172-4f80-be77-45e491837f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742d0ac9-8159-47db-b383-2bb3348a914d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e93ec30d-864d-4a48-bd22-dc5a6c90afd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "8f56eb6b-95b8-482c-8c31-fe824785b743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e93ec30d-864d-4a48-bd22-dc5a6c90afd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf85c7c-5bd2-406c-964a-22489096af3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e93ec30d-864d-4a48-bd22-dc5a6c90afd5",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "86c3776b-2bd3-460d-9024-bb2600a4cc49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1b4c5d86-531e-4ce7-955c-f1b65c63fa3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86c3776b-2bd3-460d-9024-bb2600a4cc49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8b130f-be71-40ff-936f-1328df68f17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86c3776b-2bd3-460d-9024-bb2600a4cc49",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "d6c76b94-ba90-43e0-a780-f93b4d720d54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "66030948-7431-4c0d-8d66-61b53775ab15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c76b94-ba90-43e0-a780-f93b4d720d54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1254a4e7-21f6-47c1-8731-57010deec6e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c76b94-ba90-43e0-a780-f93b4d720d54",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2116961f-5246-4d36-b787-80b2edbcef25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "729d583d-9bc3-4135-ba75-7de195066231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2116961f-5246-4d36-b787-80b2edbcef25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d8cfa5-15b6-41c4-8c3e-4aa50e0be0b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2116961f-5246-4d36-b787-80b2edbcef25",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "fc88a67c-92eb-407f-aee8-585cde8c17a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "05880cf7-3bcc-4990-869c-5fd1371524e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc88a67c-92eb-407f-aee8-585cde8c17a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac122ea-6c6e-4e06-871c-7bd0cf859942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc88a67c-92eb-407f-aee8-585cde8c17a9",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "52e7ec64-7f60-4a77-a84d-77ca9446227e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "02746fe0-6e64-438c-b001-b00b19e29f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e7ec64-7f60-4a77-a84d-77ca9446227e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35d23f89-cc4e-4dfe-9f6b-f26e2c208afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e7ec64-7f60-4a77-a84d-77ca9446227e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "45a3a4f6-6308-418f-9c15-e78521ccf2a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "c6e911db-a76b-42bc-bc48-7fade6b209a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45a3a4f6-6308-418f-9c15-e78521ccf2a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad27ac45-c116-4e46-999c-a54d214fd76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45a3a4f6-6308-418f-9c15-e78521ccf2a6",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b22defa7-b244-4a17-8ada-6c1f9728623a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "b028aab8-06b8-4456-a9d8-0be9d48746cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22defa7-b244-4a17-8ada-6c1f9728623a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ff33da-cc1c-484d-9264-d91803086148",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22defa7-b244-4a17-8ada-6c1f9728623a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "90e655a7-5074-4228-8cc4-c030100f7725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9b5c4fc5-e22d-4f9d-b150-25eaccd0522e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e655a7-5074-4228-8cc4-c030100f7725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494479e8-eebb-47e6-b216-f4784576e431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e655a7-5074-4228-8cc4-c030100f7725",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "839e81c1-c9ca-4954-b62d-765cb4804206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "f115e4bf-7a4a-48fa-83b5-d2a37eb5ab17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839e81c1-c9ca-4954-b62d-765cb4804206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1254dc6e-fe38-4624-a803-d19eb5179ecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839e81c1-c9ca-4954-b62d-765cb4804206",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e0a42f7c-c258-4203-8f3a-1061a4cc592c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "8c010085-8a84-41e1-83ec-3d19de103e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0a42f7c-c258-4203-8f3a-1061a4cc592c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54cf672d-dc23-41bf-a3ef-11f32de0ebd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0a42f7c-c258-4203-8f3a-1061a4cc592c",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "26d1cb29-957d-4c3f-a6ae-0125b67b68ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9f769b83-b858-4821-837f-a356003c2838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26d1cb29-957d-4c3f-a6ae-0125b67b68ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442b6cbc-c727-49db-80fb-e3a00be44738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26d1cb29-957d-4c3f-a6ae-0125b67b68ad",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b54b9316-04f8-4307-bad9-835f625bb373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "36ce61ae-075b-4064-afde-3477324e49c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b54b9316-04f8-4307-bad9-835f625bb373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce49bc6-9357-4dc4-a3b2-296e19f01ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b54b9316-04f8-4307-bad9-835f625bb373",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c37f37cd-20cf-47d3-ae81-290725f0a7d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "260adc43-a054-41ab-95da-3f68e63eebb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37f37cd-20cf-47d3-ae81-290725f0a7d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ce3db2-072e-4c77-a25c-bdccb1b2e515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37f37cd-20cf-47d3-ae81-290725f0a7d7",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "fc3a1a94-b30b-422a-bcc8-851cce0d2132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9749ad17-b667-46e0-8c2f-9da3defa1a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3a1a94-b30b-422a-bcc8-851cce0d2132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d69605-c256-4898-95af-3ff5c09094ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3a1a94-b30b-422a-bcc8-851cce0d2132",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "bafad27d-e72c-47c2-ab04-9cc0cfafdd54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "610f618a-dad0-4337-8811-6f01959e1220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bafad27d-e72c-47c2-ab04-9cc0cfafdd54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5740792-b790-48d8-b728-c23c04c4fb68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bafad27d-e72c-47c2-ab04-9cc0cfafdd54",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e1664dfc-6f22-422c-9702-56e90657b340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "0443d822-64c5-43d5-b50a-0a060c80a581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1664dfc-6f22-422c-9702-56e90657b340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4920410b-065e-4b17-9a4d-457799e80cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1664dfc-6f22-422c-9702-56e90657b340",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "59c10d4c-8199-429d-8216-a21d5b17b41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "ffb72864-7402-4036-a44e-3054a22d57fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c10d4c-8199-429d-8216-a21d5b17b41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be978658-8620-4f07-8f07-d1a953f81881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c10d4c-8199-429d-8216-a21d5b17b41c",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f55ad30f-1534-439c-92c2-954fcb8ca958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "77890822-25d4-4926-8de9-574f6e3fea30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55ad30f-1534-439c-92c2-954fcb8ca958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9601c4-8dfc-4c2c-a2d8-d9ee3c3e390d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55ad30f-1534-439c-92c2-954fcb8ca958",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f4d1df8b-1a6c-4225-8650-2a04775013a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "18fd0e0f-0386-4fc6-aae3-67499852aa3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d1df8b-1a6c-4225-8650-2a04775013a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822eef52-13a0-42ef-8331-dd4dbd065809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d1df8b-1a6c-4225-8650-2a04775013a0",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "0971b3c4-0201-4225-bafd-13f30d6cb8ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "e7066572-59ec-4322-896d-607766f40881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0971b3c4-0201-4225-bafd-13f30d6cb8ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b7f5d6-40d7-47f5-b2bc-ed7da0eb0580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0971b3c4-0201-4225-bafd-13f30d6cb8ae",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c2528386-a05c-4a6e-a257-dde7b01fe451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "90c4d225-1981-447e-a643-cd707df3788d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2528386-a05c-4a6e-a257-dde7b01fe451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33981353-8860-4a2c-8293-2df0d6c6c3db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2528386-a05c-4a6e-a257-dde7b01fe451",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c6fb8bce-685f-44f0-a3cc-a7bf59c9d2c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9806f931-3823-4bb7-8957-0c2108df1819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6fb8bce-685f-44f0-a3cc-a7bf59c9d2c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f26e53-e617-45ad-9196-7a511bc94d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6fb8bce-685f-44f0-a3cc-a7bf59c9d2c9",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "52600df4-2e89-43cc-a8c3-3bb6e1f41c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "12fd1fbc-c9fd-4d23-9a08-068d3b6f3137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52600df4-2e89-43cc-a8c3-3bb6e1f41c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb9270cf-4d0e-4a1b-9637-8318d1b49890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52600df4-2e89-43cc-a8c3-3bb6e1f41c19",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "8c6f7779-501f-41e7-828d-fe9212e62cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1ff39ce2-370f-43bb-9a10-ea3461f2f796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6f7779-501f-41e7-828d-fe9212e62cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f759d95f-bfe9-40e0-8449-52bde66065b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6f7779-501f-41e7-828d-fe9212e62cde",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c0bad9bd-660c-4f1d-8285-ef9b04e1b09b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "6da17098-9d9c-4860-9301-1e584bd14ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0bad9bd-660c-4f1d-8285-ef9b04e1b09b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc898fc-27d7-410b-9dcf-14eb5aa0124b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0bad9bd-660c-4f1d-8285-ef9b04e1b09b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b0af7d4e-fcd7-4a02-b92a-9c1439f90bf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "d5b5c50c-775d-4a3c-9e46-1fa887da571c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0af7d4e-fcd7-4a02-b92a-9c1439f90bf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa525af0-f6ca-480c-bd30-02824e7b511e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0af7d4e-fcd7-4a02-b92a-9c1439f90bf8",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "ad4da483-73f0-4a3a-8392-53b15d213c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "c757d4fa-1185-40fe-ac61-22c7eeb640be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad4da483-73f0-4a3a-8392-53b15d213c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43c39a18-4e1f-4b4a-ac09-c6fba16ddb1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad4da483-73f0-4a3a-8392-53b15d213c14",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "75a246b2-917d-46e7-a873-b233df5b71a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "cd1e6e13-f5c9-4388-b42e-df1aa000dcfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a246b2-917d-46e7-a873-b233df5b71a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0165ac7-ec84-4ced-b493-dc841052ae52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a246b2-917d-46e7-a873-b233df5b71a5",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "458626ee-db24-4d6b-b86f-51564c994930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "82bb78e9-460a-43df-9895-a30d599458f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "458626ee-db24-4d6b-b86f-51564c994930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c0c2fb-8da3-498c-95ca-73fdeff66393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "458626ee-db24-4d6b-b86f-51564c994930",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "1da2f447-840a-4dca-8ed8-f71db9c82e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "db0949cd-3a51-4349-a1d6-6eab648c006f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da2f447-840a-4dca-8ed8-f71db9c82e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ae79782-1895-448a-aa4f-603322f15166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da2f447-840a-4dca-8ed8-f71db9c82e63",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f115ba52-8304-45d6-9ecf-f747fd57a96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "526fd426-a1ab-4452-818a-82524b76d941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f115ba52-8304-45d6-9ecf-f747fd57a96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45783e41-abe2-4553-b30e-dd6cf2d38a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f115ba52-8304-45d6-9ecf-f747fd57a96e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "a7ece4d2-5f68-43b3-8b0e-859564a87213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "1f7aa7ef-0847-421b-8449-d514df1ea71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ece4d2-5f68-43b3-8b0e-859564a87213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343d7d4e-ba15-4abf-a24b-785de70b1ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ece4d2-5f68-43b3-8b0e-859564a87213",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "af051dd9-9c00-42a8-b019-7c142763e2d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "99e8472e-4380-49c4-8c9a-a26a45e4cbec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af051dd9-9c00-42a8-b019-7c142763e2d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be55320b-67d5-455e-b7f8-770ff80a54ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af051dd9-9c00-42a8-b019-7c142763e2d5",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "0e742e9e-b4de-47e8-8895-ec4c4c454dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9b2973f1-8b56-4fbc-80df-0079c6dbb574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e742e9e-b4de-47e8-8895-ec4c4c454dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8758337-aaac-4c8f-8c54-1fe47526d9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e742e9e-b4de-47e8-8895-ec4c4c454dac",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "01239afc-3c27-47c5-ba5a-f9454d65745a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "2074fee4-e710-42f9-8ee2-730034199224",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01239afc-3c27-47c5-ba5a-f9454d65745a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87437a41-7f58-41e7-a2fa-175292f7456f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01239afc-3c27-47c5-ba5a-f9454d65745a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "973ba691-d16c-4365-bd06-3dddbe161efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "2113d353-2682-46fc-8a42-a35dc1110657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973ba691-d16c-4365-bd06-3dddbe161efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c4ea4e-2c62-49bd-a214-1fc601362bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973ba691-d16c-4365-bd06-3dddbe161efe",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "9eb3fa57-ed6d-4941-8709-1332b91096c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "686db477-28fc-4059-b725-e9ad7195270e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb3fa57-ed6d-4941-8709-1332b91096c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "821723fc-9b08-4108-868c-1b695aa6dae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb3fa57-ed6d-4941-8709-1332b91096c2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "3372eef4-47ad-4307-8155-9688375e0fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9ae6f573-45fe-4345-a045-62f5131f54d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3372eef4-47ad-4307-8155-9688375e0fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08e6a30-1f02-4915-a04f-c86e6353dbbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3372eef4-47ad-4307-8155-9688375e0fb1",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e56e1eb0-d6b3-44a6-ada3-68469e660070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "019463c3-89d7-4ef7-9cd2-49f6d428eb39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56e1eb0-d6b3-44a6-ada3-68469e660070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85dcc559-5dcf-4809-b165-21224cfafb1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56e1eb0-d6b3-44a6-ada3-68469e660070",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "a0b7a494-f6b2-4377-9adf-9ad4b228a814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "c4646915-e164-442e-8822-9d2134b0ddab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b7a494-f6b2-4377-9adf-9ad4b228a814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b65f6e3-d3df-4525-bf7c-5ffb98f63e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b7a494-f6b2-4377-9adf-9ad4b228a814",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "03e1e9c2-faa5-47c5-9322-a7077be71bb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "dc4ac628-3c56-4eaf-8efc-cfd606578e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e1e9c2-faa5-47c5-9322-a7077be71bb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f88e2d-b686-431a-b66a-ca7dda1edb15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e1e9c2-faa5-47c5-9322-a7077be71bb9",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "243af00e-c091-40cc-ac9e-f4d8e1418c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "47c4da06-011b-4705-9749-e0759ecebc46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "243af00e-c091-40cc-ac9e-f4d8e1418c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d301f4c-4020-4e88-b664-5c92cc280c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "243af00e-c091-40cc-ac9e-f4d8e1418c60",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "edb50ef1-0884-4040-97aa-51b71abfbb02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "77960c95-1ca9-46b7-a4c1-9742aac442f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb50ef1-0884-4040-97aa-51b71abfbb02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78e9e44-9736-4934-9318-f7acc1497d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb50ef1-0884-4040-97aa-51b71abfbb02",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "132af8a4-9586-4da9-9a07-154485f98897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "25f2572d-02af-496b-95f7-6ee326dc83ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132af8a4-9586-4da9-9a07-154485f98897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c845bd9-ca42-4b66-8a64-b9d1b8ed6e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132af8a4-9586-4da9-9a07-154485f98897",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f547a2bb-1b73-4832-9897-ec58ebb04c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "da53edcc-0bf1-447d-a2f3-9c445c742d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f547a2bb-1b73-4832-9897-ec58ebb04c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb75133e-a2b6-4d2e-90c2-6beab7880dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f547a2bb-1b73-4832-9897-ec58ebb04c13",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c6ff427e-a933-4259-919b-25e97217b2c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "41b59f7d-e430-4ebe-9dc8-b79ca09f5510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ff427e-a933-4259-919b-25e97217b2c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98aeea2f-b10a-4463-a93b-3596149bc64b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ff427e-a933-4259-919b-25e97217b2c2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "69826b6d-4372-4641-b012-7ca02ac80b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "5993f537-c5c5-432a-a0c2-d70c7c2d4b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69826b6d-4372-4641-b012-7ca02ac80b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3a68ad9-51ed-4d1f-825e-f076e0ada482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69826b6d-4372-4641-b012-7ca02ac80b9a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "dbe70403-c2f5-4ace-9e7a-aa100029a4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9dce6b92-be5b-4ab5-9052-2b249944509d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe70403-c2f5-4ace-9e7a-aa100029a4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e57800-015a-4a48-a25e-f28b009334ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe70403-c2f5-4ace-9e7a-aa100029a4a5",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "ce8cc192-98fb-41d3-9e80-0b60a2f8f08d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "8fd77cb6-7b85-4f0d-86c7-4c0789b30868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8cc192-98fb-41d3-9e80-0b60a2f8f08d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "546eee1f-eca3-47ca-85ef-6130c36060e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8cc192-98fb-41d3-9e80-0b60a2f8f08d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "08ab61f0-a164-431e-8e31-ff27dba75c11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "cec6fb1e-9e88-4aab-81eb-b68b42af8473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ab61f0-a164-431e-8e31-ff27dba75c11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae25ade-5c4e-403e-b1c4-c7c4b6ac6b52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ab61f0-a164-431e-8e31-ff27dba75c11",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "151ab1de-e429-4e5b-94f7-5f95c1e9f2b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9d4c5ae0-2f3e-4657-a1d9-251a9efb0bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "151ab1de-e429-4e5b-94f7-5f95c1e9f2b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a0f3493-cd7d-43e5-8b00-7040ce3e90f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "151ab1de-e429-4e5b-94f7-5f95c1e9f2b9",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "4a6518a0-233b-47f5-b58c-65fd59b4863b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "b1140442-42ae-4c7b-b472-1f18d94b3a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6518a0-233b-47f5-b58c-65fd59b4863b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b562f4c2-1a83-4e6d-8b06-68678aa14459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6518a0-233b-47f5-b58c-65fd59b4863b",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "e14ca688-e8c2-4989-9586-441a3220ffb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "997dd44a-2a8f-4d46-8454-6854055796aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e14ca688-e8c2-4989-9586-441a3220ffb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "825f97e9-a0ec-4b23-8a8c-1cc6d1c16db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e14ca688-e8c2-4989-9586-441a3220ffb6",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "2f7758e0-7791-4a72-8f12-04685cc4561d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "a4119c7e-2c6b-4250-9780-94a5dbbedaf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7758e0-7791-4a72-8f12-04685cc4561d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c4e4897-2b81-4450-a322-e0142c745ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7758e0-7791-4a72-8f12-04685cc4561d",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "a44fc762-f43e-4aba-85fa-7e273612a04e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "26bce779-16f2-43d9-a72d-8ba3c0950a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44fc762-f43e-4aba-85fa-7e273612a04e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d710561-bab3-41a4-a640-edd3210b6738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44fc762-f43e-4aba-85fa-7e273612a04e",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "0fe202fd-5779-4c28-b296-7a84590fe2ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "bc6aa22f-8cb6-4eb5-b362-cb95d6ef9b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe202fd-5779-4c28-b296-7a84590fe2ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bfc7fec-7db0-4f06-8a6f-fba1c1e05603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe202fd-5779-4c28-b296-7a84590fe2ee",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "d4aa2690-296f-4bcd-a877-085ca00c2991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "191952b5-2ed4-4b84-a361-30373c65361c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4aa2690-296f-4bcd-a877-085ca00c2991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051ee24f-3830-4377-9398-cc914ef73f1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4aa2690-296f-4bcd-a877-085ca00c2991",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f478d1b9-43e6-4cef-a210-ed5151452856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "4ec57fe2-22bb-4ca3-ae9c-adcf029e1ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f478d1b9-43e6-4cef-a210-ed5151452856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3274d130-544c-46ee-984d-be4eedbbc718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f478d1b9-43e6-4cef-a210-ed5151452856",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "be48e8a5-68bd-41cd-9432-10a6394cedab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "beb8e357-63f0-4d15-ba34-f8d91ae75c75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be48e8a5-68bd-41cd-9432-10a6394cedab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6429c6b-9999-426f-b4a6-c54258a19467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be48e8a5-68bd-41cd-9432-10a6394cedab",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "ab8f66e0-339b-48da-9bff-7600037080a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "cff47518-21cc-4172-8ec4-a64558df9389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab8f66e0-339b-48da-9bff-7600037080a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c41a6c3-02a3-482d-bc0f-df93ea60f5bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab8f66e0-339b-48da-9bff-7600037080a2",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "c33d7e64-be09-41b5-b4b6-457b12752b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "56efe18d-2081-4d50-9a5c-5e78c9f451be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33d7e64-be09-41b5-b4b6-457b12752b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff98a25-d55c-4f23-925c-bab82ffcee3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33d7e64-be09-41b5-b4b6-457b12752b9a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "efd99a34-c785-4609-bc67-599184a36756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "65a409be-82de-43ca-81ef-57300af121e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efd99a34-c785-4609-bc67-599184a36756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac65768-2c68-49f2-9c97-44cfbac47b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efd99a34-c785-4609-bc67-599184a36756",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "93a457ed-1d84-4a01-9608-85cb8a85cbbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "20d382ab-5e62-45c2-bbb5-4e86ee4e5e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a457ed-1d84-4a01-9608-85cb8a85cbbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77eb20f3-21df-4822-befc-fd2348cbc6eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a457ed-1d84-4a01-9608-85cb8a85cbbd",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "6557d611-9d73-4537-bca0-d2a5f301239f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "b0a4f114-83d7-4184-87f5-5c5cad042429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6557d611-9d73-4537-bca0-d2a5f301239f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8377d940-01c6-464c-b385-ddeeb00d92f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6557d611-9d73-4537-bca0-d2a5f301239f",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "b94aa90c-12c2-4afe-a78e-ff88b4b2da2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "9f65e212-157d-4b2b-881b-49549ebea3ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94aa90c-12c2-4afe-a78e-ff88b4b2da2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d6b710-5cd1-426a-b41c-9976adc14564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94aa90c-12c2-4afe-a78e-ff88b4b2da2a",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "5970b02e-4848-4ecb-a7e0-818c4ebc96fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "39f0f80d-9979-48c1-9fde-2f596ccabcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5970b02e-4848-4ecb-a7e0-818c4ebc96fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef921c5b-2350-4c11-bdf6-58c0b98ecc2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5970b02e-4848-4ecb-a7e0-818c4ebc96fa",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "194416e1-f8e5-4a80-847d-e3a8f4b5c057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "461fdc09-a4a5-4679-9fae-ff344498a3fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "194416e1-f8e5-4a80-847d-e3a8f4b5c057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a6f5825-7b78-4622-957f-a9a82cc26b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "194416e1-f8e5-4a80-847d-e3a8f4b5c057",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "aa67831a-2dfd-4f67-bc66-03027de92289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "2b69b84b-1fc1-444f-a3c2-bc914cb4ab83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa67831a-2dfd-4f67-bc66-03027de92289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f86e81-1d3a-41da-b756-76f13d8e53b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa67831a-2dfd-4f67-bc66-03027de92289",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        },
        {
            "id": "f959ca7a-4ad9-42a9-8e83-b62ef6306d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "compositeImage": {
                "id": "7f4566aa-2e75-4d4e-b56d-81886ed058b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f959ca7a-4ad9-42a9-8e83-b62ef6306d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b209b6b-5df5-46f4-898f-6aff418fadae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f959ca7a-4ad9-42a9-8e83-b62ef6306d2c",
                    "LayerId": "f479e388-9ac6-4929-81b1-2a795249de0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "f479e388-9ac6-4929-81b1-2a795249de0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e11916ad-2666-4b66-a4e8-e494b288e5cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}