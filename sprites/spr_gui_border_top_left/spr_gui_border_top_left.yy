{
    "id": "69d2d596-10ef-43aa-9509-2ba7e926a428",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 0,
    "bbox_right": 144,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f6d9a3a-df09-47c7-a90a-fc5ea6e4d483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69d2d596-10ef-43aa-9509-2ba7e926a428",
            "compositeImage": {
                "id": "2899ffc6-2006-4616-9ac2-dbd2c536e098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f6d9a3a-df09-47c7-a90a-fc5ea6e4d483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d161064d-5215-4eb4-a48d-2f17886c012b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f6d9a3a-df09-47c7-a90a-fc5ea6e4d483",
                    "LayerId": "1ae406e6-9124-408a-a41b-3f06bbd28b44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "1ae406e6-9124-408a-a41b-3f06bbd28b44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69d2d596-10ef-43aa-9509-2ba7e926a428",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 145,
    "xorig": 26,
    "yorig": 26
}