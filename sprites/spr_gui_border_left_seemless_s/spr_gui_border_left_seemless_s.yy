{
    "id": "fa28bf4a-f34d-4a21-8526-dae5dbc8c466",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_left_seemless_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 1,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25595193-4967-4c5a-a604-77a19b04c24a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa28bf4a-f34d-4a21-8526-dae5dbc8c466",
            "compositeImage": {
                "id": "532ae44b-47df-4a22-b52b-cc9b7d7ff6cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25595193-4967-4c5a-a604-77a19b04c24a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "318a973a-31b7-4120-bb1d-47fa5e201b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25595193-4967-4c5a-a604-77a19b04c24a",
                    "LayerId": "4709d0c8-de94-4ad3-b39a-7e2c667e0880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "4709d0c8-de94-4ad3-b39a-7e2c667e0880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa28bf4a-f34d-4a21-8526-dae5dbc8c466",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 15
}