{
    "id": "4bbf2cb5-e19d-4acb-a481-cfc793ae2bcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_yellowgreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8088e3a-6484-4d31-8217-09c52222fc68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bbf2cb5-e19d-4acb-a481-cfc793ae2bcb",
            "compositeImage": {
                "id": "b16171ef-3675-4cc8-beee-f9d7a2eaabec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8088e3a-6484-4d31-8217-09c52222fc68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db30e6b-adbd-48f6-bac9-e7e9699da95b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8088e3a-6484-4d31-8217-09c52222fc68",
                    "LayerId": "44344284-8a0c-4f13-bee1-3381338f981f"
                }
            ]
        },
        {
            "id": "1ef07695-70b6-4884-b7b3-82e2eda2570f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bbf2cb5-e19d-4acb-a481-cfc793ae2bcb",
            "compositeImage": {
                "id": "84298ac9-2f9b-47ac-a986-4ca2e500972b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef07695-70b6-4884-b7b3-82e2eda2570f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f704d2-cb55-436d-8341-5b42bec3a1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef07695-70b6-4884-b7b3-82e2eda2570f",
                    "LayerId": "44344284-8a0c-4f13-bee1-3381338f981f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "44344284-8a0c-4f13-bee1-3381338f981f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bbf2cb5-e19d-4acb-a481-cfc793ae2bcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}