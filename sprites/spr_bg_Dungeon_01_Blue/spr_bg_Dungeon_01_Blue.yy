{
    "id": "ef8bca3f-112a-481f-af04-5d2cc65a97c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Dungeon_01_Blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f85a174-b29a-4362-bcb3-ed3b9f5a4fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef8bca3f-112a-481f-af04-5d2cc65a97c9",
            "compositeImage": {
                "id": "66e0687b-a8b6-4d19-a662-9353613fac59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f85a174-b29a-4362-bcb3-ed3b9f5a4fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2edfacfb-ec57-4073-b292-a25ac333df4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f85a174-b29a-4362-bcb3-ed3b9f5a4fea",
                    "LayerId": "a0aab155-213a-4255-8989-46d104031405"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "a0aab155-213a-4255-8989-46d104031405",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef8bca3f-112a-481f-af04-5d2cc65a97c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}