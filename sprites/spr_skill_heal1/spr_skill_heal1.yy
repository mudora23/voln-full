{
    "id": "a070ea8b-d239-4d47-8ccb-ccc4b2a9a6d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_heal1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fae7302-1066-4fa6-8872-6db2c63101f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a070ea8b-d239-4d47-8ccb-ccc4b2a9a6d6",
            "compositeImage": {
                "id": "4e506e80-925c-48d8-83dd-f3ea082f6ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fae7302-1066-4fa6-8872-6db2c63101f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "477631e8-36ed-4b10-8afc-15fcc2183270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fae7302-1066-4fa6-8872-6db2c63101f1",
                    "LayerId": "8628943c-33b2-43ae-bdc3-bf143446f8f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8628943c-33b2-43ae-bdc3-bf143446f8f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a070ea8b-d239-4d47-8ccb-ccc4b2a9a6d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}