{
    "id": "37f7a1fc-6146-49aa-9a93-d70e478273f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_clipboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 8,
    "bbox_right": 41,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "380434d3-2b07-47e1-b123-ccac7d2927d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f7a1fc-6146-49aa-9a93-d70e478273f5",
            "compositeImage": {
                "id": "ca407cf6-7485-4aef-82a0-be9681cd16d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380434d3-2b07-47e1-b123-ccac7d2927d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef6ffd7-0670-4e35-b8f6-fbdf4ebf02b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380434d3-2b07-47e1-b123-ccac7d2927d7",
                    "LayerId": "d52d753c-87ad-48dc-97f9-4636d13c61fa"
                }
            ]
        },
        {
            "id": "39c66d7f-48bb-450b-bd59-16f740c88c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37f7a1fc-6146-49aa-9a93-d70e478273f5",
            "compositeImage": {
                "id": "cf5067a6-6ad2-48ed-a85a-7dbeb134ce12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c66d7f-48bb-450b-bd59-16f740c88c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5615274b-14eb-4f09-89a6-8f59349a0d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c66d7f-48bb-450b-bd59-16f740c88c19",
                    "LayerId": "d52d753c-87ad-48dc-97f9-4636d13c61fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "d52d753c-87ad-48dc-97f9-4636d13c61fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37f7a1fc-6146-49aa-9a93-d70e478273f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}