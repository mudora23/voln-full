{
    "id": "22d0944e-58d1-44b5-9ced-ca19f7f78eb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1958,
    "bbox_left": 50,
    "bbox_right": 1168,
    "bbox_top": 108,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5db011f5-28dd-4d4c-9f53-8482ce5dbd91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22d0944e-58d1-44b5-9ced-ca19f7f78eb4",
            "compositeImage": {
                "id": "e6c901c5-0c20-4a3a-b9fd-cc1ab541824e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db011f5-28dd-4d4c-9f53-8482ce5dbd91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb6816d-5ab7-4d76-9fb8-b91ba1613efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db011f5-28dd-4d4c-9f53-8482ce5dbd91",
                    "LayerId": "3115e3c1-c2b1-43c4-a4b0-da2fdd1bc429"
                }
            ]
        },
        {
            "id": "90d245bc-6e61-4c4a-b991-b38b30bd2c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22d0944e-58d1-44b5-9ced-ca19f7f78eb4",
            "compositeImage": {
                "id": "789ad5e7-e7ce-4586-ba94-643f7378cd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d245bc-6e61-4c4a-b991-b38b30bd2c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4dbb5e-335a-43b3-9808-56740abc04cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d245bc-6e61-4c4a-b991-b38b30bd2c84",
                    "LayerId": "3115e3c1-c2b1-43c4-a4b0-da2fdd1bc429"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1971,
    "layers": [
        {
            "id": "3115e3c1-c2b1-43c4-a4b0-da2fdd1bc429",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22d0944e-58d1-44b5-9ced-ca19f7f78eb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1169,
    "xorig": 0,
    "yorig": 0
}