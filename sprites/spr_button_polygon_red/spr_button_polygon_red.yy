{
    "id": "81b43cfa-6e28-47b4-904f-bf3f72c324c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_polygon_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7f0ac75-dab6-4eef-a8a3-382a188ed38a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b43cfa-6e28-47b4-904f-bf3f72c324c1",
            "compositeImage": {
                "id": "7bbafe32-24e9-43de-9e87-ab32e973dcd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f0ac75-dab6-4eef-a8a3-382a188ed38a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523e4262-833e-45cf-831c-03353f170d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f0ac75-dab6-4eef-a8a3-382a188ed38a",
                    "LayerId": "b04cdf6c-894a-4f7a-a096-3218dab16d6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "b04cdf6c-894a-4f7a-a096-3218dab16d6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81b43cfa-6e28-47b4-904f-bf3f72c324c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 35
}