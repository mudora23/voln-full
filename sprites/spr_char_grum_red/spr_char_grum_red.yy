{
    "id": "40a5890b-6d2e-4597-89e7-adde011fc6f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fed5216-7878-4efc-9d10-6cb630215469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a5890b-6d2e-4597-89e7-adde011fc6f0",
            "compositeImage": {
                "id": "3afb2aff-5d56-4693-8171-690a1873cc78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fed5216-7878-4efc-9d10-6cb630215469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1646596-e626-41c3-a91f-68aa9b8d2cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fed5216-7878-4efc-9d10-6cb630215469",
                    "LayerId": "1be49792-ec03-4ef3-b386-6139b9ae9e08"
                }
            ]
        },
        {
            "id": "f2e5a1d3-f43f-4c20-85c5-dff5d0415eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a5890b-6d2e-4597-89e7-adde011fc6f0",
            "compositeImage": {
                "id": "160c11fc-42f6-40f6-ba98-63ebfb8a1b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e5a1d3-f43f-4c20-85c5-dff5d0415eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a0ef692-8d18-4e78-b2a4-388f28ac334c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e5a1d3-f43f-4c20-85c5-dff5d0415eae",
                    "LayerId": "1be49792-ec03-4ef3-b386-6139b9ae9e08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "1be49792-ec03-4ef3-b386-6139b9ae9e08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40a5890b-6d2e-4597-89e7-adde011fc6f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}