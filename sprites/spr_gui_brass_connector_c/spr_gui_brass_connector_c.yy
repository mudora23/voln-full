{
    "id": "2ea6dc0b-ef34-4ee1-b2be-6641ab71871e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_brass_connector_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2951d095-fa33-428d-ac06-60b63893ccf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea6dc0b-ef34-4ee1-b2be-6641ab71871e",
            "compositeImage": {
                "id": "e2ab3dbf-4a33-47e9-a02d-e22439644048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2951d095-fa33-428d-ac06-60b63893ccf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f0ea82-6915-4505-9c4a-affc18ab9b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2951d095-fa33-428d-ac06-60b63893ccf3",
                    "LayerId": "71d14402-0f55-4c5d-9a67-71033845117c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "71d14402-0f55-4c5d-9a67-71033845117c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ea6dc0b-ef34-4ee1-b2be-6641ab71871e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 53,
    "yorig": 20
}