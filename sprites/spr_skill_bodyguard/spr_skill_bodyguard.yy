{
    "id": "fd7fc9f6-5be6-4511-953a-3ec839dc2424",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_bodyguard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 188,
    "bbox_left": 11,
    "bbox_right": 188,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3ff9f3e-4ee8-4da6-9e09-09ed339b5d70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd7fc9f6-5be6-4511-953a-3ec839dc2424",
            "compositeImage": {
                "id": "9f62367c-96e7-4f53-ae6e-e10753c358a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ff9f3e-4ee8-4da6-9e09-09ed339b5d70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf996df-e67a-425c-87fa-fa27ff044196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ff9f3e-4ee8-4da6-9e09-09ed339b5d70",
                    "LayerId": "a2ced3ea-ba39-4721-8aa7-68b94f5a5fac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "a2ced3ea-ba39-4721-8aa7-68b94f5a5fac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd7fc9f6-5be6-4511-953a-3ec839dc2424",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}