{
    "id": "8b409932-0742-4ded-8069-613538dbfa9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_fireball1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8c299ce-a7f2-46d7-aab2-70f87e10be5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b409932-0742-4ded-8069-613538dbfa9d",
            "compositeImage": {
                "id": "b19b9865-16b6-46b7-9b77-8039f83d5502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8c299ce-a7f2-46d7-aab2-70f87e10be5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc257169-c3d9-4b86-932d-77c3badad6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8c299ce-a7f2-46d7-aab2-70f87e10be5a",
                    "LayerId": "05744f50-a872-43d5-893f-e51842616bf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "05744f50-a872-43d5-893f-e51842616bf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b409932-0742-4ded-8069-613538dbfa9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}