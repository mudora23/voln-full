{
    "id": "558a4b2b-8246-437e-8ddd-4047f3fa09cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_toru_spi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "522d5638-aeb2-4ed7-8652-36f69e550eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "558a4b2b-8246-437e-8ddd-4047f3fa09cc",
            "compositeImage": {
                "id": "e6a96b1e-2886-4664-b163-c2b2188ca2d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522d5638-aeb2-4ed7-8652-36f69e550eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe001a9e-617b-47d7-9669-a79e6c327aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522d5638-aeb2-4ed7-8652-36f69e550eaf",
                    "LayerId": "f885a34c-2b93-4829-9224-960c82ca9d83"
                },
                {
                    "id": "b33e8705-2679-4bee-a060-5a991638275c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522d5638-aeb2-4ed7-8652-36f69e550eaf",
                    "LayerId": "1670c15b-52ff-403c-a1d8-27d827b44d0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "f885a34c-2b93-4829-9224-960c82ca9d83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "558a4b2b-8246-437e-8ddd-4047f3fa09cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1670c15b-52ff-403c-a1d8-27d827b44d0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "558a4b2b-8246-437e-8ddd-4047f3fa09cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}