{
    "id": "bbb0bac9-3baf-4803-9893-7177d6eb2d45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 0,
    "bbox_right": 144,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df3aa2e9-2ac5-418e-9bb5-b71537a6e56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbb0bac9-3baf-4803-9893-7177d6eb2d45",
            "compositeImage": {
                "id": "0c252a59-5796-4966-8c8f-661df87944a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3aa2e9-2ac5-418e-9bb5-b71537a6e56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76c5d29d-1d19-4977-aa89-de8ccfa175e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3aa2e9-2ac5-418e-9bb5-b71537a6e56f",
                    "LayerId": "e40ddce7-8912-437a-85ab-63b9b7331095"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 148,
    "layers": [
        {
            "id": "e40ddce7-8912-437a-85ab-63b9b7331095",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbb0bac9-3baf-4803-9893-7177d6eb2d45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 145,
    "xorig": 118,
    "yorig": 26
}