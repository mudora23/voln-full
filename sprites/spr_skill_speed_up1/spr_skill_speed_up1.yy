{
    "id": "e3e6493b-afa9-47b2-8cab-2bd424c8706e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_speed_up1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 1,
    "bbox_right": 99,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97a15aeb-deef-4271-a4bd-898eeba03fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3e6493b-afa9-47b2-8cab-2bd424c8706e",
            "compositeImage": {
                "id": "79960eba-53d3-46ca-8f05-41e7da498c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a15aeb-deef-4271-a4bd-898eeba03fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c2d2b1-943f-4518-a4b4-3188a77c4f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a15aeb-deef-4271-a4bd-898eeba03fcd",
                    "LayerId": "8f1b6ffe-a709-4d2f-b0be-584c8c8ef4f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8f1b6ffe-a709-4d2f-b0be-584c8c8ef4f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3e6493b-afa9-47b2-8cab-2bd424c8706e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}