{
    "id": "5c438a08-5c28-4f26-8e3a-09a87e9b3021",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_barrier1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4fcd5ac-a71c-4a0e-9070-39a67bd4d604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c438a08-5c28-4f26-8e3a-09a87e9b3021",
            "compositeImage": {
                "id": "99979b2c-3295-4497-b956-497dba90eb58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4fcd5ac-a71c-4a0e-9070-39a67bd4d604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aacc3e9f-59f1-4083-810a-af92b5b516a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4fcd5ac-a71c-4a0e-9070-39a67bd4d604",
                    "LayerId": "bb98bbd0-661d-4fc5-b081-3b3385476dd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "bb98bbd0-661d-4fc5-b081-3b3385476dd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c438a08-5c28-4f26-8e3a-09a87e9b3021",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}