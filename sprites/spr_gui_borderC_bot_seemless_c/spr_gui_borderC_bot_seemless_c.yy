{
    "id": "a6330b0b-5fee-47b6-9553-a9e835b36c3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_bot_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13cc47cb-5e89-4a15-bf5b-597f1ed8cdc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6330b0b-5fee-47b6-9553-a9e835b36c3e",
            "compositeImage": {
                "id": "0fd45c50-2ae3-4c9d-a091-b7feed446e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13cc47cb-5e89-4a15-bf5b-597f1ed8cdc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b775b2b3-2e6d-47f2-8492-9692ff97d837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13cc47cb-5e89-4a15-bf5b-597f1ed8cdc2",
                    "LayerId": "7165f07d-da1a-4494-bb8b-55aec3da26a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "7165f07d-da1a-4494-bb8b-55aec3da26a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6330b0b-5fee-47b6-9553-a9e835b36c3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 1
}