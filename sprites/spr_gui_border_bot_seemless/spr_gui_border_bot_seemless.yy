{
    "id": "35f0e07c-0bd9-430c-9aed-5f2a31eee13b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_seemless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90baa534-6bd0-4594-b345-b83d3604e0c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35f0e07c-0bd9-430c-9aed-5f2a31eee13b",
            "compositeImage": {
                "id": "cb172642-115d-470f-9853-c3b226c1344d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90baa534-6bd0-4594-b345-b83d3604e0c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "614667af-3156-4038-a1d1-60fb8186e717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90baa534-6bd0-4594-b345-b83d3604e0c0",
                    "LayerId": "76a1b6af-9eee-4a0b-bf32-255d1c48e9e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "76a1b6af-9eee-4a0b-bf32-255d1c48e9e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35f0e07c-0bd9-430c-9aed-5f2a31eee13b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 14
}