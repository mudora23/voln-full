{
    "id": "64e155b7-61fb-4f26-8898-ea81c3b74485",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_black_full",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c062346a-4fa8-4afc-85d7-342ba7c2a112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e155b7-61fb-4f26-8898-ea81c3b74485",
            "compositeImage": {
                "id": "22e0caaf-905f-4638-b507-2a1aa62a2231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c062346a-4fa8-4afc-85d7-342ba7c2a112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f6bc42-fe8d-4ec5-ba83-4f24fe58f504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c062346a-4fa8-4afc-85d7-342ba7c2a112",
                    "LayerId": "df5d6e08-ab7b-4692-b8cd-0eb9162be07f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "df5d6e08-ab7b-4692-b8cd-0eb9162be07f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64e155b7-61fb-4f26-8898-ea81c3b74485",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}