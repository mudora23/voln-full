{
    "id": "9a22489f-360c-402f-98a2-b79e72942518",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_yelloworange_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec1f0361-7e0e-4ba8-ae62-bfc81e2640f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a22489f-360c-402f-98a2-b79e72942518",
            "compositeImage": {
                "id": "9ea950a8-7b94-4174-848a-0fde50c70675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec1f0361-7e0e-4ba8-ae62-bfc81e2640f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "169aabda-bbb2-4753-b3ed-9cb7ed208c0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec1f0361-7e0e-4ba8-ae62-bfc81e2640f3",
                    "LayerId": "96d9650e-ed49-4918-abcc-269ba6815a23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "96d9650e-ed49-4918-abcc-269ba6815a23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a22489f-360c-402f-98a2-b79e72942518",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}