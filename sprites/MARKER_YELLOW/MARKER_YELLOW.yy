{
    "id": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MARKER_YELLOW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2870c855-df2b-4e53-b800-2d22957c001b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
            "compositeImage": {
                "id": "34f3670f-b523-404e-a7fc-0e0056e67887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2870c855-df2b-4e53-b800-2d22957c001b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60b00f7d-6db5-4097-83bf-10c43e07abb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2870c855-df2b-4e53-b800-2d22957c001b",
                    "LayerId": "834aa4a5-92fd-4c35-abca-f97936ea0987"
                }
            ]
        },
        {
            "id": "1d96ce0f-cb7e-4f0e-a5ac-91acb31f7d1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
            "compositeImage": {
                "id": "60c709e0-035f-4820-b101-7a6b8851b7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d96ce0f-cb7e-4f0e-a5ac-91acb31f7d1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbe353a8-c97b-4dec-a4ca-abff37ca33c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d96ce0f-cb7e-4f0e-a5ac-91acb31f7d1e",
                    "LayerId": "834aa4a5-92fd-4c35-abca-f97936ea0987"
                }
            ]
        },
        {
            "id": "f457dfca-4eff-4351-be31-a5e43da2be17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
            "compositeImage": {
                "id": "9544403f-b91d-4da7-b945-b3d383a3a692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f457dfca-4eff-4351-be31-a5e43da2be17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02b0311a-2245-4387-9a66-63d67739dd8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f457dfca-4eff-4351-be31-a5e43da2be17",
                    "LayerId": "834aa4a5-92fd-4c35-abca-f97936ea0987"
                }
            ]
        },
        {
            "id": "8f4a104f-0820-43ce-94bb-78c14e9f461e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
            "compositeImage": {
                "id": "3c4453d3-498e-4156-9993-9dc4bb92032f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4a104f-0820-43ce-94bb-78c14e9f461e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad85f5d-e4d3-4603-9a63-d820d801f16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4a104f-0820-43ce-94bb-78c14e9f461e",
                    "LayerId": "834aa4a5-92fd-4c35-abca-f97936ea0987"
                }
            ]
        },
        {
            "id": "ade3d0d2-cc97-4c62-8dc3-9d50c11ee531",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
            "compositeImage": {
                "id": "85800301-2a49-4123-8d4a-9c5d617f6f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ade3d0d2-cc97-4c62-8dc3-9d50c11ee531",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ecd2594-166d-45d9-83b5-ad945c85d11e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ade3d0d2-cc97-4c62-8dc3-9d50c11ee531",
                    "LayerId": "834aa4a5-92fd-4c35-abca-f97936ea0987"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "834aa4a5-92fd-4c35-abca-f97936ea0987",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e9601ae-a0d0-4588-b72a-d36d918eb487",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 0
}