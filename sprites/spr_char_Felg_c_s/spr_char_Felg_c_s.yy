{
    "id": "e2b811fc-ad01-40e6-b79b-be8c45aa4d70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Felg_c_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1959,
    "bbox_left": 126,
    "bbox_right": 958,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d76170f1-380b-4052-b8ce-dfd2891cf970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2b811fc-ad01-40e6-b79b-be8c45aa4d70",
            "compositeImage": {
                "id": "389e15a5-6542-4530-a4fd-1f2594143f87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76170f1-380b-4052-b8ce-dfd2891cf970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b6e9e19-d38a-4177-a2b0-fa4ef407a380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76170f1-380b-4052-b8ce-dfd2891cf970",
                    "LayerId": "76e8a55b-da62-42b0-aaeb-f22d9a8ac32c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "76e8a55b-da62-42b0-aaeb-f22d9a8ac32c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2b811fc-ad01-40e6-b79b-be8c45aa4d70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}