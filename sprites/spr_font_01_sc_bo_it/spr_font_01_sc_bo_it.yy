{
    "id": "28688a53-f43a-419e-b23c-2f905f59b97c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_sc_bo_it",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c18dfeb5-2cd9-424e-b680-f3391a0dbbde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "9335b4ef-7896-4222-8b50-6432a22dfd71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c18dfeb5-2cd9-424e-b680-f3391a0dbbde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87223106-96a9-4260-ba80-7ea9983090cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c18dfeb5-2cd9-424e-b680-f3391a0dbbde",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "171014b7-a7ab-4f2f-b446-9e2fd0a0e7db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "713dc393-b621-46a9-8a98-27feb43cf4a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171014b7-a7ab-4f2f-b446-9e2fd0a0e7db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4df1919-da44-4b06-851a-90cf816a33d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171014b7-a7ab-4f2f-b446-9e2fd0a0e7db",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "b68370c0-7dfd-4040-a74c-e6c9fd43e328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a5063244-284c-4059-aeab-06a7289aac6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68370c0-7dfd-4040-a74c-e6c9fd43e328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a2a26f5-d67b-435e-aeab-fa59f1b8a3ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68370c0-7dfd-4040-a74c-e6c9fd43e328",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "9b8651f9-783d-4124-8d6c-019d9ed0ddf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "dbe86003-18ee-45a5-9d56-aac8690e51ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b8651f9-783d-4124-8d6c-019d9ed0ddf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d7d43c-d36d-4ff2-9cbf-2058109bed1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b8651f9-783d-4124-8d6c-019d9ed0ddf3",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "70302f50-d263-460c-a667-ddcbf00547b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "88be1b72-f3d9-4567-ad4b-c3b6ecac027c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70302f50-d263-460c-a667-ddcbf00547b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e0bdee-3221-4944-a33d-56f4274329b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70302f50-d263-460c-a667-ddcbf00547b7",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "254cd0a4-88fa-45e7-a930-7f3b8c15ba96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "7b016d7a-571e-4162-acc7-b11698a7bfc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254cd0a4-88fa-45e7-a930-7f3b8c15ba96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d435095d-ab74-41a3-9f13-c9e939fc3b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254cd0a4-88fa-45e7-a930-7f3b8c15ba96",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "eef46ec8-48c5-42d3-b6e6-f873011e8c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "e9726b38-7885-45ff-8fb6-47e274e878a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef46ec8-48c5-42d3-b6e6-f873011e8c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36d00b4e-902d-400a-9e8c-63f229011da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef46ec8-48c5-42d3-b6e6-f873011e8c97",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "bbe79b94-adc1-4990-8216-0a44c13002df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "0d0a19ea-b783-4df5-8c63-2c70f78875c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe79b94-adc1-4990-8216-0a44c13002df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cceb872-540d-4016-8b1a-17f74c685916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe79b94-adc1-4990-8216-0a44c13002df",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "dd8a769a-41a5-45ad-b033-f8b41aae8698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "6e24fd57-c6a3-4063-a224-7bfe38eaae6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8a769a-41a5-45ad-b033-f8b41aae8698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0466f6a5-ee3b-41bf-80f7-4e11e7b12881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8a769a-41a5-45ad-b033-f8b41aae8698",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "99b2a760-923c-4978-8e18-c31c9bc0ed3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "6a5e1a02-5224-4a64-abb4-07d461b4dc60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b2a760-923c-4978-8e18-c31c9bc0ed3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159ee484-1edb-4f76-a434-b463d9544742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b2a760-923c-4978-8e18-c31c9bc0ed3e",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7db9e622-a54d-4158-b9da-a4b21144d2af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "7252e2a0-3488-4c73-8ac4-3eaff690c362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db9e622-a54d-4158-b9da-a4b21144d2af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf53819-6c93-443d-ad83-dad2c42a2d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db9e622-a54d-4158-b9da-a4b21144d2af",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "83d29d90-f878-499d-be55-06f2bbd46dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "3290f534-c2e8-41ee-901a-4db0a81a9cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d29d90-f878-499d-be55-06f2bbd46dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c5414d-8d39-4981-bd47-5194eb675ac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d29d90-f878-499d-be55-06f2bbd46dcd",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "2b9e464b-f8e4-459e-9595-5f5fa29c700c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f9a200e5-8a8e-4ff9-8e02-a5ae53150b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9e464b-f8e4-459e-9595-5f5fa29c700c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb470cb-89ac-46c4-baa4-43d9ff75abe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9e464b-f8e4-459e-9595-5f5fa29c700c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "b952d930-0659-4907-9842-dc1b6ca174a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "3e5eed68-74df-4953-a316-dd95b615d459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b952d930-0659-4907-9842-dc1b6ca174a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e67a5c3-475b-4fa0-8653-dc07a341361e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b952d930-0659-4907-9842-dc1b6ca174a9",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "f5640543-653e-40ea-ae53-0a4292ff9c61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "caec40f9-c3ff-455f-88ca-aa44faea1352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5640543-653e-40ea-ae53-0a4292ff9c61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7eca3d7-3c1e-4e09-8342-729fcf753bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5640543-653e-40ea-ae53-0a4292ff9c61",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "60069572-a21d-4577-a11b-eab422b3eed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "4387d6e8-bee9-42be-bbe3-92cc67369c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60069572-a21d-4577-a11b-eab422b3eed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05a2ac1e-d70c-4315-94d8-f1fc60991b2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60069572-a21d-4577-a11b-eab422b3eed8",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "20d19a75-c268-4cf9-b09f-f7f2b09d25f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "ec3bcfab-2f8f-4c06-b6c7-918ae6c4b77d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d19a75-c268-4cf9-b09f-f7f2b09d25f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ee22a0-4084-41b6-80da-368485f6b23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d19a75-c268-4cf9-b09f-f7f2b09d25f6",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0c16e36b-d9e9-4ce2-95a2-6df6605dd7ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "88ae55a3-3588-4844-b630-551ec5f788cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c16e36b-d9e9-4ce2-95a2-6df6605dd7ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86a4352-20ca-452f-a812-97a5aafaed91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c16e36b-d9e9-4ce2-95a2-6df6605dd7ef",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "f7df996c-707c-444c-a835-6239e16513f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d238fad3-6526-4637-846d-e9c17009b390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7df996c-707c-444c-a835-6239e16513f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3986845-9adc-4aee-8085-f92441c138d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7df996c-707c-444c-a835-6239e16513f4",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0b623091-65e8-4504-ad59-541ff95d9b1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a66918d9-380b-499a-ae00-bfdec106661d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b623091-65e8-4504-ad59-541ff95d9b1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feae52f1-4056-48cc-bec1-0edc8c083b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b623091-65e8-4504-ad59-541ff95d9b1a",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "977f68e1-7a6e-4a94-9358-a797ab60fad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2390d52b-869c-4b69-bdae-9e4fd2a94266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "977f68e1-7a6e-4a94-9358-a797ab60fad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df57f6d6-3a56-4fce-b4e0-223ab0d7bd53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "977f68e1-7a6e-4a94-9358-a797ab60fad7",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "4b655037-6653-4858-9ed7-14cbaf29cd3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "40957da3-2157-4713-85ba-03b1ac170157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b655037-6653-4858-9ed7-14cbaf29cd3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d6e3ed-665a-402e-833a-8041b15a4189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b655037-6653-4858-9ed7-14cbaf29cd3f",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e75f8360-10c5-4f11-8bb3-1402f60e562d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2b145dc1-fb66-4f86-b9c2-25d187005ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75f8360-10c5-4f11-8bb3-1402f60e562d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7791e06-2446-41bd-b682-26b854a117b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75f8360-10c5-4f11-8bb3-1402f60e562d",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e6120f81-3fc1-41de-9133-13eb6f97ec2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f0d950b5-87cc-46dd-8ba7-6c65ab4693dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6120f81-3fc1-41de-9133-13eb6f97ec2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a2764bf-d93a-4f16-a9ed-4d550a8ffb95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6120f81-3fc1-41de-9133-13eb6f97ec2b",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ef44f05d-4cce-48ed-be46-e83b1aecd35f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "bab4c7c6-2ccd-4fec-abbc-d8c6ffbd36e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef44f05d-4cce-48ed-be46-e83b1aecd35f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d392885f-a75b-467e-9478-9630355fc6cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef44f05d-4cce-48ed-be46-e83b1aecd35f",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "65d7dac9-0f8c-4f09-8cf1-8a9d28209c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f805bd05-221c-487f-9e5b-034241e292a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d7dac9-0f8c-4f09-8cf1-8a9d28209c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62576aed-abdb-464b-b45f-d949ca1b76ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d7dac9-0f8c-4f09-8cf1-8a9d28209c48",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0989224f-416b-46b0-955b-9dab4d9d5a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "b2ba5e8f-b967-435a-bcad-4a4f73b67ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0989224f-416b-46b0-955b-9dab4d9d5a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea5d1845-1966-4508-a5db-3711d0ee738f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0989224f-416b-46b0-955b-9dab4d9d5a8b",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "be2d1496-f7ec-4bbc-9927-553a2053b5f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2a029db8-ac83-4be4-b42a-c060320974af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be2d1496-f7ec-4bbc-9927-553a2053b5f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6a1bd3-5460-4237-b085-604e6a80a8df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be2d1496-f7ec-4bbc-9927-553a2053b5f9",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e9c191fa-381a-4fbc-b0b1-40df30df6cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "bd4916a0-4955-4cc1-ab0f-89fbdc39d80a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c191fa-381a-4fbc-b0b1-40df30df6cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e215f569-8e0f-4ba1-94a1-b235a69f1c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c191fa-381a-4fbc-b0b1-40df30df6cd4",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "c48b41dc-704a-4b09-8102-a6ebdab1c94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "4c78943f-69d7-4c45-a1c2-bc86dece9c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48b41dc-704a-4b09-8102-a6ebdab1c94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21effdcd-5e0f-4e13-9ad6-aad4ed785523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48b41dc-704a-4b09-8102-a6ebdab1c94d",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "431dc02b-3b37-404a-be42-a32be957f5f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "e4b61821-8c29-4aa5-aca2-d36841886610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "431dc02b-3b37-404a-be42-a32be957f5f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d15c4f7-5a86-4ad6-bd03-f92227dfdaa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "431dc02b-3b37-404a-be42-a32be957f5f3",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "b96562e4-9776-45b2-8a79-cc33f8b11ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "b3f4f426-952a-4a36-99c3-68ff46e59c91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96562e4-9776-45b2-8a79-cc33f8b11ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d6402df-b4ae-4c29-8f82-511fb574b031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96562e4-9776-45b2-8a79-cc33f8b11ea2",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "43c064c0-7359-4082-9988-53ee956ec155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "292f41a4-49fe-45f5-b912-312615153eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c064c0-7359-4082-9988-53ee956ec155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81aad874-0fc7-43e8-bc91-39f2c0770529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c064c0-7359-4082-9988-53ee956ec155",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "676592a4-d131-48ca-a8ad-f3c466c841ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d23caf2a-fde7-4a81-9392-299ead6b5d72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676592a4-d131-48ca-a8ad-f3c466c841ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8333da80-6af6-4cbe-a7a8-825421d64b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676592a4-d131-48ca-a8ad-f3c466c841ee",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ad21bd7f-8204-481f-9fbc-abd1ed365f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "5882d398-1ab5-42a3-93a0-1e234a931cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad21bd7f-8204-481f-9fbc-abd1ed365f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7f9c3c-bf48-4218-bf73-380fc75fd2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad21bd7f-8204-481f-9fbc-abd1ed365f2c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "a5b430af-6c12-4e3d-ad8e-26c6fd227b55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a409cd93-058e-438e-9af8-d8fcf4eacd79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5b430af-6c12-4e3d-ad8e-26c6fd227b55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b383c43-dff5-4e11-9ed6-cc318d6b956b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5b430af-6c12-4e3d-ad8e-26c6fd227b55",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "835a8476-b376-44c9-8b0d-f88e25bcf525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "626dae48-d4b7-4a00-9cb1-d283272be9db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835a8476-b376-44c9-8b0d-f88e25bcf525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c410514-e80d-4ad8-9402-102d197a04f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835a8476-b376-44c9-8b0d-f88e25bcf525",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "763bc618-0b28-4b6b-8d50-3f2651056f98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "82c48182-0022-48c3-b384-b54021935f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "763bc618-0b28-4b6b-8d50-3f2651056f98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "147cf078-7ff2-4d6a-8c13-70a1c588e97f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "763bc618-0b28-4b6b-8d50-3f2651056f98",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "4f8f220c-4118-46e8-9c37-b9fe693aa96f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "c53a721a-f004-4299-807d-ef7d6d8dd639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8f220c-4118-46e8-9c37-b9fe693aa96f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ff3596-0846-4a58-bf5f-83d0b9773753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8f220c-4118-46e8-9c37-b9fe693aa96f",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "9e382bcf-351d-49e6-86c0-176514c0b2c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "9ab84ca7-d347-4c55-8449-dc845fa7e1d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e382bcf-351d-49e6-86c0-176514c0b2c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdc5d50c-96d0-4518-815c-f85c1349fe59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e382bcf-351d-49e6-86c0-176514c0b2c2",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "58e5c0c4-ffc2-47fe-bd70-954cff66a527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "db361481-a61a-46d0-b89e-fe94321a213b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e5c0c4-ffc2-47fe-bd70-954cff66a527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e3764f-c33c-4bee-b650-12e5b2412383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e5c0c4-ffc2-47fe-bd70-954cff66a527",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "71bed41a-d7c9-4cfb-bc27-b834bd1a54aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "08e32eb0-f6c5-433e-a8b4-9dd73a0a8470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71bed41a-d7c9-4cfb-bc27-b834bd1a54aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fce9b3-e794-44fc-ab31-7324057fc65a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71bed41a-d7c9-4cfb-bc27-b834bd1a54aa",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "195363b9-eb99-4fec-900d-3ebeaaef38ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a1186cd0-f73f-4f9b-9a5c-5aaf57c621f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195363b9-eb99-4fec-900d-3ebeaaef38ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89970ece-26e4-4d44-bd44-e5feec272d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195363b9-eb99-4fec-900d-3ebeaaef38ac",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "3b975082-f218-49b3-a2c2-d0b7c923f9ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "05ab7c9d-d079-4dbe-92ec-e4cd0c543fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b975082-f218-49b3-a2c2-d0b7c923f9ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13c051e0-c6f7-4f6e-a849-c7788d92c1a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b975082-f218-49b3-a2c2-d0b7c923f9ad",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "067fd579-f92f-4cba-804d-bc1ae3e33c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "c32aa319-8612-405d-874d-df6a29e69786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "067fd579-f92f-4cba-804d-bc1ae3e33c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2e6e6c-d1d2-4541-956e-24c6568cda6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "067fd579-f92f-4cba-804d-bc1ae3e33c38",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "f10a0da9-bc7c-4f31-8e8d-e24ee735e0a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "77325862-b0af-47c4-84c3-9c2856c9bf74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10a0da9-bc7c-4f31-8e8d-e24ee735e0a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db9dfd44-9da6-423f-b62c-cb8bb3e06951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10a0da9-bc7c-4f31-8e8d-e24ee735e0a0",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "fabe2096-d994-4c07-8335-942117fadda9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d4378335-bc48-42e5-bf56-25d0190b14a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fabe2096-d994-4c07-8335-942117fadda9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbdf9239-9d5c-49f1-aaf4-57d0cc06b1ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fabe2096-d994-4c07-8335-942117fadda9",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e1a12491-c7d6-491f-96c5-db66542f4354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "61a6abf3-a991-48d6-8c27-8eb46c7efb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1a12491-c7d6-491f-96c5-db66542f4354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da046a12-7a37-4b25-ab3b-897e88fc9e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1a12491-c7d6-491f-96c5-db66542f4354",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ca11fba6-8ad2-4468-9569-4cd7c400aea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "69ffcd6f-a676-4c47-9ff1-e326507cc994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca11fba6-8ad2-4468-9569-4cd7c400aea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8bc60f-80b1-4c0c-b9f6-64876aa86fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca11fba6-8ad2-4468-9569-4cd7c400aea6",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "1a8673cb-a71c-4249-a6bd-ca5a82767e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "888483ef-0656-463c-bb1b-ed6bd36508b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a8673cb-a71c-4249-a6bd-ca5a82767e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9b55f2-6391-41ef-96d2-01b670496ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a8673cb-a71c-4249-a6bd-ca5a82767e44",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "dcf4156b-7124-4531-9d2c-c0c9b187475c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f0b5e2a8-3b8f-4f81-834e-c0eb2339869f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf4156b-7124-4531-9d2c-c0c9b187475c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85ff7c21-39dd-45c9-8180-069939fed3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf4156b-7124-4531-9d2c-c0c9b187475c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "68cbb3ce-9792-4242-b573-019d57145f22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "af6d914f-cebe-4523-8087-04335436084d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68cbb3ce-9792-4242-b573-019d57145f22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4751310d-b28a-4450-a05d-8d2030c72aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68cbb3ce-9792-4242-b573-019d57145f22",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7d03be79-dd58-4e1d-981e-bcee35c4963c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "086ac1a6-32f1-482d-ab15-45991595a442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d03be79-dd58-4e1d-981e-bcee35c4963c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7efc726-8005-4cea-b148-c4a9c7eee255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d03be79-dd58-4e1d-981e-bcee35c4963c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "44a681b0-7fec-47aa-8972-838b5fbd398c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "96599cd8-2e2f-4cb8-8230-423de2cda886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a681b0-7fec-47aa-8972-838b5fbd398c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8239374f-3d36-4049-8d49-f0305eb0c7de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a681b0-7fec-47aa-8972-838b5fbd398c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "695e1146-ea27-4904-ba31-ee1b858933a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "b2cb6c39-121c-4bae-a737-73e51c551669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "695e1146-ea27-4904-ba31-ee1b858933a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44ab54e9-21c4-45f9-beba-89014e031baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "695e1146-ea27-4904-ba31-ee1b858933a8",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "51813ee3-1519-4f36-8403-eb23f2928cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "5700d991-1ee1-484f-a599-f28b75d068ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51813ee3-1519-4f36-8403-eb23f2928cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca07d71f-012a-4c40-bb81-343f03396159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51813ee3-1519-4f36-8403-eb23f2928cd1",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "170cd0ae-9318-4e71-88b1-f48e2919e042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "1018426a-0346-4f14-8d22-f87ae09b2489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "170cd0ae-9318-4e71-88b1-f48e2919e042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54c4b94-0bd4-4e8c-85ea-ae9b0279bbda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "170cd0ae-9318-4e71-88b1-f48e2919e042",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "a90f69e9-8572-4090-8329-63bf8586e0d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "037a34c2-13c1-43bf-96d5-02a17e97ecde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90f69e9-8572-4090-8329-63bf8586e0d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e50f79-dfd5-4cd7-bd3a-182c361c3597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90f69e9-8572-4090-8329-63bf8586e0d8",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "b72da3ee-5106-44f3-809a-7e4547862164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "3bb1e59b-fc5a-471e-b7a4-15a3f59258b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72da3ee-5106-44f3-809a-7e4547862164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79fb2c92-3f1c-43d5-aa82-c7a402204496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72da3ee-5106-44f3-809a-7e4547862164",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "3c008dab-1dbf-4998-ad29-a48addf1d851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d0ac2952-d84e-4dbb-9453-bbb69ea7fe65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c008dab-1dbf-4998-ad29-a48addf1d851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4469b39-6d03-4619-8307-763e7f807a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c008dab-1dbf-4998-ad29-a48addf1d851",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "1b861dea-e3f2-496a-9b2f-d829c23a5ebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "97c51e3b-e8ee-4310-b99c-202f4d32abf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b861dea-e3f2-496a-9b2f-d829c23a5ebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b954bb78-0f0f-4e3b-99e3-73d8d85fce86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b861dea-e3f2-496a-9b2f-d829c23a5ebe",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ea5cde08-73f7-4140-912f-154ce6057330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "788fed8d-089e-452c-b06e-b1cce93e610c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5cde08-73f7-4140-912f-154ce6057330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "213dae13-34f8-413d-87de-ea0f32910d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5cde08-73f7-4140-912f-154ce6057330",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "5723c38c-4ba1-4160-a9c8-7a4a3bb88059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a717cbdc-80a7-4fb5-9dab-d36d82ae77b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5723c38c-4ba1-4160-a9c8-7a4a3bb88059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c6b678-8b4c-4317-a0bf-9fd8f3ed36f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5723c38c-4ba1-4160-a9c8-7a4a3bb88059",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "c4d40377-b331-459e-a66f-5d3c2d9a17a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "12666229-d5c7-4f70-949e-ac709575205c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d40377-b331-459e-a66f-5d3c2d9a17a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91617d57-e10f-46c1-a0fc-81c59c15bfcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d40377-b331-459e-a66f-5d3c2d9a17a3",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "839c6b55-ba03-45e3-a498-f46fe23db01b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "e01e42a3-c456-48c9-b9f2-a35692d7a02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839c6b55-ba03-45e3-a498-f46fe23db01b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26402dc2-9258-4402-86a5-387de2b5f73f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839c6b55-ba03-45e3-a498-f46fe23db01b",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "467e0736-85ab-4980-bb9b-630b212094d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "8c65dbd8-4d91-4bdf-861b-a7801c229b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467e0736-85ab-4980-bb9b-630b212094d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07dd6b52-c4e4-4c30-aa09-933e58516e39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467e0736-85ab-4980-bb9b-630b212094d8",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e0fd2ed1-1fea-4c83-bdc5-e652b1a4c595",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "369ed9ec-e7db-4162-b8c9-0a48c0ef2a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fd2ed1-1fea-4c83-bdc5-e652b1a4c595",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cecec95-aebe-40e6-87dd-2746e9a6145a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fd2ed1-1fea-4c83-bdc5-e652b1a4c595",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0a4ca1eb-0bf7-441c-8c48-4947e3ef97de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "e0fd9401-1010-48dd-9d9f-e8c47ef50833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a4ca1eb-0bf7-441c-8c48-4947e3ef97de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5712bd-8bf4-4baf-bb0d-af09500ea414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a4ca1eb-0bf7-441c-8c48-4947e3ef97de",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "6dbcd086-a158-444e-8129-5b8b60a677b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "23992358-6866-4dcf-bd72-3d68be226a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dbcd086-a158-444e-8129-5b8b60a677b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc31fcf-b43a-4d40-a66f-a007ebe9107c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dbcd086-a158-444e-8129-5b8b60a677b5",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e453a5fd-9e8f-43b7-a8c9-27aae174a626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "bfed538a-f92a-440c-bc3f-fb003d40d5a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e453a5fd-9e8f-43b7-a8c9-27aae174a626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bdcaf9a-8b2c-4401-940b-ec7829cdc8e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e453a5fd-9e8f-43b7-a8c9-27aae174a626",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "918a07bd-58b0-4c87-86eb-62c1e703a129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "613542ef-0d49-48fa-ab3a-9869cd42ca68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918a07bd-58b0-4c87-86eb-62c1e703a129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b772ba6-a974-41e2-9328-de8771f223d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918a07bd-58b0-4c87-86eb-62c1e703a129",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "fcd90261-4924-43ac-ba3e-2f210d6e46ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "6a96e831-926b-4378-997d-c46d58668227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd90261-4924-43ac-ba3e-2f210d6e46ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ebe5b1-6df8-40d1-8070-6e21589431ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd90261-4924-43ac-ba3e-2f210d6e46ee",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "38458578-d899-4f97-8926-6ef9e8b41361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2676be69-4d65-4fad-94b7-4496b3514df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38458578-d899-4f97-8926-6ef9e8b41361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad328168-1297-44d6-8384-893c4c36c5c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38458578-d899-4f97-8926-6ef9e8b41361",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ae8444da-0107-4ef6-bef3-00a668d88ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a9b6f07f-b10f-4b7c-86aa-4f15695f5775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8444da-0107-4ef6-bef3-00a668d88ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db54a95e-a8f0-4aea-b642-9e849e83c576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8444da-0107-4ef6-bef3-00a668d88ed6",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "05bd2d88-22f4-4b86-8f3b-aa86125532aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "af27cbac-f23a-4637-85dd-3022163e0594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05bd2d88-22f4-4b86-8f3b-aa86125532aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef82b116-ffd9-418b-94f9-a31d3a015ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05bd2d88-22f4-4b86-8f3b-aa86125532aa",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "f0383cfd-3951-42e3-a9c7-e58bcf494c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "9d55f1b3-16ff-4edf-8197-3962b72590e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0383cfd-3951-42e3-a9c7-e58bcf494c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aef7796-7ab9-4394-9836-3510f66cf1df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0383cfd-3951-42e3-a9c7-e58bcf494c65",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "8cb12789-4297-49da-b5cd-297daff2b9b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "8e262a88-33a4-4303-974c-482cbabe3121",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb12789-4297-49da-b5cd-297daff2b9b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88762b06-3ccf-44d4-9015-2cccacadb566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb12789-4297-49da-b5cd-297daff2b9b4",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "62f3a8c6-bed9-47b9-935c-eb242e12d744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "152bda1e-4837-4c1e-896a-8e16e867fdaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f3a8c6-bed9-47b9-935c-eb242e12d744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a722e17-a215-4f52-8f1c-a4fbf5a8e4e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f3a8c6-bed9-47b9-935c-eb242e12d744",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "76bb2b2b-4c80-4404-840c-1181c1096e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "9d64ff08-33ae-4cd6-a14e-64e40194189c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76bb2b2b-4c80-4404-840c-1181c1096e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a657bcd6-c17e-48d2-a589-e1e428170c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76bb2b2b-4c80-4404-840c-1181c1096e32",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "65c5313b-b2e5-4ffc-882c-ecbee6a5ff4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "e888dae6-577f-4be7-aba0-31b220287f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65c5313b-b2e5-4ffc-882c-ecbee6a5ff4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b652a644-08a7-4e0b-a238-81868f0a4b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c5313b-b2e5-4ffc-882c-ecbee6a5ff4d",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "d5d1b4cb-66a5-4f66-924e-7232cd311bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "0762b8ae-db22-4285-98d0-93e92b45822e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d1b4cb-66a5-4f66-924e-7232cd311bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64aa5ce-1baf-4e1c-a0a2-6a63d2ae8ed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d1b4cb-66a5-4f66-924e-7232cd311bf2",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "9b995b23-8cc6-4950-844b-f8555a7932c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "74e4bd45-4f10-48c0-8b57-e77fc5647c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b995b23-8cc6-4950-844b-f8555a7932c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d4859bf-977e-4d10-b93c-659f10495e31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b995b23-8cc6-4950-844b-f8555a7932c2",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "89f28857-7993-4f64-a641-504a9f95c336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d1975d9b-62b1-4424-8fec-f638dbf0e6e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f28857-7993-4f64-a641-504a9f95c336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe68f04d-dd74-4810-a55f-72f005c8cfc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f28857-7993-4f64-a641-504a9f95c336",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "3b1c9823-0d29-405b-8b52-17d0dce3ac60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "7dbc5de0-046f-456d-9dc6-c6ee1f5ac2ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b1c9823-0d29-405b-8b52-17d0dce3ac60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8681d3bf-21b0-44d8-9098-6c1d09ebaf57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b1c9823-0d29-405b-8b52-17d0dce3ac60",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "1fa84c22-9ac1-4e38-a7d0-b8112c78c279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "fc89a384-ea02-4585-8dc9-d4104770a4dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fa84c22-9ac1-4e38-a7d0-b8112c78c279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de738eb-09ad-4ab0-a758-4c2c060492d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fa84c22-9ac1-4e38-a7d0-b8112c78c279",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7dc150df-a8a3-4f06-9e26-3341942baa98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "ed480b4c-42da-4b0a-a2aa-53673c7d5b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc150df-a8a3-4f06-9e26-3341942baa98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd54950d-1294-4c83-b3da-401bf7db94b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc150df-a8a3-4f06-9e26-3341942baa98",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "acf733f1-3735-4c77-9b60-9a11ab6efdb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "333d94fa-c1d2-46c0-a228-13d25441238c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf733f1-3735-4c77-9b60-9a11ab6efdb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc3def6b-ccd9-4129-b391-f8298ae305fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf733f1-3735-4c77-9b60-9a11ab6efdb1",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "1e8c8d43-86d7-4e27-8c56-eae9b5afbf9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "15f9c07d-1014-46b1-ba4d-ac18ae8ed3ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8c8d43-86d7-4e27-8c56-eae9b5afbf9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15679f7-cc35-4f21-8558-c8b92e6435cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8c8d43-86d7-4e27-8c56-eae9b5afbf9c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7e879596-cff5-41b2-a0b3-2e7beafc7ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "4fb0c3dc-6069-498f-9066-dc57523274f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e879596-cff5-41b2-a0b3-2e7beafc7ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73271b04-6130-42fb-9183-bb54e63c8c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e879596-cff5-41b2-a0b3-2e7beafc7ce9",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "86fb7df4-ec17-4f5c-9db6-6a19b5200a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "cbb97991-a5e2-4901-865f-083f53320ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86fb7df4-ec17-4f5c-9db6-6a19b5200a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e296d9-9fc6-4857-9b66-55a86ae93da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86fb7df4-ec17-4f5c-9db6-6a19b5200a74",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "5aac8119-598d-4371-902a-d4d77c9b912c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "80cda5f5-9000-476b-acc2-396c2c730615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aac8119-598d-4371-902a-d4d77c9b912c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806695ef-337f-4a99-beea-6a7ff94c5dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aac8119-598d-4371-902a-d4d77c9b912c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0c348db0-73ce-4119-8dd5-6add2bde0b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "b94e36ef-9c10-41d6-903c-58e0544dd5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c348db0-73ce-4119-8dd5-6add2bde0b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c744b697-f4eb-4144-ba3a-5ce8d4b3c534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c348db0-73ce-4119-8dd5-6add2bde0b19",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "11ebbf3f-3026-4b5b-b47a-24135a67c640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "3fd7eaed-ae78-48b8-bc72-01cfe8ac49c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11ebbf3f-3026-4b5b-b47a-24135a67c640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99f7c98-2e1f-4dc0-ab35-4438f7e041d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11ebbf3f-3026-4b5b-b47a-24135a67c640",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "d454d99c-6ca8-40cb-be79-d3b45c3d9869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "7dc5aeb7-cad3-4ce2-a920-508425b86737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d454d99c-6ca8-40cb-be79-d3b45c3d9869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa362c1-b845-4469-999b-d54c3ef1ca39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d454d99c-6ca8-40cb-be79-d3b45c3d9869",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "8f84afd8-b182-4930-bc24-48ce5d717bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "3bbcfb22-a61b-4b8a-8eb5-ce5fa653ad09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f84afd8-b182-4930-bc24-48ce5d717bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f07c0a-bbb6-4305-88c0-08a8c406b600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f84afd8-b182-4930-bc24-48ce5d717bf5",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ad13ebaf-0c05-4077-bdb0-c624c7007d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "56e424a0-2980-4435-958f-957fe034e880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad13ebaf-0c05-4077-bdb0-c624c7007d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd048759-8302-4bf9-8fa0-a8be2f79699d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad13ebaf-0c05-4077-bdb0-c624c7007d58",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "b666d584-9a94-4f54-bd5f-586524e77bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f5665304-9f4d-4e46-a088-c42dd20f6bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b666d584-9a94-4f54-bd5f-586524e77bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da26c7e-fe4d-4fde-9d98-a59f28397880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b666d584-9a94-4f54-bd5f-586524e77bd5",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "252cef0a-97f6-4177-902e-2d6f573951df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "35325d43-909c-4556-97d6-1ad88489eda4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252cef0a-97f6-4177-902e-2d6f573951df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "418d9095-e2d5-41ed-82ca-99ad4c33044b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252cef0a-97f6-4177-902e-2d6f573951df",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "b70e7d83-717d-4d5b-8641-687e510a9dc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "395f50c0-e385-4627-817d-77a49fc4fb3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70e7d83-717d-4d5b-8641-687e510a9dc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3e2cd2-466c-4475-9925-01153db91f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70e7d83-717d-4d5b-8641-687e510a9dc6",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "bb73cf02-8f1b-4f38-b1be-abd755656a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "ff5f3f22-e96f-48eb-a55a-42fc2bda4e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb73cf02-8f1b-4f38-b1be-abd755656a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1211969e-f407-4919-8e68-03b60face140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb73cf02-8f1b-4f38-b1be-abd755656a1f",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "622e4ec6-179e-4f95-93c6-0b8b82f4b685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "ef3559d4-0e80-49b3-b73d-0ed7d630f69e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622e4ec6-179e-4f95-93c6-0b8b82f4b685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a86e1e4d-e4d3-4492-a905-1934781d5ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622e4ec6-179e-4f95-93c6-0b8b82f4b685",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e9467019-baea-40f4-9cb8-e99c40cb52c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "456aff38-e67f-4177-bf3d-9aa6c9d0ebb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9467019-baea-40f4-9cb8-e99c40cb52c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6bddae-81ca-4f37-9a4b-3a46ac270438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9467019-baea-40f4-9cb8-e99c40cb52c0",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "a76f5237-f00a-4227-be48-80178e22bd3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "136fc8bc-a14c-4b5b-94b7-e1bd66557a7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a76f5237-f00a-4227-be48-80178e22bd3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c184458-a123-4ffc-b6fa-0be83bc4f220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a76f5237-f00a-4227-be48-80178e22bd3c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "886194f9-4312-46a4-b8b8-17075bb36b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "9dfe90be-4de1-43e0-a63d-bd15634064c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886194f9-4312-46a4-b8b8-17075bb36b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e7e5e2f-2dab-452d-bc37-6c409d1959c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886194f9-4312-46a4-b8b8-17075bb36b30",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "03ae0de6-6d77-49d2-8d88-39bb832fd9fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "644100a2-59be-4ab7-b4db-4a80b3c8d13d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ae0de6-6d77-49d2-8d88-39bb832fd9fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2a8385-11bc-490e-9548-39771f09aeb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ae0de6-6d77-49d2-8d88-39bb832fd9fc",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "39de2202-6b91-4ad5-87b1-06ba1b12de63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d1787422-681b-4ff1-aa0d-f8a05df2e60b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39de2202-6b91-4ad5-87b1-06ba1b12de63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8227913-70e4-4aad-9ef1-139696d24743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39de2202-6b91-4ad5-87b1-06ba1b12de63",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "c45f1499-598e-478d-81e0-603a7c4b4901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "0c2c8b36-ccc1-45ec-a220-afa6a1702072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45f1499-598e-478d-81e0-603a7c4b4901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "978a6b6a-5aa0-4ddc-8ad6-e53d4227d171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45f1499-598e-478d-81e0-603a7c4b4901",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "54ade4b6-04c3-4d1c-8dd2-ede476199dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "814e9fc8-2c9f-42d6-8b76-834447b6568c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ade4b6-04c3-4d1c-8dd2-ede476199dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e2f806c-4090-4cfc-9d56-04ff7e12a829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ade4b6-04c3-4d1c-8dd2-ede476199dfe",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "8e898881-b9bb-4f61-9bdf-25a73172d3ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "74fce650-6807-46d2-86ee-92d1f41cac15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e898881-b9bb-4f61-9bdf-25a73172d3ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1410cff8-4d83-4654-b62d-ea0259d69d73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e898881-b9bb-4f61-9bdf-25a73172d3ba",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ed89fb5b-4ab0-4004-a249-c0231d086077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "c459c39c-d14f-4864-a73b-0ada54a7f4f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed89fb5b-4ab0-4004-a249-c0231d086077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6712bbd8-b80e-4fa2-a1e3-8a32449e4b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed89fb5b-4ab0-4004-a249-c0231d086077",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "c8903793-8fba-4fb2-a74d-b7cb8eb684b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "919228cf-c527-40d7-aadf-7f0873901e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8903793-8fba-4fb2-a74d-b7cb8eb684b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ec8b64-8b45-45e1-bab5-c903a0593efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8903793-8fba-4fb2-a74d-b7cb8eb684b3",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7abd11fb-4dc0-4897-9167-7a49e6b8a764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "81d53a49-9fe7-4ea3-8956-0dab4eac2904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7abd11fb-4dc0-4897-9167-7a49e6b8a764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6cd798e-95d0-4fd7-9f42-818714e95dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7abd11fb-4dc0-4897-9167-7a49e6b8a764",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7c87898e-8094-4c22-94e2-54b7189b542d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a77e2197-85b1-41ea-8bea-c165cfdbe597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c87898e-8094-4c22-94e2-54b7189b542d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e804d4-3ab7-43a7-98fd-ae06178cf6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c87898e-8094-4c22-94e2-54b7189b542d",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e5456085-1df6-4789-93a1-05bdf5b7137d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "cce2534f-07d8-4fe7-b486-86efe0576536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5456085-1df6-4789-93a1-05bdf5b7137d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05af3b9-a526-4b31-ad6f-ee992a885242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5456085-1df6-4789-93a1-05bdf5b7137d",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "12d8a8f9-6d81-4a58-9552-2a84f7c45ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "073f63a3-205d-43cd-a5b2-127f9327dee6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d8a8f9-6d81-4a58-9552-2a84f7c45ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4f1c81-a4d1-48f3-8c3e-069aadae5634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d8a8f9-6d81-4a58-9552-2a84f7c45ea3",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "c2d0ed05-89cf-49fc-ad89-9bb8310f7feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "ded9ce89-5922-4be1-a972-314bf8241336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d0ed05-89cf-49fc-ad89-9bb8310f7feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e38e34f-4f0b-4bf4-b880-7cae892bc549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d0ed05-89cf-49fc-ad89-9bb8310f7feb",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "4086e015-f71b-475c-9464-3abc632c76ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "53e66cea-0aec-42f0-b7c3-611957839e3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4086e015-f71b-475c-9464-3abc632c76ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aeeac95-eee7-49f8-882f-9e931cbc5349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4086e015-f71b-475c-9464-3abc632c76ba",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "391eb439-0453-4499-8d8f-ea574ab2e7d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2a3d340b-86ce-4318-a2fc-2fff9203827b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "391eb439-0453-4499-8d8f-ea574ab2e7d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "283b8903-fbc4-4aef-9923-fb66cc09cbf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "391eb439-0453-4499-8d8f-ea574ab2e7d4",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "f2b261ca-35db-40ae-9d5c-51587a23aa53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2d6bcde6-7292-4ea3-a289-e73e8557baa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2b261ca-35db-40ae-9d5c-51587a23aa53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00656e9d-2625-497a-b100-4b56b70f964d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2b261ca-35db-40ae-9d5c-51587a23aa53",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "32dda91f-07b3-425b-92d1-e6976e83214f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "6cfeea28-d3e3-4eb8-980e-5c052ca92488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32dda91f-07b3-425b-92d1-e6976e83214f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5a2a87-4752-4773-aeed-76073fdf9f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32dda91f-07b3-425b-92d1-e6976e83214f",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "d63e2bdd-3403-4166-a7e6-9e36ca4084a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "80066548-b90c-40f4-9727-b9e649328c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63e2bdd-3403-4166-a7e6-9e36ca4084a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96d689e0-e3ca-4f06-a8b4-b40800bfdc18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63e2bdd-3403-4166-a7e6-9e36ca4084a7",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "3dcaedab-2efc-43df-9391-6654c6651db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "efbb3110-5293-42fd-98b4-6ed72acf87b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dcaedab-2efc-43df-9391-6654c6651db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd52e3eb-b8cb-4144-8ce3-c3fcfa9397da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dcaedab-2efc-43df-9391-6654c6651db1",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e840eaaa-0391-4939-ad33-7a745db95fa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f4c44be2-cd52-46c0-8570-bd41131622d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e840eaaa-0391-4939-ad33-7a745db95fa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d4457df-fe84-4752-a30b-fd51c552b75d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e840eaaa-0391-4939-ad33-7a745db95fa4",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "c774f0af-efaa-4658-80e2-0bbab02c912a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "4df3d2e7-d8d5-49fc-9b2a-0428adebe4fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c774f0af-efaa-4658-80e2-0bbab02c912a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed5960d-53bb-4ed9-83d3-529eb4e0b70d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c774f0af-efaa-4658-80e2-0bbab02c912a",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "eeb1edf5-a5fa-4607-a6b1-5017fd5fd139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "2a619226-a7b9-43ea-9150-f25f1669a3fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb1edf5-a5fa-4607-a6b1-5017fd5fd139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a75836a8-1249-4ce0-b363-f91af68d04b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb1edf5-a5fa-4607-a6b1-5017fd5fd139",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0b78002e-d1fc-4bd5-91b2-3f754647bb0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "64423dc1-dbea-4234-84ba-d7b3803e5b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b78002e-d1fc-4bd5-91b2-3f754647bb0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d028f12-f246-479a-a611-3ca78c196866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b78002e-d1fc-4bd5-91b2-3f754647bb0d",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "3b7a62a8-f60a-4786-8d08-cb34e5e46e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "0518b2bf-9910-4cb2-a791-53fc5608a155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7a62a8-f60a-4786-8d08-cb34e5e46e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "233fc96a-d8ac-4adf-95a6-6a22e68c2ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7a62a8-f60a-4786-8d08-cb34e5e46e3f",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "a6ad2f23-677a-4731-9a6d-b9be0eeba906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "69b14f20-d8d9-49ff-a8c3-02bfb8715d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ad2f23-677a-4731-9a6d-b9be0eeba906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d390110a-3455-43b2-85d7-0e13c3f91b28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ad2f23-677a-4731-9a6d-b9be0eeba906",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "1f20bc51-9aaa-4769-9b67-509c9af0cdc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "52479b35-5370-4591-b5b3-19f206f20464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f20bc51-9aaa-4769-9b67-509c9af0cdc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d99b4e-710c-435c-8728-5df046f7ad22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f20bc51-9aaa-4769-9b67-509c9af0cdc6",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "805ae163-482c-4f63-b51f-9f07516e6879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "aee1bbaf-9997-41d4-95ce-77c51a20920d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "805ae163-482c-4f63-b51f-9f07516e6879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44b72a4-2974-49fb-b231-6afc99256f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "805ae163-482c-4f63-b51f-9f07516e6879",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "23210b87-45c5-4770-846c-076833481b18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "c9bbf9da-c13c-4de9-bb05-80951e9cd3a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23210b87-45c5-4770-846c-076833481b18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e138076d-8d50-4c85-aacd-f63fdd917487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23210b87-45c5-4770-846c-076833481b18",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "2444ba14-2be9-4a72-967f-fe5459e50e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "a38b8439-b936-4848-8f67-f003a28b55db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2444ba14-2be9-4a72-967f-fe5459e50e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0011ba4c-9288-4e0a-ac7b-219525f2e2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2444ba14-2be9-4a72-967f-fe5459e50e70",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "6ed6d0a7-f220-4262-8222-b523f098dc7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "90ab544a-f973-4b6f-851c-9e1acab2f7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed6d0a7-f220-4262-8222-b523f098dc7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3934aa-3c88-4fcb-a0d3-977bb8ef5e7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed6d0a7-f220-4262-8222-b523f098dc7c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "9201e3c8-2b00-41b2-9c74-5dbe8c50807c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "1e0c6e7d-608b-490b-b4be-604cdf8f6b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9201e3c8-2b00-41b2-9c74-5dbe8c50807c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d694c3f-05e6-4687-8374-46f3a18232ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9201e3c8-2b00-41b2-9c74-5dbe8c50807c",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "7e6fc6c9-57fe-4e99-be93-e337a86901ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "b97d31a7-5a34-42a0-bda7-37a8d0adce0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6fc6c9-57fe-4e99-be93-e337a86901ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96af445-16a8-427d-b485-d87ff84e379d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6fc6c9-57fe-4e99-be93-e337a86901ea",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "0f562a0a-2064-47b1-b426-ec1cd4df1a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "20ae834f-1dd1-46fc-a82e-3b97f8f1d984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f562a0a-2064-47b1-b426-ec1cd4df1a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d22c1b1-eeb1-4cfc-9542-5eed4d0dc5f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f562a0a-2064-47b1-b426-ec1cd4df1a34",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "2614f8db-0309-4497-9245-99b0453bc103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "450174df-3b13-4621-864e-48b519b18c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2614f8db-0309-4497-9245-99b0453bc103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "013607e6-4323-4f4b-a32f-a01cc01d5ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2614f8db-0309-4497-9245-99b0453bc103",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "063fd49b-6bf9-401f-be65-21da2737d8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "febff9e4-a0b6-45b9-9763-b6243dad4747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063fd49b-6bf9-401f-be65-21da2737d8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05922a8c-c962-4e0f-9a6c-1eeb8c7a8d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063fd49b-6bf9-401f-be65-21da2737d8ad",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "88eef506-ff4e-49aa-9627-00f76bda2ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "fcdca88f-697a-4d21-95c5-53961962233e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88eef506-ff4e-49aa-9627-00f76bda2ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26db89f2-3328-4c4e-be30-c17cc88cbb86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88eef506-ff4e-49aa-9627-00f76bda2ab7",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "53490f15-ac65-4263-bcf8-01930543a7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "7276b9d7-6dba-4905-a198-a3d1cf77ac5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53490f15-ac65-4263-bcf8-01930543a7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd34da02-5d89-48e2-889d-649ebb7c8305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53490f15-ac65-4263-bcf8-01930543a7d3",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "ea07ea8e-8668-44e9-87e5-f513d7f79c78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "b5530654-3772-4f56-91db-060ea4c8a77e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea07ea8e-8668-44e9-87e5-f513d7f79c78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "502baa52-1212-4f5d-beed-6d29c24b6b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea07ea8e-8668-44e9-87e5-f513d7f79c78",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "86670cb8-2c01-4710-bfc0-5ada9786b7a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "d4f64692-62c4-43d7-9dd9-b6a7d6b4fcee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86670cb8-2c01-4710-bfc0-5ada9786b7a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ade0c7-d92a-413f-ab54-1bcb462424da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86670cb8-2c01-4710-bfc0-5ada9786b7a8",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "112297f0-c4dc-46e7-b387-83f51d4b125a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "f6970566-3600-44e0-864b-a25bd731e2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112297f0-c4dc-46e7-b387-83f51d4b125a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac52f3cc-159f-4a49-a771-24dcdb6c1801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112297f0-c4dc-46e7-b387-83f51d4b125a",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "e97fefd7-1894-4373-ac78-df9ad7359466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "6ed7a9da-a236-4454-998c-01dead1350af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97fefd7-1894-4373-ac78-df9ad7359466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3ff9cc9-6c25-489c-a64d-46a38d5a3566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97fefd7-1894-4373-ac78-df9ad7359466",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        },
        {
            "id": "47278780-16fe-4dc4-afd8-4524c9cafc9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "compositeImage": {
                "id": "0d7ca767-8906-41a1-9495-9eb03095b1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47278780-16fe-4dc4-afd8-4524c9cafc9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aab8f56-1b95-403a-9c30-0f0cc085e98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47278780-16fe-4dc4-afd8-4524c9cafc9a",
                    "LayerId": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "ebd69706-d104-4860-8c0d-55e2c9e5e1c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28688a53-f43a-419e-b23c-2f905f59b97c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}