{
    "id": "019e5953-9ff9-4fc3-9654-e09b4f5b967f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_stealth_wis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ded49e7-23e2-4f34-afe1-3e488bc6dd00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019e5953-9ff9-4fc3-9654-e09b4f5b967f",
            "compositeImage": {
                "id": "83677361-f601-45de-94e1-cea6e51398af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ded49e7-23e2-4f34-afe1-3e488bc6dd00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12113593-c5c7-43c5-82e4-b7f3212ba646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ded49e7-23e2-4f34-afe1-3e488bc6dd00",
                    "LayerId": "98816082-cd97-4b02-8f38-dbeba313349d"
                },
                {
                    "id": "ab1cec6f-8077-41bb-a0b1-a52dac55dc55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ded49e7-23e2-4f34-afe1-3e488bc6dd00",
                    "LayerId": "2e5744f9-e93f-43d7-8c69-76aebab19966"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "2e5744f9-e93f-43d7-8c69-76aebab19966",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "019e5953-9ff9-4fc3-9654-e09b4f5b967f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 6",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "98816082-cd97-4b02-8f38-dbeba313349d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "019e5953-9ff9-4fc3-9654-e09b4f5b967f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}