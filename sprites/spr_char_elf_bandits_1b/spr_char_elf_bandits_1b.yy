{
    "id": "5af1dcee-9ca8-490f-9a86-489bd5fa2fbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_1b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1958,
    "bbox_left": 50,
    "bbox_right": 1168,
    "bbox_top": 108,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c5efbad-70ca-411e-9726-d98c26db9365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af1dcee-9ca8-490f-9a86-489bd5fa2fbd",
            "compositeImage": {
                "id": "e35047f7-cddd-4190-9a45-ef8f352b33c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5efbad-70ca-411e-9726-d98c26db9365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c4395c-fdd7-4fdd-b96b-4dfa69349e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5efbad-70ca-411e-9726-d98c26db9365",
                    "LayerId": "618b9791-1078-47f9-8ee0-e2510b9cdbab"
                }
            ]
        },
        {
            "id": "5eb5c2e9-fc11-4966-98cf-7f0b7b5c29e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af1dcee-9ca8-490f-9a86-489bd5fa2fbd",
            "compositeImage": {
                "id": "e5c192ae-e19e-4485-bf3e-98411722fa5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb5c2e9-fc11-4966-98cf-7f0b7b5c29e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58889537-bd14-44ce-a91e-66727741f011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb5c2e9-fc11-4966-98cf-7f0b7b5c29e2",
                    "LayerId": "618b9791-1078-47f9-8ee0-e2510b9cdbab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1971,
    "layers": [
        {
            "id": "618b9791-1078-47f9-8ee0-e2510b9cdbab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5af1dcee-9ca8-490f-9a86-489bd5fa2fbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1169,
    "xorig": 0,
    "yorig": 0
}