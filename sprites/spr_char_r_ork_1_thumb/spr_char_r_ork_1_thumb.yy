{
    "id": "cce060bf-301d-4969-a8a6-ee3d812a1c2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_1_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fe0a532-14f4-4113-945c-66d52017631e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce060bf-301d-4969-a8a6-ee3d812a1c2b",
            "compositeImage": {
                "id": "f5d75a8e-679f-42c5-a1ec-cb5d438e3a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe0a532-14f4-4113-945c-66d52017631e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "955602a8-1686-47fc-a916-fa2a52244ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe0a532-14f4-4113-945c-66d52017631e",
                    "LayerId": "ee0555c7-3a9e-4b6e-8ee1-99cecdb60b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "ee0555c7-3a9e-4b6e-8ee1-99cecdb60b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cce060bf-301d-4969-a8a6-ee3d812a1c2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}