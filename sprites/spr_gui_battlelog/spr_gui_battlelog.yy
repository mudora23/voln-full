{
    "id": "4c74733e-8571-4683-8148-4f4677b60af5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_battlelog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 439,
    "bbox_left": 0,
    "bbox_right": 359,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56b5d32d-040b-4fa7-b639-4893957408a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c74733e-8571-4683-8148-4f4677b60af5",
            "compositeImage": {
                "id": "d7d7498b-7519-4487-947a-67ce0da8e34a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56b5d32d-040b-4fa7-b639-4893957408a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baa0c5f0-b96c-4450-b223-73695b094693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56b5d32d-040b-4fa7-b639-4893957408a1",
                    "LayerId": "439432b4-92c1-4858-b013-05882df7bd46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 440,
    "layers": [
        {
            "id": "439432b4-92c1-4858-b013-05882df7bd46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c74733e-8571-4683-8148-4f4677b60af5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 360,
    "xorig": 0,
    "yorig": 0
}