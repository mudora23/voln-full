{
    "id": "11716d9c-cdbb-4c10-8512-1ba9e68d6a80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d4f665d-d154-48f5-a3f2-8cb2149e6495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11716d9c-cdbb-4c10-8512-1ba9e68d6a80",
            "compositeImage": {
                "id": "7cc12006-86a0-4fbf-aedf-ceee2c6e83b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d4f665d-d154-48f5-a3f2-8cb2149e6495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc47b79b-31a0-4fae-908b-467c263a898f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d4f665d-d154-48f5-a3f2-8cb2149e6495",
                    "LayerId": "944e294a-4c1d-4257-adb2-fb15e7df2440"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "944e294a-4c1d-4257-adb2-fb15e7df2440",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11716d9c-cdbb-4c10-8512-1ba9e68d6a80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}