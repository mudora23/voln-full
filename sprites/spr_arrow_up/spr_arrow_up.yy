{
    "id": "4429fc86-4633-4d94-aee7-7376b8882652",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3e03e4f-234e-4352-9d7d-86b269a194ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4429fc86-4633-4d94-aee7-7376b8882652",
            "compositeImage": {
                "id": "129cf100-f31b-4193-9f5a-78638652e67a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e03e4f-234e-4352-9d7d-86b269a194ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41617d40-3b05-4b56-89ae-7a47c43ffa35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e03e4f-234e-4352-9d7d-86b269a194ca",
                    "LayerId": "5d4c6093-ead9-4eb9-bcba-3cdee03e2ebb"
                }
            ]
        },
        {
            "id": "de3f0e42-749a-413a-81b1-d46328422562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4429fc86-4633-4d94-aee7-7376b8882652",
            "compositeImage": {
                "id": "80d3a5f1-5efd-4ef4-9e06-9919da6ff1b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de3f0e42-749a-413a-81b1-d46328422562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ac9f3b-ecd1-49b2-a731-c33db1a0f10a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de3f0e42-749a-413a-81b1-d46328422562",
                    "LayerId": "5d4c6093-ead9-4eb9-bcba-3cdee03e2ebb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "5d4c6093-ead9-4eb9-bcba-3cdee03e2ebb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4429fc86-4633-4d94-aee7-7376b8882652",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}