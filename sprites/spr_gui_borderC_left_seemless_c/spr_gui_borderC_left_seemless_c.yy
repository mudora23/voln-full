{
    "id": "08ea8e08-8122-43e0-a1d2-72a4287b732b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_left_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46c0bf3c-d604-4055-b63b-34185f32c759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08ea8e08-8122-43e0-a1d2-72a4287b732b",
            "compositeImage": {
                "id": "fc2a63c2-f425-4034-8ee0-014838a143cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c0bf3c-d604-4055-b63b-34185f32c759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa78d601-5629-4577-a7ca-1d39f80feeb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c0bf3c-d604-4055-b63b-34185f32c759",
                    "LayerId": "44fdaf1b-89d0-4ba0-bbeb-400df2c59c56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "44fdaf1b-89d0-4ba0-bbeb-400df2c59c56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08ea8e08-8122-43e0-a1d2-72a4287b732b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 13,
    "yorig": 19
}