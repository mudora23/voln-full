{
    "id": "07b1ee26-bf53-4a7d-be39-261c89c44fcd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_blizzard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 187,
    "bbox_left": 10,
    "bbox_right": 187,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "469ef259-4967-4ea6-a289-6e64fba46c7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b1ee26-bf53-4a7d-be39-261c89c44fcd",
            "compositeImage": {
                "id": "4ede3af2-41cd-4499-940a-8475d918c63d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "469ef259-4967-4ea6-a289-6e64fba46c7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "035286f0-7710-4846-950e-19970c9cffbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "469ef259-4967-4ea6-a289-6e64fba46c7c",
                    "LayerId": "442fb408-a370-41d0-9d65-356cbe42b645"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "442fb408-a370-41d0-9d65-356cbe42b645",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07b1ee26-bf53-4a7d-be39-261c89c44fcd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}