{
    "id": "83caeb71-4570-4c60-9c99-31e475480b27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "427a1f7f-48ed-44bb-914d-8594370b9f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83caeb71-4570-4c60-9c99-31e475480b27",
            "compositeImage": {
                "id": "82141674-563f-4d3f-b2d7-6f3a57c26e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427a1f7f-48ed-44bb-914d-8594370b9f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2470e511-1cec-4624-bfa6-562ce3c586a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427a1f7f-48ed-44bb-914d-8594370b9f8e",
                    "LayerId": "bcc74bf5-4d3a-4759-bc7a-5db2ab2347e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 145,
    "layers": [
        {
            "id": "bcc74bf5-4d3a-4759-bc7a-5db2ab2347e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83caeb71-4570-4c60-9c99-31e475480b27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 119,
    "yorig": 119
}