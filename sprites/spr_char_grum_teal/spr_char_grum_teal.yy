{
    "id": "bdf483c4-daa7-4b6b-883a-4f8e05bdb38f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_teal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20d44898-c01c-46be-b003-f9bc808ed9d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdf483c4-daa7-4b6b-883a-4f8e05bdb38f",
            "compositeImage": {
                "id": "d66ed458-dfbd-4d4d-9afa-1027215c56be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d44898-c01c-46be-b003-f9bc808ed9d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54db670b-f28b-45c6-be57-efc2a7b26efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d44898-c01c-46be-b003-f9bc808ed9d9",
                    "LayerId": "d4d47401-6a6d-4c77-956a-21128349d787"
                }
            ]
        },
        {
            "id": "5a17ab8e-369c-43f9-8af5-72f83cbf12cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdf483c4-daa7-4b6b-883a-4f8e05bdb38f",
            "compositeImage": {
                "id": "0eaf3815-7e45-43cd-b7dc-33d3790daa55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a17ab8e-369c-43f9-8af5-72f83cbf12cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a94c4432-a55b-40cc-9a79-8ca276b0262a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a17ab8e-369c-43f9-8af5-72f83cbf12cc",
                    "LayerId": "d4d47401-6a6d-4c77-956a-21128349d787"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "d4d47401-6a6d-4c77-956a-21128349d787",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdf483c4-daa7-4b6b-883a-4f8e05bdb38f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}