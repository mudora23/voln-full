{
    "id": "343309c9-8010-48fd-a28b-9cc76a649cf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_2_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 3,
    "bbox_right": 276,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e150e113-aa94-4ade-aa40-2f813e86766f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "343309c9-8010-48fd-a28b-9cc76a649cf2",
            "compositeImage": {
                "id": "a0258ca6-33ff-4412-bf5a-471d7f5bb70a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e150e113-aa94-4ade-aa40-2f813e86766f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a354e9e-8cd9-45aa-9f8f-74a2a061bf7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e150e113-aa94-4ade-aa40-2f813e86766f",
                    "LayerId": "bcc0b34c-830f-4a34-a966-403c7fc3776c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "bcc0b34c-830f-4a34-a966-403c7fc3776c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "343309c9-8010-48fd-a28b-9cc76a649cf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}