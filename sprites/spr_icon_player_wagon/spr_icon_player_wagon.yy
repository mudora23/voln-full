{
    "id": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_player_wagon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 1,
    "bbox_right": 48,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd8a5cea-a5b6-4326-9286-20a90f64b2a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "193da7b5-c12f-47af-b4c6-b69e06d20d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8a5cea-a5b6-4326-9286-20a90f64b2a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33507213-1cf6-41e4-81fe-bcdcca999a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8a5cea-a5b6-4326-9286-20a90f64b2a4",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "deaabc63-9ee1-461f-8ff6-f7cdd5d91068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "8a41eea7-0268-48fe-a1f1-ea3285314430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deaabc63-9ee1-461f-8ff6-f7cdd5d91068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657f349e-1aad-4d18-ad47-d239225278e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deaabc63-9ee1-461f-8ff6-f7cdd5d91068",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "009f48eb-f568-48c9-9e8c-2e6e953707df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "50cd112d-d588-460b-bd97-517b7e77d723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "009f48eb-f568-48c9-9e8c-2e6e953707df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c337af-ccbb-4ef5-a770-d2db0d1b9104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009f48eb-f568-48c9-9e8c-2e6e953707df",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "fc4587c4-dcbf-4cb5-9442-5639c287d781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "6f635193-6160-4b9f-9e53-37f91c7d1e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc4587c4-dcbf-4cb5-9442-5639c287d781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601b07ba-7d5c-47e8-bbf0-3f5a7a33f0b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc4587c4-dcbf-4cb5-9442-5639c287d781",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "9d7e1d34-b564-4bad-9666-ab8b54e55a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "53b1d12f-e80b-44f9-820e-293c8ddb13b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7e1d34-b564-4bad-9666-ab8b54e55a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1b6b23-a7bb-417a-bb3f-b61b4e3c23b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7e1d34-b564-4bad-9666-ab8b54e55a69",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "29a88e38-eb88-49b4-802f-eb0dee45bdd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "02b4ff8c-7f3b-4ce1-a7b0-14be545a0481",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a88e38-eb88-49b4-802f-eb0dee45bdd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d51993d-e93a-4907-a830-0b488e15a2e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a88e38-eb88-49b4-802f-eb0dee45bdd0",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "19dffdde-780d-4561-ad3a-449207268ba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "08f9d929-0ab5-4c46-b7fb-78f300cbaba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19dffdde-780d-4561-ad3a-449207268ba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ef1490-000a-47ce-9c35-959328f68f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19dffdde-780d-4561-ad3a-449207268ba7",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "b2d3224e-18a5-4a12-be17-7382d2b43a1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "ecdf3926-2461-48c2-8c91-b2e6a1b76bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2d3224e-18a5-4a12-be17-7382d2b43a1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd13da7-f2d4-45a8-b05c-65ab0e49fc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2d3224e-18a5-4a12-be17-7382d2b43a1a",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "fbd3f820-c593-44d5-9b25-baf81e65a0f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "0f8576a3-7711-4dd5-b155-beb64d419035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd3f820-c593-44d5-9b25-baf81e65a0f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d29dd9-9a2c-4b89-a7dc-b4bc043fe7f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd3f820-c593-44d5-9b25-baf81e65a0f8",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "daf7b222-771f-4fd5-8515-f71f877c9ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "2a0fc4db-28b2-41b1-9b55-8edc684c9456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf7b222-771f-4fd5-8515-f71f877c9ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3265aa59-5f4a-421c-bc98-167fa3f2a60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf7b222-771f-4fd5-8515-f71f877c9ecf",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "990e2a3d-c5c7-40d7-9c26-5f8cd3f3caa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "8f8f4726-3e2b-420e-a495-c0a146cb5e1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "990e2a3d-c5c7-40d7-9c26-5f8cd3f3caa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcceab64-dc4b-49be-a9c5-09f9e7d76e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "990e2a3d-c5c7-40d7-9c26-5f8cd3f3caa6",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "c35a8319-1816-4466-8c3b-23451607fd1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "47cfb847-705b-48fa-821f-44a19ae63a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c35a8319-1816-4466-8c3b-23451607fd1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a03703b7-1374-4e25-a439-881a0d2d31d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c35a8319-1816-4466-8c3b-23451607fd1e",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "29c4c7b1-af30-4e2b-8665-69871ce434e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "fafb8a2f-67c5-4c41-b1ab-c89e6b293b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c4c7b1-af30-4e2b-8665-69871ce434e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4acee387-4158-450f-bcfd-a51b4208787d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c4c7b1-af30-4e2b-8665-69871ce434e8",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "964ecce5-9507-458e-9e9f-946f4864634d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "036f4fd2-dad3-44da-8206-ce0a439a8447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "964ecce5-9507-458e-9e9f-946f4864634d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c041d200-1994-491a-9f81-98650986d998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "964ecce5-9507-458e-9e9f-946f4864634d",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "7b69af0f-812a-4225-a456-71a4cced4175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "04fc5031-4b11-47b1-a3f0-4f4a2a1abb16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b69af0f-812a-4225-a456-71a4cced4175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d086db-ffa5-4b05-a114-b491264aa2a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b69af0f-812a-4225-a456-71a4cced4175",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "429622bc-dd7d-48c4-99b2-1d8d3cb60fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "d4d4b67f-4d66-4083-ad1a-d31aa92f6173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "429622bc-dd7d-48c4-99b2-1d8d3cb60fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0f7735-8252-4148-8958-e96f68272882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "429622bc-dd7d-48c4-99b2-1d8d3cb60fc4",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "d84dfa04-7764-416d-82f5-c0c27e14c4f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "3a110ea6-f138-47ba-8f97-32f675c1b6cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84dfa04-7764-416d-82f5-c0c27e14c4f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a037eb26-20aa-49ba-afbd-71feb01e41dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84dfa04-7764-416d-82f5-c0c27e14c4f2",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "a7070311-9d75-4603-a61b-005c864b1df6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "ace482a8-f465-4b67-8a6c-00ddbe6d538b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7070311-9d75-4603-a61b-005c864b1df6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0141cb8d-a67b-4240-a174-6ebbb77d429c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7070311-9d75-4603-a61b-005c864b1df6",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "12c03400-0a5c-464f-82bc-5e9666090623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "c18d324c-9781-4956-9f4d-91ab99a088aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c03400-0a5c-464f-82bc-5e9666090623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "406a57bb-23ad-44a6-91a1-b4e199619094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c03400-0a5c-464f-82bc-5e9666090623",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "9f8d4645-cd2c-4e50-b2f7-ad1069cc9c8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "31ad36f4-371c-43cd-a5bf-45e8d865ca60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8d4645-cd2c-4e50-b2f7-ad1069cc9c8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c07e77-18e4-412c-bb8d-5ed8b2685ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8d4645-cd2c-4e50-b2f7-ad1069cc9c8f",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "a9031b1e-a179-402d-b901-2d1b5eb9f991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "c3d8dfa4-a316-4975-b4a8-7a7a5db0dc47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9031b1e-a179-402d-b901-2d1b5eb9f991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c33a0250-2b68-45e9-bc5d-b4817222c5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9031b1e-a179-402d-b901-2d1b5eb9f991",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "a4984223-1bed-4666-9c92-3cdeca9aaf7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "e5ef64f7-3df0-47cd-b035-f986b5bb4b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4984223-1bed-4666-9c92-3cdeca9aaf7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9c1516-0136-4854-bf3c-4ebb4a029991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4984223-1bed-4666-9c92-3cdeca9aaf7e",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "921a5e8f-383c-4c2f-be60-db6b0546fda1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "c2235c1e-3d3c-4150-bcc3-3486042e63ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921a5e8f-383c-4c2f-be60-db6b0546fda1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb3c2b5f-45b4-45e0-885a-23388620b2d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921a5e8f-383c-4c2f-be60-db6b0546fda1",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        },
        {
            "id": "ced0d157-4ac0-41fb-bded-56241e879fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "compositeImage": {
                "id": "6fca2209-aa4f-44c8-a7a5-46969079beb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced0d157-4ac0-41fb-bded-56241e879fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0860ee9-6b8c-4a2c-9f28-d8fa130595ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced0d157-4ac0-41fb-bded-56241e879fd0",
                    "LayerId": "5ee62f9d-2852-401b-8583-bb51f51761eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "5ee62f9d-2852-401b-8583-bb51f51761eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 35
}