{
    "id": "f8deb84e-f444-4baa-8691-012df8ed804d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da665025-71c5-4108-be52-1e8432223f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8deb84e-f444-4baa-8691-012df8ed804d",
            "compositeImage": {
                "id": "f3ba6add-0763-48c0-891b-7631755e6000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da665025-71c5-4108-be52-1e8432223f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc0590c-1bfc-439a-b1af-df25c58c341a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da665025-71c5-4108-be52-1e8432223f3f",
                    "LayerId": "82fdce9b-6e62-462e-88d4-9143f2975d20"
                }
            ]
        },
        {
            "id": "43b168b5-871e-4cc0-a57e-2847482fdef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8deb84e-f444-4baa-8691-012df8ed804d",
            "compositeImage": {
                "id": "df1f458a-16e7-4067-8283-190e1fc7692d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43b168b5-871e-4cc0-a57e-2847482fdef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beec0705-f68d-4c7b-819a-fe91b8280b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43b168b5-871e-4cc0-a57e-2847482fdef7",
                    "LayerId": "82fdce9b-6e62-462e-88d4-9143f2975d20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "82fdce9b-6e62-462e-88d4-9143f2975d20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8deb84e-f444-4baa-8691-012df8ed804d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}