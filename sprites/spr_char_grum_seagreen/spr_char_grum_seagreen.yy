{
    "id": "b4113b2d-41ff-4e51-9a5f-285c014c7f12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_seagreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b1d6dfa-e22d-4003-b029-8cce4166d7f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4113b2d-41ff-4e51-9a5f-285c014c7f12",
            "compositeImage": {
                "id": "91d0ff08-801c-4d72-83ed-b0e4b65ba56d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1d6dfa-e22d-4003-b029-8cce4166d7f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "379dade1-da55-4f9b-aa45-21839bb65572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1d6dfa-e22d-4003-b029-8cce4166d7f4",
                    "LayerId": "bb549689-935d-47ba-942e-8f3ecaa3db49"
                }
            ]
        },
        {
            "id": "27857011-52d2-4995-bca6-c28776708c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4113b2d-41ff-4e51-9a5f-285c014c7f12",
            "compositeImage": {
                "id": "ca8755ce-7a8f-4530-bacf-1c9692aff8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27857011-52d2-4995-bca6-c28776708c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cdbfc59-415e-4a48-83cc-a699ca19f371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27857011-52d2-4995-bca6-c28776708c84",
                    "LayerId": "bb549689-935d-47ba-942e-8f3ecaa3db49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "bb549689-935d-47ba-942e-8f3ecaa3db49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4113b2d-41ff-4e51-9a5f-285c014c7f12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}