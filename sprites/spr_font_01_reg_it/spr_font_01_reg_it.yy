{
    "id": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_reg_it",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e555edc5-76da-4bd1-8fba-75f501604acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ec89ae2b-4e54-4c5c-b14d-1bdc9c6791cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e555edc5-76da-4bd1-8fba-75f501604acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69210d28-f70f-4140-b4e8-84a5969944b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e555edc5-76da-4bd1-8fba-75f501604acf",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "0576ae2d-49c9-40cd-8cce-b3a51ea932db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "e2f3188e-3ce3-4a23-840b-1ac72d48f44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0576ae2d-49c9-40cd-8cce-b3a51ea932db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb144d15-5bb4-4f2a-b50c-d8be4c7fd85a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0576ae2d-49c9-40cd-8cce-b3a51ea932db",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "cd4dd243-fad8-4657-89ed-f88211da78a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "76683aa9-7440-48fb-a111-567494ea23d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4dd243-fad8-4657-89ed-f88211da78a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b971f1b8-ffb8-488b-8073-db613b708b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4dd243-fad8-4657-89ed-f88211da78a5",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "4062b9a2-9070-44b0-afc2-3e73f76fd86c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "285339b2-92dd-4a29-8b8e-821332973f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4062b9a2-9070-44b0-afc2-3e73f76fd86c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e6abd0d-e6a8-4fd5-a49e-dd6129898cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4062b9a2-9070-44b0-afc2-3e73f76fd86c",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "545d787a-36b5-465c-b027-f2a2dc2f7538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "deee0c3c-ca12-4f5d-86e4-05954d32bc3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "545d787a-36b5-465c-b027-f2a2dc2f7538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "325d853f-0985-4e9d-878d-fdd50919b345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "545d787a-36b5-465c-b027-f2a2dc2f7538",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "462c8c63-cf88-49f6-bef2-9b241c268398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "c18c558c-5e93-4586-999a-03016835c639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462c8c63-cf88-49f6-bef2-9b241c268398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85212d46-330d-4bf6-8685-6669796f5e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462c8c63-cf88-49f6-bef2-9b241c268398",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "5ed19a84-6c14-4f4a-af73-5ff416086dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d8bab333-c48e-467c-a075-554b3902b9a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ed19a84-6c14-4f4a-af73-5ff416086dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2030efaa-fd43-4239-ab49-73b70c7372e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ed19a84-6c14-4f4a-af73-5ff416086dd1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "74c97b40-864d-46b4-8d40-3fad54ba32c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "52428306-ba65-49d4-988f-339edd5215c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c97b40-864d-46b4-8d40-3fad54ba32c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f4dfc71-db8b-45df-96e1-3d116d731e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c97b40-864d-46b4-8d40-3fad54ba32c1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9068b788-832f-443f-a182-dfeebd183148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d58aa794-ecbe-4b06-b6d4-0d7562c65063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9068b788-832f-443f-a182-dfeebd183148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5004f9-6c96-4ff6-b6a7-b03b3d9d301f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9068b788-832f-443f-a182-dfeebd183148",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "8b3db855-511d-4d87-ae65-a3266e5ca20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "0bfc09e8-9dcd-453f-828d-c478e9ba7eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3db855-511d-4d87-ae65-a3266e5ca20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83495809-48a9-411d-8de4-49fd6c968efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3db855-511d-4d87-ae65-a3266e5ca20b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "375c63f2-af47-470b-a108-4ca29935ea61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "b04bcfdc-96ff-462f-85fd-d948b8e239a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375c63f2-af47-470b-a108-4ca29935ea61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62821ad9-69cb-4cd5-bfe2-f0096afda2d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375c63f2-af47-470b-a108-4ca29935ea61",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "28ba8064-5363-4e07-8426-964b06e20963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "136d67f8-0bd0-48bd-82a5-fa063386c22b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ba8064-5363-4e07-8426-964b06e20963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6603d40-11a3-4867-8642-b02ec4eb2752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ba8064-5363-4e07-8426-964b06e20963",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "3ccf7d9e-998f-49ff-866c-c1bd0e004ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d7e297c9-2625-4012-a2f7-58af893968ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ccf7d9e-998f-49ff-866c-c1bd0e004ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd4ff38a-a28b-44db-be68-3b5620a0bb99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ccf7d9e-998f-49ff-866c-c1bd0e004ad0",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "aec77f59-d161-4e57-a962-2426e87185a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "47220b75-285b-45f6-a10a-803e72992d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec77f59-d161-4e57-a962-2426e87185a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f1fb717-3540-4f8a-a4ca-949cc86e191a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec77f59-d161-4e57-a962-2426e87185a5",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "6c52f3cf-86c8-4c3f-8d3a-fef8e610734b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "6abdce0d-9e5f-487b-8387-279791b4bf9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c52f3cf-86c8-4c3f-8d3a-fef8e610734b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9abd8d4f-5ff1-4976-8c4e-6467fee4ce39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c52f3cf-86c8-4c3f-8d3a-fef8e610734b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "699d5f07-4543-4477-93d3-6f8e362b710f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "39855477-b754-4910-a634-d5a3ec6969b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699d5f07-4543-4477-93d3-6f8e362b710f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "973dd33b-42e1-420a-aff0-fa29649acc33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699d5f07-4543-4477-93d3-6f8e362b710f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "3d20ef63-507e-4418-8ce6-bc5441919c07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "6456a7a4-ef03-4fa6-8d37-97aeb2a46c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d20ef63-507e-4418-8ce6-bc5441919c07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "083df5ab-8764-4cc9-b246-0e139fe002f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d20ef63-507e-4418-8ce6-bc5441919c07",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "0a82c415-6df0-43e3-82ca-b69f41cabcff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "cf1a2fda-bc15-4743-81f9-399b899b8f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a82c415-6df0-43e3-82ca-b69f41cabcff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6be86cf-2aab-4fc4-afe4-67242d7160c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a82c415-6df0-43e3-82ca-b69f41cabcff",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "fe9ad340-0197-457c-b782-66077bb0806f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d329ef30-5a55-400c-a5b5-946d45f79df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe9ad340-0197-457c-b782-66077bb0806f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f644cd-a7f1-4f61-bf10-745ecda13b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe9ad340-0197-457c-b782-66077bb0806f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "ef96be58-65ff-419c-a933-1eaa652b2141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "9688b062-c12d-4a7c-a7b7-7e64fa7e2313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef96be58-65ff-419c-a933-1eaa652b2141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac1d92db-5b07-4078-a5c8-1826e1c32db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef96be58-65ff-419c-a933-1eaa652b2141",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "684aa12d-614b-470a-9f25-c5f1d929940a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "78318755-ea54-4da7-a402-7d588a26c735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "684aa12d-614b-470a-9f25-c5f1d929940a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "624f3551-db51-4575-a3ee-591494dc8fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "684aa12d-614b-470a-9f25-c5f1d929940a",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9954e78a-ec29-4a4e-be19-30b12b6e2d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ded1f7db-4338-406a-a175-593ad0d0b497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9954e78a-ec29-4a4e-be19-30b12b6e2d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3edec073-2ed7-42e9-b80b-0128254eac61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9954e78a-ec29-4a4e-be19-30b12b6e2d58",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "94bb8a14-20a9-4d51-8484-2c6ff2a56ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "7c6a26d8-20c1-49ca-8354-a05acc2e49de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94bb8a14-20a9-4d51-8484-2c6ff2a56ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9460a06e-c22a-4527-b454-ba20a20f8a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94bb8a14-20a9-4d51-8484-2c6ff2a56ccd",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "30e7ce5e-5349-424f-83c2-70c7e903b649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4b07c6fe-c016-48cc-9b3a-42e36cdceba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e7ce5e-5349-424f-83c2-70c7e903b649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b47ff6b-849b-4641-8cd5-4c08474fa505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e7ce5e-5349-424f-83c2-70c7e903b649",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "5af29d10-0be2-4b02-ad90-6efeb5f4e4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "43e6975c-24e9-4228-bdc8-9c7df37e2bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af29d10-0be2-4b02-ad90-6efeb5f4e4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa91d492-55d2-408e-b0c6-a7cba1c8adb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af29d10-0be2-4b02-ad90-6efeb5f4e4f0",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "b15afae5-dc1a-4f79-83e9-594e9085dad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "cdbb07f6-40e6-4d11-aa07-bbc8abafda28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b15afae5-dc1a-4f79-83e9-594e9085dad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7600255f-6190-4c58-89fd-0fc2008ccc02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15afae5-dc1a-4f79-83e9-594e9085dad6",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "350d326e-6601-4eae-8be6-568a9ca8ae7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ca1a8025-da26-429d-b89b-98470b0caafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350d326e-6601-4eae-8be6-568a9ca8ae7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f651ea11-eae2-48a3-ab58-fd60d7187cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350d326e-6601-4eae-8be6-568a9ca8ae7d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "6d18c6e3-9d83-4add-badb-d8c37dec0ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "8f46366d-4d91-4e2c-9a02-49c81435930e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d18c6e3-9d83-4add-badb-d8c37dec0ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0962263d-1f11-486f-9a33-cb191a44cbe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d18c6e3-9d83-4add-badb-d8c37dec0ddd",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "20fcdbf6-465f-4fc6-8c53-f2f34a7506bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ecabc5ef-985e-433a-a43b-d6b48dbeed7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20fcdbf6-465f-4fc6-8c53-f2f34a7506bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7caf72-bec3-40a4-994b-2a656310a77f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20fcdbf6-465f-4fc6-8c53-f2f34a7506bf",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9b8be58d-b99f-4c75-b332-16eed49a023d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "41b844cf-788a-4168-855f-f1112afa7dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b8be58d-b99f-4c75-b332-16eed49a023d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188fa8aa-7290-4c71-9198-05ae794e833f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b8be58d-b99f-4c75-b332-16eed49a023d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "d2f920d3-6fb8-4fbb-bb45-755c668cbd52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "2bd97832-9bb5-405a-b725-0a35a76e209c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f920d3-6fb8-4fbb-bb45-755c668cbd52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f7c288-00cd-4d9b-97c0-2c6687317498",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f920d3-6fb8-4fbb-bb45-755c668cbd52",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "42117fce-80af-440a-a34a-d653f9c7e8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "fc7a36d2-4b91-42c3-84a5-2ccd9674e483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42117fce-80af-440a-a34a-d653f9c7e8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "982c2e16-7a16-4efc-8c50-5dc55f09b304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42117fce-80af-440a-a34a-d653f9c7e8f7",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "d20f08f7-544c-4029-9a38-26d4e0015229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "555d07c3-91db-4d29-b292-2f3062e7af5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d20f08f7-544c-4029-9a38-26d4e0015229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9705bf-1c42-4db6-a0bf-d719868868c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d20f08f7-544c-4029-9a38-26d4e0015229",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "1258328c-ac15-413f-a769-64c5eae3aac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4f5b5b0a-28bf-4549-8e1f-a32638338931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1258328c-ac15-413f-a769-64c5eae3aac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522d9d77-1028-4bba-8301-a2f28a0d1f00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1258328c-ac15-413f-a769-64c5eae3aac0",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "62a3f3c4-93ff-4fad-908c-ce4c81d1864b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "0fd21b51-e1fc-4aa8-b9a4-e35a8537819b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a3f3c4-93ff-4fad-908c-ce4c81d1864b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fbec5bb-b1cd-4fab-a020-18383787e3fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a3f3c4-93ff-4fad-908c-ce4c81d1864b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "2483025a-cbeb-4bf4-a14d-5cb0ac01f6cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "96cd5c29-09b4-47c8-a2ed-b434ccd6d905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2483025a-cbeb-4bf4-a14d-5cb0ac01f6cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6115c0d-9bb0-4322-a5b6-22c81d1564c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2483025a-cbeb-4bf4-a14d-5cb0ac01f6cd",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "7c2880cf-8414-436b-91ef-8986910deec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "72e14eff-aad8-4287-88ea-14839d42768f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2880cf-8414-436b-91ef-8986910deec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d3ee59-e62e-4feb-8e67-5c73d08cd921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2880cf-8414-436b-91ef-8986910deec0",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "a4d2bee8-e009-49c0-9cda-fb8557faf9fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "6ac0390a-eaf2-46a7-8d79-015c9b2548c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d2bee8-e009-49c0-9cda-fb8557faf9fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe69537-c0b6-4d50-bd2b-a1089c2aa825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d2bee8-e009-49c0-9cda-fb8557faf9fa",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "2e9c12b8-fe93-457f-a590-a7fa7c08bc81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "7902bfa9-91c9-494b-866d-77b1d6947bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9c12b8-fe93-457f-a590-a7fa7c08bc81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888eb850-1ee7-4e88-b11e-ded680f51869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9c12b8-fe93-457f-a590-a7fa7c08bc81",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "02e3a906-17c7-4838-bcb0-54e0871fe56a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "c2a34fdc-1793-408b-93b9-b8aeb82cec7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02e3a906-17c7-4838-bcb0-54e0871fe56a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59761028-4878-4dce-995e-b83718a06df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02e3a906-17c7-4838-bcb0-54e0871fe56a",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "3c52bc0a-9412-4f38-9071-3b8d899c2a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "f80515de-017f-4784-995a-4f5a9032e382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c52bc0a-9412-4f38-9071-3b8d899c2a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2288ed0-07b8-4ced-a4ad-1dee42f55bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c52bc0a-9412-4f38-9071-3b8d899c2a39",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "37b891de-1d8c-471e-9073-1d8be2ac0332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "255c8368-5290-4d3c-a363-62d874073147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b891de-1d8c-471e-9073-1d8be2ac0332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7081a1a5-36c5-411c-8546-f2cbaeacf097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b891de-1d8c-471e-9073-1d8be2ac0332",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "0300735e-e7dc-470e-8d01-6a1aff02b910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "602c79ec-7c56-4eec-b795-36cd6e48845f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0300735e-e7dc-470e-8d01-6a1aff02b910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595667f5-e8b0-41cb-9aa1-41dbedcaa73c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0300735e-e7dc-470e-8d01-6a1aff02b910",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "43a090d0-593e-4cc9-ab62-94476031970f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "e5563676-129b-4f32-ab7b-0ea6da8de002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a090d0-593e-4cc9-ab62-94476031970f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c81eab86-55c6-4c9e-ae8a-694800fec84d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a090d0-593e-4cc9-ab62-94476031970f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "52885cc0-9bb8-4a57-a77d-a76a4c7e3349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d607fdf8-bdf0-4694-abe3-62b68a762ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52885cc0-9bb8-4a57-a77d-a76a4c7e3349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d71bfa96-6b34-41c9-8cad-87189f8de417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52885cc0-9bb8-4a57-a77d-a76a4c7e3349",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "b070c048-a697-4c54-950c-1a5e3a3b77f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "152627ae-df0f-4aa8-b2ee-a5c20901875a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b070c048-a697-4c54-950c-1a5e3a3b77f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e8ad0e-cefd-4315-b4a5-3600882dbdc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b070c048-a697-4c54-950c-1a5e3a3b77f7",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "aa06aed9-c393-4820-bf3b-b727750391c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "e29779d6-c7b8-4be6-8599-bbbc46a2cf49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa06aed9-c393-4820-bf3b-b727750391c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e92639-1f90-42e8-b68e-4c327cc7cd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa06aed9-c393-4820-bf3b-b727750391c1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "5e0eda0f-a4eb-40d6-b73e-bd69abf9e716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "fece605d-6e0f-4563-b5b7-80e828d79385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e0eda0f-a4eb-40d6-b73e-bd69abf9e716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a56474d-6cdf-4d05-a21a-5893cf0839f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e0eda0f-a4eb-40d6-b73e-bd69abf9e716",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "77b31c77-1550-4344-bb4a-8028f161d59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "aa2c35f7-cf41-4c94-90a3-81bf61efb954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77b31c77-1550-4344-bb4a-8028f161d59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c510ba5d-3ca0-499f-ae1e-982172b8bfa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77b31c77-1550-4344-bb4a-8028f161d59d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "84e0a992-aca3-45ed-b0b6-2e2d14a9bbf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1dc6476d-02a8-4967-b5df-8d794e0c758b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e0a992-aca3-45ed-b0b6-2e2d14a9bbf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d8cb434-375a-4d91-b847-202eb0e5ab0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e0a992-aca3-45ed-b0b6-2e2d14a9bbf9",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "5976cdfb-2333-4d3c-a37c-e510f7b64a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ebdc093c-5068-44c0-8f39-ce517938afe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5976cdfb-2333-4d3c-a37c-e510f7b64a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d1bf8b-88ac-4729-8da1-b159939363fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5976cdfb-2333-4d3c-a37c-e510f7b64a0c",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "efe62857-8e3b-47ed-9126-ef2b181e6e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1db78612-ff68-4a7b-b090-1209ded2314e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe62857-8e3b-47ed-9126-ef2b181e6e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "723adc02-0062-4128-8f17-650909b6b62d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe62857-8e3b-47ed-9126-ef2b181e6e39",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "4c19fb23-1602-493a-994b-0b900d1119fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "134525bc-c80c-4a70-9ce6-91e374f9d58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c19fb23-1602-493a-994b-0b900d1119fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b247d4-3b91-43a8-8cfc-ff7730709ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c19fb23-1602-493a-994b-0b900d1119fc",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "f68ff212-2fb0-4bed-8ced-4599eb795501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "6b562d67-f7ab-43ae-bcee-61081cc1dd1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f68ff212-2fb0-4bed-8ced-4599eb795501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee49a723-69a6-48ef-b600-12d43198f6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f68ff212-2fb0-4bed-8ced-4599eb795501",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "d29cde2e-ea36-450d-83d7-83ea20501bf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "426c964f-6014-46ca-8015-7a1165783182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29cde2e-ea36-450d-83d7-83ea20501bf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51c6e6be-c4d9-4717-8ef0-8f9e04a4a7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29cde2e-ea36-450d-83d7-83ea20501bf8",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "ae4fc55b-a8f0-4fc8-aff8-eea237fbb77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "af6750ce-e833-4ee0-bdf7-f67166f5e73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae4fc55b-a8f0-4fc8-aff8-eea237fbb77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e4eaec-b491-4bc3-8b70-1ebbfd62f387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae4fc55b-a8f0-4fc8-aff8-eea237fbb77a",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "562a4054-5ec8-45aa-8ac3-7f0183717d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "c200e6de-d6af-4175-9b48-6558ecda521d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "562a4054-5ec8-45aa-8ac3-7f0183717d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5d0c069-a236-4970-a0fc-4460309c1a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "562a4054-5ec8-45aa-8ac3-7f0183717d57",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e79da1ed-63e0-4880-a459-e2b157ea5b36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "09669563-449c-41c9-bee2-5076d38b7544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79da1ed-63e0-4880-a459-e2b157ea5b36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7807f683-8360-4b54-b08e-61ff858c4c9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79da1ed-63e0-4880-a459-e2b157ea5b36",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "ad37e9b4-9013-41c3-bef2-e9a9e6fd0d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "8aeebb91-001a-432f-a363-f6d699acf62f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad37e9b4-9013-41c3-bef2-e9a9e6fd0d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23785ee2-fc84-451c-bae8-ca9ae19aa134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad37e9b4-9013-41c3-bef2-e9a9e6fd0d32",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "df66fe27-b3e3-408c-922f-b27fada94295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "9d28ea09-20e6-4428-932e-f9954ac8d3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df66fe27-b3e3-408c-922f-b27fada94295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "543d3d84-dcac-440b-9bb8-8894245468e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df66fe27-b3e3-408c-922f-b27fada94295",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "121a57c3-62ab-4030-a563-89e9e1dea049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "0b1a8b8d-c967-4aa5-ba41-3fc3e068c3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121a57c3-62ab-4030-a563-89e9e1dea049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2184a9ca-fcaa-4541-978a-b3e14e385f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121a57c3-62ab-4030-a563-89e9e1dea049",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "807bdeda-ae04-47c7-bddf-a40ba8ff801e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "926bd9e7-a143-4b0c-a9f4-5d31bc44a1d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807bdeda-ae04-47c7-bddf-a40ba8ff801e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c593094-8087-4f84-9c30-f92a80696c35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807bdeda-ae04-47c7-bddf-a40ba8ff801e",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e0c0cc99-bfec-46fa-bfff-258f1662807d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ecafa84e-01cb-4836-8aec-9a70c542f4fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c0cc99-bfec-46fa-bfff-258f1662807d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687e068a-8693-4568-b4f1-738b229a049f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c0cc99-bfec-46fa-bfff-258f1662807d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9e04037e-7394-4a74-834c-fe7530c8d8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a665682a-1c32-40eb-90a8-29ff728d3021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e04037e-7394-4a74-834c-fe7530c8d8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda6bdee-261b-49de-829e-e1b0e9b65288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e04037e-7394-4a74-834c-fe7530c8d8e9",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "7cfb7f41-9125-4759-8a74-ca9d61211dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "8aaee030-a00a-4b5f-a928-7ac199a53db3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfb7f41-9125-4759-8a74-ca9d61211dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4aef1f1-0d36-49a6-8e59-0c4795282820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfb7f41-9125-4759-8a74-ca9d61211dc3",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "69940e4a-977d-4180-b123-fd1164e03f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4a84da8d-80a9-45a0-a3b7-e6c8f78900ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69940e4a-977d-4180-b123-fd1164e03f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "347b3a97-0e34-4306-8b60-ce154abe6454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69940e4a-977d-4180-b123-fd1164e03f47",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "1e363e3d-979d-4717-9702-3176dc2e2649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "bda5f40d-ab65-4499-83ce-210dde5d3742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e363e3d-979d-4717-9702-3176dc2e2649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f781f2-75bb-46f8-84c2-13149f4f4603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e363e3d-979d-4717-9702-3176dc2e2649",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "cc43cf32-9e8e-4b39-b1e1-40d794d77491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4b3b6d36-2262-4541-ac7f-6817a8a09757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc43cf32-9e8e-4b39-b1e1-40d794d77491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126effec-9d6e-46b6-81c1-262cc16d3f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc43cf32-9e8e-4b39-b1e1-40d794d77491",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "bd2fe132-4f1d-4985-92bc-b1525652fb9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "87be1638-a942-4ad9-b2ac-d062dea1269c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd2fe132-4f1d-4985-92bc-b1525652fb9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141136e5-5a76-448f-9f88-2f75324effdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd2fe132-4f1d-4985-92bc-b1525652fb9d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e67f985d-96a1-47e8-b9ff-bde1b4eec671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "7d736eee-5904-4c22-aeb0-bc4d8432cbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67f985d-96a1-47e8-b9ff-bde1b4eec671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f57290-dac4-4f5b-aa40-172957a8a473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67f985d-96a1-47e8-b9ff-bde1b4eec671",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "cf1def7e-e4e1-4cf2-b32c-a881b56b1a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "03b40707-ee6c-45d3-8d78-c2a0c7e19e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf1def7e-e4e1-4cf2-b32c-a881b56b1a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1e205f-715d-494e-b460-613fdd87f0d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1def7e-e4e1-4cf2-b32c-a881b56b1a75",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "62a0f504-d582-4f28-8095-acc3159fb37c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "f537b708-3126-4505-b5e2-9dbb0133889c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a0f504-d582-4f28-8095-acc3159fb37c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a408765-4947-4b0a-bbae-fff1e5c2f307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a0f504-d582-4f28-8095-acc3159fb37c",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "90aef2e7-2c4f-4962-8901-0f830d5bf07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a8dccffc-b6ef-4cf1-9bfd-36e59acf9442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90aef2e7-2c4f-4962-8901-0f830d5bf07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9b03a2-a534-44e0-8ad5-edf5c28f8881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90aef2e7-2c4f-4962-8901-0f830d5bf07d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "dce3ccab-ad4e-4445-b991-c5adf1ae9eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "2422aca8-b40f-402d-89ba-79ed3a140801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce3ccab-ad4e-4445-b991-c5adf1ae9eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c38448f-7a72-4800-9322-cfaedee2415d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce3ccab-ad4e-4445-b991-c5adf1ae9eb1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "01d900fa-05da-48b3-a2c3-4144b669970b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "583e3505-c882-4c35-9a2c-1c3722ea8ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d900fa-05da-48b3-a2c3-4144b669970b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0ea767-c285-40a9-9560-41651a79b0a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d900fa-05da-48b3-a2c3-4144b669970b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "053af7ca-c2c4-4819-96bb-472d976169ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "72697a61-6eaf-4f12-ada0-a8bd4c545dd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053af7ca-c2c4-4819-96bb-472d976169ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2923324d-8c7d-4c19-830f-c70aa7419b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053af7ca-c2c4-4819-96bb-472d976169ae",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "14f32505-9eaa-44b8-8c18-e31a91401185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "20f980bc-ca76-479c-b079-b4b9656890fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f32505-9eaa-44b8-8c18-e31a91401185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125bb31b-7484-494d-9953-08da5c030799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f32505-9eaa-44b8-8c18-e31a91401185",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "2fb8a096-8999-4622-81bf-66fafcf7213f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1f7e1228-3973-40b4-b347-f161c2029941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb8a096-8999-4622-81bf-66fafcf7213f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f611b3-d7c0-44a5-8dce-d46b1cdac744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb8a096-8999-4622-81bf-66fafcf7213f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "6d4fadcf-006f-40f2-baee-df4f7276a655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "722a3422-3a83-447f-b414-b6abe9ab3282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d4fadcf-006f-40f2-baee-df4f7276a655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f76658b-611d-47bc-bd51-6ffded9becf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d4fadcf-006f-40f2-baee-df4f7276a655",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "1d645d4a-1472-4773-8a74-98b1861d5994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "2b83e23b-2ba4-4d8c-b533-edae5ae957e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d645d4a-1472-4773-8a74-98b1861d5994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b212dbae-b54d-43ba-99a1-0272cd7eabe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d645d4a-1472-4773-8a74-98b1861d5994",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "4ea37630-39c7-436d-b935-ac6f15397b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "8f3b0af1-baa9-43fb-9a50-5f674f7ea4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ea37630-39c7-436d-b935-ac6f15397b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9396a009-43c6-4a34-9b92-5e582e63f1e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ea37630-39c7-436d-b935-ac6f15397b19",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "74248d5d-6d92-4fff-b86c-d83a2f3a46c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a956bbc6-f5d0-4c79-8f06-d830d03c0d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74248d5d-6d92-4fff-b86c-d83a2f3a46c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2012f3ca-db55-4cb8-8a3b-804944cb458f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74248d5d-6d92-4fff-b86c-d83a2f3a46c8",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "731ba71a-ce76-419a-bae3-bae804ee6616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4b577981-0666-4beb-b5cb-29b8092c017a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "731ba71a-ce76-419a-bae3-bae804ee6616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5071d28-037f-479c-a377-20c44d1e7806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "731ba71a-ce76-419a-bae3-bae804ee6616",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "12ea2e91-9e35-4ca7-ba83-c24514e07b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "5a649b84-a185-46d4-a136-65a509e9d103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ea2e91-9e35-4ca7-ba83-c24514e07b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6871d5ea-5e11-45e6-8287-3aa77986d415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ea2e91-9e35-4ca7-ba83-c24514e07b08",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "11e60b55-d886-4494-820b-cc8e4aefa685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "2bd6c428-e7d8-470e-8540-4d99b2c1bacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e60b55-d886-4494-820b-cc8e4aefa685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d497b29c-6964-48d5-8343-273c7a38c45a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e60b55-d886-4494-820b-cc8e4aefa685",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "05545f4c-e8ad-4556-a1b8-a1e1174b6ec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "584a98b9-4d65-43b1-a160-c38aa471d235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05545f4c-e8ad-4556-a1b8-a1e1174b6ec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041f28fe-2338-4d9b-8a7f-b62a1e5ab4c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05545f4c-e8ad-4556-a1b8-a1e1174b6ec6",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "116c239e-f1e4-4633-9ac0-afe6bca78a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "51993f14-c9f9-4f31-b6a4-58ce74a05b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116c239e-f1e4-4633-9ac0-afe6bca78a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e764d1f-7216-497b-a367-9506431cd31f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116c239e-f1e4-4633-9ac0-afe6bca78a10",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9e1e13d8-ffb8-4c31-8c67-a1ed913144b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "eeb5dbee-b58e-4abd-9a58-bce4a588ebee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e1e13d8-ffb8-4c31-8c67-a1ed913144b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33cf01e0-3c57-4db3-8d91-c6e06bd86c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1e13d8-ffb8-4c31-8c67-a1ed913144b5",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "aeb861d5-97df-434c-b2ee-ae76899597f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "dae03a80-94c2-4a6c-988c-e0fa659d720e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeb861d5-97df-434c-b2ee-ae76899597f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c2ba322-07ad-4b8c-a08f-37aca44c009b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeb861d5-97df-434c-b2ee-ae76899597f1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e3426d53-2bf2-4157-a5e1-d5bf9ab0831d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "17725a6a-618f-43e6-82e6-85f1f66a07a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3426d53-2bf2-4157-a5e1-d5bf9ab0831d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe55ce27-a1f7-4c3d-9f88-078d99d0e2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3426d53-2bf2-4157-a5e1-d5bf9ab0831d",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "eda56104-ecc4-4ca2-b2be-744099349e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "0cd8fecc-f45a-481f-b480-3e2f46be89e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda56104-ecc4-4ca2-b2be-744099349e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c5be6c-d0e8-4f54-94ba-66d2991a5c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda56104-ecc4-4ca2-b2be-744099349e76",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "24426b18-64e1-4084-a534-844f8ac5e825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ba32fe13-90d4-4412-b15d-06a441ccbf97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24426b18-64e1-4084-a534-844f8ac5e825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80a03bc-5d2a-43a8-b3fe-4231b078df62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24426b18-64e1-4084-a534-844f8ac5e825",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "75758ad5-9be4-4b0b-b0c4-4a36978a1e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "f7ef140f-4966-476f-9255-0fea99e60440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75758ad5-9be4-4b0b-b0c4-4a36978a1e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566ba87d-2e22-49ac-9823-f16cb3f6e549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75758ad5-9be4-4b0b-b0c4-4a36978a1e5b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e346301f-84f0-496e-a72b-28bcb09bb25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "e89193b4-5fcd-46a7-8ce5-357e0ddee99f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e346301f-84f0-496e-a72b-28bcb09bb25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716a21a1-6a89-4b49-a08d-4d9d6da067c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e346301f-84f0-496e-a72b-28bcb09bb25b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "098454a0-8d97-402d-a96f-0bc0e366bcb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "2fec0929-9fc0-4994-a40b-2f928c15551b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098454a0-8d97-402d-a96f-0bc0e366bcb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c25840-7198-48b8-ac15-3915ba443f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098454a0-8d97-402d-a96f-0bc0e366bcb3",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e89cd5ae-848e-4d44-9fb7-b5b734cb629b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a3e6ee90-c7f9-4c88-8f81-a0885259134d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89cd5ae-848e-4d44-9fb7-b5b734cb629b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd05f6f-6871-43e6-9cac-6bee5f90ee2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89cd5ae-848e-4d44-9fb7-b5b734cb629b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "3b935cb6-5c5e-403a-a2b1-1296fee86ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "fb6640ed-b65b-4202-99e4-24ae1374509b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b935cb6-5c5e-403a-a2b1-1296fee86ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50c20d48-b2f8-4373-8cc9-80b98f953b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b935cb6-5c5e-403a-a2b1-1296fee86ffc",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "02d18d60-8b9e-479a-8c85-f43b1d11fcc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "776d85f9-b166-4bc7-aae0-c30ee131ee05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d18d60-8b9e-479a-8c85-f43b1d11fcc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b9e457-2d87-459e-b8dd-f48497e7adb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d18d60-8b9e-479a-8c85-f43b1d11fcc4",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e77ffd21-1025-4249-8de6-033449aa4add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "0d291b84-7d67-4d12-9de6-5642277ecb6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e77ffd21-1025-4249-8de6-033449aa4add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9617d455-71dc-4804-9355-4f4fb93b2695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e77ffd21-1025-4249-8de6-033449aa4add",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "5c7e4957-6575-438b-9620-0eba0c13f777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a875992a-3c75-4130-8c82-2ed45271aef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7e4957-6575-438b-9620-0eba0c13f777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e56ae8-42b2-4c34-a331-602695b7f8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7e4957-6575-438b-9620-0eba0c13f777",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "4d29a71b-6039-4f79-b220-07df34a34c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "674d7ff0-17cf-4d2d-8e0f-7bb33f661b96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d29a71b-6039-4f79-b220-07df34a34c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d2e4f3-a0f6-4031-8054-12acb192ec18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d29a71b-6039-4f79-b220-07df34a34c30",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "c1fc7c1d-180e-4caf-866a-f237eb70b4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4d4d12e6-d8eb-4b9d-ba92-f38e3e01afb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1fc7c1d-180e-4caf-866a-f237eb70b4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa62b29c-2235-4032-aef5-53647d41a415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1fc7c1d-180e-4caf-866a-f237eb70b4b0",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9770ba79-0206-40de-a924-2b534896a0a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "24fd1ba0-4643-45c8-9e72-f85b918ab32f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9770ba79-0206-40de-a924-2b534896a0a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c61613-65c9-4985-909c-796243635cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9770ba79-0206-40de-a924-2b534896a0a5",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "55dbe27d-075f-4f1f-b30b-92168e3bace0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "3628e8bf-5770-41e0-855c-21ef3889affc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55dbe27d-075f-4f1f-b30b-92168e3bace0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5a19e5d-4c81-42c6-a739-a4c2ccc227fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55dbe27d-075f-4f1f-b30b-92168e3bace0",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "772f44ae-8ee5-4d6c-a6d8-f074187659cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "51e546a2-d92d-4340-9882-99910c8e69f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772f44ae-8ee5-4d6c-a6d8-f074187659cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68ab444-ec9c-4795-83bd-f4a7e1fe677c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772f44ae-8ee5-4d6c-a6d8-f074187659cc",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "bb76ea4f-eee2-4502-8c5b-bfce88a40b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "75c27b35-03ca-4d6f-a266-c4823e0b8d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb76ea4f-eee2-4502-8c5b-bfce88a40b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe70918e-4978-47ad-956e-708e13d28ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb76ea4f-eee2-4502-8c5b-bfce88a40b47",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "8fe6fd50-7fa1-4c04-970f-9daada32a364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d3f76710-c7fd-4011-9606-799b1a6b8eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fe6fd50-7fa1-4c04-970f-9daada32a364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600dc865-0331-4d9e-86eb-13bb89bbba98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fe6fd50-7fa1-4c04-970f-9daada32a364",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e5e58b55-4b14-48b7-a045-753d4448c43b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "22f7dca1-4d53-4544-a664-f052c1365c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e58b55-4b14-48b7-a045-753d4448c43b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e0451ae-a686-4b68-96c0-2ff9d20c1978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e58b55-4b14-48b7-a045-753d4448c43b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "eff2f5d5-dc97-431b-9564-25ccef9c2de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "7fd9fe16-b343-44c0-9480-65a33de12c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff2f5d5-dc97-431b-9564-25ccef9c2de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ce615d-c0ab-4463-9249-a4cf73b563f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff2f5d5-dc97-431b-9564-25ccef9c2de1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "3bbef2f6-b828-4d35-87b9-006ad0da8d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "ff9fedce-fac2-4be2-9cb9-2a64e4052a1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bbef2f6-b828-4d35-87b9-006ad0da8d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3df5bec-e22e-442b-8fe7-26b0d8dcb986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bbef2f6-b828-4d35-87b9-006ad0da8d87",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "dec1ac39-ada2-4223-990a-b84172179bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "3edf2cab-e2e6-43af-8a1e-a08214008118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec1ac39-ada2-4223-990a-b84172179bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5684e013-00c5-4ed7-935b-f1d20b900203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec1ac39-ada2-4223-990a-b84172179bb3",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "128f2a32-b9f9-4daa-98a3-12a9af9bb933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "c929e54e-7f4a-4aca-8068-9c1fbf96fee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "128f2a32-b9f9-4daa-98a3-12a9af9bb933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7f2be89-0aa1-42c9-afc5-91180c903f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128f2a32-b9f9-4daa-98a3-12a9af9bb933",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "cf5ca847-862d-4425-b399-31d23272805c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "4608bae5-d502-4cb7-9085-5451bc6087ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf5ca847-862d-4425-b399-31d23272805c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342e6393-1795-41fe-b864-e3bbc714b317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf5ca847-862d-4425-b399-31d23272805c",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "4bcc7960-36a7-4c97-be09-2496d0aedd05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "655e5f22-f560-4263-90c1-f13c2a0c7e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bcc7960-36a7-4c97-be09-2496d0aedd05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae49de9-fa3c-451b-8024-3bddc01aa8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bcc7960-36a7-4c97-be09-2496d0aedd05",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "f27453c5-de58-468d-8ba7-bb71e98e5aa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "464ccdd5-a9f4-4847-ac20-3baad4b42f55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f27453c5-de58-468d-8ba7-bb71e98e5aa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19430e9c-5d32-419b-9642-2fd28fa7a134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f27453c5-de58-468d-8ba7-bb71e98e5aa1",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "b9213123-13ed-404b-93a7-2550227d3ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "6ce3d34f-92d4-4850-8d7f-b86e3911add9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9213123-13ed-404b-93a7-2550227d3ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb04a68c-40cd-4928-8f32-788df2ad8109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9213123-13ed-404b-93a7-2550227d3ee2",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "802b6db3-616f-4bca-ae29-938a9a51c40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1570a2aa-1a15-458a-9819-630f9e7a35d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "802b6db3-616f-4bca-ae29-938a9a51c40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb4584b4-bf35-4571-b229-360042c1ccce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802b6db3-616f-4bca-ae29-938a9a51c40f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "07cbbd1c-26d0-4f74-ada3-656985222d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "94402a12-a260-4c80-9923-4019ec7d657d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07cbbd1c-26d0-4f74-ada3-656985222d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2382803-5a06-46a6-a7e2-cd657fe6626b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cbbd1c-26d0-4f74-ada3-656985222d63",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "ea5e5b7e-527a-4526-888a-75137e89c560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1a175c3a-65ce-4727-9f32-7d65dd59a786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5e5b7e-527a-4526-888a-75137e89c560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7999773e-9801-49bc-91ea-5739398585aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5e5b7e-527a-4526-888a-75137e89c560",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "1cf49ad1-af7d-4ffc-89dd-42111ba53f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "27774c3f-4590-4b10-892e-f8cfacaf565d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf49ad1-af7d-4ffc-89dd-42111ba53f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2682824c-7991-4a19-ace9-b044d0e38e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf49ad1-af7d-4ffc-89dd-42111ba53f8b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e3c51a66-2a52-4449-bf2c-118f52462736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "f4d56527-4d15-4b68-9f03-77eb0eef17f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c51a66-2a52-4449-bf2c-118f52462736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f3d57e2-50d5-4936-8fbc-bc31df861228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c51a66-2a52-4449-bf2c-118f52462736",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "98f71aaa-7410-4695-8ec0-2414cd305702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1e9613d3-e34e-4a03-8339-fde19aafc614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f71aaa-7410-4695-8ec0-2414cd305702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3292d22-f018-473e-9091-f885c50621a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f71aaa-7410-4695-8ec0-2414cd305702",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "5e8d5d1d-09be-4cdd-8c63-978697add381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "3fc12db0-1657-4d6f-96ac-d5ab7c8a356c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e8d5d1d-09be-4cdd-8c63-978697add381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489028af-88dd-4dbf-9778-665130b7c427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e8d5d1d-09be-4cdd-8c63-978697add381",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "28cacbf4-f6b8-4eef-9520-f2adbeffcefb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a64ec107-52a6-434b-a8f7-940b334f063d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28cacbf4-f6b8-4eef-9520-f2adbeffcefb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23baaeef-fa29-4013-a706-7f7d15b53a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28cacbf4-f6b8-4eef-9520-f2adbeffcefb",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "d3a4d52e-7d97-4f92-85a4-304967c5c4ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "56631d22-25a0-48fb-9d93-8a3819cb7eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a4d52e-7d97-4f92-85a4-304967c5c4ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98783d29-9218-4aba-ac5c-34b68f356c0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a4d52e-7d97-4f92-85a4-304967c5c4ea",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e9fb0314-f346-4ba2-ba63-2ea6decebae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "82621c4a-60a7-438a-9572-89d4275db671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fb0314-f346-4ba2-ba63-2ea6decebae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f008e065-0512-4053-b5a1-11d263155f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fb0314-f346-4ba2-ba63-2ea6decebae4",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "cecfc152-035d-4e04-8b8f-14c7eb445a9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "10abeb5a-ae3a-4a9b-be02-f271804f1e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecfc152-035d-4e04-8b8f-14c7eb445a9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e655333b-3f99-47eb-bd87-eb07d6c85064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecfc152-035d-4e04-8b8f-14c7eb445a9c",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "23a918c5-8787-4839-b16f-683e8b1b1cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "5036780d-7231-4f52-8552-3f7e1ab0c139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a918c5-8787-4839-b16f-683e8b1b1cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8b01e93-b107-486f-baf5-d5bdcacad7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a918c5-8787-4839-b16f-683e8b1b1cdb",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "65b46786-8a6b-4650-821a-d7920eb92b70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "06e5fa01-adc9-4e62-9a75-13b1863b9adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b46786-8a6b-4650-821a-d7920eb92b70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32e79595-164a-4d88-984a-5d6530b959b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b46786-8a6b-4650-821a-d7920eb92b70",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "ca3bf633-804b-467f-b8fd-7b3e2fa2e03c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "95a223da-ec3e-480d-be3c-23eaa7dd7308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca3bf633-804b-467f-b8fd-7b3e2fa2e03c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57ed4840-53d5-4e7a-8c0b-0b84c100dee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca3bf633-804b-467f-b8fd-7b3e2fa2e03c",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "20f3c9ca-a05d-438e-9ea9-efe8d57412ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "f28ff184-de4a-4e15-b921-a12578171a9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20f3c9ca-a05d-438e-9ea9-efe8d57412ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494c94ec-2687-4d5e-9959-3b3dac3ae30e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20f3c9ca-a05d-438e-9ea9-efe8d57412ba",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "19c70da9-d803-4de6-96a2-b76dcaea5b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "cd9f198b-a0fa-447e-b62c-d7bdc7665d2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c70da9-d803-4de6-96a2-b76dcaea5b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b54887-b461-410d-b318-1fac86834bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c70da9-d803-4de6-96a2-b76dcaea5b86",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "f4f34c00-5f61-4643-b167-6adc68dea395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "41170374-e243-4351-9cb0-517b88c014e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4f34c00-5f61-4643-b167-6adc68dea395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "039d4ba7-49f9-4467-a24d-e49459ddfc1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4f34c00-5f61-4643-b167-6adc68dea395",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "aaddbac4-cfdf-4590-9776-b82f30d4ac1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "e3760728-dabb-4919-9687-0519f5178784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaddbac4-cfdf-4590-9776-b82f30d4ac1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42c8885-279f-493a-8a62-b6c7b7b02ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaddbac4-cfdf-4590-9776-b82f30d4ac1b",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "15071cda-1679-482d-afc4-76d4def4009f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "f99a945b-10c0-46bf-afba-13ea713a1bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15071cda-1679-482d-afc4-76d4def4009f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef37ea0d-23cf-49a2-9636-b36871ffb3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15071cda-1679-482d-afc4-76d4def4009f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "46f936be-154e-4e4a-b377-8ec9ac2450ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "31130aa7-dd95-4fc2-891a-5214ae1fb48e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46f936be-154e-4e4a-b377-8ec9ac2450ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a139060a-0e6c-4d99-8dd4-34ea9ffae35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46f936be-154e-4e4a-b377-8ec9ac2450ae",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "7a54ceaa-6d9b-4494-9beb-746af853256f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "b9d6fcd9-bbdb-422f-8737-626645237827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a54ceaa-6d9b-4494-9beb-746af853256f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a49874a7-7733-4255-9b49-cb0ab8dab1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a54ceaa-6d9b-4494-9beb-746af853256f",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "e8b9a3bf-8cd2-440f-be98-434c42e0b480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "afd39340-e197-4e6d-a55c-d0dd231fce8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b9a3bf-8cd2-440f-be98-434c42e0b480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a634881-5dcc-4d58-aaf8-633d2571f10d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b9a3bf-8cd2-440f-be98-434c42e0b480",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "f549aa89-2f85-4ac1-90be-8a061912ff70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "1184f425-6ffc-4814-b1e9-31f0139c5c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f549aa89-2f85-4ac1-90be-8a061912ff70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca71e455-d02c-4fec-b4f8-b783190060b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f549aa89-2f85-4ac1-90be-8a061912ff70",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "9741d83e-3950-493e-bff2-7ca57ef544ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "23ad788a-991b-4167-85f4-d898d478f4df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9741d83e-3950-493e-bff2-7ca57ef544ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5051e2a0-69f7-4ae6-94df-4867e80e2d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9741d83e-3950-493e-bff2-7ca57ef544ef",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "b75a7403-9a4c-40b0-b979-32a1744bcf99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "a2d193dd-7055-4ecd-ae87-36ef75388457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75a7403-9a4c-40b0-b979-32a1744bcf99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79b09f08-53ee-4702-91ff-326c4d4ab824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75a7403-9a4c-40b0-b979-32a1744bcf99",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "7f6659a0-0e6c-4d39-ad4b-ce62cec6dec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "c213453a-cf02-4ad8-98f1-7321827adba8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f6659a0-0e6c-4d39-ad4b-ce62cec6dec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd95d3d0-6310-42dc-a08c-8b49f8ccf6a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f6659a0-0e6c-4d39-ad4b-ce62cec6dec7",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "ebd2cf38-0652-43bb-a507-18b87e48f749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "467bf957-f4eb-41a0-a90e-defb13f926b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebd2cf38-0652-43bb-a507-18b87e48f749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83cc41f9-a93f-49c4-a41a-62ff73fc401e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebd2cf38-0652-43bb-a507-18b87e48f749",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "021831db-ab8c-400b-b41f-e2c44d973d20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "d676a296-13aa-47f4-9aed-2b928b48cffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021831db-ab8c-400b-b41f-e2c44d973d20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea8807b1-0304-4982-8791-9b448a3a422f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021831db-ab8c-400b-b41f-e2c44d973d20",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        },
        {
            "id": "241e2328-3015-43aa-8e3b-be4e7373a819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "compositeImage": {
                "id": "87e7197a-8a31-4842-8585-db6b2a0c6e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241e2328-3015-43aa-8e3b-be4e7373a819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3c7629-d266-4ba3-8dda-c0d6593da3a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241e2328-3015-43aa-8e3b-be4e7373a819",
                    "LayerId": "da182ffc-b667-41fb-a2e4-da45b7476a1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "da182ffc-b667-41fb-a2e4-da45b7476a1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d19e5c1-a537-4b23-9a72-19d8b37106d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}