{
    "id": "85eb1c3f-3b04-4060-b88c-0e57f89b4458",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_top_left_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85686624-650d-4a2b-b9bc-a6e18ddf921f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85eb1c3f-3b04-4060-b88c-0e57f89b4458",
            "compositeImage": {
                "id": "f50b0149-5e6b-42de-8726-4c4568d0479a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85686624-650d-4a2b-b9bc-a6e18ddf921f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7022067-1178-416a-8c1d-5a682d0b4422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85686624-650d-4a2b-b9bc-a6e18ddf921f",
                    "LayerId": "042ac05d-69db-4eda-87f2-754e38e2fd51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "042ac05d-69db-4eda-87f2-754e38e2fd51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85eb1c3f-3b04-4060-b88c-0e57f89b4458",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 15,
    "yorig": 15
}