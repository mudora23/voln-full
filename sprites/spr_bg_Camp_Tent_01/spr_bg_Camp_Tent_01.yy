{
    "id": "62507d9b-1939-40fa-8b98-ea87d24022c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Camp_Tent_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13dc0e12-cb93-46ea-9719-9c7c986c2890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62507d9b-1939-40fa-8b98-ea87d24022c4",
            "compositeImage": {
                "id": "0f0d5d36-78f6-454a-880a-bf387fcf4f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13dc0e12-cb93-46ea-9719-9c7c986c2890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75323cae-4127-45da-9cb6-869e3246cd1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13dc0e12-cb93-46ea-9719-9c7c986c2890",
                    "LayerId": "ebf45e54-3de2-41bc-b00a-162d94c47590"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "ebf45e54-3de2-41bc-b00a-162d94c47590",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62507d9b-1939-40fa-8b98-ea87d24022c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}