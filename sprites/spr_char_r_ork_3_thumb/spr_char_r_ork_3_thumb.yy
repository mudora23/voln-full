{
    "id": "4b1c0e68-abad-4e2e-abdc-e7e972af389a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_3_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "687f7aa6-932f-422b-bfc0-3249a3818328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b1c0e68-abad-4e2e-abdc-e7e972af389a",
            "compositeImage": {
                "id": "2e90ad40-2ded-49d8-b2c8-0c72f4650b6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "687f7aa6-932f-422b-bfc0-3249a3818328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ff1701-5190-4e33-a643-63e79ce2d8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "687f7aa6-932f-422b-bfc0-3249a3818328",
                    "LayerId": "48f83907-f96e-4bee-aa25-b7748e067d0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "48f83907-f96e-4bee-aa25-b7748e067d0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b1c0e68-abad-4e2e-abdc-e7e972af389a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}