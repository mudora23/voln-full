{
    "id": "5c4753e3-4d4e-41cb-9b84-d77a0b8e5a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_wreck1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 10,
    "bbox_right": 89,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2af4f4f-f772-4745-8459-c3c8c3cc0c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c4753e3-4d4e-41cb-9b84-d77a0b8e5a5c",
            "compositeImage": {
                "id": "44acd09d-60d8-450d-b5e8-0c7d70697be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2af4f4f-f772-4745-8459-c3c8c3cc0c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90fc92ae-85df-400b-89d5-db3c35201e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2af4f4f-f772-4745-8459-c3c8c3cc0c44",
                    "LayerId": "9e204406-41fd-48a9-8431-30ab7f0c7271"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9e204406-41fd-48a9-8431-30ab7f0c7271",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c4753e3-4d4e-41cb-9b84-d77a0b8e5a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}