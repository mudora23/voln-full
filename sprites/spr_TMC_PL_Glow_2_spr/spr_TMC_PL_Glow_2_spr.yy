{
    "id": "b4811453-32b8-4395-a7d6-073c55ec2ec5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TMC_PL_Glow_2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 3,
    "bbox_right": 92,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43d46bec-686e-4321-a446-b1e4eb893b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4811453-32b8-4395-a7d6-073c55ec2ec5",
            "compositeImage": {
                "id": "0ebce512-3a12-46bc-a1a6-527b3c3bce77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d46bec-686e-4321-a446-b1e4eb893b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "950376e0-2e80-4218-9e44-841aec69cabd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d46bec-686e-4321-a446-b1e4eb893b92",
                    "LayerId": "1b3b9a9b-fdb0-41eb-85cb-4fdba6761c6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1b3b9a9b-fdb0-41eb-85cb-4fdba6761c6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4811453-32b8-4395-a7d6-073c55ec2ec5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}