{
    "id": "f53e212e-abcd-419e-9832-d74b99a5f112",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a44ee856-74d8-4f05-a919-0871cdad0baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f53e212e-abcd-419e-9832-d74b99a5f112",
            "compositeImage": {
                "id": "94b22a56-ced4-49f0-acca-44269725a2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44ee856-74d8-4f05-a919-0871cdad0baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45984042-d8d9-4cb7-b1e9-63a75044e1e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44ee856-74d8-4f05-a919-0871cdad0baf",
                    "LayerId": "734ba894-3bc6-4bfa-b49a-39336cfc8f84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "734ba894-3bc6-4bfa-b49a-39336cfc8f84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f53e212e-abcd-419e-9832-d74b99a5f112",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}