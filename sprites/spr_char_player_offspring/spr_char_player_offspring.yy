{
    "id": "e674c45d-b1d3-4093-ba40-3f23106edab1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_player_offspring",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1804,
    "bbox_left": 0,
    "bbox_right": 866,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8de48abd-7bcf-48da-b61e-9576d249fd94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "64388e89-e9a2-411a-9907-fb2753e810f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de48abd-7bcf-48da-b61e-9576d249fd94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb6a5b3-9ff3-4918-b302-1d305f0a8da0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de48abd-7bcf-48da-b61e-9576d249fd94",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "5f0771f1-7e68-49c3-bbcf-e6a8d0a9b6e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "15c2cac8-b9fe-43b8-9d4f-0cf83388e526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f0771f1-7e68-49c3-bbcf-e6a8d0a9b6e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aba7723-2c7a-4a9f-b11e-9cfab864f46d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f0771f1-7e68-49c3-bbcf-e6a8d0a9b6e1",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "b43d3841-b630-4180-8ccb-316e59864919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "30d7e0bf-3f19-4677-98d9-3af7deb9d19b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43d3841-b630-4180-8ccb-316e59864919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57efd7e2-8fd0-4ad5-9471-57b9ec903e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43d3841-b630-4180-8ccb-316e59864919",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "0f667285-5cbb-4d7a-97bf-6872777e746f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "9c614f13-b35e-4610-814d-a501d0aa5de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f667285-5cbb-4d7a-97bf-6872777e746f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa6c66a-5a64-4dc7-b571-6e54cce8a3d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f667285-5cbb-4d7a-97bf-6872777e746f",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "d56462de-cebc-47ad-90c7-704e7e8564da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "7d7a453b-7baf-4b4b-85f1-cc0c1579626a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56462de-cebc-47ad-90c7-704e7e8564da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a517b665-f9cb-469b-9bc1-4c5b45bbcb99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56462de-cebc-47ad-90c7-704e7e8564da",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "1c51eebb-58f5-4177-9401-e1f577ecfa5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "e29800c4-66a9-43ec-a06a-2104f95815c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c51eebb-58f5-4177-9401-e1f577ecfa5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14394174-238d-4f40-8786-f27888dbb149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c51eebb-58f5-4177-9401-e1f577ecfa5c",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "b817bb17-4125-4ebc-87f3-0707036a1475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "3e653c43-db00-45af-aa97-7c1356d0abb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b817bb17-4125-4ebc-87f3-0707036a1475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18bcdd12-a75d-47ce-8e7b-fe10d70833d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b817bb17-4125-4ebc-87f3-0707036a1475",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "240a44f2-ffa4-40fb-888d-0be7e8b55fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "756c1a63-e19b-4544-8c18-8e24f40822bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240a44f2-ffa4-40fb-888d-0be7e8b55fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22249ee-eeaf-4d16-87f2-0c01d469122c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240a44f2-ffa4-40fb-888d-0be7e8b55fe6",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "fec85ed2-f5f4-45f7-a2ae-7f41d9c696ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "ec78a41f-35bf-42ba-9d36-29611cf6cfa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fec85ed2-f5f4-45f7-a2ae-7f41d9c696ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e170b9b5-6597-4bb1-bae7-e1d74ef0f17e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fec85ed2-f5f4-45f7-a2ae-7f41d9c696ce",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "8ee95e32-271c-4216-8070-bf76b87fb396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "60f4462f-507a-48b7-9605-f6c8be8296b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ee95e32-271c-4216-8070-bf76b87fb396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9df6b64b-5306-4578-b4f6-936f51401539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ee95e32-271c-4216-8070-bf76b87fb396",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "9918ab88-ad5d-4345-917c-b1e0d799fcf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "62f5f8f4-76ec-4b4e-a8f6-0671360bff23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9918ab88-ad5d-4345-917c-b1e0d799fcf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8caad6a1-97c1-4456-87fc-fc36d942ad0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9918ab88-ad5d-4345-917c-b1e0d799fcf6",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "4765fc23-3704-43f0-b5b5-acd542274203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "578c4669-d501-4237-b633-3a08fb03aa12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4765fc23-3704-43f0-b5b5-acd542274203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23a2465-aaf0-4d93-a06f-6c5a62a3a5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4765fc23-3704-43f0-b5b5-acd542274203",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "cfa00e13-ca5f-4729-a528-88e5e747156d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "fde4154f-7189-43d7-93c7-da12b39b902a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfa00e13-ca5f-4729-a528-88e5e747156d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9299ab7-afec-4f26-914d-080d4b91e192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfa00e13-ca5f-4729-a528-88e5e747156d",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "e5598491-0df1-45af-8d6f-acc59551521d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "f04c123a-bed4-43c7-aa22-6d9cc65c24ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5598491-0df1-45af-8d6f-acc59551521d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cfaef59-3ad3-4d05-a76c-0d8aded711cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5598491-0df1-45af-8d6f-acc59551521d",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "63127101-64d6-4021-876c-ff453bca2eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "594b7320-4f16-4dea-a97e-fc5e3253f46e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63127101-64d6-4021-876c-ff453bca2eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a983a98-c48b-4515-a757-e9fbe131712b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63127101-64d6-4021-876c-ff453bca2eea",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "dbcd3cb8-0876-4c74-90b1-5102b52f8698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "9f7b0d41-99f8-4690-b470-69ffbd7caa23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbcd3cb8-0876-4c74-90b1-5102b52f8698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cdf28c1-8baf-4f45-870c-130bdbd09335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbcd3cb8-0876-4c74-90b1-5102b52f8698",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "884ae7d9-1f47-41c9-ae47-adec27b31e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "9d5c919b-2385-479c-af2a-27b51d608a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "884ae7d9-1f47-41c9-ae47-adec27b31e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6e332ea-681a-4bbc-8d2c-9e685a9706fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "884ae7d9-1f47-41c9-ae47-adec27b31e75",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "e6f6d714-ca67-4831-acf0-28f23bfaaa86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "24ee178b-da94-4a42-953c-8ac17b0cebe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f6d714-ca67-4831-acf0-28f23bfaaa86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c4a029-df22-48f2-a9c8-f2447f8ab0e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f6d714-ca67-4831-acf0-28f23bfaaa86",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "5e5aed39-3c85-41fd-b26a-5547d18cd07f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "55f42935-4106-40e1-af98-cd47bb676a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5aed39-3c85-41fd-b26a-5547d18cd07f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff2360a-5c27-451f-aea1-f53e0cdeb3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5aed39-3c85-41fd-b26a-5547d18cd07f",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "b5ac8dd4-5499-4789-8ac3-ad817fb065e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "facbd203-8d1e-4e84-8412-8ff3e538d65a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5ac8dd4-5499-4789-8ac3-ad817fb065e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d1fabaa-bcd9-4786-83b1-d24f72af2d0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5ac8dd4-5499-4789-8ac3-ad817fb065e0",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "2c5a06e3-60ae-4139-9b28-88bd034aa674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "8c5825ad-15e7-442e-9a30-32c94fb9fd86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c5a06e3-60ae-4139-9b28-88bd034aa674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fec3d08b-d681-45ae-a5b2-6a6de81c5700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c5a06e3-60ae-4139-9b28-88bd034aa674",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "77033cc2-7643-4c10-b9a6-884d72ea44ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "efd748e1-363b-481b-9919-9856dc786d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77033cc2-7643-4c10-b9a6-884d72ea44ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75faffb7-c495-4249-a9a4-4e0f6c02897a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77033cc2-7643-4c10-b9a6-884d72ea44ef",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "8d649ef7-c342-4d96-9cc4-47002707d1f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "19cd3846-c54d-4d46-8ff4-b09325e3a50f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d649ef7-c342-4d96-9cc4-47002707d1f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610926a2-702c-44a1-91e8-3cd4c55b15b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d649ef7-c342-4d96-9cc4-47002707d1f7",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "8edab6e6-a24f-4c4d-aa67-2384022ccb18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "8fe9068a-4cc2-4d67-828e-1fac44195cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8edab6e6-a24f-4c4d-aa67-2384022ccb18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef09e2a6-c4e1-492f-aba7-d5048e45afb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8edab6e6-a24f-4c4d-aa67-2384022ccb18",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "48ac1100-a155-4bac-9cd6-f515141c82c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "9bf7bf4e-58db-4750-b94d-1f5839b72e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ac1100-a155-4bac-9cd6-f515141c82c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae89ddb6-b6b1-479c-81d5-ce8e15abaeed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ac1100-a155-4bac-9cd6-f515141c82c2",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "2ec4abc0-5e9e-4f81-b9cb-9d360bf5af5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "ea489530-f1fb-45f7-afbd-491af23f666a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec4abc0-5e9e-4f81-b9cb-9d360bf5af5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098b73c9-124b-46d4-820a-7f98cf2096c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec4abc0-5e9e-4f81-b9cb-9d360bf5af5b",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        },
        {
            "id": "b0d1f597-2887-456d-bb64-a5da22188982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "compositeImage": {
                "id": "677d9f82-81fa-4c79-9bcc-7075bf3e0110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d1f597-2887-456d-bb64-a5da22188982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42aefd71-15e6-40ee-8354-cfa71a5fc549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d1f597-2887-456d-bb64-a5da22188982",
                    "LayerId": "59e876ed-c381-4d74-92dc-629d5098da74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "59e876ed-c381-4d74-92dc-629d5098da74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e674c45d-b1d3-4093-ba40-3f23106edab1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}