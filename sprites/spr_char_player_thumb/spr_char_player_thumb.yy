{
    "id": "09797754-af2e-4108-b846-2adf4b99ae92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_player_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d99367cf-627b-4ed2-a340-2f5cc962f7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09797754-af2e-4108-b846-2adf4b99ae92",
            "compositeImage": {
                "id": "face922e-98dc-4309-903b-9e521c3c5f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99367cf-627b-4ed2-a340-2f5cc962f7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c4b86e-560d-4433-810e-bd063e455db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99367cf-627b-4ed2-a340-2f5cc962f7ff",
                    "LayerId": "f78d2dcf-f137-4d4c-b8d8-1489f5367fd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "f78d2dcf-f137-4d4c-b8d8-1489f5367fd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09797754-af2e-4108-b846-2adf4b99ae92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}