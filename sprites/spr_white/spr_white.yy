{
    "id": "1134e792-5e26-475d-8454-af34be029f02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccebc2f8-8db5-42bf-a294-310a7531d2b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1134e792-5e26-475d-8454-af34be029f02",
            "compositeImage": {
                "id": "46e77370-3103-4df6-86e2-4e3dfad2c2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccebc2f8-8db5-42bf-a294-310a7531d2b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07284135-1773-4eca-b2fb-ed87be3ab7cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccebc2f8-8db5-42bf-a294-310a7531d2b8",
                    "LayerId": "8eb43731-6758-4a4f-b3ef-9201b06cbdbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8eb43731-6758-4a4f-b3ef-9201b06cbdbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1134e792-5e26-475d-8454-af34be029f02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}