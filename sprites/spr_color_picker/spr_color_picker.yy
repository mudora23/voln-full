{
    "id": "ec2b22e9-ad53-40df-bf1f-89eadba465c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_color_picker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e1d5ba4-9822-401e-ab6c-c1819d5fd3da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec2b22e9-ad53-40df-bf1f-89eadba465c2",
            "compositeImage": {
                "id": "d88dba25-cffd-463c-a359-eef422a11da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e1d5ba4-9822-401e-ab6c-c1819d5fd3da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4996332b-9ffe-450d-a9b5-b919089c3e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e1d5ba4-9822-401e-ab6c-c1819d5fd3da",
                    "LayerId": "197fad0a-a851-48b0-a610-b985788a1908"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "197fad0a-a851-48b0-a610-b985788a1908",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec2b22e9-ad53-40df-bf1f-89eadba465c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}