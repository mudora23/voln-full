{
    "id": "88fe4623-dfa0-4635-bec7-a2a974f4a11b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_poison",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95b64a52-b898-451b-9c7f-d237ee3885aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88fe4623-dfa0-4635-bec7-a2a974f4a11b",
            "compositeImage": {
                "id": "c57a0c8b-b1d6-402c-b908-a38f193b3564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b64a52-b898-451b-9c7f-d237ee3885aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6363aa9-af4d-4750-81e4-bf1b465aa70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b64a52-b898-451b-9c7f-d237ee3885aa",
                    "LayerId": "d421da2d-b70c-43ab-b5ec-2a5a51ca1f5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "d421da2d-b70c-43ab-b5ec-2a5a51ca1f5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88fe4623-dfa0-4635-bec7-a2a974f4a11b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}