{
    "id": "e925f2ac-8333-4924-a566-8018a76eaea0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_right_seemless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02d6bf40-943b-476c-b3ad-0b19ba583ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e925f2ac-8333-4924-a566-8018a76eaea0",
            "compositeImage": {
                "id": "6049ba38-0aae-4001-883d-47134dda286c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d6bf40-943b-476c-b3ad-0b19ba583ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac9aee4b-4d59-4756-948c-974e339f4d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d6bf40-943b-476c-b3ad-0b19ba583ff1",
                    "LayerId": "b2741726-07a7-44a8-95b9-86ecdcb547ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "b2741726-07a7-44a8-95b9-86ecdcb547ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e925f2ac-8333-4924-a566-8018a76eaea0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 15,
    "yorig": 18
}