{
    "id": "0ff7c3b1-b398-4483-9c2f-c3d1474ccdcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Bal_r_ss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2210,
    "bbox_left": 80,
    "bbox_right": 1031,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e99969d7-914c-464d-a97c-58facbeb1a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ff7c3b1-b398-4483-9c2f-c3d1474ccdcc",
            "compositeImage": {
                "id": "6939b273-cf14-4e64-b64a-afbcac59598b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99969d7-914c-464d-a97c-58facbeb1a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3ea2d68-0f8e-46f8-8dd3-349a7566f78b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99969d7-914c-464d-a97c-58facbeb1a7a",
                    "LayerId": "46704d38-0a20-4cba-8f2d-99efdb4a6609"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2228,
    "layers": [
        {
            "id": "46704d38-0a20-4cba-8f2d-99efdb4a6609",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ff7c3b1-b398-4483-9c2f-c3d1474ccdcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1180,
    "xorig": 0,
    "yorig": 0
}