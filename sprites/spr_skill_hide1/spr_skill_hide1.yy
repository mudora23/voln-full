{
    "id": "6336d5bd-a56d-4932-9804-3382aa83912f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_hide1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 24,
    "bbox_right": 76,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17f79698-8e5c-4ba2-91c2-6b734e76f836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6336d5bd-a56d-4932-9804-3382aa83912f",
            "compositeImage": {
                "id": "9c881410-6c57-4971-94af-7c0d615eead2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f79698-8e5c-4ba2-91c2-6b734e76f836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c48fcf-fea1-4724-ba81-f0712aab7734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f79698-8e5c-4ba2-91c2-6b734e76f836",
                    "LayerId": "c0aba842-5874-4a37-abb0-3c7faf274104"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "c0aba842-5874-4a37-abb0-3c7faf274104",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6336d5bd-a56d-4932-9804-3382aa83912f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}