{
    "id": "781316d1-c663-4124-acdf-bec634b0dee8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cg_S_0007",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2843,
    "bbox_left": 23,
    "bbox_right": 2269,
    "bbox_top": 124,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21ad694e-cbde-4333-9e44-99f85396826e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "781316d1-c663-4124-acdf-bec634b0dee8",
            "compositeImage": {
                "id": "c2611cf7-2735-44cf-89fe-f55cd774fa18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ad694e-cbde-4333-9e44-99f85396826e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f5ad39-46bc-4de6-945e-edad71bda25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ad694e-cbde-4333-9e44-99f85396826e",
                    "LayerId": "191f3492-ad11-4d8c-a83a-99d635a38e01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2844,
    "layers": [
        {
            "id": "191f3492-ad11-4d8c-a83a-99d635a38e01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "781316d1-c663-4124-acdf-bec634b0dee8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2270,
    "xorig": 0,
    "yorig": 0
}