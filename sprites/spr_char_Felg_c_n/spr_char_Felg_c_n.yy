{
    "id": "2a5366bc-e782-4975-8055-3268c8333311",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Felg_c_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1959,
    "bbox_left": 126,
    "bbox_right": 958,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ee687a8-c096-4023-9d6f-96f79b37236f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5366bc-e782-4975-8055-3268c8333311",
            "compositeImage": {
                "id": "6fad0a4b-9031-4c43-992d-51524ec9576a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee687a8-c096-4023-9d6f-96f79b37236f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f5e34d4-2d31-45a8-a474-3b6e4a4e41b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee687a8-c096-4023-9d6f-96f79b37236f",
                    "LayerId": "0396e4bc-1579-439d-b3a5-144327c3caf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "0396e4bc-1579-439d-b3a5-144327c3caf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a5366bc-e782-4975-8055-3268c8333311",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}