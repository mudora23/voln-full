{
    "id": "c36027d9-be9c-46a2-bdf8-0c74e8004c5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1657e68d-e396-418b-969d-6fd83c9c4108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c36027d9-be9c-46a2-bdf8-0c74e8004c5c",
            "compositeImage": {
                "id": "0cdc15d5-97f7-48fb-8a80-d8ec78d75e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1657e68d-e396-418b-969d-6fd83c9c4108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb59aa5-70d0-4a83-ad33-79d1cc544bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1657e68d-e396-418b-969d-6fd83c9c4108",
                    "LayerId": "8029d492-8b2c-4de0-bacd-8c858c95e56b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "8029d492-8b2c-4de0-bacd-8c858c95e56b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c36027d9-be9c-46a2-bdf8-0c74e8004c5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 70,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}