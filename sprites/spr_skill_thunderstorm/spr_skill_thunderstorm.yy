{
    "id": "f8a96a08-67cf-4f90-ba6b-c5e1cdde6501",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_thunderstorm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 189,
    "bbox_left": 11,
    "bbox_right": 189,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4ad22a1-8d11-41ed-91b8-f8cc6368696a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8a96a08-67cf-4f90-ba6b-c5e1cdde6501",
            "compositeImage": {
                "id": "8db63b74-9576-4e2d-80aa-eaf39a470303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ad22a1-8d11-41ed-91b8-f8cc6368696a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c2bb6cc-86ff-40bb-a261-e2b9f693948e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ad22a1-8d11-41ed-91b8-f8cc6368696a",
                    "LayerId": "1348bebd-cb6a-4e78-b602-a56bd045e1b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "1348bebd-cb6a-4e78-b602-a56bd045e1b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8a96a08-67cf-4f90-ba6b-c5e1cdde6501",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}