{
    "id": "61b95ca0-8572-415a-a35d-a3379c34de37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_player_offspring_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c64ed784-612b-47db-ad5b-3efc265e0168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "0adc2be2-d03d-46a9-8af7-288c3f541fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64ed784-612b-47db-ad5b-3efc265e0168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84476d8-3962-492a-b7d0-99c96c8addde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64ed784-612b-47db-ad5b-3efc265e0168",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "3ae30630-fafe-44a0-898b-b102cdf0eed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "b8f70b21-2bc2-49e0-846e-b8026081c6ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ae30630-fafe-44a0-898b-b102cdf0eed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69197a03-6f7d-43ed-8afe-7b52c2580f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ae30630-fafe-44a0-898b-b102cdf0eed0",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "c8256806-8609-468a-b576-2a2cf2c00503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "e17325e3-ae4f-47fa-afd8-c10deea4b088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8256806-8609-468a-b576-2a2cf2c00503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c44786-e25c-484b-b046-58c76bc703bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8256806-8609-468a-b576-2a2cf2c00503",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "fe645140-59d8-4e3d-a6ce-0e1d07ffc7b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "3a52be9b-4a42-4344-87e3-efa39f89de82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe645140-59d8-4e3d-a6ce-0e1d07ffc7b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c7e28d7-211e-401f-8986-6b6d5477e6ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe645140-59d8-4e3d-a6ce-0e1d07ffc7b1",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "87f979a2-14fd-41a4-becd-3308963111a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "ecbaf170-5b1d-4653-bd8a-fa1778f56f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87f979a2-14fd-41a4-becd-3308963111a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257f153e-28a3-4e51-a941-28db2162d1d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87f979a2-14fd-41a4-becd-3308963111a7",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "0c75b35f-60eb-40c8-a09e-958750d24e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "25fd7861-2f93-4798-95c2-2649fad5aecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c75b35f-60eb-40c8-a09e-958750d24e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74d4f87-a245-404a-9c12-8b1593ea84eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c75b35f-60eb-40c8-a09e-958750d24e59",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "a1139026-cf74-4066-b6f8-3e5847b420e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "cf887461-a118-4bf0-b59c-d4e01b88a387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1139026-cf74-4066-b6f8-3e5847b420e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c86855-c419-4d6a-b50a-12803ff4b76b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1139026-cf74-4066-b6f8-3e5847b420e9",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "ef4a837d-643d-49ea-a73b-5449691f5fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "022b491e-9aab-40bd-9e02-497b8ba10b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4a837d-643d-49ea-a73b-5449691f5fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f428688-c014-4031-8259-9bbbc632b31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4a837d-643d-49ea-a73b-5449691f5fbc",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "5817791c-01ae-4e80-b6f0-0cfb3e60198e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "463be241-cc98-4a11-a29e-a3d34f8818dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5817791c-01ae-4e80-b6f0-0cfb3e60198e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c6427b8-943e-45a7-a917-12afcfa3e71c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5817791c-01ae-4e80-b6f0-0cfb3e60198e",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "d45d35ce-991d-45e6-8f1e-891090d5e067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "4b49dcd2-e156-4868-be59-cf9bef7a4ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d45d35ce-991d-45e6-8f1e-891090d5e067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "543b881c-4286-4abd-b465-965d3f8a3a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45d35ce-991d-45e6-8f1e-891090d5e067",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "65ec6126-d6af-4975-9126-dad84ccd5c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "57efc627-140e-40f0-8f33-ca1cf76e908b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ec6126-d6af-4975-9126-dad84ccd5c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3fca44-847e-4c8f-82d1-3bb88e518b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ec6126-d6af-4975-9126-dad84ccd5c82",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "12ea4ffd-0af5-450a-ab85-eba181739c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "1342dce6-278c-4ef9-b7f8-fa1926f0f685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ea4ffd-0af5-450a-ab85-eba181739c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af551363-06f0-42d7-b8ea-bf173add3260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ea4ffd-0af5-450a-ab85-eba181739c27",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "a1115143-19d9-4999-af1e-9a46cb4d3daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "a9e17e9e-cb08-4a91-8f10-95778935bd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1115143-19d9-4999-af1e-9a46cb4d3daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc75a9da-7569-4d4a-b80d-5a92323e6403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1115143-19d9-4999-af1e-9a46cb4d3daf",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "53a28f85-f6e2-46be-badb-bce7cd095071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "10b1e2a7-a9d8-4b84-b8f3-f6d1f073cc3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a28f85-f6e2-46be-badb-bce7cd095071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d396e9-9206-4791-9b57-a6be98af9a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a28f85-f6e2-46be-badb-bce7cd095071",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "4fa719ca-e274-44c6-a2be-a85672404e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "2f94e271-ba58-434b-a2ce-7f5137f5ee06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa719ca-e274-44c6-a2be-a85672404e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd87a91f-100f-435c-a01f-fadba2e77a81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa719ca-e274-44c6-a2be-a85672404e76",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "e72b85af-89db-4cb7-b5e6-34da0947c81d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "ef057e6a-1ede-4816-900a-9dad34d1bc7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72b85af-89db-4cb7-b5e6-34da0947c81d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0b2a428-6c54-4685-be54-08d7b9f81296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72b85af-89db-4cb7-b5e6-34da0947c81d",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "37b01fcb-47b4-4f2a-94ce-7d94eae3cf8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "387f9afd-a7a3-4975-a1bb-117e50e68afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b01fcb-47b4-4f2a-94ce-7d94eae3cf8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6e5b97b-611a-49b3-a12f-5b44d2368d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b01fcb-47b4-4f2a-94ce-7d94eae3cf8a",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "c74bd708-95c2-4036-ab82-a63ebbaac344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "816b3443-237f-4f8a-bf69-8454c5dea351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74bd708-95c2-4036-ab82-a63ebbaac344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d85ff3ff-51e3-4a17-9dde-2d3658a18bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74bd708-95c2-4036-ab82-a63ebbaac344",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "e513c4b6-ab6b-4782-93c5-b60dd899bf82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "6f0365ae-0af4-4327-810d-d5741c08b977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e513c4b6-ab6b-4782-93c5-b60dd899bf82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd4b0a58-62db-446f-b725-9f6a9ff52d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e513c4b6-ab6b-4782-93c5-b60dd899bf82",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "5ea6ec61-9fe6-4b51-bd41-2a37572d711a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "30026c93-c7e8-4abe-a4a3-7eba6ff66778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ea6ec61-9fe6-4b51-bd41-2a37572d711a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99517250-8e83-4c07-b2e4-ed06c7808d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ea6ec61-9fe6-4b51-bd41-2a37572d711a",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "25cd6d95-9d12-4d62-95b1-685c2dc9e813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "40a2f151-54dc-4065-bdce-cd12e0e6873c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25cd6d95-9d12-4d62-95b1-685c2dc9e813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa51afb-8012-4247-b12f-8ae64a7e1d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25cd6d95-9d12-4d62-95b1-685c2dc9e813",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "74eb350b-d7ba-455d-94d6-56b1dbca3dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "bca341ac-c6df-4459-9437-170918f31090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74eb350b-d7ba-455d-94d6-56b1dbca3dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24b3b5c-014d-4c1c-83c8-c676487750b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74eb350b-d7ba-455d-94d6-56b1dbca3dd6",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "1de87c6f-1be9-4fd0-a34b-2a8965c88413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "1dc5c7c2-0414-43d1-9472-b74669d239b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de87c6f-1be9-4fd0-a34b-2a8965c88413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "062752f3-ee28-47dc-8f82-336f26388edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de87c6f-1be9-4fd0-a34b-2a8965c88413",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "8f32d330-310f-4179-932d-1f93ccd5085e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "249d148f-38a0-4138-a064-cc7f2ed1bbab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f32d330-310f-4179-932d-1f93ccd5085e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f842ab6-c4e5-4ab3-8ae4-4f2236b2b157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f32d330-310f-4179-932d-1f93ccd5085e",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "c386edcc-7309-4a34-a881-44e47f996c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "a3d7dc2c-f3f2-476a-9a49-5dee2476a41d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c386edcc-7309-4a34-a881-44e47f996c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb0eb44-ce0d-40c0-92e7-f922a8522109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c386edcc-7309-4a34-a881-44e47f996c1e",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "029e29f2-c44f-4737-b188-e7d6320a6201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "5b958535-d637-4e5f-90e3-8ccc67d0b55c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029e29f2-c44f-4737-b188-e7d6320a6201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc7cd15c-747d-4a7d-9424-7b6eed79433c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029e29f2-c44f-4737-b188-e7d6320a6201",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        },
        {
            "id": "34da5f10-15d0-4d73-af72-d670e954856e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "compositeImage": {
                "id": "3fded9cc-e2b0-430f-8f65-31010f1cc1b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34da5f10-15d0-4d73-af72-d670e954856e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a941d0ea-742d-4829-90a9-f706e5fef449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34da5f10-15d0-4d73-af72-d670e954856e",
                    "LayerId": "eeee236f-0ef0-4249-b828-16534763437e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "eeee236f-0ef0-4249-b828-16534763437e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61b95ca0-8572-415a-a35d-a3379c34de37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}