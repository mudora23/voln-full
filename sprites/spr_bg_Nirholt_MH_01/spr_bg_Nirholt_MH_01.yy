{
    "id": "756bc1cf-41ee-4dec-a026-468cb94bb912",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_MH_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59852c51-f6ac-42ab-9668-1c95f24bc124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756bc1cf-41ee-4dec-a026-468cb94bb912",
            "compositeImage": {
                "id": "9f19386d-45cf-4f75-9cfb-ac9e7176ab04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59852c51-f6ac-42ab-9668-1c95f24bc124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a627fdc5-4e84-4a70-a3c6-f1f1a179c143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59852c51-f6ac-42ab-9668-1c95f24bc124",
                    "LayerId": "9650ec63-4b5a-4040-98b2-6c78448f84d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "9650ec63-4b5a-4040-98b2-6c78448f84d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "756bc1cf-41ee-4dec-a026-468cb94bb912",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}