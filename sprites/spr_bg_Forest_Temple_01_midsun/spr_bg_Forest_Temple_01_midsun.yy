{
    "id": "e8cdb747-b457-4272-9931-ca0054309aaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Temple_01_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96457fa0-f199-4781-b1f3-43480f06df27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8cdb747-b457-4272-9931-ca0054309aaa",
            "compositeImage": {
                "id": "ed2644f0-fb77-43db-bf20-36f756b2c41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96457fa0-f199-4781-b1f3-43480f06df27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddac99a-8827-4bfd-8b00-049e8700f12f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96457fa0-f199-4781-b1f3-43480f06df27",
                    "LayerId": "66ad35e2-811e-4fff-83f9-24d341b2f5c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "66ad35e2-811e-4fff-83f9-24d341b2f5c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8cdb747-b457-4272-9931-ca0054309aaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}