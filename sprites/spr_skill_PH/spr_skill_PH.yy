{
    "id": "129df5d6-2535-4050-a0a2-83cf41b21009",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_PH",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d0c0245-723f-45f2-b706-4b80eb6e1393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "129df5d6-2535-4050-a0a2-83cf41b21009",
            "compositeImage": {
                "id": "c3a8c46c-7efe-40b8-99c6-82864d65580e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0c0245-723f-45f2-b706-4b80eb6e1393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da7b646-ffc2-4158-aaa0-96c6ce5f9214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0c0245-723f-45f2-b706-4b80eb6e1393",
                    "LayerId": "47030034-ce70-41b1-9034-85a9bbf9a24a"
                },
                {
                    "id": "31f0a0a5-cd85-411c-86ab-f451c262f356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0c0245-723f-45f2-b706-4b80eb6e1393",
                    "LayerId": "578c10cf-95c3-470d-b5b0-db6ad4c3eb5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "47030034-ce70-41b1-9034-85a9bbf9a24a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "129df5d6-2535-4050-a0a2-83cf41b21009",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "578c10cf-95c3-470d-b5b0-db6ad4c3eb5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "129df5d6-2535-4050-a0a2-83cf41b21009",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}