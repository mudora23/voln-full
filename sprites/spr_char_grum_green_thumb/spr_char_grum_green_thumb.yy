{
    "id": "6ff89361-97fb-4852-852a-42cd84e3a2c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_green_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f8aa570-a34a-4b0a-9e88-efdc31ddce2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff89361-97fb-4852-852a-42cd84e3a2c8",
            "compositeImage": {
                "id": "599f9fda-cdbb-458f-baea-e53f08fc1c2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8aa570-a34a-4b0a-9e88-efdc31ddce2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a23ebc07-9cca-4845-9319-a488d0637db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8aa570-a34a-4b0a-9e88-efdc31ddce2d",
                    "LayerId": "1b6e3489-f3df-4909-b5ca-5676633c144e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "1b6e3489-f3df-4909-b5ca-5676633c144e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ff89361-97fb-4852-852a-42cd84e3a2c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}