{
    "id": "5782379b-9f1f-4d8e-8d3b-0cfc10d427a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_iceblue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50fcf628-0cc2-4281-bd1c-bc419dc75905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5782379b-9f1f-4d8e-8d3b-0cfc10d427a2",
            "compositeImage": {
                "id": "32eb5dfd-8fa7-4ebc-9f3f-3bef02937d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50fcf628-0cc2-4281-bd1c-bc419dc75905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b86d6ba-2c6f-46b0-a5bf-ab043df70bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50fcf628-0cc2-4281-bd1c-bc419dc75905",
                    "LayerId": "0042ce53-eac9-450b-bfa7-4371d08b3f95"
                }
            ]
        },
        {
            "id": "c7dc00e4-5725-4aa0-abc5-844b32800331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5782379b-9f1f-4d8e-8d3b-0cfc10d427a2",
            "compositeImage": {
                "id": "fb79588c-a869-43f0-9b0a-74530ad73211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7dc00e4-5725-4aa0-abc5-844b32800331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa23c0c3-6c21-422d-bcfa-2265928a6759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7dc00e4-5725-4aa0-abc5-844b32800331",
                    "LayerId": "0042ce53-eac9-450b-bfa7-4371d08b3f95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "0042ce53-eac9-450b-bfa7-4371d08b3f95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5782379b-9f1f-4d8e-8d3b-0cfc10d427a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}