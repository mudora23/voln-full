{
    "id": "bf3e4a77-6026-4918-a7ff-45a076d11b38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_coin_still_full",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 37,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d27c8de-b5f3-4d33-bace-dc1795d4a90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf3e4a77-6026-4918-a7ff-45a076d11b38",
            "compositeImage": {
                "id": "1a5f27ba-baa9-4897-a8d8-1acfbd21e022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d27c8de-b5f3-4d33-bace-dc1795d4a90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4c7da7d-3865-41fb-bb1d-186e50c66868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d27c8de-b5f3-4d33-bace-dc1795d4a90c",
                    "LayerId": "d773db6a-e06a-4aaa-a825-e4718f49cfdb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "d773db6a-e06a-4aaa-a825-e4718f49cfdb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf3e4a77-6026-4918-a7ff-45a076d11b38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 19
}