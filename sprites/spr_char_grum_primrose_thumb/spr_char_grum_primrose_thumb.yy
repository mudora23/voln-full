{
    "id": "aa96305f-9231-49ec-aef2-319a681afe82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_primrose_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e2ee409-8c0b-4415-a4d3-20068f677662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa96305f-9231-49ec-aef2-319a681afe82",
            "compositeImage": {
                "id": "d471ebd9-bf44-4742-bde8-528e1c386594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2ee409-8c0b-4415-a4d3-20068f677662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c401747f-5851-40fe-9d07-5f8e3fbf8d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2ee409-8c0b-4415-a4d3-20068f677662",
                    "LayerId": "ea3f7c1a-598e-45db-9bdd-fe149a8979b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "ea3f7c1a-598e-45db-9bdd-fe149a8979b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa96305f-9231-49ec-aef2-319a681afe82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}