{
    "id": "5a2ff4ee-e645-46c1-a5a4-0e77500b7aaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Lorn_s_n_sm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1999,
    "bbox_left": 194,
    "bbox_right": 1010,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6067534-4194-4cf1-8828-2f97ff36e8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2ff4ee-e645-46c1-a5a4-0e77500b7aaa",
            "compositeImage": {
                "id": "8edd351d-45b3-4934-be66-c65c27a48a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6067534-4194-4cf1-8828-2f97ff36e8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e36408-9b43-4993-8c1e-eaa9ba2f8cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6067534-4194-4cf1-8828-2f97ff36e8f0",
                    "LayerId": "f257401b-622d-4a4a-8dd3-c8c205052f6b"
                }
            ]
        },
        {
            "id": "e81fb59e-f283-41cb-bb41-3d202d3764cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2ff4ee-e645-46c1-a5a4-0e77500b7aaa",
            "compositeImage": {
                "id": "ae9177e7-c8d4-413a-bad1-f35b89825d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81fb59e-f283-41cb-bb41-3d202d3764cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d23cb1-735b-4348-9197-13d77292b8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81fb59e-f283-41cb-bb41-3d202d3764cd",
                    "LayerId": "f257401b-622d-4a4a-8dd3-c8c205052f6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "f257401b-622d-4a4a-8dd3-c8c205052f6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a2ff4ee-e645-46c1-a5a4-0e77500b7aaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}