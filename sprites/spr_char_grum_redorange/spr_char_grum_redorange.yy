{
    "id": "4ee66b83-5040-4efc-aa08-be7559d35df5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_redorange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "158884de-1a7b-46d7-9936-a78781ca0935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ee66b83-5040-4efc-aa08-be7559d35df5",
            "compositeImage": {
                "id": "d0865206-37e8-412f-a25c-eb9b831023f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158884de-1a7b-46d7-9936-a78781ca0935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bd812b4-842c-485e-a2aa-d2ffd021e09c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158884de-1a7b-46d7-9936-a78781ca0935",
                    "LayerId": "f051560a-c106-43bb-b9bf-7cdef0e6e3ec"
                }
            ]
        },
        {
            "id": "05e7a728-b47d-4671-ae20-4a61c39acc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ee66b83-5040-4efc-aa08-be7559d35df5",
            "compositeImage": {
                "id": "6ce1f55f-16fe-465b-b44a-e726b8c1218f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e7a728-b47d-4671-ae20-4a61c39acc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef4918cc-22f4-4dc5-bae0-76e64ddc453c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e7a728-b47d-4671-ae20-4a61c39acc5b",
                    "LayerId": "f051560a-c106-43bb-b9bf-7cdef0e6e3ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "f051560a-c106-43bb-b9bf-7cdef0e6e3ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ee66b83-5040-4efc-aa08-be7559d35df5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}