{
    "id": "aad8a176-db21-4dbd-8a2f-4fdaf146d631",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_anon_male_b_g",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1779,
    "bbox_left": 147,
    "bbox_right": 1070,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9043e501-dcf9-4a77-8672-f97346c3c02d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aad8a176-db21-4dbd-8a2f-4fdaf146d631",
            "compositeImage": {
                "id": "fd6aec76-2952-4ab9-b1d4-930924364f50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9043e501-dcf9-4a77-8672-f97346c3c02d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb97f675-3217-446c-8047-793c30450b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9043e501-dcf9-4a77-8672-f97346c3c02d",
                    "LayerId": "0c82ab98-f581-4387-8145-ab76403898f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1780,
    "layers": [
        {
            "id": "0c82ab98-f581-4387-8145-ab76403898f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aad8a176-db21-4dbd-8a2f-4fdaf146d631",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1110,
    "xorig": 0,
    "yorig": 0
}