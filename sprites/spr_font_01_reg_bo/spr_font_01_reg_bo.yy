{
    "id": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_reg_bo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd89ec8d-8def-4b81-a981-50375f51ac4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "6a7c3d8c-2868-4466-a11b-65ccc7ad2e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd89ec8d-8def-4b81-a981-50375f51ac4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2954828-ba31-4fb6-9297-84d6337eed9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd89ec8d-8def-4b81-a981-50375f51ac4c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "3c88481f-2ba5-4f19-9a3e-82738a85b457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "2e25a787-0117-4831-9ba5-18671c92ac9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c88481f-2ba5-4f19-9a3e-82738a85b457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c61ebcc-2447-413f-93d6-b03a4023a187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c88481f-2ba5-4f19-9a3e-82738a85b457",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "b2f902fe-ff85-40c0-9918-4e3c5347bbd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "b32c99e0-fc8b-47ee-a064-55a7583a11aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2f902fe-ff85-40c0-9918-4e3c5347bbd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a8ff66-5699-4313-a042-9ea6857c5098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2f902fe-ff85-40c0-9918-4e3c5347bbd5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "6dc571ef-94e4-42d5-a0bf-8f9e7a33b89b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "b6c1d2a8-0012-488a-82af-3ac90f53d774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dc571ef-94e4-42d5-a0bf-8f9e7a33b89b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efe7601-293d-4304-8aae-7299e07848e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dc571ef-94e4-42d5-a0bf-8f9e7a33b89b",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "242bcef8-89b7-43c4-a8d4-ce3cfd4f719a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1f117b68-0ef4-4451-8eb4-24f4381bad1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "242bcef8-89b7-43c4-a8d4-ce3cfd4f719a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a6cf4e4-477f-4678-9123-f0c0dec29b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "242bcef8-89b7-43c4-a8d4-ce3cfd4f719a",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "8f805bc4-dafd-4a18-938d-018b9c7ae62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1172f450-431c-471e-aa4b-f0db182b7153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f805bc4-dafd-4a18-938d-018b9c7ae62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb72dcff-942d-4342-87ef-0a5169c7e15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f805bc4-dafd-4a18-938d-018b9c7ae62f",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "350e65b6-1248-4763-b480-91c1949bd4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "e3408c97-2144-44de-bfc9-e8bf1069d6bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350e65b6-1248-4763-b480-91c1949bd4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996a39f4-c55e-43c7-8029-c71fe74d6616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350e65b6-1248-4763-b480-91c1949bd4a2",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "73ad3fd9-1a24-4c9b-b7b8-206b1fb86c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1bf4342d-9146-4f52-80b5-1b4f4f39751c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ad3fd9-1a24-4c9b-b7b8-206b1fb86c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85ea2b76-975c-4ab1-b873-7d364dfe6961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ad3fd9-1a24-4c9b-b7b8-206b1fb86c8c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "dd32e215-a007-4d29-93e9-dbfe501f6403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "f8a7c194-338d-4ddb-8355-bc41d2fc61ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd32e215-a007-4d29-93e9-dbfe501f6403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1bffb10-17bb-430f-9f53-3d34547f3394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd32e215-a007-4d29-93e9-dbfe501f6403",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "3604de91-5bb2-4bc8-948b-958144dcbbd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "8dba9970-d5be-4caf-a7ea-8f9050b0b729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3604de91-5bb2-4bc8-948b-958144dcbbd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7b8d18-d2b4-4759-8a50-82234902d48a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3604de91-5bb2-4bc8-948b-958144dcbbd5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "29d0299a-9377-49f2-948b-2bad332a1faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "e59743fb-dbf7-48a0-b3a4-e44eda9371b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d0299a-9377-49f2-948b-2bad332a1faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feae322a-788a-4d29-9df8-ab8503963872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d0299a-9377-49f2-948b-2bad332a1faa",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c5c557b6-baaa-4794-9608-79c952592c07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1ae90086-9d69-433c-97ef-02e430def4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5c557b6-baaa-4794-9608-79c952592c07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fecb5c2-3c39-4ba0-81e8-c459f98d8275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5c557b6-baaa-4794-9608-79c952592c07",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e164bd51-8b06-485b-a3d2-441d39cdab40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "39aef349-16b0-4c63-ba10-7438d315adaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e164bd51-8b06-485b-a3d2-441d39cdab40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59879c3-c439-4cd2-aa4f-10810ce5fb1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e164bd51-8b06-485b-a3d2-441d39cdab40",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "d64eb78b-2497-4b1f-9e48-e5e193872088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d1d2f0ac-3305-42e0-80c7-f6f3c0d91e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d64eb78b-2497-4b1f-9e48-e5e193872088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119441b9-1ac3-431a-8f35-e456a88f4223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d64eb78b-2497-4b1f-9e48-e5e193872088",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "08efe81f-8822-4c6e-a9ec-9c642b1a4f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "a9277c65-e95d-4cd5-b97a-558aa31c590f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08efe81f-8822-4c6e-a9ec-9c642b1a4f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2023ad-7163-49c7-921b-6fe54a6a13c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08efe81f-8822-4c6e-a9ec-9c642b1a4f52",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "eced0c2d-082c-4529-923c-0529ff6a50c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "51711a06-3589-4066-9a06-2e78aca78ec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eced0c2d-082c-4529-923c-0529ff6a50c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e72381c-fb87-4a20-9668-f651b0ec840e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eced0c2d-082c-4529-923c-0529ff6a50c3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "8992a9d9-5f62-4c69-9431-03d051addf6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "6d9763c6-6e3f-4f7b-89c9-b2595cfd2660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8992a9d9-5f62-4c69-9431-03d051addf6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2504d4a8-10a0-42af-8123-862f98fd2218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8992a9d9-5f62-4c69-9431-03d051addf6f",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "49392808-05ce-4c49-8f84-864659e8a8d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "f07bbdf9-145b-4d92-8770-04331b836c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49392808-05ce-4c49-8f84-864659e8a8d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e7a0ef9-884b-405f-af7b-4de300b0877f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49392808-05ce-4c49-8f84-864659e8a8d6",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "79a96074-a282-497c-b66a-e52717dcb44c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "9e493533-a0ed-42d2-83c1-82806f9ba9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a96074-a282-497c-b66a-e52717dcb44c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf428bd3-75ad-4b78-abc1-983303384344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a96074-a282-497c-b66a-e52717dcb44c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c7038f09-36e6-424a-a741-7078bcf2abc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "94f023cf-54f5-421a-b72c-2d8237b2c3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7038f09-36e6-424a-a741-7078bcf2abc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f543f97-0eff-484f-a633-1bf0e1ec7ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7038f09-36e6-424a-a741-7078bcf2abc0",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "eaf31d6c-f843-44e2-b3fa-c2d9dcfe7173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "7d916bd9-9e79-4ccf-a9d7-3155ae35b3bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf31d6c-f843-44e2-b3fa-c2d9dcfe7173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25877f2d-2287-4044-b230-97a784c5e6cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf31d6c-f843-44e2-b3fa-c2d9dcfe7173",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "fe4f6e23-718a-4de8-a8db-ece019fe91b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "b2032e99-c9b7-4dc9-b08d-ecc86f76840e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4f6e23-718a-4de8-a8db-ece019fe91b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e7ca9ed-0e04-4c82-a4d9-e853f6012528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4f6e23-718a-4de8-a8db-ece019fe91b5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "fd2c9ddf-a699-4b44-b1e0-b79aefcbc455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "188aa451-6395-484b-8d0c-4b681d2c315d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2c9ddf-a699-4b44-b1e0-b79aefcbc455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82bf3a7-36a3-4b65-b31b-d7fda69b126c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2c9ddf-a699-4b44-b1e0-b79aefcbc455",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2b55f8c6-48de-4a40-bcf9-1e5c46a90a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "f776cd26-37f5-4cad-9891-ef591372a8fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b55f8c6-48de-4a40-bcf9-1e5c46a90a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16710b17-0bd7-43de-9b8a-5a2b46d93fb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b55f8c6-48de-4a40-bcf9-1e5c46a90a00",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "58c21673-0ba9-4fcf-9de0-b3d2f02b2a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "5621812e-20ca-40f3-ab58-89430a355a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c21673-0ba9-4fcf-9de0-b3d2f02b2a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a754df22-a852-46ad-ba9a-3b6733eda34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c21673-0ba9-4fcf-9de0-b3d2f02b2a7c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "24ded84d-861a-4974-96bd-e7af6b1bed7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "9a13fbe8-8fbc-4730-9b62-1cb4057c0ffb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ded84d-861a-4974-96bd-e7af6b1bed7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445d1f62-3f2d-47a7-8d14-d6af8ba480a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ded84d-861a-4974-96bd-e7af6b1bed7b",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "a98d62cd-3a48-4e13-bdaf-fd8b9f91a4d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "568e4dbd-1557-47ae-b2cf-77e634ba9673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a98d62cd-3a48-4e13-bdaf-fd8b9f91a4d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b250520f-1156-457d-9cd1-9d1c22aeeebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a98d62cd-3a48-4e13-bdaf-fd8b9f91a4d5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e159d246-301b-4ba3-863d-0856186a0e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "68bfcb4c-3e1a-4a98-8135-41fec13fce45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e159d246-301b-4ba3-863d-0856186a0e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06617c8-6fe0-4a81-8bbc-d29754783613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e159d246-301b-4ba3-863d-0856186a0e1a",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "b2c5ebd4-195b-488c-b745-1c56506e6e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "62ba5db0-78db-4cc3-b6cf-230bd400e5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2c5ebd4-195b-488c-b745-1c56506e6e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab3ab3c9-68a3-4894-b88e-dcd7e4bb38b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2c5ebd4-195b-488c-b745-1c56506e6e04",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "3470ebea-6c80-4e30-9400-80f0f78b1e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1bf459eb-26eb-4b63-a36e-7ff1deabd136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3470ebea-6c80-4e30-9400-80f0f78b1e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eae6640-5fee-485b-a28b-1b15d7f04db4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3470ebea-6c80-4e30-9400-80f0f78b1e58",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "a1302576-3b0b-404e-8ba2-8c572a7a575c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4a69e088-501f-4fbd-9abb-10a673b9e213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1302576-3b0b-404e-8ba2-8c572a7a575c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e021d2-7706-4a38-9bdb-46844e15a5fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1302576-3b0b-404e-8ba2-8c572a7a575c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "96ff7865-20e9-45e8-a535-933c05ade9eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d388b110-1aad-46b1-a2a5-34cdd2829bd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ff7865-20e9-45e8-a535-933c05ade9eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38b0469-8f3c-46c8-947c-a7a9a4b96859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ff7865-20e9-45e8-a535-933c05ade9eb",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "10421439-f779-4de7-93e4-7766bd8085c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "b5152d87-b39e-41c3-8a0b-aa3ac9e84998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10421439-f779-4de7-93e4-7766bd8085c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "441a0043-2629-4d00-ad64-7500caf0a967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10421439-f779-4de7-93e4-7766bd8085c8",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "1249f5d0-9738-441e-bce2-cdc6042f93ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1b194fa5-0fc7-490f-8760-1f82a0b64136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1249f5d0-9738-441e-bce2-cdc6042f93ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e7eb9d-b35b-4558-87af-4043f65738a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1249f5d0-9738-441e-bce2-cdc6042f93ab",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "6806578c-335b-4047-9820-c6e10456094e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "2093a899-67ca-4604-a83b-3610f6b009ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6806578c-335b-4047-9820-c6e10456094e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f344fb95-2262-41f5-a161-66d79624f791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6806578c-335b-4047-9820-c6e10456094e",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c728e5d0-581d-42f7-ab3d-56693eed7971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "28a128f0-21b1-46f7-8e0d-602efa3593dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c728e5d0-581d-42f7-ab3d-56693eed7971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69345725-7c5b-45f7-87db-c6b10e79ce39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c728e5d0-581d-42f7-ab3d-56693eed7971",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "a149b97d-1021-4632-a2b9-d331f859a467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "70bae7f1-94ce-4350-abe7-addb2b0a755d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a149b97d-1021-4632-a2b9-d331f859a467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b576733-12d1-4166-b0d9-62a7378eff4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a149b97d-1021-4632-a2b9-d331f859a467",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "32d247f9-21a4-464f-af96-59594f6ce4ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "064bba0b-c0a9-4495-92b7-eda864bf207a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32d247f9-21a4-464f-af96-59594f6ce4ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31cb55e2-7857-4943-b8e6-49e83ee62990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d247f9-21a4-464f-af96-59594f6ce4ac",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "4cbee011-d142-4940-a6aa-9c331da5babd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d0c87411-f5ec-4761-b5b5-fe3889e6e613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cbee011-d142-4940-a6aa-9c331da5babd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7106c812-abca-49a8-ace1-5fecbe998c45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cbee011-d142-4940-a6aa-9c331da5babd",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "efd2137a-e820-4d55-be20-6909720c7dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "ef34c485-4863-4f19-8ee2-1a46f384ac4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efd2137a-e820-4d55-be20-6909720c7dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a01e360-32f7-42da-8bc4-89a2d86eac51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efd2137a-e820-4d55-be20-6909720c7dc7",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "48a3c7c6-ecf4-477c-8e04-9e4686f2b7e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "cc3dcee9-bdfc-41b6-9d18-b8c8cf5bb68a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a3c7c6-ecf4-477c-8e04-9e4686f2b7e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f769360-6e68-4e39-ae48-cdee1e956bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a3c7c6-ecf4-477c-8e04-9e4686f2b7e9",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "89c2677f-f417-4b59-903c-a00d5a419419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "a8a52da5-e942-4477-8ebb-53df5d05f06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c2677f-f417-4b59-903c-a00d5a419419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf44692-9363-414b-b5e7-aad64d2ecd0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c2677f-f417-4b59-903c-a00d5a419419",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "00097d8a-5b92-4790-8a5b-fda8627816c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "3903842e-18d7-4d47-9ea2-3cc4e0fe41c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00097d8a-5b92-4790-8a5b-fda8627816c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9641eb41-b76e-424c-997f-09f367cb64ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00097d8a-5b92-4790-8a5b-fda8627816c9",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2e15f97a-4309-426e-a944-0ef1c0d4aabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "af050178-dc48-41b1-be48-189d2fee4f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e15f97a-4309-426e-a944-0ef1c0d4aabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5095e8cc-f283-4c6c-88e4-22be05a20be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e15f97a-4309-426e-a944-0ef1c0d4aabd",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "65ac8324-9e65-4fa4-8ee3-21115a6370fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4101d4b3-cbb2-4970-b407-5840c51fad12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ac8324-9e65-4fa4-8ee3-21115a6370fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd8523b-5e38-4e47-8ec6-a95b7acf6bd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ac8324-9e65-4fa4-8ee3-21115a6370fb",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e22785bd-c837-4e79-8a76-a664f6adbb2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "c4c899d0-05df-4f28-9114-840c5681c266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22785bd-c837-4e79-8a76-a664f6adbb2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be261a0f-24b8-4c86-8998-f64754d0d598",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22785bd-c837-4e79-8a76-a664f6adbb2b",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "375bffd1-8717-4c2c-9ef1-03063443bf5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "dc9a1357-64df-4b52-bcee-67d396a30bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375bffd1-8717-4c2c-9ef1-03063443bf5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de8bcb93-33f6-43ed-8753-b3aa1ac8762e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375bffd1-8717-4c2c-9ef1-03063443bf5b",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "66ff3019-3fff-480c-8d36-723f67d4305d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4f6b8324-e36a-4f20-8109-a642b2b2d1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66ff3019-3fff-480c-8d36-723f67d4305d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3113526-da08-49df-818a-d2b66481937b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66ff3019-3fff-480c-8d36-723f67d4305d",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e131fe37-ead4-4298-a910-6785a97e2ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "28931509-3cb5-45ec-bb85-05b149105a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e131fe37-ead4-4298-a910-6785a97e2ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dc55083-25ec-4d1a-a9c1-b0cac6f490d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e131fe37-ead4-4298-a910-6785a97e2ccb",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "fb3828b8-3a33-4886-942c-232559cbae59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "2c3cb2e4-fd63-46d9-83a2-b33af1fe5f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb3828b8-3a33-4886-942c-232559cbae59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101c91e8-c479-4bc4-b76e-5d2c31fdbc1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb3828b8-3a33-4886-942c-232559cbae59",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "0b40c0de-13b7-48bb-a1b2-69e1af9e06c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "cfbd3ea9-7c9b-4246-8379-e5bb83ab2782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b40c0de-13b7-48bb-a1b2-69e1af9e06c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476b6ce6-acd1-42bf-9535-60ba5d2bfa15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b40c0de-13b7-48bb-a1b2-69e1af9e06c3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "5f78c9e8-b357-4af9-acaa-4026ab18b2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "a2ed467d-1897-4eb4-9a3b-3331b44979d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f78c9e8-b357-4af9-acaa-4026ab18b2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d232f236-c83a-49d5-a7fb-029471245abe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f78c9e8-b357-4af9-acaa-4026ab18b2ac",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "78e151c7-535c-44e3-b4cc-e8bedf0c904b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "90cf8ab9-0e07-42df-afb4-defc2dcdc1e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78e151c7-535c-44e3-b4cc-e8bedf0c904b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a2825db-5e55-48ba-9d59-eb2e8ea76c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e151c7-535c-44e3-b4cc-e8bedf0c904b",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "d32c1736-65aa-4d16-a305-ca0c35a28ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "c89975cf-64a6-440a-995d-26e79eccf590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d32c1736-65aa-4d16-a305-ca0c35a28ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf72eb5-8962-4da0-a373-3daeff2590d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d32c1736-65aa-4d16-a305-ca0c35a28ea6",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "90dd4fc7-c5ad-4c6b-9d13-1f8013be6451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "06975c74-129f-4e93-b0b2-e8198802c2b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90dd4fc7-c5ad-4c6b-9d13-1f8013be6451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5780ad90-4af4-48f7-b4fc-17511b792c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90dd4fc7-c5ad-4c6b-9d13-1f8013be6451",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "25b23141-c093-41f5-be6c-a67b69f62204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "55607831-987d-4cfd-9522-66f1ab5cb491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b23141-c093-41f5-be6c-a67b69f62204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1d02cec-cda7-42f9-891f-f6debf009d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b23141-c093-41f5-be6c-a67b69f62204",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "8344b40a-adf8-4f72-8e48-a4f0f2a82df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "e4353766-e8cf-474f-9fbf-71abfc38bdb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8344b40a-adf8-4f72-8e48-a4f0f2a82df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af06090-1b87-4fbb-918f-cbc72d0e4a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8344b40a-adf8-4f72-8e48-a4f0f2a82df0",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "97694ce8-6f35-490f-8ee0-30867351bf35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "8114d840-8e2e-4ca2-ab70-a35463c28405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97694ce8-6f35-490f-8ee0-30867351bf35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a21c0a-72fa-4a28-9374-393070ed6135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97694ce8-6f35-490f-8ee0-30867351bf35",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "b5547267-e006-4859-b12d-452334c69370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "3a832a11-30fb-48f1-982d-855838040d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5547267-e006-4859-b12d-452334c69370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63e9b090-e8a6-480f-8910-3512a216e296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5547267-e006-4859-b12d-452334c69370",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "382db739-d29a-4c56-9957-3a4a5167a253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d02c408c-9227-45c4-8dc8-3ddfeb473c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "382db739-d29a-4c56-9957-3a4a5167a253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bfb182a-2871-402f-8b6e-817dfd34e0b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "382db739-d29a-4c56-9957-3a4a5167a253",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "99714d4c-6491-4989-a63d-7f279f70a833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "60d9a43c-fe2b-4761-9443-7f1119fc6d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99714d4c-6491-4989-a63d-7f279f70a833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f4e430-f7f5-42a3-b64b-971f0b531c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99714d4c-6491-4989-a63d-7f279f70a833",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "8ca757bb-0da7-4b97-a81e-6a9b0c5e8709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "54011742-6712-459c-b901-ca9e13e04fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca757bb-0da7-4b97-a81e-6a9b0c5e8709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa82a5b-f7bd-4df3-b05d-65e1db39beb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca757bb-0da7-4b97-a81e-6a9b0c5e8709",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c5f71764-f730-4ddb-a8b9-b40d880f5aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "21ac2408-8aad-4ca9-a141-3014bda3760b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f71764-f730-4ddb-a8b9-b40d880f5aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7d4b42-1b8f-4dd7-8435-2b8c71f5e86a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f71764-f730-4ddb-a8b9-b40d880f5aac",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "7e094033-1044-4f5e-824a-922cc7c293fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "c402c610-fdbc-42f7-bb7c-acf18cffda93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e094033-1044-4f5e-824a-922cc7c293fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75315f8f-ba9a-4775-b73d-ae8b8cd25cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e094033-1044-4f5e-824a-922cc7c293fc",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "a02ab9ed-be16-44bf-a461-cd32464afdcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "601f9696-65f2-4423-9ec8-567252b078de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a02ab9ed-be16-44bf-a461-cd32464afdcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd8317e-cf34-4f7a-8b39-c2f638be7dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a02ab9ed-be16-44bf-a461-cd32464afdcd",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "8fd125fc-6bb3-4869-b23f-a0be476f688c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "2154998b-060b-44f5-9319-a04c77ed2800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd125fc-6bb3-4869-b23f-a0be476f688c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8fc3a4-6276-45da-99cc-6d7fb19b80cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd125fc-6bb3-4869-b23f-a0be476f688c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "a5d4d404-d3a8-44f1-b7ef-8919a0218ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "bedbfe69-b1b1-45aa-82b8-884384d5300c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d4d404-d3a8-44f1-b7ef-8919a0218ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e94442-45c2-4f5f-82e4-677627db51fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d4d404-d3a8-44f1-b7ef-8919a0218ce4",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "994bd4c0-82bf-44bd-a44e-2389b58d3909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "9822a5b0-b993-4084-b85a-ad3a0d8d80ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994bd4c0-82bf-44bd-a44e-2389b58d3909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da41241c-bade-4605-a97a-35f0dfdf57ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994bd4c0-82bf-44bd-a44e-2389b58d3909",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "8cadd210-dc97-4d63-9ded-cfb66611a3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "44e02b2c-cecd-4832-b11c-311cf2ac0c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cadd210-dc97-4d63-9ded-cfb66611a3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d54f898-a5d4-4d5e-8fa5-36a8e2a56ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cadd210-dc97-4d63-9ded-cfb66611a3db",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2c0bec1d-bf80-40d2-bf4a-5f239fd667d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "ef6011b9-02b1-4bd6-bfb9-2715d1bb40ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0bec1d-bf80-40d2-bf4a-5f239fd667d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7638197e-0677-4a67-b800-70c2084cf1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0bec1d-bf80-40d2-bf4a-5f239fd667d8",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e69d2d6c-65ff-4073-ae39-8740b466e9ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "f792673c-b5b4-45f0-a75b-2cf5e6540d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69d2d6c-65ff-4073-ae39-8740b466e9ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb65cd60-e23e-4b45-a19b-bedaed385ace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69d2d6c-65ff-4073-ae39-8740b466e9ec",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f88bd825-6aee-45f1-a2f3-17ecc63914da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4b82f181-567e-4254-9d62-a47531bfda33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f88bd825-6aee-45f1-a2f3-17ecc63914da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd04865-2243-4f25-bcb2-0d60498261fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f88bd825-6aee-45f1-a2f3-17ecc63914da",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "adf654a5-823d-4052-b70e-0e3d0142aa15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "a3d1bf84-7ecb-406d-a39d-f9dce4f13eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf654a5-823d-4052-b70e-0e3d0142aa15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08524dba-abcb-4eae-9186-40edb1d39e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf654a5-823d-4052-b70e-0e3d0142aa15",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "26fb95ce-c0f7-418e-aac2-f5619f62bbc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4b8ab75f-0cbc-4b0d-b4f1-de3a83f96570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26fb95ce-c0f7-418e-aac2-f5619f62bbc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb2f7e1-293f-4d31-bfef-6ddb888cd9dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26fb95ce-c0f7-418e-aac2-f5619f62bbc2",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "651e53f7-944a-4970-8e81-9ddd5923b073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4e399ecc-c43b-4a50-b5f3-88ad98ad432d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "651e53f7-944a-4970-8e81-9ddd5923b073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfa8413-1692-4b63-ba82-7f6194718f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "651e53f7-944a-4970-8e81-9ddd5923b073",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f9720da7-7626-4e33-9c46-9abb6c5a1dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "da289810-81ae-4e86-bf44-0de462945866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9720da7-7626-4e33-9c46-9abb6c5a1dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ccd14a-0543-4261-9470-ca8234a3ef09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9720da7-7626-4e33-9c46-9abb6c5a1dbf",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c24b6150-ee2e-46b2-b314-c32221af79e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "e6cd7588-f348-4ec0-9ec3-c7bb1ebf1e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c24b6150-ee2e-46b2-b314-c32221af79e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "975b8a10-39ef-4fec-8415-7b0acf9a010e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c24b6150-ee2e-46b2-b314-c32221af79e2",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "463edca2-ac8e-46cf-9d00-ea26c47fd77e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "67da91fd-b088-431c-b841-eab8a7c7b244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463edca2-ac8e-46cf-9d00-ea26c47fd77e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd94cfdb-f182-46dd-94e4-7e74522aa3d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463edca2-ac8e-46cf-9d00-ea26c47fd77e",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "1d907f16-3532-4305-a8b4-42ff9abcba98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "432141b8-5b7b-4bd7-a669-6d0ea5b0ed11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d907f16-3532-4305-a8b4-42ff9abcba98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71559052-7133-464d-a2cc-64a68aa5ffe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d907f16-3532-4305-a8b4-42ff9abcba98",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c29bbfbc-9bab-4406-923d-8b47a04286cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "6e514074-fdd2-47c1-ba0a-cbb472d493a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c29bbfbc-9bab-4406-923d-8b47a04286cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b110079f-3860-4521-a66a-047a349230bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c29bbfbc-9bab-4406-923d-8b47a04286cf",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c168aa88-ef67-41ff-b326-a84ef6c235a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "08b50c84-2287-4e13-8fe9-4c7a2452cace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c168aa88-ef67-41ff-b326-a84ef6c235a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e92ed55-770c-47b8-a268-e9fe6ded7052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c168aa88-ef67-41ff-b326-a84ef6c235a3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "08d88e70-58a0-4b0b-a2e2-a23c9932f89e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "0888e72e-1f8c-4de7-9386-70367e2f5df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d88e70-58a0-4b0b-a2e2-a23c9932f89e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82db377-6802-4560-9258-ef5473bc5916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d88e70-58a0-4b0b-a2e2-a23c9932f89e",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "773ff098-2297-430a-998c-2a5cd7dd75f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "8c66108d-a6f8-44cf-a47d-1652426f8470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "773ff098-2297-430a-998c-2a5cd7dd75f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba3d8b5-36e7-4c0f-9c62-d3512dc1705e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773ff098-2297-430a-998c-2a5cd7dd75f4",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "d379963a-3a65-4e94-87f6-f4039827f709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "5ed991da-7f32-4999-97df-319c17169d4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d379963a-3a65-4e94-87f6-f4039827f709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b39748-9265-498e-be4c-4e6908b4e0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d379963a-3a65-4e94-87f6-f4039827f709",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "77ae3f27-e52f-4629-a487-bcd2a6d69d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "0509b161-fb64-473c-a6cd-461c8aac45c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ae3f27-e52f-4629-a487-bcd2a6d69d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec11d11-3b74-495f-a16f-95568381be7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ae3f27-e52f-4629-a487-bcd2a6d69d95",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2641358f-0e19-4d58-9121-c4bbd676f565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "5c56cb44-8032-409b-99e4-169e7b270537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2641358f-0e19-4d58-9121-c4bbd676f565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd6bce5-ad25-43e1-9964-03f6699687c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2641358f-0e19-4d58-9121-c4bbd676f565",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "4a65b890-c3d0-4a30-9a79-0c61cad62f03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "79d9f319-3ee5-4451-8cae-89b7679ce4aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a65b890-c3d0-4a30-9a79-0c61cad62f03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b493da-9054-47cc-ab1c-52a83ff88e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a65b890-c3d0-4a30-9a79-0c61cad62f03",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "b97a0727-2e79-4945-abce-931d3e6d52b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "ed06758c-6ca4-4fc6-adcf-b15a5ba78433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97a0727-2e79-4945-abce-931d3e6d52b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93e27a93-e97b-496f-8af5-421d3e4468b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97a0727-2e79-4945-abce-931d3e6d52b5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f9bdabd7-bf17-4e4b-9533-0ccb23a3494d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "a435fc82-602e-4026-b3eb-b01110c57950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9bdabd7-bf17-4e4b-9533-0ccb23a3494d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba61988f-4be4-4300-9514-96c7b1eddbfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9bdabd7-bf17-4e4b-9533-0ccb23a3494d",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f9dc970f-8798-459c-bca1-3fe6f0022c64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "00aec4d3-13bc-431b-81dd-d752250fb5ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9dc970f-8798-459c-bca1-3fe6f0022c64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ea0231-3233-4166-8499-542b22fcea6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9dc970f-8798-459c-bca1-3fe6f0022c64",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "5e38a326-6db5-43e8-bd84-b12ca732a383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "ff1b69e4-5c65-49b5-9839-f155f57ebf04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e38a326-6db5-43e8-bd84-b12ca732a383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "544536c8-3594-4c46-a21d-0a55cafa970a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e38a326-6db5-43e8-bd84-b12ca732a383",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "87f40741-09db-4c08-a4e8-7084c401c610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "6e09ae86-249e-42ed-a650-7452a3e88567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87f40741-09db-4c08-a4e8-7084c401c610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78f64d9-6df8-42ca-9ca9-fa1e792a7ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87f40741-09db-4c08-a4e8-7084c401c610",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2434d60a-f518-4e9d-80d1-a54729795de0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "40a3e0bc-1483-42bd-85f9-dc2350f340e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2434d60a-f518-4e9d-80d1-a54729795de0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec55503b-dccb-4e08-b7f8-2ca04f4616fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2434d60a-f518-4e9d-80d1-a54729795de0",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "41a1a5dc-72e5-4ac3-8ecd-be7a50e8b26f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d40fc811-bf7c-4262-830d-00cc0eac8dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a1a5dc-72e5-4ac3-8ecd-be7a50e8b26f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "457efa26-994a-4fa7-8fc0-8e083c6ee758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a1a5dc-72e5-4ac3-8ecd-be7a50e8b26f",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "bf21dfb2-5609-4791-b358-aa30b3d592b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "cf763da1-cbdf-499e-9d5d-e298a58209b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf21dfb2-5609-4791-b358-aa30b3d592b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4801c0e3-8236-4599-9182-ad6929ecfc8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf21dfb2-5609-4791-b358-aa30b3d592b4",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "38528a63-e3d2-4c4b-b148-01f6e59f017c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "0bbfa061-1b12-41a1-a3ad-1583437d357c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38528a63-e3d2-4c4b-b148-01f6e59f017c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09645200-cee2-426d-85fc-5051867b8f7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38528a63-e3d2-4c4b-b148-01f6e59f017c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f19a5b3f-7a15-4496-9b3e-13db765cd67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "73c5c650-6a95-49ae-ab47-06a7383decbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f19a5b3f-7a15-4496-9b3e-13db765cd67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9ea4de-33f0-4771-aeaf-4bfa0c6e005e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f19a5b3f-7a15-4496-9b3e-13db765cd67f",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "7415eada-7e33-49e3-8ad7-c818ef181751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "fffdb9a8-1106-4d69-9d58-d676986ab28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7415eada-7e33-49e3-8ad7-c818ef181751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87319cc9-4735-4b82-8014-d25bf99b2e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7415eada-7e33-49e3-8ad7-c818ef181751",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "bd3ae8f2-c03c-4b69-8bba-d6ee0533a168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "e3579c78-e54f-4aec-b008-d726ac2cf6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd3ae8f2-c03c-4b69-8bba-d6ee0533a168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71893d45-3a52-4829-8ac9-047c1c75dd4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd3ae8f2-c03c-4b69-8bba-d6ee0533a168",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e0ffa644-a122-439e-b8a1-d83d2992071d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d4bed890-63db-451e-9c82-ceada3907b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ffa644-a122-439e-b8a1-d83d2992071d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46f0c737-2b9b-4b08-b7ce-effcd2cde6d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ffa644-a122-439e-b8a1-d83d2992071d",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "38b81ded-2234-4703-bcfa-00a0517be820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "c9d424e2-c6e2-4b95-8d53-128e8d423ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b81ded-2234-4703-bcfa-00a0517be820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dae252b-9b15-4091-bafa-a5c4654bd4c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b81ded-2234-4703-bcfa-00a0517be820",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "b3d5e8d4-b81f-49a4-a17c-24bb5b0dda49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "19883df0-fe7e-48ea-a93d-cc32906951f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d5e8d4-b81f-49a4-a17c-24bb5b0dda49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cbef70f-a369-48c6-bb7f-44dd53d835cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d5e8d4-b81f-49a4-a17c-24bb5b0dda49",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "64c092fd-e115-4854-915c-98ff5cfeef00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "68355a6f-c95d-4577-9e45-e0ebadaf70ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c092fd-e115-4854-915c-98ff5cfeef00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66eefbe6-f455-4673-ad70-06e941aa8981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c092fd-e115-4854-915c-98ff5cfeef00",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "3b7b3227-453d-43a2-85ee-5a22c4e53501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "dd483abe-c8e2-4f39-80d6-707453369f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7b3227-453d-43a2-85ee-5a22c4e53501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7063d94a-b011-4696-bc40-86dce17b671b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7b3227-453d-43a2-85ee-5a22c4e53501",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "40987ada-79a1-4001-b579-91a0b496e020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4d429241-e7d3-4dc3-9384-c8b97913392c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40987ada-79a1-4001-b579-91a0b496e020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a826db4-6894-4a42-9378-5a875df8e65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40987ada-79a1-4001-b579-91a0b496e020",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "56ce54cb-216d-46bd-ba37-cee24d700a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "61a60a88-93d3-4a0f-8b93-c084ddad2581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ce54cb-216d-46bd-ba37-cee24d700a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7b8541-c5cc-4932-a83f-99a8482c0717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ce54cb-216d-46bd-ba37-cee24d700a07",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "ccf14db8-afcc-4d26-a6b4-376dd3f6168a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "5d7dc232-4eb8-424d-904f-4cf59bd1d210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf14db8-afcc-4d26-a6b4-376dd3f6168a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27db867f-f899-41e5-a7ff-38d732141af4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf14db8-afcc-4d26-a6b4-376dd3f6168a",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "1fd26e81-b201-41d1-b426-378af26ce34c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "3beafa86-ada3-40f0-a538-821867cbc1cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd26e81-b201-41d1-b426-378af26ce34c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8b1a4cc-60ba-41ea-8f68-f497e33d422e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd26e81-b201-41d1-b426-378af26ce34c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f798d2e0-db48-476a-8c96-b9fa80688894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "51889bb2-5e13-40ec-8a5b-fb646da39a76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f798d2e0-db48-476a-8c96-b9fa80688894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be88a15c-d761-4863-8f83-7dee1c6a5aa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f798d2e0-db48-476a-8c96-b9fa80688894",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "5847eacb-bd74-4425-a201-1686780cb8bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1982413f-2886-49af-9a38-863f72ccab91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5847eacb-bd74-4425-a201-1686780cb8bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3875e6f5-089b-452e-9373-a471247d2b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5847eacb-bd74-4425-a201-1686780cb8bb",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "69e07348-df46-4e6f-9cb4-927df902d471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "7e9692e4-1814-4df1-a185-4229d2e5a20d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69e07348-df46-4e6f-9cb4-927df902d471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38725ad-8e8b-4ca3-9eac-353059945954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69e07348-df46-4e6f-9cb4-927df902d471",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "4e7e5c45-c8ac-49aa-a03b-0269ade80490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "5572c510-70dc-44e2-9308-833ede3a9c65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e7e5c45-c8ac-49aa-a03b-0269ade80490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eba43ef2-25e5-469f-afd9-ff14bb931274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e7e5c45-c8ac-49aa-a03b-0269ade80490",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "9f17dd1e-743a-4758-9efe-2915d25b7da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "3be94273-5619-4e67-8533-1421062969a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f17dd1e-743a-4758-9efe-2915d25b7da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a307c138-6ba4-4664-9118-c3c05680842e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f17dd1e-743a-4758-9efe-2915d25b7da1",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c4879355-fe56-446a-a839-609a8d6e1881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1a10bee9-3642-470f-8d8b-48fb76d0872c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4879355-fe56-446a-a839-609a8d6e1881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8d6193-b6a1-424c-b268-40380617bece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4879355-fe56-446a-a839-609a8d6e1881",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "27ddd13e-9e41-4d19-921a-213c099f59a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "5ba006ab-e519-4be9-be13-7ca4f61511f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ddd13e-9e41-4d19-921a-213c099f59a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91617ba7-7556-4d49-bd27-38d6a978ed94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ddd13e-9e41-4d19-921a-213c099f59a7",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "70c6ac9f-a681-4b44-a9a8-04d8e92d77e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "6dd9f8ba-de01-4d2f-8e9f-8e6309c98880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70c6ac9f-a681-4b44-a9a8-04d8e92d77e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88594a4e-02b4-4658-b471-fa8b3b6f48b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70c6ac9f-a681-4b44-a9a8-04d8e92d77e6",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "47fddefc-6307-482d-af35-bbd6afda28be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "dc74ebd3-217f-4fd5-aca5-6e28cd576be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47fddefc-6307-482d-af35-bbd6afda28be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f52270-a7e6-4cf1-9f9b-c81d6c8c6b09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47fddefc-6307-482d-af35-bbd6afda28be",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "e83b682a-7bcc-4ed0-9ed5-94a610458203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "9019d215-a237-4dcd-b601-17cf4ef97869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e83b682a-7bcc-4ed0-9ed5-94a610458203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84af8f0e-4633-4ab7-bda3-337dc30c63b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83b682a-7bcc-4ed0-9ed5-94a610458203",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "ae5c25c4-acb6-488a-a815-ed486c06956c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "7b9f81e2-7849-42fc-bd80-ed2db65324f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae5c25c4-acb6-488a-a815-ed486c06956c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6418ec2-328c-418c-8c5f-b48802a47dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5c25c4-acb6-488a-a815-ed486c06956c",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "66218d79-bb52-4359-819b-d75d542a0ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "35dbf5ad-fffe-4e9f-84fb-93b6210293ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66218d79-bb52-4359-819b-d75d542a0ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650f5fc2-1f4a-4aa3-8f35-39d4325077f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66218d79-bb52-4359-819b-d75d542a0ed3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "34a0aedc-0786-4289-aa7b-ea4cd660e429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "bd5dea6c-5451-4642-ad8d-10221a5ad60d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34a0aedc-0786-4289-aa7b-ea4cd660e429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba12690-d962-4822-a25d-95f6a125dfd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34a0aedc-0786-4289-aa7b-ea4cd660e429",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "723cdf9a-e9ca-4903-b1fa-b3e727f891e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "8d6d739d-f295-4601-b53f-551389c5abb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "723cdf9a-e9ca-4903-b1fa-b3e727f891e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a7e1e9-f503-439c-a850-312c85c04601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "723cdf9a-e9ca-4903-b1fa-b3e727f891e3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2333ae4e-ae04-4e55-9540-f54e70bac3ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d99a869d-6cc0-4545-94d3-06eabab0a0cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2333ae4e-ae04-4e55-9540-f54e70bac3ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d2c79c3-3529-4407-be60-5225caf1e6d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2333ae4e-ae04-4e55-9540-f54e70bac3ed",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "881ea608-b444-46d8-93da-41afbd21cccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "3d8420d9-cf7f-4b1d-a10e-89eba961946d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "881ea608-b444-46d8-93da-41afbd21cccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67457516-ccef-437e-8e75-6a467f454df4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881ea608-b444-46d8-93da-41afbd21cccf",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "2df0d584-cc16-4404-9bfe-355a46ff12c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "1ad925e5-6145-4ba3-bdeb-6e4129c85efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2df0d584-cc16-4404-9bfe-355a46ff12c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e20daef9-f97f-4be1-99c0-adb3a1e00568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2df0d584-cc16-4404-9bfe-355a46ff12c3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "9e01b22a-8943-42ed-a78f-5b9663d84f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "e9cdc40c-5042-4f16-9a31-7083beb48519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e01b22a-8943-42ed-a78f-5b9663d84f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8c4a10d-b150-4337-ab7f-b8f0ab6c7e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e01b22a-8943-42ed-a78f-5b9663d84f37",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "9d0737ea-8ea5-4e34-9deb-522083ebe903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "4ce8fba2-164e-47cb-9df5-bb03d45bcbe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d0737ea-8ea5-4e34-9deb-522083ebe903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d699cc-6f98-4b5e-8111-65b1e5f0a604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d0737ea-8ea5-4e34-9deb-522083ebe903",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "c4bfe424-a18f-4a7a-8397-b45e1f3126c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "8f97c435-522a-4c46-8d53-ede72d2aa557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4bfe424-a18f-4a7a-8397-b45e1f3126c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13647664-ca9c-44ee-82f4-f3e30f259ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4bfe424-a18f-4a7a-8397-b45e1f3126c8",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "ac9e0c93-f450-4a76-aa79-e1a8a6204f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "130c6cbb-706a-4981-a9fb-0384c16bbef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9e0c93-f450-4a76-aa79-e1a8a6204f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f0548e-fa57-4de2-8099-56267b5a0842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9e0c93-f450-4a76-aa79-e1a8a6204f4d",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "fe0774ff-9ab5-4770-a776-4c8065dc7bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "ebbe5c64-30f0-4f91-bd1c-894e7b03c42c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe0774ff-9ab5-4770-a776-4c8065dc7bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ee56b56-5b23-4a44-9a16-c7ae5d11169a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe0774ff-9ab5-4770-a776-4c8065dc7bb3",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "dafb9d92-dc2a-4a0c-8734-b681b23ed8fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "079d28c0-d091-4664-b228-9929947219eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dafb9d92-dc2a-4a0c-8734-b681b23ed8fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95115f42-b3cb-4e40-a91e-549ceb5b9152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dafb9d92-dc2a-4a0c-8734-b681b23ed8fd",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "4635d607-9364-464a-aee5-86b1d04a0d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "93f3e355-0148-4ec3-8d12-4274e38a1758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4635d607-9364-464a-aee5-86b1d04a0d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8127d3f9-0594-4ee2-ae5a-be038fc1409c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4635d607-9364-464a-aee5-86b1d04a0d27",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "da02b2f4-a4a1-43c1-911d-b19dbdca6054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "86ee99b6-df9a-4426-bab3-e57e957c5009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da02b2f4-a4a1-43c1-911d-b19dbdca6054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c77f561-5ab0-424a-896a-1350d6b48502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da02b2f4-a4a1-43c1-911d-b19dbdca6054",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "f3cc62c8-68b5-40fd-99a9-1f026d992e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "08944e15-e38d-41af-a56b-cf10066cf6e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3cc62c8-68b5-40fd-99a9-1f026d992e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5680cebe-166f-4246-a39f-0553341d4239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3cc62c8-68b5-40fd-99a9-1f026d992e48",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "842439cd-1fe8-4676-a666-635af04ea75e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "52605cc7-f925-4785-a739-eba0c755331b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "842439cd-1fe8-4676-a666-635af04ea75e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09115f47-3b2a-4dc1-b057-9e32a6eb64a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "842439cd-1fe8-4676-a666-635af04ea75e",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "551db35c-e947-4eae-8fb2-4545e1168874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "3a06f786-c0db-4b8e-b239-addeecf26293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "551db35c-e947-4eae-8fb2-4545e1168874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4dc21a4-2696-4d84-b953-2a35bd686b32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "551db35c-e947-4eae-8fb2-4545e1168874",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "a9829866-fd61-42ce-8a9e-cc863d06f0c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "d551abf8-a2db-4b32-912b-3750172dff17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9829866-fd61-42ce-8a9e-cc863d06f0c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871dd2cf-7b17-40d6-894e-994d6d9b4c1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9829866-fd61-42ce-8a9e-cc863d06f0c5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "d4276179-c96f-4021-b623-95b939ed4e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "9dc4f944-908a-4553-b080-89d99a48f987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4276179-c96f-4021-b623-95b939ed4e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eeb422c-3d02-4ebe-a60e-158dbf160cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4276179-c96f-4021-b623-95b939ed4e59",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "26563735-92fe-4950-9e5a-1fef0cce196b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "77d98b61-6132-44ef-aafd-0598b09eb515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26563735-92fe-4950-9e5a-1fef0cce196b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00fefe19-7cc6-4e66-9af7-4afae6c2f6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26563735-92fe-4950-9e5a-1fef0cce196b",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "93c9550d-5ade-4807-aca7-cf99fcf25d66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "a4b3a8bf-41f5-4652-8930-43bcbf4e79dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c9550d-5ade-4807-aca7-cf99fcf25d66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03421844-5ddb-4eba-bf59-4c9c61e3865e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c9550d-5ade-4807-aca7-cf99fcf25d66",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "76cf69a8-c2e1-401a-8c68-72090fa50593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "75fae873-943c-4aaf-97c6-b02e06f0ff09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76cf69a8-c2e1-401a-8c68-72090fa50593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70dae894-cbff-4ca0-bd38-b1608c0fdca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76cf69a8-c2e1-401a-8c68-72090fa50593",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "77f2aef3-28af-417c-95b9-6b18b7c87ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "8082fcbe-37ad-4fcd-ac2a-ac49ece3d0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f2aef3-28af-417c-95b9-6b18b7c87ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b10b06-cccd-48a6-ae72-ce547476b2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f2aef3-28af-417c-95b9-6b18b7c87ca5",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "9ddb4d43-3da4-482d-910e-aa5e420eccba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "820826db-fdca-43c5-abf6-066a16728c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ddb4d43-3da4-482d-910e-aa5e420eccba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0982b610-6363-4b82-9a10-fed07b0162ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ddb4d43-3da4-482d-910e-aa5e420eccba",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "63ca1270-cf31-40db-a6e8-9526278c5069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "7cc47cb6-9992-44ff-bd43-88bdc0d1274f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ca1270-cf31-40db-a6e8-9526278c5069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48074f48-5d5b-44cf-8079-d884aed97448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ca1270-cf31-40db-a6e8-9526278c5069",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        },
        {
            "id": "227b0802-ad97-484b-916a-bf7a170f1bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "compositeImage": {
                "id": "73e0acb6-e544-4bfe-b8ed-a8bca37b53ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "227b0802-ad97-484b-916a-bf7a170f1bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ab8eba7-3c59-4673-b6be-da98bb8f62a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "227b0802-ad97-484b-916a-bf7a170f1bb8",
                    "LayerId": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "ee6f836c-0b91-4ea8-94dd-a10bb7f96457",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "143632b0-5f6f-4ea7-862b-a2683a8d65cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}