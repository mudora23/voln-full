{
    "id": "55cba67a-891c-4de2-9948-6794996fc835",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_might_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf6c6921-7f55-477d-ab9d-14afeb9bb207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55cba67a-891c-4de2-9948-6794996fc835",
            "compositeImage": {
                "id": "3e32eb25-cc2a-4dc6-8931-1e9a5e8ea4d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf6c6921-7f55-477d-ab9d-14afeb9bb207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f167b55-b2f9-4130-86bf-0e9167414212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6c6921-7f55-477d-ab9d-14afeb9bb207",
                    "LayerId": "08eff6d8-1d4e-48c0-bf21-94569150a045"
                },
                {
                    "id": "f7fdf5ec-9c19-47e6-8efa-fd4014a04ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6c6921-7f55-477d-ab9d-14afeb9bb207",
                    "LayerId": "9b14810f-be9c-4ad0-876e-caba04e00750"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "9b14810f-be9c-4ad0-876e-caba04e00750",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55cba67a-891c-4de2-9948-6794996fc835",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "08eff6d8-1d4e-48c0-bf21-94569150a045",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55cba67a-891c-4de2-9948-6794996fc835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}