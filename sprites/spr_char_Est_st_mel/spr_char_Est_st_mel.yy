{
    "id": "e784f55b-5d39-430a-a2ed-07dbf1f4196f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Est_st_mel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1743,
    "bbox_left": 235,
    "bbox_right": 697,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3385012b-8c51-4b82-b584-ce3f1bf8da33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e784f55b-5d39-430a-a2ed-07dbf1f4196f",
            "compositeImage": {
                "id": "77f2817d-08f0-4422-8664-377c8cccd96e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3385012b-8c51-4b82-b584-ce3f1bf8da33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f75a5b6-f5a7-444f-9cd4-a0805abee710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3385012b-8c51-4b82-b584-ce3f1bf8da33",
                    "LayerId": "6c8ff210-7d08-4549-8e54-209498578a53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1804,
    "layers": [
        {
            "id": "6c8ff210-7d08-4549-8e54-209498578a53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e784f55b-5d39-430a-a2ed-07dbf1f4196f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 901,
    "xorig": 0,
    "yorig": 0
}