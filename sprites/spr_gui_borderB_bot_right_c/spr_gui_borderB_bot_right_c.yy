{
    "id": "59e141c3-d081-4fa4-8857-ddcae693115b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_bot_right_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 0,
    "bbox_right": 134,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5243cd29-9130-48c5-9994-ca0840b32d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59e141c3-d081-4fa4-8857-ddcae693115b",
            "compositeImage": {
                "id": "8c10d2c6-6e11-4c75-9c5d-e26c44ea2f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5243cd29-9130-48c5-9994-ca0840b32d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fbec06b-d8ea-4bf4-b1d9-53f5cf013f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5243cd29-9130-48c5-9994-ca0840b32d78",
                    "LayerId": "2ad2cb9e-bb51-4857-adbf-71c87e74474d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "2ad2cb9e-bb51-4857-adbf-71c87e74474d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59e141c3-d081-4fa4-8857-ddcae693115b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 119,
    "yorig": 119
}