{
    "id": "07f20616-1584-4f47-856c-cdcdc3ce293a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_1_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cfe1365-e5dc-41fd-8329-931f0dba12c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f20616-1584-4f47-856c-cdcdc3ce293a",
            "compositeImage": {
                "id": "12c9ba6d-d4d8-458d-942f-b78c20161c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cfe1365-e5dc-41fd-8329-931f0dba12c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066a0620-bb06-4141-b491-d49b6e7bc84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cfe1365-e5dc-41fd-8329-931f0dba12c9",
                    "LayerId": "7a3ef9cb-a16d-4a9d-8031-9975cbd6ede0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "7a3ef9cb-a16d-4a9d-8031-9975cbd6ede0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07f20616-1584-4f47-856c-cdcdc3ce293a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}