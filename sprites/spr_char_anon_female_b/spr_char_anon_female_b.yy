{
    "id": "80480a16-f215-4e31-8c8d-837192270fe6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_anon_female_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1779,
    "bbox_left": 137,
    "bbox_right": 1066,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03e44138-d01c-4995-89bc-038b2113f27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80480a16-f215-4e31-8c8d-837192270fe6",
            "compositeImage": {
                "id": "5b42bcf6-0e9d-4900-88ca-254d15628db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e44138-d01c-4995-89bc-038b2113f27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b158f995-abeb-4811-a6f0-f56cdc4ea049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e44138-d01c-4995-89bc-038b2113f27d",
                    "LayerId": "8aa37f61-624c-4573-ae41-12fe9dcb913e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1780,
    "layers": [
        {
            "id": "8aa37f61-624c-4573-ae41-12fe9dcb913e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80480a16-f215-4e31-8c8d-837192270fe6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1110,
    "xorig": 0,
    "yorig": 0
}