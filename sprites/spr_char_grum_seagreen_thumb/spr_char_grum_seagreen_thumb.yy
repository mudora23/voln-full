{
    "id": "7a247f2c-3b8c-462b-b5b9-0e9d2b7efb5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_seagreen_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42eaddfc-1273-467b-a724-1cd5d09d12c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a247f2c-3b8c-462b-b5b9-0e9d2b7efb5a",
            "compositeImage": {
                "id": "9928d7e8-dcae-47e1-95c7-488e2a740e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42eaddfc-1273-467b-a724-1cd5d09d12c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29127572-83c8-44c2-a085-28ad6128d8b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42eaddfc-1273-467b-a724-1cd5d09d12c5",
                    "LayerId": "5b5971f8-300c-4f77-a05a-4b5bbd269b66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "5b5971f8-300c-4f77-a05a-4b5bbd269b66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a247f2c-3b8c-462b-b5b9-0e9d2b7efb5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}