{
    "id": "af4ca434-44d6-4928-990f-331010ff40ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_jar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 2,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69492b64-cf67-406f-8cec-8112985845d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af4ca434-44d6-4928-990f-331010ff40ba",
            "compositeImage": {
                "id": "e619cc9b-fce0-42d6-89df-dc5f4c6fa266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69492b64-cf67-406f-8cec-8112985845d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4404f36a-9eab-4717-9b5d-0f3048fd7ff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69492b64-cf67-406f-8cec-8112985845d8",
                    "LayerId": "de3ad029-3fae-49c6-8c59-565d1e2f6093"
                }
            ]
        },
        {
            "id": "f0c74ee1-6d95-4b76-8088-d81bbb2a5666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af4ca434-44d6-4928-990f-331010ff40ba",
            "compositeImage": {
                "id": "950a247e-b476-4564-8398-383d3a816374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c74ee1-6d95-4b76-8088-d81bbb2a5666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ff670e-26c0-41a3-baf8-5a5254c2db63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c74ee1-6d95-4b76-8088-d81bbb2a5666",
                    "LayerId": "de3ad029-3fae-49c6-8c59-565d1e2f6093"
                }
            ]
        },
        {
            "id": "8ce99f07-fca0-4942-a08c-31e7c70ceb01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af4ca434-44d6-4928-990f-331010ff40ba",
            "compositeImage": {
                "id": "b2c2db0c-3aa7-41b8-9b1c-eb2e8624b104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce99f07-fca0-4942-a08c-31e7c70ceb01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d4019f-5c99-4939-bc26-1bbffa2e056b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce99f07-fca0-4942-a08c-31e7c70ceb01",
                    "LayerId": "de3ad029-3fae-49c6-8c59-565d1e2f6093"
                }
            ]
        },
        {
            "id": "290f9ef4-52b6-495e-a326-efedc2b70db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af4ca434-44d6-4928-990f-331010ff40ba",
            "compositeImage": {
                "id": "37a21476-19b4-4a14-9a38-8a863737e66a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290f9ef4-52b6-495e-a326-efedc2b70db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe26da7b-0e90-42bb-b98c-d25d9d0c3b46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290f9ef4-52b6-495e-a326-efedc2b70db3",
                    "LayerId": "de3ad029-3fae-49c6-8c59-565d1e2f6093"
                }
            ]
        },
        {
            "id": "083d0b41-cfb3-432a-a37a-75c2d63a0ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af4ca434-44d6-4928-990f-331010ff40ba",
            "compositeImage": {
                "id": "ea8889b1-64ff-4af6-9c37-d38aad330193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083d0b41-cfb3-432a-a37a-75c2d63a0ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab54a259-a857-4cf7-9f0d-e878831e539d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083d0b41-cfb3-432a-a37a-75c2d63a0ffe",
                    "LayerId": "de3ad029-3fae-49c6-8c59-565d1e2f6093"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "de3ad029-3fae-49c6-8c59-565d1e2f6093",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af4ca434-44d6-4928-990f-331010ff40ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}