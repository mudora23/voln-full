{
    "id": "1ea55d92-5241-4be4-8ba7-db99afc9ecc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_left_seemless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 1,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e96d80ee-9ff7-4da0-999e-20b947bd47c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ea55d92-5241-4be4-8ba7-db99afc9ecc6",
            "compositeImage": {
                "id": "e5efda83-187f-4cc6-a1b2-cbcc77de4174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96d80ee-9ff7-4da0-999e-20b947bd47c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7674e170-852a-4c85-b0eb-0904e95dd2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96d80ee-9ff7-4da0-999e-20b947bd47c1",
                    "LayerId": "cfef8f55-1342-4105-986b-3d265f596d5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "cfef8f55-1342-4105-986b-3d265f596d5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ea55d92-5241-4be4-8ba7-db99afc9ecc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 8,
    "yorig": 19
}