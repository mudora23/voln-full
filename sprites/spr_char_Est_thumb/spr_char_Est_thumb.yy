{
    "id": "e3484e9b-610a-44f8-9322-582ba08d3dbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Est_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 8,
    "bbox_right": 294,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb4b008e-89be-46d3-8831-c9e6206b6577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3484e9b-610a-44f8-9322-582ba08d3dbe",
            "compositeImage": {
                "id": "9885c854-fe4d-4877-9d0b-b95aff920c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb4b008e-89be-46d3-8831-c9e6206b6577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8729e4b4-569d-4fe9-8dcf-72f565e8b562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb4b008e-89be-46d3-8831-c9e6206b6577",
                    "LayerId": "2e6cd51d-9446-440d-9326-48258bacded3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "2e6cd51d-9446-440d-9326-48258bacded3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3484e9b-610a-44f8-9322-582ba08d3dbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}