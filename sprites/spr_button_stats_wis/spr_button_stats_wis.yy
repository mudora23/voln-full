{
    "id": "9f679a99-f7ae-468f-a4e7-1d1f9dc943b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_stats_wis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "673bd0a5-0805-44d3-861e-b79657ac8bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f679a99-f7ae-468f-a4e7-1d1f9dc943b7",
            "compositeImage": {
                "id": "baf59e4e-f7f6-48d1-90c4-c97e2f47df16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673bd0a5-0805-44d3-861e-b79657ac8bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d8d894-9cda-40f5-819f-65473fc1beb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673bd0a5-0805-44d3-861e-b79657ac8bbb",
                    "LayerId": "0f72c197-8850-4fac-8833-f7448338aa47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "0f72c197-8850-4fac-8833-f7448338aa47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f679a99-f7ae-468f-a4e7-1d1f9dc943b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}