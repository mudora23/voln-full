{
    "id": "1ff51738-0518-4cbb-a8fd-d8d132a4580e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_blind",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 2,
    "bbox_right": 48,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71b416d2-8d56-41c5-8fcf-898508b96193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ff51738-0518-4cbb-a8fd-d8d132a4580e",
            "compositeImage": {
                "id": "6d3692de-8135-49ff-bbde-6a06a94f280f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b416d2-8d56-41c5-8fcf-898508b96193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcae6c36-bec0-4cb6-9507-1a0d5509ee67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b416d2-8d56-41c5-8fcf-898508b96193",
                    "LayerId": "8ac0eaf6-5a17-430c-a8a6-0bcfe7a19d9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "8ac0eaf6-5a17-430c-a8a6-0bcfe7a19d9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ff51738-0518-4cbb-a8fd-d8d132a4580e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}