{
    "id": "074e8536-9079-4a72-8497-f334b559eb8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_bot_left_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 0,
    "bbox_right": 135,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff8eec98-870c-4fde-a746-94a6a2a61562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074e8536-9079-4a72-8497-f334b559eb8c",
            "compositeImage": {
                "id": "d2452302-c2c6-4e53-b1bd-0738ffbe276f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8eec98-870c-4fde-a746-94a6a2a61562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d05ae7-f484-412a-985c-0e241616d2c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8eec98-870c-4fde-a746-94a6a2a61562",
                    "LayerId": "8db4b31e-114c-496f-8390-95e5f4db2030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 137,
    "layers": [
        {
            "id": "8db4b31e-114c-496f-8390-95e5f4db2030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "074e8536-9079-4a72-8497-f334b559eb8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 136,
    "xorig": 16,
    "yorig": 120
}