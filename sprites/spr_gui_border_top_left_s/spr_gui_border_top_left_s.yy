{
    "id": "44cb62b9-b628-4243-88b8-bfdbb5b74dd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_top_left_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 0,
    "bbox_right": 115,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0e849a6-1387-4b97-acb9-30558ee1c993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44cb62b9-b628-4243-88b8-bfdbb5b74dd3",
            "compositeImage": {
                "id": "12c9b2c6-af08-4051-934a-83f2b67d25a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0e849a6-1387-4b97-acb9-30558ee1c993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5df2c81d-8276-4c96-8e2c-5c3325cb395b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0e849a6-1387-4b97-acb9-30558ee1c993",
                    "LayerId": "1ccef75a-a023-4e59-9e20-962a762d771f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "1ccef75a-a023-4e59-9e20-962a762d771f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44cb62b9-b628-4243-88b8-bfdbb5b74dd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 21,
    "yorig": 21
}