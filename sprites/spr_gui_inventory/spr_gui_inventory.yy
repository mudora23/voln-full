{
    "id": "c3be5441-bf26-4340-8965-3201a8fdd2a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_inventory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 9,
    "bbox_right": 241,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "856a6179-c229-4fee-8cc4-c001483b53e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3be5441-bf26-4340-8965-3201a8fdd2a5",
            "compositeImage": {
                "id": "db0c84ee-b245-449d-b2c9-77289bcfbdba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "856a6179-c229-4fee-8cc4-c001483b53e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f11bb7d-e831-45ca-a000-c86fe32ff82e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "856a6179-c229-4fee-8cc4-c001483b53e8",
                    "LayerId": "97d5ae5e-87a8-49ee-9b52-42246f925dc2"
                }
            ]
        },
        {
            "id": "b9ffcc7f-c558-4800-a516-6c19d5f474f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3be5441-bf26-4340-8965-3201a8fdd2a5",
            "compositeImage": {
                "id": "f65da8cf-bd79-41c9-a04c-b0ff38f4a281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ffcc7f-c558-4800-a516-6c19d5f474f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff412733-ca16-4171-ab38-00b335e8e374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ffcc7f-c558-4800-a516-6c19d5f474f6",
                    "LayerId": "97d5ae5e-87a8-49ee-9b52-42246f925dc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "97d5ae5e-87a8-49ee-9b52-42246f925dc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3be5441-bf26-4340-8965-3201a8fdd2a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}