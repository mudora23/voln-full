{
    "id": "a245e9da-4e9d-46cd-bff7-dd78c0415ec3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TMC_PL_Cloud_2_spr_0_part",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 4,
    "bbox_right": 123,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65f94f93-a8c7-46f1-a8c4-931011d4117f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a245e9da-4e9d-46cd-bff7-dd78c0415ec3",
            "compositeImage": {
                "id": "5c75e7ea-82e6-43a2-8b0c-66766fdcfd5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f94f93-a8c7-46f1-a8c4-931011d4117f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001c139d-7790-4f88-9fce-1ceabe83e60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f94f93-a8c7-46f1-a8c4-931011d4117f",
                    "LayerId": "aea7330f-8bd6-4d3d-a274-26e28d682629"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aea7330f-8bd6-4d3d-a274-26e28d682629",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a245e9da-4e9d-46cd-bff7-dd78c0415ec3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}