{
    "id": "7187c1b7-0cc9-492c-bb1b-8ed24059b9ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_2b_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a5ffa70-1453-4fe5-aeb3-ad2a2d5175de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7187c1b7-0cc9-492c-bb1b-8ed24059b9ae",
            "compositeImage": {
                "id": "caf4edf2-2fc8-4d20-a292-45ceebc59268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a5ffa70-1453-4fe5-aeb3-ad2a2d5175de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb36ca4-d78e-4ada-8790-31862afe287d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a5ffa70-1453-4fe5-aeb3-ad2a2d5175de",
                    "LayerId": "f09fb4a2-460e-42cf-a15a-d6763dd5d678"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "f09fb4a2-460e-42cf-a15a-d6763dd5d678",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7187c1b7-0cc9-492c-bb1b-8ed24059b9ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}