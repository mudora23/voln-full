{
    "id": "b0aac774-75aa-4cc7-b181-d4aba9663f29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Dungeon_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36c66c0e-872b-44dc-a9e9-ff68eedd605b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0aac774-75aa-4cc7-b181-d4aba9663f29",
            "compositeImage": {
                "id": "3eb71e59-3ca8-47e0-a3c1-4fb18e908b27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c66c0e-872b-44dc-a9e9-ff68eedd605b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6224f27-94b4-40d5-934d-998adb5a2e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c66c0e-872b-44dc-a9e9-ff68eedd605b",
                    "LayerId": "8ec20420-e737-4e90-b92d-619dff2fbfc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "8ec20420-e737-4e90-b92d-619dff2fbfc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0aac774-75aa-4cc7-b181-d4aba9663f29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}