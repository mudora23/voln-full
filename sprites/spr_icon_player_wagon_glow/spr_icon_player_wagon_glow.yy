{
    "id": "983849f3-976b-4944-a1f7-38456270d56d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_player_wagon_glow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70e6feb3-0aeb-4499-b109-3a00cf2ca4d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "0e3b32f4-63f6-4713-a831-56781eacb88a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e6feb3-0aeb-4499-b109-3a00cf2ca4d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f948d5-2017-493b-9aed-b60babd3646d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e6feb3-0aeb-4499-b109-3a00cf2ca4d2",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "f99d2066-a695-46f0-92cc-4cf08e96dd2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "82324b12-b4ac-41cd-9ed8-261849fda9d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f99d2066-a695-46f0-92cc-4cf08e96dd2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bec679-3a9a-42bc-b492-ca1c32b1f8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f99d2066-a695-46f0-92cc-4cf08e96dd2e",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "c8bef053-f022-4c3a-9445-57b2d9a0e00b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "2e4b0755-2063-47f0-be99-adc488950185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bef053-f022-4c3a-9445-57b2d9a0e00b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36df8964-a2d9-4994-897e-06b278bda5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bef053-f022-4c3a-9445-57b2d9a0e00b",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "36a7fe91-5302-4f1d-9578-d71119c0bbac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "cc15f2f0-8552-4625-bb91-36a72876cb89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a7fe91-5302-4f1d-9578-d71119c0bbac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e8ffcf-7ebb-431c-b0e2-811b80031cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a7fe91-5302-4f1d-9578-d71119c0bbac",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "40c1f4d7-d489-4af8-a1c2-14a49dde6862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "affffee5-38ce-45dd-987f-9dd9cc828788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c1f4d7-d489-4af8-a1c2-14a49dde6862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e569e0ff-e83a-465a-9968-24be8d434720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c1f4d7-d489-4af8-a1c2-14a49dde6862",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "c1a20bdf-bc4a-46f8-b0d5-0c2963f93d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "49c6660d-3736-4177-b8d5-1836f2d7854b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a20bdf-bc4a-46f8-b0d5-0c2963f93d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b934c9-81d3-4f6e-ab7f-b9cc8708a77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a20bdf-bc4a-46f8-b0d5-0c2963f93d3b",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "5a8ecaa8-2089-48d6-a72a-6afcf32fc62e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "1b6da012-29e5-4130-b3cf-f75e2bfc73ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a8ecaa8-2089-48d6-a72a-6afcf32fc62e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59a3082-acad-422c-90e8-6379fd182c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a8ecaa8-2089-48d6-a72a-6afcf32fc62e",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "f143f3e3-8bfe-41e7-9f50-b1334a4e3d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "d81049e0-f774-4934-8a16-03721282ba1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f143f3e3-8bfe-41e7-9f50-b1334a4e3d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "045c8883-ff7d-4b9a-b18c-f9187fd7e9ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f143f3e3-8bfe-41e7-9f50-b1334a4e3d2e",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "4779a8bf-9efd-401c-98f5-d84e347380e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "908ab61a-3fbd-4edc-8f04-358874b471da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4779a8bf-9efd-401c-98f5-d84e347380e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7298d45-ba57-4aa2-9722-fb7f164d809f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4779a8bf-9efd-401c-98f5-d84e347380e7",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "ce871140-d580-401c-8252-3bd6116c6c1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "f8d5b42e-1cab-40ea-8b1b-613c0d1a3688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce871140-d580-401c-8252-3bd6116c6c1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb0448a-55ff-4c69-b858-4fd367c9131b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce871140-d580-401c-8252-3bd6116c6c1d",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "66d6a1d8-8f3e-4510-b6c3-308c7fbf585f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "48d7956c-d387-4164-bee8-f811d946422d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d6a1d8-8f3e-4510-b6c3-308c7fbf585f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5240c10e-edae-4edd-9e58-c22cf5be1300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d6a1d8-8f3e-4510-b6c3-308c7fbf585f",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "fde0b3e6-a71c-4c36-87e3-026e0679a259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "8d98c0be-0d8f-4268-8c7b-b50c93970c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde0b3e6-a71c-4c36-87e3-026e0679a259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaef73a6-f4de-465a-b553-c3f9ed197c11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde0b3e6-a71c-4c36-87e3-026e0679a259",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "7cc7bd89-dd9c-4586-b0d6-84dc7b59933a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "db8b5369-654c-4e55-900c-ee8ebebf54b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc7bd89-dd9c-4586-b0d6-84dc7b59933a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b7147e-1d08-4a29-8ed5-23afa157c1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc7bd89-dd9c-4586-b0d6-84dc7b59933a",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "b774777e-f747-4e2b-89e2-b8bc4be66923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "e1f77f46-9169-4225-9283-d15b85c8f654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b774777e-f747-4e2b-89e2-b8bc4be66923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f19800-e7ea-4d2f-be7a-223e98d97909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b774777e-f747-4e2b-89e2-b8bc4be66923",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "96e0b08d-17a8-4503-89a9-9a41e8f3a2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "a24f7fab-c73f-414c-af82-bb90ad875596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e0b08d-17a8-4503-89a9-9a41e8f3a2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0ee26a-113d-43dd-96bd-04f1db8f5fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e0b08d-17a8-4503-89a9-9a41e8f3a2d0",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "9bcbfb09-f656-4bb1-b4ad-99b88e10ca36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "c479b244-33a9-4b4a-8d67-920603889543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bcbfb09-f656-4bb1-b4ad-99b88e10ca36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c76468-c932-40b8-aa02-b2024e799784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bcbfb09-f656-4bb1-b4ad-99b88e10ca36",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "5ad706dd-c3a9-43a2-afc9-8e16c50cfef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "ec6656d7-352d-4155-94b2-16713c9319da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad706dd-c3a9-43a2-afc9-8e16c50cfef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "503a8b6e-645e-46d0-af1a-91e61ba8271d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad706dd-c3a9-43a2-afc9-8e16c50cfef9",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "f68ff243-86ce-4084-ad97-709bbcfbf77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "9a20fe2f-d881-4616-a479-a826ec940ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f68ff243-86ce-4084-ad97-709bbcfbf77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3292edac-9592-4428-97d9-2c73ace04ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f68ff243-86ce-4084-ad97-709bbcfbf77d",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "c7b34f74-afa1-457d-b052-a5730df1722e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "a18b0e23-b316-4583-8b60-333c4d5fa42f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7b34f74-afa1-457d-b052-a5730df1722e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117a9d4f-f397-45ce-9e03-4ca5ae13eb02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7b34f74-afa1-457d-b052-a5730df1722e",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "b1da1cf7-f65a-49e5-b3d6-8714fbeae9f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "276bded8-c379-4558-8373-6fd2a1970bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1da1cf7-f65a-49e5-b3d6-8714fbeae9f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d4103f-4d00-4655-9fed-4d54683acf4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1da1cf7-f65a-49e5-b3d6-8714fbeae9f6",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "fd6b6c2e-634d-4c05-9bb6-26978a90fce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "eab27a3d-dcf2-451e-92cb-af2eeed4e06f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6b6c2e-634d-4c05-9bb6-26978a90fce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1eedb63-7aab-4c35-927d-4d1f7b4eb78e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6b6c2e-634d-4c05-9bb6-26978a90fce7",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "45c016c3-7217-4103-919f-9f81bb37d553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "7e6980aa-57e3-40ec-8d28-2aebfcfb7ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c016c3-7217-4103-919f-9f81bb37d553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc02dfe1-36fb-40c1-904c-dcbeb06fad73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c016c3-7217-4103-919f-9f81bb37d553",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "4bcdfd39-e88e-4345-98f5-6d40103cd5e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "45dda69a-2fe9-4d65-a59b-9576fcc6ca8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bcdfd39-e88e-4345-98f5-6d40103cd5e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612b2aa4-13a5-4486-b669-73440f61de1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bcdfd39-e88e-4345-98f5-6d40103cd5e3",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        },
        {
            "id": "e4daa8b3-aa07-4334-8edc-fb370a85681e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "compositeImage": {
                "id": "2d9640c5-c008-49a0-8c06-560907ea3465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4daa8b3-aa07-4334-8edc-fb370a85681e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c428d2fc-d4ad-45df-9242-68477ac44900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4daa8b3-aa07-4334-8edc-fb370a85681e",
                    "LayerId": "65c145c7-5859-4386-829c-6d675058b0b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "65c145c7-5859-4386-829c-6d675058b0b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "983849f3-976b-4944-a1f7-38456270d56d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 35
}