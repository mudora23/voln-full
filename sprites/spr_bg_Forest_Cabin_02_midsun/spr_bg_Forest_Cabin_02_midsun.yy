{
    "id": "375c220e-ba09-4154-a365-15b0d5c9a6b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Forest_Cabin_02_midsun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ccdf35c-dfc7-4692-9597-efc000424b65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "375c220e-ba09-4154-a365-15b0d5c9a6b5",
            "compositeImage": {
                "id": "fff86df6-f620-4f7e-bb6d-65614147c2be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ccdf35c-dfc7-4692-9597-efc000424b65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9030654e-0f32-4801-865b-e3dda7da5a5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ccdf35c-dfc7-4692-9597-efc000424b65",
                    "LayerId": "a7a82192-8f45-42fe-bfe5-a08e8de161c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "a7a82192-8f45-42fe-bfe5-a08e8de161c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "375c220e-ba09-4154-a365-15b0d5c9a6b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}