{
    "id": "e76135dd-8917-4830-8f22-3bb38e020240",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_Nirholt_HD_01_empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 331,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4c85a32-23a8-4eb3-9d0e-532ce3fcc060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76135dd-8917-4830-8f22-3bb38e020240",
            "compositeImage": {
                "id": "abcda1e6-1c71-4e6f-9e78-aca03d66b148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4c85a32-23a8-4eb3-9d0e-532ce3fcc060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc2a632-5c34-401c-a39e-839d5b697cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4c85a32-23a8-4eb3-9d0e-532ce3fcc060",
                    "LayerId": "5f2b3b79-e716-478f-9816-c73b8a4a4349"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 332,
    "layers": [
        {
            "id": "5f2b3b79-e716-478f-9816-c73b8a4a4349",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e76135dd-8917-4830-8f22-3bb38e020240",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}