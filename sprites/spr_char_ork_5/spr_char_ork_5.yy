{
    "id": "0113947a-33c4-4cd2-ae93-569c3fe83afe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_ork_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1997,
    "bbox_left": 0,
    "bbox_right": 1111,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec584ac0-053c-48aa-ae5a-df9babada1f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0113947a-33c4-4cd2-ae93-569c3fe83afe",
            "compositeImage": {
                "id": "1317bb42-7b43-4c5f-ba54-737a81bede05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec584ac0-053c-48aa-ae5a-df9babada1f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4682d87-fbe2-482b-9d87-f8635c24a1db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec584ac0-053c-48aa-ae5a-df9babada1f8",
                    "LayerId": "a7cd2035-99ba-4e4a-98be-a0b5582622c3"
                }
            ]
        },
        {
            "id": "80030813-bf77-4380-a0d9-605129eb986b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0113947a-33c4-4cd2-ae93-569c3fe83afe",
            "compositeImage": {
                "id": "732dc581-317c-4db7-9e87-1ff5f885e3f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80030813-bf77-4380-a0d9-605129eb986b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e928faca-acf7-4705-9463-93e90dff5812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80030813-bf77-4380-a0d9-605129eb986b",
                    "LayerId": "a7cd2035-99ba-4e4a-98be-a0b5582622c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "a7cd2035-99ba-4e4a-98be-a0b5582622c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0113947a-33c4-4cd2-ae93-569c3fe83afe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1150,
    "xorig": 0,
    "yorig": 0
}