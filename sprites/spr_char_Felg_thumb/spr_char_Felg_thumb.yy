{
    "id": "939c2b9b-e7c1-4ca9-80af-57cc3230018c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Felg_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 45,
    "bbox_right": 267,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d5c8a0b-76b5-48a9-8255-17145f345482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "939c2b9b-e7c1-4ca9-80af-57cc3230018c",
            "compositeImage": {
                "id": "c54966af-a83d-4f83-9447-6bcc83c5663f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d5c8a0b-76b5-48a9-8255-17145f345482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60fc2143-f4c6-40d3-b910-d1580fc7eb01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5c8a0b-76b5-48a9-8255-17145f345482",
                    "LayerId": "07b7f4cc-dcc0-40e8-84b7-e85809b4a272"
                }
            ]
        },
        {
            "id": "3c871e1f-c012-4a8d-92ff-27de88fd758e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "939c2b9b-e7c1-4ca9-80af-57cc3230018c",
            "compositeImage": {
                "id": "894a928b-6bc5-4080-bafd-610210e10a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c871e1f-c012-4a8d-92ff-27de88fd758e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f9ef8d-7095-4b1d-8fd0-844fd717bfe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c871e1f-c012-4a8d-92ff-27de88fd758e",
                    "LayerId": "07b7f4cc-dcc0-40e8-84b7-e85809b4a272"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "07b7f4cc-dcc0-40e8-84b7-e85809b4a272",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "939c2b9b-e7c1-4ca9-80af-57cc3230018c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}