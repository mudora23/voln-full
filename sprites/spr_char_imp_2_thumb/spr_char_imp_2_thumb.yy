{
    "id": "e1b787a5-84be-44ed-86c5-4fc431f7326f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_imp_2_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1661c49-5ca5-40b5-858c-0ceb31a1a092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b787a5-84be-44ed-86c5-4fc431f7326f",
            "compositeImage": {
                "id": "0bc16208-7239-42d6-9cfe-3d5562139d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1661c49-5ca5-40b5-858c-0ceb31a1a092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a1d92ad-ab81-4c0e-88e9-70366bb5370c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1661c49-5ca5-40b5-858c-0ceb31a1a092",
                    "LayerId": "72710b19-7d3a-40fa-b485-080e8954a9d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "72710b19-7d3a-40fa-b485-080e8954a9d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1b787a5-84be-44ed-86c5-4fc431f7326f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}