{
    "id": "d0f29370-57cf-4b4a-a639-a107a31bbb35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_blue_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dea8acf-3298-459c-8a2c-25332e5ee095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0f29370-57cf-4b4a-a639-a107a31bbb35",
            "compositeImage": {
                "id": "14eec062-e52c-4469-a0d5-e00e2a4b20b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dea8acf-3298-459c-8a2c-25332e5ee095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3df47ee2-9379-4864-90be-a180976fa6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dea8acf-3298-459c-8a2c-25332e5ee095",
                    "LayerId": "8fefa57c-4990-4b12-a741-39e2cdea0d18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "8fefa57c-4990-4b12-a741-39e2cdea0d18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0f29370-57cf-4b4a-a639-a107a31bbb35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}