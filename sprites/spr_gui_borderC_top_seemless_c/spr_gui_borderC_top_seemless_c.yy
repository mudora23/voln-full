{
    "id": "71f7902e-d600-4a4a-a7c4-a5a0835dd95d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_top_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d09f5394-a8ec-4489-81f1-96d2cb1fbda3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71f7902e-d600-4a4a-a7c4-a5a0835dd95d",
            "compositeImage": {
                "id": "03dc74f3-2313-4bed-a6ad-c66be268f047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09f5394-a8ec-4489-81f1-96d2cb1fbda3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05443cda-29e6-4276-ac2f-12793638e9db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09f5394-a8ec-4489-81f1-96d2cb1fbda3",
                    "LayerId": "8d959dbe-95f9-4f4d-8db5-03e21586d171"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "8d959dbe-95f9-4f4d-8db5-03e21586d171",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71f7902e-d600-4a4a-a7c4-a5a0835dd95d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 13
}