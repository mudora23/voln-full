{
    "id": "fe8c8f13-8f25-42cf-a89b-36979e03154c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Zeyd_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66a9d511-affd-44d7-b0a3-4309c8151d1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe8c8f13-8f25-42cf-a89b-36979e03154c",
            "compositeImage": {
                "id": "27510a95-14a9-426c-bd0f-f275fb2800ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66a9d511-affd-44d7-b0a3-4309c8151d1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e62eae-94f4-4bff-af72-c67addd221e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66a9d511-affd-44d7-b0a3-4309c8151d1e",
                    "LayerId": "9fe754e3-002d-42e4-a404-e7770f222a8a"
                }
            ]
        },
        {
            "id": "ea5f019b-201e-47d6-b0c3-2d7f33ace701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe8c8f13-8f25-42cf-a89b-36979e03154c",
            "compositeImage": {
                "id": "bed89200-8a8b-4260-91f9-7dd8e35e38db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5f019b-201e-47d6-b0c3-2d7f33ace701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3f9231-e025-419c-a9cc-6f60aec65fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5f019b-201e-47d6-b0c3-2d7f33ace701",
                    "LayerId": "9fe754e3-002d-42e4-a404-e7770f222a8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "9fe754e3-002d-42e4-a404-e7770f222a8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe8c8f13-8f25-42cf-a89b-36979e03154c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}