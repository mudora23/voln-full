{
    "id": "3d2e9c0b-b3d9-42c7-873a-715ce32f461d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2001,
    "bbox_left": 20,
    "bbox_right": 1229,
    "bbox_top": 130,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49a842d0-9419-4d79-93dd-79f01bec44bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d2e9c0b-b3d9-42c7-873a-715ce32f461d",
            "compositeImage": {
                "id": "38dd20b7-cad5-4fe4-8527-403bc994f8a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a842d0-9419-4d79-93dd-79f01bec44bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51cea91d-8dc0-47e7-a13b-3fca91f73bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a842d0-9419-4d79-93dd-79f01bec44bb",
                    "LayerId": "ad483d4b-68d0-4b15-991c-b87266f3cc4e"
                }
            ]
        },
        {
            "id": "d3ef861a-80f2-458b-ba90-a32492784261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d2e9c0b-b3d9-42c7-873a-715ce32f461d",
            "compositeImage": {
                "id": "49d70095-b4c0-4324-b98a-9ad9d1f0ab40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ef861a-80f2-458b-ba90-a32492784261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb8f9719-0134-4aa8-be22-cb1622c343f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ef861a-80f2-458b-ba90-a32492784261",
                    "LayerId": "ad483d4b-68d0-4b15-991c-b87266f3cc4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2004,
    "layers": [
        {
            "id": "ad483d4b-68d0-4b15-991c-b87266f3cc4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d2e9c0b-b3d9-42c7-873a-715ce32f461d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1230,
    "xorig": 0,
    "yorig": 0
}