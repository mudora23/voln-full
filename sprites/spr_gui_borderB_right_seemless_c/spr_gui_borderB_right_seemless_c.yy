{
    "id": "4c574554-0035-4df3-9095-460eecbae8e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderB_right_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1ef0999-f080-4233-8fd8-039fc58392ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c574554-0035-4df3-9095-460eecbae8e0",
            "compositeImage": {
                "id": "48cfdc89-69a5-4539-85b9-de5700d511c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1ef0999-f080-4233-8fd8-039fc58392ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa2f6ba-f0da-4e9d-aee1-4a93c7cdbdd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1ef0999-f080-4233-8fd8-039fc58392ed",
                    "LayerId": "9357adca-3965-44c5-bcaa-84787c3cedd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "9357adca-3965-44c5-bcaa-84787c3cedd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c574554-0035-4df3-9095-460eecbae8e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 4,
    "yorig": 19
}