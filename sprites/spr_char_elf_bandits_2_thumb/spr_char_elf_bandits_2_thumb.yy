{
    "id": "44cabac0-44b2-43f2-9cc4-13fa66831b2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_elf_bandits_2_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "622bf73b-e27d-4da0-8927-2144addfdf39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44cabac0-44b2-43f2-9cc4-13fa66831b2a",
            "compositeImage": {
                "id": "9e5b334b-651a-4374-88e7-02e92c8739f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622bf73b-e27d-4da0-8927-2144addfdf39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f98d6f5-c989-40d4-b68c-74d6f8b9f011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622bf73b-e27d-4da0-8927-2144addfdf39",
                    "LayerId": "f5c1ec5c-8bb0-480b-bb56-7d5fa28841e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "f5c1ec5c-8bb0-480b-bb56-7d5fa28841e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44cabac0-44b2-43f2-9cc4-13fa66831b2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}