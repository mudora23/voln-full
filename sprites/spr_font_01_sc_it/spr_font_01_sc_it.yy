{
    "id": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_font_01_sc_it",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bd45b85-505f-4b3f-a259-fb1db539d99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9a69d9e9-7890-48ac-8dbb-f6e549aac53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd45b85-505f-4b3f-a259-fb1db539d99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa44cca-e2f6-45d7-a6f4-777a49320f98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd45b85-505f-4b3f-a259-fb1db539d99a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "fb26c3a6-9eaf-4031-902e-224b536a8664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "62b99866-9751-42d7-b34f-cbf2a4f6e4d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb26c3a6-9eaf-4031-902e-224b536a8664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf955022-eaaf-4a29-82f9-f2ed9a6aba20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb26c3a6-9eaf-4031-902e-224b536a8664",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "029e9a44-a3de-4c50-a5c8-baee50ee458a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "28fa66d7-f954-4987-8366-fd1c9f5115c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029e9a44-a3de-4c50-a5c8-baee50ee458a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7f931d-43f0-4b8c-ae79-1189af59c717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029e9a44-a3de-4c50-a5c8-baee50ee458a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e380e85c-7b8a-48c3-b77f-f301521a3646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d429a555-08cf-481f-9641-70dd90778c6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e380e85c-7b8a-48c3-b77f-f301521a3646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50998fe-1e51-4ce5-a6d4-93a1a783af1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e380e85c-7b8a-48c3-b77f-f301521a3646",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d91730f2-a9e4-462b-9238-f311e6b8ae1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "392c75e5-f284-4e85-b8c6-3bb2dc4891c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91730f2-a9e4-462b-9238-f311e6b8ae1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92b5999-d70d-49c5-a7f5-6dd25c7caf6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91730f2-a9e4-462b-9238-f311e6b8ae1b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "0ae82f71-3f8b-46a0-b96e-2d7f1371993b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "13a4b521-8894-419f-ac20-2e641a880f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae82f71-3f8b-46a0-b96e-2d7f1371993b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03526ca6-7112-4581-afb0-9707b82907f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae82f71-3f8b-46a0-b96e-2d7f1371993b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "edab0efa-7bd3-443f-8935-857216ba78bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "ea7c091c-ca98-4ee7-9111-a9e36a6ab597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edab0efa-7bd3-443f-8935-857216ba78bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce678886-ce13-429f-a6a6-697e9c0000b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edab0efa-7bd3-443f-8935-857216ba78bd",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "ce75d9a9-a084-4d87-8d07-7179b01b5698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "08695a11-5354-48a0-9bfc-3a5f1767df08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce75d9a9-a084-4d87-8d07-7179b01b5698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81fd95d2-ec3b-46ab-b3e9-e3647817111a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce75d9a9-a084-4d87-8d07-7179b01b5698",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "9331800b-77b4-4425-9426-c2fc64387b04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "48f34572-529c-4f7b-bd40-3e40c03ea6eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9331800b-77b4-4425-9426-c2fc64387b04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63dc68d-57e7-4048-abf2-5b3296f1fdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9331800b-77b4-4425-9426-c2fc64387b04",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "3d2c1770-d4c5-4ed4-83b8-b1ff8e51ce13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "e0d8de5d-54cd-4821-85c3-8923fcd409b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2c1770-d4c5-4ed4-83b8-b1ff8e51ce13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26283b19-d23b-487b-8c00-4dcb4c96fac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2c1770-d4c5-4ed4-83b8-b1ff8e51ce13",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "49460424-af18-40e6-9fe0-6c63acaae694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "cd6b5f2c-ba42-44c9-b7d7-0f49b25f2589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49460424-af18-40e6-9fe0-6c63acaae694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4707927a-4e85-4391-965c-16ae68347e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49460424-af18-40e6-9fe0-6c63acaae694",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "205f4ba1-3fd5-4f6a-b713-82414e7c5dff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9f4055bf-d9db-40c1-b12b-71782369bf4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "205f4ba1-3fd5-4f6a-b713-82414e7c5dff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc645179-8dd1-4997-adee-a6c1d606b45d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "205f4ba1-3fd5-4f6a-b713-82414e7c5dff",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f5af0cb3-b9f0-42bc-b26d-428a6483b27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9b222ac5-fa01-4b79-b887-ca6cd4ffb814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5af0cb3-b9f0-42bc-b26d-428a6483b27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b86c8d2a-ddc6-4f56-b9d2-10f2c8ae5746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5af0cb3-b9f0-42bc-b26d-428a6483b27e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "ac76e98b-a0fa-4eda-b4a1-f0a45d727cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5a8dc0fd-0ea8-4e16-b4f8-54838385a5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac76e98b-a0fa-4eda-b4a1-f0a45d727cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb5427a-882b-4f18-8fc3-3da369842d47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac76e98b-a0fa-4eda-b4a1-f0a45d727cf5",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "96bd90bf-fe24-4118-ba14-4844acae5db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "1671c5ad-b94d-4872-a3c4-99a1c96e6997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96bd90bf-fe24-4118-ba14-4844acae5db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d81cd53-9120-4f0f-b073-a4c1c6b4c91f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96bd90bf-fe24-4118-ba14-4844acae5db6",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "da583b85-6b6c-4c7c-9394-8c6bd1adebf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5a2016ad-bc07-4dab-aeb3-bf6b4afa3eba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da583b85-6b6c-4c7c-9394-8c6bd1adebf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8626b797-8c38-4d10-8cb1-173d1311e26e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da583b85-6b6c-4c7c-9394-8c6bd1adebf4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "77e33ee2-dd78-48a1-99c4-333a7af0e956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "272ecd1c-ad75-4392-9a8d-884d1a95801f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e33ee2-dd78-48a1-99c4-333a7af0e956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f04b8cd2-834b-4646-a3c0-aecb202658b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e33ee2-dd78-48a1-99c4-333a7af0e956",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f7efe13d-bf88-48d6-86a5-88d210a8f30c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "c77f2257-a5a8-4583-9125-0f3522056bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7efe13d-bf88-48d6-86a5-88d210a8f30c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa213fd-0dd1-45f9-8051-430a69d1209c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7efe13d-bf88-48d6-86a5-88d210a8f30c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "8cd14b0c-1896-4e5f-9dcb-a3939ed739d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "935c65f3-a578-430d-b48f-88eb14869ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd14b0c-1896-4e5f-9dcb-a3939ed739d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172737b0-8196-4f31-b3e3-8c36b779ec9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd14b0c-1896-4e5f-9dcb-a3939ed739d0",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "98ce3100-3445-4ad0-83ba-956ea89c749c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f71131aa-46fc-4ad0-8114-68f7263bd19c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ce3100-3445-4ad0-83ba-956ea89c749c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e8aa73-b168-4ebc-bdc4-6298dbd96aa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ce3100-3445-4ad0-83ba-956ea89c749c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "8357fe43-3081-41b5-9e08-c2c93a2496ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "bc6f1f01-f38f-4ce1-ab8e-1aa9ace6aa55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8357fe43-3081-41b5-9e08-c2c93a2496ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef19c381-003a-4b65-9d76-8f86d2fb6051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8357fe43-3081-41b5-9e08-c2c93a2496ea",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "dd39c8b1-ca97-4d42-86e1-8dadafb1fcf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "b946d94a-e278-4373-bbe9-563f5013cb25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd39c8b1-ca97-4d42-86e1-8dadafb1fcf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02417ac3-ece1-4f0a-820e-e83ae4e1891d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd39c8b1-ca97-4d42-86e1-8dadafb1fcf4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "fe61ed1c-1238-496d-8ff1-cdf4c439f9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "86db2d77-f351-4ae4-9a0c-b1ba76764e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe61ed1c-1238-496d-8ff1-cdf4c439f9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3afddf84-af41-4b86-92ba-63f42d9fc410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe61ed1c-1238-496d-8ff1-cdf4c439f9c8",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "eda45ebd-75cb-4e74-a40e-5903e7c9629e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "8faeb4e6-3825-4b76-81d7-b9df78475220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda45ebd-75cb-4e74-a40e-5903e7c9629e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f030429-1128-4230-b9cd-18cfe51e2eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda45ebd-75cb-4e74-a40e-5903e7c9629e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "c8bc7995-49fb-46f7-8b29-8c4939465d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "75b8f2b3-989a-4e0c-9db0-219de058d09c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bc7995-49fb-46f7-8b29-8c4939465d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533f7998-f77c-41f1-b9d6-c8cd3cbe6e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bc7995-49fb-46f7-8b29-8c4939465d00",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "8a78c35a-5d90-43ef-94f5-3e21fca5a1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "3f02c840-9fb9-42f0-ae07-7cbd1118ed82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a78c35a-5d90-43ef-94f5-3e21fca5a1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bf5b244-df53-496a-8585-4a7af6db9a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a78c35a-5d90-43ef-94f5-3e21fca5a1bd",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "136ede32-166a-4b5f-a614-c93fbadc4ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "330912fe-0a13-4e93-84af-16edc9bf95eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136ede32-166a-4b5f-a614-c93fbadc4ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a03d4799-56a9-4df1-9145-dc8a423f3543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136ede32-166a-4b5f-a614-c93fbadc4ba4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "aa4f41a9-b510-4ea3-85ff-f5e54c72a582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "922a1575-63dd-4899-b6af-1f06c0733c31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa4f41a9-b510-4ea3-85ff-f5e54c72a582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d795e4-5319-481b-8fe6-4e4907c8777f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa4f41a9-b510-4ea3-85ff-f5e54c72a582",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b5897b2b-7c6c-47e5-a15d-749716caead3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "30042795-78b2-400e-a832-5e455416174b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5897b2b-7c6c-47e5-a15d-749716caead3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "885206d6-d9e7-4002-af1d-f7b1b8ed42eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5897b2b-7c6c-47e5-a15d-749716caead3",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d07ee339-e783-4d4e-9386-1aa3fdc8a68b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "013ec4d1-244e-4752-b424-5d62edb602da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07ee339-e783-4d4e-9386-1aa3fdc8a68b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce46122-27ca-4fa8-ac97-ec62d1985283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07ee339-e783-4d4e-9386-1aa3fdc8a68b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f3d6f494-fa5f-4bb0-801e-633c9f4faf59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "1499bdf5-c062-4653-b6ae-66a1c2efa368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d6f494-fa5f-4bb0-801e-633c9f4faf59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c244c71-65e1-446e-86b9-d1f033e6faa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d6f494-fa5f-4bb0-801e-633c9f4faf59",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d42a0a06-e11b-416c-a6c6-d67c2626004b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "88661e36-3a39-42d6-8c94-7978dfdd6a24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d42a0a06-e11b-416c-a6c6-d67c2626004b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f4f50a9-2595-4dfe-8979-e0392b7955ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d42a0a06-e11b-416c-a6c6-d67c2626004b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "ed3143f3-e979-42aa-8721-5251774619b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "1c212e76-e3af-4441-81a0-5ef01a99915e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed3143f3-e979-42aa-8721-5251774619b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b0730f0-e67b-40b9-85ac-d9f75c45ccce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed3143f3-e979-42aa-8721-5251774619b7",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a0172b39-65b5-4a28-b101-75a93fd06c3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "00cc7d6c-2b4c-4837-9d2d-bfe656733593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0172b39-65b5-4a28-b101-75a93fd06c3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "999381ea-5499-4af4-8eec-f77d7892717b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0172b39-65b5-4a28-b101-75a93fd06c3c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "6b82b1d0-f374-4c36-a50e-9198c25b475c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "e0fe6f3c-c81f-44f1-aada-97ef0866c733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b82b1d0-f374-4c36-a50e-9198c25b475c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d019208f-3775-4b6e-bccf-2cb6adccb2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b82b1d0-f374-4c36-a50e-9198c25b475c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b653b104-38df-4d60-8572-5168b1af7e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "159722fb-0383-4a0f-9913-80859ffc6193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b653b104-38df-4d60-8572-5168b1af7e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e489c085-85a7-4d1f-9ec6-6f5b4d5c57b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b653b104-38df-4d60-8572-5168b1af7e5f",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "c598b853-c6e6-4cb9-9188-4d830e0126a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "c42fa9e3-4e33-4030-99fd-5941a25e05e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c598b853-c6e6-4cb9-9188-4d830e0126a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be95ed78-34cf-45a2-af9a-a13b36c3bc2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c598b853-c6e6-4cb9-9188-4d830e0126a9",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f7245f7e-3e84-4d93-9280-cecbb85256cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "eb17004e-bf85-47a1-a2ba-96fa03d68967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7245f7e-3e84-4d93-9280-cecbb85256cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6287e8ac-5454-4739-b0c4-6fb95b2c9de6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7245f7e-3e84-4d93-9280-cecbb85256cd",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "5ebb3160-e98e-457c-8e6c-c04f93913d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "ce196f71-1ed0-4818-9a69-e1b9f2a3cb46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ebb3160-e98e-457c-8e6c-c04f93913d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "703089ba-996b-4cd6-afa8-0b2b284f4e13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ebb3160-e98e-457c-8e6c-c04f93913d8e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "49ada1e5-ecbf-4498-b5e1-07093d2f39a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d1a91214-ecab-465f-b2af-ce46f7a297a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ada1e5-ecbf-4498-b5e1-07093d2f39a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db88c13d-15ad-4602-9104-fd60bb7a44de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ada1e5-ecbf-4498-b5e1-07093d2f39a3",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "614e5104-8ed7-4cb2-a33a-1fcd0b31ba49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5a9644a6-fc8b-4d08-a981-2a7ace36950f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614e5104-8ed7-4cb2-a33a-1fcd0b31ba49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8060d685-b774-4b77-a766-1ff46b991a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614e5104-8ed7-4cb2-a33a-1fcd0b31ba49",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "5902e96f-bf32-4739-a2ab-ec622c998a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f7f9ede5-f438-4825-90ec-176766ba50ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5902e96f-bf32-4739-a2ab-ec622c998a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da430c32-7b41-4e6c-ac19-c0fcf3176ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5902e96f-bf32-4739-a2ab-ec622c998a1e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "c24cbe08-e298-4c71-af79-c46ebc89d6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "960a8363-66d1-4a80-b97f-229211cb970b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c24cbe08-e298-4c71-af79-c46ebc89d6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a544a2-351f-4166-8418-0c616fd73cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c24cbe08-e298-4c71-af79-c46ebc89d6ec",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "00260d80-9aee-4491-9bc7-a67460d2851a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "efa6abb2-0acb-4126-b123-87b1f94d4aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00260d80-9aee-4491-9bc7-a67460d2851a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a57e242-d1c3-45f9-8786-16755288fc5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00260d80-9aee-4491-9bc7-a67460d2851a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a3413d48-2bdd-42d3-a388-92054eeca9fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "0af9614e-5eec-4e6e-8561-36307b0ab8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3413d48-2bdd-42d3-a388-92054eeca9fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3bde222-16b0-40cb-9153-41756cfa68e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3413d48-2bdd-42d3-a388-92054eeca9fc",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "02388252-8d6f-42ee-985a-4041ac1959bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "fc7a190c-46c3-4948-85ef-1f7183ef948e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02388252-8d6f-42ee-985a-4041ac1959bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616688ae-5172-4c93-b7ed-f7979eb5cdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02388252-8d6f-42ee-985a-4041ac1959bb",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "90305c01-f29d-4fa2-b99c-d88f497442ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "6727b708-b99a-4ba4-b02d-234c24cc3f05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90305c01-f29d-4fa2-b99c-d88f497442ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd86490d-814d-4136-838d-3d07a8be4e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90305c01-f29d-4fa2-b99c-d88f497442ac",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "4c649f70-87a7-4a73-a2a8-2ec112438fd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "62d42f48-e0be-4bfc-b6b7-5cba637dc940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c649f70-87a7-4a73-a2a8-2ec112438fd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1afd522-5962-4e0e-b4e7-602ecf538426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c649f70-87a7-4a73-a2a8-2ec112438fd5",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "5b955214-93cc-4674-8ed2-1e9cf165abe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "8b1fddf5-8772-48e8-9679-a7ab50dde381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b955214-93cc-4674-8ed2-1e9cf165abe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070d7902-46f6-46ad-84f8-c15047124e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b955214-93cc-4674-8ed2-1e9cf165abe3",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "28cc2083-cec5-4fb2-b9fa-5175ac11da53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d860f7e8-204d-4c5d-b8ee-f120db89d1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28cc2083-cec5-4fb2-b9fa-5175ac11da53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed9b14dd-68d8-498f-bf2e-a7772a927d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28cc2083-cec5-4fb2-b9fa-5175ac11da53",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "93d277a6-06f8-48a5-ae44-778847d74cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "c55e8329-8cd5-4d1a-a52c-4352457a44d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d277a6-06f8-48a5-ae44-778847d74cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f98288fc-730f-4fd6-8a24-3061bb20176f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d277a6-06f8-48a5-ae44-778847d74cc7",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "8a6255a3-42a3-4bbd-9644-91d8eb130530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d360cd36-23fc-42e2-b59b-b66d6dfd5a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a6255a3-42a3-4bbd-9644-91d8eb130530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83523a0-2661-40d1-bb56-8f1d890d4219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a6255a3-42a3-4bbd-9644-91d8eb130530",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "2d7f7ef9-97ca-4d2f-93b9-88b19b6d2fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "faa2433f-9bb4-420f-9c41-4440b4326de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d7f7ef9-97ca-4d2f-93b9-88b19b6d2fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21cc29a6-d63d-4919-8b7e-12a506a1103c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d7f7ef9-97ca-4d2f-93b9-88b19b6d2fbe",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "1eda186b-e15c-4759-a9ef-ef62fbbc0b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a3a42807-ef5c-4e94-9629-47a071462365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eda186b-e15c-4759-a9ef-ef62fbbc0b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fdceee-6f97-4c24-b0c7-407cdc72e26b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eda186b-e15c-4759-a9ef-ef62fbbc0b31",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "0e7b6af8-ec8b-400f-96d7-7efb31c4648c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5940d8a2-1600-4582-a095-ba60de10b491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7b6af8-ec8b-400f-96d7-7efb31c4648c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12160917-b859-4339-83e5-c67a7377dbf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7b6af8-ec8b-400f-96d7-7efb31c4648c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "8f083dd7-92f7-47c6-93e3-41b6aa703d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "657a0616-3d11-465d-b20e-f914cf8b9c68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f083dd7-92f7-47c6-93e3-41b6aa703d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c08cfc-3804-44c4-be3a-f4b90682514a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f083dd7-92f7-47c6-93e3-41b6aa703d68",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "4bd24f39-02c5-4acf-a716-7c6f16be0b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "3ac063c9-cebd-4ab5-84bb-4fb657c9331f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd24f39-02c5-4acf-a716-7c6f16be0b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2566e946-598e-4346-9ca6-fbff72ab7e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd24f39-02c5-4acf-a716-7c6f16be0b6e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b9d58e8b-5fe5-466e-a49a-959305061b8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "6e09afe8-5709-42df-a93b-3c090d58f153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d58e8b-5fe5-466e-a49a-959305061b8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33dc5f9e-9782-4213-aa44-fc4071cd00ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d58e8b-5fe5-466e-a49a-959305061b8a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "16e6c6a5-03b9-4bd0-8f62-256a8aa20736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5d356d9f-0c7c-490d-a282-14995a5185a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16e6c6a5-03b9-4bd0-8f62-256a8aa20736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ec0976-4d25-46b5-b96f-795df0b8962b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16e6c6a5-03b9-4bd0-8f62-256a8aa20736",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a2b3f09c-7379-4fcf-8e07-aa796243bbc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "6266cc04-1dba-4336-9e54-53bae3de2762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2b3f09c-7379-4fcf-8e07-aa796243bbc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e903e2-da88-46f4-8baf-04012caf2b79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2b3f09c-7379-4fcf-8e07-aa796243bbc0",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "c42529a5-0bf9-44eb-a68b-8a64deaad1d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5dbc59bf-752a-4afa-bf3d-17cc59231c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c42529a5-0bf9-44eb-a68b-8a64deaad1d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c308500-e672-4573-a549-120c42b459a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c42529a5-0bf9-44eb-a68b-8a64deaad1d5",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d3995709-b728-44a7-abbb-61c87bd14739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9bcdb7a4-c2e9-44b7-a55d-484ffcc5e633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3995709-b728-44a7-abbb-61c87bd14739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05370ef7-995c-4902-8259-656cd67a182b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3995709-b728-44a7-abbb-61c87bd14739",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "00119682-1fb5-4b9e-a4cd-66392d1211a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "06dea985-dfbf-48aa-b1a9-1be750a669c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00119682-1fb5-4b9e-a4cd-66392d1211a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f56dc2-6d9e-45a6-ad32-1242f010930d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00119682-1fb5-4b9e-a4cd-66392d1211a0",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "ea9f6ebd-72f5-44d3-a871-ba9ee7d4b53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "351edd8b-a4cf-47ee-9865-082b129764e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea9f6ebd-72f5-44d3-a871-ba9ee7d4b53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae8f01f-c3f1-40a4-abe5-a12c752d744a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea9f6ebd-72f5-44d3-a871-ba9ee7d4b53e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e1e8f86c-9e48-46d4-943d-adf9a285e966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "bab1587f-78f8-4332-8ced-7964c4501376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e8f86c-9e48-46d4-943d-adf9a285e966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56fa2bf5-0896-4ae4-96ae-1e729862c360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e8f86c-9e48-46d4-943d-adf9a285e966",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b8eb26d3-854e-4bde-bd22-5321a2f8b0ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "3038e327-0161-44c8-a9db-2e0599e8e61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8eb26d3-854e-4bde-bd22-5321a2f8b0ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d052c24-1ec9-4c30-adf7-94bc9700a2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8eb26d3-854e-4bde-bd22-5321a2f8b0ad",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "69cf2bb7-c32a-4ab4-9d38-3fd1b65c2d05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "754f1aee-a4e3-4d8a-863e-7a7597e6a6f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69cf2bb7-c32a-4ab4-9d38-3fd1b65c2d05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b76f8a0-fd19-47a7-8af2-9f5a686106da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69cf2bb7-c32a-4ab4-9d38-3fd1b65c2d05",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "235f01f5-69be-4f75-9047-897b00a9539a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "e33e1945-a845-4499-954e-bac321d4047a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235f01f5-69be-4f75-9047-897b00a9539a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b59695e-4646-4fe1-9b9a-02dbafad0ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235f01f5-69be-4f75-9047-897b00a9539a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "55f7a54c-6c9e-45cd-b586-3673feac2672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "44814461-9002-4001-ba45-398a1c45acfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f7a54c-6c9e-45cd-b586-3673feac2672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15908243-2396-47d6-8e7c-5b2646855400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f7a54c-6c9e-45cd-b586-3673feac2672",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "5be60d72-95b9-43b9-b937-3d14d47846f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9ea5342d-ca99-4288-99cf-eb5749527ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be60d72-95b9-43b9-b937-3d14d47846f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b531edef-a3a1-4680-bfa2-970334822d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be60d72-95b9-43b9-b937-3d14d47846f3",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d2afea39-99b8-46fd-af75-9374b947b18e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "1b757436-5c1c-4901-8e66-ad92188a7ae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2afea39-99b8-46fd-af75-9374b947b18e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d3630ec-f1b1-4e98-a843-eafc4770f797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2afea39-99b8-46fd-af75-9374b947b18e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "791f098f-79ff-4b63-930d-471c0b7b2340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "68c269b3-221a-4627-90c6-7c705b78804e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "791f098f-79ff-4b63-930d-471c0b7b2340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b73656bb-2fba-472d-8665-b2438b82d532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "791f098f-79ff-4b63-930d-471c0b7b2340",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "fb4088e4-3eb7-45d4-93c8-af94b0a6cb96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d22656eb-f6ca-4678-9671-d22d6287269a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4088e4-3eb7-45d4-93c8-af94b0a6cb96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1151bf-16c3-4c29-9cce-aa98c922282e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4088e4-3eb7-45d4-93c8-af94b0a6cb96",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "ae600d66-1053-4155-aa4a-307c306d86d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "42b1a2b0-feec-46fb-b0a7-fa2773fed15f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae600d66-1053-4155-aa4a-307c306d86d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "509f49dc-1ff0-45b9-8104-b1a07a9731fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae600d66-1053-4155-aa4a-307c306d86d9",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "3dc639b1-4203-4f0e-957e-dd62fa49002a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f3e77423-d7b4-4e59-92b2-638340311eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dc639b1-4203-4f0e-957e-dd62fa49002a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3c143f-049a-467a-abfd-cf4f57c8987c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc639b1-4203-4f0e-957e-dd62fa49002a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "46cacbe9-e2b5-45bc-a59d-f8504d044d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "581af5e5-e748-4e49-adcd-8d216d82f964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46cacbe9-e2b5-45bc-a59d-f8504d044d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0216edab-62d0-4fd9-9c72-4dd9d20e05b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46cacbe9-e2b5-45bc-a59d-f8504d044d4f",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "6c33ae7b-bb70-496b-b131-c4788016b113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "28e7fbbb-8aca-4d2c-a2e5-e90a4bd5c5a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c33ae7b-bb70-496b-b131-c4788016b113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ccfdd4-d636-438b-bfb2-1d7d5d409afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c33ae7b-bb70-496b-b131-c4788016b113",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "58d111b8-43c8-4077-ace3-1af7ef3877f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "cc4733ba-2141-47b9-8049-783c4b09cabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d111b8-43c8-4077-ace3-1af7ef3877f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6674ceaa-3c70-421f-a159-d11bd1104004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d111b8-43c8-4077-ace3-1af7ef3877f1",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "7336d2ec-0bd7-4ff4-9146-6a03c3c1eed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "e8b7cd86-e811-4113-bf13-695ff8a55023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7336d2ec-0bd7-4ff4-9146-6a03c3c1eed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e40a409-1977-44a0-a9b6-2db4ec91f48c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7336d2ec-0bd7-4ff4-9146-6a03c3c1eed2",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "55b51d68-f301-42f2-a864-702d1895bcd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "2b7ac8e9-90d5-4237-a8c0-022a62702ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b51d68-f301-42f2-a864-702d1895bcd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ddab33e-38ca-4e0d-ba1f-1b540077f511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b51d68-f301-42f2-a864-702d1895bcd4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "9e995c9a-ffac-4d82-942c-8c65a0dc0672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "bd721d58-f65e-413d-920f-bdcd2a8704c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e995c9a-ffac-4d82-942c-8c65a0dc0672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46fe8f4e-aeb3-4b0b-a53f-a96609f457bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e995c9a-ffac-4d82-942c-8c65a0dc0672",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b62b8c6d-2f23-439d-b3d5-aaeddf2338f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "3e16dfab-3188-4566-b361-ad8273e572a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62b8c6d-2f23-439d-b3d5-aaeddf2338f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee5772e7-0fe6-4df5-9261-6df50b7678d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62b8c6d-2f23-439d-b3d5-aaeddf2338f5",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "69a9bfbb-14e2-4930-a0cd-3d55e26e2835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9a3bdbe6-de83-4d36-9840-d4e6c9c24771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a9bfbb-14e2-4930-a0cd-3d55e26e2835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bbad846-bad7-46d1-b06a-8a956f4cc50e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a9bfbb-14e2-4930-a0cd-3d55e26e2835",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "833e4b1f-6fdc-4f22-8840-ed847336a141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "488a8cae-1fc6-45fb-bc59-5d2ab6d7c1ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833e4b1f-6fdc-4f22-8840-ed847336a141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8df7c6f-58c9-4be2-af12-fdca1b9169df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833e4b1f-6fdc-4f22-8840-ed847336a141",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "9b343a7f-1190-48c5-9018-83a0877b0504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "e839f5da-d672-4fb4-ba72-e8a395445bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b343a7f-1190-48c5-9018-83a0877b0504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f865107-d118-426e-8526-1ef1d298404a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b343a7f-1190-48c5-9018-83a0877b0504",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "92349149-6f60-4c06-b004-e6092d5d08ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "8202cddd-deb3-4110-813e-716a72796e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92349149-6f60-4c06-b004-e6092d5d08ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b1efc84-7d94-48dc-8908-755443c4d862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92349149-6f60-4c06-b004-e6092d5d08ec",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "2d538e33-c3ad-4dbb-a87e-65fa49f54a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "79ba0032-08b1-49d0-99a2-01cea383068a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d538e33-c3ad-4dbb-a87e-65fa49f54a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe97b62-1f30-4e28-a7f2-09ddd69b46ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d538e33-c3ad-4dbb-a87e-65fa49f54a6c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "5e14bc37-b156-41cf-a44a-0a23c18afcf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "55b16864-387d-4b56-9bb8-b6ef7e84fbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e14bc37-b156-41cf-a44a-0a23c18afcf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79da4f84-3ebd-43a7-b7f8-8b504ad2e620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e14bc37-b156-41cf-a44a-0a23c18afcf6",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "fbb301d6-b628-4d1c-aee8-63e678818071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "fc9c0f29-69de-4306-8b0f-852cd6c5a1f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb301d6-b628-4d1c-aee8-63e678818071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5b5e24-b7b8-435f-8f40-fb87016215e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb301d6-b628-4d1c-aee8-63e678818071",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d60fddea-9172-4d70-aa98-6c91e1c8eeb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "93e888ed-4ea2-4cdd-a09e-ec5597f722dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d60fddea-9172-4d70-aa98-6c91e1c8eeb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b20def3-5e1b-4917-823d-3a9337536e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d60fddea-9172-4d70-aa98-6c91e1c8eeb4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "67302f7a-1bb3-48a5-955c-54b130ff00c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "82262e15-8ab3-4c8d-a81c-5037cb271e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67302f7a-1bb3-48a5-955c-54b130ff00c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b3f48b-a4eb-444e-b632-3a29a178b5b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67302f7a-1bb3-48a5-955c-54b130ff00c6",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "7723c8d2-eed2-4e1c-b937-3a7e79bcf8e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "889551b0-9b97-4f9b-9883-000741333c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7723c8d2-eed2-4e1c-b937-3a7e79bcf8e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "469c643b-4a0c-4773-b86f-e8e56453b2fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7723c8d2-eed2-4e1c-b937-3a7e79bcf8e2",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "48c6e13d-8523-4dd6-b957-0c9f35d747c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "1ce50b39-3d8c-4a0c-8f95-4391fa860a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48c6e13d-8523-4dd6-b957-0c9f35d747c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb7fdb38-ecd0-44b0-88f3-9c87fe35434c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c6e13d-8523-4dd6-b957-0c9f35d747c8",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "047a91fa-ed4e-4000-8c2f-1b3ea1c3b99d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "0b7b9773-af0b-41bb-a18e-5dbde4fe9054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "047a91fa-ed4e-4000-8c2f-1b3ea1c3b99d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6302790e-6d92-4951-b9da-eb85af68744d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "047a91fa-ed4e-4000-8c2f-1b3ea1c3b99d",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a36460b8-fb0e-409b-ad4f-3149d6051a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f5a073a1-4a63-462a-8112-e6696ee20c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36460b8-fb0e-409b-ad4f-3149d6051a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "120f321a-26d6-4f76-a368-6447caf214a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36460b8-fb0e-409b-ad4f-3149d6051a6a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "dfb3d1c9-269b-4ccb-a9f3-856744d676f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "8d1f2273-a62a-4511-bcf3-9baa041efacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb3d1c9-269b-4ccb-a9f3-856744d676f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3e2101-0762-4369-86c8-2b1a8ac562d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb3d1c9-269b-4ccb-a9f3-856744d676f1",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e857f808-541a-4c9c-b53c-6cd4f8608f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a3273b24-7690-4895-a675-25c5b73f0511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e857f808-541a-4c9c-b53c-6cd4f8608f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a3f3a16-cabf-4764-9e5a-f9f1f1c9d8e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e857f808-541a-4c9c-b53c-6cd4f8608f5b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d28a8d72-43d8-49e0-b622-c7c7ac327ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "931c9383-eb4f-4eb4-9f69-de82983f77c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d28a8d72-43d8-49e0-b622-c7c7ac327ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6b30fc-110c-448d-a2b8-892369faa286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d28a8d72-43d8-49e0-b622-c7c7ac327ee1",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "7acca765-4ced-49fb-97b9-7711f9559f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "da75dc79-8f5e-4dcb-9409-a25d06e2a01d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7acca765-4ced-49fb-97b9-7711f9559f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41c7946-cb3e-4200-bbe8-16995b30f44f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7acca765-4ced-49fb-97b9-7711f9559f5f",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "00d2b3b9-f716-4711-9d73-75fa909e7014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "0a726b3b-556f-4f3c-a242-86e9f47818fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d2b3b9-f716-4711-9d73-75fa909e7014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ad9362-b550-4aa8-8994-53e2b05b6c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d2b3b9-f716-4711-9d73-75fa909e7014",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "7206d5c7-1c6c-4356-97d1-e27d19ae7d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "da494d6e-5d8d-4a7d-b67c-ecf3ecb9798d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7206d5c7-1c6c-4356-97d1-e27d19ae7d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51abeb07-2a72-46ca-ba6f-827e7cb9a92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7206d5c7-1c6c-4356-97d1-e27d19ae7d1a",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a14da5d8-55ce-4ffc-a9e9-0dfeab083740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "190d6a87-a320-4a73-a624-72f17543a0c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a14da5d8-55ce-4ffc-a9e9-0dfeab083740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dbc7fb4-2333-45c8-96dd-2fba6f806fbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a14da5d8-55ce-4ffc-a9e9-0dfeab083740",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d18eaf18-95aa-4365-bfd3-ec330987f507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "6ec75f7e-6580-4f4b-a570-f855739ff9e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18eaf18-95aa-4365-bfd3-ec330987f507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8f2214-6218-4890-af57-4cb6bbcb6569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18eaf18-95aa-4365-bfd3-ec330987f507",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a99b8e1a-6eb7-4bcb-887c-78e4c6fdf45b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a321a420-c9a2-4db0-8e80-287253dcedf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99b8e1a-6eb7-4bcb-887c-78e4c6fdf45b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd464c71-444d-457c-a668-13e90a19b3ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99b8e1a-6eb7-4bcb-887c-78e4c6fdf45b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f95b0989-abbd-47c9-9fe6-61d76c86a4f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f9875a91-1146-4098-a53c-62542117610b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f95b0989-abbd-47c9-9fe6-61d76c86a4f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053484c0-cb3f-4e13-8df4-41070ed04b95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f95b0989-abbd-47c9-9fe6-61d76c86a4f3",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b8c68d3c-f0bd-4baf-abfb-09a8cd0994e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f96dd72e-8300-44d7-a5fa-77c46c31a0e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8c68d3c-f0bd-4baf-abfb-09a8cd0994e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ef714e-0373-49ff-a11f-c74c5855d587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8c68d3c-f0bd-4baf-abfb-09a8cd0994e5",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "26ac33ed-20a8-4fed-afee-984993211c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a9c0a4da-c87a-48aa-a186-ff5a5f94f323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ac33ed-20a8-4fed-afee-984993211c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a0f083-735c-4a56-be6b-35c51c2d603f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ac33ed-20a8-4fed-afee-984993211c68",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "3013ad7a-8ee8-4af9-9a1b-95be1dcb3676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "9212fb87-ea7b-4958-9da5-18d811189fa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3013ad7a-8ee8-4af9-9a1b-95be1dcb3676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a63771-83be-4495-bd48-f88230f78f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3013ad7a-8ee8-4af9-9a1b-95be1dcb3676",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "27a27ff3-0fbd-4e75-a098-787501eac215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "06d91630-baff-4c46-8d77-c9443338d353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27a27ff3-0fbd-4e75-a098-787501eac215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3259b32-3490-491c-a16a-0c61747f886e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a27ff3-0fbd-4e75-a098-787501eac215",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "7f44de7d-110f-4d60-8f96-3f3ea762736f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "6df08a01-3bcc-4e6b-8e39-029b2449a23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f44de7d-110f-4d60-8f96-3f3ea762736f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92a8b33-0608-46b2-93c1-4988db219fa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f44de7d-110f-4d60-8f96-3f3ea762736f",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "13609b13-09e1-4040-a5eb-dbef8639b8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "f47a027d-ec08-4d92-a6da-59c53be5b7b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13609b13-09e1-4040-a5eb-dbef8639b8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670ee3f2-e7b6-48c6-a23f-85ac36f7eb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13609b13-09e1-4040-a5eb-dbef8639b8f7",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "c93b6ba9-b32a-4393-99cd-a8b3cf72b7d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "acf075df-34b4-4cd1-b03c-4cbf9982198c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93b6ba9-b32a-4393-99cd-a8b3cf72b7d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6e597c-dd8c-474e-be56-fdab256205e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93b6ba9-b32a-4393-99cd-a8b3cf72b7d4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f498ea4d-fba1-433a-9317-5d6138523ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "43e65ba1-cbe1-4d0b-a40d-11e6d8e2e535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f498ea4d-fba1-433a-9317-5d6138523ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf8a81c-6b36-4550-9d4a-1d5c0b9089eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f498ea4d-fba1-433a-9317-5d6138523ad2",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f5d03602-1083-48dc-b45e-e6a78e77dc18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "cf78f4d0-cff6-47e0-befd-2bef670d8de9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d03602-1083-48dc-b45e-e6a78e77dc18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf97bdb-4353-4247-93f7-2ce6917d3929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d03602-1083-48dc-b45e-e6a78e77dc18",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "cab769b8-7bf7-4388-9fca-031cb9cbf1fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "ea520e16-02c2-4011-b5d8-a4a08cd87a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cab769b8-7bf7-4388-9fca-031cb9cbf1fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "652d2c3d-d11c-4416-963c-ca33f6fcbbe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cab769b8-7bf7-4388-9fca-031cb9cbf1fd",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "d778b1b6-64f5-4ff7-aa2f-2494f9c9695b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "b4c4a910-8476-44a4-9506-f1068d41b58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d778b1b6-64f5-4ff7-aa2f-2494f9c9695b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e23a15-7864-42c9-9a4f-575091628859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d778b1b6-64f5-4ff7-aa2f-2494f9c9695b",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "1312cf57-2282-4318-b90c-89241f27b9cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "090f4b0e-08bc-4843-89b5-3bb500ffd125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1312cf57-2282-4318-b90c-89241f27b9cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e680675-0ff7-47c7-b80e-afa01f26dc26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1312cf57-2282-4318-b90c-89241f27b9cd",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "1ced76a9-1d92-4eac-8b55-3d12fe918d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "bc907c86-c631-48a0-a70f-5fd84a68f293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ced76a9-1d92-4eac-8b55-3d12fe918d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96fdd9e-c8b8-4f1c-a7c8-7268302a9007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ced76a9-1d92-4eac-8b55-3d12fe918d96",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b74abf4b-695e-49c0-8814-bd299322e304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "69db5385-0db8-45bb-b6cc-22c7293f03f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b74abf4b-695e-49c0-8814-bd299322e304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6421f7f-1df9-4371-a350-623403dec9d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74abf4b-695e-49c0-8814-bd299322e304",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "bcc8ab29-60ee-4dfc-a187-a63d9d71f382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "e6753a4e-40f4-4620-8321-8924e1525661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcc8ab29-60ee-4dfc-a187-a63d9d71f382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f142fdec-9886-4232-9b08-f9cbb95cb330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcc8ab29-60ee-4dfc-a187-a63d9d71f382",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "63158996-7bdb-4b11-b125-4b3d547ae313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "c2f55979-56ef-4de1-836e-58863014c03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63158996-7bdb-4b11-b125-4b3d547ae313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "248612cd-4086-40cd-b802-e3b0201e8691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63158996-7bdb-4b11-b125-4b3d547ae313",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "625c4a90-4f2f-4586-93ff-60439b4c2862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "779b9762-5035-4bfe-8a4b-18967fffde90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "625c4a90-4f2f-4586-93ff-60439b4c2862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f49ca1c4-dee3-4616-aaa5-c53b237a44a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "625c4a90-4f2f-4586-93ff-60439b4c2862",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "871b6253-14f6-4d04-9fb9-40b27c6e46ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "cf83473b-9acf-4ab3-906d-36bb80a744e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "871b6253-14f6-4d04-9fb9-40b27c6e46ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f28a2c7-b920-4950-9424-1bebd3781254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "871b6253-14f6-4d04-9fb9-40b27c6e46ed",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "cffaca3c-a820-4b7e-9355-68ce2c1376a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "898b890e-de25-4951-b589-fbf684769afb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffaca3c-a820-4b7e-9355-68ce2c1376a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a516e3-0629-45a8-951e-d361b25d23cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffaca3c-a820-4b7e-9355-68ce2c1376a5",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "583f3aba-8860-4f0e-a31a-465ee93cbe7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d092f7b8-bad3-4bd2-9b32-1c6901f854a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583f3aba-8860-4f0e-a31a-465ee93cbe7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca1a43ea-76ef-4a3b-b473-28f5c86fa09f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583f3aba-8860-4f0e-a31a-465ee93cbe7e",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e75a045b-0374-4f5f-a732-6aeb5742bcbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "621b2de3-24e7-48da-9f05-7b6663c0c6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75a045b-0374-4f5f-a732-6aeb5742bcbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13a5975-2f66-4389-b884-dbeff7afc464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75a045b-0374-4f5f-a732-6aeb5742bcbc",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "81b10bd9-f37a-43b4-8adf-2a878831e8f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "b8c42b29-a586-46cf-8329-ec5f9edf4d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b10bd9-f37a-43b4-8adf-2a878831e8f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b2f4105-7349-4a2a-a4ff-a38f5ee31323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b10bd9-f37a-43b4-8adf-2a878831e8f6",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "be5c111d-c9c9-4382-9ae2-e12be9fd92b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "569f4ff7-179d-4bfa-8a3b-894dfc18214f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5c111d-c9c9-4382-9ae2-e12be9fd92b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e11f4517-f0ba-48fc-87bd-63cb16c477ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5c111d-c9c9-4382-9ae2-e12be9fd92b2",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e39e92a2-7b6b-408d-a024-f4b29c5f21f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "db859ca4-8e16-4d61-8dea-3e2d2ecbea10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39e92a2-7b6b-408d-a024-f4b29c5f21f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521f05e3-31f7-4a40-9c50-7f8bd6665812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39e92a2-7b6b-408d-a024-f4b29c5f21f4",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "14c01d43-5b34-442b-abf1-073550afb301",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "74444dc3-3aaf-4ebe-85a7-2504a912ebcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c01d43-5b34-442b-abf1-073550afb301",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d56e111-c512-4012-9c50-5ff26283c035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c01d43-5b34-442b-abf1-073550afb301",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "6990ca63-d5b4-4f33-9738-67f0de60d394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a7f53cf0-5ef7-401e-b8d2-5c389fb465e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6990ca63-d5b4-4f33-9738-67f0de60d394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "516269d9-0a21-42d8-9aef-4634b86b5f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6990ca63-d5b4-4f33-9738-67f0de60d394",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e659cbc5-d8a0-4596-a63a-e4ea8cc73a76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "59946150-fbba-4bdf-a00d-f223f7c7b919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e659cbc5-d8a0-4596-a63a-e4ea8cc73a76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db800ae-3663-4630-9392-ac898307a061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e659cbc5-d8a0-4596-a63a-e4ea8cc73a76",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "8a8f6418-0fb7-432e-bbfe-baaa41540f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "d26711c6-b867-469d-83cd-bc9be0a77385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8f6418-0fb7-432e-bbfe-baaa41540f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f9a190-2788-490a-b883-1e6b780c947c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8f6418-0fb7-432e-bbfe-baaa41540f52",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "49703800-4ed0-4ada-b4bf-1453008dc994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "37a4c284-ba28-4b8b-923a-bc00bf4ec8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49703800-4ed0-4ada-b4bf-1453008dc994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef757921-9610-4bdf-a84c-e078a35ed2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49703800-4ed0-4ada-b4bf-1453008dc994",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a25048d0-7a5a-49ee-a674-3e40cebd0995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "b540d6fa-7ef5-47be-88b3-a0fc6037ed83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25048d0-7a5a-49ee-a674-3e40cebd0995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "497088d7-8e55-450d-8102-9d78672fac29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25048d0-7a5a-49ee-a674-3e40cebd0995",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "937c7796-910d-4e88-abc9-7a195ed8732c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "5388aded-76a8-4224-8301-fa4adeb7bf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "937c7796-910d-4e88-abc9-7a195ed8732c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a17645-81b6-4374-9d2c-94c6c794c4b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "937c7796-910d-4e88-abc9-7a195ed8732c",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "be9a6705-778a-47ed-8c66-8586773875a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a8200dbf-1b7b-426b-82aa-8681254c0353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be9a6705-778a-47ed-8c66-8586773875a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dcef6d7-2500-4eb5-baeb-597ad5db55a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be9a6705-778a-47ed-8c66-8586773875a6",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "627a43a5-88c7-4bbd-b362-0d6dc6b68831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "53228a92-b4c6-4798-9b0c-de7aae8313af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "627a43a5-88c7-4bbd-b362-0d6dc6b68831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011d51e7-d0ef-4a18-a1b0-ecbb72fa1637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "627a43a5-88c7-4bbd-b362-0d6dc6b68831",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "3b6be6ed-3354-438a-91b3-aac7dffa20bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "fc16121d-a6ef-44dc-a06f-84fccc69fbf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b6be6ed-3354-438a-91b3-aac7dffa20bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5a2693e-710d-4af5-b26a-a5aea250e6b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b6be6ed-3354-438a-91b3-aac7dffa20bd",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "f9a38441-306c-4164-9c85-d575178aa61f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "b20addf8-f012-4ba5-a3dd-c3ae7de9dd0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9a38441-306c-4164-9c85-d575178aa61f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc7d13ff-0270-4c5e-ae8b-4bac68951a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9a38441-306c-4164-9c85-d575178aa61f",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a4c4877e-bf49-4003-8964-943e02f6d2ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "354cc762-c871-47d7-b9f3-77a3990ac13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c4877e-bf49-4003-8964-943e02f6d2ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e38ed32-71d2-487e-98e5-a0efc35e3130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c4877e-bf49-4003-8964-943e02f6d2ad",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "b77eddb2-2453-4db7-babe-c59a48da173f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "edfa1aed-dcf9-46df-b30e-d51a5d1dc202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77eddb2-2453-4db7-babe-c59a48da173f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ede2c5d-7854-447d-b72b-7bbb945040ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77eddb2-2453-4db7-babe-c59a48da173f",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "e3c3e174-9606-4eca-adb7-445aefcd21da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "a680c5b5-4832-4e9c-86f5-dc6a2f2a5b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c3e174-9606-4eca-adb7-445aefcd21da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54e54cc-2b7e-41cd-8af2-9515ca2d08f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c3e174-9606-4eca-adb7-445aefcd21da",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "a2793925-8f1a-4586-b05c-1533c0c45777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "bd612095-9ddc-442a-ad8c-875b8667293e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2793925-8f1a-4586-b05c-1533c0c45777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c0ea98-48a5-47f0-b892-bcc6942b4c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2793925-8f1a-4586-b05c-1533c0c45777",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        },
        {
            "id": "bda9b215-cab7-44bf-b890-07459f720322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "compositeImage": {
                "id": "c2cb3551-f8cf-40bc-8aa8-a5066baf8fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda9b215-cab7-44bf-b890-07459f720322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217c50b5-1c87-4fef-a778-3db4a30865af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda9b215-cab7-44bf-b890-07459f720322",
                    "LayerId": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "a5a86e1f-5f79-4e81-a25a-acbd4fe59fc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4d3f2dd-0e49-4bcb-b764-2dcdab47eafc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}