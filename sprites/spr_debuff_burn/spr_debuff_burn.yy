{
    "id": "31164bb4-9f5e-4f40-92c7-03cf84f7619e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debuff_burn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 4,
    "bbox_right": 44,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e8742ca-d565-4be0-aa0d-c1067ba76c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31164bb4-9f5e-4f40-92c7-03cf84f7619e",
            "compositeImage": {
                "id": "6c5771f3-1769-406f-bea4-8b00fd709ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e8742ca-d565-4be0-aa0d-c1067ba76c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5463b6a-fa2a-464f-969d-b3fcc6592781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e8742ca-d565-4be0-aa0d-c1067ba76c43",
                    "LayerId": "d0728d08-1aa8-4719-9bd3-e02ec0805c11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "d0728d08-1aa8-4719-9bd3-e02ec0805c11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31164bb4-9f5e-4f40-92c7-03cf84f7619e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}