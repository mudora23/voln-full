{
    "id": "594ed8e9-b2c4-4fdf-af68-8929eb819c38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_r_ork_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1931,
    "bbox_left": 0,
    "bbox_right": 742,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd486707-8514-4e09-b598-411def7e4c91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "594ed8e9-b2c4-4fdf-af68-8929eb819c38",
            "compositeImage": {
                "id": "1264567f-905f-4195-a8e4-49edd416dda5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd486707-8514-4e09-b598-411def7e4c91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334ec46c-1ca1-4e66-9bdd-ede18e7855e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd486707-8514-4e09-b598-411def7e4c91",
                    "LayerId": "d863da74-c535-4fa0-981f-338edc625054"
                }
            ]
        },
        {
            "id": "63b08c1e-cef1-464d-b45e-194edf08e62e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "594ed8e9-b2c4-4fdf-af68-8929eb819c38",
            "compositeImage": {
                "id": "d83ce045-3da6-4c34-86c7-f0b104261c75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b08c1e-cef1-464d-b45e-194edf08e62e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54532333-d238-451a-a32d-3c48d9b17b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b08c1e-cef1-464d-b45e-194edf08e62e",
                    "LayerId": "d863da74-c535-4fa0-981f-338edc625054"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2004,
    "layers": [
        {
            "id": "d863da74-c535-4fa0-981f-338edc625054",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "594ed8e9-b2c4-4fdf-af68-8929eb819c38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 762,
    "xorig": 0,
    "yorig": 0
}