{
    "id": "17e06d4e-e6de-4cbc-bdf8-1b807156609d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_border_bot_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a33a3760-899d-4008-a269-ebfd32519f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17e06d4e-e6de-4cbc-bdf8-1b807156609d",
            "compositeImage": {
                "id": "2c755f0b-0b6c-4074-b892-e322c544bb97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33a3760-899d-4008-a269-ebfd32519f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9daa10de-81f2-4efb-adf7-96d62e67d70a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33a3760-899d-4008-a269-ebfd32519f30",
                    "LayerId": "558a10c7-9c42-4e22-9494-bbec24cd06b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "558a10c7-9c42-4e22-9494-bbec24cd06b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17e06d4e-e6de-4cbc-bdf8-1b807156609d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 9
}