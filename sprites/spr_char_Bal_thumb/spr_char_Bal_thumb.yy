{
    "id": "2d62b4e3-6ce0-41f4-acc4-f9b2dd29bd96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_Bal_thumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3a9f274-f30b-4df4-a273-3ced77a1a9ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d62b4e3-6ce0-41f4-acc4-f9b2dd29bd96",
            "compositeImage": {
                "id": "255b34f1-d3b4-499b-807c-627a316e54f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a9f274-f30b-4df4-a273-3ced77a1a9ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f12da24-f17e-4dde-b85d-982deeeead6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a9f274-f30b-4df4-a273-3ced77a1a9ae",
                    "LayerId": "d4c44bea-ba15-4b2b-a867-fd7eefae479b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "d4c44bea-ba15-4b2b-a867-fd7eefae479b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d62b4e3-6ce0-41f4-acc4-f9b2dd29bd96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}