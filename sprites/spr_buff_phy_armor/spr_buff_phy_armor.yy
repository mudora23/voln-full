{
    "id": "7c8c04d4-69c1-4195-a66b-973e7bc2ad91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_phy_armor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fd49ced-ab86-41f8-a5fb-e16888e57edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c8c04d4-69c1-4195-a66b-973e7bc2ad91",
            "compositeImage": {
                "id": "a2432019-a69a-47ef-9e4a-9f3885d0ba9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd49ced-ab86-41f8-a5fb-e16888e57edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa284fe5-6cee-4497-83d0-043dd4585bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd49ced-ab86-41f8-a5fb-e16888e57edf",
                    "LayerId": "c6e80678-812e-4bb8-9caa-5494af93f2e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "c6e80678-812e-4bb8-9caa-5494af93f2e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c8c04d4-69c1-4195-a66b-973e7bc2ad91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}