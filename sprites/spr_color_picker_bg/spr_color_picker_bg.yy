{
    "id": "913f6f0f-db53-4e77-bf61-0e12e60a0beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_color_picker_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 989,
    "bbox_left": 0,
    "bbox_right": 989,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f718f877-95d5-488d-b89f-a717cb66840b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "913f6f0f-db53-4e77-bf61-0e12e60a0beb",
            "compositeImage": {
                "id": "1c3c48ad-82dc-479d-af90-abfc2a0a0b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f718f877-95d5-488d-b89f-a717cb66840b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02be4c1f-6120-4674-a2db-ab731259a154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f718f877-95d5-488d-b89f-a717cb66840b",
                    "LayerId": "993fc26e-1969-449b-9c0e-12ec23ec6ecc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 990,
    "layers": [
        {
            "id": "993fc26e-1969-449b-9c0e-12ec23ec6ecc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "913f6f0f-db53-4e77-bf61-0e12e60a0beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 990,
    "xorig": 0,
    "yorig": 0
}