{
    "id": "56ab24d2-c03c-461e-a0f2-fa7332660100",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skill_might_fou",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "611357e5-f72e-40ca-a333-435f50fe73a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ab24d2-c03c-461e-a0f2-fa7332660100",
            "compositeImage": {
                "id": "866d7289-999b-4d5d-b23c-b22785a80d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "611357e5-f72e-40ca-a333-435f50fe73a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be89359-af0f-4029-90b1-cf63e734570f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611357e5-f72e-40ca-a333-435f50fe73a9",
                    "LayerId": "0d0d81c4-87cd-4ba9-af77-1d04ad6358ff"
                },
                {
                    "id": "7070f158-2fba-4c82-b086-185e99747e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611357e5-f72e-40ca-a333-435f50fe73a9",
                    "LayerId": "4e3a0d33-3a60-4c3f-b184-8431248e95d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "4e3a0d33-3a60-4c3f-b184-8431248e95d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56ab24d2-c03c-461e-a0f2-fa7332660100",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0d0d81c4-87cd-4ba9-af77-1d04ad6358ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56ab24d2-c03c-461e-a0f2-fa7332660100",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}