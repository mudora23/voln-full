{
    "id": "2799393b-1078-4c92-878c-2b036235d172",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_grum_yelloworange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1863,
    "bbox_left": 22,
    "bbox_right": 2301,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e371254d-f9a3-4178-b5cf-826fac68a027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2799393b-1078-4c92-878c-2b036235d172",
            "compositeImage": {
                "id": "bc5ac1d2-3358-4e48-90ba-ec815a8b1105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e371254d-f9a3-4178-b5cf-826fac68a027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691ad55f-d3d7-4e40-90b2-ef1c04f32cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e371254d-f9a3-4178-b5cf-826fac68a027",
                    "LayerId": "61cf5d7e-7923-4108-97e3-70331a751469"
                }
            ]
        },
        {
            "id": "74784959-6802-4fbb-8795-66b1eeb5c24a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2799393b-1078-4c92-878c-2b036235d172",
            "compositeImage": {
                "id": "004f3f89-20d5-4400-b4f1-461dbbc220cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74784959-6802-4fbb-8795-66b1eeb5c24a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e3efa7-ee6e-492f-80af-018ec5492c28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74784959-6802-4fbb-8795-66b1eeb5c24a",
                    "LayerId": "61cf5d7e-7923-4108-97e3-70331a751469"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1905,
    "layers": [
        {
            "id": "61cf5d7e-7923-4108-97e3-70331a751469",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2799393b-1078-4c92-878c-2b036235d172",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2358,
    "xorig": 0,
    "yorig": 0
}