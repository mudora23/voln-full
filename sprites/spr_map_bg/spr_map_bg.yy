{
    "id": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_bg",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 449,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "688866db-5eef-45d6-b58e-7ea60639f9c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "c8bdbe25-a6b5-4624-8066-aa8228a1f94b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688866db-5eef-45d6-b58e-7ea60639f9c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49c9d884-7664-40d7-9477-025a7ba582f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688866db-5eef-45d6-b58e-7ea60639f9c2",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "c28579c5-f3af-4d97-92a7-7c2cb3b83044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "a2d7c8b2-e365-403f-98ac-b18c94217ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28579c5-f3af-4d97-92a7-7c2cb3b83044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797aec5c-943d-4c62-86e6-731dc90da79f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28579c5-f3af-4d97-92a7-7c2cb3b83044",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "48cc4c53-2a2f-452d-a6ea-ae01abfbecef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "a99c9d41-f9df-4133-8547-02b6593effc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48cc4c53-2a2f-452d-a6ea-ae01abfbecef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64176954-4bb7-421d-9845-16ab59b2e765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48cc4c53-2a2f-452d-a6ea-ae01abfbecef",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "e67ed91a-955d-4aa6-b4fe-44d530d132a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "4552860e-b199-4ad8-a1e2-87f2d48c6f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67ed91a-955d-4aa6-b4fe-44d530d132a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7e6ce2-0b79-4747-953a-996b1a6e3c1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67ed91a-955d-4aa6-b4fe-44d530d132a1",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "b5b11cf7-cf36-4a2c-89ea-86b8597d4511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b6b2f8fa-df87-4a2a-85c0-000bc65de209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b11cf7-cf36-4a2c-89ea-86b8597d4511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34cf150f-9968-4dfd-be58-115f9a7fdd74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b11cf7-cf36-4a2c-89ea-86b8597d4511",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "1421068c-12aa-429d-800c-4185de3e99c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "98c86b31-a775-416c-a5a8-cd8767c67f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1421068c-12aa-429d-800c-4185de3e99c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db4196ac-75a2-4fea-a1fc-147a55ce9601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1421068c-12aa-429d-800c-4185de3e99c5",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "7a7789ef-2e30-4a4d-be20-1ecbe3645add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "eb561ea8-1a90-4568-b1cd-6f7340ac4739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7789ef-2e30-4a4d-be20-1ecbe3645add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0237f4-c69b-4ffa-b6a6-091cee8e703b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7789ef-2e30-4a4d-be20-1ecbe3645add",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "2d039a61-8135-493d-95f2-ff082c67815c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "f44d9cd3-807c-4ede-9f7a-5857508f5c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d039a61-8135-493d-95f2-ff082c67815c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65269720-5037-43b9-94d7-411625167764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d039a61-8135-493d-95f2-ff082c67815c",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "7b40758f-5cc8-440f-9ab9-e0ddbf7fac99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "29a256cf-9ba8-4433-aaac-3d11efd927d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b40758f-5cc8-440f-9ab9-e0ddbf7fac99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4e415c7-4e95-4eca-a62c-8af1cdbbfd04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b40758f-5cc8-440f-9ab9-e0ddbf7fac99",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "05a4fbd7-f046-4db7-84a1-a98c826e24be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b220dbe7-d214-42f7-a0b1-2bd4113253ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a4fbd7-f046-4db7-84a1-a98c826e24be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4c6468-7818-43fc-939c-3eaefbcc3324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a4fbd7-f046-4db7-84a1-a98c826e24be",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "bb04de47-33b4-4ed8-a62d-35c3ab396d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "450d4646-1c32-4dc1-b89c-d279a5a85938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb04de47-33b4-4ed8-a62d-35c3ab396d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79a98b3-5448-483b-b65f-40a00113eab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb04de47-33b4-4ed8-a62d-35c3ab396d5b",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "bbcdd517-ab3a-4613-ba81-6e6db6f2ef2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "a0347343-7cbd-428a-905e-9a917ad8afec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbcdd517-ab3a-4613-ba81-6e6db6f2ef2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13e9d24-623a-4551-a2b7-8f457ca21b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbcdd517-ab3a-4613-ba81-6e6db6f2ef2b",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "374fefe0-bf62-4df3-b6aa-73b0850fe8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "d35ef10e-ac4f-4979-a010-cb7f40806e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374fefe0-bf62-4df3-b6aa-73b0850fe8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6093cfb6-f319-49c9-a67f-fa0fa8e2bc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374fefe0-bf62-4df3-b6aa-73b0850fe8b1",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "136a161b-4d92-440f-aeb1-55df8ce7dbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "276ff01c-ce89-4ced-be86-8f01bf377838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136a161b-4d92-440f-aeb1-55df8ce7dbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c03c579a-9fe3-4a43-81e8-809da66d7c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136a161b-4d92-440f-aeb1-55df8ce7dbef",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "111a170b-49fc-4df9-9f05-653e3215ffc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "bc55550b-4824-4751-9848-0844b8b26d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "111a170b-49fc-4df9-9f05-653e3215ffc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "936bea00-6fb4-489e-a295-ef589f8411d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "111a170b-49fc-4df9-9f05-653e3215ffc4",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "a4d94ccd-1a2d-4131-9da7-647b6da6fd34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b52e527d-d6d5-4e82-8fa2-7ea630b4a57c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d94ccd-1a2d-4131-9da7-647b6da6fd34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65510f5e-79e0-4120-b156-319ca72c14a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d94ccd-1a2d-4131-9da7-647b6da6fd34",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "539906dd-a24c-44d5-b268-58f82a070e0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "5dbffe53-5825-4336-b7af-f8a503f2166b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "539906dd-a24c-44d5-b268-58f82a070e0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d94d7f4-f70a-478a-8ea7-a92f26ee199d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "539906dd-a24c-44d5-b268-58f82a070e0e",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "5694ca9d-72d5-4455-b646-972e5dfc5185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b5658e12-9aa4-4873-8257-d66190dd5094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5694ca9d-72d5-4455-b646-972e5dfc5185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632a20ce-6740-4fb3-b5f5-5001b23e3588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5694ca9d-72d5-4455-b646-972e5dfc5185",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "c4400a2d-ce3f-43d3-9982-b1a7d91b9b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "885ee812-7dfb-4ad8-8a13-492ef761c863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4400a2d-ce3f-43d3-9982-b1a7d91b9b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "594f3ee1-a986-40ba-bab6-eed2a4923de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4400a2d-ce3f-43d3-9982-b1a7d91b9b1c",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "32647002-6b8b-4092-a389-1f7d74339a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "f2a1bec2-d6ae-40a2-86a0-c9416fe4cf99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32647002-6b8b-4092-a389-1f7d74339a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93760f8-a344-4be2-81f8-b5763fa4fca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32647002-6b8b-4092-a389-1f7d74339a61",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "0e9fc708-0ac1-4190-9dad-88183d7d9a2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "904bd77c-6a42-4802-95dd-bb55976144da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e9fc708-0ac1-4190-9dad-88183d7d9a2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0e8b72-de5a-40eb-aacd-0d74cf33a503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e9fc708-0ac1-4190-9dad-88183d7d9a2c",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "4bd9a847-9867-488b-ba4e-78f7c52725a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "38e0c610-2891-465c-900b-3ab84ac65373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd9a847-9867-488b-ba4e-78f7c52725a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "805b53a1-5695-49b4-b847-0ffa7a89c45a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd9a847-9867-488b-ba4e-78f7c52725a4",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "88d56b62-068f-4ecd-b0bc-8f0835dbcf6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "2d1a7a8b-2ede-46b6-a511-a835acddec0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d56b62-068f-4ecd-b0bc-8f0835dbcf6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f362b42c-000f-414c-b800-a8b5f0d1198c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d56b62-068f-4ecd-b0bc-8f0835dbcf6e",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "12dd2adf-f393-4a9c-b519-b95ac6174037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "6bee42ce-2b04-4055-bd72-dc87368db8e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12dd2adf-f393-4a9c-b519-b95ac6174037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7909b026-db0d-44b5-9121-2cc62d39ead2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12dd2adf-f393-4a9c-b519-b95ac6174037",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "0d785d7e-ebdf-4aa8-af05-16745fcc8fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "3f9822d5-8a8d-48b6-bdcd-7d0b4dba09bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d785d7e-ebdf-4aa8-af05-16745fcc8fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d10339ba-5e34-49b5-98fe-64c3bd4004aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d785d7e-ebdf-4aa8-af05-16745fcc8fd9",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "b0440eaf-946b-434b-8c44-2e75e5becd41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "e3e559be-e8b8-4b09-8148-779408f888ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0440eaf-946b-434b-8c44-2e75e5becd41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332a26a6-b849-4272-90c4-863a834628b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0440eaf-946b-434b-8c44-2e75e5becd41",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "1b2be1a9-8908-45d1-867d-5a63610a8ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "041ce5d5-6745-4009-a697-20ab62c16048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b2be1a9-8908-45d1-867d-5a63610a8ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc931fd4-d0c3-4eb7-8b5a-07ccfba3b0d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2be1a9-8908-45d1-867d-5a63610a8ee1",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "654fbc52-c5c2-40bf-9337-b613614ce009",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "128175b9-6e8b-4513-b8ac-0901a651b95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "654fbc52-c5c2-40bf-9337-b613614ce009",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1dabbb7-45bc-4f18-942d-47c2a0a90e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654fbc52-c5c2-40bf-9337-b613614ce009",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "61e75b5e-ad8e-4739-892f-013263ddf7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "52452cf8-efb0-4939-b4bb-8e2360a75e5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61e75b5e-ad8e-4739-892f-013263ddf7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548cb6e6-d611-45b5-a514-d61fec3e94c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61e75b5e-ad8e-4739-892f-013263ddf7b8",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "67287720-187e-4893-9e02-4e7a0849118e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "1ade5ca6-153e-4ebd-ab77-87a84728265a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67287720-187e-4893-9e02-4e7a0849118e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b291f09-7593-495b-a481-90767d2769a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67287720-187e-4893-9e02-4e7a0849118e",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "eef70804-d41b-4787-a895-d1edf224d388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "c96de207-53f6-4911-a01d-421314ba77aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef70804-d41b-4787-a895-d1edf224d388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c43d150-669e-46a3-b966-9fe0ec05274e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef70804-d41b-4787-a895-d1edf224d388",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "a9218609-27de-4c54-8811-25eaa0b9c4ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "15e024b5-e1a9-4ff4-9aa8-6cba585fa4f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9218609-27de-4c54-8811-25eaa0b9c4ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616975fe-014e-4083-a2a2-1474b6e2111a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9218609-27de-4c54-8811-25eaa0b9c4ba",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ca49d39b-bb4b-4cec-b2cd-5798a50dbab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "d5a6b884-96b4-46b7-8887-63d71a38833d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca49d39b-bb4b-4cec-b2cd-5798a50dbab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22022fca-45e3-445f-b92a-4461a2fbf330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca49d39b-bb4b-4cec-b2cd-5798a50dbab7",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "95b354cb-04fe-44ec-b5ea-ac3fabda4b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "1ff01932-c4fd-492c-b146-09d554fc2728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b354cb-04fe-44ec-b5ea-ac3fabda4b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9745c4cf-763f-400c-b69a-b3791e6271a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b354cb-04fe-44ec-b5ea-ac3fabda4b8b",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ff41476d-dd18-4987-9517-7ae02c353212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "6a023abb-19ec-4c3f-8717-86104adcc2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff41476d-dd18-4987-9517-7ae02c353212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feb1b807-24c1-49f2-a33d-7a6bc019e725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff41476d-dd18-4987-9517-7ae02c353212",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "3cc3c91c-298c-4874-846c-a02224a75e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "1eaa687a-602b-4efa-b75f-d357fe8c1cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc3c91c-298c-4874-846c-a02224a75e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c64e7a6-470e-491f-b8ab-344ede9858ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc3c91c-298c-4874-846c-a02224a75e3d",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ece19cc5-ec82-479c-ba51-14d639929ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "f1f6cac7-7afa-4a2f-bf84-f56829c106c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece19cc5-ec82-479c-ba51-14d639929ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76b93c4f-0e08-4c17-9817-452e00afd5bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece19cc5-ec82-479c-ba51-14d639929ef4",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "9534e2af-6886-4016-8434-d26628d73057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b815c666-2121-49b7-bcf1-e798991762a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9534e2af-6886-4016-8434-d26628d73057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f2e3282-f654-4d41-95fb-ae27f5b27b78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9534e2af-6886-4016-8434-d26628d73057",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "841f45df-cf50-42d0-abb0-59abe21331a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "1a0221e4-fdbd-4e25-87c4-051f1b9cae19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841f45df-cf50-42d0-abb0-59abe21331a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec528861-b31b-4000-b16f-b1b75004b9a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841f45df-cf50-42d0-abb0-59abe21331a6",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "6010694f-c6b3-4619-b8aa-e703b64df4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "18254b60-1620-4ced-a24b-4e6af4ef24d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6010694f-c6b3-4619-b8aa-e703b64df4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e05a5f-b712-4e27-ab13-1a4a185fa498",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6010694f-c6b3-4619-b8aa-e703b64df4ef",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "e7938db1-45b6-45ab-884c-3ab50a9887b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "46ec33e4-44f9-406f-b827-2be023450930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7938db1-45b6-45ab-884c-3ab50a9887b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ffc2a1-a1e6-4d9d-84bd-c98c6be117ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7938db1-45b6-45ab-884c-3ab50a9887b3",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f9c7d04e-c356-4967-bcf7-b5de8aa95583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "33365521-0589-41ea-927e-5fa3c8c1ccba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c7d04e-c356-4967-bcf7-b5de8aa95583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b9f128-2848-41d1-a9e8-8840ef55e628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c7d04e-c356-4967-bcf7-b5de8aa95583",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "2856c8cd-7668-4fd6-80e5-1bb2e1508b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "a8e1278d-2906-4ad8-a8a2-02a460ac36f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2856c8cd-7668-4fd6-80e5-1bb2e1508b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37e3f466-509f-4829-a985-81f6522f9658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2856c8cd-7668-4fd6-80e5-1bb2e1508b72",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "cbbfd14f-5180-4736-9898-fe248c6d4db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "3bed7ca1-16c3-4cd1-b96a-3755e7a972b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbbfd14f-5180-4736-9898-fe248c6d4db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16be8130-dc18-4da7-b633-9c641ea3e6f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbbfd14f-5180-4736-9898-fe248c6d4db8",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "57f45a22-728b-439e-abf1-36b91ab33278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "766126a9-2f88-4f09-bd3b-ef2639a2b0f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f45a22-728b-439e-abf1-36b91ab33278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46dc519e-4c73-47a8-98c4-a4afdc1b0819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f45a22-728b-439e-abf1-36b91ab33278",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "7a7aa566-10c5-49f5-b5bf-060765337c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "eb147ae0-77b2-468f-8bc1-cfdfd0c880eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7aa566-10c5-49f5-b5bf-060765337c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f759cd6-0a52-4aed-b52e-97c8acfa8154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7aa566-10c5-49f5-b5bf-060765337c9b",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "caaac2aa-2761-46fe-897a-88bdb396e2a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "236383a0-513e-43ce-9d97-f5a924da2a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caaac2aa-2761-46fe-897a-88bdb396e2a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61955fcc-ce58-4e4c-8c82-1e806d783593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caaac2aa-2761-46fe-897a-88bdb396e2a8",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "48cd6408-962c-4e28-a011-b31ac58befbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "bc05633d-dc34-45b9-bf80-e13ebd42eaeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48cd6408-962c-4e28-a011-b31ac58befbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6de8e6-24d5-46f7-86d6-1a205644ca59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48cd6408-962c-4e28-a011-b31ac58befbf",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "1de4c169-d620-48fb-a690-040c1bb2a6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "483db562-da57-4167-b55f-9bb8f3c4d1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de4c169-d620-48fb-a690-040c1bb2a6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc744e8c-8579-4511-aec5-2d66ad464c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de4c169-d620-48fb-a690-040c1bb2a6ec",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "907723b2-dbd2-4887-bc72-3b6046d2b396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "50985f2f-5188-4851-8f99-7f6e82c33c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907723b2-dbd2-4887-bc72-3b6046d2b396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cecaf362-7ada-49c1-8772-60a332ce7cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907723b2-dbd2-4887-bc72-3b6046d2b396",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "c86113ae-fd20-424a-af72-390a16f23957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "2cd43672-a479-4858-9bea-602de985c587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86113ae-fd20-424a-af72-390a16f23957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054b97f1-f9c3-4e76-a3bf-fd45b45559bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86113ae-fd20-424a-af72-390a16f23957",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "9761489e-d55d-4fe3-af68-e42854dae00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "828127e7-29e6-4d8b-865a-864d5ddb513c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9761489e-d55d-4fe3-af68-e42854dae00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02328223-0e86-4a82-aff1-81243e5cf657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9761489e-d55d-4fe3-af68-e42854dae00c",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "1f2f8c87-fd5a-4587-b809-9c1b9de900cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "8b1c409d-bde3-403c-8eeb-9f142cc73315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2f8c87-fd5a-4587-b809-9c1b9de900cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e98a913-420c-4a22-8c6f-4edec9b2369f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2f8c87-fd5a-4587-b809-9c1b9de900cc",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "8f1c89a3-63e0-4fe3-82e3-fd55b449ada6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "3e0ae7ae-a18d-42d9-8b7e-6674b2ddf779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1c89a3-63e0-4fe3-82e3-fd55b449ada6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9366c871-9e94-46d5-8315-a3524270b949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1c89a3-63e0-4fe3-82e3-fd55b449ada6",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "58b8a20a-1ec6-4a37-8590-fd06bb601ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "eaa41703-c7e8-4b15-8146-a463323c1864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b8a20a-1ec6-4a37-8590-fd06bb601ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa5734ec-3fb8-4107-bdba-d64877cfb524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b8a20a-1ec6-4a37-8590-fd06bb601ff5",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f1b44d5a-1233-4506-b1a7-d28ea16ef175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "314c4a9b-ed93-436a-9c4d-0c5c4526129b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b44d5a-1233-4506-b1a7-d28ea16ef175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e0b956-167d-4a21-9e4d-dd4b68edfbef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b44d5a-1233-4506-b1a7-d28ea16ef175",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ea3fe27f-aea1-4efd-bea3-295d2353fcc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "563208b0-2a5a-4e6f-9d9d-d0d9e9e98347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea3fe27f-aea1-4efd-bea3-295d2353fcc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047cc915-661b-4953-844a-88fc4ed4dce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea3fe27f-aea1-4efd-bea3-295d2353fcc6",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ed61eedf-e166-42d4-aaa2-7fda899f6f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "23e7981f-b5de-4b7e-97d3-e2f1c6685fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed61eedf-e166-42d4-aaa2-7fda899f6f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a567e5-4402-49f1-a290-93ad7e1b9afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed61eedf-e166-42d4-aaa2-7fda899f6f85",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "46687c21-f606-41ff-983e-527784cacc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "75597d9a-c712-4532-80b9-d9070e174c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46687c21-f606-41ff-983e-527784cacc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48086a66-10f5-46b3-923d-f4375510c388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46687c21-f606-41ff-983e-527784cacc5b",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "757dd296-4da0-447e-b8db-7c749e14fd24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "e2e122e3-912d-4c57-861c-afb9b337f936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757dd296-4da0-447e-b8db-7c749e14fd24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5619df71-74d3-46dd-ae94-532b8d288022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757dd296-4da0-447e-b8db-7c749e14fd24",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "851e1273-dfe4-43fd-9629-670ca77d84ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "ddfb1abd-5fbc-48ee-a460-4b08a717e0b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "851e1273-dfe4-43fd-9629-670ca77d84ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb119a01-b2ef-452a-99d1-92eef6f26d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "851e1273-dfe4-43fd-9629-670ca77d84ec",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "703e9115-71c2-4c62-b54b-587948c6e105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "2c96d5c9-1328-47ca-8c8c-fe38ddfa1b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703e9115-71c2-4c62-b54b-587948c6e105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21bcc5b-bc7e-4885-ad8c-460e750adf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703e9115-71c2-4c62-b54b-587948c6e105",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "cf8f4a86-4dab-4ba7-852e-4054dde649a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "82d4f6a5-b17f-4517-b735-809913bcd12a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf8f4a86-4dab-4ba7-852e-4054dde649a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c8ff6c-0f73-44ee-825b-3f7ce9397a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf8f4a86-4dab-4ba7-852e-4054dde649a8",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "057c5fbb-385d-4a59-a79f-305a757be281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "302faaec-a583-479a-96e1-f4a70d9d07f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057c5fbb-385d-4a59-a79f-305a757be281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15de313-b098-4a98-a239-ae0bf970296f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057c5fbb-385d-4a59-a79f-305a757be281",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f295702b-f2f0-44ea-be52-35eae2899683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "e104f3a1-181a-469e-af54-f23963e8d2e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f295702b-f2f0-44ea-be52-35eae2899683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5048ac1f-91db-46fc-9c87-c837414069f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f295702b-f2f0-44ea-be52-35eae2899683",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ca2db966-830c-4159-a7dd-8c43ea705289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "1430ce9e-7745-4b9b-ac21-15022e7c6f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2db966-830c-4159-a7dd-8c43ea705289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f548398d-f35e-4fdf-b6d0-44621fbe80e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2db966-830c-4159-a7dd-8c43ea705289",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "62a117c9-2cea-4ed2-a945-144261053b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "fd7f4a60-eb4e-4bbe-bd13-fa4e0939c358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a117c9-2cea-4ed2-a945-144261053b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dc33087-36eb-4203-9eba-621191e1b471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a117c9-2cea-4ed2-a945-144261053b78",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "8beb37d0-990e-47a9-af70-72fb61dd0d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "21dcb25c-73a8-4f2d-b7aa-ee1e78867ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8beb37d0-990e-47a9-af70-72fb61dd0d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4218ceb2-da23-4b70-803b-b0f1f1dadfcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8beb37d0-990e-47a9-af70-72fb61dd0d74",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "5b14f054-68c9-4fb5-8856-475aa8d27016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "e3cf354b-c98a-47ca-8c1f-0e87e8534c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b14f054-68c9-4fb5-8856-475aa8d27016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcfccab4-7868-49c7-a2b7-fc9563d12e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b14f054-68c9-4fb5-8856-475aa8d27016",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "6000697a-a61a-4581-aeb0-57509626adcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "2a4e8efd-d5df-4891-9f63-a3600075c764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6000697a-a61a-4581-aeb0-57509626adcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c09111-16ca-4235-85a1-5acf0e97da7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6000697a-a61a-4581-aeb0-57509626adcb",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "348f466d-208d-43f1-b5d0-d7d767bea746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "72a29a85-5bb7-4eec-97ea-fa59825aa83b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "348f466d-208d-43f1-b5d0-d7d767bea746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06661026-1351-4d54-9ff5-046efa312963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "348f466d-208d-43f1-b5d0-d7d767bea746",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "c2b2cb0a-83d7-4413-96f8-7c52513ddd61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b1715935-6677-4c76-a856-48d64b35ed75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b2cb0a-83d7-4413-96f8-7c52513ddd61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca493a3d-bff0-474d-a903-af202e7d8931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b2cb0a-83d7-4413-96f8-7c52513ddd61",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "88cb941d-028e-4031-a316-bfd382b565c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "8e2b1fe5-01aa-4fc1-ac38-01ef209f7214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cb941d-028e-4031-a316-bfd382b565c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0502aa1-0cd9-421e-a1c3-49d9217f17f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cb941d-028e-4031-a316-bfd382b565c6",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "45458ead-965f-4e4c-8584-3e3ab29d55f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "32f986dc-a29f-4bab-a992-c367d2151405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45458ead-965f-4e4c-8584-3e3ab29d55f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c2bcac-929e-4d40-9007-b75516730bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45458ead-965f-4e4c-8584-3e3ab29d55f8",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "38e3e5f5-9adf-4cbc-8dad-91a3491c06f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b115797e-70e3-42e0-8965-22f6c95de209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38e3e5f5-9adf-4cbc-8dad-91a3491c06f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b131e8d7-e71f-4911-915a-23211a8fd418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38e3e5f5-9adf-4cbc-8dad-91a3491c06f5",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f5c7a8cf-0db9-4db6-9ed0-7e893730ff4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "f564e6b9-f7d0-4799-98b4-36ee62f0344b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5c7a8cf-0db9-4db6-9ed0-7e893730ff4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f1f3141-504d-418b-a982-cd093c63de23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5c7a8cf-0db9-4db6-9ed0-7e893730ff4d",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "42c99f53-0173-4cd0-a698-df159ad91d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "debc7fea-d9c7-4506-89e7-9b7d7e3c2b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42c99f53-0173-4cd0-a698-df159ad91d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a615af8-f128-4e47-ab30-2d38da408954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42c99f53-0173-4cd0-a698-df159ad91d3a",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "a201b4a4-cbab-488a-b5a8-25b1974ea22b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "20f711f8-58cc-4cbf-96b7-871289b70d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a201b4a4-cbab-488a-b5a8-25b1974ea22b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8fc3754-8f82-4db8-bf59-8cf7626fe32b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a201b4a4-cbab-488a-b5a8-25b1974ea22b",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f1bb5d6d-d0f6-4c17-bafc-1210a988f34f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "bd680c83-f074-4132-b7e7-a50a2e1faa0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1bb5d6d-d0f6-4c17-bafc-1210a988f34f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0098b037-69e5-429c-8826-4e2c67ebb4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1bb5d6d-d0f6-4c17-bafc-1210a988f34f",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "69fbcb3b-ff06-40de-bcfe-d4999562d503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "37258745-fd00-4836-bff9-f6e0e69a6fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69fbcb3b-ff06-40de-bcfe-d4999562d503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af0b4a7-dd06-4e09-974c-f01f83c2ebd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69fbcb3b-ff06-40de-bcfe-d4999562d503",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "9f3f7500-cc94-4c1c-93a8-229d16f4dc77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "f9b011f4-be3f-4374-8b68-3ed5f2bfd1f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3f7500-cc94-4c1c-93a8-229d16f4dc77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e5f00b-19e6-4384-a2c6-94e8e2abd14b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3f7500-cc94-4c1c-93a8-229d16f4dc77",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "a1174612-0774-4dac-a730-8021bbc1d60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "ee295051-c776-4146-8950-95ed8d833845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1174612-0774-4dac-a730-8021bbc1d60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a5b396d-3fa2-4461-a216-f27a2567b5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1174612-0774-4dac-a730-8021bbc1d60f",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "3829624b-5ff7-417b-a953-d0ec0a2f4d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "8ae52909-725b-41f5-af5a-a696221cf1c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3829624b-5ff7-417b-a953-d0ec0a2f4d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd142701-460c-4646-8ab4-827a798238a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3829624b-5ff7-417b-a953-d0ec0a2f4d59",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "de3ee0d8-4128-44d4-bf21-6f004967b980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "7b3dc41e-3803-4e38-b4db-b8b98a752e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de3ee0d8-4128-44d4-bf21-6f004967b980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7479a771-9e06-4aaa-8821-2eda6afb6153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de3ee0d8-4128-44d4-bf21-6f004967b980",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "c1f85213-4b06-4468-9a74-660663a63e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "78a3b764-4261-4a32-b89c-36d4adb4a234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f85213-4b06-4468-9a74-660663a63e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c11ed2-5028-41f3-ac67-d05751cf7f7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f85213-4b06-4468-9a74-660663a63e00",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "88cb6db0-225c-4ac8-bb13-245838ac3898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "75a992a6-77c6-42da-afc3-fc1aa289dbca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cb6db0-225c-4ac8-bb13-245838ac3898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d932e812-78cd-4c13-8e1d-fe4a1e38bc5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cb6db0-225c-4ac8-bb13-245838ac3898",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "5bb3d4aa-cae9-4668-971a-93475d998c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "3fd623e8-ccef-4625-9f43-6713ff3d3939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bb3d4aa-cae9-4668-971a-93475d998c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82cd30e0-22a9-447b-a788-11b90fa2b39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bb3d4aa-cae9-4668-971a-93475d998c4c",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "6c522fd7-9b1e-4c19-9103-00b2ba9bdd73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "bd739296-d19e-47a4-bf9b-73907e5e636e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c522fd7-9b1e-4c19-9103-00b2ba9bdd73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46863b10-5a44-4b91-97bf-fdc40d16d9c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c522fd7-9b1e-4c19-9103-00b2ba9bdd73",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "0025bee8-efbe-4789-8ccb-b015f75d7d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "cc5aafac-0699-4972-9ee0-8008a32b43dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0025bee8-efbe-4789-8ccb-b015f75d7d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782f12b2-ac15-42f5-bd5c-baf501634313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0025bee8-efbe-4789-8ccb-b015f75d7d4c",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "b26d36ef-6eb7-4d55-b46b-0123dceeb8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "2b1c02c6-2367-4fa8-9190-8a75048eaf1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b26d36ef-6eb7-4d55-b46b-0123dceeb8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1253d9-86d7-4ffc-865a-dd1e6b663ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b26d36ef-6eb7-4d55-b46b-0123dceeb8ca",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "d73be062-13cd-45fe-857e-efb05f794845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "1ea447cf-f44c-4592-bd73-7ed27d32276e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73be062-13cd-45fe-857e-efb05f794845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7276b268-4824-46d9-ac34-909f46922b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73be062-13cd-45fe-857e-efb05f794845",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "169c2e8f-6a5b-47f7-849c-d3d6d1a2699d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "15eeb7bc-b83c-42b9-bf5d-14eca9f4fefd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "169c2e8f-6a5b-47f7-849c-d3d6d1a2699d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9adacac4-08a9-495a-b017-41eb1a31da44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "169c2e8f-6a5b-47f7-849c-d3d6d1a2699d",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "916efc23-625c-4535-bd9c-fcaf23dfb515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "917898cc-4dc9-4daa-9138-3baafb32330d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916efc23-625c-4535-bd9c-fcaf23dfb515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e13c0f8-6583-486b-a6de-9f9f37c3fb4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916efc23-625c-4535-bd9c-fcaf23dfb515",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "6d8496f6-918d-43c1-84ca-df3be72767bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "b8655860-fe08-4de2-be03-f7a5e1269ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8496f6-918d-43c1-84ca-df3be72767bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9243d5a7-0075-4f3b-8b77-b6167f342ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8496f6-918d-43c1-84ca-df3be72767bd",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "92b32221-43ed-4bb8-9924-7af13068a34e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "6cbb51cb-323b-453f-81a7-4f4db7047ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b32221-43ed-4bb8-9924-7af13068a34e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae2b80c5-d1f4-4679-8a05-e43c22eec5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b32221-43ed-4bb8-9924-7af13068a34e",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "222ec203-fa6b-4d2e-b214-dfe2d103ed55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "84455efc-0fd6-467c-8db4-c689b1625b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "222ec203-fa6b-4d2e-b214-dfe2d103ed55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed1f763d-5086-4aab-93c1-87fd4458aed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "222ec203-fa6b-4d2e-b214-dfe2d103ed55",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "991eda66-779b-431c-aef0-91d3f194db37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "4d7b536c-d46d-42ea-95a5-2e9289972132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "991eda66-779b-431c-aef0-91d3f194db37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f045ed2-3a3b-495e-b97d-1789234a12b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "991eda66-779b-431c-aef0-91d3f194db37",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f70bfb3f-468c-4d8c-b73a-4b60f807f8a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "c6ca8037-db49-42db-88d8-a05bdcd8c75e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f70bfb3f-468c-4d8c-b73a-4b60f807f8a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11c5c9f-3de4-455a-9dd7-940bf91ba0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f70bfb3f-468c-4d8c-b73a-4b60f807f8a4",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "46da3a99-cfeb-41c6-9951-3464c224dbd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "80e71958-b4ea-43f1-8fad-2946c41144df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46da3a99-cfeb-41c6-9951-3464c224dbd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521ed365-bd43-4cea-9c3a-9d54b8b4fe5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46da3a99-cfeb-41c6-9951-3464c224dbd3",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "ea977359-4a5d-4421-be62-7085c4b279b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "3506e666-274b-4e5e-89e6-13d14839b0e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea977359-4a5d-4421-be62-7085c4b279b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5136927c-5cb0-40fb-b73d-2c2a17b8f3f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea977359-4a5d-4421-be62-7085c4b279b6",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        },
        {
            "id": "f1d61abc-ec55-46fa-81e7-e9c6d96f8413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "compositeImage": {
                "id": "6bddf26c-1812-4865-81ef-1a1c32ebbe75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d61abc-ec55-46fa-81e7-e9c6d96f8413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "565d792a-7753-4504-b099-c52f9ef9cf4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d61abc-ec55-46fa-81e7-e9c6d96f8413",
                    "LayerId": "fc7c3cf5-6b84-45f7-844f-77ac1c175682"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "fc7c3cf5-6b84-45f7-844f-77ac1c175682",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3095ae5-5c72-4f4d-9045-a9ae450ab70b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 450,
    "xorig": 0,
    "yorig": 0
}