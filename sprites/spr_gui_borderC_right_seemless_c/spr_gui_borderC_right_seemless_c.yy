{
    "id": "df62c2ff-66e9-42ba-af6e-e14a196c3340",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_borderC_right_seemless_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7efca09b-cf36-4b46-9e63-c529c1945a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df62c2ff-66e9-42ba-af6e-e14a196c3340",
            "compositeImage": {
                "id": "ad18c2eb-49f2-4d3d-b7dd-a75408b5ce2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7efca09b-cf36-4b46-9e63-c529c1945a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61aa5080-5406-46eb-80ab-b074f0cc9d6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7efca09b-cf36-4b46-9e63-c529c1945a16",
                    "LayerId": "d1440dba-3687-40b9-953a-c6695bd8d5a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "d1440dba-3687-40b9-953a-c6695bd8d5a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df62c2ff-66e9-42ba-af6e-e14a196c3340",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 2,
    "yorig": 19
}