{
    "id": "75cf4d54-b0aa-472e-8105-ef333b69c361",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_occult_part",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 509,
    "bbox_left": 29,
    "bbox_right": 509,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0c363e8-d984-4618-9e4a-b0e1f691c473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75cf4d54-b0aa-472e-8105-ef333b69c361",
            "compositeImage": {
                "id": "c8ba7d52-d905-41fc-bc74-612e58621a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c363e8-d984-4618-9e4a-b0e1f691c473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383c09be-4e93-4af8-81cd-90f8f46ef78f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c363e8-d984-4618-9e4a-b0e1f691c473",
                    "LayerId": "60ee5602-5008-473c-9ee2-ef407a790a72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "60ee5602-5008-473c-9ee2-ef407a790a72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75cf4d54-b0aa-472e-8105-ef333b69c361",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 540,
    "xorig": 270,
    "yorig": 270
}