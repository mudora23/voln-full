{
    "id": "8855848d-3dfb-4759-995d-a6e47c3732eb",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01n_bold",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "96f7f04b-6246-4ce0-b06f-0e4583b50c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 396,
                "y": 242
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1df1bfa3-d544-43a3-bc53-3b9b0f380817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 72,
                "y": 290
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "69b3362c-9492-433d-83d3-7908a6381c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 81,
                "y": 290
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2bc104fa-c751-43e9-9536-ab5320721554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 224,
                "y": 194
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6bebc81e-7dc3-4bdf-aa44-b16a52395d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 40,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 108,
                "y": 194
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6905cfae-f802-48ca-80e0-91670c8e51ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 28,
                "y": 146
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e644e86a-e247-4e43-b48c-751c1966728c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 427,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f2e458e7-f197-425b-bd6e-e89006a1cc03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 175,
                "y": 290
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6d64bfd2-8ac6-4b06-89a8-931262be1da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 282,
                "y": 242
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "72d069b5-6df0-43d5-acfe-a08e40b1df7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 116,
                "y": 242
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "dbfcfcb1-a6e1-4f56-b925-ed99d77958c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 94,
                "y": 290
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "24fe999c-181c-4280-bfa3-4f6a91ce6be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 454,
                "y": 194
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1fe8697a-4ee8-45d5-af20-f2bb0c9c59de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 43,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 442,
                "y": 242
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aa4c7c70-a669-461f-91c2-feedef09d661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 452,
                "y": 242
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "95816aa0-8ca0-4e4c-8fbe-5d2ab24abdf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 54,
                "y": 290
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1b647409-2a3b-4f29-a486-b0d48a8dbd14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 336,
                "y": 242
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "03151d62-f8bf-435f-a45e-8922bbe617bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 45,
                "y": 194
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ce46c932-bc55-45cf-946c-834e4a118df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 416,
                "y": 194
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dbc60b48-074c-4e6b-a198-e237c1c7852a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 305,
                "y": 194
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9e9b2ae2-89ea-4aca-af60-99fa2d4cdd60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 265,
                "y": 194
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c4f8a6cb-82db-4251-a148-389e67df4bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 471,
                "y": 146
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bdd53590-edac-4972-8495-5388398be051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 21,
                "y": 242
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b5e2ed35-85ed-4370-9c72-9dcd7093ebd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 24,
                "y": 194
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "31d92ddf-cde4-49ab-afc9-63a1cb0fcda0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 285,
                "y": 194
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5602e35f-1d39-4268-91d3-26123bbb1afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 66,
                "y": 194
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5f159c78-9d41-4ae8-87c8-4bdafb393a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 87,
                "y": 194
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "71557ee4-b778-4752-abad-bd859a929a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 63,
                "y": 290
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b0856133-c902-432d-93fa-26c4de6d55e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 43,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 422,
                "y": 242
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "35e43dbb-07c9-4ba9-9afd-82f1cf528618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 474,
                "y": 194
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b18f40ca-4750-4073-8035-2ee6860c59a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 178,
                "y": 242
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a9322ae5-6ca5-4898-845f-eaa1687cb0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 396,
                "y": 194
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "32aa83d8-a3c4-4f46-be44-0282d14a63ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 59,
                "y": 242
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "062a7cd3-7e7a-46b2-ba8d-6a5694f38b6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 42,
                "offset": 3,
                "shift": 40,
                "w": 33,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6782a398-02df-4fc3-b768-8cd15059170d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 413,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0e50004e-9309-4c7c-be0f-ba87cafc7f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 366,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "220b3d07-2dd8-4f80-b9bc-cacd3711cb91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 99,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b212381d-252c-4ce2-ab52-445c6bb71c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 368,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3eb650f1-4dc3-46dd-9f83-a5ff1fa69ec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 100,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c3697cb3-d84d-4248-898e-62d8290a84e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 267,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "809d48f9-0508-414e-bf24-054baab591be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9a7b260c-2344-49ed-ac94-a65347f66ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 0,
                "shift": 33,
                "w": 32,
                "x": 280,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8d72a63b-9133-4e17-a85b-5498e7877640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 250,
                "y": 242
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "29fc1f4d-5f04-4451-96c8-eaa5dcff109b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 43,
                "offset": -3,
                "shift": 15,
                "w": 18,
                "x": 310,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bebe9e0b-b4ee-4909-baa5-49bdd3077fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 0,
                "shift": 28,
                "w": 30,
                "x": 66,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "426dc799-808f-46b5-b277-07913396b0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 75,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1b43cb0f-6558-4262-a00c-e5fbcf10c2ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6b903c20-ca8f-4680-9ad7-8cf64087cb9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 191,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "30457917-5504-43bb-8419-226a0382d399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 398,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "130c381b-1ed1-421c-9cf2-cfb2f74a55a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eb2d7d40-5233-4c4a-b4a9-6c53adc7c16f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bc2a5891-083c-4b7a-9b83-10fa45135681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 222,
                "y": 50
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "af8c6dc5-df7a-4346-84c6-07bea04c7091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 184,
                "y": 194
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "da123b7a-e5c0-41ae-9b28-bbac8dc22eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 127,
                "y": 98
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a56d5604-b809-49fd-ab20-7f93f4651963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8dbd0f37-3ad2-44d3-9398-cc14e7e69634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 253,
                "y": 50
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a541ffba-c45f-492e-9ebe-298162beb7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "24c957fd-e67c-4ae0-8691-53c231d8d0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 160,
                "y": 50
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "16ab2394-5936-4dd9-a8a9-1e839101fae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 263,
                "y": 98
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0ea363a1-02f7-4d6c-8960-23391e05ff5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 182,
                "y": 98
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5d89d7cb-db1b-4a4a-8e96-b8f9fa53aee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 44,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 310,
                "y": 242
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7abeda11-3db3-4903-985e-9d617af46622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 295,
                "y": 242
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7ccb0215-6955-4081-ad57-d9dac1495044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 44,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 323,
                "y": 242
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7622cae9-dca7-4f79-926f-8fb3c2aa8919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 466,
                "y": 242
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "781df436-9f43-453e-b7f7-7d844f40224e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 53,
                "y": 146
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "539074c9-e7b0-4b79-a366-856147039199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 156,
                "y": 290
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c0efb38e-f79f-4b7e-b75c-50a6ba560abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 383,
                "y": 146
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ff53bea8-48bc-4303-854b-cff943775020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": -1,
                "shift": 22,
                "w": 22,
                "x": 175,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1868a029-d717-4cf3-933d-34b97ed6aef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 77,
                "y": 242
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c87ebb74-5999-42eb-ae73-e150810a1118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 219,
                "y": 146
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1b401903-9495-4164-985d-5809b0e08fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eb5362ea-77cd-42be-90ea-70e0522884b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 18,
                "x": 245,
                "y": 194
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "61ac3a2c-b77b-4288-8f20-77ccef8236e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "226af4c8-7f69-4449-a3b7-bc94e81b689b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 290,
                "y": 98
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6bd7547d-f34c-44f7-a024-37455b23acee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 408,
                "y": 242
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "628951de-c442-46da-9dd7-628684394ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": -4,
                "shift": 12,
                "w": 14,
                "x": 342,
                "y": 194
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "17744224-b50f-4e90-a3ab-e5cb9a9b75c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 155,
                "y": 98
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "67f97b8d-bd5e-40cb-9476-0f44a7d21a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 351,
                "y": 242
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5f6b9fdc-79ce-4bd4-9027-505b1f4801d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bfc53357-fbcf-4163-b8ed-8a20c0787912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 236,
                "y": 98
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "92bc5b39-2136-4d8e-97b6-929c3b8d859c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 449,
                "y": 146
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "de8ffd86-f272-4d8d-840a-b2b6a5b28d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 456,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "31ceec97-0e0c-46cf-9112-7930ddde2071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 480,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "898eec94-9a07-4cce-a715-ab82f483ec1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 40,
                "y": 242
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c1a4a3d0-cea4-4cb6-a04f-173d628bf4b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 242
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "dda9d0f5-2ffe-4875-adff-19d72d1e7807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 234,
                "y": 242
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2b34cc84-5aee-490a-a84e-6db1a8aa61c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 393,
                "y": 98
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8b2b83e8-6342-4c7b-82c7-d16633e7b35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 360,
                "y": 146
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "35246575-b14a-4f91-9546-8ae72a196a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a010f57f-56c6-4bd0-97cf-b2038bfe14a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 243,
                "y": 146
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "16afe62f-dd9e-4fcb-bd61-5377a39f6952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 283,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d1ca1538-21fa-4854-8183-127c97ef1a4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 144,
                "y": 194
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "21e3ea2e-bee2-48df-be11-c45ad190e357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 164,
                "y": 242
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "45829574-cb4a-4910-a007-9f3cc4f9c023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 142,
                "y": 290
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e380e854-1413-4051-a2f9-adb660546fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 44,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 220,
                "y": 242
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "533b9121-4c3c-4c7f-9646-4aa21a741de9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 95,
                "y": 242
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "e51a009d-e826-48b4-ae5d-c1c55339de4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "e6871bc2-038f-4e2b-9342-3ca460c835ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "89a697e9-80ff-479e-a732-b3caa2500a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 266,
                "y": 242
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "39dad538-01f9-4f01-9ee3-1b9fc7a08d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 338,
                "y": 50
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "9823be58-912b-4fc6-832a-34990032add6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 34,
                "y": 50
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "1b2ff08d-70b6-48ac-9886-a4eb756f2286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "220332ab-2788-4f9f-a7a4-cd17f6481396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 471,
                "y": 98
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "da548dc4-90fb-43a6-bab4-5bed89dd8ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 405,
                "y": 146
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "f40886d8-33a6-4432-9d0b-fd3eed17b9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 427,
                "y": 146
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "22744ad9-a72a-4743-a856-662deb4c2f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 381,
                "y": 242
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "f55c1c23-40ff-4b41-8e31-183b9205ee89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "ee922d7f-183d-4bc8-a420-2aa60f8fa24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 445,
                "y": 98
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "67638130-37cf-45f4-96c3-e411978a6cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 419,
                "y": 98
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "8d856324-b39c-4d2b-b001-569f7873ea6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 45,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 48,
                "y": 98
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "e09c4519-366f-40dc-88a9-aeb01204341a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 45,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "a1622ccf-fd30-4747-9b1c-1a9329fa0939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 344,
                "y": 98
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "eb5051fb-e2e4-4f4d-af6d-295b8a730466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 150,
                "y": 146
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "fed5964b-a78f-4b8b-9327-103f7c192bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 358,
                "y": 194
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "9417162d-dd0b-4ce4-9c8f-320ffef99044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 36,
                "offset": 0,
                "shift": 33,
                "w": 32,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "3d59772a-c50d-4f05-b863-f88cb595afc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 317,
                "y": 98
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "1f6bff4a-5eca-4158-875d-11c7cc0c5d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 125,
                "y": 146
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "1244d113-0c7a-47aa-af9b-d281f581bd99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 366,
                "y": 242
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "479cecef-20d4-4597-97dc-2ca727feaf6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 43,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "f23c828c-8f8a-47e7-9846-f73c62a0fd56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 45,
                "offset": 0,
                "shift": 25,
                "w": 21,
                "x": 25,
                "y": 98
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "9b4b4ee9-ec54-4276-b708-7442ef841070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 36,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "aa7d2283-c2a2-41d0-8e54-f0aff76c3f70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 377,
                "y": 194
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "6cd4e28a-3421-4232-ab56-979095b85769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "76587992-640b-44ae-9807-d70cf8805170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 46,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 291,
                "y": 146
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "0ae3f46a-e634-4c02-81a6-cd044dcd8553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 199,
                "y": 146
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "d880dea0-d883-4266-93e0-c82eb423e597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 127,
                "y": 194
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "59428d90-7db9-4419-ac9b-a6e1118ae1c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 209,
                "y": 98
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "6483d088-3442-4008-ba5c-7f6b894f8820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 164,
                "y": 194
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "7aa11b23-78ba-445a-8259-70834cd1f5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 36,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 71,
                "y": 98
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "757cb487-fc2b-42e0-8bd7-54741ab707e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 435,
                "y": 194
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "bfd40a5b-377f-48c2-9c21-08e2384748b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 204,
                "y": 194
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "04d16b15-f409-4a66-b971-9ce82cae5c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 147,
                "y": 242
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "23d1b484-0334-424f-ab99-c4f12a911760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 27,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 198,
                "y": 242
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "0a8f3a78-79b1-4e9a-b2a3-0b0653d0f9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 27,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 330,
                "y": 146
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "d27db025-d163-4cd4-86ef-4749b2d946d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 132,
                "y": 290
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "c94e094b-75ad-4939-ae5b-9e83761806a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 122,
                "y": 290
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "088cd57d-b9f5-4ac0-a395-f08d7babfc66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 43,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 432,
                "y": 242
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "3ac807d2-6b8c-4c68-868b-78ddda2c5220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 147,
                "y": 290
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "59f8d989-c347-4b8f-b7d7-6cf1fd8e90fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 18,
                "y": 290
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "b7556c29-5deb-44df-9b22-b02ac74dd0a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 481,
                "y": 242
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "53da3866-e0e0-4d7c-a897-976509e370ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 43,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 325,
                "y": 194
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "9d028c88-4992-4fd0-955a-e5eb34590e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 290
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "24d7e9d5-2002-4ec2-8625-8a59e717224c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 36,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 308,
                "y": 50
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "07574e6f-281e-42a9-87e3-0d01737d5a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 15,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 166,
                "y": 290
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "e0029c63-de3d-4f43-a256-e8b59de31762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 108,
                "y": 290
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "c9f621e6-0574-486d-990c-b94608202662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 15,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 35,
                "y": 290
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 30,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}