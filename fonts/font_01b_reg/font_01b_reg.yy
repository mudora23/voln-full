{
    "id": "ac16c1fe-aa5e-4e19-bbd2-b233cf67e082",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01b_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f4cb46ca-6fec-47e3-adf1-bb69b33143b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 91,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 561,
                "y": 374
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5c6657f2-96a1-4877-a13a-f05de2135514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 73,
                "offset": 7,
                "shift": 23,
                "w": 9,
                "x": 247,
                "y": 467
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a1ad00f2-e175-4a23-abbc-5d14e9b7e52b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 5,
                "shift": 27,
                "w": 18,
                "x": 227,
                "y": 467
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9dc58d78-e371-4e0e-9751-984e2ba92c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 71,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 643,
                "y": 281
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8d84c165-9cca-4f8e-88ed-84405adc26a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 80,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 397,
                "y": 281
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "66ebd612-4d4c-46d7-a3f5-b3ba67d5c2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 73,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 175,
                "y": 188
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "eba46d5b-bc6a-413f-8d8d-ebfcf54e0808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 73,
                "offset": 3,
                "shift": 56,
                "w": 51,
                "x": 437,
                "y": 95
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3628fb24-262b-43db-8113-c6c15f0f562d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 4,
                "shift": 15,
                "w": 8,
                "x": 334,
                "y": 467
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ce1ea0bf-2388-4f83-a16d-58f7b696ff9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 89,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 639,
                "y": 374
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "44bcbbad-5ee2-4e73-b30a-6b1febbc8065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 89,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 661,
                "y": 374
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "88f5de9c-b6af-4d1f-98d4-c96d010d21bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 76,
                "y": 467
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "506f3a29-d7dc-4921-a8f0-c7130fae5f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 68,
                "offset": 4,
                "shift": 44,
                "w": 36,
                "x": 605,
                "y": 281
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "139cab4c-c7bb-45b6-b833-5a43e14a4e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 83,
                "offset": 3,
                "shift": 18,
                "w": 11,
                "x": 63,
                "y": 467
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7aae4212-b900-4be2-9a69-c35bd536513d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 54,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 963,
                "y": 374
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f2ea2cf8-3ad6-4dd3-9c87-8c7ed1396336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 73,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 203,
                "y": 467
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "89e5c356-6d2d-4de5-a526-23333a7f956e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 683,
                "y": 374
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3047bebe-0952-4ad8-8d1c-b284d42e9cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 73,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 840,
                "y": 281
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2d65f808-b586-4e96-bfa0-c5ddb74a5dee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 72,
                "offset": 7,
                "shift": 37,
                "w": 24,
                "x": 733,
                "y": 374
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1c2c692c-39a1-4ec5-9048-968c01b16ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 73,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 241,
                "y": 374
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c37ff77d-8c86-4c1a-9371-567c7222012f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 73,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 906,
                "y": 281
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b7ff2578-36f5-4af2-af65-1567e34d6653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 72,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 569,
                "y": 281
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "48fefcac-3e29-4e22-a302-b685ec1cd41b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 73,
                "offset": 4,
                "shift": 37,
                "w": 29,
                "x": 303,
                "y": 374
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ea999961-361b-40da-b752-673de7ff1546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 73,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 972,
                "y": 281
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e8554f4a-1d31-40d5-91af-29ede67c1854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 73,
                "offset": 4,
                "shift": 37,
                "w": 30,
                "x": 145,
                "y": 374
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4ac01d8c-f576-452f-99a5-dc8aa606fc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 73,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 939,
                "y": 281
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "04d0b7b1-5f5b-4146-bbc2-f3397ed1aa17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 73,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 679,
                "y": 281
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cf483274-da6a-4b5a-8a44-c52c7029e1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 70,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 215,
                "y": 467
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c8953987-3cce-43f4-9f65-41c37483ac47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 83,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 36,
                "y": 467
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4b0e6866-d70a-4049-abc3-9536526f504a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 66,
                "offset": 4,
                "shift": 44,
                "w": 34,
                "x": 38,
                "y": 374
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "11317a71-9b17-4c7c-86a8-ffec9218d314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 61,
                "offset": 4,
                "shift": 44,
                "w": 36,
                "x": 107,
                "y": 374
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2865228d-b1a5-43a1-b37a-974b87305553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 66,
                "offset": 6,
                "shift": 44,
                "w": 34,
                "x": 2,
                "y": 374
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f5eb996b-e5e3-4a53-b030-20b6ee04be21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 73,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 272,
                "y": 374
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "74bd7d81-780b-4ccd-8a3f-26149816a45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 85,
                "offset": 4,
                "shift": 72,
                "w": 64,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cb6bfa60-ac42-48d8-ade0-9fc7f4258877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 72,
                "offset": 0,
                "shift": 56,
                "w": 56,
                "x": 729,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8cfa0f46-af2f-4788-b2d9-c8a33a5894c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 72,
                "offset": 1,
                "shift": 47,
                "w": 43,
                "x": 267,
                "y": 188
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5074413a-f91b-42aa-9093-9cc2bd34d8e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 73,
                "offset": 2,
                "shift": 52,
                "w": 47,
                "x": 857,
                "y": 95
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3b52a691-1eb8-42f9-85b2-e6b9b1e888d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 72,
                "offset": 1,
                "shift": 56,
                "w": 52,
                "x": 383,
                "y": 95
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "cfd24ac9-e73a-45c6-ad7c-124536a9f583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 72,
                "offset": 0,
                "shift": 44,
                "w": 42,
                "x": 654,
                "y": 188
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "434ae979-66f0-4e3f-a50d-01e5cfac752c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 72,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 137,
                "y": 281
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f811b752-7017-4a7c-8f7c-3f46868b42b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 73,
                "offset": 2,
                "shift": 56,
                "w": 53,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d09fe60a-54dd-455a-b185-8958179d63f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 72,
                "offset": 1,
                "shift": 58,
                "w": 56,
                "x": 671,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fb8f24fd-46f9-4d34-9610-0c301410fcc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 72,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 810,
                "y": 374
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "85c49bfc-e6e9-47f4-9a6e-a7d132976871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 86,
                "offset": -6,
                "shift": 26,
                "w": 31,
                "x": 177,
                "y": 281
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d71cb252-f03a-4235-81d7-b49bd116681c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 72,
                "offset": 1,
                "shift": 51,
                "w": 50,
                "x": 533,
                "y": 95
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b229d71d-7177-4162-9bdc-6b21f02b305f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 72,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 863,
                "y": 188
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0c93ee10-552b-4d56-a46b-0592724ff0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 73,
                "offset": 1,
                "shift": 67,
                "w": 65,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "af7d9b00-a8d5-46c3-9b86-4326df38abcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 73,
                "offset": 0,
                "shift": 56,
                "w": 56,
                "x": 555,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "871dc158-7660-4900-94a6-13a2b6d66258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 73,
                "offset": 2,
                "shift": 56,
                "w": 52,
                "x": 275,
                "y": 95
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "42f25efb-569f-43fb-b7ca-a53ffb8a41fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 72,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 734,
                "y": 188
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d646eb1b-1128-47fd-aa4a-b593a6b2d980",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 89,
                "offset": 2,
                "shift": 56,
                "w": 53,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e5eaac87-6cef-4fa5-b7b8-8ed1321d59a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 73,
                "offset": 0,
                "shift": 47,
                "w": 48,
                "x": 717,
                "y": 95
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "da74b6d3-634a-4c9c-baa4-8f82de47d6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 73,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 461,
                "y": 281
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6026b445-2179-4506-93a2-e1d42487a2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 72,
                "offset": 0,
                "shift": 48,
                "w": 48,
                "x": 807,
                "y": 95
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "908af858-6f72-4444-a622-60e7b1df8bd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 73,
                "offset": 0,
                "shift": 53,
                "w": 53,
                "x": 903,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "de709fc8-ea92-48fe-bddd-e1a165d09105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 73,
                "offset": 0,
                "shift": 52,
                "w": 52,
                "x": 221,
                "y": 95
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8e0200cf-7a95-4192-bfd5-d10abb1d9508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 73,
                "offset": 0,
                "shift": 76,
                "w": 77,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "38a203a4-19db-4fe3-983c-8f529a51be4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 72,
                "offset": 0,
                "shift": 53,
                "w": 52,
                "x": 329,
                "y": 95
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d34d5033-41e0-4192-9652-e64ecc608518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 72,
                "offset": 0,
                "shift": 46,
                "w": 46,
                "x": 2,
                "y": 188
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "38dccc82-05d4-470d-bdb8-92747ff2a599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 73,
                "offset": 3,
                "shift": 48,
                "w": 44,
                "x": 129,
                "y": 188
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9bde2bb7-0855-41e9-896e-1c4f4020cfbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 87,
                "offset": 8,
                "shift": 28,
                "w": 18,
                "x": 878,
                "y": 374
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c34935c1-8894-4a0a-b442-4925f3cac795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 76,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 708,
                "y": 374
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "87072b58-021d-4bf1-b85a-40135ad43f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 87,
                "offset": 2,
                "shift": 28,
                "w": 18,
                "x": 858,
                "y": 374
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c965884f-1c7f-4734-b5d1-a0900d4f5068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 44,
                "offset": 8,
                "shift": 41,
                "w": 25,
                "x": 986,
                "y": 374
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b3f0e2fa-0e34-4d5d-a4a1-bc2192d981d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 84,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 50,
                "y": 188
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5df7e4e0-fd01-4dd4-b946-01e08d4d17eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 7,
                "shift": 31,
                "w": 14,
                "x": 272,
                "y": 467
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "33c2521c-0996-491f-9f1f-fb64dc127447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 35,
                "x": 323,
                "y": 281
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "509deaa7-f2cf-41e3-9104-4b2494c7a3f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 74,
                "offset": 0,
                "shift": 39,
                "w": 37,
                "x": 98,
                "y": 281
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b2cf3d7c-be48-4b45-9ac3-22fca40c26c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 73,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 209,
                "y": 374
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9fb2a506-f99a-42ef-934c-6d004f46c43a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 74,
                "offset": 3,
                "shift": 40,
                "w": 38,
                "x": 58,
                "y": 281
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "46a510ce-3e5d-4759-82f5-3292a4e30c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 73,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 774,
                "y": 281
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c64eb5f9-3822-4af0-b5fb-9ac3bbb24eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 72,
                "offset": 1,
                "shift": 25,
                "w": 31,
                "x": 74,
                "y": 374
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "40dc0921-b933-4298-8250-02a76511dec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 91,
                "offset": 2,
                "shift": 40,
                "w": 37,
                "x": 906,
                "y": 95
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f787f4ec-830d-453e-9e89-fb0af9f28e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 72,
                "offset": 1,
                "shift": 43,
                "w": 42,
                "x": 610,
                "y": 188
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "723a1bd4-24a9-4f65-b09b-c635a4d7db6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 72,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 921,
                "y": 374
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "482e8738-471a-4113-a69b-3963da197c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 90,
                "offset": -4,
                "shift": 22,
                "w": 20,
                "x": 583,
                "y": 374
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2d487182-7bf4-477d-8562-09dfc564f818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 72,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 820,
                "y": 188
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "13f4a642-17f6-4367-8368-05646a33075d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 72,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 942,
                "y": 374
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "46279600-3def-4c5e-a253-9d6089194893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 72,
                "offset": 1,
                "shift": 63,
                "w": 62,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4cdfb7ff-e8df-4a2c-8cab-32bf52c4f4fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 72,
                "offset": 1,
                "shift": 43,
                "w": 42,
                "x": 478,
                "y": 188
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8b796646-cbcc-4c6b-95c7-7ca1870c1dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 74,
                "offset": 3,
                "shift": 40,
                "w": 35,
                "x": 286,
                "y": 281
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7d151d0d-664e-4abe-a9b9-6bf6abcbe72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 91,
                "offset": 0,
                "shift": 41,
                "w": 39,
                "x": 626,
                "y": 95
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d6c45cb1-8b0f-4fc2-96a1-ddb161c12be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 91,
                "offset": 2,
                "shift": 40,
                "w": 39,
                "x": 585,
                "y": 95
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2f887c9e-2d35-4346-b4d0-6de6b9fccd7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 72,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 364,
                "y": 374
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "cf7fa2d6-e050-44e1-acca-5e2cdbdf6979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 73,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 482,
                "y": 374
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bfd29e88-9e5f-489f-ad75-adcf6ab3637e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 73,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 785,
                "y": 374
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "cda79a9a-bf68-4ece-a784-d5949f803d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 74,
                "offset": 1,
                "shift": 42,
                "w": 41,
                "x": 435,
                "y": 188
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "baada28f-a9c5-4f72-852d-8558e14dd2e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 73,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 906,
                "y": 188
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ccd66f3b-be55-4453-84fa-a8011f6a4c52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 73,
                "offset": 0,
                "shift": 60,
                "w": 60,
                "x": 392,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4481bd5f-817f-472f-9828-bb47560bf9f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 72,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 210,
                "y": 281
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c3f4d3d6-b714-44cb-b98d-79e4c5b1077d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 90,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 490,
                "y": 95
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "94eccfb2-8a13-458f-a6cb-daeb579f3f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 73,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 873,
                "y": 281
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1b100540-3696-4699-bb3e-cbc5b2ad0465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 89,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 538,
                "y": 374
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5e793c80-c743-40bd-ad4a-d7dbefe3d35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 90,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 328,
                "y": 467
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ca61ae7d-4204-435c-91d5-b9be24ed4ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 89,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 420,
                "y": 374
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7e3a9943-7d33-4712-8ff9-36d5e22ce32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 56,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 605,
                "y": 374
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "dcaeae8e-ce73-4776-9aa0-2548b0f90b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 72,
                "offset": 0,
                "shift": 56,
                "w": 56,
                "x": 787,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "bf61d037-4c59-404d-aeaf-d8fb3dfe03a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 72,
                "offset": 0,
                "shift": 56,
                "w": 56,
                "x": 845,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "6aff001d-1c5f-4a57-9e40-bb82d8acad73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 72,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 759,
                "y": 374
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "a8bef5b2-edb3-438a-ac17-ac7a2f8e5c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 72,
                "offset": 0,
                "shift": 56,
                "w": 53,
                "x": 112,
                "y": 95
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "d36ad3ec-ed1c-43f9-bab6-4f740af654f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 73,
                "offset": 0,
                "shift": 53,
                "w": 53,
                "x": 958,
                "y": 2
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "d1b984c6-3fb9-468e-9bba-852e8266a659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 73,
                "offset": 0,
                "shift": 53,
                "w": 53,
                "x": 57,
                "y": 95
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "f374740c-7801-457a-84f1-237cf15ee095",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 72,
                "offset": 0,
                "shift": 42,
                "w": 40,
                "x": 948,
                "y": 188
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "003ac8bc-6739-4445-8779-1622fc0a910b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 35,
                "x": 360,
                "y": 281
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "c0b39002-009b-4c61-9929-835489618e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 74,
                "offset": 2,
                "shift": 37,
                "w": 35,
                "x": 249,
                "y": 281
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "0acb1dcc-101b-40b8-9945-46a99bc58797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 72,
                "offset": -1,
                "shift": 22,
                "w": 22,
                "x": 834,
                "y": 374
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "4a477a68-4a6e-4690-ad91-75ffcd905b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 73,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 533,
                "y": 281
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "01572fcf-d61e-440b-942a-4641b2f3364d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 74,
                "offset": 1,
                "shift": 42,
                "w": 41,
                "x": 392,
                "y": 188
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "f528627c-97d6-4bfd-adc6-536ae3d58ba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 74,
                "offset": 1,
                "shift": 42,
                "w": 41,
                "x": 349,
                "y": 188
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "4f9d2765-2105-4e99-acd7-a91caffe570b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 91,
                "offset": 0,
                "shift": 40,
                "w": 38,
                "x": 767,
                "y": 95
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "d3624d82-ae6a-486c-b3f0-c9f806083330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 88,
                "offset": 0,
                "shift": 56,
                "w": 57,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "7e627d9e-7583-4854-b466-e4a76dd206b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 88,
                "offset": 2,
                "shift": 37,
                "w": 35,
                "x": 312,
                "y": 188
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "ffddb8b2-48c0-4237-a53f-1c83cc51f637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 72,
                "offset": 0,
                "shift": 44,
                "w": 42,
                "x": 522,
                "y": 188
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "1d9dab1b-3b2a-4f51-a035-fb164600eb4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 73,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 741,
                "y": 281
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "8a671a34-a130-4dbe-a720-7d7703c7fc94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 72,
                "offset": 1,
                "shift": 58,
                "w": 56,
                "x": 613,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "a80ab597-907e-4579-8dd9-bc72ac98f25b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 72,
                "offset": 1,
                "shift": 43,
                "w": 42,
                "x": 566,
                "y": 188
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "0632704b-7165-4da2-9b02-db7ef3350be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 72,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 777,
                "y": 188
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "c4212080-3160-4461-9915-66c5a13d85bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 72,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 898,
                "y": 374
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "8a2b10a5-aef8-4f73-812a-6faee36d233f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 86,
                "offset": 1,
                "shift": 58,
                "w": 49,
                "x": 504,
                "y": 2
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "7c860f42-b908-4fc4-9013-cf45c9b924f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 90,
                "offset": 1,
                "shift": 42,
                "w": 36,
                "x": 91,
                "y": 188
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "ce4ccabe-8bfe-41e2-a618-34a1d9dae769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 73,
                "offset": 0,
                "shift": 47,
                "w": 48,
                "x": 667,
                "y": 95
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "4dc6353b-85eb-4be9-b094-c31720cfb743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 72,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 334,
                "y": 374
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "fb8b29bc-6604-47d2-adc3-48aab54a8977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 90,
                "offset": 0,
                "shift": 47,
                "w": 48,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "1f0d3467-557a-4f49-9dec-e420d272993d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 90,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 431,
                "y": 281
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "dead2da5-1d32-475b-b6b6-557f45a8ffc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 88,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 698,
                "y": 188
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "17642742-065b-4c12-9253-c1bdca382ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 88,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 713,
                "y": 281
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "ceae5341-c878-44d0-8219-0e9d801467fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 73,
                "offset": 3,
                "shift": 48,
                "w": 44,
                "x": 221,
                "y": 188
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "3f555618-b0c4-499f-bce1-991be9f68aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 73,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 807,
                "y": 281
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "50d3c1a8-f07a-4919-952f-b955981e2a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 73,
                "offset": 3,
                "shift": 51,
                "w": 46,
                "x": 945,
                "y": 95
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "62912604-dcf1-476c-8dea-1a833d6c51e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 73,
                "offset": 2,
                "shift": 36,
                "w": 30,
                "x": 177,
                "y": 374
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "7e34250e-0710-4a69-8223-d0ab58880526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 73,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 497,
                "y": 281
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "2d446488-4525-4dbd-acb5-6d78fb4c98e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 73,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 510,
                "y": 374
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "e10357e6-ffc0-44a9-b534-7059594494ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 53,
                "offset": 4,
                "shift": 44,
                "w": 36,
                "x": 444,
                "y": 374
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "ad2025cf-4edf-4136-b824-8ff6bc7694a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 53,
                "offset": 2,
                "shift": 59,
                "w": 54,
                "x": 2,
                "y": 281
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "c95141fd-6a59-4969-996b-9a6e0c8e7ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 31,
                "offset": 5,
                "shift": 21,
                "w": 12,
                "x": 314,
                "y": 467
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "7836f6d5-c9c7-49bf-aaa9-7f23582b894e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 34,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 288,
                "y": 467
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "8d0f5e50-8e51-4eb9-a9f1-b69cd779cbf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 83,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 50,
                "y": 467
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "c6db8d07-fac7-4fff-97b3-219c7c6cd347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 34,
                "offset": 5,
                "shift": 21,
                "w": 12,
                "x": 258,
                "y": 467
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "70c8cac7-90e4-44f8-9f2c-9c1dc1a38aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 31,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 177,
                "y": 467
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "692505e0-405d-44fc-8475-c762e705745b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 34,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 101,
                "y": 467
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "6665dd0e-1e8e-453f-80b6-304ba1cdfce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 83,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 394,
                "y": 374
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "ad06c39d-6aa1-4276-9289-d55624e73e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 34,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 127,
                "y": 467
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "7a5ce4e8-a1b3-474d-996b-9336d9cc008d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 73,
                "offset": 4,
                "shift": 60,
                "w": 52,
                "x": 167,
                "y": 95
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "f0e8742c-1d14-424a-b4b1-39536efdc687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 34,
                "offset": 5,
                "shift": 20,
                "w": 11,
                "x": 301,
                "y": 467
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "96f6dac6-61f2-4928-abf3-b24bbffce3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 34,
                "offset": 5,
                "shift": 30,
                "w": 22,
                "x": 153,
                "y": 467
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "13399259-8068-45ef-974c-309f91a4fc6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 34,
                "offset": 5,
                "shift": 41,
                "w": 32,
                "x": 2,
                "y": 467
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 60,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}