{
    "id": "b161a131-c32a-4209-a83c-4071a5ed67c0",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01n_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c9787631-a916-4661-8882-0dce9ae0a129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 242
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2d11c22d-f44c-49fc-9a4d-5114e62ad027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 365,
                "y": 242
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "637266e5-6d7b-48ec-b718-1ce5eeb386d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 207,
                "y": 242
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6a43d91d-9832-4a74-8921-e852ffe757d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 427,
                "y": 146
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "27745a4f-4e12-469f-a7c2-16d16743348b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 182,
                "y": 194
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ba54b154-761d-40ac-9641-b109a0f5df57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 100,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "553aaa90-2db0-478c-84cb-a49b7eeb2426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 258,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "886cda97-9a4e-4ead-ac9b-45df18869b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 379,
                "y": 242
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "81a6c3a3-6d1b-4253-9f05-b16c23db140c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 194,
                "y": 242
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "35e599b7-cba9-46d3-8688-030f5c0e6696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 181,
                "y": 242
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "79ef7f3f-ec80-4f05-9c16-7be5153e54df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 17,
                "y": 242
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "79d51887-8669-4e76-a91c-0f2f6a2b0538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 172,
                "y": 146
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3cc42b44-7cd2-421c-bfef-bfd992b8fdd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 357,
                "y": 242
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0d072048-8ba4-4434-8717-64f8aeb898d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 155,
                "y": 242
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "43085bc5-210e-49dd-8e99-5b657c1b13e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 372,
                "y": 242
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b19b8795-479e-4ede-a302-65908aa9dcf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 45,
                "y": 242
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ed04d3ff-60a1-4318-8da2-55362174194f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 74,
                "y": 194
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ec407c83-df71-4eea-9199-d6390d2b2db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 389,
                "y": 194
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "874ac254-cec2-4123-a0b8-09a02de1b08a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 287,
                "y": 194
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2196086d-7199-4a94-89e3-51178f1e4912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 164,
                "y": 194
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c44711d4-7604-4f79-afd7-c4cbe9e3ca8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 408,
                "y": 146
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dd79f118-e688-44c6-8139-a031f15ca28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 270,
                "y": 194
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9bc6ee06-fa54-4623-b1f1-355fce0bf719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 56,
                "y": 194
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "dd13296a-2404-4f02-8f86-e29ccad36e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 236,
                "y": 194
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "96fa7368-969f-4732-ad36-8c1d0c171d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 20,
                "y": 194
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6ab7dcf0-df07-440c-b729-23cae4f9740e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 146,
                "y": 194
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f1d9c44c-5a19-4a3b-9a57-aa5d17fa5450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 349,
                "y": 242
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d7cb5708-9ec3-4cb8-984e-5caaa62d2622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 341,
                "y": 242
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "77cbc3b2-0fcf-407a-b689-2fedf0b6ac7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 484,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f5161b03-7f1a-47d2-82b9-5b3b080c94a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 312,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "60085463-86d9-4f5e-9d9d-df1aba36ddf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 446,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "375e6117-02e8-4917-aa33-8f3cfccc652d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 253,
                "y": 194
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "eec57c9a-9c67-4b29-9ad6-c8e08a2c621d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a8c073eb-795f-443e-ac7d-ea2241b6497a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "aed25e77-6b67-48c0-b4d1-c53ef6491697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 124,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "269f0ac4-0a1f-457c-9de1-5464aa185a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 313,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "27aa5d57-2064-4284-8600-43f2bb22d59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 31,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8f7f95cf-ce85-4341-ad2f-60871f3cc88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 195,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "82d1e042-acf8-4e35-acbc-9a628c7c8f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 151,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "73350a63-ec42-46ad-a246-e74e5a6901d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0dca2834-7aca-4443-9487-3e161c5c5fed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7a02de61-579d-4eb6-9f81-efba7cfddf0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 73,
                "y": 242
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2f657904-1756-45a1-978c-6cb457b555b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": -3,
                "shift": 13,
                "w": 16,
                "x": 218,
                "y": 194
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4a81771d-5a79-4201-866e-1f9250f2b3d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 230,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "876979a2-cbae-47c9-914b-f6141c39b27f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 333,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "54479d8c-e546-48cc-a68b-87fdeb0d5030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2de33994-114e-4388-b507-c963252e2e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b332dc3c-3df7-4d2e-b66a-56ba81375a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 174,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "22255b8f-4794-4603-8015-34d7a2ac4af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 310,
                "y": 98
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7aaf377d-614f-4412-98b3-8a8d6cb7b77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ffec64dd-a55d-463b-a93a-43aed7debe6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 443,
                "y": 50
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e3b2783d-86e2-4b12-adc2-424cd9dd281b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 465,
                "y": 146
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8eab3d54-8887-4da6-be61-c40ed75b4172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 365,
                "y": 50
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1f0d4b89-70f0-4497-8fb3-4abf2a59298c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 89,
                "y": 50
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ad874e31-7a9c-466a-954a-a2f3b479c6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 146,
                "y": 50
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bcfb5baf-fe16-4880-8e12-07abba76a497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "53ae0781-51b1-45d3-970b-fa74a67e3581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 202,
                "y": 50
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5022d07b-298c-48a6-a74b-235983f554d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 27,
                "y": 98
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0ee2bb94-d601-41dd-a3a0-ee3302412af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 469,
                "y": 50
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5ccf700a-6c7d-4793-9d42-4c11bddb5886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 4,
                "shift": 14,
                "w": 9,
                "x": 278,
                "y": 242
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "509c2a58-6805-4ff0-9ab9-fb702d7b4327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 115,
                "y": 242
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "07bf8c1d-cfe7-4a01-b336-d30f161a1aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 9,
                "x": 267,
                "y": 242
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f919fbab-e0af-4cd0-9821-c07f8ae2f977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 479,
                "y": 194
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f4637256-aa37-481d-954f-984ba8e50aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 446,
                "y": 98
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7817068f-6af5-4abf-8b6a-73ce62cb264a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 289,
                "y": 242
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "afae5b23-c42a-4fe7-994c-1e49ccf546e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 212,
                "y": 146
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "30279c26-2beb-4644-b497-515caf49adde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 46,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a77c3413-869f-4a5e-99ae-4f85c97c1d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 372,
                "y": 194
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "af978aa4-f8c1-4634-abb2-6bf05201d907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7f8d4439-0710-45e4-9da3-3ccddcb15487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f77a61f3-6ba1-4e04-9157-5ca9e37061c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 16,
                "x": 110,
                "y": 194
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "985edecd-6663-41ce-8ed5-beb71017e577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 67,
                "y": 146
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "dc74d6d4-0fda-447a-aad1-713616660762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 76,
                "y": 98
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0783dd30-7cfc-436c-9a19-d4d898274bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 219,
                "y": 242
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0e291ae3-74e5-471b-9afd-67870cd70d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -2,
                "shift": 11,
                "w": 10,
                "x": 243,
                "y": 242
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "80c85cb3-af1d-47db-9adb-84db08682055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 379,
                "y": 98
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cdb3dc41-8ca0-4c39-91c8-8022f73dceea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 255,
                "y": 242
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "08573f2b-bab1-4f15-9947-ce8ef3c8255c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b63aedac-3b16-46cf-a53d-30b1d0e5c51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 52,
                "y": 98
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0fb194c5-8391-4450-8c0b-8143a9005a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 272,
                "y": 146
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "799312ad-8fa4-4448-8370-4cfe6c27dc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 468,
                "y": 98
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f660592c-e34e-4619-9d02-9645922817e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 424,
                "y": 98
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d6a2e12c-36ab-4de0-b641-b4c9dd923540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 338,
                "y": 194
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4d5722d5-b66d-40f5-b5d8-ebcada7bc3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 464,
                "y": 194
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "27ed64d1-7e34-4a6c-b102-cedb57ff862c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 31,
                "y": 242
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "97382669-7d82-4380-a588-049d4aff43f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 172,
                "y": 98
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b88531d5-e37e-4422-9756-a634e7c7913b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 402,
                "y": 98
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6d4580e5-25b4-4e1b-a9f6-47278eaa9478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "69076e8e-ce52-4f40-9650-8b1090d710ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 130,
                "y": 146
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d51d1032-d6d2-4b33-99e1-9a51616ffa17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 264,
                "y": 98
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d376c554-0e28-4aaa-96f9-60376450022e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 128,
                "y": 194
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "780529fb-04ff-468a-9150-152755d57d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 129,
                "y": 242
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ad344907-702b-4739-990f-fede1aa36797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 385,
                "y": 242
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ed9b5dd2-427d-4a5e-a8d3-fe081f0eded3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 168,
                "y": 242
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b1398c55-bae1-4464-8863-901f93071d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 194
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "39e506d5-84f4-46a6-942b-a0d9cd15af50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "184dd433-2f90-46c8-9d3e-dc62c5b266b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "7ee3d18a-e9a7-42ca-9c24-ef79fbda9e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 87,
                "y": 242
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "70bf1eb6-7499-424b-aeca-3720c49c7baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 391,
                "y": 2
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "f55a6208-367d-412c-992e-9d53709d8733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "7c4b15a6-e866-466f-9358-f40f42864906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "a51daf62-db37-42d1-973c-c81003f6fb39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 24,
                "y": 146
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "176b6449-5dec-4e10-aa68-c32dd1450307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 252,
                "y": 146
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "d252da70-629f-41f4-a6b8-e98e95a7111d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 232,
                "y": 146
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b84f521c-3ffd-47f4-95f8-d81aea3c9704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 46,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 59,
                "y": 242
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "5f5bc085-fe4f-4b5c-8974-c19508bb8588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 370,
                "y": 146
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "a7b55b64-a809-473e-888a-caf42091dd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 356,
                "y": 98
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "20e1b187-9c78-4eee-a81c-b07f15d9d998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 287,
                "y": 98
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "090bd968-b990-4cc6-8a6a-0b09086d7f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 109,
                "y": 146
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "ea0e98f1-7c19-423a-b24a-9225a954285f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 29,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "b99dc34d-82c7-475f-8ec2-01078a6665a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 192,
                "y": 146
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "c2ce4da0-d809-4bd4-b631-61d994a9b8cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 241,
                "y": 98
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "ba5ee709-bc7e-4920-b58e-c05e98355416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 194
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "606fa9b5-066b-4367-80e0-284eac282083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "6dc8a50d-1015-4c16-882a-0cc45f4d7193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 148,
                "y": 98
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "1eb80d2f-aca2-4dcd-a9bf-ab01319f6cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 218,
                "y": 98
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "680d1577-ac0b-4f18-a2b8-1a54d549e964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 142,
                "y": 242
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "a2ec0b34-f9da-4130-9181-62e597519f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 25,
                "x": 286,
                "y": 50
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "c9a4f4b3-7083-44bc-ba6c-c31d9bf1874f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 88,
                "y": 146
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "4be98799-919a-4920-b81e-68e5f4cf6a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 417,
                "y": 50
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "a6d04c16-836c-41cc-8d00-8fc11a4487b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 46,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 321,
                "y": 194
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "1b640dc3-2e39-47ff-b8f2-e9ada611ca9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 391,
                "y": 50
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "19d7b731-667d-45ba-997b-834b157a852d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 46,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 355,
                "y": 194
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "003a4729-6f25-45a5-b39a-ab7780fe3c48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 351,
                "y": 146
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "e340efbd-51e5-45eb-9a6a-029ecbc60a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 434,
                "y": 194
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "fbe23b42-05b3-4293-9594-ef2977a1a1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "d42cf545-dad4-4822-b55c-2eed0cfec261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 38,
                "y": 194
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "26a73523-55e1-4f50-bace-545b9c784fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 339,
                "y": 50
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "9421b7be-77c5-4d02-a42c-605abc6ab99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 304,
                "y": 194
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "4b855845-cbb6-4882-a197-3330c8aa8c3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 332,
                "y": 146
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "956bf949-0a07-4bb2-9595-1260fd60c040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 404,
                "y": 194
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "4bede228-7222-41a8-ba56-5cac84f85ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 292,
                "y": 146
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "08f4e03d-d64f-4847-95ee-a13b2101716f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 60,
                "y": 50
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "7d9356d0-a5a7-46e8-b95a-4f003618badf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 299,
                "y": 242
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "a4bf41b2-6fa1-480d-b8bb-6d8145087a78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 333,
                "y": 242
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "f22fc317-7f81-4416-92e0-829a1699197a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 325,
                "y": 242
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "2ecf531b-3da0-40d7-85ec-a4db224b2d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 308,
                "y": 242
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "b08a348d-251a-4d0f-9882-7a321d21f52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 494,
                "y": 194
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "f3a35b48-9107-4f11-adac-eafc5e268d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 419,
                "y": 194
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "9e637014-ac74-42d4-9266-75f7b14af0a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 449,
                "y": 194
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "a3eabae9-c7f7-4c29-9a9b-9f2724bd6327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "1d64fa57-4644-41c6-90bd-c3ec266a8ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 46,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 118,
                "y": 50
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "51c676b3-833c-4cae-8f7b-cb2efe34a379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 46,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 317,
                "y": 242
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "30de5f4b-8d0b-43bc-a06d-d1b5a4832d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 46,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 101,
                "y": 242
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "a753b8c4-c216-4cbb-9352-97ed4dbe4d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 46,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 389,
                "y": 146
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}