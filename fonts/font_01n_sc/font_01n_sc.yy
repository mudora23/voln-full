{
    "id": "81f8d5e4-c613-4ac6-ac4f-d6ea051b86fc",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01n_sc",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine Capitals",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0f17d7a6-482f-41f5-9368-8eb4d6bd6c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 286,
                "y": 242
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "66caba96-5379-4f04-a761-ff205e32ec6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 420,
                "y": 242
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d9970ff5-4213-45a2-9bf3-6765b50d0ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 298,
                "y": 242
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4052c67e-9e75-4701-8f88-aa5d6566d327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 194
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f793eb23-880e-40ee-aeca-48e90ae78014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 316,
                "y": 194
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "165e01f6-69fc-4a52-bded-28434eb44330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 201,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d716e11a-d86d-447d-8d8c-37002bc975c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 52,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2320d930-8785-4cdd-9c3e-a43f972cf637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 434,
                "y": 242
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "94213bb2-7fa9-47f7-974f-b890f977e647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 221,
                "y": 242
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c5698ca5-e3ff-420b-8240-8d0519195f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 242
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8f602f2d-df09-4525-ad26-38eee7c0f32a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 207,
                "y": 242
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2c59b442-6f99-4699-93e8-a6ff61faa3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 349,
                "y": 146
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a8e0a53e-7e47-4112-a8bd-cc8c9597f9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 372,
                "y": 242
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9743b33e-0b6f-4184-aff9-584130510f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 247,
                "y": 242
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b6434230-1b00-4299-a82d-5302837b9af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 427,
                "y": 242
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b2163539-deec-48d7-b76f-2f7dc095f98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 193,
                "y": 242
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "22ed7ed4-2577-43de-98d2-bfff8ee6c589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 280,
                "y": 194
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "97f05199-9405-459b-860a-258fbd7fb2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 93,
                "y": 242
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c4a3340c-42aa-46ff-b738-b29a2128393d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 370,
                "y": 194
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "14c4dc87-de76-44d3-8ecf-f773bd622d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 244,
                "y": 194
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "98d3d970-81fc-4bde-a86b-028db754f907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 135,
                "y": 194
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "20f43aef-77a5-429e-8293-dfca60c4ceb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 404,
                "y": 194
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "09cf9113-c950-414f-a798-6dc72c220256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 172,
                "y": 194
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7e449701-8260-4328-afa7-a4c352815722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 455,
                "y": 194
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8b0017e7-37ae-4138-bd71-cc6873b24e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 334,
                "y": 194
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "acef6bab-2324-450f-893d-49802d7f3766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 352,
                "y": 194
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "54391bdf-a8f4-4dac-aa6b-70ceaf375e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 388,
                "y": 242
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d924096b-b74d-4e19-863d-4743b57eb437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 396,
                "y": 242
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5eff7e70-9941-42cb-99ee-bb6818ba51da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 40,
                "y": 194
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e69e35bf-da21-4f42-b8e6-fe9a6d65365d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 369,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c4ab869e-854c-4013-a35d-7609691e0f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 59,
                "y": 194
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "45ed30ac-907a-4812-bd16-3f4d3492a784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 421,
                "y": 194
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5035ba9f-fc1d-438f-ae9d-34afb8f9c8d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bbf1f628-5f77-463d-bba0-b245ef7e73e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b55b0a5a-7144-4ea0-a989-3878562f8c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 225,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "87011adf-bd55-4e37-92cb-9ffaa3db3e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 414,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1cc9366c-5948-4761-9996-63985b3c5534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7888c234-673f-405b-9542-96bfe5d3a609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 48,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bd696d00-1fda-4569-aa4b-58cb0f8d7443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 288,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "172cb497-d748-49c1-bf87-b87f15aef0af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "195cd593-a892-4a64-a5a8-afbd96f97313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f7561306-1d0b-4de4-8053-7d7b31a15127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 137,
                "y": 242
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "47afc944-0d42-4f29-8c22-1dec57f60718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": -3,
                "shift": 13,
                "w": 16,
                "x": 190,
                "y": 194
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7915dd77-7ec9-4285-a603-7c3dce56f334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 89,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "be70a920-5f43-4f24-8856-54c8070f6f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 486,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bf96d7e9-66cd-4ff6-a520-5a1cb3cd3a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b332e9d3-f711-4bb8-b873-548d988bd546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 299,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "dc6a9484-3ad1-4f44-87ba-315e79a24857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 145,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2321cf1a-f67a-48c8-8341-98cf8906309e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 440,
                "y": 98
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "526b3124-6aa1-403f-8d0b-4680987fc57c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f367f47e-f239-4df0-9f44-3e8be877849a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 388,
                "y": 50
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1c0f0fb7-a156-49ba-ae6f-e4a11b8e9783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 116,
                "y": 194
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "05175051-e435-4056-8031-0ac7a2e65fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 310,
                "y": 50
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ea3c5f1a-a8a1-419b-96e5-e9382a263e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 60,
                "y": 50
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6c438451-3824-461b-8fcd-27c21613cf64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 201,
                "y": 50
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "520de52c-e44c-43aa-97a9-08571b40653e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f412df88-d597-4955-bfa7-311416d17f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 229,
                "y": 50
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5ecd7432-1740-4286-b3df-3231145bc425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8c14dda1-5f2f-45cb-80f6-e177e1043d46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 440,
                "y": 50
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5c018926-3927-4c58-adcd-993c01be30b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 4,
                "shift": 14,
                "w": 9,
                "x": 322,
                "y": 242
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1c3dc6bc-ed5a-40cc-a3c6-1ffbe1640073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 165,
                "y": 242
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d6b24e43-c1c7-4c78-8877-ef20419145e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 9,
                "x": 333,
                "y": 242
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b07c38cf-31b5-41e8-b814-1850eb8dbf6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 108,
                "y": 242
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d733bafb-a816-41be-a667-6dc39cd9b013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 138,
                "y": 146
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "03505c21-f666-447f-8bf0-44e7f90e1b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 344,
                "y": 242
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9293d148-ddde-45c4-bd25-399e6ef0a684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 465,
                "y": 50
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2e813da7-cca5-4b6a-988f-29a1509a5f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 329,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ad5953bf-87ca-47a3-bfc7-a2c7a899e49a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 97,
                "y": 194
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "006d7d62-bb8d-4dc4-9ddf-4732cc47ad80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "51fe75cd-1467-44e9-83a2-2908b5e34f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 78,
                "y": 194
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dda1bdb7-46b8-4093-942f-f31a68aaa593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 298,
                "y": 194
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "68024167-225b-4e89-9f71-7cd877b20d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 94,
                "y": 146
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "753f7b8e-aa07-4427-9bc9-fe7f843d7b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 27,
                "y": 98
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a3ceea0b-8022-4e52-b860-d400c4ad4a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 310,
                "y": 242
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7d33e4bb-28b5-419e-ba48-ee074209cd87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -2,
                "shift": 12,
                "w": 14,
                "x": 488,
                "y": 194
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "613dc3ce-ca7b-4dde-ab02-8fd394964e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 116,
                "y": 146
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "684e9b12-4f58-4a12-b668-ec5b372d858e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 226,
                "y": 194
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dca82599-eb6b-4433-b3db-c5fe769b8b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 173,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1fc94fc6-8efb-43b4-be1f-90c829025c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 177,
                "y": 98
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d6a08598-2e29-4d32-83f3-db9a1f40adee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 25,
                "y": 146
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "00e4533e-adcd-4fd7-bc27-bd03011ed0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 465,
                "y": 146
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4c7891de-0f9a-48b8-920b-2b93d1f00e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 297,
                "y": 98
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a59ab7a1-8ac9-456b-990b-0d3a1df9c9f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 267,
                "y": 146
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1a2c651c-c9a1-4255-98ac-fd20490a33b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4dc1774a-39ef-4b66-8d0f-bc1adea1be65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 225,
                "y": 146
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bf8545bb-3c83-490a-9019-2d06ccb2f002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 345,
                "y": 98
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "62237d9d-a3d7-4530-bd97-d5f33ec24bfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 321,
                "y": 98
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0279a2da-01ed-40f2-aed9-81092245dbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bd8d544a-97e0-42ac-8e50-782bba7a91b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 182,
                "y": 146
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8258edac-1cb8-4ea6-b646-3a2f1ee6a3bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 204,
                "y": 146
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "25acc3ad-504e-4f53-9380-dec15091b65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 408,
                "y": 146
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eb177434-29a2-458c-932e-689262a1a815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 273,
                "y": 242
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d215e84d-e39a-4177-bd41-03949ac80550",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 440,
                "y": 242
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2cc613cf-0a48-4e90-a9fc-b431b98481a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 260,
                "y": 242
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "51b48ea7-e95b-4040-b439-4d8104a4619b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 208,
                "y": 194
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "756952ad-de36-4856-bfa5-93df9760e3c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 269,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "db4ce246-74cb-4ef6-a1c1-0e0e6e6ee63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "d0aa62c9-5cf6-4176-ae9b-ec2148b825f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 151,
                "y": 242
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "2aefedc5-1f44-47d8-8bbf-783e7a96867d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 446,
                "y": 2
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "dcc5e78d-8e03-47e6-9061-9c2dc45e6d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "55ac0331-606b-43a1-a925-00a5b75ca56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "f36a411a-2cea-4e1a-9108-ae199560d062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 160,
                "y": 146
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "9fcd0422-fde1-45d8-90d3-bd25ea94db2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 77,
                "y": 98
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "d9e1b5d7-396f-401d-95f1-0a848b84f54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 102,
                "y": 98
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b8e5059d-f910-4caf-ac4f-2babf2665107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 46,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 242
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "4ee24c5d-441e-4974-b072-b4f697c8df1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "33cea26a-801a-4145-b798-6f1671610cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 249,
                "y": 98
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "65089d17-3782-4ed1-93aa-4290b94f4d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "ad006590-2763-420f-87c0-5ffe8a3fa540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 446,
                "y": 146
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "081693a0-60be-4118-a9c2-01d0ce70f52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 46,
                "offset": 0,
                "shift": 28,
                "w": 29,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "95d43d63-4d13-4f25-8f97-43cb8b976741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 152,
                "y": 98
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "8e1506f2-9362-4fd5-8621-9a4ed8187bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 463,
                "y": 98
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "9259c8a8-9371-4d2e-90b2-4852b8324ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 154,
                "y": 194
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "9df859bb-894e-467f-b1fd-d8012b6b7468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "e97daa4a-11b9-4d47-9cd8-374d1aad0e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 369,
                "y": 98
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "cea29348-af80-4d9b-971c-003a4a320f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 71,
                "y": 146
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "3978d136-55be-4d45-a19e-7e9fb66bba2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 46,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 262,
                "y": 194
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "1de2c3dd-4801-4325-804f-9d7b382e1540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 25,
                "x": 257,
                "y": 50
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "027563a1-7550-4ee4-839a-f42a0c988ca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 393,
                "y": 98
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "146c9cb0-0809-4962-81bc-1d58d7473d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 362,
                "y": 50
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "6dab9ef9-cbc2-4230-b2a1-15e884d4b443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 246,
                "y": 146
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "ab195911-9996-4cfb-bc07-cd287936f638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 336,
                "y": 50
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "de4cfca8-6df1-4774-bdd6-2c8e86a0e679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 46,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 387,
                "y": 194
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "2847a266-e16a-4294-a7e9-da41817d9e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 427,
                "y": 146
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "b1edc06c-9b26-49ac-b176-d168251bf6d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 472,
                "y": 194
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "7b707b8d-df27-42dd-b2bd-0e5c556a6f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 127,
                "y": 98
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "8574da4f-829f-4b7f-ac1e-0a325ae1defa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 484,
                "y": 146
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "6104067c-0d53-4289-a880-87e062b7d8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 284,
                "y": 50
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "bf6f4c57-f32d-4d22-a185-3445325e191f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 438,
                "y": 194
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "ca29f3bc-c0d6-46ab-b014-49cb209a5258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 389,
                "y": 146
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "8c26c211-efee-424f-a27f-0db63d69f387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 78,
                "y": 242
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "e9d27975-9e54-45ce-9249-eae59c4be70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 309,
                "y": 146
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "101eb346-59bc-4f25-87c1-ce589d5df4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 31,
                "y": 50
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "8bb86515-579f-4fb2-a7e8-305a908cd77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 354,
                "y": 242
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "cba93014-e3e7-4711-8e53-165aaaca85d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 412,
                "y": 242
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "22037371-af3a-4d63-80b7-e6b1229f81d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 380,
                "y": 242
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "d50caf66-3a34-40b6-aeaa-88fc60a24552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 363,
                "y": 242
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "962ce0eb-0fe6-41c0-b062-71cf48758f3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 63,
                "y": 242
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "34f3e512-d008-47a9-b547-00a74d010d01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 48,
                "y": 242
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "68eeedf6-3f46-4584-b326-ed4a806a6983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 242
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "3e944fb8-ccdf-41f2-b364-a320f70c07ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 242
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "321b52ad-34c3-4ae5-bba8-da9fe18296d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 46,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 117,
                "y": 50
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "4411f83a-d510-43c4-a1ba-ed42abee3012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 46,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 404,
                "y": 242
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "c85bcb94-9190-4a42-b37f-4bfe1749fe30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 46,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 179,
                "y": 242
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "f6cd857a-c818-477f-b3f1-f491fa2fdd2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 46,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 194
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 30,
    "styleName": "Small Caps",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}