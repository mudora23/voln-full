{
    "id": "828f73fb-5e1a-493e-ad9e-f6070656e60d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01_20_sc",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine Capitals",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ab5c542b-e743-495d-8571-050c63770738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 130
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0c1b5f85-388b-47b1-91e8-9faf6b839682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 96,
                "y": 130
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4ff02446-5c18-4882-855a-990ee5367dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 497,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bb52ee22-c3ef-4e5f-8909-5b1b6fb20b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "27b20955-31ff-4cb3-8fae-5ef91b729165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f786e94c-dfa1-4391-bf55-641451260f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 294,
                "y": 34
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b8544cef-6d32-4618-a1bf-52750ba57ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 366,
                "y": 34
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5ddbd003-23a2-4a38-8cf9-537a76f34291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 120,
                "y": 130
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "73c2034d-f052-46e6-b3e6-77216a8a07c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 130
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6a172f23-7ab6-4b31-9d7e-e2359c92dcca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 130
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b7b73b63-6209-4725-a705-28391e607f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 377,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e07f1633-8562-4091-8a9a-2a3735af4c4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 307,
                "y": 66
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "85334e59-b7c8-4d3b-9866-22c0c0b6a832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 108,
                "y": 130
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ce8ea969-dea5-4568-a822-c658b44e85e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 387,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "95b301c6-51e5-4256-b7c3-79b4466b3989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 114,
                "y": 130
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ad9e2299-91f9-4741-859a-0a01c6a4aa40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 407,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ad8446d6-e1e3-4a21-af9f-55a16df3547e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 114,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6ca22c60-1995-4350-b391-88d8df40708a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 314,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7b5cf8cc-bd03-4ea5-b10a-18394ffa2669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 205,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1f33dc8d-1374-4af7-a722-145908dc9b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 140,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "751ac5ea-27e7-441e-869a-2049ee5d2661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 322,
                "y": 66
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c8a1b73c-5c6f-41c3-9dae-5dfd6e96f402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 218,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5bf41a07-a498-4855-a9d2-4f31e88c14c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 153,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8b27912a-c5b1-4803-ae51-cceb58e0a3c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 166,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6bc2f0ac-4200-402d-9a58-50f0571f2533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 179,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "eb786356-094b-4003-b06b-0d4eada03d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 192,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "12474f20-8df5-4ebb-8055-ff41caaf5671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 102,
                "y": 130
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cbda813f-b13d-426c-902f-dd5bc9d24947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 89,
                "y": 130
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d8826e4f-21e3-4f2e-b744-a0360d051b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 493,
                "y": 66
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b88ae0df-8198-41fc-9b68-03f14129a535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 262,
                "y": 66
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9ac986cd-961f-461b-ac60-c3616b26810e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 16,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4928ae25-c415-4c81-85f2-ea3b7bc71b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 280,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "046131e1-5e51-45f7-a7dc-d213c2ce7e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a843625f-3353-4c5b-8c7d-eebe592f116d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "45ece6d9-4d8d-4c78-854f-a8ab83cf6acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 453,
                "y": 34
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fc6ea5a1-af11-469c-bb42-0db737acddb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 150,
                "y": 34
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3c16e01d-c12e-4b81-b4d0-16e5afff2027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "88cd22d8-c5c4-4536-8a3b-91e3b744c8d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 36,
                "y": 66
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "38fb53b8-bf54-456b-b925-fca0f402548e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 277,
                "y": 66
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "60789580-8b2d-4971-ac14-5cf3315eb109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "015d92a2-42f3-4f7a-954b-97921430724c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1c0ad8df-9090-4b7c-8e73-794a1bbf2d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "29d92a6b-c775-4a58-90c0-59409c2cb96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": -2,
                "shift": 9,
                "w": 11,
                "x": 231,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b308b6ce-ac32-4b87-9d3a-29bc279a1260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f80b2d7a-1f5b-4c6c-8659-aab5f6e32f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 215,
                "y": 66
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2a589531-30b9-4833-bcf1-1f6fdc47f22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3179b1fc-2928-4666-a61b-41a7abc3a64b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "89fa08ad-05a4-441c-879c-0a2adb3a6596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 41,
                "y": 34
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9580d389-1ed4-4ba8-973b-90bc2ac6ddd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 231,
                "y": 66
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b207c8a1-f616-4f21-becd-7922f7db0d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "13447864-6d28-41be-9240-d86de17db2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 168,
                "y": 34
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "471a1bfe-13f0-4ad5-a6ea-91a8a60c0a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 98
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c61b98a2-7ddb-413c-a9e8-c72f66e048c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 78,
                "y": 34
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a0db39bf-92c6-4994-9358-7fd5d0a0378a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fca86150-6f1c-4996-9ed9-6c272a6940ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 374,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b7457f4b-3907-4f54-9d7c-c132a8ed64f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cf22b948-d10e-4953-b71f-927caaad3c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e704b9e4-7f04-45e8-8e34-3ddcd446ac13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 34
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "aff455a0-f6c7-451e-a730-61f77575d31c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 70,
                "y": 66
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e935d680-7361-4c49-ba5d-91e1221c5a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 29,
                "y": 130
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c4f9b31c-6bd6-4b11-b6f5-5a2a2ce806b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 457,
                "y": 98
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9239b878-e7d4-4d08-8dea-0a7a555883fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c9ea9dfa-12ed-456d-9719-0233488bbc98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 487,
                "y": 98
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3e2d7017-aa07-4992-b336-6fd258884887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 103,
                "y": 66
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d92bb293-3e0d-477f-b8af-c82702f93db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 54,
                "y": 130
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a9851c81-6603-4267-abc1-636161c11795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 132,
                "y": 34
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7900fd40-4c85-440e-ae99-be5cfebe45b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 352,
                "y": 66
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d1cbe76a-f27c-4e44-9e5b-8d2adb771d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 98
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ce1242a2-2d54-4dab-9d19-58a00e8e7af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 436,
                "y": 34
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9e2837aa-8108-4632-89bd-1acf940ec1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 98
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5db3ad60-9154-44e0-85cd-9f56ccc7517f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 100,
                "y": 98
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "81cda6cf-cae0-4510-b31c-5e18f849df6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 87,
                "y": 66
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "561ff92d-afe4-462d-b400-24475beb9be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 186,
                "y": 34
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "46bf7ba0-1d11-42fe-a1fb-1ca7a4ceeddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 367,
                "y": 98
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "da1e5855-5e58-437f-b53b-3d625ab23fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 268,
                "y": 98
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c3d4f212-650b-4c0c-be10-90cd4dfa270c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 19,
                "y": 66
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1d880e5a-0d6b-4c6c-ba49-0fd565fd0dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 479,
                "y": 66
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd42f33a-8db5-4356-ad37-7d86fff401cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e4815de4-5a38-4c13-b0f5-f10ab0e94806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 96,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f280bf5c-08e1-4f45-a5d8-31610005b8c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 183,
                "y": 66
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b32b209f-0138-411f-a416-c5a66e65495a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7befceff-ca50-4d55-b01f-b4fdebed6e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 135,
                "y": 66
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fd3f0d3c-1b4e-4231-b3d7-2e193a4fad00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 119,
                "y": 66
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "83981e67-9e6d-4bdd-b653-9d25123818c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 292,
                "y": 98
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f0ddd221-a7aa-4c8e-b21a-763fa2e1347f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 292,
                "y": 66
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a89ccea8-c16f-4bcb-a3ee-8eb417ed3bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 60,
                "y": 34
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c713a965-e7d7-481a-8c85-71283b15b72e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 487,
                "y": 34
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "404e819b-ab32-482b-862a-b5ce46f2ebc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ed9c394b-cd6a-49c5-a9d3-acbbc5528182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 53,
                "y": 66
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1f444863-943f-473d-89b3-f835efbbfeef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 337,
                "y": 66
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d6f106fc-b01f-41b6-82a6-d4dc5bed1b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 367,
                "y": 66
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "53e48494-02d2-462b-b0fb-d447719ac1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 447,
                "y": 98
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3580feed-8d2f-484d-89b7-cd18ad86653e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 125,
                "y": 130
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bbd295a4-fd80-4dec-9b46-fef1d6d283b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 467,
                "y": 98
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9176e648-3c5c-42b3-bdcd-33532f0d02ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 395,
                "y": 66
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "08af7f4e-20be-49a5-a6c3-16976ca95ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "cb0132c0-9624-4aaa-87ef-92898e174fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "930d2616-c465-4316-bcdc-e4d6bb99096c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 477,
                "y": 98
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "80990215-69c2-450f-a19f-aa64b9003d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "2b5d98cb-01b4-4207-a582-77913dbff420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "570cf4b5-dda4-4da9-a116-1911a644b463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "165f4432-9153-42b2-ab01-68845cde0348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 199,
                "y": 66
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "26d0059e-8375-4775-b449-2c45736d44a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 384,
                "y": 34
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "3bf86907-890f-4f74-abce-55e156e5e426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 222,
                "y": 34
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "4cde2259-5fe1-4642-b381-b754245e51da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 427,
                "y": 98
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "7107b056-43ec-4ee1-9695-f6245a453cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "2a707a09-1afd-413a-8e4a-00398b6196e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 330,
                "y": 34
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "855887e4-31ea-497d-b98a-9434939e5260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 348,
                "y": 34
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "f38845be-7446-4bac-972a-5a9cb78a9213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 451,
                "y": 66
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "89c36558-78bc-4acf-b9ef-836768ebc285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 252,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "272ed89c-37e0-4375-a0fa-64fdae75f595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 204,
                "y": 34
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "04d86317-46b4-4859-ad8e-af104d807c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 470,
                "y": 34
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "cc5ebd09-5ada-4e52-aa92-87d0ccef8460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 256,
                "y": 98
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "8b350928-f878-4e63-8ca2-a0f1bb25799b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 30,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "b7480081-7891-4210-9b4f-8ecd86789b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 419,
                "y": 34
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "3ae5089c-c1ef-4ebb-ad57-71e95a939176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 167,
                "y": 66
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "9acd5a26-5c42-4e0d-9b20-39ceea4b4d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 381,
                "y": 66
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "d534a199-c070-487f-a877-5cf18e692b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 30,
                "offset": 0,
                "shift": 20,
                "w": 17,
                "x": 22,
                "y": 34
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "276a7160-0f6f-404b-b880-be3984955211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 276,
                "y": 34
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "9764d06d-0c16-4c83-b822-14a5650afabc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 258,
                "y": 34
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "40e3b6b8-5349-456b-b24a-122ce95e5352",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 151,
                "y": 66
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "eb2551b9-e969-4a1c-ab63-ad545ee44cab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 240,
                "y": 34
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "2391c508-c0dc-4a7a-a762-640b4e3822ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 244,
                "y": 98
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "d75c9a9d-2cdf-45ed-9e4d-ffd349512bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 437,
                "y": 66
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "9abcef76-1179-4aca-975b-08b7350f6288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 325,
                "y": 98
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "3545e985-756a-4ba2-8165-8dc7bd9be2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 402,
                "y": 34
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "1ad0f0e5-fd3c-4ded-a3f1-58af3d76f3dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 423,
                "y": 66
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "c9aabf3c-7a6b-494d-9cea-cd86e6637af7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 312,
                "y": 34
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "c67c5abb-73f2-44e5-8dae-4b15dd8ff125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 98
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "4027f1de-811e-45ab-8652-e31860fa3379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 409,
                "y": 66
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "2adf8bc7-386b-4999-9baf-579e571c44d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 303,
                "y": 98
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "bcc0414b-f272-4557-a6d9-af400a8c764c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 247,
                "y": 66
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "f95d03fb-e22e-4f87-aa48-26a9d74d3158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 30,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "3681b207-767c-4830-9fa6-7238d483bc1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 61,
                "y": 130
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "bcba576d-d016-49d2-8a15-61c008d26d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 130
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "44c223b0-e027-45ad-a6d1-d50eb63925c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 75,
                "y": 130
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "670c2966-eb5d-41b1-97d4-0b3e0e3f7e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 82,
                "y": 130
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "2a6a6763-39c8-48b5-ae55-8017cbbb1086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 357,
                "y": 98
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "b581c9d3-37c6-48cd-a547-4b620e22c436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 347,
                "y": 98
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "c91c20cc-0cd3-4491-9012-fb14bd74703a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 397,
                "y": 98
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "bb8c6a42-2eed-4cff-b096-f0fed35d0a64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 336,
                "y": 98
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "ac47ba53-b797-46b2-9880-184ca8c6fa1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "8b93795f-c365-45ee-ad7d-37066d1dcd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 47,
                "y": 130
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "9135405b-6ec7-4a86-8f49-68b620a38198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 437,
                "y": 98
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "125a6313-b6b0-4c44-9cd9-bc9e76ed1d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 465,
                "y": 66
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 20,
    "styleName": "Small Caps",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}