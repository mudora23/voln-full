{
    "id": "3f467c2f-8303-4403-a838-77c4af250a44",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01s_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b43efe63-99fc-4efe-85bc-4a64e369ee92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 190,
                "y": 127
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3dd8acf1-8638-4a0a-80dd-ca4aa6cf6581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 98,
                "y": 152
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5bf4d1d0-bc02-45bf-9710-bee4786f6030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 111,
                "y": 152
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7a78a829-01e5-4a5d-803c-cfb8e3974933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 102
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0a612758-1a3b-4778-b146-df8627039898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 77
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a25fb246-32d2-44a6-b67a-1d373ed04206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 78,
                "y": 77
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4863bde6-623a-4532-830d-a4845fe63b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 34,
                "y": 27
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8ff5c7e1-d3d6-4e33-b534-3d3f1db31763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 159,
                "y": 152
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7e7a9971-9140-404a-bb8d-4135efcf9526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 80,
                "y": 127
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fd0bd44b-5275-45e8-9c56-292f82b859ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 72,
                "y": 127
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8c292600-e5c0-4375-a229-d41d9512b249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 51,
                "y": 152
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "095c417e-9520-46d7-8a2b-d04c8b3312d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 229,
                "y": 102
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c37a13f3-7e5a-40e8-a708-ba0deecaa1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 25,
                "y": 152
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5df9aa49-ec81-48f0-8939-9c8575469781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 17,
                "y": 152
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "04890f87-bf31-4ff7-aa20-68cc236fbc61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 88,
                "y": 152
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6489c9d8-7de9-449c-8977-599805a5c9e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 171,
                "y": 127
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "38b14728-527a-4e88-914a-caa556a2b73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 70,
                "y": 102
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4707e266-9e1c-4b98-8d26-e2fad793d6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 135,
                "y": 127
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c7396359-60bc-4f79-bc99-0cb0f7fdd834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 200,
                "y": 102
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dba72ea8-cf84-48d3-bd9e-8a6b855fd715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 48,
                "y": 102
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7ea72f64-9559-4ebe-bd39-e1f5be4004f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 102
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7b9d4b2b-00b7-4806-8e2e-be78d1627b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ea9bb21a-2695-461f-bb88-ce37cdf7b9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 125,
                "y": 102
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0f482a6f-ed29-44e4-90a4-ad946771746f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 127
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "64e6185b-6fdc-461c-8f1f-e7700b0ad029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 102
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b42bbe56-e511-4f41-8137-4cb76df64f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 127
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a9a9b11f-05e6-4078-be32-786b218927b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 93,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c1ec4344-50d8-40fd-b68d-55316b6d41fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 60,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "94f35cef-91d7-4fb5-831f-55e5f4a7dc47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "825a861d-0757-4325-a522-3279073b4529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 88,
                "y": 127
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ef52960d-791c-4c13-be02-1fe23d870994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "238fc6ac-1810-4707-9001-27818ee8084a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 127
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ebce965f-277e-4f9c-8b75-90d2f181431d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5851eced-90e4-4ffb-8f64-148bbbcb7abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "504bf210-0152-42e0-b50c-c7cbcc57d0a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 91,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0d4d8748-fafc-4a78-95c3-ab0aed8bbf50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 192,
                "y": 27
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c2a575b6-aa39-4da8-9ff8-efbbec5ded0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 114,
                "y": 27
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "08f7ca7a-7d4f-426d-8f47-d3c527bb9b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 104,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6a935a19-e004-4d16-a55d-72ffc986345d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 166,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8e6cd6d1-e228-4988-93b4-af9f2f92709e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 82,
                "y": 27
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "aee08099-15af-4069-b942-e2944783bccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "94e02dc0-480f-4635-a2d4-681838856e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2e6b2ed7-1446-4cef-8942-3655d91b44f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": -2,
                "shift": 6,
                "w": 9,
                "x": 129,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3a21ea66-7168-4820-a151-d417a260029f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 162,
                "y": 27
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2aac0c2d-c534-4567-956a-763d534fb21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 65,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c6385357-da77-4130-af41-358832b1d9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4aa9df18-5165-45a7-be0e-8c372f883b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 146,
                "y": 27
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1a85149e-1a97-4ec1-854c-f706624f6fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "84297def-0dd8-4219-a9ef-528b48a44e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 52,
                "y": 77
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "817f8636-6162-4895-8c99-e6bbb558389f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9db83b52-d3d2-4603-8e93-617b79141655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 90,
                "y": 52
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "175fafe0-be98-44c3-aba1-dc6697a1a7f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 92,
                "y": 102
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c342923b-5ba3-4339-8843-c988a929d285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 104,
                "y": 52
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3beeceb0-d51f-4ef6-b65a-75536bdfdbe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 98,
                "y": 27
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3f169c3a-57d0-4ca8-a404-d861d6336cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 177,
                "y": 27
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b0d0eaf9-a41f-4950-aedf-1486132126e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f6f5ec56-e5a6-411a-befb-c4151427d307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 207,
                "y": 27
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b7f66b1d-1801-4fea-8f85-2347c83007aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 118,
                "y": 52
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fb569503-0890-4999-818c-4a7ab1cbd716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 52,
                "y": 52
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a7c09f67-c734-4742-856f-e71ddb2b20ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 219,
                "y": 127
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6695841b-6deb-467a-a634-b04528ccfd45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 197,
                "y": 127
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6ea87702-c508-418d-aa0a-775d5f86e96a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 212,
                "y": 127
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "42adbc8f-edf5-4d77-bbf9-47a1592df5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 42,
                "y": 152
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1329ea16-502d-4ca2-8c84-afc811f4bd30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 52
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d897027b-db3b-4541-9c3d-f3f34a8fd3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 137,
                "y": 152
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5e652474-2d8a-4260-bfb2-2fedbe97075f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 237,
                "y": 77
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bcb1a64e-664f-473b-8983-ad497c90b7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 77
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5d921d2e-41fd-4507-b35b-02a7857f4257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 127
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "88d629fe-d9dc-4c13-9ae4-620e13fc3c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 158,
                "y": 52
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "94bfc71b-c015-4b74-a5ab-e844b0bfff48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 114,
                "y": 102
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "57f6bdbc-d774-4eb6-acee-cf67b396a6e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 240,
                "y": 102
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "26d1846c-4b19-4f41-9c84-8b74227f9f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "021e7c1b-7c0c-49e9-9cba-320a89c913dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 39,
                "y": 77
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "50ee7af6-3fd6-48f0-a075-741046b7f68b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 234,
                "y": 127
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ebd4409d-a1a6-4c38-96cb-6462d92d00bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 205,
                "y": 127
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ae1843bc-78aa-4ffe-a974-fcb8261c02f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6f1157a9-7d2a-472e-aef6-dfa21f74c290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 10,
                "y": 152
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "899770ab-9170-4bd0-baf0-03fcff11aa36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a6f5b3c7-78c6-4c1c-a164-564784e4d0e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 26,
                "y": 77
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "dd5d4959-5df0-4e76-bec6-9934c41c04f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 102
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ec005f8d-eaab-4584-8b31-04eb35c262e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1e5c98ac-6185-4d76-9fba-1cd943cdfaaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6b41555f-d93a-4649-b70d-9b5c9c47b2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 52,
                "y": 127
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d5b73689-75cd-4252-9e45-c4df4a72e740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 162,
                "y": 127
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "50cc25b3-5dde-40e4-a969-b36a0abc869c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 144,
                "y": 127
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "465a588a-63bc-4ad1-89fe-bb9453e6a7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 171,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "cb303260-114a-4bca-8bd3-b4339cd88f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 178,
                "y": 77
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "56174f0c-db84-4e6a-9ff2-86dfc66b4b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b833db41-8d67-4532-a9ff-2867ba6ee5cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 77
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8d762e7f-be93-4670-b140-bc2ef7203659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "23f7208b-4f42-4736-85ce-919b86098ef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 26,
                "y": 102
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f3920cf5-75d5-40c9-aa4e-ddcea3faa43c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 127
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "50d3a3d4-f20d-4736-a653-ae8cbb022139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 127,
                "y": 152
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9daac0a4-65b7-4e27-8ad9-8303777bff83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 107,
                "y": 127
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3e7b1d3c-90f1-4cd0-8647-7249c3e363d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 124,
                "y": 127
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "4a9de002-66a2-480a-b49a-a224d1fd5102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "3e78df1a-6205-4d0f-aa03-ad9cce995e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 130,
                "y": 27
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "52578204-a34a-489f-b426-1e7ea7b95148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 242,
                "y": 127
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "d19b6652-1645-4fbc-bf58-0803a9655e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 18,
                "y": 27
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "3a0503f1-e43b-448f-99e9-6f910d711436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 50,
                "y": 27
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "bf0cdedf-1049-43b6-9174-02a204cf50f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 66,
                "y": 27
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "65b6cafc-fe42-4368-8e01-4d53fe173d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 225,
                "y": 77
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "542758ae-a149-46c5-922a-1ca726ee7c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 213,
                "y": 77
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "3009a1ce-9522-44ee-9fea-152f17281510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "83e578f7-639e-41cb-90aa-14f26c272257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 18,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 153,
                "y": 127
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "042dc971-4280-42cb-8148-97d4a00293a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 81,
                "y": 102
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "0f15afdf-05ee-40c0-8086-35079e07d5a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 184,
                "y": 52
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "cb196c68-d1ce-41d3-a9f1-11be4271fadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 197,
                "y": 52
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "3886139c-c5de-4e2e-8911-df227bee73e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 52
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "cd8419f8-1e4b-4dad-a32c-2c527e73691e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "670ac30b-5eee-4e59-b05c-75b1f040f5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 78,
                "y": 52
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "3b84b173-751a-4b44-b278-a266daec2a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 210,
                "y": 52
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "af9975df-530e-47c5-93ef-7e23d061b057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 103,
                "y": 102
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "aca114cb-6ae0-4777-9a6d-4db3ab9fafa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "4ace4dc7-0429-4968-afd5-44efbaa066cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 223,
                "y": 52
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "67626d88-574c-45a6-87cf-95446bb4a368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 236,
                "y": 52
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "a5e9f5eb-78e4-4bca-968d-f52af1d39b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 226,
                "y": 127
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "c5172395-9be2-449d-bc99-3fd3840c8f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "f57a6f10-9d11-45cd-b840-e769ea34182a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 66,
                "y": 52
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "72177107-b4c3-4fe7-a401-cd4b160ba055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 132,
                "y": 52
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "929fbb01-3322-4903-a6e9-48f404847f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 62,
                "y": 127
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "bc2ef3c7-bde5-4906-853d-8932eaf7f514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "9144afdf-790b-4bdc-a484-e48f7649042c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 140,
                "y": 77
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "f00b0831-cc2f-4a5c-8eb1-9790d723ef4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 15,
                "y": 77
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "ede6eaf0-fa5c-42b8-a739-02d21a7d21d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 158,
                "y": 102
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "2fa03462-a93b-4d78-afcb-d303e18185ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 38,
                "y": 52
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "c248b472-9da9-463d-8b30-308dc60b48b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 37,
                "y": 102
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "94bb9b66-37fb-42e8-ab75-e72513d8495c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 222,
                "y": 27
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "59a60d67-bf0f-4b9c-9aec-d5ce449a3349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 127
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "eeff4f64-f52d-44f1-b213-afbbf3e476bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 59,
                "y": 102
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "ab9a7142-5ffc-4eb5-8ca6-4514e27e5636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 115,
                "y": 127
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "71a7c919-cff2-4e04-b8f4-c3fbfe21621d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 179,
                "y": 127
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "8da1f22c-5c51-474f-b80e-a53c389d4918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 13,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 150,
                "y": 77
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "07a43efa-000e-4ef2-88fe-fcb12340a495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 7,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 154,
                "y": 152
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "6be54ff7-668d-4554-84e2-1a8b680a22a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 144,
                "y": 152
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "0665794f-5f9c-4c7d-9039-05dc5ec14a45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 83,
                "y": 152
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "0ac43c4b-26c2-4e9b-b3b9-2f4eca02250f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 131,
                "y": 152
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "921094ec-a643-47da-970c-ccb566051381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 7,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 152
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "0853730f-f5d7-4025-9fa1-ababdceb87c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 152
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "9e20d70b-0c40-4da7-b45c-7e337417f96a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 210,
                "y": 102
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "b44ee68b-0626-4bca-9ed1-fd581201daf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 152
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "36198235-b50a-4e61-8d1d-4a63dbb619c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 237,
                "y": 27
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "824a6fd0-ff22-4fe7-9634-d4ab361e9fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 149,
                "y": 152
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "09360dd4-1eac-451a-b262-5bddd9271b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 103,
                "y": 152
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "6c7993a6-3ad9-4c16-a5d6-b79851b9a011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 9,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 31,
                "y": 152
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}