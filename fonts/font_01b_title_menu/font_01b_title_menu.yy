{
    "id": "ce131ca2-bf8e-4041-9362-1fa9a6636e75",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01b_title_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Spyced",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "46f3d383-a112-43f4-82c2-769c76e48819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 92,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 296,
                "y": 284
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3b33b1f9-3d14-49db-9c6b-a2cc8d88b1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 92,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 477,
                "y": 284
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "56deb975-a0d2-4ded-8df5-11b3399c4ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 92,
                "offset": 3,
                "shift": 22,
                "w": 15,
                "x": 460,
                "y": 284
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0a818514-1046-498e-bb05-cdf5f7ae6793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 92,
                "offset": -1,
                "shift": 71,
                "w": 72,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "789d357b-7835-4362-a59b-26e27ffbd3be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 92,
                "offset": -1,
                "shift": 40,
                "w": 37,
                "x": 201,
                "y": 190
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "202a51d3-33b1-404f-957d-110e3759fc15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 92,
                "offset": 0,
                "shift": 63,
                "w": 58,
                "x": 549,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e6b73cc1-7368-4172-ad26-17811c733e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 92,
                "offset": -1,
                "shift": 61,
                "w": 61,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2b783e23-002e-4f94-8ce6-66af3761acfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 92,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 575,
                "y": 284
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "080b35d9-ee1a-4b75-96d0-f54a34baf52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 92,
                "offset": 1,
                "shift": 30,
                "w": 26,
                "x": 87,
                "y": 284
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4882e2dc-951c-4f60-84b9-0435a60e20dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 92,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 938,
                "y": 190
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "aa796b79-e865-4dd8-b456-b697c611ed5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 92,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 428,
                "y": 190
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f5e93768-a48f-4084-989d-8ded419227f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 92,
                "offset": 6,
                "shift": 39,
                "w": 26,
                "x": 31,
                "y": 284
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bc370863-5422-40c6-8a70-876b4316ce64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 92,
                "offset": 1,
                "shift": 18,
                "w": 13,
                "x": 510,
                "y": 284
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "51303608-19c9-4803-9653-adc8ab37bb18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 92,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 424,
                "y": 284
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e511081f-1f7b-4eba-86fb-41c4dba5cd99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 92,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 565,
                "y": 284
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5e1489aa-d660-4f0a-8bd9-dba94c78f52a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 92,
                "offset": -1,
                "shift": 34,
                "w": 34,
                "x": 465,
                "y": 190
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e913b434-6dcf-4edb-a05a-4138501a7f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 92,
                "offset": 1,
                "shift": 43,
                "w": 40,
                "x": 614,
                "y": 96
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b70cba56-7651-438d-9808-90cd2ef0dd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 92,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 340,
                "y": 284
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8c011f1f-b581-4bf0-b8b1-8b050dd66fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 92,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 501,
                "y": 190
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bd56052c-2e69-46bb-bc3b-970fc420a2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 92,
                "offset": 1,
                "shift": 39,
                "w": 38,
                "x": 122,
                "y": 190
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8975c5df-34fd-4ae5-89fd-1495992a3aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 92,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 82,
                "y": 190
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d1d523b6-a4d9-435c-a193-1e68dc8c626f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 92,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 442,
                "y": 96
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8261cdc1-1dae-4a91-9d23-7bf5a4b868db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 92,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 738,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "75607d8f-103e-4562-bcc7-9c65d61e0bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 92,
                "offset": 0,
                "shift": 40,
                "w": 39,
                "x": 697,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "05bc1953-e46b-481b-b64e-8e54f9991da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 92,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 42,
                "y": 190
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "97a4ade9-4fc9-4005-8acc-1f9d09019704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 92,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 391,
                "y": 190
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "508ac54c-b78b-41fe-b2c0-6739ff9c58b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 92,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 555,
                "y": 284
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "396854cb-2198-4ff3-9fa0-67c68cd7bc44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 92,
                "offset": 1,
                "shift": 18,
                "w": 13,
                "x": 540,
                "y": 284
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ba984f45-910e-435c-9b95-77df2e14ac65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 92,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 2,
                "y": 284
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "09f35194-58dd-433c-920d-6a9f45b28744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 92,
                "offset": 6,
                "shift": 39,
                "w": 26,
                "x": 59,
                "y": 284
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "71f1b0c6-9d5a-4467-ac50-0fd3472cb3b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 92,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 967,
                "y": 190
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6b212a9b-879c-43bd-af30-8b640695cba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 92,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 879,
                "y": 190
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8dc2a8bd-1d48-4bb8-a403-56d8d7bc5f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 92,
                "offset": 1,
                "shift": 55,
                "w": 52,
                "x": 667,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "371ff576-8a6e-4957-bc93-d3abde39ad1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 92,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 485,
                "y": 96
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9fe7349a-e28c-4312-9467-d0271088c601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 92,
                "offset": -1,
                "shift": 47,
                "w": 46,
                "x": 303,
                "y": 96
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6dec959d-5ea6-4fb5-9639-889250d5717e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 92,
                "offset": 1,
                "shift": 49,
                "w": 48,
                "x": 106,
                "y": 96
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4a0a8675-e54b-4526-94f8-849169b42530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 92,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 399,
                "y": 96
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6943e8ec-7e2a-41e4-b0b8-f1fdf0e3fd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 92,
                "offset": 1,
                "shift": 42,
                "w": 39,
                "x": 779,
                "y": 96
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a11adda4-65c9-46e0-86bc-a21327b06fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 92,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 861,
                "y": 96
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f6ef15bd-3508-4a52-aedd-4ee363f61474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 92,
                "offset": 1,
                "shift": 51,
                "w": 47,
                "x": 206,
                "y": 96
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ead48833-cb7e-4147-93d3-e20fea88f7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 92,
                "offset": -1,
                "shift": 52,
                "w": 51,
                "x": 775,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b761a6e2-41c4-43ae-a7dc-07c9bf90305c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 92,
                "offset": -1,
                "shift": 19,
                "w": 19,
                "x": 382,
                "y": 284
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "135caa67-7576-4408-b3fe-9d6f51660aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 92,
                "offset": 1,
                "shift": 40,
                "w": 39,
                "x": 820,
                "y": 96
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5c8e74a8-d09d-49fc-b9ca-080f10510cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 92,
                "offset": -1,
                "shift": 51,
                "w": 65,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b5b6af49-bc3e-4058-9e3d-611151cf608e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 92,
                "offset": 0,
                "shift": 24,
                "w": 51,
                "x": 828,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2657506c-5a85-45e2-9c94-6ae958a6e570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 92,
                "offset": 1,
                "shift": 66,
                "w": 64,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "285a23c5-5855-4b60-a67d-cfb05e2cd441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 92,
                "offset": 0,
                "shift": 52,
                "w": 48,
                "x": 156,
                "y": 96
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "62925e02-4261-4d96-bb98-e451bf4ca46e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 92,
                "offset": 1,
                "shift": 53,
                "w": 51,
                "x": 881,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b67cf0e5-557d-4496-a9c5-5408d4b90307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 92,
                "offset": -1,
                "shift": 39,
                "w": 38,
                "x": 942,
                "y": 96
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1cd05711-3c95-4b3d-b220-b1137c0692cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 92,
                "offset": 2,
                "shift": 50,
                "w": 46,
                "x": 255,
                "y": 96
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "50c5495d-79a6-4545-b19d-d00022ec7c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 92,
                "offset": -4,
                "shift": 42,
                "w": 64,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a58dff47-49fd-4753-907f-5b9558b0d71a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 92,
                "offset": 1,
                "shift": 38,
                "w": 37,
                "x": 240,
                "y": 190
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "70d0176b-690e-4b58-aa66-d953ef7a9cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 92,
                "offset": 0,
                "shift": 53,
                "w": 52,
                "x": 721,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "db0a674f-1c13-4ea5-8a8e-199346e2aea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 92,
                "offset": 2,
                "shift": 52,
                "w": 50,
                "x": 54,
                "y": 96
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "139547b8-f8ec-4c15-b732-49905243b5f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 92,
                "offset": 0,
                "shift": 52,
                "w": 50,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0327186d-6428-4bb4-9dba-c0eaebdf9a24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 92,
                "offset": 1,
                "shift": 61,
                "w": 59,
                "x": 488,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "537f9ed0-f658-43d8-9f36-2eab00264f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 92,
                "offset": -6,
                "shift": 51,
                "w": 87,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "aedc43b3-59c1-4e6a-8e81-d4aafccb7c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 92,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 677,
                "y": 190
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "87416261-4138-4f36-a793-c14b39ad349d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 92,
                "offset": 1,
                "shift": 42,
                "w": 41,
                "x": 528,
                "y": 96
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ba314dbd-6296-445a-a3f0-a5d825cdd66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 92,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 403,
                "y": 284
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2b0e92fd-ba55-4992-82e3-09ba2f5d652e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 92,
                "offset": -2,
                "shift": 30,
                "w": 33,
                "x": 572,
                "y": 190
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "319ec712-5f19-4ae2-8983-3f49fbd340dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 92,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 361,
                "y": 284
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a030f393-f6e2-4095-9ad2-523b41bc4059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 92,
                "offset": 3,
                "shift": 40,
                "w": 35,
                "x": 317,
                "y": 190
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "26d67696-aa09-4520-b354-b9da46922f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 92,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 982,
                "y": 96
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5028aa11-95a0-484d-9612-988d49822a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 92,
                "offset": 6,
                "shift": 40,
                "w": 23,
                "x": 223,
                "y": 284
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9a35d772-6436-4e34-8755-19324bfaa7a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 92,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 712,
                "y": 190
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fc41656f-3b41-4844-97ff-4ca398e7c70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 92,
                "offset": 1,
                "shift": 42,
                "w": 39,
                "x": 656,
                "y": 96
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c51698b7-27bb-4594-9714-b0be67c97c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 92,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 814,
                "y": 190
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a9f2381b-afed-4636-8539-67771b7b97fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 92,
                "offset": 1,
                "shift": 41,
                "w": 38,
                "x": 902,
                "y": 96
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bf1a74f8-a1ff-401c-8482-9971da15206c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 92,
                "offset": 1,
                "shift": 36,
                "w": 35,
                "x": 354,
                "y": 190
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c192406f-506a-4a0e-9ecd-e87c09694b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 92,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 318,
                "y": 284
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "278ffd31-38a2-488f-918e-6bad7005e04b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 92,
                "offset": -7,
                "shift": 35,
                "w": 41,
                "x": 571,
                "y": 96
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f0a734a9-aa28-47f0-b431-8c49c662453b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 92,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 607,
                "y": 190
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "97cb5ba4-f57b-4f33-b184-31f62b874b96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 92,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 525,
                "y": 284
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f445b582-25ad-4fda-9b9f-9cbb24be64a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 92,
                "offset": -7,
                "shift": 15,
                "w": 21,
                "x": 273,
                "y": 284
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d7da4e92-06ad-4aa5-bc6c-dd715a16abf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 92,
                "offset": 0,
                "shift": 30,
                "w": 56,
                "x": 609,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5fbc2348-07de-4b65-95d7-7a3f2d879b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 92,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 443,
                "y": 284
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bf0ace2c-a2dd-48c4-8b6d-dc20ddffe0b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 92,
                "offset": -1,
                "shift": 52,
                "w": 51,
                "x": 934,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "080f1d5f-1448-44f8-a6f9-2bfe34efefa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 92,
                "offset": -1,
                "shift": 33,
                "w": 32,
                "x": 780,
                "y": 190
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8e25f992-f591-45d7-993f-194621f39ec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 92,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 279,
                "y": 190
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "38011c54-276c-439b-84a4-dfa5651d056e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 92,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 642,
                "y": 190
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1f2f1eb5-ca3f-4cff-9b66-b34e91bebc92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 92,
                "offset": 0,
                "shift": 35,
                "w": 33,
                "x": 537,
                "y": 190
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "58a1e166-81f2-40d8-aa8b-7daaac026077",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 92,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 115,
                "y": 284
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "305312d1-64c1-457c-b395-01faba4f5bae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 92,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 248,
                "y": 284
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7885184d-1eed-40fc-9858-2328b03cf392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 92,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 170,
                "y": 284
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a7653b01-f69e-4c49-9064-6cb38643635e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 92,
                "offset": -1,
                "shift": 30,
                "w": 30,
                "x": 847,
                "y": 190
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "72522afe-80fd-4b2d-969a-7a478eee3208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 92,
                "offset": 0,
                "shift": 38,
                "w": 37,
                "x": 162,
                "y": 190
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7f670b2c-57c0-4058-8d07-15bdd0412f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 92,
                "offset": 0,
                "shift": 61,
                "w": 59,
                "x": 427,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3253f5be-ae53-45f2-b293-dceda7fb340e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 92,
                "offset": -5,
                "shift": 40,
                "w": 46,
                "x": 351,
                "y": 96
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8817607e-3481-473d-93e2-f1c6185d5e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 92,
                "offset": 0,
                "shift": 29,
                "w": 27,
                "x": 909,
                "y": 190
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "80c73d37-ea9d-42f8-86b7-0717f2c13184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 92,
                "offset": -1,
                "shift": 31,
                "w": 32,
                "x": 746,
                "y": 190
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7c5bdf6b-ca76-4be5-aef9-a64379d89da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 92,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 143,
                "y": 284
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "042b1665-463c-4de5-a220-f343ce28d440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 92,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 494,
                "y": 284
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c6e8efd2-234c-4705-afaf-03300f93eb87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 92,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 197,
                "y": 284
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a919bf7e-2153-412e-8ee8-47b93c6d7d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 92,
                "offset": 0,
                "shift": 40,
                "w": 38,
                "x": 2,
                "y": 190
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ad9fc64c-0156-4a90-9bf9-71d564f9e551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 36,
            "second": 88
        },
        {
            "id": "0878f2f4-6f1a-4bcf-bd79-a3952a95f6e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 41
        },
        {
            "id": "e92a22ad-b5da-4b26-b976-cdae7370424a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 47
        },
        {
            "id": "62b65bf9-6c77-4474-9677-af30038b27a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 38,
            "second": 51
        },
        {
            "id": "7f83bf65-a311-469e-96d1-61cb19805550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 38,
            "second": 74
        },
        {
            "id": "87c72fe6-3aab-464e-9449-f5482c45c3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 38,
            "second": 84
        },
        {
            "id": "286886db-c3ee-4c65-9d6b-16df568c8e9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 38,
            "second": 86
        },
        {
            "id": "bcbf3074-d983-471c-b34b-15444176dd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 38,
            "second": 88
        },
        {
            "id": "f157c74a-59ab-483a-a791-814fbd945298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 38,
            "second": 89
        },
        {
            "id": "dd0447ad-75cc-474e-99ad-86df750e6025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 90
        },
        {
            "id": "f00b761b-1592-41c5-923c-17e29b461254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 92
        },
        {
            "id": "d34df86c-a605-492e-bfcf-066b57e3f1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 38,
            "second": 93
        },
        {
            "id": "8fc424ce-d074-49a7-833c-74f9261d3af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 38,
            "second": 125
        },
        {
            "id": "06792d13-8715-4a49-8c05-4a7fd04c1d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 40,
            "second": 88
        },
        {
            "id": "cab633d4-74a0-4e69-974b-a34b3dc972a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 40,
            "second": 119
        },
        {
            "id": "67f39a3e-22af-48b4-bafd-b14c0fccb051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 42,
            "second": 88
        },
        {
            "id": "fdaf13e4-56f7-4359-8a4e-8d5246f2185c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 48,
            "second": 66
        },
        {
            "id": "6685b653-5447-46b7-9f18-d04df3735c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 48,
            "second": 88
        },
        {
            "id": "b1b59519-f76d-4de6-989b-3f5bf7dda9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 88
        },
        {
            "id": "e97a634f-6e08-409e-9f4c-dab9c86e7ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 66
        },
        {
            "id": "81dc859c-12fb-4988-9481-efefd77fdd3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 50,
            "second": 88
        },
        {
            "id": "55b15a0c-4da9-44cd-8088-a565c3f9c33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 51,
            "second": 88
        },
        {
            "id": "c28fb3f1-9d7f-46ae-9d1f-22ab57bcf4c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 66
        },
        {
            "id": "075cd4c2-020d-4a23-a64e-262dedef0d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 52,
            "second": 88
        },
        {
            "id": "81384e95-499b-40df-b8c9-a598cac72cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 53,
            "second": 88
        },
        {
            "id": "58f2ef24-7b7e-44c5-a1d5-fde417f2ef20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 53,
            "second": 118
        },
        {
            "id": "01109dd3-1022-4218-b1d6-c460c4cc9f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 119
        },
        {
            "id": "3b2e0997-7613-42ae-979b-a23eefac0cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 54,
            "second": 41
        },
        {
            "id": "351f9e5b-2642-4645-a375-ca9166eccc70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 54,
            "second": 51
        },
        {
            "id": "2e1bd775-5db1-4943-8bb6-9cb1fcb96b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 53
        },
        {
            "id": "56065f1f-c722-40fb-a228-a21f3a105dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 55
        },
        {
            "id": "694879db-58e1-43a7-bb8e-8847417275aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 54,
            "second": 57
        },
        {
            "id": "9eb1e842-eb4a-4861-b2e2-67ee65b988e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 63
        },
        {
            "id": "5e75c0a7-61f2-4ae8-a59f-e9cf610cf603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 66
        },
        {
            "id": "268cbc61-c867-4f5b-931f-7ff28f5738c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 68
        },
        {
            "id": "5371bd33-6665-4559-b9ec-57b4549ed51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 72
        },
        {
            "id": "0b14ba85-330a-4e24-b241-a0ddfa6146cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 73
        },
        {
            "id": "9e6e4f9b-aa40-4390-8921-748e8890d1df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 54,
            "second": 74
        },
        {
            "id": "f5cf7e57-665b-423c-96c5-8deba812b851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 78
        },
        {
            "id": "f4f1bb1a-a3f2-4055-9d78-f81e0bc09a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 80
        },
        {
            "id": "0ffac714-f73d-4377-9dce-ac28c09e1399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 82
        },
        {
            "id": "ded5f993-09b9-48d1-ac9a-3730905253ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 54,
            "second": 83
        },
        {
            "id": "cf38dbad-9717-4902-ae35-0ac53c3f1ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 54,
            "second": 84
        },
        {
            "id": "15918a68-498a-4a09-ad08-c9d117f4a2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 54,
            "second": 88
        },
        {
            "id": "b6957f51-abb2-4ab9-974b-4d632086298e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 54,
            "second": 89
        },
        {
            "id": "691267ef-651c-4902-b748-41bb26312931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 54,
            "second": 119
        },
        {
            "id": "230990b9-9878-4caa-a32c-4f311c8795b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 191
        },
        {
            "id": "6989932c-cc9e-45a7-a09f-b69db3b7ee3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 192
        },
        {
            "id": "9f80b04d-b8a7-40a3-95d4-36cb95f3d28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 193
        },
        {
            "id": "073ac469-a314-4be7-867d-5282f8c598e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 194
        },
        {
            "id": "0cb5313d-ed3e-4f94-8ba9-cc130aba1a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 195
        },
        {
            "id": "ab6ad08e-f861-472f-b9c5-31b18e9b2718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 196
        },
        {
            "id": "c22c6c86-43cd-416d-993e-f7e1c6b6a3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 197
        },
        {
            "id": "28ea1973-aa73-4202-ba03-fdf1b4e45ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 54,
            "second": 221
        },
        {
            "id": "493bef74-d6ef-417e-ac9c-d0ad7885b218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 55,
            "second": 54
        },
        {
            "id": "e19673f8-8626-40b9-ba31-10467992e3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 55,
            "second": 56
        },
        {
            "id": "8549c7b6-d3f0-4b02-b4d2-bc6537d8555c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 55,
            "second": 88
        },
        {
            "id": "a0130ac9-50b8-4c54-a168-4212313625d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 55,
            "second": 119
        },
        {
            "id": "75ed1e6f-9452-4bc2-a7f6-6aee9f635468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 56,
            "second": 51
        },
        {
            "id": "bfd8f725-413a-49e3-89c8-d325840b3441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 66
        },
        {
            "id": "02a61560-8f81-46a6-871b-839729f4ff1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 74
        },
        {
            "id": "d709808c-c854-4d02-a301-8a8ab31af3ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 80
        },
        {
            "id": "aff7dfda-79cf-445d-b88c-f549d84f6307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 56,
            "second": 84
        },
        {
            "id": "bc2eef6b-6041-4dfa-ae42-acc00fd89942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 56,
            "second": 88
        },
        {
            "id": "c59af62d-42f5-4405-bd1a-28b947255281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 56,
            "second": 89
        },
        {
            "id": "41333257-b65a-4776-8225-d7df2feadb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 56,
            "second": 221
        },
        {
            "id": "2d747cc6-d556-42c0-9f76-d640e72d79bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 57,
            "second": 54
        },
        {
            "id": "50bbfe8e-6c46-4934-8f42-cee5d8393757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 66
        },
        {
            "id": "13e8b1bc-f90f-4421-8c23-8d0f6085ab14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 68
        },
        {
            "id": "786a078d-be77-47ac-920a-49038de49795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 57,
            "second": 88
        },
        {
            "id": "e3c88fe6-fb57-4ab9-a8d3-453f5ed49ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 89
        },
        {
            "id": "39a36aa6-3e66-49db-a7e0-37ad978ff643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 63,
            "second": 88
        },
        {
            "id": "c06ef1b3-eac3-47af-bd0a-9031aab5aeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 64,
            "second": 88
        },
        {
            "id": "5d18039f-22b2-43a2-85d4-def18d8a3b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 39
        },
        {
            "id": "ffb6fdda-abea-4fd4-a3b9-d33511b5819f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 65
        },
        {
            "id": "4d0d5f64-bd10-4640-a7fd-90c94402c995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 66
        },
        {
            "id": "90f16b3d-13a3-46bb-a445-0f09fdd3d4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 68
        },
        {
            "id": "83399af4-83e9-4817-8714-feb10f43233e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 72
        },
        {
            "id": "3ac98733-03b7-4277-b487-cde0a1aa6680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 73
        },
        {
            "id": "5875767a-6b20-4500-b909-1a161f863c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 65,
            "second": 74
        },
        {
            "id": "a8996c01-619d-4df4-8c13-292c0e2f857e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 75
        },
        {
            "id": "bfb23920-140d-403c-918b-d8d62490d2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 76
        },
        {
            "id": "fe2f45e6-f90e-4b41-87c3-51643241c92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 78
        },
        {
            "id": "840ead53-8923-42f2-92a7-229f7dd0a269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 80
        },
        {
            "id": "450a55aa-76f1-45fd-8294-62869e77030a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 82
        },
        {
            "id": "0dfd8224-69e8-4970-ad6d-1618b93732a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 65,
            "second": 84
        },
        {
            "id": "13dc22a9-4532-4303-b7bf-e9c593427c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 65,
            "second": 86
        },
        {
            "id": "d9d6ce64-a00d-4f70-9568-d5649abf1d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 88
        },
        {
            "id": "d3b9c168-97a9-4716-8399-d2b02aed3a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 65,
            "second": 89
        },
        {
            "id": "b8205d27-dccd-407f-919e-a09ddaa41887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 90
        },
        {
            "id": "5ba88315-e2ca-4ff1-a1bf-87b372029c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 98
        },
        {
            "id": "af79091b-41dd-4ef8-b147-c6abdd8aab65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 100
        },
        {
            "id": "f5e6546a-fbb2-401d-8c0f-8394836b0852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 104
        },
        {
            "id": "f64c1aa7-8577-4cd0-92e6-aacea188b50b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 107
        },
        {
            "id": "dfd2f54c-df8f-43a6-8c02-dd26cae2c877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 108
        },
        {
            "id": "f72f0466-d435-42ea-866d-49b205f33644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 192
        },
        {
            "id": "58c494da-fe68-4774-9efe-373d2dd30a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 193
        },
        {
            "id": "a75a2ecb-adbf-440f-9622-620f998bad9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 194
        },
        {
            "id": "0a211bba-dc00-492b-adf0-2a2f74bfbf7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 195
        },
        {
            "id": "ce9eeece-dd11-4e10-8677-86b4fa931ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 196
        },
        {
            "id": "3204fb26-3c96-48f9-9b40-f9caa59680cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 197
        },
        {
            "id": "acfe7eb2-d9de-4f62-a232-9479afc59c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 204
        },
        {
            "id": "9b947f3a-65c2-4a6a-a48e-df3282af8a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 205
        },
        {
            "id": "c0e84d60-3dd1-4a13-88b5-00140db3cf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 206
        },
        {
            "id": "01212d6a-79bb-44e8-9935-98e0196bd8ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 207
        },
        {
            "id": "a62173b2-8cb9-4c71-9d7d-7b7479b821e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 209
        },
        {
            "id": "36ddd782-2c15-449b-9a4a-01092562c81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 65,
            "second": 221
        },
        {
            "id": "79adf076-84cf-4cd5-8a42-9089e9bf7639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 66,
            "second": 39
        },
        {
            "id": "4d9bd37b-0ef8-44b2-8d69-68e844051215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 66,
            "second": 41
        },
        {
            "id": "45467a03-6915-4b52-a019-20ff1a58dbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 66,
            "second": 42
        },
        {
            "id": "66d9bb94-39d5-4773-9a06-d11b39a1086c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 49
        },
        {
            "id": "f4426ea4-6f99-47f8-a286-37991fecd595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 50
        },
        {
            "id": "ef0cd2ed-2a24-4671-8ba0-7cdc8bfc03a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 66,
            "second": 51
        },
        {
            "id": "bd3d1a33-1474-4acf-b466-34dfd5d02b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 53
        },
        {
            "id": "96c4c802-b4d6-4e92-9c86-003623252ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 66,
            "second": 55
        },
        {
            "id": "4db635dc-e8c7-4d3e-965f-c67d36b01c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 66,
            "second": 57
        },
        {
            "id": "c0cbac2e-420c-4c1b-a78b-e714304fcff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 66,
            "second": 63
        },
        {
            "id": "f23ba1d7-6f78-48ce-ba70-0f073ac71bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 66,
            "second": 65
        },
        {
            "id": "81f2ad3a-39ae-455a-b289-de75a7394569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 66
        },
        {
            "id": "5f7720f8-1b81-4588-a07d-ecee7e4b1566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 68
        },
        {
            "id": "a074eb4f-0460-460c-a7bb-c7df4689e39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 69
        },
        {
            "id": "943606a8-32fe-4155-b251-c9aff15efebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 70
        },
        {
            "id": "d1d67bbe-ed43-4f1d-ba8e-b52f73f9f248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 72
        },
        {
            "id": "f2a83dff-327d-4252-ba1c-c943f78a5ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 73
        },
        {
            "id": "ce65cc97-13a9-4655-b7be-8a8bc812f030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 66,
            "second": 74
        },
        {
            "id": "873e79c6-7023-4dcf-965e-1bc0ac6815e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 75
        },
        {
            "id": "0f39c85a-9c9c-43dd-adb2-86ac717f02de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 76
        },
        {
            "id": "0d08cb4b-9b89-4342-b76f-04f653d9cec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 78
        },
        {
            "id": "9782a572-2292-4807-9ac3-b31e542df1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 80
        },
        {
            "id": "fa9289f6-8d25-4720-a638-4da3eeea6c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 66,
            "second": 82
        },
        {
            "id": "b54e9010-0027-43d8-8876-8e48edc730f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 66,
            "second": 83
        },
        {
            "id": "4d9dd8f1-b176-4ca8-ad6a-85dc194455b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 66,
            "second": 84
        },
        {
            "id": "fd63ef25-8e77-47ac-8b3c-d11377a2a82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 66,
            "second": 86
        },
        {
            "id": "c7a525ab-2e14-4f39-b212-59db0f4b8c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 66,
            "second": 88
        },
        {
            "id": "02fef1aa-4190-430a-9f02-2107cb4c1beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 66,
            "second": 89
        },
        {
            "id": "4d3b43d9-3e57-4c8f-9e8f-0e7adefba120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 66,
            "second": 92
        },
        {
            "id": "1c150304-bbe4-40a5-97aa-74d050d21b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 106
        },
        {
            "id": "664f61c3-2b30-4127-a0a6-3203b19153b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 118
        },
        {
            "id": "12b67385-d06b-4303-8ffb-5108c1274b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 66,
            "second": 119
        },
        {
            "id": "7d6d5ca9-41c7-4b2c-b165-66d42b520963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 66,
            "second": 191
        },
        {
            "id": "567fd510-84df-4c43-a0e7-984a1738fc8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 192
        },
        {
            "id": "7b10a092-6f2a-4b51-816f-c59521514c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 193
        },
        {
            "id": "dc06b651-bb49-4c2b-8ad1-69831b84b52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 194
        },
        {
            "id": "5b0ec6e5-a90f-4d07-a3be-ee8ee4395624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 195
        },
        {
            "id": "8e9cfbf8-3caa-4cb5-8292-1007b024b732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 196
        },
        {
            "id": "a2d9a494-1a42-4e17-b4a3-22b148e2b05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 66,
            "second": 197
        },
        {
            "id": "63c52ebd-213a-4bc8-a627-02b993554393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 198
        },
        {
            "id": "bde2bc3f-d3e7-4a6f-a14a-b2b0a6007ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 200
        },
        {
            "id": "678c313e-78dd-48da-8e39-b94a03872e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 201
        },
        {
            "id": "c5d4c421-0da8-4c65-861f-5de791677b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 202
        },
        {
            "id": "17f50d5c-1dd9-4bf6-a439-491168350905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 203
        },
        {
            "id": "b77d2fc0-bfe9-4e57-8075-4342d53339d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 66,
            "second": 204
        },
        {
            "id": "77130e68-b049-48b5-b2a0-19c5c22ac2c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 205
        },
        {
            "id": "7b304d3b-e3f1-4897-b54f-793a311a9c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 206
        },
        {
            "id": "f93ac0c2-eb3a-42e9-937a-1e8a3e931bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 207
        },
        {
            "id": "40d9fe69-845a-406c-90e3-fc9097a3bcc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 208
        },
        {
            "id": "7a12e098-d6fe-4be7-a0e9-b2ddc8a5c267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 209
        },
        {
            "id": "6d3d67a4-f8ff-4d41-b762-306f0fa0e869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 66,
            "second": 221
        },
        {
            "id": "4ad28eaf-1fd5-48aa-90c6-58a09538e322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 66,
            "second": 8482
        },
        {
            "id": "0685719e-7478-43ab-9190-822e09d527f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 67,
            "second": 52
        },
        {
            "id": "fec432ba-5c3f-4a5f-b1ea-c4dca58c5e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 53
        },
        {
            "id": "57a510af-d7c3-4e8c-b962-b20d79aef1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 67,
            "second": 54
        },
        {
            "id": "7f6dd7d2-9036-45d2-8981-6bef3fab7ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 67,
            "second": 56
        },
        {
            "id": "18971619-c4ae-426c-a76f-d2c45770523b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 67,
            "second": 64
        },
        {
            "id": "551bbdde-204a-4793-ab86-ce7026f047e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 65
        },
        {
            "id": "086decec-b7c0-4eae-9f31-2fce284e1447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 66
        },
        {
            "id": "0e2c44e9-14b1-46d4-8f6f-b345a2c680c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 67
        },
        {
            "id": "c9a322ea-e09e-4c81-9e96-2c53d1300ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 68
        },
        {
            "id": "7aec3ac0-a480-4714-a753-367331fc0466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 69
        },
        {
            "id": "158750bf-4bfd-4c51-88af-cb87cd1830a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 70
        },
        {
            "id": "28176853-bc21-4c72-a526-4aa24cc02627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 71
        },
        {
            "id": "e9ee14ac-dccf-4eae-a5e9-169d82ab8f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 72
        },
        {
            "id": "32ecb4af-a99e-4bab-bc6d-0bbb163bf4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 73
        },
        {
            "id": "9fc41da6-c6d1-47fd-ae28-25e499279cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 75
        },
        {
            "id": "5506d850-b688-4aeb-a4ab-a0d36d7ce7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 76
        },
        {
            "id": "6987cc2e-e5ed-44b5-ac54-8ae605963367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 67,
            "second": 77
        },
        {
            "id": "28679757-75ed-4a78-be43-6120b6468de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 78
        },
        {
            "id": "dd35859c-2e8b-47be-a8ff-f436fbb27f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 67,
            "second": 79
        },
        {
            "id": "9792d492-70a6-4115-96bf-14817ae2f85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 80
        },
        {
            "id": "84f3628e-98e1-47bf-a2a7-f6a4cb1401cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 67,
            "second": 81
        },
        {
            "id": "a5eb3e41-b617-4ee6-b1f4-66c3fe116c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 67,
            "second": 82
        },
        {
            "id": "028bf96f-43d6-49b9-9177-eabfe685e496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 83
        },
        {
            "id": "3d71cd67-311a-4328-904b-476735fd10c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 85
        },
        {
            "id": "a792fc09-6f97-40a7-98fa-065729a5770f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 88
        },
        {
            "id": "8ee8152e-6201-4eda-a49c-e548b80a2542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 89
        },
        {
            "id": "af5e561d-a86b-4fd6-bde7-0e5c01878b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 67,
            "second": 97
        },
        {
            "id": "1976dfa9-c0dd-4614-b3d2-ab0d2470f1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 67,
            "second": 99
        },
        {
            "id": "e27084e8-2922-4656-9b89-de8e79c20145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 67,
            "second": 100
        },
        {
            "id": "ac10a467-8f80-46f0-9fcb-c6d14b8b34f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 101
        },
        {
            "id": "e2bd5252-7914-40e9-96d7-4507466a36d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 103
        },
        {
            "id": "33d86829-e12b-443a-be3b-070395981bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 105
        },
        {
            "id": "a4e05487-f054-453b-989c-b4d196f55a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 106
        },
        {
            "id": "60eef5a8-a2d6-465a-88c2-321deb4433f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 67,
            "second": 109
        },
        {
            "id": "4a66a7a3-64d9-4579-bfb4-4b2a72e8bf8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 67,
            "second": 110
        },
        {
            "id": "76c87375-ce7f-4d25-a357-25d3ae07d5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 67,
            "second": 111
        },
        {
            "id": "e08148bf-68aa-4d4d-9a49-9bbd83cbafb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 67,
            "second": 112
        },
        {
            "id": "42c59965-d663-4839-b4f3-ed3a98436a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -26,
            "first": 67,
            "second": 113
        },
        {
            "id": "01bd7986-d70e-4bb0-a9f2-264671bc1b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 67,
            "second": 114
        },
        {
            "id": "3017ba25-2c8b-4ac0-b491-7eb4533f418d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 67,
            "second": 115
        },
        {
            "id": "c1fd5503-0443-4bfa-8903-cb9d293dabca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 116
        },
        {
            "id": "81c27f76-3358-4ddf-8135-04ca12958c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 67,
            "second": 117
        },
        {
            "id": "6b99f9b9-0bdb-4930-8a6d-c7de771114e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -29,
            "first": 67,
            "second": 118
        },
        {
            "id": "0fc91b87-a00d-4fc0-b81c-676722168e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -30,
            "first": 67,
            "second": 119
        },
        {
            "id": "96f08f74-0adb-4442-afe1-39011fe1664d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 120
        },
        {
            "id": "73d493e8-af05-4681-bfbf-98d1f205a1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 67,
            "second": 121
        },
        {
            "id": "a339e89b-b2bd-4b1b-8177-eb29d8999a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 122
        },
        {
            "id": "07bebc47-fca3-4ed2-97a4-860f0562a1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 192
        },
        {
            "id": "f05ee1a9-427d-484d-b920-55fc4fc08693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 193
        },
        {
            "id": "d7b19edc-6c73-45b5-8956-7c20c86c9337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 194
        },
        {
            "id": "123a9adc-9b21-4a1d-ad61-c1a2f5216c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 195
        },
        {
            "id": "6ad4f002-ae47-4c5f-90ee-e9356ff8c4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 196
        },
        {
            "id": "36919cda-dde8-4f28-86c1-5eb46c18600a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 197
        },
        {
            "id": "ed4c4f6c-9f40-4616-92b2-bc1bb074454e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 198
        },
        {
            "id": "89617420-5642-4681-9b3c-52ec13ac85e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 199
        },
        {
            "id": "c38bf431-2649-42a7-9d7e-a6ab3d4f0d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 200
        },
        {
            "id": "a9992f6f-6d28-4710-a7b7-b35dece13b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 201
        },
        {
            "id": "99dc2016-a1ba-4864-9da4-346d0f71ab62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 202
        },
        {
            "id": "c5d4f86e-8b8a-479c-84d3-0205f0ed76ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 203
        },
        {
            "id": "373f3ac1-a8f3-462a-88a2-5bb132784941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 204
        },
        {
            "id": "88cd7398-e057-44dd-83ed-c25cbdb97a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 205
        },
        {
            "id": "0c4d9756-f8cd-4291-b585-5041a7f2c6b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 206
        },
        {
            "id": "33f23d24-7aa8-4e87-b2a8-a2a2bcecca0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 207
        },
        {
            "id": "eb375ad4-7c9b-449c-b8ed-551de9292f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 208
        },
        {
            "id": "5106693d-6776-460a-ab24-298a82a9becd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 209
        },
        {
            "id": "a58dd2e1-e381-4dd0-af28-f51ecf7219bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 210
        },
        {
            "id": "160e02d8-e8b9-4fc3-b622-289d74213ad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 211
        },
        {
            "id": "ecff65be-04fd-4502-9bd1-e0d84288460b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 212
        },
        {
            "id": "25f4faa3-d639-4da6-81e8-0ee098650ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 213
        },
        {
            "id": "36932dc7-bd03-4bf3-ad84-92ad9320af61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 214
        },
        {
            "id": "8ec78a8d-7626-4c9c-85d0-0bc419f1f027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 216
        },
        {
            "id": "c566d1aa-4421-451d-96a8-c8848f07d050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 217
        },
        {
            "id": "6669eba8-ff54-4dee-929d-cca90967bf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 218
        },
        {
            "id": "4c8b8d5c-b9ba-4341-a856-bee56136f194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 219
        },
        {
            "id": "8683c88e-a365-45d4-bc0f-6da3a63fbb99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 220
        },
        {
            "id": "5a88da76-ebf2-49a0-b792-75a0b79ba30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 221
        },
        {
            "id": "39d46a62-f6dc-477d-a48d-eabe519b6fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 223
        },
        {
            "id": "82d93ee1-968c-41c2-a659-1725535be296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 67,
            "second": 224
        },
        {
            "id": "f3fdd37c-7fe5-487b-98ca-1284b654dcbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 225
        },
        {
            "id": "bbfaa096-3e3a-4cce-a842-387c30cbbf92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 67,
            "second": 226
        },
        {
            "id": "87ce8de5-30d9-415c-ba4a-dc3051a2ffc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 67,
            "second": 227
        },
        {
            "id": "a057921c-efe8-494a-8c6f-380ff0434656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 67,
            "second": 228
        },
        {
            "id": "cb2bc97f-ad23-4609-96fb-61d8f7ad86a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 67,
            "second": 229
        },
        {
            "id": "1b89be74-5d7a-484b-8b3e-dc8c9b6975b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 67,
            "second": 230
        },
        {
            "id": "c3794b9c-2ed5-4214-b8bc-4cc5f1bad5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 67,
            "second": 231
        },
        {
            "id": "4fd30e36-55a5-44c9-a0c2-d0f59acd4200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 232
        },
        {
            "id": "7692db34-2612-40ff-92d3-ed6623ee4dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 67,
            "second": 233
        },
        {
            "id": "67ce4e5a-7d90-4b93-8340-10d52313ca7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 67,
            "second": 234
        },
        {
            "id": "c0154594-d6f3-44d6-8b6d-feb55c0c3506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 67,
            "second": 235
        },
        {
            "id": "5ea2f149-a10c-4c44-8ca4-fac4843d9038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 67,
            "second": 236
        },
        {
            "id": "32334223-f3bd-4785-935a-a5daf7454c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 237
        },
        {
            "id": "1ad9eb39-3c70-470d-b9ee-0f6f701f61be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 238
        },
        {
            "id": "128171bd-4022-44fb-adc5-ff5596df1264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 240
        },
        {
            "id": "f3be90ab-936a-4d48-b9fb-5daa37188434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 241
        },
        {
            "id": "913a9fd2-2d32-4924-bee6-bfe36e08a45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 242
        },
        {
            "id": "534208c5-3fe6-4e09-97d8-948c14fb270e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 243
        },
        {
            "id": "134380db-f798-49e5-9b39-d43d86f18c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 244
        },
        {
            "id": "3195b9b3-5082-45c0-920a-d5efa516ece1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 245
        },
        {
            "id": "c7ae1ffc-3855-4b19-901c-1382d7a40229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 246
        },
        {
            "id": "b8bc7932-644a-4f92-b643-0e91047ef5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 67,
            "second": 248
        },
        {
            "id": "8664b0c0-4b3c-4c81-b8a5-bf1a2f858511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 249
        },
        {
            "id": "040a84b9-17d1-4811-9754-f96334d96ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 67,
            "second": 250
        },
        {
            "id": "d5861e7c-66fa-4f13-8358-d4c165212c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 67,
            "second": 251
        },
        {
            "id": "56ed12c8-9643-496f-8171-51cb3e14dba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 252
        },
        {
            "id": "6db6fd49-091f-4c75-b7b7-72efd2deb8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 67,
            "second": 253
        },
        {
            "id": "fe1c0fbe-dd6a-441e-ab20-7c35e8f46217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 255
        },
        {
            "id": "13d573bb-851e-4e5c-83ad-a353fb06521f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 67,
            "second": 338
        },
        {
            "id": "34d15642-4ca5-4245-a8cb-8bab9ed881b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 67,
            "second": 339
        },
        {
            "id": "9822b171-e2bd-4fa2-820c-85a6d5e202c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 65
        },
        {
            "id": "61b713de-831b-4b2d-b241-c05e36ead8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 66
        },
        {
            "id": "f4fe70d1-890e-49f0-b623-80c96b2cbe8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 68
        },
        {
            "id": "956a293a-de9a-4c3e-bbad-407f93c02bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 72
        },
        {
            "id": "7e24274a-694a-47ab-bd4e-3cfaa387142e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 73
        },
        {
            "id": "3c50438b-633f-4812-9568-8e26cf22699a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 74
        },
        {
            "id": "fe3df4da-7fad-40d7-be61-92ce39f6ec1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 75
        },
        {
            "id": "2603b2dc-1aef-48b0-bc60-08a47995b942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 76
        },
        {
            "id": "06a04d7d-d0e0-4c97-b405-9e8e6bd7d1fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 68,
            "second": 78
        },
        {
            "id": "bd02bb5b-b79a-4286-9a99-e94256153d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 68,
            "second": 80
        },
        {
            "id": "033176b1-1504-42a1-94db-857ef9bc4102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 82
        },
        {
            "id": "dfa5fc6d-da4a-4445-8937-84e00bc13990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 84
        },
        {
            "id": "4f61355e-13a4-4876-b1d4-ac971ccde4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 68,
            "second": 86
        },
        {
            "id": "c2376379-c519-4228-9256-e53a85893056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 68,
            "second": 88
        },
        {
            "id": "a60bcdf9-e820-491c-8600-ac4129118cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 68,
            "second": 89
        },
        {
            "id": "5e88fb56-a79d-43a7-a116-ef1e2702babc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 90
        },
        {
            "id": "e370c480-2c69-438c-842d-20ab31bfd318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 192
        },
        {
            "id": "da79cee1-7ac8-43cb-a8a2-bbd68caf5906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 193
        },
        {
            "id": "6b6f5ccb-b809-452e-a998-be9fcb17e60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 194
        },
        {
            "id": "5f1019a4-2f80-4545-b568-b8b758ebca02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 195
        },
        {
            "id": "aa463dae-888b-41bc-81f6-7ce86a6c35c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 196
        },
        {
            "id": "59f20d0a-f1a0-417f-bafe-3c45866f4ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 197
        },
        {
            "id": "ca16c145-8203-465a-8dec-a8ee92a098a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 204
        },
        {
            "id": "0a416940-1de3-4113-9ce3-49af2600bd3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 205
        },
        {
            "id": "26300de4-67ba-41e1-8f7d-73f5c5aa45f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 207
        },
        {
            "id": "c7cb5d42-ed74-4834-ad96-526535d35aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 209
        },
        {
            "id": "1038da09-1a10-4656-87b7-9ec0232ff6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 221
        },
        {
            "id": "76ddf69e-a72a-49b4-b533-2eb67ec22782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 48
        },
        {
            "id": "1723520a-0be7-44b6-9bc0-14f1c6a0aeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 69,
            "second": 52
        },
        {
            "id": "0fafdd10-c8fb-477a-97d5-8ba2f3ab4d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 53
        },
        {
            "id": "4450cf2d-88b5-47c5-9d6b-2ececa214aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 69,
            "second": 54
        },
        {
            "id": "eeb66482-9c80-4bf2-a889-18df0cae61d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 69,
            "second": 56
        },
        {
            "id": "195fc95a-c3f9-4cb8-a153-0ffe82d8177b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 69,
            "second": 64
        },
        {
            "id": "152d4b44-c7a5-4df7-95cf-54608b853c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 67
        },
        {
            "id": "6724b6a3-2b37-410a-9eef-af5ae29b8b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 71
        },
        {
            "id": "8c0a709c-caad-42a7-84fe-5100b512950a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 77
        },
        {
            "id": "b795be40-1bcc-4920-9f4f-734e5d6236b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 69,
            "second": 79
        },
        {
            "id": "25fa14a6-7c4a-4f3e-8adc-4d1a84a2c6ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 69,
            "second": 81
        },
        {
            "id": "7a3d39ac-f28a-41a5-b7e6-34e4851a441c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 69,
            "second": 85
        },
        {
            "id": "c97d6475-120f-493a-a3ab-eeafe2ad7420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 69,
            "second": 88
        },
        {
            "id": "3b6d4c48-df46-453b-8f0c-6ad1023cf375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 69,
            "second": 89
        },
        {
            "id": "82fd2c47-a81d-4e50-be5d-da1965f06a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 69,
            "second": 97
        },
        {
            "id": "9a3ad2d9-a37f-443b-b7d9-801c53d8ef1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 99
        },
        {
            "id": "9e60a26c-d609-49f9-8ed0-0b86265b0a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 69,
            "second": 100
        },
        {
            "id": "87fd300e-9b50-4b79-bfc3-b3314b765860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 69,
            "second": 101
        },
        {
            "id": "c1dbe6f4-def5-42ed-b4fc-93d9cc6ea6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 103
        },
        {
            "id": "396d03a1-d4d3-431a-9c41-ff897539632c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 105
        },
        {
            "id": "c85e19f0-e8cb-47ad-a318-3c3060e8515f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 69,
            "second": 106
        },
        {
            "id": "853c71bb-f299-4b7b-9444-d9cd63c575c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 109
        },
        {
            "id": "88fb0b2d-118e-455d-b84f-c34de0f801db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 110
        },
        {
            "id": "18cbc94f-6f8f-4c7c-a525-0ea355304444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 69,
            "second": 111
        },
        {
            "id": "63ecbc94-0c42-4c05-b13b-9ee580d25587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 112
        },
        {
            "id": "aeedd49c-5e12-4d41-bc3a-13244e3320cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 69,
            "second": 113
        },
        {
            "id": "74b308c5-052a-4990-a8cf-facba33f69b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 114
        },
        {
            "id": "5865964b-4202-4903-b199-65e7195bb0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 115
        },
        {
            "id": "da0daef6-c5c5-4ad8-9605-24c7d7ec912b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 116
        },
        {
            "id": "e137bac7-c0f9-4fc2-a150-d49500f6dfc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 117
        },
        {
            "id": "340d50bb-29e2-46e6-85d7-5b7f3bfd1e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 69,
            "second": 118
        },
        {
            "id": "1dbfa3a0-06a3-422d-b1c4-e50ba4aa5c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 69,
            "second": 119
        },
        {
            "id": "06ff75ff-a3bf-420b-aba8-cd684b11887e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 120
        },
        {
            "id": "43bc0d3e-f849-4849-b14f-8c52e9bfa705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 121
        },
        {
            "id": "87196e65-12ec-4fb8-b49f-412141fd4e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 69,
            "second": 122
        },
        {
            "id": "d890097a-9770-4456-82ed-6b3595faa0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 69,
            "second": 198
        },
        {
            "id": "bc02d153-5c03-4773-89ef-055dcc320dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 69,
            "second": 199
        },
        {
            "id": "0db517e5-3ea8-4ae7-b84c-dd291217fb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 210
        },
        {
            "id": "dcd9df74-ae58-4990-aeeb-172fdd999772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 211
        },
        {
            "id": "48f78640-d4a1-4896-9b0d-03dba059cc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 212
        },
        {
            "id": "2c3d2180-17c4-421c-b597-daa7375df470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 213
        },
        {
            "id": "ce3d4cc5-cdd9-47ce-9715-f6bfc43463be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 214
        },
        {
            "id": "577ea335-11a2-4c4f-ba30-8f8a4fbd5969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 216
        },
        {
            "id": "f76fe362-b305-4a23-9315-b4a0ae2b164b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 217
        },
        {
            "id": "88a75f75-e4ee-4293-b29f-b07fbd099371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 218
        },
        {
            "id": "6b7c6690-2fc5-4e34-a8d4-c6e55470df2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 219
        },
        {
            "id": "4b0c0df5-bc6b-469b-8dbf-473995254d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 220
        },
        {
            "id": "ae1556dd-ef69-46e8-abf6-d127694176ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 221
        },
        {
            "id": "f2fbe9de-c6f9-43f7-8e50-bff513d30404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 223
        },
        {
            "id": "ed2f175c-8323-4aa9-ad7f-70d5d4c2814b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 224
        },
        {
            "id": "ddffef48-f91b-49ad-bc61-a99d27f1243d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 69,
            "second": 225
        },
        {
            "id": "9337d4ec-7822-4c40-9b2b-1369190d6294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 69,
            "second": 226
        },
        {
            "id": "c078fc75-c894-46aa-9ef8-577e68d99b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 69,
            "second": 227
        },
        {
            "id": "0d411966-3fe0-4397-b057-90204b980045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 69,
            "second": 228
        },
        {
            "id": "e28b12e0-8d05-4de7-aa6e-1db447407977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 69,
            "second": 229
        },
        {
            "id": "af3bb0ef-351a-43a0-9b2a-850b3229ff10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 69,
            "second": 230
        },
        {
            "id": "211f81be-3eae-4f8e-91c1-dd88a386ce70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 69,
            "second": 231
        },
        {
            "id": "e11dc428-99ae-4177-ac85-e240d631a4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 69,
            "second": 232
        },
        {
            "id": "9dd8ab4b-5f89-4452-976b-1bd4ca2dc448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 69,
            "second": 233
        },
        {
            "id": "365109cb-2625-4552-9171-057d51dbc013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 69,
            "second": 234
        },
        {
            "id": "374b2bca-0573-49aa-a916-f48a8c9ba6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 235
        },
        {
            "id": "a222c37e-abb3-4611-9728-51666d549953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 69,
            "second": 236
        },
        {
            "id": "91ec9d5d-e79b-4072-b835-3a5d2457ed9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 237
        },
        {
            "id": "26713c68-3d59-4a6c-8c35-f5989d425619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 238
        },
        {
            "id": "b665deb2-0dc4-48f3-827b-ae11b54c552d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 240
        },
        {
            "id": "d4200d8c-127c-464b-bd12-37de4fca0416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 241
        },
        {
            "id": "ff5535fd-504f-4b00-b628-342ec7d634a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 242
        },
        {
            "id": "fc79f6f4-2cb1-470c-9465-836a4b3fb197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 69,
            "second": 243
        },
        {
            "id": "34a734b9-94d1-4763-848b-14cbb1ee0e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 69,
            "second": 244
        },
        {
            "id": "98e6e4e6-8ad6-4a6c-8cf6-b51e10322acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 245
        },
        {
            "id": "f87d968d-b1d6-4cab-9a4a-c8cb896385b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 69,
            "second": 246
        },
        {
            "id": "ad01aff8-e609-4f7e-bdbc-d3420dabf86a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 69,
            "second": 248
        },
        {
            "id": "cd6c51d5-371d-46d0-9322-ef9ea3aa0892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 69,
            "second": 249
        },
        {
            "id": "708d4d17-171c-43ba-b0b2-f14c1ba8e06b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 250
        },
        {
            "id": "d360a21d-6de0-4cfb-9053-5824c067c3d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 251
        },
        {
            "id": "7584548e-ee8c-4937-aabe-dee0e0be9bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 252
        },
        {
            "id": "cd395346-960a-49a2-a47c-5096a1a9b47a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 253
        },
        {
            "id": "c691e385-6ee5-4703-a7c2-2512dc02d3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 255
        },
        {
            "id": "f07ee498-fb64-4b81-983a-1f52e1edeef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 69,
            "second": 338
        },
        {
            "id": "e3d1b744-6357-4ac6-856a-e78a9d979690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 69,
            "second": 339
        },
        {
            "id": "e1382eac-d7b9-446b-ad6f-a4960cc4f571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 48
        },
        {
            "id": "a1d467c4-f726-4fc5-976b-c7ff5ae5617e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 70,
            "second": 52
        },
        {
            "id": "a0a2b39e-0d84-4d64-ac2a-b64d4851b84f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 53
        },
        {
            "id": "2d113ccf-925f-495b-b585-01932cbf322f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 54
        },
        {
            "id": "918f4e57-ec80-45d3-af8f-1013eb5459c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 56
        },
        {
            "id": "2f690121-d459-4e82-8fff-44bc7fa1131e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 64
        },
        {
            "id": "31802622-07a1-4923-a870-0a43e299ef11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 67
        },
        {
            "id": "f3b38fe0-1318-4ee3-9540-497742689e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 71
        },
        {
            "id": "87193e43-54a6-4399-9351-3c8f05f10bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 77
        },
        {
            "id": "ef7ed568-a514-47f7-80f7-85cea0132c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 79
        },
        {
            "id": "9c5d97a9-4d87-4428-8ba5-f0be469acbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 81
        },
        {
            "id": "5297ae6d-e709-405e-a26c-63beb8b01b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 85
        },
        {
            "id": "de07bf7c-8c87-4f30-abe6-f030f9fd1d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 88
        },
        {
            "id": "a903ddb6-c3f5-49c6-8175-b22ad90170b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 70,
            "second": 97
        },
        {
            "id": "3e81d1ae-ca24-445f-9ea7-fff8918cc7c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 99
        },
        {
            "id": "e805706d-a1d5-4e74-83b6-29dd96bbc779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 70,
            "second": 100
        },
        {
            "id": "67a7685a-afb0-4f9f-aa69-a5248a90d095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 101
        },
        {
            "id": "9fa0c86d-5e6d-487a-92a7-1c2d16fcb624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 103
        },
        {
            "id": "ce67a080-321a-4724-99ac-f97d8e004798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 106
        },
        {
            "id": "f33d80b3-cf9a-45cb-9b04-d07a7ff71224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 109
        },
        {
            "id": "f5e1a9c9-d7f3-4fa9-996c-8e9c43760510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 110
        },
        {
            "id": "a49df60d-842a-4f96-96da-f7321bf9b906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 70,
            "second": 111
        },
        {
            "id": "45e20260-601f-4450-8ad5-6437e86ab8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 112
        },
        {
            "id": "f223346e-aa65-462d-b95d-7001d380ad06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 113
        },
        {
            "id": "d78a5047-de72-48f8-ad2f-4db6d5a23734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 114
        },
        {
            "id": "a1713681-8997-4d93-bc46-26589d23df12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 115
        },
        {
            "id": "5c6388ff-35c6-4fce-883a-68d73c7d1b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 116
        },
        {
            "id": "b164ffdc-f2b0-497b-b1c0-f6bd86a2a6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 117
        },
        {
            "id": "604f32b6-3fbd-4ee4-9199-7e6df8e50f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 70,
            "second": 118
        },
        {
            "id": "8ded1ea4-21ef-47be-9fa6-1550d645dd39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 119
        },
        {
            "id": "997957ba-b4a7-4293-83f0-02303c8a6b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 120
        },
        {
            "id": "8c4d207f-27b6-4648-8fe9-4812284b7aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 121
        },
        {
            "id": "7a95d8e1-6905-4efe-9c38-69c251b9fb3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 122
        },
        {
            "id": "cf3c4b27-e6e2-4615-8843-74cd9a949fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 70,
            "second": 198
        },
        {
            "id": "02d17e8f-ddab-4d99-ac66-b81e0a1393c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 199
        },
        {
            "id": "cde8a94b-3538-4b35-b2d0-cfd6bcd84d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 210
        },
        {
            "id": "83b5517e-c2a3-43de-9d12-1ac7108f8f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 211
        },
        {
            "id": "89eb7239-9cb5-47fb-956c-4e09339c2894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 212
        },
        {
            "id": "f0b66acc-70c4-42da-a364-7eb401b29d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 213
        },
        {
            "id": "8371152c-3dec-4810-857c-25e5dfa5b995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 214
        },
        {
            "id": "16598855-61f6-4974-baf6-4494515c5f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 216
        },
        {
            "id": "98a79a18-35ee-4c6d-8eb6-4919f74d0e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 217
        },
        {
            "id": "a5938ab6-d7a6-448c-8b96-825b2d3dba39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 218
        },
        {
            "id": "a08e8987-4b71-419d-b210-484e67660393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 219
        },
        {
            "id": "bca5b516-048e-4bd8-8a19-7cb5df4efa1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 220
        },
        {
            "id": "d7aab378-a952-42aa-a2bc-1f7c1b13d2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 223
        },
        {
            "id": "7aae9aec-3f7c-4bd9-9318-578c0dd20851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 224
        },
        {
            "id": "d89a1bbb-44bc-4f2f-a839-cbccfd321897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 225
        },
        {
            "id": "ecb330bc-3940-4ea6-85b6-4d5cbd148017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 70,
            "second": 226
        },
        {
            "id": "14f1b72a-d719-4162-817b-eddb75a46ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 227
        },
        {
            "id": "a0b30201-9607-47cd-ad0a-8a4ccb0b2043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 228
        },
        {
            "id": "5e7b3a4b-657f-482e-b3b2-76f3ba584729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 70,
            "second": 229
        },
        {
            "id": "b1485abe-bce2-47f9-8b49-1f3f9dd0c901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 70,
            "second": 230
        },
        {
            "id": "2454752c-fd0c-458b-aa7b-22325676e041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 70,
            "second": 231
        },
        {
            "id": "76c16485-40a6-45b7-8798-aea337492cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 232
        },
        {
            "id": "3daeedf6-853f-4dd9-9e25-f203ef6fd102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 233
        },
        {
            "id": "a588742d-0f34-4862-90f5-c5d73c83f854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 234
        },
        {
            "id": "8055b9aa-6a98-461a-a94c-562d959b6ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 235
        },
        {
            "id": "8bb70fc9-18e7-4458-9735-11cfa7c600ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 240
        },
        {
            "id": "94f79920-1ed9-4ac1-82ad-cbc047ea3627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 242
        },
        {
            "id": "0af358ee-8930-4cac-aa80-e48b15de6428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 243
        },
        {
            "id": "4594ee11-aa53-4125-b739-7be7200bce43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 70,
            "second": 244
        },
        {
            "id": "4dbe375a-36df-4801-97b0-dd36547aa3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 245
        },
        {
            "id": "a0628df5-06d9-4da5-a9bb-6723a1d4c10c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 70,
            "second": 246
        },
        {
            "id": "1c2f0cfa-6163-483b-8d65-f0521f328cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 70,
            "second": 248
        },
        {
            "id": "80fe23f6-4cfb-4823-a672-7a501611a472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 250
        },
        {
            "id": "4ca1db1e-77fb-4c7e-a2cf-1979dfba8606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 251
        },
        {
            "id": "6d4e53f6-5bfe-43a4-a525-b09207b37de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 252
        },
        {
            "id": "bbe702da-6933-4409-b387-63827dc863d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 253
        },
        {
            "id": "b525cb28-c4c8-41f7-a65d-a3b781b8d6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 255
        },
        {
            "id": "9e84de6a-27ac-455e-ab98-7fc10e416e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 338
        },
        {
            "id": "155716d6-0ba5-4bf5-98aa-c81aa4534634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 70,
            "second": 339
        },
        {
            "id": "d524504d-20b0-4cc1-8aa9-459a06feab92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 71,
            "second": 66
        },
        {
            "id": "5387454d-6040-47b3-9267-d0f9c1254852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 69
        },
        {
            "id": "3ee1a8b9-befd-4187-bf40-d8f3572de40c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 70
        },
        {
            "id": "e0bf8d34-0db3-4e85-8aa5-9257a900c0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 71,
            "second": 72
        },
        {
            "id": "52c7bd63-636e-411f-8d99-a2a7c23c8c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 73
        },
        {
            "id": "b20d6fd4-20b3-4881-bafd-e4a8e41f8722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 77
        },
        {
            "id": "70130c55-ca2d-4f35-b232-7c4bae886cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 79
        },
        {
            "id": "c1b09741-00ea-49dc-8fe8-f80fdc2dd93f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 71,
            "second": 80
        },
        {
            "id": "54b26e2c-e9da-4b55-a79f-0fb718f752b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 81
        },
        {
            "id": "ac2b6480-c3b8-4a7a-b2b0-8b76e3e6a6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 83
        },
        {
            "id": "ca91e451-bb5f-4816-a149-1b7ccd86cbc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 85
        },
        {
            "id": "e5a02dc5-1808-48c8-993c-80bd1c020f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 71,
            "second": 86
        },
        {
            "id": "077e8fa3-de28-4be7-ba75-bde1bd39721c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 87
        },
        {
            "id": "47eab9f5-16d6-47d9-9e0d-287080c2f3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 71,
            "second": 88
        },
        {
            "id": "3911f97d-bc83-4cc1-bc6e-3d173c784056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 71,
            "second": 89
        },
        {
            "id": "f1fcf679-78ac-4ebd-93a0-90627267fd16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 71,
            "second": 97
        },
        {
            "id": "2b1c8b17-5d78-4915-b68e-9c07b4841007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 101
        },
        {
            "id": "87b1fbd4-a125-4dbb-993c-cb9eaa27952d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 103
        },
        {
            "id": "307d7412-a7dc-405d-9af8-adb59f314da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 105
        },
        {
            "id": "c2b5480f-7c1d-4413-b10b-f6ca9bbcbacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 71,
            "second": 106
        },
        {
            "id": "a35c12bc-4a88-4f2d-b66d-6f2ee1324261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 71,
            "second": 109
        },
        {
            "id": "ea2e8576-ba1b-4728-9154-c3be3ede59c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 110
        },
        {
            "id": "ab86fc96-2c4b-4368-9c7e-b63b3eefd358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 111
        },
        {
            "id": "7fd096df-ad5b-4943-ba32-ba45e06701a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 112
        },
        {
            "id": "fa1554af-31cc-4826-925c-5b5342849afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 114
        },
        {
            "id": "cf9780f4-1921-4976-a4ba-48239c945ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 117
        },
        {
            "id": "7beb944d-7b1b-47fd-a457-c1745619256b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 71,
            "second": 118
        },
        {
            "id": "c0bef342-3812-4e12-8700-98a4a55389ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 71,
            "second": 119
        },
        {
            "id": "ce7c4160-d458-4dc8-b6d0-0cc563c20df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 120
        },
        {
            "id": "6ea53aeb-4ee0-446d-b8f2-f18435619d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 121
        },
        {
            "id": "5ecdb1e4-6514-4a06-ad9a-8e009b1f6aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 192
        },
        {
            "id": "ca836ed1-4bbe-4f5e-aa01-21bc49fd78eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 193
        },
        {
            "id": "8399fa0b-999b-49cd-8419-9f9a06946f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 195
        },
        {
            "id": "d29ff0f4-8a9a-417c-8bfa-b8c746d4a79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 196
        },
        {
            "id": "6fec931f-3236-4878-9c44-b2728240837f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 71,
            "second": 198
        },
        {
            "id": "a66aff78-2bc4-4951-89d0-0f00ce91b280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 200
        },
        {
            "id": "f166e56e-d581-42ce-a233-bcd6ed84b8da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 201
        },
        {
            "id": "17a2d109-ac78-42e4-9558-d94ee814e7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 203
        },
        {
            "id": "07b3f04d-de12-4521-9a87-1060e8dcd426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 204
        },
        {
            "id": "f02b5d3a-023e-4147-8ab6-ba4c31899485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 208
        },
        {
            "id": "a3995594-996e-4564-b3d1-6c8ed053152f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 232
        },
        {
            "id": "d77e05a6-4c9d-4021-86b7-ebff74967b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 233
        },
        {
            "id": "da0325f0-3f5d-4dac-a9f3-7983a5f0e990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 234
        },
        {
            "id": "d563ceca-c280-4441-8717-7b26f1bdafdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 235
        },
        {
            "id": "0395ea43-75b5-4b38-be0a-bc84bca0a9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 237
        },
        {
            "id": "46728920-bb12-422f-a9da-7fe3482b361f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 71,
            "second": 241
        },
        {
            "id": "4d45db83-9200-4d5a-bd82-20cc2549987f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 242
        },
        {
            "id": "ad63e6f5-72b4-41c4-bbfc-203349aed848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 243
        },
        {
            "id": "0ea95e37-3732-4601-ac4e-651b42b9fc97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 244
        },
        {
            "id": "c11379a5-3fde-4394-9148-06399abd54a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 245
        },
        {
            "id": "57ba0b40-b2b0-4099-b821-4ad7148fbfc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 246
        },
        {
            "id": "bfdcd30a-b115-489e-bb60-348e4cd1f7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 248
        },
        {
            "id": "e28476b6-b24a-47ac-8c8d-ab98e3c11d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 250
        },
        {
            "id": "0d53338e-3b3f-4856-a66a-cf6c57f9511b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 251
        },
        {
            "id": "a7752159-c8dc-4ae4-968a-fb402e65e6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 252
        },
        {
            "id": "908aa852-3b1e-4b9b-b9b4-37de3978b80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 253
        },
        {
            "id": "795dbd59-4bfa-44b9-a32b-63791832c032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 255
        },
        {
            "id": "78f5843e-523f-471b-8644-a65d56f8b8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 339
        },
        {
            "id": "f482067a-cc6e-4ac7-85f0-0be11978529b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 65
        },
        {
            "id": "5adf7d38-8f66-470a-ac6d-184333467a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 72,
            "second": 66
        },
        {
            "id": "6579335a-0ce0-4f9d-9ef4-e3e1265f5049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 68
        },
        {
            "id": "786c9a8c-0b52-407c-9483-7f74deaa5963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 72
        },
        {
            "id": "0a66f7d5-3f00-477e-a1a6-dae4fc164123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 73
        },
        {
            "id": "046f5398-bb0c-44d5-9893-ee1532136f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 74
        },
        {
            "id": "4fe5833d-b7b5-437b-b9a9-bfc4fee286e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 75
        },
        {
            "id": "4e8933df-253e-457c-8e59-a8f2248d35c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 76
        },
        {
            "id": "d93994ab-8625-450c-876d-30bc70ef01e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 78
        },
        {
            "id": "5bb90ca7-0d57-4d8c-99a1-365a88868dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 72,
            "second": 80
        },
        {
            "id": "69c4efa0-8c83-4429-886a-5e8625346bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 82
        },
        {
            "id": "cc3803f4-3ee9-4b99-ae8c-fff4518745b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 84
        },
        {
            "id": "6affdd20-a0d9-4735-9a99-ea4783498587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 85
        },
        {
            "id": "7d49d120-0f51-4976-b010-a5eb25e9cec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 86
        },
        {
            "id": "8ea3e9ad-8ce0-4ba0-b7f6-6f19781d1608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 72,
            "second": 88
        },
        {
            "id": "5f3009c7-1b79-45b5-be7b-56b3c592b234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 72,
            "second": 89
        },
        {
            "id": "3170b448-8ed3-472c-9d9a-fda2b7a401e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 90
        },
        {
            "id": "7c3b26ea-3773-4ac0-b886-e9242d057980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 97
        },
        {
            "id": "136f9df1-0c92-4182-ac27-722544798878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 100
        },
        {
            "id": "a4e23a55-b42d-47bf-ab0b-fdc5f40b0b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 101
        },
        {
            "id": "1679560e-7cce-4794-a797-92e9237e5a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 103
        },
        {
            "id": "4888b593-7af6-46ab-864b-004e9bc388c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 104
        },
        {
            "id": "8b015171-3555-4e19-b63d-1342d1df3f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 111
        },
        {
            "id": "e23ba7c6-b5e5-4346-9b32-086d9aa2899c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 192
        },
        {
            "id": "9caf56a4-00e2-4106-a833-a8da474f1093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 193
        },
        {
            "id": "f259aa4d-dfdf-4203-9200-0e9dfddafff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 195
        },
        {
            "id": "6860a53a-386e-41db-93eb-f7ba01694231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 196
        },
        {
            "id": "6144ae43-5765-490d-a9d9-7011ae754b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 204
        },
        {
            "id": "42562e25-2eda-4f4a-95aa-6724d44d93d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 206
        },
        {
            "id": "cd7614ac-dc2c-462d-92f5-eac634ec9369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 207
        },
        {
            "id": "386ebb5e-e5c3-4645-822b-5c85543f88d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 209
        },
        {
            "id": "6f6559fe-da1a-4a9e-b818-ea7a62bfd9df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 217
        },
        {
            "id": "035ecfb8-97fb-4670-8adc-5c759a4c8baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 218
        },
        {
            "id": "7dcb2505-5768-4726-ad09-b48d66a32981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 219
        },
        {
            "id": "09c00b3b-420f-4f0b-a71e-dc5a269d3ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 220
        },
        {
            "id": "b916c39d-b3c4-4646-9bcd-ae66a9278cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 72,
            "second": 221
        },
        {
            "id": "92f65efd-a11c-4209-940d-3efbb96f7be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 224
        },
        {
            "id": "019e92fb-7732-49b7-80a7-d29264aca62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 225
        },
        {
            "id": "a4684e2f-9a66-4bcd-a51e-f9f0b2ae923b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 226
        },
        {
            "id": "c2c3e05a-3092-433f-9516-d419d1ae35d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 227
        },
        {
            "id": "bb119ed1-46d6-4a89-a7d6-81aeb80dfe61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 228
        },
        {
            "id": "0164083d-d289-47fb-8b38-33d404a868f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 229
        },
        {
            "id": "867f1e55-019f-4f7f-94b9-a2610569774e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 230
        },
        {
            "id": "5fa6153c-5e6b-4715-aecc-1a39cba45bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 231
        },
        {
            "id": "84a20d5d-2b4e-4263-9faa-5f20c83712b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 232
        },
        {
            "id": "8c016182-a96a-4ab2-b2b7-2a5a646df06d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 72,
            "second": 233
        },
        {
            "id": "3d54af61-ddbc-4e60-ad1e-5c74c11f59d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 234
        },
        {
            "id": "ae7e6faf-6a39-4b37-9e14-fb74591b408b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 235
        },
        {
            "id": "11d9c260-f7ab-4820-b34e-c1a9540f74f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 240
        },
        {
            "id": "a630e670-447f-458b-be2b-26d2288ddd86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 242
        },
        {
            "id": "c0e9c0ed-692f-4fe1-b42f-14f8aaea5ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 243
        },
        {
            "id": "72d52a23-15d8-4f23-b6c6-feb25150d394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 244
        },
        {
            "id": "1bb65b33-6cac-4c28-8ef7-e80fb986a0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 72,
            "second": 245
        },
        {
            "id": "36858d94-0807-4079-b94a-bf37ee6a18b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 246
        },
        {
            "id": "e91d1852-f87c-4804-9ac9-8849d500a29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 72,
            "second": 248
        },
        {
            "id": "301d8ad2-55e1-40cf-b740-0bd0f882e8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 72,
            "second": 339
        },
        {
            "id": "b4fc4bc5-d10a-4531-9969-31e7166d19aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 85
        },
        {
            "id": "06220d57-5d05-4987-b1cf-33b60a406127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 73,
            "second": 88
        },
        {
            "id": "ad6b3bf4-a9e2-4079-9b39-571626981570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 89
        },
        {
            "id": "27a74919-6b55-4cda-85de-d6ad2c2e8706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 97
        },
        {
            "id": "ca7befc3-8564-400f-98be-43564fd1bf2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 101
        },
        {
            "id": "4a9648dd-d4fe-4120-8489-40bcadd72332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 106
        },
        {
            "id": "e54a182f-4b5f-46ad-97fb-a8e872712805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 108
        },
        {
            "id": "c925afb3-1300-49c5-b184-c68d7af01c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 217
        },
        {
            "id": "8c16ae2a-3cd4-479d-b8e3-9737e2b76ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 218
        },
        {
            "id": "7ff1658e-af60-4603-bbaa-9e7fb17ae7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 219
        },
        {
            "id": "1bdaa37b-28d2-4d6c-a3c2-2ca6a841455f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 220
        },
        {
            "id": "d3e8734a-1017-4101-abdf-bc88527c6a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 221
        },
        {
            "id": "c41fd601-ace8-4884-a46a-ed1d6227f954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 224
        },
        {
            "id": "227a7b37-07d5-41ee-9406-cc370786ff58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 225
        },
        {
            "id": "39fcecb9-7880-4db8-a41a-d41d7548c327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 226
        },
        {
            "id": "8b1d6a0d-1bda-42ba-8d98-55ecb4a4c7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 227
        },
        {
            "id": "3b82333f-0570-4854-bea0-599b0a455a13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 228
        },
        {
            "id": "5a21e209-ce06-41da-b4bc-550ae3038e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 229
        },
        {
            "id": "cd7b2179-9fbc-4872-82d0-e0ae041bd4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 230
        },
        {
            "id": "8d542d2c-da0b-4620-9f3e-cee86a9d9da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 232
        },
        {
            "id": "1dadc04f-a94d-4fd1-a138-c43e35792f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 233
        },
        {
            "id": "7cd998c7-fef1-4b98-8bf0-c47412daf5b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 234
        },
        {
            "id": "f60cd64c-67b1-43a7-8a51-fb62e5017ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 235
        },
        {
            "id": "d7aba15e-3e46-4866-9942-a4dec83c7717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 240
        },
        {
            "id": "e7f7ba97-953f-4067-9209-da4b647bc39f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 88
        },
        {
            "id": "19d16343-b18d-42b3-a2ad-8d29f5820594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 97
        },
        {
            "id": "7023c039-b343-4e03-9d1f-d91de843c975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 75,
            "second": 49
        },
        {
            "id": "6fa9bd92-a722-4186-b41c-c9e225d38770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 50
        },
        {
            "id": "a821769c-b484-4f0b-82c7-66ce4c040127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 51
        },
        {
            "id": "0a077377-c24e-4d06-a70e-49e94f6d8bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 75,
            "second": 53
        },
        {
            "id": "7c3db7ad-d4cd-4eba-8e39-78c1aecc21ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 55
        },
        {
            "id": "795961f3-cc65-4a20-8e48-4cc22b2ea8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 56
        },
        {
            "id": "ba86ab3a-914d-4606-8bbc-c957f3230fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 65
        },
        {
            "id": "f500353e-9d4d-460a-91c1-1cc08eb66f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 66
        },
        {
            "id": "eb03455a-fc4b-4e6e-a7fe-0e0e8e3b7afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 75,
            "second": 67
        },
        {
            "id": "c76462a8-04c3-4c4d-8d60-d0b1aca4af02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 68
        },
        {
            "id": "5a5f981d-85bb-4b95-a61c-293b46e3c369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 69
        },
        {
            "id": "71914770-c97d-49e5-89c7-2bad1f0c7074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 70
        },
        {
            "id": "d9ae4203-2f98-4709-a4ee-ba6bdaecf795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 75,
            "second": 71
        },
        {
            "id": "b1945dee-54f8-483b-ad0a-a03144c5e45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 72
        },
        {
            "id": "ff776d0e-d657-47dc-95cc-51e0fd0fdc8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 73
        },
        {
            "id": "3a386afd-c8c9-4b6a-abd8-e27311dfef96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 74
        },
        {
            "id": "a2b78c23-48b6-40ad-aca1-df3be5b62382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 75
        },
        {
            "id": "fa884224-5f6f-4b3e-9922-f7c763d39e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 76
        },
        {
            "id": "7cd3591e-aa32-4918-8548-1212ca1de246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 77
        },
        {
            "id": "21f01b69-b5f9-48fc-b714-a2eb2ad907cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 79
        },
        {
            "id": "695f11cb-df87-4f67-b546-bdb3212d2afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 80
        },
        {
            "id": "c2825f1c-dafe-4b94-b6e5-0e09df89a5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 75,
            "second": 81
        },
        {
            "id": "accaf47f-2721-42d3-849a-d387667baebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 82
        },
        {
            "id": "b66e562f-aeaf-4def-ac8c-32c1f5a77f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 75,
            "second": 83
        },
        {
            "id": "c23b9922-aa0b-45a3-a8fd-7d8c28193b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 84
        },
        {
            "id": "a693a65f-d645-45d6-bdbd-b8d86ad67bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 85
        },
        {
            "id": "68a80dc0-61bc-4e95-a0ba-38b6940817ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 86
        },
        {
            "id": "3888db5c-591e-4a31-aca8-ae1427251843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 87
        },
        {
            "id": "906a48a7-c9ad-4771-9ee8-dafeebad5466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 75,
            "second": 88
        },
        {
            "id": "60d4cc72-c05d-4054-9839-f310a81b4557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 89
        },
        {
            "id": "06afd59f-4470-4405-9249-b98f7b32270a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 90
        },
        {
            "id": "a14e0ff3-782d-49c6-90de-49b364b4ca1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 97
        },
        {
            "id": "52759136-f589-42c5-9e8a-d145a24a603d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 99
        },
        {
            "id": "6484997d-da38-492a-80bb-49f9f2046914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 100
        },
        {
            "id": "8f771c7f-6d62-4080-883d-96355a663119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 101
        },
        {
            "id": "bcd693f2-68de-4a7f-88cc-73141a995044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 102
        },
        {
            "id": "1c9a95a4-2871-4db7-af97-658a4e15e797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 103
        },
        {
            "id": "95a3bd33-92a2-4b7a-bcf6-ac6916198b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 105
        },
        {
            "id": "23edbe72-3c97-41a0-8664-659e5c4b9a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 106
        },
        {
            "id": "f0a3caf4-46ce-4222-a21e-658b1b5e71db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 108
        },
        {
            "id": "55c50430-a8d5-41ed-98a1-2b11e68f709f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 109
        },
        {
            "id": "cc1081a0-3c4a-49e5-822c-e0f933ae5512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 110
        },
        {
            "id": "85315d13-93ef-4250-8fe1-6adbc219362f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 111
        },
        {
            "id": "0848a928-4bf0-48b9-9692-a70b2d79dd83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 113
        },
        {
            "id": "2d303236-786e-4354-958b-e120ba20f595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 114
        },
        {
            "id": "ce0ba3ec-ba2b-40f1-9da5-caee5d7f4343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 115
        },
        {
            "id": "5abb5b49-1fa8-424d-a170-68b104e7503e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 116
        },
        {
            "id": "f931511a-cd85-40bf-aa82-5fcb6ec8c194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 117
        },
        {
            "id": "f926afbc-4a16-46c6-8d1f-b84d2248449e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 118
        },
        {
            "id": "95fba167-5259-4fe5-a789-f6c92eee7ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 119
        },
        {
            "id": "7dc941c1-99f9-4a50-ae44-fba98fef54f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 122
        },
        {
            "id": "e711a855-9a2e-40c2-8901-ef80e3c25e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 192
        },
        {
            "id": "c3c78ba8-69c7-4ec8-8ad9-8c7fe2ac99b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 193
        },
        {
            "id": "fe1a8860-f57d-4dfb-b5fc-8e25a74675f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 194
        },
        {
            "id": "47796d82-cf8a-4bc1-abce-43e04e9d5b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 195
        },
        {
            "id": "85e2b564-47c0-460b-9cba-79035f45437e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 196
        },
        {
            "id": "c7666ac2-0401-44d0-a427-b8dd0abf209d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 197
        },
        {
            "id": "167a9035-5b6c-4f6a-a261-fe5b82bbc8f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 198
        },
        {
            "id": "48d30569-cb6f-4c5a-82b7-9753569a08ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 199
        },
        {
            "id": "80d6f039-9f14-4d10-aa03-5b0deb0b3d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 200
        },
        {
            "id": "036eb89c-8479-4dbc-80aa-f42c2d0408e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 201
        },
        {
            "id": "d062f1be-71da-4dfc-ba3b-2241da5e5ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 202
        },
        {
            "id": "dd787292-86aa-4790-bb94-7fa57d952fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 203
        },
        {
            "id": "8e0e67ed-7498-4795-8e43-615ccae82940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 204
        },
        {
            "id": "df6031f6-036b-45d8-9d6a-8add2ad13017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 205
        },
        {
            "id": "3c83b7f3-7c61-4c33-b9dd-b54be3b3727b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 206
        },
        {
            "id": "0e1cb65a-c509-4587-9db1-eb8bf6a7379e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 207
        },
        {
            "id": "2c268693-ded5-40a5-9a2a-c0ea1da4dccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 75,
            "second": 208
        },
        {
            "id": "d6375e12-0108-445d-80af-c7ae34660dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 210
        },
        {
            "id": "b61b6919-05b3-4b2b-b1f5-5be2e13f40e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 211
        },
        {
            "id": "a25723c0-b900-4f12-9a0a-fdc76375912f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 212
        },
        {
            "id": "4266179c-7396-4c58-a93e-c0c502a1b572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 213
        },
        {
            "id": "80dcf4cc-3fbb-4e1e-b761-2ab1a56368ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 214
        },
        {
            "id": "14b0f450-b2b7-4400-84ad-b9f3d7a386ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 75,
            "second": 216
        },
        {
            "id": "b381bf70-a4d5-40b8-a68e-8de6d2d10843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 217
        },
        {
            "id": "902f392c-2025-46a3-ac86-3fbb18e2b549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 218
        },
        {
            "id": "9e03091e-e690-40de-a544-b4058534b575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 219
        },
        {
            "id": "c9bea4cd-0306-4fc7-9f5f-9a5ea4641d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 220
        },
        {
            "id": "2fef09b2-5ad9-4ef5-b35d-3e02816080f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 221
        },
        {
            "id": "c079c148-4f5b-4de5-a748-24ca83adffaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 75,
            "second": 223
        },
        {
            "id": "fc31f218-a7f8-4d96-914f-d32cc0de19b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 224
        },
        {
            "id": "877bd65d-9373-4327-b7e0-98311100a728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 225
        },
        {
            "id": "0e244d51-cf0e-4526-a7f5-c8b44d523df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 226
        },
        {
            "id": "2318da62-d2d5-4a5f-8d29-640a22f8c6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 227
        },
        {
            "id": "ced8fc92-0de9-4d35-b2a4-771546c694c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 228
        },
        {
            "id": "bdad030e-6be8-40aa-8b32-ccce6ccc3877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 229
        },
        {
            "id": "9aa3151d-fced-4304-b4c7-45c659f5e515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 230
        },
        {
            "id": "dbd561b6-47d8-4d60-a5a0-a3ce6a28e38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 232
        },
        {
            "id": "b44d116e-7347-452e-acec-04bc9cc50f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 233
        },
        {
            "id": "927f5824-1f29-4365-be91-7b781e557092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 234
        },
        {
            "id": "e1ded24f-c43f-4678-b512-2abd92da67dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 235
        },
        {
            "id": "7dbd31fa-1e22-48c7-b487-a0bd11ff726d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 236
        },
        {
            "id": "0f1841df-4808-49a3-be3a-daa4f1e4701f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 237
        },
        {
            "id": "5fc86ebf-5bf5-43ae-a1f8-c1cc4e9d41c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 238
        },
        {
            "id": "4908a1af-a0c1-44c5-9882-fc0e8b7672cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 239
        },
        {
            "id": "13b09f69-cdda-4bd6-b2bb-ca7db18cdb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 240
        },
        {
            "id": "344fdc6d-5ff4-43e6-80b9-5fcb0745a1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 75,
            "second": 241
        },
        {
            "id": "27b7227d-019a-4b07-94f4-839807fc8b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 242
        },
        {
            "id": "a8ffaa93-44c2-4fdb-81ad-56fb5f973bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 243
        },
        {
            "id": "992a7eb1-86c8-43a1-a241-0c39a9114cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 244
        },
        {
            "id": "f269f738-3d91-4103-810d-65d47167632f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 245
        },
        {
            "id": "e6234e3f-3208-4709-a28b-a3622dbfe572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 246
        },
        {
            "id": "91bec8d3-558e-4594-94eb-3ac690c8d62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 248
        },
        {
            "id": "72598553-1c5b-4da0-9f33-5e81e508b7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 249
        },
        {
            "id": "2e1fa82f-a0d9-4f67-b5c2-3d7d3151966e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 250
        },
        {
            "id": "7fdb7b41-e8a6-4f5c-9443-0deb966124af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 251
        },
        {
            "id": "5011b8e3-dbda-42e1-8800-1c067e6bbcc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 252
        },
        {
            "id": "fca5e6a9-69e3-4285-826e-ffbbe28fc2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 338
        },
        {
            "id": "c60b1eb7-30b1-4068-af50-b8c8dc1f2f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 339
        },
        {
            "id": "3d511a54-fe4a-49b9-9571-ea64867b43d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 39
        },
        {
            "id": "64b1012b-9660-4f8c-ad01-feb8447e5031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 76
        },
        {
            "id": "cd864bdf-67e0-496f-b6c0-91c7af4c38c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 77
        },
        {
            "id": "1b515c85-e31d-4f6e-97e0-c8a660edb8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 79
        },
        {
            "id": "003935d7-2017-4ae8-aefe-31a05ff73b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 81
        },
        {
            "id": "e2731dc0-a38d-4675-a2dc-6573149b2c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 85
        },
        {
            "id": "287a855a-ca1a-4e53-b090-0b300b1b5e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "57ce81c4-af4e-4a71-93ca-c9e0784bbc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "74e92734-0045-44bb-b2a6-196e325634e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 76,
            "second": 88
        },
        {
            "id": "822e7b37-d08a-4e55-a021-249a353b1213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 97
        },
        {
            "id": "8a5c8e6c-79e3-4abf-b6b9-9ff6f73d3186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 100
        },
        {
            "id": "8a28b545-3d15-49ce-998a-e52ad0fe6fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 101
        },
        {
            "id": "457bcc58-b924-460c-8af4-db759bb6246f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 107
        },
        {
            "id": "aeb57f0b-c0b9-4f05-ab19-ed5f6b99910b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 111
        },
        {
            "id": "950c6746-f665-473e-abdf-53b75111903b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 117
        },
        {
            "id": "b4a455ed-1af4-44e4-9baa-b249d70435de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 118
        },
        {
            "id": "6996cb80-e0b0-4f53-8f20-c21a4b4b78ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 119
        },
        {
            "id": "e1659426-8fed-478a-b1b2-42633992bccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 210
        },
        {
            "id": "38d82163-b049-4b45-bfe4-b9812e778e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 211
        },
        {
            "id": "dea99a39-fc0a-4695-8644-45392a5183d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 212
        },
        {
            "id": "5efa6451-be39-4e03-aeaf-3141580023dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 213
        },
        {
            "id": "63474bfa-7bc7-4931-a3b6-ccc5b22add87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 214
        },
        {
            "id": "de8d0dc0-65d7-400e-9d32-c7a52064724f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 217
        },
        {
            "id": "2e6080c7-dd87-43de-8cf9-bf47163bae1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 218
        },
        {
            "id": "14f5e313-5538-4da0-9cd8-ac7b68266def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 219
        },
        {
            "id": "2cf4d9ef-0f32-4c90-a7c3-6413bae4aba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 220
        },
        {
            "id": "2eaa3ed4-9b82-4ff0-bc0f-ae40ce542c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 224
        },
        {
            "id": "f51d694b-317b-489a-a9a2-10ea21f2a651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 225
        },
        {
            "id": "a6798c0b-f31a-4883-aa93-d02ca1335108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 226
        },
        {
            "id": "b8988fa8-c56d-4827-9ccf-d0231fc051a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 227
        },
        {
            "id": "22e668cd-983c-4db6-b897-268a228c89ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 228
        },
        {
            "id": "e1d3e8e4-e895-4af6-8915-90fa97f60ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 229
        },
        {
            "id": "ff26b2cf-322e-45ce-be26-50644e007571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 230
        },
        {
            "id": "fda96bfe-af22-4337-afbc-6663fda41684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 232
        },
        {
            "id": "b97efb61-12e2-4739-82ce-34db7fc16fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 233
        },
        {
            "id": "4c314cff-249d-40c2-986f-5c901a9f3f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 234
        },
        {
            "id": "12c53af4-be73-4098-8412-c261741e03a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 235
        },
        {
            "id": "b6cace75-1f95-4be2-ad7b-6b871314e196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 240
        },
        {
            "id": "617d99b9-0be1-442c-b448-ea76a95deb0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 242
        },
        {
            "id": "16ed330d-4635-4bc2-94bd-ef6dfc827e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 243
        },
        {
            "id": "1cdaf7d8-ef2d-4a35-a12d-2b217059aec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 244
        },
        {
            "id": "36d84a8e-92eb-408d-aad0-ee0fea172de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 245
        },
        {
            "id": "e9b0f342-d4dd-4dc8-9a83-eb51714fb6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 246
        },
        {
            "id": "ee2b29f3-84f2-44bb-9453-7132f8694211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 248
        },
        {
            "id": "479d88f2-268f-44d3-9c7a-3036f21b9a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 249
        },
        {
            "id": "4c77a2eb-2c78-44d5-9c10-e19ce38bf1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 250
        },
        {
            "id": "0aefc9d4-2150-48a2-a05c-c78f9745b87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 251
        },
        {
            "id": "6051c4df-d367-4285-9977-08574aada6a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 252
        },
        {
            "id": "fd3211de-89fb-4430-a4d7-affd64be6297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 338
        },
        {
            "id": "6c0451b3-6caa-494d-92f9-4f8ace743a78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 339
        },
        {
            "id": "085b7839-bf68-47b8-9235-eb3fb6169d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 77,
            "second": 39
        },
        {
            "id": "c406910f-4c8e-4d68-8201-26782067bc08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 65
        },
        {
            "id": "3d653799-4ce8-42e8-a75a-ab6ed6910aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 77,
            "second": 66
        },
        {
            "id": "416d7c4d-4799-41cb-8854-07cbbf7db1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 68
        },
        {
            "id": "cf5a77e6-5cf4-47bd-89b2-7a4f1c91a33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 72
        },
        {
            "id": "46992ae9-09fd-4886-a3b2-d439566a99ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 73
        },
        {
            "id": "a1b922ee-fab1-4600-a841-92ba05b0e9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 77,
            "second": 74
        },
        {
            "id": "8d5b84f1-fd02-4f91-a718-d84b5cbfd168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 75
        },
        {
            "id": "18b6e576-442b-4940-b75b-1d187936da95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 76
        },
        {
            "id": "ca3d91ed-e97c-4131-8105-8b0b76559932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 77,
            "second": 78
        },
        {
            "id": "b55dd465-13c9-4f72-8d4f-010a0424a41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 80
        },
        {
            "id": "f9907a89-5ff5-499e-b218-50eb41fb9614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 77,
            "second": 82
        },
        {
            "id": "d640ed22-13ef-4440-baf9-31883a3f470e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 77,
            "second": 84
        },
        {
            "id": "1a1d5ab3-4bef-43b9-9250-a853fed9b1d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 77,
            "second": 86
        },
        {
            "id": "5ab96614-4048-4c64-83e3-3efa3f56576a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 88
        },
        {
            "id": "c1b672e4-3d47-48e8-b4d3-75e9a46a6e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 77,
            "second": 89
        },
        {
            "id": "707f1d14-e6a1-4326-849a-963f0f39c572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 192
        },
        {
            "id": "243a53fe-5750-4a16-95c8-468257dac69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 193
        },
        {
            "id": "02ca9f03-d516-42fe-bceb-9f831de8b89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 194
        },
        {
            "id": "1db21bf5-ddd6-4a48-9d0a-a970ff49ac8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 195
        },
        {
            "id": "0d52d43e-6486-4f21-8063-bdd64ce7e981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 196
        },
        {
            "id": "5c03c16f-db7a-4262-b1da-2eef8b385c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 197
        },
        {
            "id": "88be159f-64a4-498f-a0bd-405750bc8b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 77,
            "second": 204
        },
        {
            "id": "3e02d305-9642-408a-b332-1d2063b3c054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 205
        },
        {
            "id": "7ca2f90e-43a9-4d51-8d11-6147fe72cf27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 206
        },
        {
            "id": "d601d38c-9f49-4071-bf0e-9d786092b430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 207
        },
        {
            "id": "98e5b074-f8f7-4caa-b9ee-cb46b1444ba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 209
        },
        {
            "id": "d169b832-dcd9-4520-9db5-00d9b66d1381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 77,
            "second": 221
        },
        {
            "id": "793ce8cb-a930-4f45-bd7b-5dac1df9ced9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 66
        },
        {
            "id": "8e1f103e-ae55-4a03-847d-c66019e567c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 67
        },
        {
            "id": "99033abc-5676-4b87-b82f-e6814e8d3a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 68
        },
        {
            "id": "bcfff9bc-41a9-43ec-806f-6c3d7ea22781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 69
        },
        {
            "id": "8c4d70d3-b1d2-4e04-9704-db15166eeb64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 70
        },
        {
            "id": "002138b2-af61-4ad3-9a77-d223c5562253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 71
        },
        {
            "id": "fa71cd41-f893-4c78-a54c-5ec2aadb8e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 72
        },
        {
            "id": "ce6d9154-63f4-4dff-a8c4-d0d28c5ef8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 77
        },
        {
            "id": "472305d3-12c9-4371-a123-e3b2519c1c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 78
        },
        {
            "id": "19e3089d-6bb1-40e8-817f-4e50a9bd16c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 81
        },
        {
            "id": "1a8a1269-49c6-4907-a9e6-81485da88ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 85
        },
        {
            "id": "d2e5d3f7-1fbf-411b-966e-b31ccd95df08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 86
        },
        {
            "id": "d0560558-538b-4e42-8cfd-dcef7c6bbcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 88
        },
        {
            "id": "17cdf1a4-8395-4cf6-bd21-fefd6db8c617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 89
        },
        {
            "id": "1c510c16-c202-4a3a-895c-fffdda61668b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 90
        },
        {
            "id": "541c0dfd-c037-4695-8326-95896e2ffb82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 97
        },
        {
            "id": "42b2b83d-8980-4e88-ac62-c986ebe4c1bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 98
        },
        {
            "id": "d1ffb866-2f5a-4e15-ab78-a1322ac44d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 99
        },
        {
            "id": "ce806ff8-2052-4b82-96cc-47b217d59bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 100
        },
        {
            "id": "5b1e9c44-9666-4f56-8466-35c67d3512a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 101
        },
        {
            "id": "9121beae-f940-4c6c-b5d9-9ddcdd6129e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 103
        },
        {
            "id": "a417a02b-c8ac-4640-985f-7ae60f589afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 109
        },
        {
            "id": "3cf7d43e-ca7e-43b4-a1d8-d8796e6032bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 110
        },
        {
            "id": "5f9a89ef-b880-45e5-9301-f060bbae7499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 111
        },
        {
            "id": "54473d2f-942d-47fd-9726-4b4737b90eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 112
        },
        {
            "id": "16e4ee46-a403-4cb3-8875-9de4ecf9e7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 113
        },
        {
            "id": "cfc5ec7e-e118-4330-a9a6-581a7cec9772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 115
        },
        {
            "id": "13816d46-9499-4920-ac60-27289a7a9a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 117
        },
        {
            "id": "25627c1c-11ed-498f-a94d-220e900ec07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 118
        },
        {
            "id": "c313b6a1-7a86-4848-b135-ab776dbc1042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 119
        },
        {
            "id": "5d125e73-aee3-4047-be9b-1cfa201d5e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 120
        },
        {
            "id": "63dcda26-dd28-4f55-aa1c-d0d20d495f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 121
        },
        {
            "id": "bb79149f-8279-4981-8d29-b4e8566f59d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 122
        },
        {
            "id": "fbd0cc8b-d9d8-484d-975d-1b2058bbe607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 192
        },
        {
            "id": "31de611a-6f3b-4039-95a2-6a91d1e71819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 193
        },
        {
            "id": "577a07e0-8ba3-4dc8-a607-4d7f65260f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 195
        },
        {
            "id": "27d45d68-9694-45a0-b705-67cbdf7f8aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 196
        },
        {
            "id": "a3e35ad2-7227-4914-8298-6b6da1148e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 198
        },
        {
            "id": "102a042f-49a2-4255-ba30-6d5911ea7e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 200
        },
        {
            "id": "a34cd4c3-eea1-4b82-b3f8-42f0e78443a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 201
        },
        {
            "id": "2171500e-6228-443b-87df-cfe8999155f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 202
        },
        {
            "id": "9f7dc8a7-bec7-4280-93b7-2dd0a64c0fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 203
        },
        {
            "id": "36c97a21-c62e-4593-be18-05fd2d94f209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 204
        },
        {
            "id": "414e9f74-f7db-4c86-92f7-29bb384f1dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 207
        },
        {
            "id": "0a96903e-b1ad-400c-a882-934aa24b83a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 208
        },
        {
            "id": "ff7bf3ed-42a2-4aa6-81bd-d3d23bf2a851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 217
        },
        {
            "id": "9b7f3229-38eb-4eca-8e76-e0d9a768810c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 218
        },
        {
            "id": "1b72be26-56f6-425c-a305-71c3c59d49b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 219
        },
        {
            "id": "0bf5bb13-092e-47ba-95cf-a20c24777978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 220
        },
        {
            "id": "61ab6f24-7729-467c-be50-b50882e9a10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 221
        },
        {
            "id": "aafc18c0-d897-4b88-9b7b-ef510689d97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 224
        },
        {
            "id": "5163a37d-a6f2-4b90-875c-605569b58ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 225
        },
        {
            "id": "562f31d8-cc50-483f-a65c-2e5bdea5c93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 226
        },
        {
            "id": "089b7e7c-95a0-4da5-8dab-16d392d83d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 227
        },
        {
            "id": "eae37ebb-3b90-41ae-b04a-82c5fa3c646e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 228
        },
        {
            "id": "9dc57a52-b624-4924-aa91-5817ed47f503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 229
        },
        {
            "id": "57ca6dab-e301-4450-989c-2f996e179760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 230
        },
        {
            "id": "9055d9d6-1354-41a4-ae55-b8480fbd5361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 232
        },
        {
            "id": "d26035bb-4125-40e8-ac6b-73ed7bfb28ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 233
        },
        {
            "id": "5197d559-0345-4dde-a672-321b7436a4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 234
        },
        {
            "id": "55ce57dd-2389-476e-8956-318d768a5041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 235
        },
        {
            "id": "3fec9ab0-68b5-4149-a3b4-4963772aa844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 242
        },
        {
            "id": "56032003-b884-4e0f-af49-0edb93b6051f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 243
        },
        {
            "id": "16ca3f30-7a34-42d3-afc4-93dcc435a59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 244
        },
        {
            "id": "c1b83f82-be9c-4565-95bb-0e762225a9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 245
        },
        {
            "id": "8bd76503-7d37-49c4-9bd8-4b90ba9f528a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 246
        },
        {
            "id": "911a6a9e-74a1-4b93-950e-84157bca25b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 248
        },
        {
            "id": "904b2f32-9a7e-4274-85a6-5f4cac5a4648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 249
        },
        {
            "id": "370f906f-2e6d-470a-b2b0-441779bcb5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 250
        },
        {
            "id": "8f330a2c-dce7-4157-a602-d599f0a74f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 251
        },
        {
            "id": "3adedc7d-c308-4ac9-a7e8-98da4fa40092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 252
        },
        {
            "id": "5da48e52-3259-4b36-8ef5-8361f647ba0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 253
        },
        {
            "id": "52120c8f-a502-497c-b3de-3f8aa28fea66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 255
        },
        {
            "id": "cb0e26ac-8c32-4548-aa74-0b6535f63eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 339
        },
        {
            "id": "b12e18af-b44e-40c5-988e-bf2957556367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 79,
            "second": 39
        },
        {
            "id": "9d97fc71-fffa-40b4-91c7-532076edbbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 66
        },
        {
            "id": "681899b5-d9f4-41c8-a64e-01bb976e9177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 68
        },
        {
            "id": "75f77644-87e6-4958-9675-97acc1d53ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 72
        },
        {
            "id": "0194e4ab-fb1c-4a84-beba-a089941403a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 73
        },
        {
            "id": "86487bb7-5f07-4fe5-8b05-12f2633343ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 74
        },
        {
            "id": "4e5c62a8-6b6a-4ab8-b971-abee0498715c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 76
        },
        {
            "id": "1e1f0c88-a4b1-4bc5-9425-8bfc198dd910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 78
        },
        {
            "id": "c4a6d166-038f-4921-8254-56154e7fb4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 80
        },
        {
            "id": "7ed60b69-6aec-4c63-b6da-d6c73d1cdd70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 79,
            "second": 82
        },
        {
            "id": "70107704-e05b-486d-aa3c-643c1bc15be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 84
        },
        {
            "id": "ab9419fd-bbd4-41f4-a110-beaf0dad1c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 79,
            "second": 86
        },
        {
            "id": "4cc01b2f-7123-4cf3-86f3-17d9ba976204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 79,
            "second": 88
        },
        {
            "id": "b69c1bfd-d185-45e6-a98c-14bbd928da2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 79,
            "second": 89
        },
        {
            "id": "b4403d0f-099d-4de3-9baf-f5fb5165ce21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 192
        },
        {
            "id": "58f76bfe-8023-493d-b163-db1ad3d78ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 193
        },
        {
            "id": "1c65f190-3fb6-4748-aacc-a8c41c3e9bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 195
        },
        {
            "id": "44f0d315-b943-4cac-a73c-13b39416abe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 196
        },
        {
            "id": "c1c7e573-b81f-4acc-bee5-8dc3ad5d4cbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 79,
            "second": 204
        },
        {
            "id": "c325c00d-c0e5-467f-84ad-4f60bae86e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 205
        },
        {
            "id": "6fc57add-aad5-4357-b53c-74003dc4b3bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 207
        },
        {
            "id": "9e185276-7b0d-4edf-834b-8d8bc72ac3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 209
        },
        {
            "id": "f7625bb4-bdd8-4bbf-a276-6f9c755566f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 79,
            "second": 221
        },
        {
            "id": "a81586e1-5830-4fc0-9337-568220f128cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 39
        },
        {
            "id": "2a6037ff-da17-47f2-9110-5aa8f2df697a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "c4c5afe8-90b5-44d4-981e-ae5f4c27109f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 66
        },
        {
            "id": "a2e74886-38ad-49c2-9db8-e4fcd0a5fac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 68
        },
        {
            "id": "ce38560c-18eb-43d9-9dd2-742c6ae21d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 72
        },
        {
            "id": "64b9ab34-8274-4b0e-89c0-277c2e56a75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 73
        },
        {
            "id": "a536df71-5780-470f-9166-caa22356f07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "1489ee8b-52ad-49a1-b94b-cd0320bfe099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 75
        },
        {
            "id": "ddc1316c-4399-4d0c-93b6-0c445507bf71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 78
        },
        {
            "id": "0d338a44-1496-452b-a057-e362ba2f093b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 80
        },
        {
            "id": "fa50e6df-f502-43b9-bdf1-660ba103750f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 82
        },
        {
            "id": "cea2629f-dcfc-409d-a93f-a6a9644d4a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 84
        },
        {
            "id": "bca2a067-84b4-4dfc-8dba-9357d769ad1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 85
        },
        {
            "id": "6cf72dd7-6ba1-4fb9-b7e9-11c9bbfb539a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 86
        },
        {
            "id": "ad37d8cb-d38b-4882-a7b5-346f75dc24f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 88
        },
        {
            "id": "f1d07dfa-c499-4987-936a-8dfa42ddf107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 89
        },
        {
            "id": "3f2d6529-a869-41e1-8492-eb29ab98ecb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 90
        },
        {
            "id": "d01cf159-f69c-44a2-8e1f-31e4bd75ced9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 97
        },
        {
            "id": "bed95917-1baf-4561-8f66-95f34e8ab22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 98
        },
        {
            "id": "af75097a-0497-4c5b-9d58-2892015893e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 99
        },
        {
            "id": "b377a287-1826-45a9-b7ce-a69dcd36cf37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 100
        },
        {
            "id": "5aacbd05-a10b-47cd-822e-0cb4bac0d381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 101
        },
        {
            "id": "f3eb8da5-fbd9-43ff-b32e-cbc8bf43aea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 103
        },
        {
            "id": "9b4d0023-f4d9-48d6-a512-79caae81f51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 104
        },
        {
            "id": "e91a3253-7ac1-43b4-94b8-2a6258d84fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 111
        },
        {
            "id": "dd7eabfc-c7d2-4922-b99a-56b5f9984825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 113
        },
        {
            "id": "6147ac4f-4582-4b68-b4d4-a1bd7ae980b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 192
        },
        {
            "id": "cb79eaaf-8fae-4d32-8255-814b0764f20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 193
        },
        {
            "id": "74136c05-64a8-4877-b08b-effd17b80569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "70388dd7-e50a-4770-ab00-745d16d8c09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 195
        },
        {
            "id": "a277cfc6-cfe9-4598-8597-18317d623d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 196
        },
        {
            "id": "66810392-265b-4348-96bb-3337863ccd70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 197
        },
        {
            "id": "aa71edce-9880-4149-91f2-80b9403e21f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 198
        },
        {
            "id": "eb8d1f41-5793-4092-861b-0a5d3cee0ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 204
        },
        {
            "id": "f76b6f9f-d65f-40e6-bb25-045d3081a3ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 205
        },
        {
            "id": "433e1822-6557-434e-8f1f-79253da9c240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 206
        },
        {
            "id": "36c50c18-b55b-4a31-abc3-5349e6c09a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 207
        },
        {
            "id": "c5796d05-cdd6-40dd-a8da-02491c0af7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 209
        },
        {
            "id": "a4f2cebd-898f-4a53-8954-c8c5e6435f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 217
        },
        {
            "id": "f0cf2a87-9d31-40e0-aa3b-1cd4f4d17fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 218
        },
        {
            "id": "ef8c9a9b-f566-4e58-89cb-138432b0a303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 219
        },
        {
            "id": "a5e69c18-5012-43ac-9ba8-e848d7db0dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 220
        },
        {
            "id": "e471f6fa-2583-43ae-b038-83d708981f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 221
        },
        {
            "id": "6f39b9a9-8cd4-4eda-a363-dc277036f369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 224
        },
        {
            "id": "1d12d60b-73e2-442f-8340-ceb1e329ba9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 225
        },
        {
            "id": "98f3cf19-bbfe-4f05-b981-47bf333e8c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 226
        },
        {
            "id": "cc507a56-27cc-4d25-a4fc-e61fc08652b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 227
        },
        {
            "id": "1d66f8ba-d4b0-48f5-a80d-85ba57f7311b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 228
        },
        {
            "id": "ee62f5ad-56ea-480c-892a-c5abf601c5ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 229
        },
        {
            "id": "01a5627c-36da-4377-9810-258e37392ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 230
        },
        {
            "id": "2dc3da43-f341-44a2-973a-d73362662dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 231
        },
        {
            "id": "a3f5a328-2745-4400-b0f4-07b4f8d6e374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 232
        },
        {
            "id": "d73d5d62-4683-4a4e-9815-26e5901fd7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 233
        },
        {
            "id": "a9dfd72c-a3d4-441e-a4ab-c2d514980de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 234
        },
        {
            "id": "26ab3b01-e21e-4687-81c2-507a003bb302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 235
        },
        {
            "id": "bd129c00-f7d7-41cb-b2f7-b9ef3f24ab3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 240
        },
        {
            "id": "a048be9d-74f6-4c3a-8cf6-cea8e9c9e416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 242
        },
        {
            "id": "38c2303a-32b1-4056-a746-32bec2c90795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 243
        },
        {
            "id": "0d6e9f69-f64b-4fd3-bf96-f0f73ea91560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 244
        },
        {
            "id": "ac2e08a7-aa29-4ecc-aa1e-0900f4e290c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 245
        },
        {
            "id": "9ab53940-8c4a-4990-a099-dd7a4840ef4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 246
        },
        {
            "id": "b7efc7a3-6152-43ee-adfd-4602d3597b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 248
        },
        {
            "id": "21cfdaf9-2321-407d-baff-c2d99c3805bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 339
        },
        {
            "id": "408907bc-db8d-4db5-995a-96c1c98907c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 66
        },
        {
            "id": "30fa34cc-b24a-490e-acb0-a90db5fad19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 68
        },
        {
            "id": "f4cef999-1f92-49f6-baed-6cb12ea8a599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 72
        },
        {
            "id": "57e21aa5-5045-4902-aec9-b11e6764bb1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 73
        },
        {
            "id": "2f1c01d0-2d2a-4807-87e0-3ba15e0ef1f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 74
        },
        {
            "id": "a436d83e-b097-4625-a612-6117ca1c070d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 76
        },
        {
            "id": "634ef1fa-e25a-47f4-8fe8-8804c5c8a3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 78
        },
        {
            "id": "9f1a00d3-5a9e-409f-8adb-3cf6acdedf63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 80
        },
        {
            "id": "77faaa32-9ba8-4d8d-a9ca-2dd0d14b72ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 84
        },
        {
            "id": "8221a5c9-a8e0-4f5d-ba3f-8c34e35f8660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 85
        },
        {
            "id": "bd333fba-694b-4517-80e4-da2b8de4125c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 81,
            "second": 86
        },
        {
            "id": "ce7e88f3-d5cc-4eb8-987e-be5ee4387957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 81,
            "second": 88
        },
        {
            "id": "3ebcca13-b12f-4a8a-a408-1767c8b85cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 81,
            "second": 89
        },
        {
            "id": "da5076c6-2afa-4295-9303-12288e19a5c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 98
        },
        {
            "id": "11235af4-c6ac-4e4c-b6a1-b2055112c4c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 192
        },
        {
            "id": "7e9cbe20-d5b8-4b30-aeb8-8df3ad7d52b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 193
        },
        {
            "id": "15a581a1-c4e4-42a3-998e-4d747439d8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 195
        },
        {
            "id": "c85de1be-07f5-4752-9d74-b918f30fcc9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 196
        },
        {
            "id": "82f7ce87-3540-4a72-9c64-844a8c5fe88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 204
        },
        {
            "id": "94eb02b1-66e6-4104-81a8-76bc20f50de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 205
        },
        {
            "id": "3c767e7b-34a7-4f99-b0ab-8cb863f44e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 206
        },
        {
            "id": "d9613111-048f-402b-a10b-7a13b3b916d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 81,
            "second": 207
        },
        {
            "id": "a95fb625-d5de-48e4-9cd6-20aaa9df6d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 209
        },
        {
            "id": "7baec5d0-d821-42a1-b0fb-db740a782cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 217
        },
        {
            "id": "40c523f4-6ec3-40d2-bd86-9be46dba35ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 218
        },
        {
            "id": "ae7f3646-2aff-4830-8a86-b2ff520ec28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 219
        },
        {
            "id": "96dff661-18cf-4f55-96f5-ecf0c922b0b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 220
        },
        {
            "id": "5ad97503-b535-4a30-a3d9-2657efd9a163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 81,
            "second": 221
        },
        {
            "id": "a455869b-6477-4f0d-b147-0e90b5bd1da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 39
        },
        {
            "id": "9aad2cd0-d071-4390-b2fe-37e1e5945a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 66
        },
        {
            "id": "df7d5620-0cad-4014-b37d-434e7000e918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 68
        },
        {
            "id": "b86939db-13ea-4d1e-988a-1c159b0febdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 72
        },
        {
            "id": "668c1e27-26d4-4969-93e2-5a5734533ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 73
        },
        {
            "id": "1dd1ba35-8972-467f-a64f-77bd0310f118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 74
        },
        {
            "id": "11943b2c-d01b-4dcb-9247-c72a69b29ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 80
        },
        {
            "id": "cdbcc908-8325-4359-b374-6adcc4220390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 82
        },
        {
            "id": "968a0903-0acc-40b1-a2df-aad3f061a11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 84
        },
        {
            "id": "1bd18aff-e6e7-476e-8057-f34c11b2caa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 85
        },
        {
            "id": "c55e3116-fc8d-46c0-a315-24ccb90de989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 86
        },
        {
            "id": "7f0caf85-0d49-40af-a857-88424b554929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 82,
            "second": 88
        },
        {
            "id": "b4fd1732-704b-4791-b99e-b0990d32bfea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 82,
            "second": 89
        },
        {
            "id": "9ed1fcf5-8630-46db-9856-c82d07b6ed01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 97
        },
        {
            "id": "165ae73b-bf7f-4b6b-acca-a14265827b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 100
        },
        {
            "id": "b60e3a05-58d8-473e-a3e7-fa9989aa1760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 101
        },
        {
            "id": "a13ed74a-d919-4f2e-836b-2365e1e1fc15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 103
        },
        {
            "id": "b7b1cfc4-a399-4d5a-aa3c-cffcf80b46c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 217
        },
        {
            "id": "65d23a2c-a2af-4979-8d9d-4c00b0d28ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 218
        },
        {
            "id": "c678e397-3c87-4afb-8315-c483846b6e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 219
        },
        {
            "id": "e4addd4c-a63d-4b63-be1b-820073645c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 220
        },
        {
            "id": "34df227b-ffe9-4235-b322-87e24b77143a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 224
        },
        {
            "id": "5b094ca6-eefe-4f96-a61b-a8f990ce9ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 225
        },
        {
            "id": "5251286c-4caf-4ee0-a09b-c39816531a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 226
        },
        {
            "id": "b55d1f3d-b29b-46ab-8af3-2d7a3203e9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 228
        },
        {
            "id": "8c066c24-2ff5-4f4c-b413-72536c7d1ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 229
        },
        {
            "id": "65b366af-0a7d-4479-9f6f-a68f7c84e381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 230
        },
        {
            "id": "8a11270e-7fc1-4cd4-abcf-15c780f4d479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 232
        },
        {
            "id": "d8103b09-287b-4370-bd6e-d3fcc943d669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 233
        },
        {
            "id": "79865ca6-ede1-4f04-88f3-4533d6b482be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 234
        },
        {
            "id": "a469a7b3-0312-476f-a44d-a8364c2bec64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 235
        },
        {
            "id": "a36a851d-3e9b-40b2-b735-49268721f823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 67
        },
        {
            "id": "866f03bc-0ea8-4e79-8af0-c6c95230d792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 71
        },
        {
            "id": "9c33426a-1318-48b4-8317-83a8d3ea1b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 79
        },
        {
            "id": "6a7253e2-2aad-4cea-8edc-8b06acf6d054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 81
        },
        {
            "id": "1b0f6130-96a6-46af-8545-ba187f39f81a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 83
        },
        {
            "id": "a8dfc4d5-f3cf-4dad-bf2b-884c1e09b9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 85
        },
        {
            "id": "6eda59dd-723e-404f-8667-1a2b367f272c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 88
        },
        {
            "id": "5db50157-cf2a-455b-9094-c5274cb5733e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 89
        },
        {
            "id": "229ec362-8d55-4f86-b2fe-b22fb40ce7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 97
        },
        {
            "id": "c094fc6a-dacc-495f-ba96-f59da1ebc1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 101
        },
        {
            "id": "a5ccdd34-20c7-43e5-9030-5b0c03e04dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 102
        },
        {
            "id": "0c9ed0bf-39de-4f17-9163-29759890033e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 103
        },
        {
            "id": "6e7c9391-7f36-4a1f-bd1f-bbb4cc898fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 105
        },
        {
            "id": "d15cbc68-af46-4bb3-87d9-a58be66ba3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 106
        },
        {
            "id": "4a9c9163-08db-4549-8cc4-0d8e824dc213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 109
        },
        {
            "id": "3213e278-d3f4-4c6c-8a8e-965ad6f6cbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 110
        },
        {
            "id": "08ffeeaf-c6d7-4f5e-9c54-fac0e4ce0207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 111
        },
        {
            "id": "e87bfb4a-6799-44eb-9aa7-9e88f65a9df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 112
        },
        {
            "id": "30dab56e-6cf4-4bf6-b103-5f0de43180d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 114
        },
        {
            "id": "6e552be6-fe81-45e6-a376-2ec46ee531af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 115
        },
        {
            "id": "858a13d5-70a2-4d1b-a589-0b2d9fa602ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 116
        },
        {
            "id": "f39ab5f6-7961-4144-90d6-e71f58660062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 117
        },
        {
            "id": "94064fd3-236b-4c2c-bb7e-ea63de69067c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 118
        },
        {
            "id": "293eb12b-de91-4a51-bf1f-0ad92c84db98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 119
        },
        {
            "id": "5dae2c5c-c69f-40c6-be07-5ef542acf58f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 120
        },
        {
            "id": "c721ae61-9a6c-453b-a0fb-a4965c7f16c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 121
        },
        {
            "id": "c012fca4-98ff-4b80-9791-58c9472c592b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 122
        },
        {
            "id": "757d1d96-49c7-4248-9c97-c5198ec75e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 198
        },
        {
            "id": "3f5c5b39-954b-41c6-9989-1b8c1b3e2341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 199
        },
        {
            "id": "4c966bbf-25a9-4df9-9a61-ecfdf19eee20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 210
        },
        {
            "id": "3118228e-ee70-4df6-94f6-54c1a4a8cfaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 211
        },
        {
            "id": "73f2898e-e02d-4870-a47d-0e30ac95528a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 212
        },
        {
            "id": "7fc955c7-f213-4787-8006-91cb4249fc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 213
        },
        {
            "id": "fc216fb2-f9dd-4614-a380-5363ae00df40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 214
        },
        {
            "id": "36557b72-73c7-48cc-bda5-6512bc377da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 216
        },
        {
            "id": "5cc469b7-f909-49c8-a04e-75885d2ecfc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 217
        },
        {
            "id": "d4f3b469-61b5-4e6d-bae5-44e3a620b2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 218
        },
        {
            "id": "059602db-0942-4a87-9be5-44ea9da29709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 219
        },
        {
            "id": "77a3ca81-cbcc-4efa-815f-12f2ef59c209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 220
        },
        {
            "id": "5c9aaade-92fd-4b17-9956-5b23a7e28231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 221
        },
        {
            "id": "0932b953-f987-44c6-9637-a42db3d97e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 223
        },
        {
            "id": "85ba3b79-9b73-42f3-a2b1-5d173ece9028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 224
        },
        {
            "id": "19023be7-236f-4272-84c4-5159dcbbbb32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 225
        },
        {
            "id": "13d86577-2641-485c-bde7-1142a5b737c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 226
        },
        {
            "id": "5a2f95ea-679b-4792-a9cc-3334062b1ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 227
        },
        {
            "id": "1fe305ed-8cc4-4d03-aaef-9652b9634213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 228
        },
        {
            "id": "13bc6566-6656-486f-a5a7-322525d0999e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 229
        },
        {
            "id": "bd76f1f3-2fa9-4079-ae18-550a4a5eee4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 230
        },
        {
            "id": "34445c3b-7e0b-4448-937c-37323ac14bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 231
        },
        {
            "id": "68542bb3-14ad-473d-9de3-9578c55bc588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 232
        },
        {
            "id": "3cd58922-1e98-4105-bd80-7bba84104a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 233
        },
        {
            "id": "a83ee430-ab4c-4f5f-813f-32f9c0e125b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 234
        },
        {
            "id": "57d360c8-ccaa-482f-9a34-7ec4c4d9151b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 235
        },
        {
            "id": "a3786286-f01f-46f4-9234-4a930b7b6497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 83,
            "second": 236
        },
        {
            "id": "f6cec7c2-62cf-4240-b1cb-d440ad045a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 237
        },
        {
            "id": "c3b0c549-06d0-47ce-86f4-dfb5f5c9afad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 238
        },
        {
            "id": "61753131-0bcd-4096-ab6f-49e83b98890e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 241
        },
        {
            "id": "65c128b3-4ae1-4848-b598-9be32122786a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 242
        },
        {
            "id": "58864191-0435-42eb-a864-0b048a88a50c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 243
        },
        {
            "id": "57023889-aa66-4b14-aafd-1a489650049d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 244
        },
        {
            "id": "47eb79b8-ff91-4e7c-b600-5d032f00dd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 245
        },
        {
            "id": "dde9aa9b-6747-4f3b-90ed-25fd364789a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 246
        },
        {
            "id": "e93ebf51-6949-4052-abd3-ec0631d019a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 248
        },
        {
            "id": "8fa04369-2d18-4869-88e3-23e0d8475baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 249
        },
        {
            "id": "488584a6-fcf9-44b7-aa09-b2c37b576599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 250
        },
        {
            "id": "09ac6bb0-b03a-4013-8f5e-b41984a9bb6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 251
        },
        {
            "id": "93146c04-23fe-460f-a33a-320bbfa0ab08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 252
        },
        {
            "id": "95b71652-c392-48cb-919e-f2c22aa82989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 253
        },
        {
            "id": "9fedb774-e095-41b7-824d-23a80120874e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 255
        },
        {
            "id": "9794a68f-b20f-4975-aea7-ec4ece71bfe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 338
        },
        {
            "id": "908fc1b6-b919-4b55-83b1-edec8be2c6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 339
        },
        {
            "id": "db80b92c-0dca-48af-a8b6-605b83cbd531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 48
        },
        {
            "id": "54b15f80-381c-4d78-96f3-31e9ee82e341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 49
        },
        {
            "id": "3514b280-0b20-438c-8d55-bae94a27cb6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 52
        },
        {
            "id": "57fb8698-3225-488b-b6d7-64c5ba29e44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 53
        },
        {
            "id": "2d0345af-2de1-44bf-8ace-f1336461641d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 84,
            "second": 54
        },
        {
            "id": "9c6ee2c9-2d0b-443e-8918-c683d18416e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 56
        },
        {
            "id": "c8253342-4f6f-4138-bd12-b91c2735b967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 67
        },
        {
            "id": "c04ca9fd-8ee7-4cb9-ba4d-17692d4ce601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 71
        },
        {
            "id": "8c407579-fc6c-4b68-a650-57d3a563789a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 77
        },
        {
            "id": "9dc99cc8-a6f3-479e-9068-fbb9e325c44c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 79
        },
        {
            "id": "483c3117-3241-467d-888f-28a5090fcfed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 81
        },
        {
            "id": "bd41165a-32ac-48d6-be94-16ae6c5e63db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 85
        },
        {
            "id": "52ed2f77-b987-4e54-b666-2fa645eb7c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 88
        },
        {
            "id": "e91bf83a-ff74-4a15-bb75-3c0c76b8c81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 89
        },
        {
            "id": "657071eb-01d9-4638-b742-6bbd4e17183f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 84,
            "second": 97
        },
        {
            "id": "61c33bcf-9688-4a86-8a4a-e003302a560e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 99
        },
        {
            "id": "fd889f08-8542-491d-a449-bbe9a07fa2e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 100
        },
        {
            "id": "8c94c7f9-fcea-4156-9a18-0ebdcf9c66e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 84,
            "second": 101
        },
        {
            "id": "9b367d36-ee81-4792-a705-9f257a644fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 102
        },
        {
            "id": "ec58a7f8-dee2-4787-87ae-e667a93b0e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 84,
            "second": 103
        },
        {
            "id": "f31ce073-aefb-4918-bdd4-a9bfa66c5448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 105
        },
        {
            "id": "02287fdf-b58a-48ff-8e9a-75d820e53e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 106
        },
        {
            "id": "90d2ed5e-c2dc-4f95-aa3a-ee845aee957d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 109
        },
        {
            "id": "ae4b9501-81b9-4a1e-acea-0a3bfcf49ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 110
        },
        {
            "id": "1f01a9da-7b1e-4de5-9f57-e86fbf666fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 111
        },
        {
            "id": "35dd5ec3-a65d-4340-ad78-ffb1e83ed7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 112
        },
        {
            "id": "40c955c6-0586-4f87-812c-a5bfe4f06620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 113
        },
        {
            "id": "7b3eb8d7-9ca7-4ace-8531-40f027fff85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 114
        },
        {
            "id": "2be45779-594e-4188-88ba-6aa990fa09d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 115
        },
        {
            "id": "0442b913-0eef-49cc-ac8e-c3350dc5fe27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 84,
            "second": 116
        },
        {
            "id": "eb218003-dd10-466a-bc91-29e1aad8658d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 117
        },
        {
            "id": "d13de7bf-4415-4ac5-a82a-46ce35620fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 84,
            "second": 118
        },
        {
            "id": "6dbbd018-4a35-42f5-a78d-dae79f0cd352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 84,
            "second": 119
        },
        {
            "id": "6b490dea-d865-4184-a31e-56b22be4f675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 120
        },
        {
            "id": "d5ecaf5c-0409-41ca-8987-8c954cd9d940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 84,
            "second": 121
        },
        {
            "id": "38320455-7e38-4770-b5c5-ef69dff4108e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 122
        },
        {
            "id": "b647fe32-0ff3-478c-ab86-16056f72683b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 198
        },
        {
            "id": "787b222d-444f-418e-87bb-c5b3303459d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 199
        },
        {
            "id": "9e06bd3c-b896-4403-aac9-95cfac6a5c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 210
        },
        {
            "id": "6abc704d-e7d0-4a4e-8c20-bff7d2030ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 211
        },
        {
            "id": "b95bcac0-0e10-42be-9682-ed42dd525ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 212
        },
        {
            "id": "98e54be0-9249-4763-aebe-5c0e293f3c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 213
        },
        {
            "id": "1fff9a47-d159-4faf-8edd-a9bd44f7a788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 214
        },
        {
            "id": "d15db8e8-ad8e-4974-8eba-9e39bcf5a49d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 216
        },
        {
            "id": "764bd098-2252-4c43-bdd2-6acfbb2f074d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 217
        },
        {
            "id": "ebc40436-2376-4453-9823-da97bb2deebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 218
        },
        {
            "id": "99610ea0-d2ce-48c9-aaed-6dac25023a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 219
        },
        {
            "id": "aff0a47e-b35e-43fb-91ec-079bef409df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 220
        },
        {
            "id": "9a1029c8-f96f-446d-a48a-1c591ef2013b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 221
        },
        {
            "id": "bd631c71-6a09-4d01-80be-460ad285b1bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 223
        },
        {
            "id": "c0e6b652-cf63-4656-ae1b-e6b5913bc8b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 224
        },
        {
            "id": "a87bb0c8-e98f-4fb9-987d-0b99ec2330a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 225
        },
        {
            "id": "ef57a462-111c-4ade-9f5d-2bb827df166f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 84,
            "second": 226
        },
        {
            "id": "e954fd76-ef56-428c-a7ee-38db7ad7b439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 227
        },
        {
            "id": "ff20b79e-10a2-421b-aa59-300734dfff05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 228
        },
        {
            "id": "af3e7fa2-a265-465e-9fe3-d8b2c75e6bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 84,
            "second": 229
        },
        {
            "id": "712e490a-b53e-4e49-801f-9d38f8f09ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 84,
            "second": 230
        },
        {
            "id": "e434476c-7c44-4405-8e73-2409bc5346e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 231
        },
        {
            "id": "c7b79ed6-efb3-4eef-ac27-4e8e6245d910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 232
        },
        {
            "id": "f03292f9-5cca-458a-aacd-7d838a637135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 84,
            "second": 233
        },
        {
            "id": "ca0b1a45-fd93-4f62-823f-a45eefe47272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 84,
            "second": 234
        },
        {
            "id": "53966bad-8b1a-42ec-8331-da4d134d04f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 235
        },
        {
            "id": "98e1aa94-b59a-42a8-844d-e2e64fdbaf2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 84,
            "second": 236
        },
        {
            "id": "4d10ab4b-3e41-40f2-b494-dc0ca3f98c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 237
        },
        {
            "id": "fd6fbf79-57b4-412f-8c89-2de674b75e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 240
        },
        {
            "id": "554e1e9c-e751-46b9-bd89-a744449d9ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 241
        },
        {
            "id": "fb68f171-d357-4f00-9a22-15b2975f3f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 242
        },
        {
            "id": "07d0b2a5-a061-4e52-ba2c-09e8a44cf25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 84,
            "second": 243
        },
        {
            "id": "73a2e89a-b8f6-4573-afac-2a00fbc9a1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 84,
            "second": 244
        },
        {
            "id": "1364d513-92a6-4829-ad8c-a2c0fec66572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 245
        },
        {
            "id": "af2278b0-2bae-4d59-ad26-bd04f7dcc345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 246
        },
        {
            "id": "31acbe8c-2bb9-45db-a3fe-198a87f748dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 84,
            "second": 248
        },
        {
            "id": "5146132b-7476-4642-a131-2bba2633e51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 249
        },
        {
            "id": "22470aee-66a1-42b6-b9e5-01fd8063a225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 250
        },
        {
            "id": "b58bb22b-ee1d-4ff3-a9d6-bb96832f2030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 251
        },
        {
            "id": "1c8e722e-9d6a-4476-8f4e-4a925d45080a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 252
        },
        {
            "id": "a5588317-dc56-4e1b-9b1f-13a3d35fdab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 253
        },
        {
            "id": "dd8fac93-bd63-42ed-90c4-0790f91ae0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 255
        },
        {
            "id": "611cbe32-ff5d-4f7e-b7af-36caf5367af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 338
        },
        {
            "id": "e5589d1f-b316-4163-b71f-ed8e5f6ee886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 84,
            "second": 339
        },
        {
            "id": "65483459-8e95-4100-bcbf-25d35087652d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 85,
            "second": 88
        },
        {
            "id": "73316b12-3b4a-45c3-92d5-78a13dd82633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 89
        },
        {
            "id": "501c4ba6-7584-455a-863d-5b1872ce1ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 39
        },
        {
            "id": "93655bd8-9cfe-450f-ad41-2ee6af5251a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 41
        },
        {
            "id": "41a9c3ca-b239-4644-8b9e-e1bb04bbc8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 47
        },
        {
            "id": "864a7fdf-e1b0-4eec-bf6d-8d2b2635d05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 51
        },
        {
            "id": "b3786e54-3b77-4349-a33c-ea5267fdc1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 54
        },
        {
            "id": "3c790cc9-634b-4b6d-ae77-44218233d545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 55
        },
        {
            "id": "37b8cd70-0854-4d8e-979c-1613d159617b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "7cba14e8-059a-4b3c-997a-d3f22159325b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 66
        },
        {
            "id": "d7b3ec6e-bd3d-49cd-ac42-827f47070526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 68
        },
        {
            "id": "0e6ddc67-e30b-488c-9047-3f74d608f5a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 72
        },
        {
            "id": "ef1a7d12-c817-4ed1-98e1-a8e7efbbe0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 73
        },
        {
            "id": "8ca18df0-1664-4b14-888b-54d9393a3f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 74
        },
        {
            "id": "38c4cea1-1b33-4683-9141-b7a9066b331c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 75
        },
        {
            "id": "57cabaf7-f04d-45e8-9be7-b615f048ab44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 76
        },
        {
            "id": "17576867-5c69-4b7b-8c7e-1d618cff5699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 78
        },
        {
            "id": "493330a9-f219-4705-a407-a5561b510e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 80
        },
        {
            "id": "cc9d5407-5ff0-413b-a326-37e1f2fbd975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 82
        },
        {
            "id": "21cf6eb7-5868-4cfe-861f-11cca0f1a5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 84
        },
        {
            "id": "36b911de-0826-4b9a-8cda-b06051324a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 85
        },
        {
            "id": "caacf816-530c-4968-947b-1e30fcaf662b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 86
        },
        {
            "id": "a89ea6ff-0dcb-40aa-a9b5-215cc27d59bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 88
        },
        {
            "id": "7bc81670-b5d2-452b-9903-42982bdbefa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 89
        },
        {
            "id": "2b8ee180-481c-4cd7-8ab1-ce24209e2bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 90
        },
        {
            "id": "7b1494a7-aa6f-46a4-97c5-e0f6f04dd8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "82bd7c89-25ec-41e8-8f89-fff857ef6092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 100
        },
        {
            "id": "6b6d2704-250f-4df5-805b-4181023c9e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "8886f84f-5976-4cc4-9f80-ed933e9b5484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 111
        },
        {
            "id": "f891bd9d-3b3e-42d1-a08f-db91cebcaf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "22dbf658-54f9-42ca-9609-d3adb79dcbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "ed76bd50-ee91-45f2-a794-41f34919bbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "3d875732-eedf-47bd-b2dd-f7d824e9f477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "b95ffa6f-f031-4c48-b0b8-418dcf6cdc61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 204
        },
        {
            "id": "5a3d9104-8f82-4d67-ae85-ea50fedc0302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 205
        },
        {
            "id": "a41d89ec-e93f-44ba-b6fe-9052d641f207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 207
        },
        {
            "id": "e0dc295e-fd8c-4e72-a074-c854bb7d8367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 209
        },
        {
            "id": "ff99f389-9fdd-4bc4-8d00-33fda98478a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 221
        },
        {
            "id": "bdc06919-81ce-4931-acb5-2782fe606be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 224
        },
        {
            "id": "e56209cd-2a04-4f1f-b775-ca5ea2c7399f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 225
        },
        {
            "id": "f9db2ddb-bab4-496c-bc33-6f75a4b774a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 226
        },
        {
            "id": "c90665de-bb90-470a-a489-d18cd9fa8595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 228
        },
        {
            "id": "0ab2f700-b3a0-45bd-8aeb-c64030e0a52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 229
        },
        {
            "id": "34f17924-c92d-4d9b-936f-10a0264f636e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 230
        },
        {
            "id": "7263ab46-3d83-49e4-82bd-cda37b6e1c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 232
        },
        {
            "id": "5dfab510-48ac-4a06-ac35-26fd0bcbc4ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 233
        },
        {
            "id": "7362b085-fc84-473b-808c-22cf6f9801ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 234
        },
        {
            "id": "715e7a31-349c-4c88-97b8-c64ca0ce57ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 235
        },
        {
            "id": "dc14fcff-5b0b-43d8-8eaf-a190940a2364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 242
        },
        {
            "id": "e292cc05-25fc-4327-9577-3ee64fb3cc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 243
        },
        {
            "id": "6c87c413-a681-488e-8863-3466f743bbe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 244
        },
        {
            "id": "f66ca0bc-296e-46ed-9ce9-111fbe574c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "cc3e9408-2e91-45e1-aa49-c1becabc5e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 246
        },
        {
            "id": "b6d027fc-3baf-40d9-8d94-d163a5167cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 248
        },
        {
            "id": "9de74b0e-6f46-4521-9c0d-77ba436fadb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 339
        },
        {
            "id": "78bec789-04a2-47ce-a116-fafcb6adb2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 88
        },
        {
            "id": "181a2ab1-6dae-470c-9b83-d0052078121c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "d6438056-fc03-49b1-b2d1-389e86b5f64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 100
        },
        {
            "id": "e879ad38-a5ca-4bb5-9c32-aa8b815a8cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "b368a8de-2460-4908-bcc7-bb770fec176c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "b6e09840-f157-4945-b3e0-a90f3a716fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 192
        },
        {
            "id": "6aff17e6-e46f-476d-9c1c-607d1b0bf67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "b7900e5d-804b-4036-8128-7b35f1bcccca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "6cd46a2e-747a-48cf-921c-e2394f7e7508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "789668df-910e-4687-97f9-b2a06c8df5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 204
        },
        {
            "id": "1fa20a06-e825-4dd1-9acc-b7fc9632d135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 205
        },
        {
            "id": "1fadb780-0e84-407f-94a4-268aa99ceb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 224
        },
        {
            "id": "309a50a5-b06d-457b-81c0-d43932598758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 225
        },
        {
            "id": "7d4793de-6d83-4028-95d2-7199b743fb88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 226
        },
        {
            "id": "517878e1-4c3d-420f-b9d4-9d7783ef5070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 227
        },
        {
            "id": "d564f8fe-a623-4c3b-871c-f055c4250f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 228
        },
        {
            "id": "c252b271-ef2f-46fc-b343-acf8d90f5304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 229
        },
        {
            "id": "1d390330-bf6c-48fd-b35f-e9d2122ac6b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 230
        },
        {
            "id": "751fb643-d654-421d-a475-7410f8e4e877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 232
        },
        {
            "id": "281aab52-a2f7-4246-a459-29e78fbde3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 233
        },
        {
            "id": "b00ce770-aeb6-4ac6-969c-cc126e2500df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 234
        },
        {
            "id": "882f527b-c13b-4c72-93a0-eead5feb5156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 235
        },
        {
            "id": "f32757e3-b559-440e-b9a2-0634880b5d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 240
        },
        {
            "id": "cf7b5686-b687-472f-9813-891c578f51d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 242
        },
        {
            "id": "c23b64da-52f1-4834-b7c1-38d72646cc3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 243
        },
        {
            "id": "efec0ec0-59f2-4e14-a7bf-4c6a4ae536a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 244
        },
        {
            "id": "660284b3-21c5-4edf-a30a-151ec422d6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 245
        },
        {
            "id": "0782a624-5690-4bea-a5a2-f040fe2f2d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 246
        },
        {
            "id": "3cdd9382-14b7-435e-be2e-a0983ea9c3d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 248
        },
        {
            "id": "67227b06-66c1-4bb4-9a2d-ecd32f344446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 339
        },
        {
            "id": "b8f7baa9-65b2-45f9-b9b4-ab958b4074da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 39
        },
        {
            "id": "7d2183a7-478b-46dd-b094-fc690079b106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 88,
            "second": 42
        },
        {
            "id": "ce9abd42-a9dd-4d3c-af2c-d34ec598baaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 48
        },
        {
            "id": "378bd58a-7894-4760-96bb-e6241335cd21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 49
        },
        {
            "id": "5553e1fa-4898-4bf7-a7aa-308cdf691605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 50
        },
        {
            "id": "77ee6fa6-1e15-49d2-941e-946b1d606410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 51
        },
        {
            "id": "92fdbc36-13d3-4b54-b8d7-72b19db7e079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 88,
            "second": 52
        },
        {
            "id": "1a17de38-84e6-4905-a1f8-1a6d21bdecf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 88,
            "second": 53
        },
        {
            "id": "f3427122-f06c-48ec-ae91-91feda73b73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 54
        },
        {
            "id": "e35bbe43-0d6b-43da-ae40-a06b310d560a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 55
        },
        {
            "id": "2235eb56-823f-41da-967d-cdcbc13880e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 56
        },
        {
            "id": "d5fba69d-8bed-4509-97e9-5eedc3f39881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 57
        },
        {
            "id": "74becfd1-830c-43b5-a87a-787f6892463d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 63
        },
        {
            "id": "7f276e77-8228-4817-a9fd-04c160ab126d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 88,
            "second": 64
        },
        {
            "id": "d51f2b0d-e37a-418c-a7ee-bd6f3e44a109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 65
        },
        {
            "id": "a5361242-250f-4b94-a609-89784c001267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 66
        },
        {
            "id": "660ad041-0366-401c-aa3f-f72ab24a1eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 67
        },
        {
            "id": "1f2e8d9e-91c6-4125-9acc-3dfd05f01797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 68
        },
        {
            "id": "a1905520-e005-49cc-baed-cb465d92b0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 69
        },
        {
            "id": "ebd93811-6075-4031-ae96-cee3e14d6c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 70
        },
        {
            "id": "613a0288-edb9-4e16-8580-edc8b179a173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 88,
            "second": 71
        },
        {
            "id": "e4c5fd7e-aff9-4725-a69d-eea415623358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 72
        },
        {
            "id": "00c61ad1-759c-45d5-92c7-258cb878ee2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 73
        },
        {
            "id": "319d8672-8465-4751-a758-15bacdbf1390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 74
        },
        {
            "id": "64351250-6a18-4e9d-ba76-def65f0b251b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 75
        },
        {
            "id": "0080b6d8-b395-4344-a0e8-37dd6686455d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 76
        },
        {
            "id": "98cb2a73-048e-4a00-9acd-b946d73d61fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 88,
            "second": 77
        },
        {
            "id": "4c114603-5e2f-4b9c-972e-0d8c7ae21241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 78
        },
        {
            "id": "39db9040-ab42-4449-a904-138ee99d1548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 79
        },
        {
            "id": "f7c858cb-eeb1-45a9-9d93-b976d8a6fe0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 80
        },
        {
            "id": "9e2933d9-55a2-44ea-93fd-32855ea4aa23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 81
        },
        {
            "id": "aa58218e-8977-4c6b-8c15-dd79e20d1045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 82
        },
        {
            "id": "398e17b4-bd65-40e5-af99-3d4b2831acae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 83
        },
        {
            "id": "9415bc4e-2402-4c05-bd85-2d0729d6414f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 84
        },
        {
            "id": "32d28db1-fa8b-4d6a-a83b-c2c70991c9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 85
        },
        {
            "id": "bd6116be-cc9b-4211-9a2b-155cb7d59bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 86
        },
        {
            "id": "788aa3b6-4170-4938-825a-748fc55fbf99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 87
        },
        {
            "id": "d4f8042e-0bfc-4fa0-82f9-6b7dc8272f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 88,
            "second": 88
        },
        {
            "id": "2114759f-2cfc-4a90-889b-222649ce204e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 89
        },
        {
            "id": "038531be-062b-4447-a52d-4783813372ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 90
        },
        {
            "id": "c3c116e9-7da0-4422-8c00-f4143c5b65ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 97
        },
        {
            "id": "02306cb4-e30b-4440-90ae-59bf8a9b61cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 101
        },
        {
            "id": "9883549f-cda5-448a-92e4-6fe77d8240c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 102
        },
        {
            "id": "a4bcc57d-0c1f-40b9-bec7-13f3870aa79f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 103
        },
        {
            "id": "53ad2e67-42f6-4ed7-8942-65ddfb927c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 105
        },
        {
            "id": "122105e6-78d2-424f-acb3-613025fbac9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 106
        },
        {
            "id": "ea94b3d3-9b6e-46c2-96a7-38536469ac59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 108
        },
        {
            "id": "0e035c1c-a271-4516-87c9-5e8da650a81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 109
        },
        {
            "id": "cebf436a-c095-4c0b-963e-5f489f260296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 110
        },
        {
            "id": "f9c27621-0bcf-433b-bedb-4f6d468f5c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 112
        },
        {
            "id": "abcf77c8-8a3e-4329-9310-8be29235b092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 113
        },
        {
            "id": "180686f2-22f6-4676-944e-cfdede49b7f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 114
        },
        {
            "id": "be2692a0-5fa1-4a4a-ab4f-b0b3e99380ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 115
        },
        {
            "id": "116db0c9-d8b9-4c8a-b204-733881eb54d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 116
        },
        {
            "id": "c4e5c231-8064-4bc4-a178-560330135a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 117
        },
        {
            "id": "ab2d6a9e-d897-468a-b164-60f7235f259a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 119
        },
        {
            "id": "1ddc1e69-ce12-4a89-bbce-d1d17cda24bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 122
        },
        {
            "id": "f2f138cf-f02d-43a3-9382-43e52f85a8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 192
        },
        {
            "id": "075461cd-324b-4fb9-93c5-a76167e0008e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 193
        },
        {
            "id": "43bf0a23-6c1d-4c5e-9e07-b5313f8c6231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 194
        },
        {
            "id": "b5430738-20c9-4bee-bd79-f79db7dd5ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 195
        },
        {
            "id": "0cd07745-9745-4056-a14f-0fa5274f5239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 196
        },
        {
            "id": "4366501f-255b-455a-8b23-d1134b751b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 197
        },
        {
            "id": "6884c570-9090-4105-97ed-c05153e49a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 88,
            "second": 198
        },
        {
            "id": "2b0633da-b437-480f-8a8d-e6b48f060590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 199
        },
        {
            "id": "cc836070-25c9-4b83-9b37-7637fcaa44f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 200
        },
        {
            "id": "d5ebdcbd-831f-4c8a-96f3-4a609a1194c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 201
        },
        {
            "id": "15e7b324-f36f-4db2-b1b8-0aa1339081e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 202
        },
        {
            "id": "fe87b44b-0d98-4dfb-ba15-3b4135716ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 203
        },
        {
            "id": "e0bce4bb-42f0-484d-872f-e61316402b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 204
        },
        {
            "id": "eb174432-3695-420b-a032-3ab3905a3222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 205
        },
        {
            "id": "20800808-af2e-4392-9ee5-0868646d3596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 206
        },
        {
            "id": "0b40c4d8-904d-48d6-8c15-b03ba53e3e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 207
        },
        {
            "id": "59764d56-2464-4ce4-8ee9-9491949b6013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 208
        },
        {
            "id": "d6e33bc2-3fe3-4773-83ad-970ab59b8db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 209
        },
        {
            "id": "e60a376c-4ecc-43d9-8fe6-b7fbeb3f5b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 210
        },
        {
            "id": "562f31b2-376f-4aba-a18f-8b57e7ac378e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 211
        },
        {
            "id": "d20fc1e5-52e1-48a4-b44e-c68948935338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 212
        },
        {
            "id": "7e4a5f3f-6b7b-4212-8509-edd5587bbed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 213
        },
        {
            "id": "f64a8a43-6ab0-4fec-afcc-ba68571c0794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 214
        },
        {
            "id": "04b8c8b4-3918-4745-a3ca-f8fd6d3153b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 216
        },
        {
            "id": "3b62fa39-40a6-41c6-bbf6-3f0d8dcb1dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 217
        },
        {
            "id": "8076a162-9e95-4546-aff7-d4bdbca7148b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 218
        },
        {
            "id": "59f1aafb-a6d7-4f23-9a84-21e36b56ca3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 219
        },
        {
            "id": "df2dbdb4-0d66-4640-834b-1789d9b8d89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 220
        },
        {
            "id": "3b5f5aa1-5573-4c73-aa7d-a67df2ccbb9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 221
        },
        {
            "id": "48098d54-6498-4509-8e2a-c42f3141b3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 223
        },
        {
            "id": "11ae76e9-a2ea-445b-a8da-d72980db4cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 224
        },
        {
            "id": "11b0e1b7-944a-4696-86b9-5bac5e41e0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 225
        },
        {
            "id": "77732303-5212-4b4a-935d-631c9df2819c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 226
        },
        {
            "id": "4843b9a4-5804-44d7-a18c-ad284f8cb13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 227
        },
        {
            "id": "94a31ed0-2816-46ec-af32-c6427c163bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 228
        },
        {
            "id": "d9443782-80b2-4f3f-8a60-7fc9ad248aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 229
        },
        {
            "id": "7ed144ab-f61a-4ab2-bea7-1f04003fce2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 230
        },
        {
            "id": "6be652ab-d517-47d8-a2e7-8b275345d628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 232
        },
        {
            "id": "6af613da-efce-4533-9a0c-25348c2c2f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 233
        },
        {
            "id": "c198acaa-4ba4-48c0-8e83-709a0b88482e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 234
        },
        {
            "id": "c413a4b4-4082-44fe-8fb9-7d448a8e57a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 235
        },
        {
            "id": "a16ae19a-e59e-4861-8c85-ba66a10e4352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 241
        },
        {
            "id": "e10b6793-1f73-44f2-b35d-cde2c41f3248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 249
        },
        {
            "id": "0e599454-6494-4bee-bc70-ab59ab15fadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 250
        },
        {
            "id": "b645f41c-083e-4365-9313-e8cbcacb9540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 251
        },
        {
            "id": "b39ff43b-e347-4a18-9825-80a74f8bd8f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 252
        },
        {
            "id": "71418c49-2dfd-4a8f-8d59-c7d0b194e8cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 338
        },
        {
            "id": "1c1a9035-4c85-4ca7-b524-48282734a228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 8260
        },
        {
            "id": "e193e2c9-5a7f-4a5b-b3a0-11acc3a2361d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 88,
            "second": 8355
        },
        {
            "id": "71735aa8-0fbc-4b0d-934d-c52d619d7b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 8356
        },
        {
            "id": "efd16e4f-5f76-4c0e-83df-89da5e450838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 8359
        },
        {
            "id": "f3cf64b7-5385-4bdd-8186-df4de32bfe87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 8364
        },
        {
            "id": "6888c29a-6987-4516-bf6b-1e734819f9fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 8482
        },
        {
            "id": "fcc0e158-b76f-491d-9632-d51ac6f83584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 8706
        },
        {
            "id": "c2826039-2be0-4a6e-b90e-b9146b52d2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 88,
            "second": 8725
        },
        {
            "id": "04a0c0c8-350f-4033-99cf-bf115dee127e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 66
        },
        {
            "id": "2d23d7bb-5087-4145-9337-d502aecd756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 68
        },
        {
            "id": "a18fb79c-6ea5-47ac-a72a-adb0d019c86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 78
        },
        {
            "id": "4c4c3ccd-1831-4b59-bcab-82d01b3e3b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 80
        },
        {
            "id": "aa826b55-b423-47ba-b611-b3b2184f9af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 82
        },
        {
            "id": "1a6ab044-9674-4049-83c6-de5b935c83c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 85
        },
        {
            "id": "0f17597d-16f0-4dc7-8a53-7df4978d0aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 86
        },
        {
            "id": "4b28a451-74e9-499c-b185-bc3a4d846911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 88
        },
        {
            "id": "17843bdb-38c2-447e-bf5b-ce7f9c75ade7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 89
        },
        {
            "id": "b2010e3a-d58b-4abb-8989-3623ceba78ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 97
        },
        {
            "id": "25c2d657-e1d0-4960-87b1-54cc09fd77a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "9e48e2c6-2b79-4c0e-aad1-c3547454775c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 100
        },
        {
            "id": "aeb93701-bf75-4978-b3f4-9a375230eda9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 101
        },
        {
            "id": "97ead1b3-b838-459f-91d8-19636817b0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 103
        },
        {
            "id": "bc5cd468-4652-487b-9a47-e3ea5ff86f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 111
        },
        {
            "id": "7173c5f8-84d2-4efa-9d0e-762d4f9ea4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "f9890a4a-7f23-4bc5-8731-b687e18fce63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "6d37f54a-1a1b-42a4-ace9-ae43def27704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 192
        },
        {
            "id": "333858f1-60e0-4336-bde8-ea49f61149d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 193
        },
        {
            "id": "6504307a-e4c0-442d-a5f1-e0a8fd9ed577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 194
        },
        {
            "id": "42b1cc4d-e043-4dee-904f-a70cb1d8d243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 195
        },
        {
            "id": "85682ea1-8b59-4eef-b7ce-7452de777026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "5e0a7ded-7800-4046-9d40-1c8584c01797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "731a1020-363f-4fbb-abcb-c1df96509876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 198
        },
        {
            "id": "f504fe7c-02b8-4d4c-b727-a6c3befb2563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 204
        },
        {
            "id": "139ec048-c401-45b1-9990-d654224f9da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 217
        },
        {
            "id": "ff980465-8f9c-4c79-aeaf-10865aabbd5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 218
        },
        {
            "id": "58daf8d8-03ae-40bf-813f-ef71aee8b476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 219
        },
        {
            "id": "2667f9f2-5006-4feb-991e-25a7e53c3071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 220
        },
        {
            "id": "8996bffa-62cd-4cdc-9a0f-62a58d49ad16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 221
        },
        {
            "id": "1cddfc5e-b356-4343-95c6-f2c355a3386f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 224
        },
        {
            "id": "2b5afc19-052a-4910-85e7-0363d06e0944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 225
        },
        {
            "id": "667526d4-2bc1-45bb-92d2-f3e2d4996622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 226
        },
        {
            "id": "2257da1b-6894-4c4a-aaa5-29f8ee8699a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 227
        },
        {
            "id": "f7bd6443-40f8-4274-8b53-b3bfffc95a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 228
        },
        {
            "id": "b4d4ec6e-9644-4ec0-9c4b-40fd43261c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 229
        },
        {
            "id": "7d12fb72-4e5f-47f3-b1a0-00a4ce794b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 230
        },
        {
            "id": "4082dd77-8b8f-4120-b046-198f7bebdb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "b3db61b5-5c69-4f5b-af71-4a6a23650773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 232
        },
        {
            "id": "bc591c76-78d1-4a08-83fa-57e03f7b0e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 233
        },
        {
            "id": "eee6235f-86f2-4ac7-8a97-7b7c7499a2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 234
        },
        {
            "id": "63288c07-20c6-4cc0-83b9-c9093f3b1804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 235
        },
        {
            "id": "ba3a598a-b12e-40e3-b207-44fc24ffa995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 242
        },
        {
            "id": "47d71907-0ba5-4031-bac4-9e4bc46115d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 243
        },
        {
            "id": "d40a4d91-d5ce-4b83-b971-60ffb3562bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 244
        },
        {
            "id": "acdacdea-fe4f-4bbf-950f-3c07462f76ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 245
        },
        {
            "id": "c62bfd2a-d795-4b8a-9925-cd900d6a9421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 246
        },
        {
            "id": "1fc486c5-8f96-4701-9214-ec31ee861e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 248
        },
        {
            "id": "c76544d0-1f9d-4af0-a699-0f561205dce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 339
        },
        {
            "id": "d395370b-ec04-4677-a7a1-476fec9359f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 56
        },
        {
            "id": "76940bea-5d68-478d-91da-60a37e6aa617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 67
        },
        {
            "id": "ec211934-b699-4129-b17d-9ba03d2a69fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 71
        },
        {
            "id": "8c6e5085-c493-46af-ab2a-f4e218f3c67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 77
        },
        {
            "id": "8c544a48-a49b-40a6-bebd-4aa4891863b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 79
        },
        {
            "id": "e4850f3e-3df9-4e4b-9daf-76e0629a69fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 81
        },
        {
            "id": "e8bc8e67-4ecb-4144-a5ff-966d2bf68cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 83
        },
        {
            "id": "0f2fa934-518d-4e6a-8bb8-a129bff23d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 85
        },
        {
            "id": "ed9df7bc-e905-4bca-ac33-3d62fb54c104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 87
        },
        {
            "id": "df057cfd-211a-4e59-a184-e1753d19317e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 88
        },
        {
            "id": "31259c97-2887-4261-98dd-1b5beebe218c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 89
        },
        {
            "id": "1dfeb937-7965-4162-bcc5-d5ff4761b271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 97
        },
        {
            "id": "03d3f7f6-2f24-42ff-94b7-b73ba83661fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 99
        },
        {
            "id": "0bb808af-4df1-4b4b-a102-901757971ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 100
        },
        {
            "id": "9c8e8d6f-84d8-4581-8350-b5cc54f100a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 101
        },
        {
            "id": "bb6feb41-afe9-4eb3-aed0-6ea9753c3268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 102
        },
        {
            "id": "97efb87f-76e7-4226-b853-4bc769eba4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 103
        },
        {
            "id": "43d52b28-b6ec-4554-b6df-32b80b869e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 105
        },
        {
            "id": "cc6e2bd9-4c3b-41df-a737-e47d5b6bc743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 90,
            "second": 106
        },
        {
            "id": "e8ff6918-66cf-42b7-84b8-154db5e7b474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 107
        },
        {
            "id": "c3cbd722-7341-4640-8563-8b06646c7da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 108
        },
        {
            "id": "13a337f0-59bd-4b23-8ef5-9492e5fb228f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 90,
            "second": 109
        },
        {
            "id": "4d4451c0-d911-4e75-a471-4db96cd2da6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 110
        },
        {
            "id": "f27593e0-d843-48ae-aaa9-c381afbfea7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 111
        },
        {
            "id": "35ea1e11-ddff-4bcf-a475-94749cdbe581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 112
        },
        {
            "id": "8e9fd030-aa86-4458-b65c-3aadbc3dcf88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 113
        },
        {
            "id": "e4dba80f-c5f2-4f28-a081-da48066e18bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 114
        },
        {
            "id": "5aaa551c-a772-4037-a81c-5dfb5d00b15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 90,
            "second": 115
        },
        {
            "id": "54d88326-d2ce-4d51-abd1-b56fb7a55d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 90,
            "second": 116
        },
        {
            "id": "fad92c5a-832d-4652-b0fe-031c2567d57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 117
        },
        {
            "id": "cba287c1-2077-438d-a447-44158106d1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 90,
            "second": 118
        },
        {
            "id": "9faf2ffc-d239-4dbf-a69a-bf41b2cb4253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 90,
            "second": 119
        },
        {
            "id": "c5c7ea30-36e2-49b4-adfc-3198f8204a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 120
        },
        {
            "id": "69afa32a-a417-463b-b999-c3d0e1f9b272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 121
        },
        {
            "id": "b4856b53-c123-43fc-9b31-46d1f84cf684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 122
        },
        {
            "id": "6dee5a6a-b9d7-420b-a6c8-81d1bda34cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 198
        },
        {
            "id": "9033af54-3675-4fef-8c5e-67233da9d3e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 199
        },
        {
            "id": "befbeccc-5514-4e48-a45c-9ad58c900453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 210
        },
        {
            "id": "d836c9df-6e3a-4ddd-a2be-a63226c39e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 211
        },
        {
            "id": "ddc8bf9f-9c07-44c5-964e-eafca177637a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 212
        },
        {
            "id": "a499ed81-dcc8-487d-b5ef-c729d89f367f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 213
        },
        {
            "id": "df766229-a9c0-445d-abd8-b45b9d5d5178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 214
        },
        {
            "id": "933e4d18-e7b7-4ae5-887a-3a149e3fad49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 216
        },
        {
            "id": "c0e36e77-acee-4d9f-a666-a7fe8808f6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 217
        },
        {
            "id": "55bd7146-f9e7-4e8a-9b42-de1b86203fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 218
        },
        {
            "id": "40ce837e-fb74-45e2-a220-3e733883173b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 219
        },
        {
            "id": "d661ef7c-97b6-46a3-a0df-d0eac9e2c64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 220
        },
        {
            "id": "f88f81d0-8def-4bb6-9606-7beb2b036a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 221
        },
        {
            "id": "36317241-d0f6-42c1-b259-3f0de97bea34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 223
        },
        {
            "id": "09c82c5e-d222-445a-971c-0acbdae2b657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 224
        },
        {
            "id": "e9b4e67d-ae9d-44dc-b432-13e31d916d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 225
        },
        {
            "id": "514ebf75-b0f6-4b1d-92aa-66fd26cd3f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 226
        },
        {
            "id": "d0a0831f-7797-418a-82b3-0c0169b668c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 227
        },
        {
            "id": "abee252c-0f0c-4283-9135-fe38679d688c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 228
        },
        {
            "id": "3edad349-4e01-4840-ac4a-ccba1923875e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 229
        },
        {
            "id": "4df51e7c-6543-41fa-8888-dbca6c830401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 230
        },
        {
            "id": "28a72acd-47e3-404e-8bfc-9ee218c226df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 231
        },
        {
            "id": "57558049-0ecb-4c0b-8a97-ee9895ef36b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 232
        },
        {
            "id": "72f9b063-21fd-499f-9ee5-92cadf0d9f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 233
        },
        {
            "id": "383065c0-98d0-489d-be1c-0c92a468840b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 234
        },
        {
            "id": "a1a9dcdf-a205-4e3d-82ff-9e2c2c72a377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 235
        },
        {
            "id": "c6587536-085c-43fd-b7c5-07bf3fdfe953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 90,
            "second": 236
        },
        {
            "id": "0f94c777-4dbd-4a20-af5f-24c9a63db800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 237
        },
        {
            "id": "12e6fedf-27f5-4c15-a345-7df3c02f0737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 238
        },
        {
            "id": "5192fd40-baed-400f-800f-263bf7e49ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 239
        },
        {
            "id": "61f5b013-345b-4c95-a2ae-890506db3c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 240
        },
        {
            "id": "a6fe10c6-dab2-43c8-beaa-f8cde2669abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 241
        },
        {
            "id": "9a2f93e6-a8d1-48c9-9487-0c69782e35db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 242
        },
        {
            "id": "3d955805-6b8c-4e3c-b8d6-15be1e220bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 243
        },
        {
            "id": "18c4fd94-d7d4-4080-9b85-d7b5b1665f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 244
        },
        {
            "id": "12c60310-6e6d-4f1a-bd54-e3ca2fda3823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 245
        },
        {
            "id": "d5f426c6-a28e-4b12-a6c4-856f4a2feb9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 246
        },
        {
            "id": "76628771-cb3b-4f88-9f4b-d140278cfc2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 248
        },
        {
            "id": "f9dc4a8f-05ee-4162-ad1d-8e96e48623f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 249
        },
        {
            "id": "1118b149-faa5-4349-842f-d6ed99681089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 250
        },
        {
            "id": "4dfefee5-c984-4544-abd1-619fd0bcb98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 251
        },
        {
            "id": "f01bd591-6c12-4630-8181-33e5c9c4714c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 252
        },
        {
            "id": "b60e3ac0-6636-4267-9221-fc72638e3670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 253
        },
        {
            "id": "ea6103de-8d15-4727-ad42-eb799acd7013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 255
        },
        {
            "id": "960565a8-c07c-4ffc-a051-69118408c986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 338
        },
        {
            "id": "22bd0e99-9960-4343-8ea6-5eb80203f89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 339
        },
        {
            "id": "21dc0220-9c39-4f4d-95e3-35c5196edc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 92,
            "second": 88
        },
        {
            "id": "d0ec738c-4c26-4602-a905-881595d0d548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 41
        },
        {
            "id": "a377f234-51f3-4847-9ac7-18bf9886d39f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 72
        },
        {
            "id": "c0d71608-717e-404c-b209-80056464ea38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 97,
            "second": 74
        },
        {
            "id": "650cdc0c-c050-446a-8a8e-ebb6d4fadd8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 78
        },
        {
            "id": "bca33625-01a1-4a85-a189-7cd56a92036f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 80
        },
        {
            "id": "2ec60ac7-4a4f-4376-a678-0c6071c23809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 83
        },
        {
            "id": "9304d4ab-d358-43ca-8c43-b9b57559e918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 97,
            "second": 84
        },
        {
            "id": "5d1a3a64-f3f0-4e5e-bdcb-8928ebe87d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 97,
            "second": 86
        },
        {
            "id": "7238e96f-bf6d-4692-b1f2-147c76de5175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 97,
            "second": 88
        },
        {
            "id": "1439cd8c-69d9-48d4-b1fb-28c0c6230855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 97,
            "second": 89
        },
        {
            "id": "72ba4ab5-e84c-426e-8074-9e8d8b9981ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 118
        },
        {
            "id": "473ad5bc-2a73-4f0b-9ad3-6efec6c5a8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 119
        },
        {
            "id": "d6aef34c-b4d5-46a0-973f-0fa2b343015e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 192
        },
        {
            "id": "64ee0c0e-8c36-414d-9220-42bf840f01d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 193
        },
        {
            "id": "bbccd4c6-bae2-43c4-9317-c77433c266c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 195
        },
        {
            "id": "be9ed802-2e79-448e-b357-d1390480dce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 196
        },
        {
            "id": "7a0e01d1-386b-46a7-b94b-3838acf32658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 204
        },
        {
            "id": "1ab4cda7-a23d-4ea7-ab9d-da181a959a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 205
        },
        {
            "id": "d47ccbc8-b0d3-4c33-a242-5e989d9577d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 207
        },
        {
            "id": "c0cc9c8d-dbb9-4ff1-b4b8-a540b0758667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 209
        },
        {
            "id": "0f90d0f5-bdfb-4a2f-ada0-fdc1c3bff018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 97,
            "second": 221
        },
        {
            "id": "af1edb1f-f547-4e49-8818-33d4291e4e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 98,
            "second": 41
        },
        {
            "id": "7575dfd4-509b-49cf-8fdd-002a57abef05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 50
        },
        {
            "id": "e41a6bef-5a15-48f6-956c-d4099baf0402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 98,
            "second": 51
        },
        {
            "id": "b2b71e8e-0c88-4f15-9142-cf2cda63e1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 98,
            "second": 57
        },
        {
            "id": "3f569915-010e-4eba-9f89-c81d864ce646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 66
        },
        {
            "id": "52f47491-6f14-4726-bfe9-f8f8f26b9617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 68
        },
        {
            "id": "9f4ed57a-e545-457b-b76c-49df250f53c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 72
        },
        {
            "id": "a02547ec-2f2f-4730-a553-62f8b6b3adac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 73
        },
        {
            "id": "569b9ce6-6933-4373-96aa-b64eff269ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 98,
            "second": 74
        },
        {
            "id": "879b082e-29e2-42dc-b494-8bdcedc429fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 78
        },
        {
            "id": "989e1779-097c-408d-bde8-c13b0ec0a93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 80
        },
        {
            "id": "f3042a03-1fed-4e91-a5bb-ca06ac184a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 98,
            "second": 83
        },
        {
            "id": "63ff465e-0459-4bc8-be87-2a32509a56ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 98,
            "second": 84
        },
        {
            "id": "a1921e57-878c-4a6e-b493-e91fa4fb6fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 98,
            "second": 86
        },
        {
            "id": "54094a38-e970-47d0-9295-bdb5cda154c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 98,
            "second": 88
        },
        {
            "id": "af3dd20c-8d52-43b9-94fa-1d6b98b3d883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 98,
            "second": 89
        },
        {
            "id": "f4bf5bc0-61dd-423e-a8e8-c09da20338ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 98,
            "second": 118
        },
        {
            "id": "0371c529-f1d9-4ce4-9528-f1092a0ecbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 98,
            "second": 119
        },
        {
            "id": "bff0c2a9-fdf3-4e59-9286-ba75a7894e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 192
        },
        {
            "id": "56d87fe6-c023-4546-98c6-05367e5bca96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 193
        },
        {
            "id": "e1ba82bd-67d7-477c-a1c3-74d21d2bddb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 195
        },
        {
            "id": "b1d16184-2695-4089-982d-f4b5050e0c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 196
        },
        {
            "id": "c6d2eeea-4268-4696-abb1-3439f7b2606d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 98,
            "second": 204
        },
        {
            "id": "240c0080-eec0-44d5-8a16-5f91b3d230c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 205
        },
        {
            "id": "5a13ebda-9963-4b0b-9ca6-cdd287d107da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 206
        },
        {
            "id": "3708fd96-75a3-47d3-9a0b-9bb15b0dd01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 207
        },
        {
            "id": "eb1eab12-4a0d-4cb3-b9e2-662fd799bef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 209
        },
        {
            "id": "f5f63deb-df3b-426b-ad7c-f9b00bb5a903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 98,
            "second": 221
        },
        {
            "id": "03d27a3b-ae81-46cd-a2c8-48f130bfc1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 99,
            "second": 41
        },
        {
            "id": "02c41c88-fb3b-4157-b58c-24a1098ab1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 99,
            "second": 48
        },
        {
            "id": "10d3f015-1f04-465a-b2cc-5728f6c2c177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 99,
            "second": 50
        },
        {
            "id": "862299d4-1b32-4001-8237-0dbc77516f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 99,
            "second": 51
        },
        {
            "id": "df760958-b4ce-4696-9ec2-a5da1074f7c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 66
        },
        {
            "id": "f4b9f9e4-a81d-4706-b08a-89b37057eee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 99,
            "second": 74
        },
        {
            "id": "cce2fe6e-ed79-4656-a939-8a5c3a8e2225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 78
        },
        {
            "id": "9106f2c5-3351-401f-9949-68ed8c43699b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 80
        },
        {
            "id": "47fe76d8-0b71-49c7-8d09-9a10ea4155f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 82
        },
        {
            "id": "0cae4a2e-a317-491e-8eaf-06b69f697ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 99,
            "second": 84
        },
        {
            "id": "328fee1c-c67f-4ee8-a455-97e2853dfd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 99,
            "second": 86
        },
        {
            "id": "beefc4f6-1c62-4e46-b566-8829cc4c801a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 99,
            "second": 88
        },
        {
            "id": "9b97f6d0-4252-425c-b1c9-28fe536edbae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 99,
            "second": 89
        },
        {
            "id": "9ecd2db7-7d6d-473f-904a-2d95f45dda1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 90
        },
        {
            "id": "e41298a3-d06e-4269-9168-8f19e0b86a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 97
        },
        {
            "id": "87b4ba55-b321-4bd8-bcb2-b39d82e8b6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 100
        },
        {
            "id": "9b4b0958-7e22-4e9a-a4e5-e4de1f7e2dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 101
        },
        {
            "id": "f5cb78c2-50f1-44f1-a4de-af67a2262259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 111
        },
        {
            "id": "0968b0cf-8c84-46de-b249-dfd98cfe8d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 118
        },
        {
            "id": "3a79414d-df14-4105-bcc4-46cea6bf8c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 119
        },
        {
            "id": "792e80f0-1393-4a86-95b2-8cf5c3e2921e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 192
        },
        {
            "id": "095dd830-afd0-42f2-8d3d-3486900daf1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 193
        },
        {
            "id": "4341b5c1-dbee-44f7-a1d7-d290d327d7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 195
        },
        {
            "id": "3aff1843-6d18-42dd-a374-0f0726a79145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 196
        },
        {
            "id": "3d4dff25-17d8-43ef-96a8-4228594be3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 204
        },
        {
            "id": "e17495c3-7f55-4e47-a7db-b79f88475e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 205
        },
        {
            "id": "7389f944-c4ef-4981-89e9-10a58027f397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 207
        },
        {
            "id": "98021f3e-30ff-4b85-aab2-511016f8b87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 209
        },
        {
            "id": "045d1a7c-9e27-4f58-adab-743f60516fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 99,
            "second": 221
        },
        {
            "id": "025f1bd4-821f-426a-adce-107dff7956e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 224
        },
        {
            "id": "ceec2d1c-8a65-4534-a39e-c936e435c3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 225
        },
        {
            "id": "fdc59a1d-955e-47c2-bb81-6fb19d204136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 226
        },
        {
            "id": "a6507005-2e3f-4717-8dde-5ca0bede2f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 227
        },
        {
            "id": "8a8de547-6fa6-4929-b515-899434683b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 228
        },
        {
            "id": "1a51615a-b71c-4aea-80c1-3a55e23e8f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 229
        },
        {
            "id": "abd19124-a031-4ea8-97c1-8cf814db5743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 230
        },
        {
            "id": "8bcb0e10-0699-4a90-b7b0-86cc2e2dd8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 232
        },
        {
            "id": "166e0903-c947-4929-b954-79d41153ac7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 233
        },
        {
            "id": "f231b2c2-a6f0-4672-89d5-1db458d4f54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 234
        },
        {
            "id": "3898b396-73bf-4a2a-8fde-a1c0a69a8426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 235
        },
        {
            "id": "ba77b6d3-feae-4f80-b721-9db8cd31580e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 240
        },
        {
            "id": "1072edbb-aa52-4d7d-a810-15c29a6452b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 242
        },
        {
            "id": "ccb838ab-a4ff-45d6-8f71-80391e403cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 243
        },
        {
            "id": "eeeb4b44-56b7-456a-8cdb-3bff6a630928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 244
        },
        {
            "id": "e69b0f46-6eb7-4d75-acac-a56c413a621e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 245
        },
        {
            "id": "b59b1ef3-86c2-436a-be13-c99a644be6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 246
        },
        {
            "id": "06ffae52-6665-428d-ac5a-582231266bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 248
        },
        {
            "id": "545f0c32-7a61-4382-b127-c2312748af28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 339
        },
        {
            "id": "94ad46bb-cfb3-44c0-8458-7833b75a8756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 8706
        },
        {
            "id": "815d0595-5a84-4b44-b0b3-a20b4dc4fde2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 100,
            "second": 51
        },
        {
            "id": "13a5a5c8-7897-4a35-b7d9-e30c39dca837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 57
        },
        {
            "id": "cc8db050-554e-4023-ba1e-9eea59ffbb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 65
        },
        {
            "id": "297c1b62-635c-44a7-9fa7-e5d352a407c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 66
        },
        {
            "id": "86e9db44-dad9-41e2-bbb6-1fe4808230f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 68
        },
        {
            "id": "d746bc21-c7f6-40ce-bdf1-8fe7c46d41e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 72
        },
        {
            "id": "d9627e0c-f24c-40d0-8dbd-1fbb46613230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 73
        },
        {
            "id": "6b886d4b-dbbf-4bc0-82ac-48597f807861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 100,
            "second": 74
        },
        {
            "id": "839d0f21-ba94-4721-ad45-cbb0073778bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 76
        },
        {
            "id": "63409700-261d-4f41-aab4-89450a732e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 78
        },
        {
            "id": "75a7c00b-d35a-44ce-bd4b-4570b555f8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 80
        },
        {
            "id": "0f32ffc4-29c0-459f-857f-84daed66abe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 84
        },
        {
            "id": "0bd9193e-435a-4791-a169-5ea26ee73ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 100,
            "second": 86
        },
        {
            "id": "09a24825-2733-4673-ab69-42af81e18a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 100,
            "second": 88
        },
        {
            "id": "f25bbb7d-941c-411b-ba71-088e80ae3ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 100,
            "second": 89
        },
        {
            "id": "93ad359b-e710-49d1-a9c0-23b3d481b7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 104
        },
        {
            "id": "0eb96dec-1b55-404c-9caf-c66270467541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 100,
            "second": 118
        },
        {
            "id": "38ea7a73-11a1-4539-9c30-c5e89c713682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 100,
            "second": 119
        },
        {
            "id": "9d3a9b97-0678-4b43-994e-5ffa514e885d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 192
        },
        {
            "id": "a1ee5aed-e843-4d86-854b-61106bdf2dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 193
        },
        {
            "id": "aa7ffc9d-ae43-4ca7-9794-1b0a4176fdfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 194
        },
        {
            "id": "f1ea1d73-6497-4655-b1f1-e8f4e200467d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 195
        },
        {
            "id": "be9a9be3-00af-4d86-8289-92615b3e1f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 196
        },
        {
            "id": "58418fe2-7605-4d1d-bbe0-8b6ac45b65dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 197
        },
        {
            "id": "8ed1ae62-6d8c-4574-a962-1f65775bc339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 205
        },
        {
            "id": "3d1ef45f-7f0f-408e-9a89-36d5524d0378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 209
        },
        {
            "id": "05a971af-af99-4b52-8cc8-d3cf60bd405e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 100,
            "second": 221
        },
        {
            "id": "de92fc3f-54b1-4583-be5d-de87ca4a75f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 51
        },
        {
            "id": "2446b4d1-7d55-41b4-88d4-70a74ba6e1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 101,
            "second": 74
        },
        {
            "id": "c8da2aba-f951-438b-bde2-f021aaaeec78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 101,
            "second": 84
        },
        {
            "id": "b697abe2-4bf1-4815-8594-f0594a42dfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 101,
            "second": 86
        },
        {
            "id": "1815c99e-01c5-4722-bd16-2aa99d642c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 101,
            "second": 88
        },
        {
            "id": "2b9b1c1b-68d9-4c10-a0db-2c6e4a5446dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 101,
            "second": 89
        },
        {
            "id": "a0b5d30b-d9ee-4b57-8e49-081774f2ff03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 100
        },
        {
            "id": "3f046945-9e26-4457-bfa0-5f7c5e9afe0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 111
        },
        {
            "id": "db51eb5c-9f46-4ce6-b898-e64f6f1192fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 192
        },
        {
            "id": "b9f13b6d-9466-4cbb-83a5-229baefdaccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 193
        },
        {
            "id": "b23e4d43-1f9f-4c0a-bd4d-106958f1b237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 195
        },
        {
            "id": "c558a48b-81f4-4fd9-9cf4-8d4e0a70e175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 196
        },
        {
            "id": "6f581847-080d-48f9-b6fb-f6ddff0d595e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 204
        },
        {
            "id": "de7bc5a2-3b95-4271-b72a-46f015dfcbd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 101,
            "second": 221
        },
        {
            "id": "65c614bd-a81a-4eb7-a2f7-4c797d8fa16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 240
        },
        {
            "id": "2d6551f2-2105-4908-801b-c39aa885698f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 242
        },
        {
            "id": "e27565b4-d28a-4dbe-8c10-b2b178ec9d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 243
        },
        {
            "id": "63c99eef-893c-4942-b05d-228bb4e9a1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 244
        },
        {
            "id": "16dfce7a-b170-4f77-966e-6a18d6c4faee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 245
        },
        {
            "id": "f56a0755-cf59-48d4-9bfd-31ccde179bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 246
        },
        {
            "id": "e880c1a8-65b5-443b-9583-398715e4d154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 248
        },
        {
            "id": "f36b2850-c2c0-4a5a-8858-0c55f1a80428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 339
        },
        {
            "id": "2fc36dfb-24f3-43f5-be0f-615cd41aeec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 66
        },
        {
            "id": "99dfcd45-e7a4-4634-b6b1-9ffa93f39d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 78
        },
        {
            "id": "de1d4c55-4a2e-4e1f-94dd-f7584dc474d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 80
        },
        {
            "id": "7596a57b-2bfe-4060-8c1e-0266c4c4e573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 86
        },
        {
            "id": "af810cf5-8d3f-4aa3-994f-1c52fff53448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 88
        },
        {
            "id": "47c7f95d-1e7c-4cde-a316-545fea475436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 89
        },
        {
            "id": "f53741c9-9308-4915-b46e-d0d07fe607a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 204
        },
        {
            "id": "a319baba-a2f7-42ca-ab79-a2fec3b51cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 221
        },
        {
            "id": "3d58fd84-4de0-4b0e-a658-13a57cbce3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 103,
            "second": 51
        },
        {
            "id": "72738e10-4593-47cf-a49d-503001577401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 103,
            "second": 53
        },
        {
            "id": "e4e7eb62-9a18-4700-9811-21070da91d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 103,
            "second": 57
        },
        {
            "id": "a0eb5e25-5816-4eeb-a47d-b73561ccc8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 66
        },
        {
            "id": "73700437-42a0-4e55-84d5-b64cfd8dd763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 68
        },
        {
            "id": "81b5fc9a-7bee-4c23-a9fb-cd44f27a1ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 72
        },
        {
            "id": "010845db-bed3-4d3a-8013-e31f035f3c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 73
        },
        {
            "id": "5471f39b-2a45-4ebe-948f-428ad8760a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 103,
            "second": 74
        },
        {
            "id": "1086e809-1208-4de0-9adc-0a99ba8856a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 78
        },
        {
            "id": "be812c89-ac87-4437-870e-f27154e014f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 80
        },
        {
            "id": "c89a7357-f97c-44f6-9f88-8f818927403a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 103,
            "second": 83
        },
        {
            "id": "7f74c840-5aae-4e61-ba30-9b6c693dd25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 103,
            "second": 84
        },
        {
            "id": "08c33539-1834-4c08-8e98-4bc1b4641a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 103,
            "second": 86
        },
        {
            "id": "df298cf0-d746-4342-b405-83b3f5374668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 88
        },
        {
            "id": "9640f5a7-1eca-4013-8c0f-2a7e0433fee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 103,
            "second": 89
        },
        {
            "id": "f2384c81-4326-4f62-8263-7ce70fd31491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 102
        },
        {
            "id": "adc9b140-705f-4b40-ab41-5e2c6eb020cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 104
        },
        {
            "id": "77b779fa-5a7b-4cfc-b15a-d3c389eab3ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 106
        },
        {
            "id": "1fc6bef5-b7fc-4718-8f93-d20a96741b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 109
        },
        {
            "id": "7325f729-7edb-470a-b9aa-a9529729fda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 110
        },
        {
            "id": "4ece990a-9aa8-411a-b6e7-d28a02ed0fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 112
        },
        {
            "id": "0a20c5be-56b2-412e-8b6b-de2d4941dc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 103,
            "second": 118
        },
        {
            "id": "74a68f7a-85fb-48a5-be94-655f73b216f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 103,
            "second": 119
        },
        {
            "id": "4256c1ec-03f1-4ef4-ad77-1ddf71795489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 192
        },
        {
            "id": "ca421e84-0910-46dd-a92d-5592b481c53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 193
        },
        {
            "id": "1feb9302-7940-402d-80b3-b5f3897ff462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 195
        },
        {
            "id": "9834403b-e5ce-46cd-9c0c-682114ea1a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 196
        },
        {
            "id": "74b035d0-422a-4c22-baaa-6999d49879f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 198
        },
        {
            "id": "c39b8c85-6cf9-4f81-a07c-cbbe781fceaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 103,
            "second": 204
        },
        {
            "id": "c3376c6e-ae2a-44b1-966a-cbaad66bd868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 205
        },
        {
            "id": "3767f7e2-8435-44b3-99ca-be92ea036548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 103,
            "second": 206
        },
        {
            "id": "dc08d542-736e-4270-b721-cbb39cc5ac72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 103,
            "second": 207
        },
        {
            "id": "9d6cde2f-84ed-43c5-bc95-635fb8ad6ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 103,
            "second": 209
        },
        {
            "id": "02697412-1fd1-420c-a3c1-457d6691f183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 103,
            "second": 221
        },
        {
            "id": "dea820a0-1592-4d6c-a1cc-c08a0382bc8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 41
        },
        {
            "id": "f7127b8f-cb79-42b5-a56b-a531b71f0ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 104,
            "second": 51
        },
        {
            "id": "a22970bf-5502-402b-bc04-d9c85113a0fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 104,
            "second": 65
        },
        {
            "id": "426e1a1a-e17b-45fa-bfb0-49cf51d05b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 66
        },
        {
            "id": "547ffc61-2201-4d9a-aeb9-625abacc47cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 68
        },
        {
            "id": "da96e675-a9d5-4384-b480-dbd78bcb9411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 104,
            "second": 72
        },
        {
            "id": "3b78b949-091b-43b4-b15c-78534f0bbbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 104,
            "second": 73
        },
        {
            "id": "21af3623-7cb8-45a7-a7b3-f3b0ea4c13fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 104,
            "second": 74
        },
        {
            "id": "d51e2699-ba8c-4a27-82db-621ffeef104f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 104,
            "second": 76
        },
        {
            "id": "b7da92c3-a096-4433-9ee2-ea94bfbfd98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 78
        },
        {
            "id": "de696161-1d5b-49fa-8889-7dcb4ee759ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 80
        },
        {
            "id": "fb22b173-37e5-4574-bf84-34b6ccfa7e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 104,
            "second": 83
        },
        {
            "id": "aff0288c-49b2-4fe4-aac1-00743006d19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 104,
            "second": 84
        },
        {
            "id": "90aababe-0660-45e2-b723-d4d85314cf45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 104,
            "second": 86
        },
        {
            "id": "eeb4729e-c675-4705-b239-ad4a6ce31c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 88
        },
        {
            "id": "e050faf0-5b20-4b28-a35c-2919d6aa51ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 104,
            "second": 89
        },
        {
            "id": "f759a99c-837e-4e82-86b9-b5f1e492ba05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 97
        },
        {
            "id": "6c5ae636-4736-4b39-be15-39a700733600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 104,
            "second": 104
        },
        {
            "id": "89ef49da-dc68-4761-8972-2c7a65b5fa4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 109
        },
        {
            "id": "0112e44a-1e01-4b8a-b86e-acac9d31bcea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 104,
            "second": 110
        },
        {
            "id": "f4213db7-1db6-4329-9d29-9d6d2503961b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 117
        },
        {
            "id": "1d7c8de4-b6d2-486c-a5e5-05892370f7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 104,
            "second": 118
        },
        {
            "id": "8d9d679c-19ec-4c95-8739-7e053814eb2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 104,
            "second": 119
        },
        {
            "id": "d5d54833-c937-4acd-bc3c-2c5ade3ceaac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 192
        },
        {
            "id": "85d101ba-4de6-46c2-a3dc-8b5b4635aa5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 193
        },
        {
            "id": "728746e3-b7ed-4457-8003-00489a9b823f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 104,
            "second": 194
        },
        {
            "id": "0cdbfdd6-00e6-4017-9d03-db188dbe7d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 195
        },
        {
            "id": "6d7d43e7-d96a-494b-b8ac-01a5e35d8924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 196
        },
        {
            "id": "690a7fe4-416c-4753-8228-e38e570a4e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 104,
            "second": 197
        },
        {
            "id": "72800c1c-8abb-4b6a-a5ac-7dc25c4cf713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 104,
            "second": 204
        },
        {
            "id": "3be8371e-de25-424e-bf0c-45631c8135d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 104,
            "second": 205
        },
        {
            "id": "74518150-5a3a-4f9c-9a99-63301eae8d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 104,
            "second": 206
        },
        {
            "id": "1b232c2b-1b09-447f-a03e-dbf63845d8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 207
        },
        {
            "id": "106d80e7-8a0e-4793-8619-803255f74d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 104,
            "second": 209
        },
        {
            "id": "957ebaaf-4127-4891-b5bf-6e9c9df5a5c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 104,
            "second": 221
        },
        {
            "id": "8699321a-3975-4c74-b430-ede186489a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 105,
            "second": 51
        },
        {
            "id": "6a06700e-18bb-4b3d-8837-ed74d2cf8c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 65
        },
        {
            "id": "9aade3ed-7dfb-4449-9b20-e4823bf0dabd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 66
        },
        {
            "id": "70e6fdf6-ac33-419e-979b-9f8fa7eeee1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 68
        },
        {
            "id": "6b3be22b-a36e-413e-8ce0-c986305b9869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 105,
            "second": 72
        },
        {
            "id": "798be52d-bb42-478a-847a-522018fb54fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 105,
            "second": 73
        },
        {
            "id": "396948e9-b1da-49f0-a3dc-b527471b340b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 105,
            "second": 74
        },
        {
            "id": "c58376d2-dc65-445b-9c03-986a537ad24b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 76
        },
        {
            "id": "1728a64b-6004-4cdd-a390-ef8c8e9937fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 78
        },
        {
            "id": "78e8a3bf-e415-4790-9a15-a8993b01fbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 80
        },
        {
            "id": "3cf0bbca-e1de-4053-a7a1-127114c57346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 84
        },
        {
            "id": "ae8cd72e-ae59-4690-861f-8c31c399b518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 105,
            "second": 86
        },
        {
            "id": "afe97545-1400-4917-8944-5e2d85595a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 105,
            "second": 88
        },
        {
            "id": "32896612-4a83-4d98-905c-128f1156f60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 105,
            "second": 89
        },
        {
            "id": "09d7cd0e-6b9e-4d79-b293-fca6e4efa52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 90
        },
        {
            "id": "be8f7fd2-f3c0-40e1-bbff-06b24c658f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 104
        },
        {
            "id": "426e2dc7-77fd-4a57-8377-b498a7b4c4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 105,
            "second": 118
        },
        {
            "id": "20015c72-9499-4607-bfb9-90618d674901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 119
        },
        {
            "id": "39684914-74c6-44a2-b1fb-5919183c7b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 120
        },
        {
            "id": "44523f3e-5607-4519-9327-a904670ab66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 192
        },
        {
            "id": "56801fd7-b61b-4502-b23a-f8c32f84f3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 193
        },
        {
            "id": "16eb2399-43d8-49f8-950b-97e56d43ab5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 194
        },
        {
            "id": "04e53f2b-4407-4b6a-9439-d1ed2f4195b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 195
        },
        {
            "id": "f135c2fc-e596-4025-8087-67449be08f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 196
        },
        {
            "id": "62f688ef-64c1-4a04-aa0c-f72bb1065604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 105,
            "second": 197
        },
        {
            "id": "744e2c89-baf3-47e5-92a5-0fc4afd63093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 105,
            "second": 204
        },
        {
            "id": "ce8cc131-1473-4e14-b7b8-ce302d3bd47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 205
        },
        {
            "id": "740c74f8-0fc0-4a76-8802-10e727a26018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 105,
            "second": 206
        },
        {
            "id": "7c9f31f4-5306-45da-8403-a0b15093d9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 207
        },
        {
            "id": "338876c3-10b6-455a-ac79-4a64dbf4b5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 105,
            "second": 209
        },
        {
            "id": "d0db4357-0186-49fd-aebb-e31780a9abd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 105,
            "second": 221
        },
        {
            "id": "b9522917-e41f-412d-a124-d4b4d8fa6c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 66
        },
        {
            "id": "070ebf69-1157-441c-b88b-1fbef1c23a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 106,
            "second": 68
        },
        {
            "id": "4aeb7b95-1e54-4959-835b-8a479a21e519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 72
        },
        {
            "id": "ef92c3e2-3d90-42fe-a2a3-1180b88e0ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 74
        },
        {
            "id": "a65f796a-2255-46d8-9b79-2ef653758c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 106,
            "second": 78
        },
        {
            "id": "6d593c1b-fb95-4c93-9c24-4c784927431f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 106,
            "second": 80
        },
        {
            "id": "8f4ceb3f-45bc-4495-858b-7cde11841fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 84
        },
        {
            "id": "22d26f92-ea3f-4a94-aae0-c9f1efd89a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 106,
            "second": 86
        },
        {
            "id": "aad819bc-5d54-45ca-a5b8-b6f37fb87d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 106,
            "second": 88
        },
        {
            "id": "67d81174-7907-457e-ab74-47038bdd780f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 106,
            "second": 89
        },
        {
            "id": "c3bc2bbd-69b4-4724-a12e-8fde3164a370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 192
        },
        {
            "id": "18eece3d-12b0-48d5-acd2-0b83acc1e97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 193
        },
        {
            "id": "70b49719-c7e5-4f94-bdc9-2ade342b62b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 195
        },
        {
            "id": "81762810-a04f-45d2-9645-aefadd98dc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 196
        },
        {
            "id": "3287efb9-7a2c-4142-9660-0b02f189c6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 106,
            "second": 204
        },
        {
            "id": "7de12762-dbda-4e70-9350-1a89fb425b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 205
        },
        {
            "id": "8ec86b63-da87-497d-997d-eca7cb34a7c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 206
        },
        {
            "id": "c4786d32-60f0-4057-b96c-868067e5e1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 106,
            "second": 207
        },
        {
            "id": "38cf5261-2d3a-40a1-b197-1fe1bfdc6ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 106,
            "second": 209
        },
        {
            "id": "d767134b-8ed9-4275-b9e2-d63e94370e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 106,
            "second": 221
        },
        {
            "id": "035ccbf1-8433-409e-a0bf-572067d45db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 85
        },
        {
            "id": "aecbe1d2-3a49-4243-a0f0-978960c0f5d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 97
        },
        {
            "id": "3f95be99-eeae-4378-ad78-006e7b811756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 99
        },
        {
            "id": "5c1f3de1-c8ed-4a11-b7d5-a5c0cf4a8a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 100
        },
        {
            "id": "dc29558f-f504-44ec-9fad-208308ebc414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 101
        },
        {
            "id": "1ea9e948-80fc-4fa5-844d-18ab80ffbe52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 103
        },
        {
            "id": "c2da2919-8e74-4ae0-aa43-443f1b232ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 111
        },
        {
            "id": "84a31655-469b-40b6-8f02-f1255bdee940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 113
        },
        {
            "id": "1ca42f02-dbd9-44dd-ac92-8c75b2ddc1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 115
        },
        {
            "id": "3a9c4b4d-2444-4000-b26c-68531c991fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 117
        },
        {
            "id": "ba85028e-95d9-4b1a-be3d-8688fbf2fc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 118
        },
        {
            "id": "051ff67d-2452-4e6b-a0e1-e5067f40a27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 119
        },
        {
            "id": "33e8ea47-a27f-436d-9d2d-c14ba98f20ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 120
        },
        {
            "id": "dde90e9b-b251-4a57-b171-d2296dd05530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 121
        },
        {
            "id": "0b78cd00-eebf-4967-8167-7d94b06c98d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 122
        },
        {
            "id": "d4012827-4113-42c2-baed-d1f3ab20f5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 217
        },
        {
            "id": "f862823b-c658-433a-ae81-6e9e36a4631b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 218
        },
        {
            "id": "ef38b916-eb3c-40c4-bfe6-04efb1655a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 219
        },
        {
            "id": "22cf79af-7bea-43da-b35f-da8cc9038ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 220
        },
        {
            "id": "7d176b95-e619-4006-9c98-47315f27f79e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 224
        },
        {
            "id": "02696989-89cb-4476-aab0-bfd5613e97a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 225
        },
        {
            "id": "3a8f3c7c-1cac-4585-8894-bd43fc8149b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 226
        },
        {
            "id": "180d7797-4915-4f38-ad52-f4af83df07b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 227
        },
        {
            "id": "4153e19a-cd7f-4e95-9048-c229fa6bee59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 228
        },
        {
            "id": "83ce0790-8125-4f8e-bafc-6066d11ad98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 229
        },
        {
            "id": "a25a6499-8950-4412-ba72-121308c4a608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 230
        },
        {
            "id": "853407e9-4c8f-473f-9403-f2a714679021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 231
        },
        {
            "id": "f345f920-17a3-42e6-b12a-d8d4efd6fa17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 232
        },
        {
            "id": "e0a6f148-a0d2-4035-a162-3a8c731d448a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 233
        },
        {
            "id": "d6ee72ce-d739-4311-b3e3-6a4cd9264a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 234
        },
        {
            "id": "ca7f5ace-813f-40d1-99e6-2b0965479a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 235
        },
        {
            "id": "56a62b0a-248a-42d0-bdc9-dc2134b2cc2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 242
        },
        {
            "id": "47216252-825c-47b4-afe4-bb7c548a5c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 243
        },
        {
            "id": "9053ebab-dd62-4577-b4fa-aa112cf955e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 244
        },
        {
            "id": "c901250e-ca05-4925-ac96-1b9ec34d88fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 245
        },
        {
            "id": "0cc846cf-fa29-45fa-afaa-e951dc6a4940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 246
        },
        {
            "id": "6b024b52-92df-4f18-aec7-0ab061c8b4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 248
        },
        {
            "id": "a1ac1b76-ede9-4307-ba04-f43a983b456e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 339
        },
        {
            "id": "8e15cf58-e08b-46e2-af7a-edeaa9b935eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 85
        },
        {
            "id": "04d20ed3-8180-4246-ac9f-d25a4e2d7ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 88
        },
        {
            "id": "cb2807e4-9eb6-49b3-9887-75d4748aec77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 97
        },
        {
            "id": "e4f3118a-5b10-4b48-9315-f479d212ba21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 100
        },
        {
            "id": "22d718ab-9afe-4219-ae84-7370faeab56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 101
        },
        {
            "id": "cba94834-bf20-438c-a4f0-de8cda35c0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 106
        },
        {
            "id": "38a2cb9c-a64f-4742-b263-6ec50d58585a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 111
        },
        {
            "id": "3e150c8f-23a6-4017-bdf7-417b4c544bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 112
        },
        {
            "id": "f20fbcdf-76f0-480d-9588-e84b99a38192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 118
        },
        {
            "id": "7ff1a2e8-42d1-4fad-83e8-b99ec44a270b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 119
        },
        {
            "id": "3eb7c475-7afe-40c1-a08c-eeadd97991c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 217
        },
        {
            "id": "e2a5568d-c7ea-4b4b-aa31-bf4717e63a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 218
        },
        {
            "id": "a31cd3ef-a3d9-43e5-8c1a-5f599850788b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 219
        },
        {
            "id": "5c0e110b-dbc4-4049-90e2-24fbc3276e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 220
        },
        {
            "id": "2d06a329-dc3b-4ca0-8fcc-996ad33004c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 232
        },
        {
            "id": "31bc4bbc-9032-425d-8a52-91642d69154e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 233
        },
        {
            "id": "fc8d4549-a405-4183-8750-c6db1950c25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 234
        },
        {
            "id": "cd40e9b3-1b60-4b00-8b91-f5c832baafa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 235
        },
        {
            "id": "3930d740-eece-4492-8ca8-353427ebcdc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 109,
            "second": 51
        },
        {
            "id": "83fa0a8f-f399-4497-ac9c-89fda99a3216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 57
        },
        {
            "id": "d83e64b5-026b-469b-9109-66e20738be86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 65
        },
        {
            "id": "fcd6b6d2-16e2-441f-8b91-e6f7e19ad048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 68
        },
        {
            "id": "1661bef5-f36e-4ee0-ae0b-8083ed902764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 109,
            "second": 72
        },
        {
            "id": "b69c20ec-cc48-4b2a-b45c-48f04aeeaf61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 109,
            "second": 73
        },
        {
            "id": "8019166e-c511-4ef2-99f9-133646399b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 109,
            "second": 74
        },
        {
            "id": "cd44ede9-df9f-41d1-bf11-6de533d04e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 76
        },
        {
            "id": "05084515-d809-454c-aea4-ca380633404d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 78
        },
        {
            "id": "ac733b94-023f-44d4-acb3-c272dff86487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 80
        },
        {
            "id": "a31388cc-b72f-430d-aaad-44218bb04fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 83
        },
        {
            "id": "c6597156-a379-4b5b-9c0a-69eb1ea2c6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 109,
            "second": 84
        },
        {
            "id": "b7ed38f3-00cf-496e-9d74-9097bd484f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 109,
            "second": 86
        },
        {
            "id": "ac4d960c-5fa6-4840-9f28-90a71d293982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 109,
            "second": 88
        },
        {
            "id": "b1eb1d0a-8068-48df-9969-83269244b1f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 109,
            "second": 89
        },
        {
            "id": "aaf36a6a-2d86-4027-a941-8b7adf223fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 104
        },
        {
            "id": "ab48a0ee-ed78-4566-bdde-c74f0d3dc6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 118
        },
        {
            "id": "92691c02-2da4-4557-bfb8-20c747b9264a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 119
        },
        {
            "id": "085bf59f-8958-4695-a253-29d48e161dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 192
        },
        {
            "id": "94da3e10-c28d-4a85-bcc8-01e0ad72e730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 193
        },
        {
            "id": "41a41c89-328f-4713-a0a3-57cbf92fd548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 194
        },
        {
            "id": "31fe0c94-090f-4866-90db-895201658053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 195
        },
        {
            "id": "ace886c9-bbb1-4330-adaa-df2e914439b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 196
        },
        {
            "id": "a8d2f180-4d31-4e99-aa41-ba9fe5dd3257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 197
        },
        {
            "id": "bc568693-53bd-4662-8c9a-5f092d723e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 109,
            "second": 204
        },
        {
            "id": "3b675142-42f6-45a5-b131-615012390187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 205
        },
        {
            "id": "e8bef2b6-718b-450b-89da-61f3806c3ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 109,
            "second": 206
        },
        {
            "id": "d7f6d13a-9954-4d36-9cad-c91c363d6b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 207
        },
        {
            "id": "f6ebae50-ef1d-4b11-b066-29a966b8aff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 209
        },
        {
            "id": "8aff57e7-8a3e-4111-a9e9-1c9f4140cf86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 109,
            "second": 221
        },
        {
            "id": "86c6fd78-12cb-4811-a0d7-3c27084d96de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 110,
            "second": 51
        },
        {
            "id": "355893c0-02dc-40e3-ad6c-d1141d3b9975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 65
        },
        {
            "id": "fda8c266-c848-43c9-addc-a71792fff547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 68
        },
        {
            "id": "5e7c4e34-59d9-4093-8036-246e684cc765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 72
        },
        {
            "id": "626692a4-e3cf-4392-bf87-c44123e6dbad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 73
        },
        {
            "id": "f2769265-8336-4537-90da-fdf20c4c96aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 110,
            "second": 74
        },
        {
            "id": "49f257da-4e59-4097-9926-f598da23ebce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 76
        },
        {
            "id": "bfc34c5d-2738-4357-abe0-2dfe603b1cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 78
        },
        {
            "id": "1760805d-3c0d-42e3-8201-718ab8ceb7c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 80
        },
        {
            "id": "6ff9988c-268e-4d35-9fdf-7c3c63d6190a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 83
        },
        {
            "id": "276b4232-b589-4d96-ab64-24e254106647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 110,
            "second": 84
        },
        {
            "id": "1e908881-4754-4f05-b580-70e3821680d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 110,
            "second": 86
        },
        {
            "id": "8dc386ce-d4de-4d9b-b162-9e909f7dde99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 110,
            "second": 88
        },
        {
            "id": "f070dd86-b7c6-44ef-89f5-ce3b6404dad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 110,
            "second": 89
        },
        {
            "id": "c9338788-a4df-4836-8ad1-dede51378f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 104
        },
        {
            "id": "ce7b2e80-d73d-466f-9995-4bb9bf4e35ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 118
        },
        {
            "id": "94f412cc-8663-4932-ba0f-40b1b31f460b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 119
        },
        {
            "id": "499ef728-45b1-4c73-84cd-29b92ae39e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 192
        },
        {
            "id": "41ab06c3-45c7-4414-b659-3702799a26b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 193
        },
        {
            "id": "10b57320-4641-4f43-9cea-6ff543dab31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 194
        },
        {
            "id": "894a77b8-60f0-4293-8419-fa9d015a24c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 195
        },
        {
            "id": "4e796a55-2627-4b74-adb8-eb072817331a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 196
        },
        {
            "id": "2f9e4dd8-6e23-46fb-b63a-a8a790b1e730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 197
        },
        {
            "id": "4cd7c356-35b9-439a-a4c0-775ca0d4e1bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 110,
            "second": 204
        },
        {
            "id": "b4a86a4a-1294-4cc9-bb3d-a3f133c1d550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 205
        },
        {
            "id": "a5628881-ccbc-4f9c-b415-bea5737d52e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 110,
            "second": 206
        },
        {
            "id": "1d36908e-cfeb-447d-bf62-233300a46517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 207
        },
        {
            "id": "b1d5f6f8-ccdf-461f-b6a5-1ab89e931fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 110,
            "second": 209
        },
        {
            "id": "d8bb49dc-7617-4e66-bf46-45880951510b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 110,
            "second": 221
        },
        {
            "id": "4f954431-63b4-4b4a-be9a-e7d650625eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 111,
            "second": 51
        },
        {
            "id": "f3c98f5f-25dc-4862-8bdd-17205acda31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 66
        },
        {
            "id": "0632d57b-d18e-4119-903d-7256a82b4966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 68
        },
        {
            "id": "d2a0190b-2882-4522-bd51-f6ce948f34f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 72
        },
        {
            "id": "f7b1cf61-7e38-4a1f-ac31-933ebfe7d976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 111,
            "second": 74
        },
        {
            "id": "15f3c9da-e741-4026-be24-a947fdac94c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 78
        },
        {
            "id": "62134011-328c-4537-a346-b3466191c6c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 80
        },
        {
            "id": "53bab568-bbe2-4f9a-8b20-4a07b56ebefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 83
        },
        {
            "id": "4ffed93c-18eb-4085-9b6c-d99c425a4ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 111,
            "second": 84
        },
        {
            "id": "2ddd59dd-c5c9-418d-a9dc-d72b6a6f5365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 111,
            "second": 86
        },
        {
            "id": "4ee17cc7-91a1-40b4-9981-e030ab5dbae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 88
        },
        {
            "id": "a68c83ef-e2d1-4d90-9b06-0e7cd45fa427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 111,
            "second": 89
        },
        {
            "id": "dc0f8093-b641-42bc-9aec-a0c7f909a0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 112
        },
        {
            "id": "856773c8-c31b-4de9-938c-158515863143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 118
        },
        {
            "id": "58d301ca-cb78-4ca1-9043-8695e063568e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 119
        },
        {
            "id": "c03746c5-cc37-442d-867a-4959c4c656e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 120
        },
        {
            "id": "81b60a50-6e2b-4139-8f49-fe9d8cc3bf3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 192
        },
        {
            "id": "da574077-5d3a-4238-99c4-a1212884c924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 193
        },
        {
            "id": "0ea3d2e4-ab45-4295-83da-0cf33d04736b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 195
        },
        {
            "id": "8d45b4ff-6350-4bd1-87ab-e0e1b64c353c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 196
        },
        {
            "id": "fa9a51df-a2a9-4640-93df-f9c2ab1037a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 111,
            "second": 204
        },
        {
            "id": "d78b1eb9-b3ed-4c8c-8c4b-9cf6d702b0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 205
        },
        {
            "id": "ea24274a-d072-4c95-abb8-7a5a4d1510fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 206
        },
        {
            "id": "c59ab44e-9523-4d44-9a46-ca76d4da2f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 207
        },
        {
            "id": "86689ff3-367d-461f-ba52-2b89428064f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 209
        },
        {
            "id": "481fc0fd-e771-4b98-99d6-086003db5f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 111,
            "second": 221
        },
        {
            "id": "bd8929ef-3b15-4c95-914b-02b62eee3c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 112,
            "second": 51
        },
        {
            "id": "b3b56497-d8ac-44db-8067-549f77fb0c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 65
        },
        {
            "id": "262b5e37-827c-4aa7-b8fe-b5354863266f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 66
        },
        {
            "id": "8d1e81f1-51ff-48c3-b69e-8c36888462a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 68
        },
        {
            "id": "306ae23c-da0b-46c2-8c72-e5f9b8e3b657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 72
        },
        {
            "id": "8b9bcd2a-b356-403c-9b4c-6deb366052ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 73
        },
        {
            "id": "410c2a25-3d79-41ad-81e5-1c2cfcadec64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 112,
            "second": 74
        },
        {
            "id": "ecd1f591-4d7b-4441-8c07-c9dd4277a823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 76
        },
        {
            "id": "f4e5932f-12fd-4422-99c2-a747d651beed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 78
        },
        {
            "id": "f7a3616c-ea89-4bd0-ad50-36bca019d908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 80
        },
        {
            "id": "9697e092-796d-419e-a9e3-4fe05d3cf567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 83
        },
        {
            "id": "6dd9abfe-1b1a-468d-a39e-4293ca8d7ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 112,
            "second": 84
        },
        {
            "id": "dcec7ee0-a442-4fde-a9d1-ac44854828f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -16,
            "first": 112,
            "second": 86
        },
        {
            "id": "309a84a0-1417-4f51-b44a-1494473dcb9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 88
        },
        {
            "id": "2b469af1-d19b-4506-a39f-8f20f628adaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 112,
            "second": 89
        },
        {
            "id": "205a4e39-197f-490b-93a2-e80282af3c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 104
        },
        {
            "id": "4be06531-c9a1-45ae-9da9-becf6be71d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 118
        },
        {
            "id": "cfd9b726-4b0c-4e9a-93ac-2f8f2c1cb73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 119
        },
        {
            "id": "4e132391-d232-471e-a8d4-bd8183ad1f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 192
        },
        {
            "id": "8bee32bd-1465-46ac-918a-9e3c8dc550ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 193
        },
        {
            "id": "1b0c2eeb-7898-4707-bdc9-a47164446822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 194
        },
        {
            "id": "029eb256-be0d-45ad-956a-39f3d5de5dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 195
        },
        {
            "id": "884f3cac-5f74-41ae-a4dd-c78ac29597cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 196
        },
        {
            "id": "47922c42-acff-42f5-8675-031f23534fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 197
        },
        {
            "id": "af14ae39-03a9-402e-8408-66052c19d6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 112,
            "second": 204
        },
        {
            "id": "ac1ca24e-bd63-4161-b470-672212cf6e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 205
        },
        {
            "id": "93a490c1-5881-46b5-8418-3045ea3d87b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 206
        },
        {
            "id": "45495141-26ee-42a6-9108-d58a61bd6aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 207
        },
        {
            "id": "05601020-7ac5-4e54-b09b-2f4b4d2c135d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 209
        },
        {
            "id": "7672607e-c38a-4d1d-938a-63bca7d21424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 112,
            "second": 221
        },
        {
            "id": "9909fbae-fd26-4a42-b0c3-af5adc2899a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 113,
            "second": 51
        },
        {
            "id": "2295d0b7-ce7b-408a-bf2b-973d7070bb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 65
        },
        {
            "id": "224382e6-501b-4b47-ac02-69f14954d01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 66
        },
        {
            "id": "663d20f9-fc87-4873-9332-3f391ff8cc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 68
        },
        {
            "id": "16186d87-982f-498a-81f8-9371a6c1cec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 72
        },
        {
            "id": "ffc804ff-0c7a-47d6-a511-fadf0620bbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 73
        },
        {
            "id": "7809b629-4145-4527-8207-8de1aa120bc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 113,
            "second": 74
        },
        {
            "id": "c06f514f-c109-442f-a511-a3c23ae8f325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 76
        },
        {
            "id": "b6e434b4-2549-45a5-bfa4-22049c3fb462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 78
        },
        {
            "id": "ed64a734-890e-4d2b-a3a3-db0edf56311a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 80
        },
        {
            "id": "f2b93e96-3a95-4a76-ab2f-cda3c74a783c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -20,
            "first": 113,
            "second": 84
        },
        {
            "id": "429f78cf-3a98-4615-823a-e0426bbcc21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 113,
            "second": 86
        },
        {
            "id": "628b6e3e-2dad-470b-9ece-e251caa2c2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 88
        },
        {
            "id": "f7a473bd-73db-40a0-b804-3f93f2c67403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 113,
            "second": 89
        },
        {
            "id": "84c2907e-5c94-4f0a-a28b-57d0b6d1b8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 104
        },
        {
            "id": "0cc74d91-9c1c-4b09-8e5d-975594735ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 113,
            "second": 118
        },
        {
            "id": "a41cc55e-fc63-45c9-bde5-229e1a7acc98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 119
        },
        {
            "id": "6c01740f-4f15-41b6-bbf1-9a52b881f212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 192
        },
        {
            "id": "f8aef884-5454-4d22-9518-72f19c5474f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 193
        },
        {
            "id": "57318db6-817f-4d0c-aac3-9471a7495119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 194
        },
        {
            "id": "a61b6e58-4786-43a6-8094-e0702b20b692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 195
        },
        {
            "id": "66b641ba-8c79-4907-9e4a-a2b5a93ee17d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 196
        },
        {
            "id": "e99b009d-9d11-455d-8c89-0993d3112674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 197
        },
        {
            "id": "6e5264af-5d64-4efa-a276-e72016502be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 113,
            "second": 204
        },
        {
            "id": "786c630c-49aa-401b-928b-9df9f00e1836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 113,
            "second": 205
        },
        {
            "id": "2003c871-c5ac-475b-afdb-0784d5028e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 206
        },
        {
            "id": "aa641586-6df9-4941-886f-d283f637751f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 207
        },
        {
            "id": "f43b543c-b731-459f-bc2b-da465293cb91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 209
        },
        {
            "id": "cd1d1c40-565a-40cd-8049-4be2d7dac12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 113,
            "second": 221
        },
        {
            "id": "726e2c33-4003-4b58-b24d-528f6811d1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 114,
            "second": 51
        },
        {
            "id": "cb8794ce-52a3-4ff6-9af9-0e97a4a0a103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 66
        },
        {
            "id": "363052b9-5af0-42d4-8c4c-4d0798eefa94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 68
        },
        {
            "id": "787db13d-024f-448d-9f9d-7a80ba92a8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 72
        },
        {
            "id": "3c1e15c8-3863-41bd-8921-a8603f06cee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 114,
            "second": 74
        },
        {
            "id": "299680e1-be14-4480-87a4-26cdc23db702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 78
        },
        {
            "id": "b13440ee-1178-490f-b797-d77e840f1a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 80
        },
        {
            "id": "08c0d947-2729-4ffe-9b69-3a8f092718e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 114,
            "second": 84
        },
        {
            "id": "5a00b86a-4342-416f-bed1-77abd394becb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 114,
            "second": 86
        },
        {
            "id": "6a83b596-6327-4283-b5a4-c1e20701f9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 114,
            "second": 88
        },
        {
            "id": "b46329d5-534f-4a2f-9c3a-ddb86392d10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 114,
            "second": 89
        },
        {
            "id": "a82fc4b4-d7b3-4f20-9ac8-ae575ef9681d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 90
        },
        {
            "id": "0b62cf2f-cd37-4262-aed0-7270bb4f754d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 97
        },
        {
            "id": "3443be4d-9acb-430a-aedd-db1d83d6c6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 100
        },
        {
            "id": "014f0166-db1e-49c0-8adb-1b4f5abc9435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 101
        },
        {
            "id": "ca2f9c52-84a7-4559-88ac-721981c06928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 111
        },
        {
            "id": "455fbe99-5e42-41b7-806b-bcba87d63688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 192
        },
        {
            "id": "bd0b7223-1b34-4ca4-b2c6-017d8568c915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 193
        },
        {
            "id": "2379f2d6-63e5-4d0d-bf13-cd221b148f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 195
        },
        {
            "id": "7c7d3840-4338-4e22-911e-a3f635d8967c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 196
        },
        {
            "id": "7360829a-0c82-490f-a213-c8ea5127b867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 204
        },
        {
            "id": "c719826c-33f0-4522-928a-3cf2d9826498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 205
        },
        {
            "id": "bade471d-d432-420c-8ea9-640f02a2b6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 207
        },
        {
            "id": "1c1598ad-e1c2-4d37-8e98-8969c53785ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 209
        },
        {
            "id": "f8f7718f-fc2a-4dc7-a448-3156b4c29651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 114,
            "second": 221
        },
        {
            "id": "9eeb11ae-8a1a-4139-b8bc-da60d01b536b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 224
        },
        {
            "id": "03faf86f-05d4-4b56-95e6-0708d3923670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 225
        },
        {
            "id": "ff43bf8f-aaaf-4cc2-ac8e-f46b21e5de1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 226
        },
        {
            "id": "57e8fabd-25aa-43e8-b7e5-2f8408c48221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 227
        },
        {
            "id": "f3ea13a9-8a3b-4382-a513-f44e3eda7085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 228
        },
        {
            "id": "940c4615-e81b-46c4-9047-400b6cf96792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 229
        },
        {
            "id": "cf4b0a48-ed99-46d8-84d9-63353805bb11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 230
        },
        {
            "id": "d2277a2e-b824-41b1-821c-e8886a4e692b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 232
        },
        {
            "id": "2414555c-4f6d-4292-8378-0f5851876195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 233
        },
        {
            "id": "bf14acd0-6f49-48a3-9c03-d5c367cc9077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 234
        },
        {
            "id": "a35251bc-be31-4459-ada5-9a3e0c931a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 235
        },
        {
            "id": "4ea8c35f-6bce-43ff-a0e3-7083f10c88b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 240
        },
        {
            "id": "9f70f30c-b476-4ffa-bd8f-bbd596efa9fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 242
        },
        {
            "id": "a3b54a13-fea5-4ea6-ae2a-7e57b685b2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 243
        },
        {
            "id": "b502f69c-445c-4382-84e8-12dce4e4bc30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 244
        },
        {
            "id": "93c00622-e124-4128-a5c2-f4ad50bcea4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 245
        },
        {
            "id": "734132fb-b800-48fc-a4b4-dfe544e152cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 246
        },
        {
            "id": "f5547070-2cbc-4ca6-bcdf-521affc16402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 248
        },
        {
            "id": "a780ff7b-28e7-46f9-a53a-1fcd8539b96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 339
        },
        {
            "id": "719ceffa-e2dd-41a1-a17e-24ce1b7cfa14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 115,
            "second": 51
        },
        {
            "id": "fd630a6b-69c5-49c3-b397-5d1317eaaa92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 66
        },
        {
            "id": "abbc17d8-dbd5-41c3-a15f-328161f02406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 68
        },
        {
            "id": "4a0e56c9-0f9c-4d8a-9445-959fade0cfbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 72
        },
        {
            "id": "0aae1064-bf54-47cb-8d3f-0d15bca650d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 115,
            "second": 74
        },
        {
            "id": "570d9ea6-d755-43a1-b8e6-642d2f2a4ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 78
        },
        {
            "id": "449403d5-9a19-4bb7-8c37-64574a2be70a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 80
        },
        {
            "id": "f07b1924-738b-45d2-8ba4-90a84cdbe020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 115,
            "second": 84
        },
        {
            "id": "e8a9407d-29f7-49dd-b04c-b09274d3f9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 115,
            "second": 86
        },
        {
            "id": "b1fc362a-26cb-437b-9e93-4d0b6863e767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 115,
            "second": 88
        },
        {
            "id": "b8c6811e-b782-486e-9cb6-d77f852cf156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 115,
            "second": 89
        },
        {
            "id": "eeb63136-f282-4ba9-bb95-03100fe043b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 106
        },
        {
            "id": "ef8df55d-a7c9-445f-969d-d7d32cf7dbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 115
        },
        {
            "id": "0f94a8aa-4aff-4954-a97b-a2ccb2971902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 115,
            "second": 118
        },
        {
            "id": "f8ebd62a-cfd5-4862-abcb-aea4f29271f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 119
        },
        {
            "id": "122093ea-05d9-4502-8774-8927344049fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "46c73d46-4710-4398-adf8-c598fffefbc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 192
        },
        {
            "id": "97170d31-3e26-45bf-b8a6-1e7367e9b6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 193
        },
        {
            "id": "e122df76-4f8f-433d-9d5c-535f12384dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 195
        },
        {
            "id": "3d058b52-6439-439e-abc4-7f94909f35b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 196
        },
        {
            "id": "b8d923f4-b527-47f3-8b9e-d0b2fb885bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 115,
            "second": 204
        },
        {
            "id": "78eb0245-bbc1-4e74-83a1-5eb13c1dc4b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 115,
            "second": 205
        },
        {
            "id": "23f08b67-608d-4f40-a8f9-0ee1e3cbedab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 115,
            "second": 207
        },
        {
            "id": "1184bc02-0bab-4228-9522-a8ac0a9ff4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 209
        },
        {
            "id": "c9dde3b1-7405-4a6d-abb4-a89ce68b0014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 115,
            "second": 221
        },
        {
            "id": "9567ffe8-bcc5-44d0-b835-efa92d59baa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 116,
            "second": 51
        },
        {
            "id": "f72f8cae-0573-4506-ad5f-f1e5cddd08ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 66
        },
        {
            "id": "0329abba-ab35-4ccf-a500-c181c56fb3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 68
        },
        {
            "id": "51065401-7aab-46e5-94a5-d7fb550645c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 72
        },
        {
            "id": "07d6e67e-e160-4a2a-ac0b-6edc83d538e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 74
        },
        {
            "id": "dc6a4eb1-af8b-496d-baf6-b0cbe6b4dd7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 78
        },
        {
            "id": "b935ce4d-54de-4330-aa79-afbe58b193a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 80
        },
        {
            "id": "a0c1f1b3-b6a4-4f84-b5f7-cc8149e0cd91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 84
        },
        {
            "id": "98c84141-24c8-4e0e-abf9-5c308e3ecdec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 116,
            "second": 86
        },
        {
            "id": "3e43ae23-ecdd-4ea1-acb1-c8c89d1d9fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 88
        },
        {
            "id": "26a6a277-435a-4d8e-91b4-4202ed52f920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 116,
            "second": 89
        },
        {
            "id": "20c4b21f-b4aa-4163-9139-4357480ff02b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 90
        },
        {
            "id": "99dd06bc-5e3a-4e1d-bc4f-a1e4d27051b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 97
        },
        {
            "id": "2278b0d9-4cd3-4daf-b9ef-835feaab56b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 100
        },
        {
            "id": "e159b743-d16a-4bda-8be2-8d9a74c642e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 101
        },
        {
            "id": "341a5b60-2181-4828-93d4-e9cf05c86c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 111
        },
        {
            "id": "1aca8351-e9cc-4593-b3a7-73d458569241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 192
        },
        {
            "id": "2ad7c9f7-f731-4142-94b0-411839e94c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 193
        },
        {
            "id": "d1716e7e-686c-4e41-9753-282abb9c3cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 195
        },
        {
            "id": "ec3840c2-3780-4898-a331-4d9ccc4ba3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 196
        },
        {
            "id": "e4d95202-63e0-4d3a-b5d1-60b545eb7780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 204
        },
        {
            "id": "b29662c6-c37d-4b2a-84bc-04f6343caa4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 205
        },
        {
            "id": "97a671cf-ccbc-4242-a347-8142ba49b86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 207
        },
        {
            "id": "416e4248-2357-49ab-993e-7208bcd11777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 209
        },
        {
            "id": "9c8eedd8-9d4b-4bd7-9c73-dd6e11d3c1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 116,
            "second": 221
        },
        {
            "id": "0af7d648-fee4-4910-8f24-5746fca954a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 224
        },
        {
            "id": "4451afdc-6361-4330-8d9f-8f15c312fb84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 225
        },
        {
            "id": "fd5a011e-bd21-4668-905d-0f993d28b6eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 226
        },
        {
            "id": "9159dda6-9b04-40c8-85c5-0271d5abdeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 227
        },
        {
            "id": "0af4582e-1197-4a55-b5dc-150c9052eb6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 228
        },
        {
            "id": "3bc48e7e-6328-4df9-a719-52157effbe71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 229
        },
        {
            "id": "d8acca7f-0b03-4bc4-bb95-17d6b9363c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 230
        },
        {
            "id": "f4ae233c-3619-482b-9da5-50909b20a707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 232
        },
        {
            "id": "77b8b10b-bb8f-43a9-9f90-8aaa3190bfec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 233
        },
        {
            "id": "d150a55d-5c00-4607-8141-d01e9e4c3ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 234
        },
        {
            "id": "f11b0340-34ac-4864-80c3-c30f2af889e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 235
        },
        {
            "id": "8a17728f-24ba-4737-b64a-4d16daf5940d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 240
        },
        {
            "id": "5a5640d2-75de-4a74-a5f4-b44b30722eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 242
        },
        {
            "id": "6b9d7127-48ba-4082-a52f-3768f8888c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 243
        },
        {
            "id": "ff810bc2-e606-4dc9-8599-9e670eded899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 244
        },
        {
            "id": "a32e6323-6c76-4512-9c34-e1f783f5d878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 245
        },
        {
            "id": "be5966ea-d642-4ef4-9822-8fae10a54263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 246
        },
        {
            "id": "35b96a60-9947-4084-aa16-9365a6689f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 248
        },
        {
            "id": "ea6f412f-94ea-49ad-801d-b1dc296a59b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 339
        },
        {
            "id": "56a23fbe-1dbd-4fe4-a4ec-f55b1af3bce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 117,
            "second": 51
        },
        {
            "id": "a0ad0678-dca1-4f16-a8e2-a7ae3cad91a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 72
        },
        {
            "id": "3868806e-942a-4e50-b1ea-0b6199d5b07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 117,
            "second": 74
        },
        {
            "id": "2cbf2b3e-f17b-4005-b92d-d9e0c0098f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 78
        },
        {
            "id": "1301c520-4d5a-4bf8-8873-600e4faea29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 80
        },
        {
            "id": "d8edc2e0-e484-4b33-878f-9841da9c739a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 117,
            "second": 83
        },
        {
            "id": "3b12ff7c-dab5-4ec9-9c43-da166df96203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 117,
            "second": 84
        },
        {
            "id": "633c253b-a044-4404-a953-859939aa43e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 117,
            "second": 86
        },
        {
            "id": "aff7fa8e-567e-4b4c-ab1b-70c5a9ab4489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 117,
            "second": 88
        },
        {
            "id": "59f9810b-8348-4a4d-af62-ad256a6d4ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 117,
            "second": 89
        },
        {
            "id": "7383672b-d0da-4a62-b7a3-d63e90242ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 117,
            "second": 118
        },
        {
            "id": "2320f68f-668f-44cc-bbaf-860cd4781abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 119
        },
        {
            "id": "fd6a743c-f1d7-4149-8547-4bae81da6fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 192
        },
        {
            "id": "0e24d07f-8a2c-49c5-bb38-6b4f9ff3cf5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 193
        },
        {
            "id": "b96d74f9-a416-462d-8726-5f4d77c10ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 195
        },
        {
            "id": "a5e869da-fd44-4d9b-8051-707e379f692e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 196
        },
        {
            "id": "25f2117e-9a2d-49ca-9aa9-a8a7fca36afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 117,
            "second": 204
        },
        {
            "id": "40abaf98-32dc-42e6-af2d-d03766e977a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 205
        },
        {
            "id": "832d72b6-3793-4dee-a392-a140342e9ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 207
        },
        {
            "id": "52b34f05-a239-4f78-91bd-66c042f83bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 209
        },
        {
            "id": "ab57c6bc-2245-42dc-93fc-f7c601f0c8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 117,
            "second": 221
        },
        {
            "id": "046d80f0-5400-4bde-978f-2f4b637f07f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 51
        },
        {
            "id": "e87b6c48-28c1-48bc-945f-e1c637ca1ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 118,
            "second": 55
        },
        {
            "id": "d2b6ea36-c152-48d5-a0a0-23797df19f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 66
        },
        {
            "id": "94aaf653-04b6-420a-aeb6-0c6aa9eeef4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 68
        },
        {
            "id": "2f0e6bc3-46ea-4db7-94ee-2cecb9af1b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 72
        },
        {
            "id": "d7d36da0-ec39-48b6-bbd9-1eb332e7905b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 118,
            "second": 74
        },
        {
            "id": "6570b474-56e1-4ffc-b9f9-007267837b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 78
        },
        {
            "id": "c6c513a7-c26f-4ebf-9931-e276a5211b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 80
        },
        {
            "id": "3365babd-d04d-4686-b193-a42864f1e0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 118,
            "second": 84
        },
        {
            "id": "2c68005b-2eff-4f83-bd28-8c7953c5b594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 118,
            "second": 86
        },
        {
            "id": "a40ce000-aabb-48be-86af-af8a1f068115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 88
        },
        {
            "id": "236130d5-dbee-4c7f-a44e-2da35c98d627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 118,
            "second": 89
        },
        {
            "id": "cd70f1e7-8600-492d-8f1a-5f5c6c3621cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 192
        },
        {
            "id": "bebae0cc-89f3-424c-8672-5d236d98964c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 193
        },
        {
            "id": "f43f52be-9317-4a66-858d-ffe69f5b8dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 195
        },
        {
            "id": "cfca142c-b099-474f-ba31-22fa7dbbdbf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 196
        },
        {
            "id": "fe96600e-ca6b-4bee-8bed-cc9e075c6c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 204
        },
        {
            "id": "7648948e-d3c8-480d-81ce-4c7f2fad960a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 205
        },
        {
            "id": "5e353640-4356-41de-a75c-b7ae05cdc508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 206
        },
        {
            "id": "99f2b137-89dc-4168-a970-049e2e11eec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 207
        },
        {
            "id": "7911f9e3-8e80-44e6-a2fa-71f2529fa7fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 209
        },
        {
            "id": "88468116-199c-4163-9971-2926ed2734d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 221
        },
        {
            "id": "e5a5a272-6ac9-444e-b762-c1b592a9889f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 41
        },
        {
            "id": "a280bd18-3c01-4d28-ba31-5f6315970897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 119,
            "second": 51
        },
        {
            "id": "c42ad851-de46-490b-bb1d-5f7dc6a0f7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 55
        },
        {
            "id": "706ad9ae-2d20-4b1c-b15b-c8d5c2edc6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 65
        },
        {
            "id": "e82fbc29-8398-4608-a6d3-1653b9438535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 66
        },
        {
            "id": "649cedbf-cfdf-4ec1-9cdc-1378257ab23c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 68
        },
        {
            "id": "9fb78857-b641-407e-9fb0-5a276c0aee43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 72
        },
        {
            "id": "83aae698-23a6-4634-b35b-833971a01189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 73
        },
        {
            "id": "e24cb67b-a1cd-4908-bfbb-61e909e06b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -21,
            "first": 119,
            "second": 74
        },
        {
            "id": "c0d58ebc-f125-4fb1-8836-8ef5f75ce5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 76
        },
        {
            "id": "65b49f86-6844-4d7e-b07c-cf1b86b3b8fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 78
        },
        {
            "id": "2c88f2d7-892f-4f41-b740-cea0cbd46e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 80
        },
        {
            "id": "e9c4e7f2-778e-4e2f-89ab-c04750210c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -19,
            "first": 119,
            "second": 84
        },
        {
            "id": "a04475eb-d70a-4749-9be6-c1378b1e4fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 119,
            "second": 86
        },
        {
            "id": "8c8fd76b-4008-4a33-9d09-047d1be94005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 88
        },
        {
            "id": "edb1c4f2-996a-472d-9161-a5d4d7b02976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 119,
            "second": 89
        },
        {
            "id": "521cc046-ce92-4f89-895a-7a7ea1e2e94c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 192
        },
        {
            "id": "c4735e41-9ca9-48b5-a03a-08f49bca05f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 193
        },
        {
            "id": "604d2105-14c1-4640-8d3a-8184fb7dac76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 194
        },
        {
            "id": "5b1881b5-d321-4c76-b72e-414049e976d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 195
        },
        {
            "id": "745d528e-7533-4d3b-8607-246254ccdf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 196
        },
        {
            "id": "8566f877-dae0-41cb-9635-d5b8caac1c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 197
        },
        {
            "id": "a54df93f-6053-4e4d-a8ed-f18b981a60eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 119,
            "second": 204
        },
        {
            "id": "2f5dad0a-bc74-4519-b9fe-2dc9ff9a2700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 205
        },
        {
            "id": "548e7645-8370-4870-9079-29425e6fee52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 206
        },
        {
            "id": "7d57ef2d-9ce0-4c7d-840a-a0905fec21b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 207
        },
        {
            "id": "ad75d8cb-ce6b-4df8-a866-02dc30f9a018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 119,
            "second": 209
        },
        {
            "id": "1c109065-ed4f-4c5b-8fc9-0878a5f4b59d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 119,
            "second": 221
        },
        {
            "id": "cab020b2-baf6-480f-ae77-28015685219a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 120,
            "second": 51
        },
        {
            "id": "8575e371-9ef1-4781-8a85-86c9946be87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 55
        },
        {
            "id": "8dcb5b3c-c207-4b6e-b7f3-73c8df499e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 66
        },
        {
            "id": "e82ca871-03bc-49e0-91bf-4957f4a2d863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 120,
            "second": 74
        },
        {
            "id": "14c66dc9-df19-48f1-9cd0-56b3f598d1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 78
        },
        {
            "id": "682b5a5d-5adf-46d0-97d1-8c9bea8d83e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 80
        },
        {
            "id": "6d45afc0-1c23-4fed-af8e-0df64266a7ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 82
        },
        {
            "id": "e7a88ca1-0989-4279-a503-898012b3d63a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 120,
            "second": 84
        },
        {
            "id": "08264a12-5d50-468d-b2b3-f4e8ea4a7140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 85
        },
        {
            "id": "0c9532dc-77d9-4fd2-b5d3-bab30d8c551b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 86
        },
        {
            "id": "3269931c-8cff-4598-826c-cf8c2e415a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 120,
            "second": 88
        },
        {
            "id": "caf92aca-feb9-4817-84b2-5d5874a0c2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 120,
            "second": 89
        },
        {
            "id": "75f54c1a-7207-420b-ba30-b819975a8530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 90
        },
        {
            "id": "f8803991-9b05-43d4-a559-fa16fef40696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 97
        },
        {
            "id": "6e589db6-78eb-4f3f-a3f9-8012128ff15c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 98
        },
        {
            "id": "c6e8ed21-d294-4966-b3ea-a381b72e8dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 99
        },
        {
            "id": "cdfefecc-1fa3-42e0-a59c-4d80960793d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 100
        },
        {
            "id": "f6309972-9bfb-4988-a582-148b08a6b666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 101
        },
        {
            "id": "a1525b2c-e047-43b9-8cff-218ba0f038f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 120,
            "second": 103
        },
        {
            "id": "ee026a33-7950-42db-8ea5-7f4cdd728d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 111
        },
        {
            "id": "3db93990-6f89-4731-a559-e4b872f870c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 120,
            "second": 112
        },
        {
            "id": "9e9499f2-b974-4a21-bece-7778d169d1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 113
        },
        {
            "id": "2ac66515-0441-48df-b688-8a01243f111e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 115
        },
        {
            "id": "23323a31-f62f-434f-8e00-565d2a6e2176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 120
        },
        {
            "id": "42539fec-fc07-4e45-b518-5926f8a728ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 122
        },
        {
            "id": "182aae9b-8dfa-4050-a17d-74c8ef48eb98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 204
        },
        {
            "id": "06812d41-8149-4b8d-8b00-79b7e8ac0e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 209
        },
        {
            "id": "673fbd3c-f6d3-4f96-9d9d-c73be0548eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 217
        },
        {
            "id": "c034b348-e097-40bb-81f6-a8cef75fc34f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 218
        },
        {
            "id": "3a05b664-6905-4fca-8d2f-10ecb99ff30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 219
        },
        {
            "id": "a90645dd-bb2b-4838-b72d-951451680886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 220
        },
        {
            "id": "cd9a5d9a-023e-46e8-9690-b6731ab9a169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 120,
            "second": 221
        },
        {
            "id": "5924a2aa-5c5a-4528-84b6-8d5b4cfa872d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 224
        },
        {
            "id": "1c54b439-871e-43c9-b2ae-46bcad20a2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 225
        },
        {
            "id": "c769e2f8-01bc-4c0a-8d55-6a466b4c8bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 226
        },
        {
            "id": "68338946-9431-48e7-9beb-19d6b01402bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 227
        },
        {
            "id": "b8fa119d-e810-42a2-97d7-f58958ef84c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 228
        },
        {
            "id": "93b1feeb-fc6c-4221-a0c7-e6fd55c54d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 229
        },
        {
            "id": "44b918d0-83d6-430e-bd28-5f364cbdd40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 120,
            "second": 230
        },
        {
            "id": "025d26b5-e539-4e66-a12b-5f85a9b64ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 231
        },
        {
            "id": "776ed938-ca1c-4a2b-b2da-59f2391a8406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 120,
            "second": 232
        },
        {
            "id": "106c21c8-1a6b-44b2-ae0f-cdd293566efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 120,
            "second": 233
        },
        {
            "id": "02e2f2d8-d89d-4949-9ea9-18429d0fff83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 120,
            "second": 234
        },
        {
            "id": "ac8b3735-c805-43d5-b135-0fcac7e53dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 120,
            "second": 235
        },
        {
            "id": "c1ea8910-085a-48a3-a63b-50698b183d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 236
        },
        {
            "id": "d340882c-4080-40bc-8fdc-642b17c344f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 237
        },
        {
            "id": "514f5c49-ca38-4394-a7b3-b3be8b4d55d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 238
        },
        {
            "id": "706000f8-6a36-422a-88d7-dafe0a344b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 239
        },
        {
            "id": "232e4b5e-23b8-451e-80bf-1e2c4fb9a7f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 240
        },
        {
            "id": "ceef0b50-e58d-43d1-90f8-28561d29a91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 242
        },
        {
            "id": "9d2ca13b-38ea-4768-ad72-32e07844029f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 243
        },
        {
            "id": "4b6ce626-192d-406c-a308-8646237b15fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 244
        },
        {
            "id": "ee7f559c-9335-4ec1-a2d3-8ddd3770f046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 245
        },
        {
            "id": "1fc93057-aa35-48fb-a5b9-21f65d205dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 246
        },
        {
            "id": "c65af2fe-b15b-449e-8baf-07bf5aee8d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 248
        },
        {
            "id": "29521da5-d426-4ac8-a184-bb71571f5816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 339
        },
        {
            "id": "416536cb-dc10-4102-a414-13149d96d366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 121,
            "second": 51
        },
        {
            "id": "0f31442e-e90a-490b-a41b-3a634de225c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 66
        },
        {
            "id": "61669907-f4d0-41d3-8f1d-b3fdd309c408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 68
        },
        {
            "id": "ae321cbd-ccaf-4e8c-969b-57989514a033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 72
        },
        {
            "id": "d449a2f8-c7c2-493a-939d-ed12e02c9dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -22,
            "first": 121,
            "second": 74
        },
        {
            "id": "3ce2456a-3423-4e74-933c-7c5d8a3072bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 78
        },
        {
            "id": "9e90bd22-9db4-4ece-9a59-043eac46957e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 80
        },
        {
            "id": "70cc6595-0ecb-4cfd-bbbd-d81e5ddbb6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 121,
            "second": 84
        },
        {
            "id": "0feaca5a-726f-4c06-9627-eaf1be15128b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 121,
            "second": 86
        },
        {
            "id": "c68432a1-1659-4d4d-b05e-f72e4ddec34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 121,
            "second": 88
        },
        {
            "id": "797221ee-b4e7-4829-8921-ddab617363ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 121,
            "second": 89
        },
        {
            "id": "49d47d60-a8c2-4613-8f6d-598178061029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 192
        },
        {
            "id": "ddebbd97-b6e9-462a-9d5b-fe0129b6bb33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 193
        },
        {
            "id": "0120df5c-f34e-4f6f-af11-9d5300c6f422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 195
        },
        {
            "id": "fe4739e5-e35a-43f0-a5df-dbaa863a21a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 196
        },
        {
            "id": "bb41bfd6-9a78-421e-a180-4e5ea90cae7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 204
        },
        {
            "id": "31915a75-156c-4780-8676-4e2592d063fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 205
        },
        {
            "id": "3360111b-bde6-48f2-b2cb-c84dac53a873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 206
        },
        {
            "id": "68b3201a-72a5-4acd-9806-fd033f55c189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 207
        },
        {
            "id": "e2bee8c1-4144-4df6-9cae-805176a14235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 209
        },
        {
            "id": "51cb53b5-92a0-4e15-bd13-dd0dee99a108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 121,
            "second": 221
        },
        {
            "id": "581fb925-26ce-4a42-873c-fa19696077c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 122,
            "second": 51
        },
        {
            "id": "c531c309-0898-4845-9aad-5ad6fa972e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 66
        },
        {
            "id": "c98277dc-2a76-44cb-b6f4-7bd8f015d01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -18,
            "first": 122,
            "second": 74
        },
        {
            "id": "a5e5c4a8-e012-4f8c-b14d-daf8c2dba7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 78
        },
        {
            "id": "b920c1fd-fafd-457c-be39-82df87ba1334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 80
        },
        {
            "id": "256ff681-4f6c-47a3-8675-a1c4590e25e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 122,
            "second": 82
        },
        {
            "id": "32a2a2d3-15de-49f0-ac4e-f71205d65003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 122,
            "second": 84
        },
        {
            "id": "097dd722-ea7f-4b25-b465-a1a8b4f44d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 122,
            "second": 86
        },
        {
            "id": "6a1d68e4-909d-4d66-99fa-69a77e7137c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 122,
            "second": 88
        },
        {
            "id": "2cb6d816-d8d6-45ae-92f6-1bdc35f2e73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 122,
            "second": 89
        },
        {
            "id": "76188943-9f33-48ef-8327-bc0dd1654249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 122,
            "second": 204
        },
        {
            "id": "fe1c7965-c6c3-40be-a189-e9a171bc7258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 205
        },
        {
            "id": "97a160eb-51e0-4d2d-8ac0-43e4b47b6ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 207
        },
        {
            "id": "70ebff4f-0e48-4ec6-81de-a438409dd660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 209
        },
        {
            "id": "db4912b0-1e20-4e87-bc20-94ee441a2bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 122,
            "second": 221
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 60,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}