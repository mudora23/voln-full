{
    "id": "e6a902c2-f800-4b29-9d44-c4aafe83fa97",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01_25_sc",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine Capitals",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cbaa7e04-b431-4ee8-a8b5-a99059d045a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 386,
                "y": 162
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "73e18026-61e5-4da0-a9fd-3cf8e9b40882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 476,
                "y": 162
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4a11049e-a60e-4d98-941e-3d29ef9278d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 366,
                "y": 162
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8fb44fa6-fc52-4f7f-b03d-5a731cf93235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 125,
                "y": 122
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4943941a-9de1-4083-b167-26b788b5ec46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 322,
                "y": 122
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8b3decef-ffb8-419b-9a39-b82d84bbb314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 466,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5a15b5a9-bd9a-482f-808f-93d13c92a032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 424,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "14b24571-6e10-4d91-954d-33ce371a823d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 470,
                "y": 162
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "26a2c1f4-221f-4a83-9477-c45193afb449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 345,
                "y": 162
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8a503ae2-103f-498f-996b-a55be205b2b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 334,
                "y": 162
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4f06317c-d2c9-41e5-9d16-a9b3d0443d49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 162
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5ae2e3d8-0e0c-48c9-8c28-86ec5af5dcb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 486,
                "y": 82
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4a4547d1-93b9-4e4a-9014-0ad9f8f77409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 414,
                "y": 162
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4d9dd0cd-8b51-458c-9181-86e7508ea321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 312,
                "y": 162
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0efbdb0c-a22d-4c79-9b0e-d806549bd8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 456,
                "y": 162
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4210b3e4-6e1f-42be-bdd7-49cfe4049ab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 162
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "20c21999-dcb0-4545-bfde-91b843b6dc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 402,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "17901276-67e1-4a89-b196-9562a4faa35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 169,
                "y": 162
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "de5172a2-a34d-4af6-9e62-ff4fa61d5a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 417,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "55dce52f-ccca-485d-a195-70b31cb2b5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 162
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3599e60f-24bf-409e-a9e3-1b248870b32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 142,
                "y": 122
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1e70b47d-6b7f-43e0-9168-f9bf74c78445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 447,
                "y": 122
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "47f23d12-2f96-4537-b9b8-33612a5466c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 462,
                "y": 122
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fecbafd4-056b-4103-860f-199eab68fe51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 492,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1305155a-beea-4e67-bc3c-867dad568d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "dc4b5476-7cdc-4618-8f8e-0483b5170369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 370,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "80a35039-391e-4e54-afd7-11c024992b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 482,
                "y": 162
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "640ea27e-43b8-4f9a-80d0-9fbb65bc11e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 449,
                "y": 162
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b8dce6fb-885c-4aec-9125-0b7aed059889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 91,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cab0957a-94e7-40ae-9eb0-e8e1a51ffb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "732f4ed0-460f-4ab0-8254-bded088b7e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 74,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c1d091a1-cf30-4950-b685-4bcf44514fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 76,
                "y": 162
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a664ed36-3389-4fbe-9bdb-a97a7318a07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "82fa20d4-c06d-4168-8ad6-f7242b0bf0b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b1fe141b-68d0-4c08-8d86-9e1c6f3f0b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 445,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1ac513ee-479c-49d4-ae57-9bd7c466b284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 188,
                "y": 42
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f4b37776-07be-4341-843b-6a0b394e2619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 50,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2dc8a7a3-a64b-4664-ad6b-9f52141c3879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 23,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "95ad6064-6289-4b2a-8e28-fd11b75aac45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 468,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4fb0e1ae-c9d7-486d-aec5-b6d3f67cb2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "692b451b-947f-4d49-9df8-45c7398e87a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "80c49520-3273-4103-8175-660af2a6b47d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 266,
                "y": 162
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c20faee2-3ea0-4c85-87b1-3bf8d6e6b975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": -3,
                "shift": 11,
                "w": 14,
                "x": 386,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "72a059d6-f485-4967-81c4-df2c99899b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 74,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ef604c0e-7ac2-4564-973b-3b7657a4ec9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 320,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a0ca70c6-38f4-4eb0-b52c-3e3ca7f9e717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a2ad8d0e-6cbf-4651-8d29-f554b6371ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "73df376d-e38e-4598-b72c-4131689ecd50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 97,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "91d4784b-e4cd-48dc-9535-bb6645123567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 377,
                "y": 82
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2bbfff5c-c1b4-4e67-9a12-f5046c17b668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 374,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d08c6873-d4cb-4142-89e5-2ae3063824d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 276,
                "y": 42
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bb88a083-12cc-4096-81c8-a95a9bf25fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 108,
                "y": 122
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "17b391e5-2481-4d93-a44d-2d18051db4d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 166,
                "y": 42
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c0ca3bc3-8756-4dbc-ad5f-93c01c55b24e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cc4af39c-4b31-4085-b0e7-96b9d642121d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 350,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bb5c63cb-870a-42da-ba8b-d759ad7c5489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 31,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c6970925-a290-4d70-882b-c1b7858ea3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7d1c7968-a0e6-4ca6-a504-048301d4fde5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 298,
                "y": 42
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5f10ab23-3a15-46a9-b544-d429ccfb0d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 319,
                "y": 42
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9aacd8aa-2d8d-4757-a00a-6dc6c2e51556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 356,
                "y": 162
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "16bafdfc-349d-4768-b2c5-aed44ae12645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 242,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e4a0fbc4-774c-4777-846c-3fe6284612eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 396,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "dc94bf47-bab9-4d84-b07e-dc31d0a6f469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 117,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "72448cbb-1832-4cf1-aef2-7798bd75bc4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 122
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "81fc1827-b648-4c03-984e-a5f416d8bd19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 406,
                "y": 162
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dbd63b59-4241-4105-8a52-e30065507f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 361,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f4555231-54c8-403f-8475-46c73a92c76f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 176,
                "y": 122
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b494efb3-b703-43ab-a3aa-0b5811e0d4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 354,
                "y": 122
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "661e0a00-e2f3-4862-a29a-40228beca703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 358,
                "y": 82
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d8cac64d-87f9-44ac-8a49-08b30b89c73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 338,
                "y": 122
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4fec6aa5-99a1-4a79-b6f4-f473f654da6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "76ed0867-caee-4e13-8f13-f2bbc452eb3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 450,
                "y": 82
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4452c57e-baa4-454d-a325-6f26a75312c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 382,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0237e722-ea60-4fcc-950b-c116d9eac9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 376,
                "y": 162
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "95179cbf-119e-4791-8032-0d3d7ae97c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -2,
                "shift": 10,
                "w": 12,
                "x": 90,
                "y": 162
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c13de8ae-f6f2-47fa-bee9-7d32b9da1e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 301,
                "y": 82
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f3f0e019-3f57-4dbc-9bef-92fb7c422e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 32,
                "y": 162
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "de019c49-287e-49b6-bc59-38c5f5bccc6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 143,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "39e38583-9451-4ff6-9777-200540189bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 103,
                "y": 82
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f1ef61b6-28e5-4bd2-926d-2f654a10d673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 263,
                "y": 82
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5b864bbc-5222-4afc-a0e9-438faac86ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 306,
                "y": 122
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a1f1b31c-a262-4435-a69e-ede669a1292c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 203,
                "y": 82
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0a7607a1-06a7-4fa6-bb1e-373d7b7c1bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 396,
                "y": 82
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8589e6d2-965e-4aeb-8b1f-8b7f3bdcb0b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 162
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "406f979e-9069-4450-8f11-a72ae8bf7378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b779d03d-513c-4f85-8701-9dbbeb0f68ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 243,
                "y": 82
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a04d6dcb-9650-4774-8046-7c211391775a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 163,
                "y": 82
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a0af58cb-6e8d-4f0b-b667-17e8e42086a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a5411add-e0a2-4675-a007-6110162d39d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 43,
                "y": 82
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c3354b99-ff19-420e-b132-31686f83ab9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 432,
                "y": 82
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "43026be8-b6d9-419f-b225-930f5eb2855e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 242,
                "y": 122
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3bda71dd-6486-4d7b-838e-9edd27ecd6bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 301,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dca8bc4d-8d4d-4313-9721-25404401e855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 488,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c1884ef6-91ee-4eee-aedd-b2bc7938ce77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 323,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "48010314-57ad-495a-ab16-2666fadaeb70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 258,
                "y": 122
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "746e0983-6462-407f-9fc9-085dda98e468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "3e3f592b-f9ce-4d12-a82f-5349e46374b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "efb4551b-16d5-44d2-8d0b-d239b529ef58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 278,
                "y": 162
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "cfa1dfd3-aa68-48b1-b2f7-f98e3f3ad5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "bb087c9a-b0f3-4df8-8b80-50fa8fdec00d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 446,
                "y": 2
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "d6a53e24-1736-4def-8e95-1a47690e894d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "2345779b-b6a8-4f12-8f74-b761a1037e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 282,
                "y": 82
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "33a20c6e-7437-4b88-bb5b-f147d20b308b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 340,
                "y": 42
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "35b7cd1d-2596-430f-a049-2bd051b40c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 403,
                "y": 42
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "d97c22b8-2650-48cc-94a7-00a07078dddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 254,
                "y": 162
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "000b0093-c9bd-4a2a-a6f7-29112ecab20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 223,
                "y": 82
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "b7c1f0a5-2d77-4274-ac0e-fb8a0241beef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 183,
                "y": 82
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "0e9beb26-5b97-4465-af4a-13d52522ad0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 143,
                "y": 82
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "892fa887-0144-4a04-886d-cc0e3aeaf03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 122
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "c4dca20f-f519-48bb-945a-be794d2f545f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "d2f959c5-a155-403b-9547-ea26dc3520db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "09955cfc-3f0b-48fb-a394-d80d156bb6b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 123,
                "y": 82
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "c7b86107-2ec9-4286-803f-88f5bf35c896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 477,
                "y": 122
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "9ca59d1b-bc72-41e3-8f67-965c3ea4bccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "56f15f07-70a3-4b92-82d9-8b73f71fad98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 83,
                "y": 82
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "675afc0a-d09c-432f-938f-bb32968e29f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 339,
                "y": 82
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "98f07a74-c3d7-48e7-8ae9-112c17320dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 432,
                "y": 122
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "e2f4a6e8-cdf0-4729-b524-e3765471d8bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "c2286ba1-e416-4ea9-b46c-1e65c083612e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 63,
                "y": 82
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "fca83207-9ce7-4326-89f6-6a1370c790eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 210,
                "y": 42
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "3fb6203e-277d-451f-bfa6-56a010643e0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 56,
                "y": 122
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "e60eb6c4-eef2-45dc-9b78-2f0b3239938f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 232,
                "y": 42
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "01622f8e-9e94-4965-b385-bace41149b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 62,
                "y": 162
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "f7a01ab1-c675-469b-b6f1-37b47874d9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 159,
                "y": 122
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "bffd4b7d-b059-4afe-801a-3b3d3b7d7689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 143,
                "y": 162
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "24de0886-2a5b-4688-b0a2-a70cefb0c462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 487,
                "y": 42
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "fdda441c-c45c-42e8-b360-ecd47b5defe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 210,
                "y": 122
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "92764c1e-6552-4e26-8f72-809ec6a73013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 254,
                "y": 42
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "8b233d91-2fff-4866-9c39-87e3c0342aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 274,
                "y": 122
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "4b3cfae3-472a-4273-9cb3-224ca10ef29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 193,
                "y": 122
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "0fa114a6-68a9-475a-946c-76ab856c19d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 130,
                "y": 162
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "ca27313a-c999-4173-ac98-c50e025faf52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 414,
                "y": 82
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "04c586ad-51c5-409a-af79-c8b7097b1296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 38,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "e7ca2c8d-aaac-431e-a789-aef464b0c72d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 421,
                "y": 162
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "a245c82d-fb2b-4212-a72a-50904614c8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 428,
                "y": 162
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "df4fee11-166a-4ce2-bb2f-241f62163bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 435,
                "y": 162
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "a9a4caf6-c5f7-4ba0-8257-705b8fa943a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 442,
                "y": 162
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "ba73fc6e-438c-4b0d-b9e6-ef4620660952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 162
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "bcf27ce2-3600-451a-b544-d08314f19ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 162
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "c087774d-fa7d-4d1c-8b44-9aaedef2e841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 162
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "cb077ba4-0561-4252-8e3d-f5b0583ab173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 104,
                "y": 162
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "4f6b30de-905d-477f-810a-b7d0fdfd37fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 38,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 26,
                "y": 42
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "668bf6b8-6a7e-4b9f-b893-0a1a31d63120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 463,
                "y": 162
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "e90a9b47-5716-4c7c-9a39-c3f55969f23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 38,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 290,
                "y": 162
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "42df66ca-b601-4457-a4b5-a28328f2f310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 290,
                "y": 122
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 25,
    "styleName": "Small Caps",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}