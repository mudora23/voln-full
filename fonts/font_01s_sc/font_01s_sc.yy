{
    "id": "5c0b17f0-e508-4048-9d04-511b21456300",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01s_sc",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine Capitals",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "42e03be2-7b23-4fb0-95c9-4f7c460e7466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 236,
                "y": 127
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3ce30f48-de8e-4801-bc6e-cc2c2cf5b4df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 137,
                "y": 152
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b33b0fd5-39b3-4e8f-8a86-894c5a34fe94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 142,
                "y": 152
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7dbd6d42-82d6-43f4-b903-8ec94c907e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 44,
                "y": 127
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3d92a407-78bc-4fc8-8c6f-3fd6ae8b3070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 243,
                "y": 77
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d7180453-0aa8-42d2-83e6-ea255c834488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 77
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d3e49776-0f29-4bad-a593-b44c1713bd3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 198,
                "y": 52
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d0222faf-7cae-4c43-8a7a-ed842435b393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 190,
                "y": 152
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c96db2db-0e28-4ad5-b628-6376bc2fa2fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 185,
                "y": 127
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c2f1235f-e586-43c0-a2c2-9510d669df29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 217,
                "y": 127
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ba7bd77c-98f8-4ab8-bec6-f599d6fb1119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 152
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6cf1291c-80ba-49d1-9d04-02adc06fe51f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 94,
                "y": 127
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f72c617b-e97d-40a5-ad7d-25845686fd9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 152
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "50d30437-d688-4cb6-bd4e-0b1a70df924d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 48,
                "y": 152
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "51b56fac-a280-4356-a1d0-643f620f43ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 124,
                "y": 152
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f4ab1618-685b-4ed3-b5e5-c070327ae384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 209,
                "y": 127
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1b307fd0-8040-4121-9b37-9c4834d25c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 204,
                "y": 102
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "de5b0843-8b1b-4b35-8104-13d6607edb57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 176,
                "y": 127
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e6268f33-88d8-4d3a-a342-a527bb5a9b0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 127
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ba5e7afa-c4b2-478c-a12f-fa0a8d624187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 226,
                "y": 102
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e2d45952-f905-44e1-abab-e3bd39fb785f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 237,
                "y": 102
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d257abea-6f83-4767-9e81-06d6984a7a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 125,
                "y": 127
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7dd444d2-8aca-42e9-bea6-dbe986194c52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 102
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "eb6a3d01-0ff7-452a-baf4-8d528e19e5b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 135,
                "y": 127
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c6663434-58bd-41aa-9493-4606c610e5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 116,
                "y": 102
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a37915c9-eeee-4d21-9be4-0c49110a3de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 115,
                "y": 127
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2378925b-faf4-4e6f-a808-88b3db8b7692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 119,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3498ab10-5d21-4d46-827b-2a4cf5669b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 91,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2b1311cd-f68f-4260-a568-7616aea2bdd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 33,
                "y": 127
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0a18ad95-e276-4262-a416-d5254c616c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 127
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "cc0360cb-5e2d-44c6-8095-a7759269ca4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 22,
                "y": 127
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "62151f6f-b65f-4500-aaa7-63f71bf6329f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 74,
                "y": 127
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a3d24276-893a-4235-a07f-0c146e9a2430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4c73ceaa-5893-4bac-972f-a02cd9f0e785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d0b84da3-924a-443f-bdce-813b6ea3412b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 143,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "85eda4be-b144-42b1-a360-a7163ebde20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 219,
                "y": 27
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "432936f9-9d9e-4e0b-930c-4b8442ee1de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e8c43757-745f-46b3-964d-8e442e58a33b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 169,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "757eacef-7599-498f-afe7-b755ca6081aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 102
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "589e2f31-d238-4e96-a39e-73c31ee71e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 32,
                "y": 27
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5d5bb401-9c47-4cdb-8380-3491dc7938f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "cb1a05b1-2b67-4d68-aa28-4875249a9ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cb8a5d7a-0b91-4f66-8edb-226732d21314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": -2,
                "shift": 6,
                "w": 9,
                "x": 194,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4544a9a8-6f71-4268-935c-f344d2d7ab2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 234,
                "y": 27
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "df35d818-a692-4771-ab46-22c8867c69b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6f3c4e6c-4ded-475d-afd0-0a011ae536e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6d463797-7321-442b-8897-f9162a237a7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 96,
                "y": 27
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "34b4532a-5cf8-4361-a4f8-6ed21b60a31d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 48,
                "y": 27
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "acd43b38-302b-4b33-831a-aeaede66ca80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 156,
                "y": 77
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e0f38d69-4852-46f2-b15e-dfae544fa7fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fe67aa50-fcd2-49d4-bb71-c483607157ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 156,
                "y": 52
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cfc9cc00-3708-4da6-9d71-4af9eb424c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 102
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2a48d8b1-1c6c-4b82-876e-4ebcbaedc920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 44,
                "y": 52
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "21e1a5f8-b0db-406b-8565-197fd008e063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "94deb44d-f0ed-4ded-815a-c195e68a2421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 189,
                "y": 27
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6f85e948-3da2-4c40-b685-eb2710e78429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8eef6f53-4aae-4fe3-a422-9603f54d2ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 159,
                "y": 27
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cbf76880-743a-4a87-abdf-45e1bd234e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 184,
                "y": 52
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "84a867ff-8e7b-49e7-ace4-bb6dc14ee969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7dbcf6c2-b4f2-4925-b295-bffc69b158ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 34,
                "y": 152
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "31357722-11c8-424f-9172-2ba181034f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 243,
                "y": 127
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "261b963a-d545-4074-b4df-4078c733c40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 41,
                "y": 152
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ac2a0847-dde6-4775-b711-fea172de08a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 73,
                "y": 152
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "069347b9-34c1-44f8-b20d-daae9cd48182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 212,
                "y": 52
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0aaea648-780c-4850-813d-81a8b4fad037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 168,
                "y": 152
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "214600c6-3160-458a-b4b9-fcf44ce42b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 30,
                "y": 52
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2df75685-8b6e-418d-b0f1-79eeb148f94a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 102
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "53fd83d7-72a5-46a0-b191-802c40d2ff0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 171,
                "y": 102
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2718e8af-19ba-4c89-9e27-cc1fd9a81aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 106,
                "y": 77
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8b6e6b5a-2ecc-40c0-80e8-66c612ad5333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 94,
                "y": 102
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1ed9ff6f-88fd-45d8-ad23-3e4a77d5d8c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 127
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4ebdb39a-4a0b-4b40-b169-52bdb85e0229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 93,
                "y": 77
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "561c1c57-d33c-4683-9262-f52c4e3a721c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 170,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4f2394b1-3235-4cb6-933a-b1de2b74ca00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 152
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "828a0d65-fbb6-4828-abe3-7a79db170e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 13,
                "y": 127
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1365bc4e-22dc-434c-b1c4-203deb85a037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 224,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "40d8fc3e-4a37-47cb-a310-cbff3ad02d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 102
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd411397-1c1a-408b-b843-eff3f6d2fae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 144,
                "y": 27
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "83ae8fa6-79e5-4acc-b918-14d8e2e6f32f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 72,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0647bd04-6bf5-4234-99b9-cd2a2488fbf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 237,
                "y": 52
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6a643451-5449-439c-9183-db510062b3d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 215,
                "y": 102
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a566ed91-9423-404d-a7af-2702fc458519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3f7f451f-3de9-484f-ac47-7d3514b03fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 77
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "65e0df54-f617-4532-a43f-1e2b1808c758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 105,
                "y": 127
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6b339651-e361-42f2-8c1c-7344a09978be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bfe9d6e5-da1a-4917-b8d9-2da41a8becc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 86,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "788359ea-4c24-4440-aaf5-07eb659d130a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 77
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7de5321f-4b85-48c5-9d73-4d19698de475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "049c266d-98fd-44d8-a0a7-626a179c4733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 77
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2be1c19f-c7f0-44b0-85db-58ac2839188a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 77
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1194c911-8d30-43fe-96c2-4da0803b95df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 102
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d92c13e3-d7a1-4146-b823-ed4d48094234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 201,
                "y": 127
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1c5f4cb3-55d5-4df0-b37e-5852338f68f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 158,
                "y": 152
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9d185a9b-5101-47d4-b62c-54aeb1806a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 193,
                "y": 127
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "723294ed-dcac-4702-84ea-405905502441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 127
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "aca2a601-2190-423b-8230-15bd8c88fa73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 16,
                "y": 27
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "9c423f1c-7c21-4a6e-8610-5773d1fcb78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 128,
                "y": 27
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "497e3c02-77e6-4438-953b-a2c9e727ffe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "b3e4e558-af9d-4e8b-9ba5-2920aba01f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 112,
                "y": 27
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "7f09ce0a-9029-4b7d-9eab-2b0d727880b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 80,
                "y": 27
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "abe2bdc2-bbcc-4ff2-b450-b57062342cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 64,
                "y": 27
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "0c22eaa6-9026-481f-ba28-5f1fc5e96a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 102
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "92d242d4-7377-4c1c-b109-56aa4e40736c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 52
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "859686ec-6468-4a35-8340-2ba08bb5a18b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 114,
                "y": 52
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "6b15107d-94a9-4c38-b204-3afc6d12ccc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 152
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "c0346c6e-8345-4b95-b79f-a355d6e371e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 77
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "4ff48f7a-2da6-41fe-884a-78a4869aee6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 52
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "6a747fac-1d57-4f85-bc0e-86cc2723f212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 52
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "91ac4aa9-5362-4ad9-b2d4-500fc1d9cf34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 102
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "1a9c477d-d99a-4aa8-8f0e-77ce5aab6831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "2f67618f-5243-4194-b188-16940f039efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "9c99dd5f-dade-4f22-a36b-b8bbd3c21e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "556b64db-a8aa-4867-819e-e87f526d39b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 138,
                "y": 102
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "84fae69b-47a8-4ee1-aa4a-40937e18e70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "8ff51d7c-1307-45be-9de1-990864e36538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 130,
                "y": 77
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "3ea2a53b-2af3-4f96-b145-5e821ba2a258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 77
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "729d58b1-3d59-4407-b51d-19911a95af3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 102
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "d37ddf03-3002-42f2-8125-f7b49dfd6093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "b8f93c2f-0a6f-4b5a-ab74-8966893706d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "721af590-2cf1-43e5-99d9-f0cd84abc187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 58,
                "y": 52
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "d9de5f58-91b7-463b-b027-38cd265f4fda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 102
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "d73f8797-4419-4049-81d5-00012254bac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "6595a49d-54a4-4804-98c2-dbb69a919ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 205,
                "y": 77
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "4cf6e397-564f-40aa-9a9d-0888c3b7ddd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 77
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "87769966-6f34-4909-a983-20cee1ee6b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 102
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "2a3ae205-91e7-4f11-b8de-81aed1541893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 52
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "aefbb4b7-1def-4bc9-b60b-9aa61d584d8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 182,
                "y": 102
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "820c66c7-970b-4a63-8b0b-be38e0924bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 174,
                "y": 27
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "6b52895d-a68d-4213-a59e-7982d812699e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 84,
                "y": 127
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "91271777-3c43-408f-b015-6c8daf64ec59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 127,
                "y": 102
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "6f47b5cd-7079-4a42-9760-445416345826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 156,
                "y": 127
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "cc042f24-7445-479d-b8e1-65b143026ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 225,
                "y": 127
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "119f1c2c-6593-4c89-bddb-474c5c20b1cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 13,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 215,
                "y": 77
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "43d57a87-08b8-4728-992e-39a17035bdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 7,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 185,
                "y": 152
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "d609f278-ecc0-4b94-9cd5-c70285cafddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 180,
                "y": 152
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "339e2c76-4198-4039-bb1e-83f3e4f7cf1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 105,
                "y": 152
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "506b7e7e-f855-43f4-be63-10e13b9bf1d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 162,
                "y": 152
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "edcbb74e-cc51-4f70-8c56-556916872659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 7,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 149,
                "y": 152
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "151dcaf5-24bc-40ee-8560-7adf4b0982f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 152
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "9fd1674e-027b-48a8-975b-5d642ce8be13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 127
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "34b34c51-08d1-4537-badd-97222c4b7853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 152
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "525ef1f1-d8b1-4e60-80a9-fb0ecafbb188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 204,
                "y": 27
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "b740f31b-27e1-4298-8358-ad3f188e13a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 175,
                "y": 152
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "9ea4cc59-19f7-4177-a17e-d33d8aea5589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 129,
                "y": 152
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "3b4d2d50-7b12-408a-a8a2-733fd79fa602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 9,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 62,
                "y": 152
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 15,
    "styleName": "Small Caps",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}