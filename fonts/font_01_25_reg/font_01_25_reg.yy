{
    "id": "c7e54edd-16f0-4a2e-a53b-c4c936eb7833",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_01_25_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Linux Libertine",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "845ba7e3-b3d8-43bd-8b08-c7c5eef733e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 334,
                "y": 162
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "434ad1d0-5cdf-4797-8418-0a00ee891d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 434,
                "y": 162
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d5c22437-a8de-40b3-aed8-d015a1f4381c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 324,
                "y": 162
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cc124037-48d0-4e83-a6e0-8c9361097711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 448,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3e5e1b9f-2cbc-4046-9b7a-434b845a3705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 122
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5634a457-33ed-4c8b-bbc2-0606e4782922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 407,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c0420ee3-d68c-428b-94c3-291203b65cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a0b4001d-7da6-4871-8214-7a34bfca6ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 440,
                "y": 162
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c1037986-6ce1-4c40-966a-20b3dd83570e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 247,
                "y": 162
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "98cfcc24-086a-4f9b-a47e-13c38b50b92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 280,
                "y": 162
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c1e63a0f-244e-41ba-9e76-3048c4e14cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 106,
                "y": 162
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5fdeb0e7-e580-43eb-aa83-4e433fd1bb09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 233,
                "y": 82
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a6cb4e96-88ee-4fe3-9604-56b2f61a9674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 421,
                "y": 162
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0c0f4c37-9e3b-4a88-9ec6-30c3d03fddfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 236,
                "y": 162
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0de73292-bff5-4135-bdb1-b0022ea9e524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 372,
                "y": 162
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bafe050e-6b30-4c4e-be85-4b086cdb70e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 118,
                "y": 162
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a89165bb-13da-421a-947a-3b3a7363ca4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 418,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ba77a2ca-ceed-4787-b5a2-69c611acf9c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 16,
                "y": 162
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "90039983-797a-4497-89cf-06a56abe6623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 373,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dbe013c9-aeed-4298-8cbe-43effad944ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 448,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "23aedb91-4449-4790-aed3-5ea206627be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 104,
                "y": 122
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab9e628d-3fb7-4b08-b0d3-08231556f7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 343,
                "y": 122
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "84e5cc09-d533-4222-b8e0-755a6a8c64ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 313,
                "y": 122
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "400d5da6-8ccb-4ea3-ae7d-bd80779e6c89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 298,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "88427d83-34b8-4cd6-b447-13441099132b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 283,
                "y": 122
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "fc7088a9-d02a-4afa-9590-5ec8bcbe410e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 251,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b3bd8d4d-9032-43d3-b814-1b242778705c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 428,
                "y": 162
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bde1c7a5-5267-473f-b20f-a34e19285a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 407,
                "y": 162
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d11ae0ca-e06e-4219-83b1-cb8bac186e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 121,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1b354c01-bf97-42a0-86d0-90b25ecc7baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 413,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b21b397f-6307-4c2c-a8d5-b29e0ab97ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 482,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "64ab15b9-f71b-47e3-9b43-f812e284f0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 491,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "738b4856-4d76-47fe-9902-eb8958c9ad32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6d29a1dc-bbbe-4c83-90e7-eeb01d4e5775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "013c7e8c-4a2b-47fb-bc12-207ec505a6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 386,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1209a9d0-6cc5-492b-b76c-d313e2c40a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 257,
                "y": 42
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1c775796-d29a-4011-9682-b8804b000bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "83dd69ca-7fbe-4355-aa7b-22733facdaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 488,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5a18547c-54e3-4766-adfb-cd8637b3febe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 377,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1cb7c3f2-f14c-4f04-892c-bda27096c0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 26,
                "y": 42
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fff2e8df-a0a7-488f-abd7-75ec2b8fc18c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ee860cc6-6ba7-45df-b075-4afbe6818299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 130,
                "y": 162
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "02c10130-c6f6-4f41-8189-88d7d26eb1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": -3,
                "shift": 11,
                "w": 14,
                "x": 219,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7f268947-e37b-4da1-81be-899685da0771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 145,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2da67027-cf31-4005-ba39-5a9e08044619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 120,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fe8e0154-967b-491c-af60-b453d001944d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7b96c90d-14e6-4905-80fd-2c109840cb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "240a6b00-28f1-4c73-8ddb-c39a0e3420f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 122,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8cededa8-ab74-4979-8e1e-39ee035b305b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 158,
                "y": 82
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cf7dd464-c6cf-47bd-a7a2-2975e4dbffb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 425,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "307483c8-122a-4077-ac94-2009df95fd47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2c9eaa1e-8597-4231-a7f2-a24bf8f8dad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 53,
                "y": 122
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5dacf06c-4a6c-4827-a780-534391e18825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 213,
                "y": 42
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "af0ab4b6-c26c-45c1-a80c-aa3131ff4447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "10ff1b9d-4754-4a16-8a9d-3c64f5c65dac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f819ee22-eed1-4f46-8379-51c28e4b4e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 31,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7d6f3f09-28e2-4982-b1e7-3a4b7b6d4eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d00b5d18-436d-4e58-9b0b-7b5b31ca9358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 344,
                "y": 42
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "36b05174-82a1-4de4-a0d1-0b13664d2098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 365,
                "y": 42
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bdf96d4e-db50-4858-80fd-49f8994cb8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 344,
                "y": 162
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "87468e6f-5383-425e-bdaa-b2c09938f90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 142,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1dbe13f9-43ad-43c3-ac91-08a2308a1c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 354,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b7293c9c-5e01-4eaf-8999-6a661b1765c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 81,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "21bc4836-d8fc-43c5-b867-f00601fc221f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 215,
                "y": 82
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d7eeaa3c-aa5e-446a-9890-1c0f674541f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 364,
                "y": 162
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3c6282dd-3223-4014-b64a-226c09213ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 70,
                "y": 122
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "680bcfa0-9f6b-418d-962f-86a88bf78b11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 323,
                "y": 82
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7b1ca69b-da7a-4bdc-a0bd-096f08279ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 328,
                "y": 122
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b64a665c-2ba3-442d-ba02-65a91ef264f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 269,
                "y": 82
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "74c9c47d-7bac-48b6-93aa-ce8f92fc78fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 403,
                "y": 122
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c11d692d-d1e5-43d2-b3a5-617334493ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 14,
                "x": 267,
                "y": 122
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c1932ae5-0bc0-4f02-8aa1-35fdd8141bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 122
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "76ab2120-c66c-4ee1-b897-1542cdd4388f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 468,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6edb8c38-163e-46a4-ad51-44732b0caf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 214,
                "y": 162
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3be92ef5-f482-4422-8ce7-18aa0bb7f55d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -2,
                "shift": 9,
                "w": 9,
                "x": 225,
                "y": 162
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8b8f7128-bfec-47c3-8dd4-0aa25c9d3c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 139,
                "y": 82
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3952f234-75a4-42b3-89b2-2c9f4a369f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 302,
                "y": 162
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1ca069a0-21c4-4eea-990e-44c657d4680b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "329a2a5f-b99d-4dd6-a838-3a0df66906eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 428,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1964c36b-e1f8-48af-9405-fb92eec7fa58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 122
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "401bd0cb-b9bb-45e4-84bf-ae7156cf084d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 359,
                "y": 82
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4b2cfc6f-732e-4636-bc93-2e287156c55b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 287,
                "y": 82
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3d44d1d4-1532-48f3-ba4d-c0214e5bc531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7cb73908-d6b3-419f-8cf3-efc8d1cf7800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 68,
                "y": 162
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "16438c4a-e31a-4333-b6fe-a6a50e278f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 94,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a50ffe76-5baa-4ec4-9875-5599b07553c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 448,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "534e30ae-4ddd-44f3-9af5-28b3e1132397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 177,
                "y": 82
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "37c98a31-8f99-4939-bda4-f82041bcd7ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3d4fe5db-f6b8-4782-bea0-a794e3769dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 251,
                "y": 82
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "046f5a34-c95d-49f5-8aa4-506b49c2d0d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 196,
                "y": 82
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "af4b7d31-0697-4119-8d3f-d3fee2b15da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 388,
                "y": 122
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e2e60c46-7a87-467e-9538-377309fcd59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 291,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6d1ea8d-b63c-49d3-8b0d-1ba601407ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 446,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6588ccc4-2646-4c5f-be7d-273a267ab276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 313,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "51507826-b694-409f-bb90-489b30091e70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 187,
                "y": 122
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "ce5019df-57e6-43d5-8af5-9da3185ff7b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "2c67a029-df70-4823-96e2-5e61260a7eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "45f40ab9-9846-4b1d-8b74-66b266118794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 162
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "05e73d63-ce05-409f-b722-14f3dedbe5dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 98,
                "y": 42
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "39754562-2e5a-4968-8300-bbbfc667adeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 74,
                "y": 42
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "fc515a1e-d21a-476e-8739-ba1b8defc84c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 50,
                "y": 42
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "478735e6-cbf5-4b2a-afbc-b9eb36975acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 101,
                "y": 82
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "c558555e-40a9-4710-919b-eb0a95999d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 138,
                "y": 122
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "b5d5c044-d067-4989-98a7-9b9b5c8373d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 465,
                "y": 82
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "021fbde0-44a6-4b9a-94ca-66a76ad03f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 38,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 154,
                "y": 162
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "038b9e8b-e322-4a99-9cc1-409a85474e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 171,
                "y": 122
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "84c7d216-1255-4eb2-8fa9-f92160a3a967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 42,
                "y": 82
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "424977ad-a40d-4658-870f-af261a573704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 62,
                "y": 82
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "376553d4-b9e1-4afe-8d8c-999e0b357b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 395,
                "y": 82
            }
        },
        {
            "Key": 260,
            "Value": {
                "id": "d4c1f254-5586-4137-a1db-46ef6c4da7bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 260,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 261,
            "Value": {
                "id": "513ccd78-147a-4b95-8b63-5ef71442b180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 261,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 87,
                "y": 122
            }
        },
        {
            "Key": 278,
            "Value": {
                "id": "23b4b861-6bac-40a4-9fe3-4c29da81d119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 278,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 279,
            "Value": {
                "id": "d629e92b-fcdc-4cab-aba7-b7f23da95a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 279,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 358,
                "y": 122
            }
        },
        {
            "Key": 294,
            "Value": {
                "id": "c8fb4593-d02c-4ec4-b8fc-4428ad208684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 294,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 295,
            "Value": {
                "id": "432664ca-9cfa-4640-94f6-194745ece2f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 295,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 82
            }
        },
        {
            "Key": 313,
            "Value": {
                "id": "9e4cbe6c-04d2-41ec-b766-0e596aafb0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 313,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 82,
                "y": 82
            }
        },
        {
            "Key": 314,
            "Value": {
                "id": "433999a1-ce5d-4180-bd66-9f460b0f5702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 314,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 269,
                "y": 162
            }
        },
        {
            "Key": 330,
            "Value": {
                "id": "30050a26-2f7d-4778-841c-6d80f245a982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 330,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 168,
                "y": 42
            }
        },
        {
            "Key": 331,
            "Value": {
                "id": "92efbad0-7a9a-4f42-bd13-2ced2a67e55c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 331,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 341,
                "y": 82
            }
        },
        {
            "Key": 340,
            "Value": {
                "id": "560c133f-6b5e-488a-85af-9b767919cefc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 340,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 279,
                "y": 42
            }
        },
        {
            "Key": 341,
            "Value": {
                "id": "c5adbeaa-ef84-4688-ab5b-6dd25265e6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 341,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 463,
                "y": 122
            }
        },
        {
            "Key": 342,
            "Value": {
                "id": "6d87638b-717f-4448-bfb1-7d32465ee102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 342,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 301,
                "y": 42
            }
        },
        {
            "Key": 343,
            "Value": {
                "id": "c6b785d2-f1ed-4f6d-bc94-3912ebc7595d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 343,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 477,
                "y": 122
            }
        },
        {
            "Key": 350,
            "Value": {
                "id": "0be484c2-c0e7-47c1-acc9-c9e8fcefd79b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 350,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 431,
                "y": 82
            }
        },
        {
            "Key": 351,
            "Value": {
                "id": "b09fa6d3-3ddc-4537-8be4-c0e3de77efff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 351,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 42,
                "y": 162
            }
        },
        {
            "Key": 381,
            "Value": {
                "id": "99158615-1fa5-45e0-a632-13592cad8ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 381,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 323,
                "y": 42
            }
        },
        {
            "Key": 382,
            "Value": {
                "id": "cad77f18-3334-4a9f-8106-f3279ab9b156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 382,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 433,
                "y": 122
            }
        },
        {
            "Key": 399,
            "Value": {
                "id": "794a5adb-f55a-4f7f-9977-1b7a0c495d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 399,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 191,
                "y": 42
            }
        },
        {
            "Key": 601,
            "Value": {
                "id": "274d2022-1c2d-484a-922c-5af14afcf6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 601,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 155,
                "y": 122
            }
        },
        {
            "Key": 7776,
            "Value": {
                "id": "abfcd618-e11e-4613-bcf8-68fcd6decc49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7776,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 7777,
            "Value": {
                "id": "6f5de5c3-8662-43bd-a344-f69c008bf9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 7777,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 55,
                "y": 162
            }
        },
        {
            "Key": 8211,
            "Value": {
                "id": "51063789-36df-4c07-91c7-36667e6206f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8211,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 305,
                "y": 82
            }
        },
        {
            "Key": 8212,
            "Value": {
                "id": "a081e01b-0aee-4910-9489-56029c12ffc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8212,
                "h": 38,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 8216,
            "Value": {
                "id": "397b58c4-9fac-448a-9f91-c9c28ec85633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8216,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 379,
                "y": 162
            }
        },
        {
            "Key": 8217,
            "Value": {
                "id": "3bbc5fa0-2419-4d71-ab30-8fe69bb6e271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8217,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 386,
                "y": 162
            }
        },
        {
            "Key": 8218,
            "Value": {
                "id": "37c35e4f-6f6f-4d14-874a-29eb3d26f394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8218,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 393,
                "y": 162
            }
        },
        {
            "Key": 8219,
            "Value": {
                "id": "3575bff7-2fbc-4892-b66b-52eec04d54d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8219,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 400,
                "y": 162
            }
        },
        {
            "Key": 8220,
            "Value": {
                "id": "dd827001-7d5a-4e83-b8d7-d2347fb808bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8220,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 190,
                "y": 162
            }
        },
        {
            "Key": 8221,
            "Value": {
                "id": "aa1e065f-b61d-4cc9-8d27-7d98396d74ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8221,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 178,
                "y": 162
            }
        },
        {
            "Key": 8222,
            "Value": {
                "id": "288b18d6-c658-4d34-bda3-017cc1349724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8222,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 166,
                "y": 162
            }
        },
        {
            "Key": 8223,
            "Value": {
                "id": "e476860b-5fa4-4aaf-ae3e-80c8d59741f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8223,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 29,
                "y": 162
            }
        },
        {
            "Key": 8230,
            "Value": {
                "id": "6d6293e5-c0e1-485a-8685-6df0eedfeed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8230,
                "h": 38,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 8242,
            "Value": {
                "id": "666c1d28-d350-4dc0-9d8f-5deecbdb3ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8242,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 414,
                "y": 162
            }
        },
        {
            "Key": 8243,
            "Value": {
                "id": "0029003c-32f4-4f84-a97c-76c4420472b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8243,
                "h": 38,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 258,
                "y": 162
            }
        },
        {
            "Key": 8244,
            "Value": {
                "id": "08264afc-d21f-4c11-8244-0d74525d3b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8244,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 203,
                "y": 122
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 196,
            "y": 197
        },
        {
            "x": 207,
            "y": 208
        },
        {
            "x": 219,
            "y": 220
        },
        {
            "x": 222,
            "y": 222
        },
        {
            "x": 228,
            "y": 229
        },
        {
            "x": 239,
            "y": 240
        },
        {
            "x": 251,
            "y": 252
        },
        {
            "x": 254,
            "y": 254
        },
        {
            "x": 260,
            "y": 261
        },
        {
            "x": 278,
            "y": 279
        },
        {
            "x": 294,
            "y": 295
        },
        {
            "x": 313,
            "y": 314
        },
        {
            "x": 330,
            "y": 331
        },
        {
            "x": 340,
            "y": 343
        },
        {
            "x": 350,
            "y": 351
        },
        {
            "x": 381,
            "y": 382
        },
        {
            "x": 399,
            "y": 399
        },
        {
            "x": 601,
            "y": 601
        },
        {
            "x": 7776,
            "y": 7777
        },
        {
            "x": 8211,
            "y": 8212
        },
        {
            "x": 8216,
            "y": 8223
        },
        {
            "x": 8230,
            "y": 8230
        },
        {
            "x": 8242,
            "y": 8244
        }
    ],
    "sampleText": null,
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}