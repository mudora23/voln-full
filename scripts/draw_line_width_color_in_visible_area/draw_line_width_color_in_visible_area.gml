///draw_line_width_color_in_visible_area(x1,y1,x2,y2,w,col1,col2,area_x,area_y,area_w,area_h);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param w
/// @param col1
/// @param col2
/// @param area_x
/// @param area_y
/// @param area_w
/// @param area_h
draw_line_width_color(max(argument0,argument7),max(argument1,argument8),min(argument2,argument7+argument9),min(argument3,argument8+argument10),argument4,argument5,argument6);
