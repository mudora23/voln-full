///scriptvar_PC_Weapon_eq_Desc();
/// @description
var armor = var_map_get(PC_Weapon_eq);
if armor == "" return "no weapon";
else if armor == "Steel Dagger" return "a steel dagger, the twin to Zeyd’s.  It bears the insignia of Ulmårddûn on the pommel";