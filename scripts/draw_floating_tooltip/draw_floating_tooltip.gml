///draw_floating_tooltip();

with obj_control
{
	if menu_exists()
		draw_tooltip(menu_floating_tooltip_x,menu_floating_tooltip_y,menu_floating_tooltip,menu_floating_tooltip_halign,menu_floating_tooltip_valign,menu_floating_tooltip_fancy);
	else
		draw_tooltip(floating_tooltip_x,floating_tooltip_y,floating_tooltip,floating_tooltip_halign,floating_tooltip_valign,floating_tooltip_fancy);
}