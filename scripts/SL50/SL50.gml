///SL50();
/// @description Jaksie Råyn

_label("start");
	if !chunk_mark_get("SL50_Intro")
		_jump("SL50_Intro");
	else if !chunk_mark_get("SL50_After_Saving_Zeyd_A_4_A_6") && chunk_mark_get("S_0005_WON_14_C")
		_jump("SL50_After_Saving_Zeyd");
	else
		_jump("SL50_Return");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL50_Intro");
	_dialog("SL50_Intro");
	_show("gwin",spr_char_Gwin_mp_s,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL50_Intro_2");
	_dialog("SL50_Intro_3");
	_dialog("SL50_Intro_4");
	_dialog("SL50_Intro_5");
	_map();
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////	
		
_label("SL50_Return");
	_dialog("SL50_Return");
	_show("gwin",spr_char_Gwin_mp_s,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL50_Return_2");
	_dialog("SL50_Return_3");
	_map();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL50_After_Saving_Zeyd");
	_show("gwin",spr_char_Gwin_mp_s,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL50_After_Saving_Zeyd");
	_choice("Reason with her","SL50_After_Saving_Zeyd_A");
	_choice("“Forget it.”","SL50_After_Saving_Zeyd_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL50_After_Saving_Zeyd_A");
	_dialog("SL50_After_Saving_Zeyd_A");
	_dialog("SL50_After_Saving_Zeyd_A_2");
	_dialog("SL50_After_Saving_Zeyd_A_3");
	_dialog("SL50_After_Saving_Zeyd_A_4",true);
	_choice("Agree","SL50_After_Saving_Zeyd_A_4_A");
	_choice("“Never mind.”","SL50_After_Saving_Zeyd_A_4_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL50_After_Saving_Zeyd_A_4_A");
	_dialog("SL50_After_Saving_Zeyd_A_4_A");
	_dialog("SL50_After_Saving_Zeyd_A_4_A_SEX");
	_dialog("SL50_After_Saving_Zeyd_A_4_A_2");
	_dialog("SL50_After_Saving_Zeyd_A_4_A_3");
	_dialog("SL50_After_Saving_Zeyd_A_4_A_4");
	_dialog("SL50_After_Saving_Zeyd_A_4_A_5");
	_dialog("SL50_After_Saving_Zeyd_A_4_A_6");
	_map();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL50_After_Saving_Zeyd_A_4_B");
	_dialog("SL50_After_Saving_Zeyd_A_4_B");
	_map();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL50_After_Saving_Zeyd_B");
	_dialog("SL50_After_Saving_Zeyd_B");
	_map();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////











