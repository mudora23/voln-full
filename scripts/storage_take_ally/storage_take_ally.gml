///storage_take_ally(i);
/// @description
/// @param i
var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
var storage_list = obj_control.var_map[? VAR_STORAGE_LIST];
if argument0 < storage_get_ally_team_size()
{
	var char_map = storage_get_ally_char(argument0);
	ds_list_add(ally_list,char_map);
    ds_list_mark_as_map(ally_list,ds_list_size(ally_list)-1);
    storage_delete_ally_char(argument0);
}
