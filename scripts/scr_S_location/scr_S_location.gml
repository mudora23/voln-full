///scr_CS_location();
/// @description - in town now
/*
with obj_control
{
	obj_control.game_map[? BATTLE_END_DISMISS_TIMER] = noone;
	obj_control.game_map[? BATTLE_END_CAPTURE_TIMER] = noone;

	// revoer 1 hp and reduce 1 lust if neccessary
	scr_S_recover_min();



	scene_GUI(GAME_GUI_N);
	scene_layout_normal();
    scene_clear_ext(false,true,true,true,true);
	scene_dialog_set_update_surface();
    scene_bg_location(location_get_current());
	
	if demo && settings_map[? SETTINGS_PLAYTIMETOTAL] >= 60*90
		scr_S_DEMO_GAME_OVER();
	else
	{
	
		if asset_get_type("scr_SL"+string_replace(location_get_current(),"LOCATION_","")) == asset_script
		{
			script_execute(asset_get_index("scr_SL"+string_replace(location_get_current(),"LOCATION_","")));
		}
		else
		{
			scene_clear_ext(false,false,true,true,true);
			//scene_dialog_add("This is the dialog for "+string(game_map[? GAME_LOCATION]));
			if story_chunk_exists(string(game_map[? GAME_LOCATION])+"_DESC")
				scene_dialog_add(string(game_map[? GAME_LOCATION])+"_DESC");
			else if story_chunk_exists("LOCATION_"+location_get_region(game_map[? GAME_LOCATION])+"_DESC")
				scene_dialog_add("LOCATION_"+location_get_region(game_map[? GAME_LOCATION])+"_DESC");
			else
				scene_dialog_add("        You find a safe spot to park the wagon.");

			scene_dialog_set_update_surface(true);
	
			scene_layout_normal();
			//scene_bg(spr_bg_Nirholt_Streets_01_midsun);
			//scene_choice_add("RECOVER",scr_S_recover);
			scr_CS_location_choice();
		}
	}
}