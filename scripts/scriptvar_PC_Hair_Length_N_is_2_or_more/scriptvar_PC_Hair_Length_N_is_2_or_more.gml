///scriptvar_PC_Hair_Length_N_is_2_or_more();
/// @description
return var_map_get(PC_Hair_Length_N) >= 2;
