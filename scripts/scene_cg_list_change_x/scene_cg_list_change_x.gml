///scene_cg_list_change_x(ID,x_target);
/// @description  
/// @param ID
/// @param x_target

with obj_control
{

	var cg_x_target_list = obj_control.var_map[? VAR_CG_LIST_X_TARGET];
	var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	
	if ds_list_find_index(cg_id_list,argument[0]) >= 0
	{
		var existing_index = ds_list_find_index(cg_id_list,argument[0]);
		
		cg_x_target_list[| existing_index] = argument[1];
	
	}


}