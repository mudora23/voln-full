///enemy_get_capture_number();
/// @descirption - capture
var number = 0;
var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];

for(var i = 0; i < ds_list_size(enemy_list);i++)
{
	var enemy = enemy_list[| i];
	if enemy[? GAME_CHAR_CAPTURE] && enemy[? GAME_CHAR_IMPREGNATE] number++;
}
return number;