///voln_save(slot[1-6]);
/// @description
/// @param slot[1-6]

ds_map_json_save(obj_control.var_map,"game"+string(argument0)+".dat");

var info_map = ds_map_create();
ds_map_add(info_map,"player",string(var_map_get(PC_Name)));
ds_map_add(info_map,"playtime",obj_control.var_map[? VAR_PLAYTIME]);
ds_map_json_save(info_map,"info"+string(argument0)+".dat");


sprite_save(obj_control.pausescreen, 0, "thumbnail"+string(argument0)+".png");

