///char_set_default_skill_level(char_map,skill,level);
/// @description
/// @param char_map
/// @param skill
/// @param level

var skills_map = argument0[? GAME_CHAR_SKILLS_MAP];
skills_map[? argument1] = argument2;
