///array_transpose(array)
var arr = argument0, arrLen = array_length_2d(arr, 0), temp;

for (var i = 0; i < arrLen; i++) {
    for (var j = 0; j <i; j++) {
      //swap element[i,j] and element[j,i]
      temp = arr[@i,j];
      arr[@i,j] = arr[@j,i];
      arr[@j,i] = temp;
    }
  }
