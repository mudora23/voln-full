///settings_map_apply();
/// @description
if settings_map[? SETTINGS_FULLSCREEN]
    window_set_fullscreen(true);
else
{
	window_set_fullscreen(false);
}

if obj_control.settings_map_display_reset_needed
{
	if !settings_map[? SETTINGS_FULLSCREEN]
		default_window_setting();
		
	if settings_map[? SETTINGS_AA] != 0 && display_aa % (settings_map[? SETTINGS_AA] * 2) < settings_map[? SETTINGS_AA]
	    settings_map[? SETTINGS_AA] = 0;
		
	obj_control.settings_map_display_reset_needed = false;
}
//display_reset(settings_map[? SETTINGS_AA], settings_map[? SETTINGS_VSYNC]);


//show_debug_message("display_aa: "+string(display_aa));
