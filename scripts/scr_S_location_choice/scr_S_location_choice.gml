///scr_CS_location_choice();
/*
with obj_control
{

	scene_choice_add("Hump",scr_S_hump);
	if ally_exists(NAME_ZEYD)
		scene_choice_add("Talk to Zeyd",scr_S_talk_to_zeyd);
	
	if game_map[? VAR_HOURS_SINCE_LAST_REST] > 5
		scene_choice_add("Rest",scr_S_stay);
	else
		scene_choice_add("[?]Rest",scr_S_stay);
	
	if game_map[? VAR_HOURS_SINCE_LAST_SLEEP] > 10
		scene_choice_add("Sleep",scr_S_sleep);
	else
		scene_choice_add("[?]Sleep",scr_S_sleep);
		
	if game_map[? VAR_HOURS_SINCE_LAST_SLEEP] > 10
		scene_choice_add("Sleep with...",scr_S_sleep_with);
	else
		scene_choice_add("[?]Sleep with...",scr_S_sleep_with);
		

	scene_choice_add("Move",scr_S_move);

}