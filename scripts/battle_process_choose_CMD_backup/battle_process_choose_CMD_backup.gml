///battle_process_choose_CMD(CMD);
/// @description
/// @param CMD
with obj_control
{
	scene_choices_list_clear();
	obj_control.enemies_choice_enable = true;
	
	var char_map = battle_get_current_action_char();
	var char_CMD = script_get_index_ext(argument[0]);
	
	
	if char_map[? GAME_CHAR_OWNER] == OWNER_PLAYER
	{
		var enemy_list = var_map[? VAR_ENEMY_LIST];
		for(var i = 0; i < ds_list_size(enemy_list);i++)
		{
			var enemy = enemy_list[| i];
			if battle_char_get_combat_capable(enemy) && !battle_char_get_buff_level(enemy,BUFF_HIDE)
				scene_choiceCMD_add(enemy[? GAME_CHAR_DISPLAYNAME],char_CMD,i);
		}
		scene_choiceCMD_add("Cancel",battle_process_player);
		
		//show_debug_message("------Battle 'choose (Player) CMD script' is executed!------");
		//show_debug_message("------Battle 'choose (Player) CMD script' char_CMD script is "+string(argument[0]));
	}
	
	
	
	else
	{
		
		var player = var_map[? VAR_PLAYER];
		if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
			var choosing_list = var_map[? VAR_ALLY_LIST];
		else
			var choosing_list = var_map[? VAR_ENEMY_LIST];
			
		var possible_list = ds_list_create();
		
		for(var i = 0; i < ds_list_size(choosing_list);i++)
		{
			var choosing_list_char = choosing_list[| i];
			if battle_char_get_combat_capable(choosing_list_char) && !battle_char_get_buff_level(choosing_list_char,BUFF_HIDE)
				ds_list_add(possible_list,i);
		}
		
		if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
		{
			var player_map = var_map[? VAR_PLAYER];
			if battle_char_get_combat_capable(player_map) && !battle_char_get_buff_level(player_map,BUFF_HIDE)
				ds_list_add(possible_list,ds_list_size(choosing_list));
		}
		
		if ds_list_size(possible_list) == 0
		{
			battle_process_wait_CMD();
			//if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY || char_map[? GAME_CHAR_OWNER] == OWNER_ALLY
				//scene_script_execute_add_unique(0,battle_process_AI);
			//else
				//scene_script_execute_add_unique(0,battle_process_player);
		}
		else
		{
			if (char_map[? GAME_CHAR_OWNER] == OWNER_ALLY && percent_chance(80)) ||
			   (char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY && percent_chance(100/battle_player_ally_get_combat_capable_number()))
			{
				if char_CMD == script_get_name(battle_process_flirt_atk_CMD)
				{
					if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
					{
						var the_char = battle_char_list_get_highest_lust_capable_char(var_map[?VAR_ALLY_LIST]);
						if the_char == noone// && is_real(the_char) && battle_char_get_combat_capable(player) && (the_char == noone || (char_get_lust_max(the_char) - the_char[? GAME_CHAR_LUST]) < (char_get_lust_max(player) - player[? GAME_CHAR_LUST]))
							script_execute(char_CMD,ds_list_size(var_map[? VAR_ALLY_LIST]));
						else if the_char != noone && is_real(the_char)
							script_execute(char_CMD,battle_char_list_get_highest_lust_capable_char_i(var_map[? VAR_ALLY_LIST]));
						else
							battle_process_wait_CMD();

							
					}
					if char_map[? GAME_CHAR_OWNER] == OWNER_ALLY
					{
						var the_char = battle_char_list_get_highest_lust_capable_char(var_map[? VAR_ENEMY_LIST]);
						if the_char != noone && is_real(the_char)
							script_execute(char_CMD,battle_char_list_get_highest_lust_capable_char_i(var_map[? VAR_ENEMY_LIST]));
						else
							battle_process_wait_CMD();
					}
				
				}
				else
				{
				
					if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
					{

						var the_char = battle_char_list_get_lowest_hp_capable_char(var_map[? VAR_ALLY_LIST]);
						if the_char == noone// && is_real(the_char) && battle_char_get_combat_capable(player) && (the_char == noone || the_char[? GAME_CHAR_HEALTH] > player[? GAME_CHAR_HEALTH])
							script_execute(char_CMD,ds_list_size(var_map[? VAR_ALLY_LIST]));
						else if the_char != noone
							script_execute(char_CMD,battle_char_list_get_lowest_hp_capable_char_i(var_map[? VAR_ALLY_LIST]));
						else
							battle_process_wait_CMD();


					}
					if char_map[? GAME_CHAR_OWNER] == OWNER_ALLY
					{
						var the_char = battle_char_list_get_lowest_hp_capable_char(var_map[? VAR_ENEMY_LIST]);
						if the_char != noone && is_real(the_char)
							script_execute(char_CMD,battle_char_list_get_lowest_hp_capable_char_i(var_map[? VAR_ENEMY_LIST]));
						else
							battle_process_wait_CMD();
					}
				
				
				
				}
			
			
			
			}
			else
			{
				ds_list_shuffle(possible_list);
				script_execute(char_CMD,possible_list[| 0]);
			}
		}
		
		ds_list_destroy(possible_list);
		
		//show_debug_message("------Battle 'choose (AI) CMD script' is executed!------");
	}	
		
	/*for(var i = 0; i < ds_list_size(choosing_list);i++)
	{
		var char_map = choosing_list[| i];
	}*/
	
	

}
