///pet_add_char(name,unique?,level[optional]);
/// @description
/// @param name
/// @param unique?
/// @param level[optional]
if !pet_is_full()
{
	var pet_list = obj_control.var_map[? VAR_PET_LIST];
	if !argument[1] || !pet_exists(argument[0])
	{
		ds_list_add(pet_list,ds_map_create());
		var pet_index = ds_list_size(pet_list)-1;
		ds_list_mark_as_map(pet_list,pet_index);
	
		if argument_count > 2
			game_char_map_init_from_wiki(pet_list[| pet_index],argument[0],OWNER_ALLY,argument[2]);
		else
			game_char_map_init_from_wiki(pet_list[| pet_index],argument[0],OWNER_ALLY);


		char_stats_recalculate(pet_list[| pet_index]);
		char_skills_recalculate(pet_list[| pet_index]);
		char_set_info(pet_list[| pet_index],GAME_CHAR_TYPE,GAME_CHAR_PET);
	
		//game_char_map_add_missing_keys(pet_list[| pet_index],OWNER_ALLY);
		
		char_set_info(pet_list[| pet_index],GAME_CHAR_HEALTH,char_get_health_max(pet_list[| pet_index]));
		char_set_info(pet_list[| pet_index],GAME_CHAR_TORU,char_get_toru_max(pet_list[| pet_index]));
		char_set_info(pet_list[| pet_index],GAME_CHAR_STAMINA,char_get_stamina_max(pet_list[| pet_index]));
		
		return pet_list[| pet_index];

	}
	
	return noone;
}
else
{
	var storage_list = obj_control.var_map[? VAR_STORAGE_LIST];
	if !argument[1] || !storage_exists(argument[0])
	{
		ds_list_add(storage_list,ds_map_create());
		var storage_index = ds_list_size(storage_list)-1;
		ds_list_mark_as_map(storage_list,storage_index);
	
		if argument_count > 2
			game_char_map_init_from_wiki(storage_list[| storage_index],argument[0],OWNER_ALLY,argument[2]);
		else
			game_char_map_init_from_wiki(storage_list[| storage_index],argument[0],OWNER_ALLY);

		char_stats_recalculate(storage_list[| storage_index]);
		char_skills_recalculate(storage_list[| storage_index]);
		char_set_info(storage_list[| storage_index],GAME_CHAR_TYPE,GAME_CHAR_PET);
		//game_char_map_add_missing_keys(storage_list[| storage_index],OWNER_ALLY);
		
		char_set_info(storage_list[| storage_index],GAME_CHAR_HEALTH,char_get_health_max(storage_list[| storage_index]));
		char_set_info(storage_list[| storage_index],GAME_CHAR_TORU,char_get_toru_max(storage_list[| storage_index]));
		char_set_info(storage_list[| storage_index],GAME_CHAR_STAMINA,char_get_stamina_max(storage_list[| storage_index]));
		
		return storage_list[| storage_index];

	}	
	
	return noone;
}