///draw_menu_skill_tree(x1,y1,x2,y2);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2

/*
	skill_menu_yoffset = 500;
	skill_menu_xoffset = 300;
	skill_menu_zoom_ratio = 1;
*/

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var w = x2-x1;
var h = y2-y1;
var padding_w = 25;
var padding_h = 50;

draw_set_font(font_01n_reg);

// draw bg
draw_set_alpha(menu_open_bg_image_alpha);
draw_set_color(menu_idle_bg_colorscheme);
draw_rectangle(x1,y1,x2,y2, false);

draw_set_halign(fa_center);
draw_set_valign(fa_center);


skill_menu_flashing_ratio = approach(skill_menu_flashing_ratio,0,1/room_speed);
if skill_menu_flashing_ratio <= 0 skill_menu_flashing_ratio = 1;


var skill_key = ds_map_find_first(obj_control.skill_wiki_map);
while !is_undefined(skill_key)
{
	
	var skill_map = obj_control.skill_wiki_map[? skill_key];
	
	if ds_map_exists(skill_map,SKILLX) && ds_map_exists(skill_map,SKILLY)
	{
		var skill_spr = skill_map[? SKILLICON];
		var skill_x = skill_map[? SKILLX]*skill_menu_zoom_ratio;
		var skill_y = skill_map[? SKILLY]*skill_menu_zoom_ratio;
		var skill_xx = x1+skill_x+skill_menu_xoffset;
		var skill_yy = y1+skill_y+skill_menu_yoffset;
		var skill_xxx = skill_xx-sprite_get_width(skill_spr)/2*skill_menu_zoom_ratio;
		var skill_yyy = skill_yy-sprite_get_height(skill_spr)/2*skill_menu_zoom_ratio;
		//var skill_hover = mouse_over_area(skill_xx,skill_yy,sprite_get_width(skill_spr)*skill_menu_zoom_ratio,sprite_get_height(skill_spr)*skill_menu_zoom_ratio);
		//var skill_color = cond(skill_hover || char_get_skill_level(var_map_get(VAR_PLAYER),skill_key),c_white,c_gray);
		var skill_text = skill_map[? SKILLTEXT];
		var skill_text_col = skill_map[? SKILLTEXTCOL];
		var skill_req_list = skill_map[? SKILL_REQ_SKILL_LIST];
	
		for(var i = 0; i < ds_list_size(skill_req_list);i++)
		{
			var skill_connection_list_id = skill_req_list[| i];
			var skill_connection_list_map = obj_control.skill_wiki_map[? skill_connection_list_id];
			var skill_connection_list_spr = skill_connection_list_map[? SKILLICON];
			var skill_connection_list_x = skill_connection_list_map[? SKILLX]*skill_menu_zoom_ratio;
			var skill_connection_list_y = skill_connection_list_map[? SKILLY]*skill_menu_zoom_ratio;
			var skill_connection_list_xx = x1+skill_connection_list_x+skill_menu_xoffset;
			var skill_connection_list_yy = y1+skill_connection_list_y+skill_menu_yoffset;
			var skill_connection_list_xxx = skill_connection_list_xx-sprite_get_width(skill_connection_list_spr)/2*skill_menu_zoom_ratio;
			var skill_connection_list_yyy = skill_connection_list_yy-sprite_get_height(skill_connection_list_spr)/2*skill_menu_zoom_ratio;
			
			var skill_connection_list_dir = point_direction(skill_xx,skill_yy,skill_connection_list_xx,skill_connection_list_yy);
			var skill_connection_list_len = point_distance(skill_xx,skill_yy,skill_connection_list_xx,skill_connection_list_yy);
			var path_point_dis = 20;//*power(skill_menu_zoom_ratio,0.5);
		
			var skill_pointx = skill_xx;
			var skill_pointy = skill_yy;
		
			var total_dots = floor(skill_connection_list_len/path_point_dis);
			for(var j = 0 ; j < total_dots; j++)
			{
				skill_pointx += lengthdir_x(path_point_dis,skill_connection_list_dir);
				skill_pointy += lengthdir_y(path_point_dis,skill_connection_list_dir);
				if point_in_rectangle(skill_pointx,skill_pointy,x1,y1,x1+w,y1+h)
				{
					/*if char_get_skill_level(var_map_get(VAR_PLAYER),skill_connection_list_id) && char_get_skill_level(var_map_get(VAR_PLAYER),skill_key)
						var dot_alpha = 1;
					else*/
						var dot_alpha = ((1 - abs(skill_menu_flashing_ratio -  j/total_dots))-0.8)*10;


				
					draw_set_color(c_white);
					draw_set_alpha(0.2);
					draw_circle(skill_pointx,skill_pointy,2/**skill_menu_zoom_ratio*/,false);
				
					draw_set_color(skill_map[? SKILLTEXTCOLLIGHT]);
					draw_set_alpha(dot_alpha);
				
					if point_in_rectangle(skill_pointx,skill_pointy,x1,y1,x1+w,y1+h)
						if char_get_skill_level(var_map_get(VAR_PLAYER),skill_connection_list_id) >= skill_get_level_max(skill_connection_list_id)
							draw_circle(skill_pointx,skill_pointy,2/**skill_menu_zoom_ratio*/,false);
				}

				
			}
			
			draw_set_alpha(1);
			//draw_line_width_color(skill_yy,skill_yy,skill_connection_list_xxx,skill_connection_list_yyy,5,c_white,c_white);
		
		
		}
		
		
		
		
		
		
		var skill_spr = skill_map[? SKILLICON];
		if ds_map_exists(skill_map,SKILL_REQ_LEVEL_LIST) var skill_max_level = ds_list_size(skill_map[? SKILL_REQ_LEVEL_LIST]);
		else var skill_max_level = 1;
		
		var skill_hover = mouse_over_area(skill_xxx,skill_yyy,sprite_get_width(skill_spr)*skill_menu_zoom_ratio,sprite_get_height(skill_spr)*skill_menu_zoom_ratio);
		var skill_text_col = skill_map[? SKILLTEXTCOL];
		var skill_text_col_light = skill_map[? SKILLTEXTCOLLIGHT];
	
		var point_available = var_map_get(VAR_PLAYER_UNSPEND_SKILL_POINTS) > 0;
		var skill_requirement_met = skill_req_met(var_map_get(VAR_PLAYER),skill_key);
		var skill_learnt = char_get_skill_level(var_map_get(VAR_PLAYER),skill_key);
		var skill_level_met = !ds_map_exists(skill_map,SKILL_REQ_LEVEL_LIST) || (ds_map_exists(skill_map,SKILL_REQ_LEVEL_LIST) && ds_list_find_value(skill_map[? SKILL_REQ_LEVEL_LIST],skill_learnt) <= char_get_info(char_get_player(),GAME_CHAR_LEVEL));
		var skill_learnt_full = char_get_skill_level(var_map_get(VAR_PLAYER),skill_key) >= skill_max_level;
		var skill_available = skill_requirement_met && !skill_learnt_full && skill_level_met;
	
		if skill_available && point_available
			var skill_alpha = obj_control.flashing_alpha+0.5;
		else
			var skill_alpha = 1;
	

		if skill_requirement_met && (skill_learnt || skill_hover)
			var skill_color = skill_text_col_light;
		else
			var skill_color = skill_text_col;

		var skill_text = skill_map[? SKILLTEXT];
		var skill_req_list = skill_map[? SKILL_REQ_SKILL_LIST];

		draw_sprite_in_visible_area(skill_spr,0,skill_xxx,skill_yyy,skill_menu_zoom_ratio,skill_menu_zoom_ratio,skill_color,skill_alpha,argument0,argument1,argument2-argument0,argument3-argument1);
	
		if /*skill_max_level != 1 && */point_in_rectangle(skill_xxx,skill_yyy,x1+50,y1+50,x2-50,y2-50) && skill_key != SKILL_START
			draw_text_transformed_color(skill_xxx,skill_yyy,string(skill_learnt)+"/"+string(skill_max_level),skill_menu_zoom_ratio,skill_menu_zoom_ratio,0,c_white,c_white,c_white,c_white,skill_alpha);
	
		if !skill_requirement_met draw_sprite_in_visible_area(spr_lock,0,skill_xxx,skill_yyy,sprite_get_width(skill_spr)/sprite_get_width(spr_lock)*skill_menu_zoom_ratio,sprite_get_height(skill_spr)/sprite_get_height(spr_lock)*skill_menu_zoom_ratio,c_white,skill_alpha*0.45,argument0,argument1,argument2-argument0,argument3-argument1);



		if skill_available && point_available && skill_hover && action_button_pressed_get()
		{
			char_add_default_skill_level(char_get_player(),skill_key,1);
			var_map_set(VAR_PLAYER_UNSPEND_SKILL_POINTS,var_map_get(VAR_PLAYER_UNSPEND_SKILL_POINTS)-1);
			
			// battlelog
			var char_map = char_get_player();
			var battlelog_pieces_list = ds_list_create();
			ds_list_add(battlelog_pieces_list,text_add_markup(char_map[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,TAG_COLOR_ALLY_NAME));
			ds_list_add(battlelog_pieces_list,text_add_markup(" has just learnt ",TAG_FONT_N,TAG_COLOR_NORMAL));
			ds_list_add(battlelog_pieces_list,text_add_markup(skill_get_displayname(skill_key)+" (LV"+string(ds_map_find_value(char_map[? GAME_CHAR_SKILLS_MAP],skill_key))+")",TAG_FONT_N,TAG_COLOR_EFFECT));
			var battle_log = string_append_from_list(battlelog_pieces_list);
			battlelog_add(battle_log);
			ds_list_destroy(battlelog_pieces_list);
		}

		if skill_hover && mouse_over_area(x1,y1,w,h)
		{
			tooltip = skill_get_tooltip(skill_key);
			draw_set_menu_floating_tooltip(skill_get_floating_tooltip(skill_key),skill_xx,skill_yyy,fa_center,fa_bottom,true);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		//draw_sprite_in_visible_area(skill_spr,0,skill_xxx,skill_yyy,skill_menu_zoom_ratio,skill_menu_zoom_ratio,skill_color,1,argument0,argument1,argument2-argument0,argument3-argument1);
	}
	skill_key = ds_map_find_next(obj_control.skill_wiki_map,skill_key);
}		

	
	
	
		
		
	
	
	
	
		
		
if obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS] > 0
{
	draw_set_font(font_01n_reg);
	draw_set_halign(fa_right);
	draw_set_valign(fa_bottom);
	draw_set_color(COLOR_EFFECT);
	
	draw_text_transformed(x2-20,y2-20,"Unused skill points: "+string(obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS]),1,1,0);	
}

if mouse_check_button_pressed(mb_left) && mouse_over_area(argument0,argument1,argument2-argument0,argument3-argument1)
	skill_menu_holding = true;
	
if !mouse_check_button(mb_left)
	skill_menu_holding = false;
	
if skill_menu_holding
{
	skill_menu_xoffset_target -= obj_control.mouse_xprevious - mouse_x;
	skill_menu_yoffset_target -= obj_control.mouse_yprevious - mouse_y;
}
	

skill_menu_xoffset = skill_menu_xoffset_target;
skill_menu_yoffset = skill_menu_yoffset_target;

//skill_menu_zoom_ratio = 1;
//skill_menu_holding = false;
		
		
if mouse_over_area(x1,y1,w,h)
{
	if mouse_wheel_down() skill_menu_zoom_ratio = clamp(skill_menu_zoom_ratio * 0.8, skill_menu_zoom_ratio_default*0.8*0.8*0.8, skill_menu_zoom_ratio_default/0.8/0.8/0.8);
	if mouse_wheel_up() skill_menu_zoom_ratio = clamp(skill_menu_zoom_ratio / 0.8, skill_menu_zoom_ratio_default*0.8*0.8*0.8, skill_menu_zoom_ratio_default/0.8/0.8/0.8);
}







/*
var xx1 = room_width/3;
var yy1 = room_height/3;
var xx2 = room_width/3*2;
var yy2 = room_height/3*2;
var the_len = point_distance(xx1,yy1,xx2,yy2);
var the_dir = point_direction(xx1,yy1,xx2,yy2);
var point_dis = 40 * skill_menu_zoom_ratio;

repeat(floor(the_len/point_dis))
{
	xx1 += lengthdir_x(point_dis,the_dir);
	yy1 += lengthdir_y(point_dis,the_dir);
	draw_sprite_in_visible_area(spr_map_path_point,0,xx1-sprite_get_width(spr_map_path_point)/2*skill_menu_zoom_ratio,yy1-sprite_get_height(spr_map_path_point)/2*skill_menu_zoom_ratio,skill_menu_zoom_ratio,skill_menu_zoom_ratio,c_white,1,x1,y1,w,h);
}
*/