///story_execute(marked_text);
/// @description return a list of two strings: [raw_text, marked_text]
/// @param marked_text
/*

{scriptvar}
    Returns its value. If the scriptvar is invalid, “{scriptvar}” will be displayed.

{[prefix][prefix2]scriptvar[suffix][suffix2]}
    If {scriptvar} has something,
        [prefix] will be added in the beginning of the value,
		[suffix] will be added in the end of the value.
	Else,
        [prefix2] will be added in the beginning of the value,
		[suffix2] will be added in the end of the value.

Note that all prefixes and suffixes are optional. Either prefixes or suffixes can be empty. They can ONLY contain text; NO scriptvars.

*/

var marked_text = argument[0];
var text = marked_text_get_raw(marked_text);

//show_debug_message("Before story_execute...");
//show_debug_message("marked_text:"+string(marked_text));
//show_debug_message("raw_text:"+string(text));

var start_posi = noone;
var prefix_start_posi = noone;
var prefix_end_posi = noone;
var prefix2_start_posi = noone;
var prefix2_end_posi = noone;
var scriptvar_string = "";
var suffix_start_posi = noone;
var suffix_end_posi = noone;
var suffix2_start_posi = noone;
var suffix2_end_posi = noone;
var end_posi = noone;

var prefix_value = "";
var prefix2_value = "";
var scriptvar_value = "";
var suffix_value = "";
var suffix2_value = "";

var string_insert_posi = 0;

for(var posi = 1; posi <= string_length(text); posi++)
{
    var the_char = string_char_at(text,posi);
    
    
    if the_char == "{" && start_posi == noone
    {
		//show_debug_message("'{' variable starts at"+string(posi));
        start_posi = posi;
    }
    else if the_char == "}" && end_posi == noone
    {
		//show_debug_message("'}' variable ends  at"+string(posi));
        end_posi = posi;
        
        // checking if valid
		//show_debug_message("checking if the variable is valid: scriptvar_"+scriptvar_string);
        if asset_get_type("scriptvar_"+scriptvar_string) == asset_script
        {
			//show_debug_message("The variable is valid: scriptvar_"+scriptvar_string);
            if prefix_start_posi > 0 && prefix_end_posi > 0 && prefix_start_posi < prefix_end_posi - 1
                var prefix_value = string_copy(text, prefix_start_posi+1, prefix_end_posi - prefix_start_posi - 1);
            
            if prefix2_start_posi > 0 && prefix2_end_posi > 0 && prefix2_start_posi < prefix2_end_posi - 1
                var prefix2_value = string_copy(text, prefix2_start_posi+1, prefix2_end_posi - prefix2_start_posi - 1);
            
            var scriptvar_value = script_execute(asset_get_index("scriptvar_"+scriptvar_string));
            
            if suffix_start_posi > 0 && suffix_end_posi > 0 && suffix_start_posi < suffix_end_posi - 1
                var suffix_value = string_copy(text, suffix_start_posi+1, suffix_end_posi - suffix_start_posi - 1);
            
            if suffix2_start_posi > 0 && suffix2_end_posi > 0 && suffix2_start_posi < suffix2_end_posi - 1
                var suffix2_value = string_copy(text, suffix2_start_posi+1, suffix2_end_posi - suffix2_start_posi - 1);
			
			
			//show_debug_message("prefix_value: "+prefix_value);
			//show_debug_message("prefix2_value: "+prefix2_value);
			//show_debug_message("scriptvar_value: "+string(scriptvar_value));
			//show_debug_message("suffix_value: "+suffix_value);
			//show_debug_message("suffix2_value: "+suffix2_value);
			
			if (is_string(scriptvar_value) && scriptvar_value != " " && scriptvar_value != "") || scriptvar_value
			{
				prefix2_value = "";
				suffix2_value = "";
			}
			else
			{
				prefix_value = "";
				suffix_value = "";
			}
			if !is_string(scriptvar_value) scriptvar_value = "";
			
			
			
			
            text = string_delete(text, start_posi, 1);
            marked_text = string_delete(marked_text, marked_text_get_posi(start_posi), 3);
            //text_tag_map_delete_tags(text_tag_map,start_posi,start_posi);
            //text_newmarkups_delete_tags(marked_text,posi);
            //text_tag_map_tag_offset(text_tag_map,start_posi,-1);
			
	        prefix_start_posi--;
	        prefix_end_posi--;
	        prefix2_start_posi--;
	        prefix2_end_posi--;
	        suffix_start_posi--;
	        suffix_end_posi--;
	        suffix2_start_posi--;
	        suffix2_end_posi--;
	        end_posi--;

  
            if prefix_start_posi > 0 && prefix_end_posi > 0 && prefix_start_posi < prefix_end_posi
            {
                text = string_delete(text, prefix_start_posi, prefix_end_posi-prefix_start_posi+1);
				text = string_insert(prefix_value, text, prefix_start_posi);
                var posi_offset_amount = -(prefix_end_posi - prefix_start_posi + 1) + string_length(prefix_value);
                
                
                marked_text = string_delete(marked_text, marked_text_get_posi(prefix_start_posi), 3);
                marked_text = string_delete(marked_text, marked_text_get_posi(prefix_end_posi-1), 3);
				if prefix_value == ""
					marked_text = string_delete(marked_text, marked_text_get_posi(prefix_start_posi), 3*(prefix_end_posi - prefix_start_posi-1));
				//marked_text = text_markups_stretch(marked_text,prefix_end_posi,posi_offset_amount+2);
                
				//text_tag_map_delete_tags(text_tag_map,prefix_start_posi,prefix_start_posi+1);
                //text_newmarkups_delete_tags(marked_text,prefix_start_posi);
				//text_tag_map_delete_tags(text_tag_map,prefix_end_posi,prefix_end_posi+1);
                //text_newmarkups_delete_tags(marked_text,prefix_end_posi);
				//text_tag_map_tag_offset(text_tag_map,prefix_start_posi,-1);
				//text_tag_map_tag_offset(text_tag_map,prefix_end_posi-1,-1);
                //text_tag_map_tag_stretch(text_tag_map,prefix_end_posi,posi_offset_amount+2);
                

                prefix2_start_posi += posi_offset_amount;
                prefix2_end_posi += posi_offset_amount;
                suffix_start_posi += posi_offset_amount;
                suffix_end_posi += posi_offset_amount;
                suffix2_start_posi += posi_offset_amount;
                suffix2_end_posi += posi_offset_amount;
                end_posi += posi_offset_amount;
				
				string_insert_posi += posi_offset_amount;
                
            }
            
            if prefix2_start_posi > 0 && prefix2_end_posi > 0 && prefix2_start_posi < prefix2_end_posi
            {
                text = string_delete(text, prefix2_start_posi, prefix2_end_posi-prefix2_start_posi+1);
				text = string_insert(prefix2_value, text, prefix2_start_posi);
                var posi_offset_amount = -(prefix2_end_posi - prefix2_start_posi + 1) + string_length(prefix2_value);
                
                
                marked_text = string_delete(marked_text, marked_text_get_posi(prefix2_start_posi), 3);
                marked_text = string_delete(marked_text, marked_text_get_posi(prefix2_end_posi-1), 3);
				if prefix2_value == ""
					marked_text = string_delete(marked_text, marked_text_get_posi(prefix2_start_posi), 3*(prefix2_end_posi - prefix2_start_posi-1));
                //marked_text = text_markups_stretch(marked_text,prefix2_end_posi,posi_offset_amount+3);
                
				//text_tag_map_delete_tags(text_tag_map,prefix2_start_posi,prefix2_start_posi+1);
				//text_tag_map_delete_tags(text_tag_map,prefix2_end_posi,prefix2_end_posi+1);
				//text_tag_map_tag_offset(text_tag_map,prefix2_start_posi,-1);
				//text_tag_map_tag_offset(text_tag_map,prefix2_end_posi-1,-1);
                //text_tag_map_tag_stretch(text_tag_map,prefix2_end_posi,posi_offset_amount+2);
				
				
                suffix_start_posi += posi_offset_amount;
                suffix_end_posi += posi_offset_amount;
                suffix2_start_posi += posi_offset_amount;
                suffix2_end_posi += posi_offset_amount;
                end_posi += posi_offset_amount;
				
				string_insert_posi += posi_offset_amount;

            }
            
			////show_debug_message("Trying to replace the following variable: "+string_copy(text,string_insert_posi,string_length(scriptvar_string)));
            
			if string_copy(text,string_insert_posi,string_length(scriptvar_string)) == scriptvar_string
            {
				////show_debug_message("Deleting: "+string_copy(text, string_insert_posi, string_length(scriptvar_string)));
                text = string_delete(text, string_insert_posi, string_length(scriptvar_string));
				////show_debug_message("Inserting: "+scriptvar_value);
                text = string_insert(scriptvar_value, text, string_insert_posi);
				////show_debug_message("New story chunk: "+text);
                var posi_offset_amount = -string_length(scriptvar_string) + string_length(scriptvar_value);
                
				////show_debug_message("Running text_tag_map_tag_stretch; amount: "+string(posi_offset_amount));
                //text_tag_map_tag_stretch(text_tag_map,string_insert_posi+string_length(scriptvar_string)+1,posi_offset_amount);
                marked_text = text_markups_stretch(marked_text,string_insert_posi+string_length(scriptvar_string)-1,posi_offset_amount);
                
                suffix_start_posi += posi_offset_amount;
                suffix_end_posi += posi_offset_amount;
                suffix2_start_posi += posi_offset_amount;
                suffix2_end_posi += posi_offset_amount;
                end_posi += posi_offset_amount;
            }
            
            if suffix_start_posi > 0 && suffix_end_posi > 0 && suffix_start_posi < suffix_end_posi
            {
                text = string_delete(text, suffix_start_posi, suffix_end_posi-suffix_start_posi+1);
				text = string_insert(suffix_value, text, suffix_start_posi);
                var posi_offset_amount = -(suffix_end_posi - suffix_start_posi + 1) + string_length(suffix_value);
                
				//text_tag_map_delete_tags(text_tag_map,suffix_start_posi,suffix_start_posi+1);
				//text_tag_map_delete_tags(text_tag_map,suffix_end_posi,suffix_end_posi+1);
				//text_tag_map_tag_offset(text_tag_map,suffix_start_posi,-1);
				//text_tag_map_tag_offset(text_tag_map,suffix_end_posi-1,-1);
                //text_tag_map_tag_stretch(text_tag_map,suffix_end_posi,posi_offset_amount+2);
				
                marked_text = string_delete(marked_text, marked_text_get_posi(suffix_start_posi), 3);
                marked_text = string_delete(marked_text, marked_text_get_posi(suffix_end_posi-1), 3);
				if suffix_value == ""
					marked_text = string_delete(marked_text, marked_text_get_posi(suffix_start_posi), 3*(suffix_end_posi - suffix_start_posi-1));
                //marked_text = text_markups_stretch(marked_text,suffix_end_posi,posi_offset_amount+2);
                
                suffix2_start_posi += posi_offset_amount;
                suffix2_end_posi += posi_offset_amount;
                end_posi += posi_offset_amount;
                
            }
            
            if suffix2_start_posi > 0 && suffix2_end_posi > 0 && suffix2_start_posi < suffix2_end_posi
            {
                text = string_delete(text, suffix2_start_posi, suffix2_end_posi-suffix2_start_posi+1);
				text = string_insert(suffix2_value, text, suffix2_start_posi);
                var posi_offset_amount = -(suffix2_end_posi - suffix2_start_posi + 1) + string_length(suffix2_value);
                
				//text_tag_map_delete_tags(text_tag_map,suffix2_start_posi,suffix2_start_posi+1);
				//text_tag_map_delete_tags(text_tag_map,suffix2_end_posi,suffix2_end_posi+1);
				//text_tag_map_tag_offset(text_tag_map,suffix2_start_posi,-1);
				//text_tag_map_tag_offset(text_tag_map,suffix2_end_posi-1,-1);
                //text_tag_map_tag_stretch(text_tag_map,suffix2_end_posi,posi_offset_amount+2);
                marked_text = string_delete(marked_text, marked_text_get_posi(suffix2_start_posi), 3);
                marked_text = string_delete(marked_text, marked_text_get_posi(suffix2_end_posi-1), 3);
				if suffix2_value == ""
					marked_text = string_delete(marked_text, marked_text_get_posi(suffix2_start_posi), 3*(suffix2_end_posi - suffix2_start_posi-1));
                //marked_text = text_markups_stretch(marked_text,suffix2_end_posi,posi_offset_amount+2);
                
                end_posi += posi_offset_amount;
                
            }
    
            text = string_delete(text, end_posi, 1);
            marked_text = string_delete(marked_text, marked_text_get_posi(end_posi), 3);
            
            //text_tag_map_delete_tags(text_tag_map,end_posi,end_posi+1);
            //text_tag_map_tag_offset(text_tag_map,end_posi+1,-1);
            
            posi = end_posi-1;
        }
        
        // reset
        var start_posi = noone;
        var prefix_start_posi = noone;
        var prefix_end_posi = noone;
        var prefix2_start_posi = noone;
        var prefix2_end_posi = noone;
        var scriptvar_string = "";
        var suffix_start_posi = noone;
        var suffix_end_posi = noone;
        var suffix2_start_posi = noone;
        var suffix2_end_posi = noone;
        var end_posi = noone;
        
        var prefix_value = "";
        var prefix2_value = "";
        var scriptvar_value = "";
        var suffix_value = "";
        var suffix2_value = "";
        
        var string_insert_posi = 0;
    }
    else if the_char == "["
    {
        if scriptvar_string == ""
        {
            if prefix_start_posi == noone prefix_start_posi = posi;
            else if prefix2_start_posi == noone prefix2_start_posi = posi;
        }
        else
        {
            if suffix_start_posi == noone suffix_start_posi = posi;
            else if suffix2_start_posi == noone suffix2_start_posi = posi;
        }
    }
    else if the_char == "]"
    {
        if scriptvar_string == ""
        {
            if prefix_end_posi == noone prefix_end_posi = posi;
            else if prefix2_end_posi == noone prefix2_end_posi = posi;
        }
        else
        {
            if suffix_end_posi == noone suffix_end_posi = posi;
            else if suffix2_end_posi == noone suffix2_end_posi = posi;
        }
    }
    else if start_posi != noone &&
	        (prefix_start_posi == noone || prefix_end_posi != noone) &&
			(prefix2_start_posi == noone || prefix2_end_posi != noone) &&
			(suffix_start_posi == noone || suffix_end_posi != noone) &&
			(suffix2_start_posi == noone || suffix2_end_posi != noone)
			
    {
        scriptvar_string += string_no_whitespace(the_char);
		
		if string_insert_posi == 0 && string_no_whitespace(the_char) != ""
			var string_insert_posi = posi-1;
    }


}

//show_debug_message("After story_execute...");
//show_debug_message("marked_text:"+string(marked_text));
//show_debug_message("raw_text:"+string(text));

var return_list = ds_list_create();
ds_list_add(return_list,text,marked_text);
return return_list;

