///sprite_get_index_ext(sprite_in_any_form);
/// @description
/// @param sprite_in_any_form
if is_string(argument0)
{
	if asset_get_type(argument0) == asset_sprite
		return asset_get_index(argument0);
	else
		return noone;
}
else
{
	if sprite_exists(argument0)
		return argument0;
	else
		return noone;
	
}