///battle_char_list_get_combat_capable_random_index(char_list);
/// @description
/// @pram char_list

var possible_index_list = ds_list_create();

for(var i = 0 ; i < ds_list_size(argument0);i++)
{
	if battle_char_get_combat_capable(argument0[| i])
		ds_list_add(possible_index_list,i);	
}

if ds_list_size(possible_index_list) > 0
{
	ds_list_shuffle(possible_index_list);
	var the_i = possible_index_list[| 0];
	ds_list_destroy(possible_index_list);
	return the_i;
}
else
{
	ds_list_destroy(possible_index_list);
	return noone;
}