///battle_char_get_buff_level(char,buff);
/// @description
/// @param char
/// @param buff
var char_map = argument0;
var char_buff_list = char_map[? GAME_CHAR_BUFF_LIST];
var char_buff_level_list = char_map[? GAME_CHAR_BUFF_LEVEL_LIST];
var char_buff = argument1;
var char_buff_index = ds_list_find_index(char_buff_list,char_buff);
if char_buff_index >= 0
	return char_buff_level_list[| char_buff_index];
else
	return 0;