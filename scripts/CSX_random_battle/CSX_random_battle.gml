///S_random_battle();
/// @description - encounter a random battle or not?

with obj_control
{
    //scene_sprites_list_clear_except("bg",VAR_SPRITE_LIST_FADE_SPEED);
	//scene_cg_sprites_list_clear();
	
    //scene_bg_location(location_get_current());

    var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
    var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
        ds_list_clear(enemy_list);
    
    var average_level = battle_char_list_get_average_level_ext(ally_list,obj_control.var_map[? VAR_PLAYER]);
    var player_ally_number = ally_get_number()+1;
    var enemy_number = player_ally_number+choose(-1,0,0,0,1);

    var has_random_battle = false;
	var has_random_event = false;
    var rng = irandom_range(1,100);
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
    
	
	
	////show_debug_message("random event checking... at "+string(location_get_current()));
	
	
	
	/*if true
	{
		
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
		
	}
	
	
	
	
	
	
	
	
	
	else */if location_get_current() == LOCATION_1
    {
        
    }
    else if location_get_current() == LOCATION_2
    {
        
        if rng <= 40
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 40+30/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+30/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+30/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_3
    {
        
        if rng <= 50
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 50+30/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 50+30/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 50+30/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_4
    {
        
        if rng <= 50
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 50+30/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 50+30/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 50+30/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_5
    {
        
        if rng <= 50
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 50+30/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 50+30/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 50+30/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_6
    {
        
        if rng <= 40
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 40+50/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+50/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+50/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_7
    {
        
        if rng <= 25
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 25+65/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 25+65/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 25+65/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_8
    {
        
        if rng <= 75
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 75+20/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 75+20/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 75+20/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_9
    {
        
        if rng <= 40
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 40+50/4*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+50/4*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+50/4*3 && average_level >= 5
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
        else if rng <= 40+50/4*4
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_10
    {
        create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
        has_random_battle = true;
    }
    else if location_get_current() == LOCATION_11
    {
        
        if rng <= 70
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 40+50/4*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+50/4*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+50/4*3 && average_level >= 5
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
        else if rng <= 40+50/4*4
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_12
    {
        
        if rng <= 70
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 40+50/4*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+50/4*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+50/4*3 && average_level >= 5
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
        else if rng <= 40+50/4*4
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_13
    {
        
        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 80
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GREEN_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_14
    {
        
        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 95
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_15
    {
        
        create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
        has_random_battle = true;

    }
    else if location_get_current() == LOCATION_16
    {
        
        if rng <= 50
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_17
    {
        
        if rng <= 15
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 90
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_18
    {
        
    }
    else if location_get_current() == LOCATION_19
    {
        
        if rng <= 20
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 20+70/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_RED_ORK);
			has_random_battle = true;
        }
        else if rng <= 20+70/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 20+70/3*3 && average_level >= 5
        {
			create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
            has_random_battle = true;
        }


    }
    else if location_get_current() == LOCATION_20
    {
        
        if rng <= 15
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_21
    {
        
        if rng <= 85
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_22
    {
        
        if rng <= 35
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_23
    {
        
        if rng <= 45
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 95
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_24
    {
        
        if rng <= 45
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 95
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_25
    {
        
        create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
        has_random_battle = true;
    }
    else if location_get_current() == LOCATION_26
    {
        
        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true;   
        }
        else if rng <= 50
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
			has_random_battle = true;
        }
        else
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_27
    {
        
        if rng <= 45
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 95
        {
            create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_28
    {

        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 80
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
        }

    }
    else if location_get_current() == LOCATION_29
    {

        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 80
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_30
    {


    }
    else if location_get_current() == LOCATION_31
    {

        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 80
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
			has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_32
    {

        if rng <= 25
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 25+75/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 25+75/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 25+75/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_33
    {

        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 5+80/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 5+80/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 5+80/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_34
    {

        if rng <= 10
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 10+95/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 10+95/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 10+95/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_35
    {

        if rng <= 5
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 5+95/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 5+95/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 5+95/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_36
    {

        if rng <= 70
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 70+85/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 70+85/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 70+85/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_37
    {

    }
    else if location_get_current() == LOCATION_38
    {

    }
    else if location_get_current() == LOCATION_39
    {
        if rng <= 45
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 45+90/3*1
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 45+90/3*2
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
        else if rng <= 45+90/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_40
    {

        if rng <= 50
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 50+50/3*1
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 50+50/3*2
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
        else if rng <= 50+50/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP,NAME_ELF_BANDITS);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_41
    {

        if rng <= 50
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 50+45/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
        else if rng <= 50+45/3*2
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 50+45/3*1
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_42
    {
        if rng <= 0+100/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 0+100/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 0+100/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_43
    {
        if rng <= 40
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 40+50/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+50/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+50/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_44
    {
        if rng <= 40
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 40+50/3*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 40+50/3*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+50/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_45
    {
		if qj_exists("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ") && inventory_item_count(ITEM_Salo_wurm) < 5 && percent_chance(50)
		{
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
		}
        else if rng <= 20
        {
            if CSX_travel_scene_execute()
				has_random_event = true;  
        }
        else if rng <= 20+75/4*1 && average_level >= 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP,NAME_ELF_BANDITS);
			has_random_battle = true;
        }
        else if rng <= 20+75/4*2 && average_level >= 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 20+75/4*3 && average_level >= 5
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
        else if rng <= 20+75/4*4
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_46
    {

        if qj_exists("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ") && inventory_item_count(ITEM_Salo_wurm) < 5 && percent_chance(50)
		{
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
		}
        else if rng <= 15
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 15+75/3*1
        {
            create_enemies(ally_get_number()+1,10,0,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 15+75/3*2
        {
			create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS);
            has_random_battle = true;
        }
        else if rng <= 15+75/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP,NAME_ELF_BANDITS);
            has_random_battle = true;
        }

    }
    else if location_get_current() == LOCATION_47
    {
        if qj_exists("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ") && inventory_item_count(ITEM_Salo_wurm) < 5 && percent_chance(50)
		{
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
		}
        else if rng <= 35
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 35+20/3*1 && average_level > 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 35+20/3*2 && average_level > 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 35+20/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_48
    {
        if qj_exists("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ") && inventory_item_count(ITEM_Salo_wurm) < 5 && percent_chance(50)
		{
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
		}
        else if rng <= 40
        {
            if CSX_travel_scene_execute()
				has_random_event = true; 
        }
        else if rng <= 40+15/3*1 && average_level > 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 40+15/3*2 && average_level > 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 40+15/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_49
    {
        if qj_exists("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ") && inventory_item_count(ITEM_Salo_wurm) < 5 && percent_chance(50)
		{
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
		}
        else if rng <= 25
        {
            if CSX_travel_scene_execute()
				has_random_event = true;
        }
        else if rng <= 25+40/3*1 && average_level > 10
        {
            create_enemies(ally_get_number()+1,10,0,NAME_GRUM,NAME_IMP);
			has_random_battle = true;
        }
        else if rng <= 25+40/3*2 && average_level > 10
        {
			create_enemies(ally_get_number()+1,10,0,NAME_GRUM);
            has_random_battle = true;
        }
        else if rng <= 25+40/3*3
        {
			create_enemies(ally_get_number()+1,10,0,NAME_IMP);
            has_random_battle = true;
        }
    }
    else if location_get_current() == LOCATION_50
    {

    }
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
    if has_random_battle 
	{
		////show_debug_message("Random battle starting...");
		//var_map_set(VAR_MODE,VAR_MODE_BATTLE);

		//var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
		//ds_list_clear(enemy_list);
        script_execute_add(0,battle_process_setup);
		//show_message("battle_process_setup in S_random_battle");
		return true;
	}
    else if has_random_event
	{
		return true;
	}
	else return false;
}


