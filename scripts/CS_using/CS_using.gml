///CS_using(char,itemName,apply_extra_effect[optional]);
/// @description
/// @param char
/// @param itemName
/// @param apply_extra_effect[optional]

with obj_control
{

	var char = argument[0];
	var itemName = argument[1];	
	if argument_count > 2 var apply_extra_effect = argument[2];
	else var apply_extra_effect = noone;
	var itemMap = obj_control.item_wiki_map[? itemName];
	inventory_delete_item(itemName,1);
	
	// sound
	voln_play_sfx(sfx_item_use);
	
	// current hp, lust, stamina, toru
	var current_hp = char[? GAME_CHAR_HEALTH];
	var current_lust = char[? GAME_CHAR_LUST];
	var current_stamina = char[? GAME_CHAR_STAMINA];
	var current_toru = char[? GAME_CHAR_TORU];
	
	// MSG
	if var_map_get(VAR_MODE) != VAR_MODE_BATTLE
	{
		if char == char_get_player() && itemMap[? ITEM_DES_PLAYER_USE] != ""
			show_popup_box(itemMap[? ITEM_DES_PLAYER_USE]);
		else if itemMap[? ITEM_DES_ALLY_USE] != ""
			show_popup_box(char[? GAME_CHAR_DISPLAYNAME] + itemMap[? ITEM_DES_ALLY_USE]);

	}
	
	if itemName == ITEM_TEEG_SALTED
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+350+char_get_health_max(char)*0.4,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_PORK_SALTED
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+500+char_get_health_max(char)*0.5,char_get_health_max(char));
	}
	if itemName == ITEM_BUN_STALE
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+200+char_get_health_max(char)*0.3,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_BEEF_JERKED
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+400+char_get_health_max(char)*0.4,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_BUN_FRESH
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+250+char_get_health_max(char)*0.3,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+25,char_get_vitality_max(char));
	}
	if itemName == ITEM_VEJ_LEATHER
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+50+char_get_health_max(char)*0.25,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+30,char_get_vitality_max(char));
	}
	if itemName == ITEM_CAKE_KOMAR
	{
		if percent_chance_forced(apply_extra_effect,80) var_map_add(PC_Height_N,1);
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_CAKE_FANCY
	{
		if percent_chance_forced(apply_extra_effect,60) var_map_add(PC_Height_N,1);
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_PIE_LEMMUS
	{
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Height_N,1);
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_PIE_FANCY
	{
		if percent_chance_forced(apply_extra_effect,25) var_map_add(PC_Height_N,1);
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+100+char_get_health_max(char)*0.25,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+35,char_get_vitality_max(char));
	}
	if itemName == ITEM_MILK_LUMKU
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+100+char_get_health_max(char)*0.25,char_get_health_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+10,char_get_vitality_max(char));
	}
	if itemName == ITEM_MILK_PUMMDUS
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+300+char_get_health_max(char)*0.35,char_get_health_max(char));
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+10,char_get_lust_max(char));
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+50,char_get_vitality_max(char));
	}
	if itemName == ITEM_MESO_EY_TONIC_POOR
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+200+char_get_health_max(char)*0.25,char_get_health_max(char));
	}
	if itemName == ITEM_MESO_EY_TONIC_GOOD
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+1000+char_get_health_max(char)*0.4,char_get_health_max(char));
	}
	if itemName == ITEM_MESO_EY_TONIC_EXQUISITE
	{
		char[? GAME_CHAR_HEALTH] = char_get_health_max(char);
	}
	if itemName == ITEM_VITALITY_TONIC_POOR
	{
		char[? GAME_CHAR_STAMINA] = min(char[? GAME_CHAR_STAMINA]+30,char_get_stamina_max(char));
	}
	if itemName == ITEM_VITALITY_TONIC_GOOD
	{
		char[? GAME_CHAR_STAMINA] = min(char[? GAME_CHAR_STAMINA]+60,char_get_stamina_max(char));
	}
	if itemName == ITEM_VITALITY_TONIC_EXQUISITE
	{
		char[? GAME_CHAR_STAMINA] = char_get_stamina_max(char);
	}
	if itemName == ITEM_DEENSKIP_TONIC_POOR
	{
		char[? GAME_CHAR_LUST] = clamp(char[? GAME_CHAR_LUST]-25,0,char_get_lust_max(char));
	}
	if itemName == ITEM_DEENSKIP_TONIC_GOOD
	{
		char[? GAME_CHAR_LUST] = clamp(char[? GAME_CHAR_LUST]-50,0,char_get_lust_max(char));
	}
	if itemName == ITEM_DEENSKIP_TONIC_EXQUISITE
	{
		char[? GAME_CHAR_LUST] = 0;
		// cures "Tempt"
	}
	if itemName == ITEM_KOMAR_GOOD
	{
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+5,char_get_lust_max(char));
	}
	if itemName == ITEM_KOMAR_EXQUISITE
	{
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+10,char_get_lust_max(char));
	}
	if itemName == ITEM_CANDY_FRU
	{
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+5,char_get_vitality_max(char));
	}
	if itemName == ITEM_CANDY_JUDDOVIN
	{
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+5,char_get_vitality_max(char));
	}
	if itemName == ITEM_GUM_MINT
	{
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+5,char_get_vitality_max(char));
	}
	if itemName == ITEM_GUM_FANCY
	{
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+5,char_get_vitality_max(char));
	}
	if itemName == ITEM_MUSK_EXTRACT_ELF
	{
		if percent_chance_forced(apply_extra_effect,50) var_map_add(PC_Nads_Size_N,0.1);
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+25,char_get_lust_max(char));
	}
	if itemName == ITEM_MUSK_EXTRACT_DUSKIE
	{
		if percent_chance_forced(apply_extra_effect,20) var_map_add(PC_Peen_SizeL_N,0.1);
		if percent_chance_forced(apply_extra_effect,20) var_map_add(PC_Peen_SizeG_N,0.1);
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+5,char_get_lust_max(char));
	}
	if itemName == ITEM_SPRUM_DUSKIE
	{
		if percent_chance_forced(apply_extra_effect,10) var_map_add(PC_Nads_Size_N,0.1);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Spurm_Quantity_N,0.1);
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+25,char_get_lust_max(char));
	}
	if itemName == ITEM_SPRUM_SELF
	{
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+5,char_get_lust_max(char));
	}
	if itemName == ITEM_REDBERRIES_JARRED
	{
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+5,char_get_lust_max(char));
	}
	if itemName == ITEM_MUSHROOM_KORKORT
	{
		if percent_chance_forced(apply_extra_effect,10) var_map_add(PC_Height_N,0.1);
		if percent_chance_forced(apply_extra_effect,10) var_map_add(PC_Weight_N,-0.1);
		if percent_chance_forced(apply_extra_effect,80) char[? GAME_CHAR_TORU] = clamp(char[? GAME_CHAR_TORU]-5,0,char_get_toru_max(char));
	}
	if itemName == ITEM_MUSHROOM_LIRROT
	{
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Peen_SizeL_N,0.5);
		if percent_chance_forced(apply_extra_effect,20) var_map_add(PC_Peen_SizeG_N,0.2);
		if percent_chance_forced(apply_extra_effect,10) char[? GAME_CHAR_VITALITY] = 0;
		if percent_chance_forced(apply_extra_effect,30) char[? GAME_CHAR_HEALTH] *= 0.4;
	}
	if itemName == ITEM_MUSHROOM_OLKE
	{
		if percent_chance_forced(apply_extra_effect,90) var_map_add(PC_Nads_Size_N,0.1);
		if percent_chance_forced(apply_extra_effect,10) var_map_add(PC_Nads_Size_N,0.2);
	}

	if itemName == ITEM_CF1 // CF1 Decrease Player_Muscle (40% chance); heal 30% of max hp; lust - 30
	{
		char_gain_lust(char,-30);
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Muscularity_N,-5);
	}
	if itemName == ITEM_CB1 // CB1 INCREASE Player_Lips (40% chance); heal 30% of max hp; lust + 30
	{
		char_gain_lust(char,30);
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Lips_Size_N,1);
	}
	if itemName == ITEM_CM1 // CM1 INCREASE Player_Muscle (40% chance); Heal 30% of max hp; lust + 10
	{
		char_gain_lust(char,10);
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Muscularity_N,5);
	}
	if itemName == ITEM_MB1 // MB1 Increase_PC_Bum_Size (40% chance); Heal 30% of max hp; lust = 0
	{
		char_set_info(char,GAME_CHAR_LUST,0);
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Bum_Size_N,1);
	}
	if itemName == ITEM_MM1 // MM1 INCREASE PC_HairLength (40% chance); Heal 30% of max hp; lust + 10
	{
		char_gain_lust(char,10);
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Hair_Length_N,1);
	}
	if itemName == ITEM_MM2 // MM2 Increase_PC_Peen_Size (40% chance); FULL HEAL
	{
		char_set_info(char,GAME_CHAR_HEALTH,char_get_health_max(char));
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Peen_SizeL_N,1);
	}
	if itemName == ITEM_YS1 // YS1 Increase_PC_Nads_Size (40% chance); lust = 0
	{
		char_set_info(char,GAME_CHAR_LUST,0);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Nads_Size_N,0.2);
	}
	if itemName == ITEM_YL1 // YL1 Increase_PC_Peen_Glans_Size (40% chance); Heal 30% of max hp
	{
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Peen_Glans_Size_N,0.2);
	}
	if itemName == ITEM_YM1 // YM1 Decrease_PC_Nads_Size (40% chance); Heal 30% of max hp; lust - 10
	{
		char_gain_lust(char,-10);
		char_gain_hp(char,char_get_health_max(char)*0.3);
		if percent_chance_forced(apply_extra_effect,40) var_map_add(PC_Nads_Size_N,-0.2);
	}
	
	
	
	if char == var_map_get(VAR_PLAYER)
	{
		var name_des = "";
		var use_des = itemMap[? ITEM_DES_PLAYER_USE];
	}
	else
	{
		var name_des = char[? GAME_CHAR_DISPLAYNAME];
		var use_des = itemMap[? ITEM_DES_ALLY_USE];
	}
	if use_des != ""
	{
		var battlelog_pieces_list = ds_list_create();
		ds_list_add(battlelog_pieces_list,text_add_markup(name_des,TAG_FONT_N,TAG_COLOR_ALLY_NAME));
		ds_list_add(battlelog_pieces_list,text_add_markup(use_des,TAG_FONT_N,TAG_COLOR_NORMAL));
		var battle_log = string_append_from_list(battlelog_pieces_list);
		battlelog_add(battle_log);
		ds_list_destroy(battlelog_pieces_list);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*if itemName == ITEM_Bread
	{
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+300,char_get_health_max(char));
		char[? GAME_CHAR_STAMINA] = min(char[? GAME_CHAR_STAMINA]+50,char_get_stamina_max(char));
	}

	if itemName == ITEM_Candy_FRU
	{
		char[? GAME_CHAR_STAMINA] = min(char[? GAME_CHAR_STAMINA]+40,char_get_stamina_max(char));
	}*/
	
	
	
	// current hp, lust, stamina, toru
	if var_map_get(VAR_MODE) == VAR_MODE_BATTLE
	{
		var delay = 0;
		if char[? GAME_CHAR_HEALTH] > current_hp
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(char[? GAME_CHAR_HEALTH] - current_hp)),COLOR_HEAL,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_HEAL;
		}
		else if char[? GAME_CHAR_HEALTH] < current_hp
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(current_hp - char[? GAME_CHAR_HEALTH])),COLOR_HEAL,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
		}
	
		if char[? GAME_CHAR_LUST] > current_lust
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(char[? GAME_CHAR_LUST] - current_lust)),COLOR_LUST,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_LUST;
		}
		else if char[? GAME_CHAR_LUST] < current_lust
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(current_lust - char[? GAME_CHAR_LUST])),COLOR_LUST,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_HEAL;
		}
	
		if char[? GAME_CHAR_STAMINA] > current_stamina
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(char[? GAME_CHAR_STAMINA] - current_stamina)),COLOR_STAMINA,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_STAMINA;
		}
		else if char[? GAME_CHAR_STAMINA] < current_stamina
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(current_stamina - char[? GAME_CHAR_STAMINA])),COLOR_STAMINA,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
		}
	
		if char[? GAME_CHAR_TORU] > current_toru
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(char[? GAME_CHAR_TORU] - current_toru)),COLOR_TORU,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_HEAL;
		}
		else if char[? GAME_CHAR_TORU] < current_toru
		{
			draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(current_toru - char[? GAME_CHAR_TORU])),COLOR_TORU,0,1);
			delay += 0.2;
			char[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
		}
	}
	
}
