///menu_exists();
/// @description
return instance_exists(obj_examine) || instance_exists(obj_menu) || instance_exists(obj_popup_box) || instance_exists(obj_player_offspring_customize) || instance_exists(obj_ai_selector) || obj_control.debug > 0;