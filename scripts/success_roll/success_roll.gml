///success_roll(number);
/// @description
/// @param number
var success = 0;
repeat(argument0)
    success+=percent_chance(50);
return success;
