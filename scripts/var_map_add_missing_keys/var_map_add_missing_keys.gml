///var_map_add_missing_keys();
/// @description
with obj_control
{
	
	// playtime
	#macro VAR_PLAYTIME "VAR_PLAYTIME"
	ds_map_add_unique(var_map,VAR_PLAYTIME,0);
	// In-game time and time counters
	#macro VAR_TIME_YEAR "VAR_TIME_YEAR"
	ds_map_add_unique(var_map,VAR_TIME_YEAR,412);
	#macro VAR_TIME_MONTH "VAR_TIME_MONTH"
	ds_map_add_unique(var_map,VAR_TIME_MONTH,9);
	#macro VAR_TIME_DAY "VAR_TIME_DAY"
	ds_map_add_unique(var_map,VAR_TIME_DAY,16);
	#macro VAR_TIME_HOUR "VAR_TIME_HOUR"
	ds_map_add_unique(var_map,VAR_TIME_HOUR,9);
	
	#macro VAR_HOURS_SINCE_LAST_SLEEP "VAR_HOURS_SINCE_LAST_SLEEP"
	ds_map_add_unique(var_map,VAR_HOURS_SINCE_LAST_SLEEP,999);
	#macro VAR_HOURS_SINCE_LAST_REST "VAR_HOURS_SINCE_LAST_REST"
	ds_map_add_unique(var_map,VAR_HOURS_SINCE_LAST_REST,999);
	#macro VAR_HOURS_SINCE_LAST_BLESSING "VAR_HOURS_SINCE_LAST_BLESSING"
	ds_map_add_unique(var_map,VAR_HOURS_SINCE_LAST_BLESSING,999);
	
	ds_map_add_unique(var_map,VAR_CHAR_ID,10000); #macro VAR_CHAR_ID "VAR_CHAR_ID"

	// music
	#macro VAR_BG_MUSIC "VAR_BG_MUSIC"
	ds_map_add_unique(var_map,VAR_BG_MUSIC,"");
	#macro VAR_BG_MUSIC_AUTO "VAR_BG_MUSIC_AUTO"
	ds_map_add_unique(var_map,VAR_BG_MUSIC_AUTO,true);

	// inventory
	#macro VAR_INVENTORY_LIST "VAR_INVENTORY_LIST"
	ds_map_add_unique_list(var_map,VAR_INVENTORY_LIST,ds_list_create());
	
	#macro VAR_INVENTORY_AMOUNT_LIST "VAR_INVENTORY_AMOUNT_LIST"
	ds_map_add_unique_list(var_map,VAR_INVENTORY_AMOUNT_LIST,ds_list_create());
	
	#macro VAR_INVENTORY_MAX_WEIGHT "VAR_INVENTORY_MAX_WEIGHT"
	ds_map_add_unique(var_map,VAR_INVENTORY_MAX_WEIGHT,100);
	
	// quest journal
	#macro VAR_QUEST_JOURNAL_LIST "VAR_QUEST_JOURNAL_LIST"
	ds_map_add_unique_list(var_map,VAR_QUEST_JOURNAL_LIST,ds_list_create());

	// current script of executing
		#macro VAR_SCRIPT "VAR_SCRIPT"
		ds_map_add_unique(var_map,VAR_SCRIPT,"");
	
		#macro VAR_SCRIPT_POSI "VAR_SCRIPT_POSI"
		ds_map_add_unique(var_map,VAR_SCRIPT_POSI,0);
		
		#macro VAR_SCRIPT_LABEL_MAP "VAR_SCRIPT_LABEL_MAP"
		ds_map_add_unique_map(var_map,VAR_SCRIPT_LABEL_MAP,ds_map_create());
		
		#macro VAR_CHUNK_MARKED_AS_READ_MAP "VAR_CHUNK_MARKED_AS_READ_MAP"
		ds_map_add_unique_map(var_map,VAR_CHUNK_MARKED_AS_READ_MAP,ds_map_create());
		
		#macro VAR_SCRIPT_PROC_NUMBER "VAR_SCRIPT_PROC_NUMBER"
		ds_map_add_unique(var_map,VAR_SCRIPT_PROC_NUMBER,0);
		
		#macro VAR_SCRIPT_PROC_STEP "VAR_SCRIPT_PROC_STEP"
		ds_map_add_unique(var_map,VAR_SCRIPT_PROC_STEP,0);
	
		#macro VAR_SCRIPT_DIALOG_SURFACE "VAR_SCRIPT_DIALOG_SURFACE"
		ds_map_add_unique(var_map,VAR_SCRIPT_DIALOG_SURFACE,noone);
	
		#macro VAR_SCRIPT_DIALOG "VAR_SCRIPT_DIALOG"
		ds_map_add_unique(var_map,VAR_SCRIPT_DIALOG,"");
		
		#macro VAR_SCRIPT_DIALOG_SPEAKER "VAR_SCRIPT_DIALOG_SPEAKER"
		ds_map_add_unique(var_map,VAR_SCRIPT_DIALOG_SPEAKER,"");
		
		#macro VAR_SCRIPT_DIALOG_SPEAKER_DRAWN "VAR_SCRIPT_DIALOG_SPEAKER_DRAWN"
		ds_map_add_unique(var_map,VAR_SCRIPT_DIALOG_SPEAKER_DRAWN,false);
		
		#macro VAR_SCRIPT_DIALOG_RAW "VAR_SCRIPT_DIALOG_RAW"
		ds_map_add_unique(var_map,VAR_SCRIPT_DIALOG_RAW,"");
		
		#macro VAR_SCRIPT_DIALOG_LIST_CHAR "VAR_SCRIPT_DIALOG_LIST_CHAR"
		ds_map_add_unique_list(var_map,VAR_SCRIPT_DIALOG_LIST_CHAR,ds_list_create());
		
		#macro VAR_SCRIPT_DIALOG_LIST_X "VAR_SCRIPT_DIALOG_LIST_X"
		ds_map_add_unique_list(var_map,VAR_SCRIPT_DIALOG_LIST_X,ds_list_create());
		
		#macro VAR_SCRIPT_DIALOG_LIST_Y "VAR_SCRIPT_DIALOG_LIST_Y"
		ds_map_add_unique_list(var_map,VAR_SCRIPT_DIALOG_LIST_Y,ds_list_create());
		
		#macro VAR_SCRIPT_DIALOG_LIST_WIDTH "VAR_SCRIPT_DIALOG_LIST_WIDTH"
		ds_map_add_unique_list(var_map,VAR_SCRIPT_DIALOG_LIST_WIDTH,ds_list_create());
		
		#macro VAR_SCRIPT_DIALOG_LIST_HEIGHT "VAR_SCRIPT_DIALOG_LIST_HEIGHT"
		ds_map_add_unique_list(var_map,VAR_SCRIPT_DIALOG_LIST_HEIGHT,ds_list_create());
		
		#macro VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES "VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES"
		ds_map_add_unique_list(var_map,VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES,ds_list_create());
		
	// dialog
		#macro VAR_SCRIPT_DIALOG_NUMBER "VAR_SCRIPT_DIALOG_NUMBER"
		ds_map_add_unique(var_map,VAR_SCRIPT_DIALOG_NUMBER,0);
		
		#macro VAR_SCRIPT_SHOW_CHOICES "VAR_SCRIPT_SHOW_CHOICES"
		ds_map_add_unique(var_map,VAR_SCRIPT_SHOW_CHOICES,0);

	
	// this variable is used to search the current script every step
		#macro VAR_SCRIPT_POSI_FLOATING "VAR_CRIPT_POSI_FLOATING"
		ds_map_add_unique(var_map,VAR_SCRIPT_POSI_FLOATING,0);
	
	
	// the is the target script we are currently going
		#macro VAR_NEXT_SCRIPT "VAR_NEXT_SCRIPT"
		ds_map_add_unique(var_map,VAR_NEXT_SCRIPT,"");
	
		#macro VAR_NEXT_SCRIPT_POSI_OR_LABEL "VAR_NEXT_SCRIPT_POSI_OR_LABEL"
		ds_map_add_unique(var_map,VAR_NEXT_SCRIPT_POSI_OR_LABEL,noone);
		
	// this is the preset of the gui elements
		#macro VAR_GUI_PRESET "VAR_GUI_PRESET" 
			#macro VAR_GUI_MIN "VAR_GUI_MIN"
			#macro VAR_GUI_MIN_WITH_BG "VAR_GUI_MIN_WITH_BG"
			#macro VAR_GUI_NORMAL "VAR_GUI_NORMAL"
			#macro VAR_GUI_MAP "VAR_GUI_MAP"
		ds_map_add_unique(var_map,VAR_GUI_PRESET,VAR_GUI_MIN);
		
	// colorscheme
		#macro VAR_GUI_COLORSCHEME "VAR_GUI_COLORSCHEME" 
			#macro COLORSCHEME_NORMAL "COLORSCHEME_NORMAL"
			#macro COLORSCHEME_GETITEM "COLORSCHEME_GETITEM"
		ds_map_add_unique(var_map,VAR_GUI_COLORSCHEME,COLORSCHEME_NORMAL);

		
	// this is the current game mode
		#macro VAR_MODE "VAR_MODE" 
			#macro VAR_MODE_MAP "VAR_MODE_MAP"
			#macro VAR_MODE_BATTLE "VAR_MODE_BATTLE"
			#macro VAR_MODE_NORMAL "VAR_MODE_NORMAL"
			#macro VAR_MODE_SEX_CHOOSING "VAR_MODE_SEX_CHOOSING"
			#macro VAR_MODE_CAPTURE_CHOOSING "VAR_MODE_CAPTURE_CHOOSING"
			#macro VAR_MODE_STORE "VAR_MODE_STORE"
		ds_map_add_unique(var_map,VAR_MODE,VAR_MODE_NORMAL); 

																						
	// Choices
		#macro VAR_CHOICES_LIST_NAME "VAR_CHOICES_LIST_NAME"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_NAME,ds_list_create());
		
		#macro VAR_CHOICES_LIST_SCRIPT "VAR_CHOICES_LIST_SCRIPT"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_SCRIPT,ds_list_create());
		
		#macro VAR_CHOICES_LIST_POSI_OR_LABEL "VAR_CHOICES_LIST_POSI_OR_LABEL"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_POSI_OR_LABEL,ds_list_create());
			
		#macro VAR_CHOICES_LIST_TOOLTIP "VAR_CHOICES_LIST_TOOLTIP"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_TOOLTIP,ds_list_create());
																					
		#macro VAR_CHOICES_LIST_SURFACE "VAR_CHOICES_LIST_SURFACE"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_SURFACE,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT "VAR_CHOICES_LIST_EXTRASCRIPT"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG0 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG0"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG0,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG1 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG1"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG1,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG2 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG2"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG2,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG3 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG3"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG3,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG4 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG4"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG4,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG5 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG5"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG5,ds_list_create());
		
		#macro VAR_CHOICES_LIST_EXTRASCRIPT_ARG6 "VAR_CHOICES_LIST_EXTRASCRIPT_ARG6"
		ds_map_add_unique_list(var_map,VAR_CHOICES_LIST_EXTRASCRIPT_ARG6,ds_list_create());
		
	
	// char
	#macro VAR_PLAYER "VAR_PLAYER"
	if !ds_map_exists(var_map,VAR_PLAYER)
	{
		ds_map_add_map(var_map,VAR_PLAYER,ds_map_create());
			game_char_map_init_from_wiki(obj_control.var_map[? VAR_PLAYER],NAME_PLAYER,OWNER_PLAYER);
	}
	
	//#macro VAR_PLAYER_UNSPEND_STATS_POINTS "VAR_PLAYER_UNSPEND_STATS_POINTS"
	//ds_map_add_unique(var_map,VAR_PLAYER_UNSPEND_STATS_POINTS,0);
	#macro VAR_PLAYER_UNSPEND_SKILL_POINTS "VAR_PLAYER_UNSPEND_SKILL_POINTS"
	ds_map_add_unique(var_map,VAR_PLAYER_UNSPEND_SKILL_POINTS,0);
	
	
	#macro VAR_ALLY_LIST "VAR_ALLY_LIST"
	ds_map_add_unique_list(var_map,VAR_ALLY_LIST,ds_list_create());
	
	#macro VAR_ALLY_R_LIST "VAR_ALLY_R_LIST"
	ds_map_add_unique_list(var_map,VAR_ALLY_R_LIST,ds_list_create());
	
	#macro VAR_PET_LIST "VAR_PET_LIST"
	ds_map_add_unique_list(var_map,VAR_PET_LIST,ds_list_create());
	
	#macro VAR_PET_R_LIST "VAR_PET_R_LIST"
	ds_map_add_unique_list(var_map,VAR_PET_R_LIST,ds_list_create());
	
	#macro VAR_STORAGE_LIST "VAR_STORAGE_LIST"
	ds_map_add_unique_list(var_map,VAR_STORAGE_LIST,ds_list_create());
	
	#macro VAR_ENEMY_LIST "VAR_ENEMY_LIST"
	ds_map_add_unique_list(var_map,VAR_ENEMY_LIST,ds_list_create());
	
	#macro VAR_ENEMY_R_LIST "VAR_ENEMY_R_LIST"
	ds_map_add_unique_list(var_map,VAR_ENEMY_R_LIST,ds_list_create());

	// player only
	#macro PC_Height_N "PC_Height_N"
	#macro PC_Race_Genderless "PC_Race_Genderless"
	#macro PC_Skin_N "PC_Skin_N"
	#macro PC_Skin_Color_N "PC_Skin_Color_N"
	#macro PC_Hair_Length_N "PC_Hair_Length_N"
	#macro PC_Hair_N "PC_Hair_N"
	#macro PC_Hair_Color_N "PC_Hair_Color_N"
	#macro PC_Eye_N "PC_Eye_N"
	#macro PC_Eye_Color_N "PC_Eye_Color_N"
	#macro PC_Eye_Pupil_N "PC_Eye_Pupil_N"
	#macro PC_AttractFace_N "PC_AttractFace_N"
	#macro PC_Lips_Size_N "PC_Lips_Size_N"
	#macro PC_Lips_Size_Dist_N "PC_Lips_Size_Dist_N"
	#macro PC_Chest_Dist_N "PC_Chest_Dist_N"
	#macro PC_Nips_Size_N "PC_Nips_Size_N"
	#macro PC_Waist_Dist_N "PC_Waist_Dist_N"
	#macro PC_Hips_Size_N "PC_Hips_Size_N"
	#macro PC_Nips_N "PC_Nips_N"
	#macro PC_Bum_Size_N "PC_Bum_Size_N"
	#macro PC_Bum_N "PC_Bum_N"
	#macro PC_LegsL_N "PC_LegsL_N"
	#macro PC_LegsTh_N "PC_LegsTh_N"
	#macro PC_Weight_N "PC_Weight_N"
	#macro PC_Muscularity_N "PC_Muscularity_N"
	#macro PC_Peen_SizeL_N "PC_Peen_SizeL_N"
	#macro PC_Peen_SizeG_N "PC_Peen_SizeG_N"
	#macro PC_Peen_Glans_Size_N "PC_Peen_Glans_Size_N"
	#macro PC_Peen_Glans_Shape_N "PC_Peen_Glans_Shape_N"
	#macro PC_Nads_N "PC_Nads_N"
	#macro PC_Nads_Size_N "PC_Nads_Size_N"
	#macro PC_Nads_Weight_N "PC_Nads_Weight_N"
	#macro PC_Armor_eq "PC_Armor_eq"
	#macro PC_Weapon_eq "PC_Weapon_eq"
	#macro PC_Name "PC_Name"
	#macro PC_Zeyd_Opinion_N "PC_Zeyd_Opinion_N"
	#macro PC_Spurm_Quantity_N "PC_Spurm_Quantity_N"
	ds_map_add_unique(var_map,PC_Height_N,5.6);
	ds_map_add_unique(var_map,PC_Race_Genderless,"yåru");
	ds_map_add_unique(var_map,PC_Skin_N,1.5);
	ds_map_add_unique(var_map,PC_Skin_Color_N,1);
	ds_map_add_unique(var_map,PC_Hair_Length_N,2);
	ds_map_add_unique(var_map,PC_Hair_N,1);
	ds_map_add_unique(var_map,PC_Hair_Color_N,0);
	ds_map_add_unique(var_map,PC_Eye_N,1);
	ds_map_add_unique(var_map,PC_Eye_Color_N,0);
	ds_map_add_unique(var_map,PC_Eye_Pupil_N,0);
	ds_map_add_unique(var_map,PC_AttractFace_N,0);
	ds_map_add_unique(var_map,PC_Lips_Size_N,2);
	ds_map_add_unique(var_map,PC_Lips_Size_Dist_N,0);
	ds_map_add_unique(var_map,PC_Chest_Dist_N,0);
	ds_map_add_unique(var_map,PC_Nips_Size_N,0);
	ds_map_add_unique(var_map,PC_Waist_Dist_N,0);
	ds_map_add_unique(var_map,PC_Hips_Size_N,0);
	ds_map_add_unique(var_map,PC_Nips_N,1);
	ds_map_add_unique(var_map,PC_Bum_Size_N,0);
	ds_map_add_unique(var_map,PC_Bum_N,0);
	ds_map_add_unique(var_map,PC_LegsL_N,0);
	ds_map_add_unique(var_map,PC_LegsTh_N,1);
	ds_map_add_unique(var_map,PC_Weight_N,2);
	ds_map_add_unique(var_map,PC_Muscularity_N,4);
	ds_map_add_unique(var_map,PC_Peen_SizeL_N,6);
	ds_map_add_unique(var_map,PC_Peen_SizeG_N,1.6);
	ds_map_add_unique(var_map,PC_Peen_Glans_Size_N,1.6);
	ds_map_add_unique(var_map,PC_Peen_Glans_Shape_N,0);
	ds_map_add_unique(var_map,PC_Nads_N,2);
	ds_map_add_unique(var_map,PC_Nads_Size_N,1);
	ds_map_add_unique(var_map,PC_Nads_Weight_N,1);
	ds_map_add_unique(var_map,PC_Armor_eq,"");
	ds_map_add_unique(var_map,PC_Weapon_eq,"");
	ds_map_add_unique(var_map,PC_Name,"Udonmania");
	ds_map_add_unique(var_map,PC_Spurm_Quantity_N,0);
	ds_map_add_unique(var_map,PC_Spurm_Quantity_N,0);

	ds_map_add_unique(var_map,GAME_CHAR_DESC_BODY_ENABLED,0); #macro GAME_CHAR_DESC_BODY_ENABLED "GAME_CHAR_DESC_BODY_ENABLED"
	ds_map_add_unique(var_map,GAME_CHAR_DESC_HAS_NADS_ENABLED,0); #macro GAME_CHAR_DESC_HAS_NADS_ENABLED "GAME_CHAR_DESC_HAS_NADS_ENABLED"
		
	// postfx
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_NAME_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_NAME_LIST "VAR_POSTFX_LIST_NAME_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_LIFE_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_LIFE_LIST "VAR_POSTFX_LIST_LIFE_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_LIFE_MAX_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_LIFE_MAX_LIST "VAR_POSTFX_LIST_LIFE_MAX_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG0_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG0_LIST "VAR_POSTFX_LIST_ARG0_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG1_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG1_LIST "VAR_POSTFX_LIST_ARG1_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG2_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG2_LIST "VAR_POSTFX_LIST_ARG2_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG3_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG3_LIST "VAR_POSTFX_LIST_ARG3_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG4_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG4_LIST "VAR_POSTFX_LIST_ARG4_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG5_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG5_LIST "VAR_POSTFX_LIST_ARG5_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG6_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG6_LIST "VAR_POSTFX_LIST_ARG6_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG7_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG7_LIST "VAR_POSTFX_LIST_ARG7_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG8_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG8_LIST "VAR_POSTFX_LIST_ARG8_LIST"
	ds_map_add_unique_list(var_map,VAR_POSTFX_LIST_ARG9_LIST,ds_list_create()); #macro VAR_POSTFX_LIST_ARG9_LIST "VAR_POSTFX_LIST_ARG9_LIST"

	// script_execute
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST,ds_list_create()); #macro VAR_EXECUTE_LIST "VAR_EXECUTE_LIST"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_DELAY,ds_list_create()); #macro VAR_EXECUTE_LIST_DELAY "VAR_EXECUTE_LIST_DELAY"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG0,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG0 "VAR_EXECUTE_LIST_ARG0"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG1,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG1 "VAR_EXECUTE_LIST_ARG1"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG2,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG2 "VAR_EXECUTE_LIST_ARG2"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG3,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG3 "VAR_EXECUTE_LIST_ARG3"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG4,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG4 "VAR_EXECUTE_LIST_ARG4"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG5,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG5 "VAR_EXECUTE_LIST_ARG5"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG6,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG6 "VAR_EXECUTE_LIST_ARG6"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG7,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG7 "VAR_EXECUTE_LIST_ARG7"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG8,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG8 "VAR_EXECUTE_LIST_ARG8"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG9,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG9 "VAR_EXECUTE_LIST_ARG9"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG10,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG10 "VAR_EXECUTE_LIST_ARG10"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG11,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG11 "VAR_EXECUTE_LIST_ARG11"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG12,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG12 "VAR_EXECUTE_LIST_ARG12"
	ds_map_add_unique_list(var_map,VAR_EXECUTE_LIST_ARG13,ds_list_create()); #macro VAR_EXECUTE_LIST_ARG13 "VAR_EXECUTE_LIST_ARG13"

	// location
	ds_map_add_unique(var_map,VAR_LOCATION,LOCATION_7); #macro VAR_LOCATION "VAR_LOCATION"
	ds_map_add_unique_list(var_map,VAR_LOCATION_VISIBLE_LIST,ds_list_create()); #macro VAR_LOCATION_VISIBLE_LIST "VAR_LOCATION_VISIBLE_LIST"
	ds_map_add_unique_list(var_map,VAR_LOCATION_VISITED_LIST,ds_list_create()); #macro VAR_LOCATION_VISITED_LIST "VAR_LOCATION_VISITED_LIST"					
	ds_map_add_unique_list(var_map,VAR_LOCATION_PATH_AND_NEIGHBOR_LIST,ds_list_create()); #macro VAR_LOCATION_PATH_AND_NEIGHBOR_LIST "VAR_LOCATION_PATH_AND_NEIGHBOR_LIST"	
	ds_map_add_unique_list(var_map,VAR_LOCATION_UNLOCKED_LIST,ds_list_create()); #macro VAR_LOCATION_UNLOCKED_LIST "VAR_LOCATION_UNLOCKED_LIST"	
	
	// misc
	ds_map_add_unique(var_map,VAR_JAR,0); #macro VAR_JAR "VAR_JAR"
	ds_map_add_unique(var_map,VAR_GOLD,0); #macro VAR_GOLD "VAR_GOLD"
	//ds_map_add_unique(var_map,RBS_IMP_D_3_1_HOUR_COUNTER,0); #macro RBS_IMP_D_3_1_HOUR_COUNTER "RBS_IMP_D_3_1_HOUR_COUNTER"
	
	
	// battle
	ds_map_add_unique(var_map,VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER,noone); #macro VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER "VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER"
	ds_map_add_unique_list(var_map,VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST,ds_list_create()); #macro VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST "VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST"
	
	ds_map_add_unique(var_map,BATTLE_WIN_SCENE,""); #macro BATTLE_WIN_SCENE "BATTLE_WIN_SCENE"
	ds_map_add_unique(var_map,BATTLE_WIN_SCENE_LABEL,""); #macro BATTLE_WIN_SCENE_LABEL "BATTLE_WIN_SCENE_LABEL"
	ds_map_add_unique(var_map,BATTLE_LOSS_SCENE,""); #macro BATTLE_LOSS_SCENE "BATTLE_LOSS_SCENE"
	ds_map_add_unique(var_map,BATTLE_LOSS_SCENE_LABEL,""); #macro BATTLE_LOSS_SCENE_LABEL "BATTLE_LOSS_SCENE_LABEL"
	
	ds_map_add_unique(var_map,BATTLE_END_TYPE,""); #macro BATTLE_END_TYPE "BATTLE_END_TYPE"

	// others
	ds_map_add_unique_list(var_map,VAR_TEMP_SCENE_LIST,ds_list_create()); #macro VAR_TEMP_SCENE_LIST "VAR_TEMP_SCENE_LIST"
	ds_map_add_unique(var_map,VAR_TEMP_SCENE,""); #macro VAR_TEMP_SCENE "VAR_TEMP_SCENE"
	ds_map_add_unique(var_map,VAR_TEMP_SCENE_TYPE,""); #macro VAR_TEMP_SCENE_TYPE "VAR_TEMP_SCENE_TYPE"
	
	ds_map_add_unique(var_map,VAR_TEMP_ITEM,ITEM_PIE_FANCY); #macro VAR_TEMP_ITEM "VAR_TEMP_ITEM"
	
	ds_map_add_unique(var_map,VAR_TEMP_CHARID,noone); #macro VAR_TEMP_CHARID "VAR_TEMP_CHARID"
	ds_map_add_unique(var_map,VAR_TEMP_CHARID2,noone); #macro VAR_TEMP_CHARID2 "VAR_TEMP_CHARID2"
	
	ds_map_add_unique(var_map,VAR_EXTRA_AFTER_SCENE,""); #macro VAR_EXTRA_AFTER_SCENE "VAR_EXTRA_AFTER_SCENE"
	ds_map_add_unique(var_map,VAR_EXTRA_AFTER_SCENE_LABEL,""); #macro VAR_EXTRA_AFTER_SCENE_LABEL "VAR_EXTRA_AFTER_SCENE_LABEL"
	ds_map_add_unique(var_map,VAR_EXTRA2_AFTER_SCENE,""); #macro VAR_EXTRA2_AFTER_SCENE "VAR_EXTRA2_AFTER_SCENE"
	ds_map_add_unique(var_map,VAR_EXTRA2_AFTER_SCENE_LABEL,""); #macro VAR_EXTRA2_AFTER_SCENE_LABEL "VAR_EXTRA2_AFTER_SCENE_LABEL"
	
	ds_map_add_unique(var_map,VAR_NEW_QJ_NOTICE,false); #macro VAR_NEW_QJ_NOTICE "VAR_NEW_QJ_NOTICE"

	ds_map_add_unique(var_map,BERRIES_EFFECT,""); #macro BERRIES_EFFECT "BERRIES_EFFECT"

	// sprite, cg
	scene_sprite_list_init();
	scene_cg_list_init();

	// store
	ds_map_add_unique_list(var_map,VAR_STORE_LIST_NAME,ds_list_create()); #macro VAR_STORE_LIST_NAME "VAR_STORE_LIST_NAME"
	ds_map_add_unique_list(var_map,VAR_STORE_LIST_SURFACE,ds_list_create()); #macro VAR_STORE_LIST_SURFACE "VAR_STORE_LIST_SURFACE"
	ds_map_add_unique_list(var_map,VAR_STORE_LIST_ALPHA,ds_list_create()); #macro VAR_STORE_LIST_ALPHA "VAR_STORE_LIST_ALPHA"
	
	// sprites set
	ds_map_add_unique_map(var_map,VAR_CLOTHES_SETS_MAP,ds_map_create()); #macro VAR_CLOTHES_SETS_MAP "VAR_CLOTHES_SETS_MAP"

	// battlelog active
	ds_map_add_unique(var_map,VAR_BATTLELOG_ACTIVE,true); #macro VAR_BATTLELOG_ACTIVE "VAR_BATTLELOG_ACTIVE"
	ds_map_add_unique_list(var_map,VAR_BATTLELOG_COPY_LIST,ds_list_create()); #macro VAR_BATTLELOG_COPY_LIST "VAR_BATTLELOG_COPY_LIST"

}