///ds_list_insertion_sort_mapkey(lst,ascend,mapkey);
/// @description - Sort all the nunbers of the mapkey within a list using 'quicksort' algorithm.
/// @param lst - A list of maps.
/// @param ascend - Whether the values should be ascending (true) or descending (false) order.
/// @param mapkey - the key in the map.
var i,j,k;
var lst = argument0;
var ascend = argument1;
var mapkey = argument2;
for (i = 1; i < ds_list_size(lst); ++i)
{
    k = ds_map_find_value(lst[| i],mapkey); 
	//var oldmap = lst[| i];
    j = i - 1;
    while ((j >= 0) && (k < ds_map_find_value(lst[| j],mapkey)))
    {
        //ds_map_replace(lst[| j+1],mapkey,ds_map_find_value(lst[| j],mapkey));
		//lst[| j+1] = lst[| j];
		ds_list_swap(lst,j+1,j);
        --j;
    }
	//lst[| j+1] = lst[| i];
	//ds_list_swap(lst,j+1,i);

}