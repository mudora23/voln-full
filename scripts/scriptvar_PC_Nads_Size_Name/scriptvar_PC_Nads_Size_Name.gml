///scriptvar_PC_Nads_Size_Name();
/// @description
var possible_results_list = ds_list_create();

if var_map_get(PC_Nads_Size_N) < 1.2
	ds_list_add(possible_results_list,"nut","berrie","dab");
if var_map_get(PC_Nads_Size_N) >= 1.2
	ds_list_add(possible_results_list,"nad","nut","orb","flappər","slammər","slappər");
if var_map_get(PC_Nads_Size_N) >= 1.6
	ds_list_add(possible_results_list,"yam");
	
ds_list_shuffle(possible_results_list);
var result = possible_results_list[| 0];
ds_list_destroy(possible_results_list);
return result;
