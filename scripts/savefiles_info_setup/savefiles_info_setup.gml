///savefiles_info_setup(num[optional][1-6]);
/// @description
/// @param num[optional][1-6]


if argument_count == 0 || argument[0] == 1
{
    var the_map = obj_control.info_list_of_map[| 1];
    if the_map != noone
    {
        sprite_delete(the_map[? "thumb"]);
        ds_map_destroy(the_map);
    }
    
    if file_exists("info1.dat") && file_exists("thumbnail1.png")
    {
		the_map = ds_map_json_load("info1.dat");
		obj_control.info_list_of_map[| 1] = the_map;
        the_map[? "thumb"] = sprite_add("thumbnail1.png", 1, false, false, 0, 0);
    }
    else
        obj_control.info_list_of_map[| 1] = noone;
}
if argument_count == 0 || argument[0] == 2
{
    var the_map = obj_control.info_list_of_map[| 2];
    if the_map != noone
    {
        sprite_delete(the_map[? "thumb"]);
        ds_map_destroy(the_map);
    }
    
    if file_exists("info2.dat") && file_exists("thumbnail2.png")
    {
		the_map = ds_map_json_load("info2.dat");
		obj_control.info_list_of_map[| 2] = the_map;
        the_map[? "thumb"] = sprite_add("thumbnail2.png", 1, false, false, 0, 0);
    }
    else
        obj_control.info_list_of_map[| 2] = noone;
}
if argument_count == 0 || argument[0] == 3
{
    var the_map = obj_control.info_list_of_map[| 3];
    if the_map != noone
    {
        sprite_delete(the_map[? "thumb"]);
        ds_map_destroy(the_map);
    }
    
    if file_exists("info3.dat") && file_exists("thumbnail3.png")
    {
        the_map = ds_map_json_load("info3.dat");
		obj_control.info_list_of_map[| 3] = the_map;
        the_map[? "thumb"] = sprite_add("thumbnail3.png", 1, false, false, 0, 0);
    }
    else
        obj_control.info_list_of_map[| 3] = noone;
}
if argument_count == 0 || argument[0] == 4
{
    var the_map = obj_control.info_list_of_map[| 4];
    if the_map != noone
    {
        sprite_delete(the_map[? "thumb"]);
        ds_map_destroy(the_map);
    }
    
    if file_exists("info4.dat") && file_exists("thumbnail4.png")
    {
		the_map = ds_map_json_load("info4.dat");
		obj_control.info_list_of_map[| 4] = the_map;
        the_map[? "thumb"] = sprite_add("thumbnail4.png", 1, false, false, 0, 0);
    }
    else
        obj_control.info_list_of_map[| 4] = noone;
}
if argument_count == 0 || argument[0] == 5
{
    var the_map = obj_control.info_list_of_map[| 5];
    if the_map != noone
    {
        sprite_delete(the_map[? "thumb"]);
        ds_map_destroy(the_map);
    }
    
    if file_exists("info5.dat") && file_exists("thumbnail5.png")
    {
		the_map = ds_map_json_load("info5.dat");
		obj_control.info_list_of_map[| 5] = the_map;
        the_map[? "thumb"] = sprite_add("thumbnail5.png", 1, false, false, 0, 0);
    }
    else
        obj_control.info_list_of_map[| 5] = noone;
}
if argument_count == 0 || argument[0] == 6
{
    var the_map = obj_control.info_list_of_map[| 6];
    if the_map != noone
    {
        sprite_delete(the_map[? "thumb"]);
        ds_map_destroy(the_map);
    }
    
    if file_exists("info6.dat") && file_exists("thumbnail6.png")
    {
		the_map = ds_map_json_load("info6.dat");
		obj_control.info_list_of_map[| 6] = the_map;
        the_map[? "thumb"] = sprite_add("thumbnail6.png", 1, false, false, 0, 0);
    }
    else
        obj_control.info_list_of_map[| 6] = noone;
}