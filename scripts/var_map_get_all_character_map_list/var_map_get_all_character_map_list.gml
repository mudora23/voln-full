///var_map_get_all_character_map_list(not_get_but_check_only);
/// @description
/// @param not_get_but_check_only

var char_list = ds_list_create();

var cur_char_list = obj_control.var_map[? VAR_ALLY_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}
var cur_char_list = obj_control.var_map[? VAR_ALLY_R_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}
var cur_char_list = obj_control.var_map[? VAR_PET_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}
var cur_char_list = obj_control.var_map[? VAR_PET_R_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}
var cur_char_list = obj_control.var_map[? VAR_STORAGE_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}
var cur_char_list = obj_control.var_map[? VAR_ENEMY_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}
var cur_char_list = obj_control.var_map[? VAR_ENEMY_R_LIST];
for(var i = 0; i < ds_list_size(cur_char_list);i++)
{
	if argument_count > 0 && argument[0] && !ds_exists(cur_char_list[| i],ds_type_map)
		ds_list_delete(cur_char_list,i);
	else
		ds_list_add(char_list,cur_char_list[| i]);
}

if argument_count > 0 && argument[0] && !ds_exists(obj_control.var_map[? VAR_PLAYER],ds_type_map)
	obj_control.var_map[? VAR_PLAYER] = noone;
else
	ds_list_add(char_list,obj_control.var_map[? VAR_PLAYER]);

if argument_count > 0 && argument[0]
	ds_list_destroy(char_list);
else
	return char_list;