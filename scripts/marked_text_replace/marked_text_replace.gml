///marked_text_replace(marked_text,text_to_replace,starting_char_index);
/// @description
var marked_text = argument0;

for(var i = 1; i <=string_length(argument1);i++)
{
	string_overwrite(marked_text,argument2+(i-1)*3,string_char_at(argument1,i));
}

return marked_text;
