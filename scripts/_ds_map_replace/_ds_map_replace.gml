///_ds_map_replace(MAP,KEY,VAL);
/// @description  
/// @param MAP
/// @param KEY
/// @param VAL


if scene_is_current()
{
	ds_map_replace(argument0,argument1,argument2);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);