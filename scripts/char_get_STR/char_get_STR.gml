///char_get_STR(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_STR);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_STR_1);
amount += char_get_skill_level(argument0,SKILL_STR_2);
amount += char_get_skill_level(argument0,SKILL_STR_3);
amount += char_get_skill_level(argument0,SKILL_STR_4);
amount += char_get_skill_level(argument0,SKILL_STR_5);
amount += char_get_skill_level(argument0,SKILL_STR_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;