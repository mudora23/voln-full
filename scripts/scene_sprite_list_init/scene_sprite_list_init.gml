///scene_sprite_list_init();
/// @description 
with obj_control
{
	#macro VAR_SPRITE_LIST_ID "VAR_SPRITE_LIST_ID"
	#macro VAR_SPRITE_LIST_INDEX_NAME "VAR_SPRITE_LIST_INDEX_NAME"
	#macro VAR_SPRITE_LIST_INDEX_TARGET_NAME "VAR_SPRITE_LIST_INDEX_TARGET_NAME"
	#macro VAR_SPRITE_LIST_X "VAR_SPRITE_LIST_X"
	#macro VAR_SPRITE_LIST_X_TARGET "VAR_SPRITE_LIST_X_TARGET"
	#macro VAR_SPRITE_LIST_Y "VAR_SPRITE_LIST_Y"
	#macro VAR_SPRITE_LIST_Y_TARGET "VAR_SPRITE_LIST_Y_TARGET"
	#macro VAR_SPRITE_LIST_ALPHA "VAR_SPRITE_LIST_ALPHA"
	#macro VAR_SPRITE_LIST_ALPHA_TARGET "VAR_SPRITE_LIST_ALPHA_TARGET"
	#macro VAR_SPRITE_LIST_FADE_SPEED "VAR_SPRITE_LIST_FADE_SPEED"
	#macro VAR_SPRITE_LIST_SLIDE_SPEED "VAR_SPRITE_LIST_SLIDE_SPEED"
	#macro VAR_SPRITE_LIST_ORDER "VAR_SPRITE_LIST_ORDER"
	#macro VAR_SPRITE_LIST_SIZE_RATIO "VAR_SPRITE_LIST_SIZE_RATIO"

	
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_ID,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_INDEX_NAME,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_INDEX_TARGET_NAME,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_X,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_X_TARGET,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_Y,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_Y_TARGET,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_ALPHA,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_ALPHA_TARGET,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_FADE_SPEED,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_SLIDE_SPEED,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_ORDER,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_SPRITE_LIST_SIZE_RATIO,ds_list_create());

}