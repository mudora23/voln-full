///sprite_get_name_ext(sprite_in_any_form);
/// @description
/// @param sprite_in_any_form
if is_string(argument0)
{
	if asset_get_type(argument0) == asset_sprite
		return argument0;
	else
		return "";
}
else
{
	if sprite_exists(argument0)
		return sprite_get_name(argument0);
	else
		return "";
}	