//Generated for GMS2 in Geon FX v0.9.0
//Put this code in Create event

//Creating Particle System
global.ps = part_system_create();
part_system_depth(global.ps, DEPTH_Particles);

//Creating Particle Types
//OnHit1
global.ps_OnHit1 = part_type_create();
part_type_shape(global.ps_OnHit1, pt_shape_flare);
part_type_size(global.ps_OnHit1, 1, 1, 0.10, 0);
part_type_scale(global.ps_OnHit1, 1, 1);
part_type_orientation(global.ps_OnHit1, 0, 0, 0, 0, 0);
part_type_color3(global.ps_OnHit1, 65535, 4227327, 8454143);
part_type_alpha3(global.ps_OnHit1, 0.20, 0.44, 0);
part_type_blend(global.ps_OnHit1, 0);
part_type_life(global.ps_OnHit1, 30, 30);
part_type_speed(global.ps_OnHit1, 0, 0, 0, 0);
part_type_direction(global.ps_OnHit1, 0, 360, 0, 0);
part_type_gravity(global.ps_OnHit1, 0, 0);

//OnHit2
global.ps_OnHit2 = part_type_create();
part_type_shape(global.ps_OnHit2, pt_shape_pixel);
part_type_size(global.ps_OnHit2, 1, 1, 0.02, 0);
part_type_scale(global.ps_OnHit2, 20, 2);
part_type_orientation(global.ps_OnHit2, 0, 0, 0, 0, 1);
part_type_color3(global.ps_OnHit2, 33023, 4235519, 8454143);
part_type_alpha3(global.ps_OnHit2, 0.37, 0.40, 0.16);
part_type_blend(global.ps_OnHit2, 0);
part_type_life(global.ps_OnHit2, 20, 30);
part_type_speed(global.ps_OnHit2, 5, 5, 0, 0);
part_type_direction(global.ps_OnHit2, 0, 360, 0, 0);
part_type_gravity(global.ps_OnHit2, 0, 0);

//OnHit3
global.ps_OnHit3 = part_type_create();
part_type_shape(global.ps_OnHit3, pt_shape_pixel);
part_type_size(global.ps_OnHit3, 1, 1, 0, 0);
part_type_scale(global.ps_OnHit3, 2, 2);
part_type_orientation(global.ps_OnHit3, 0, 0, 0, 0, 0);
part_type_color3(global.ps_OnHit3, 65535, 4235519, 4227327);
part_type_alpha3(global.ps_OnHit3, 1, 1, 1);
part_type_blend(global.ps_OnHit3, 0);
part_type_life(global.ps_OnHit3, 20, 30);
part_type_speed(global.ps_OnHit3, 4, 5, 0, 0);
part_type_direction(global.ps_OnHit3, 0, 360, 0, 0);
part_type_gravity(global.ps_OnHit3, 0, 0);

//Creating Emitters
global.ps_OnHit1 = part_emitter_create(global.ps);
global.ps_OnHit2 = part_emitter_create(global.ps);
global.ps_OnHit3 = part_emitter_create(global.ps);




//Generated for GMS2 in Geon FX v0.9.0
//Put this code in Create event

//Creating Particle System
//global.onhitb = part_system_create();
//part_system_depth(global.onhitb, DEPTH_Particles);

//Creating Particle Types
//OnHit1B
global.ps_OnHit1B = part_type_create();
part_type_shape(global.ps_OnHit1B, pt_shape_flare);
part_type_size(global.ps_OnHit1B, 1, 1, 0.10, 0);
part_type_scale(global.ps_OnHit1B, 1, 1);
part_type_orientation(global.ps_OnHit1B, 0, 0, 0, 0, 0);
part_type_color3(global.ps_OnHit1B, 16749092, 16755027, 16776994);
part_type_alpha3(global.ps_OnHit1B, 0.20, 0.44, 0);
part_type_blend(global.ps_OnHit1B, 0);
part_type_life(global.ps_OnHit1B, 30, 30);
part_type_speed(global.ps_OnHit1B, 0, 0, 0, 0);
part_type_direction(global.ps_OnHit1B, 0, 360, 0, 0);
part_type_gravity(global.ps_OnHit1B, 0, 0);

//OnHit2B
global.ps_OnHit2B = part_type_create();
part_type_shape(global.ps_OnHit2B, pt_shape_pixel);
part_type_size(global.ps_OnHit2B, 1, 1, 0.02, 0);
part_type_scale(global.ps_OnHit2B, 20, 2);
part_type_orientation(global.ps_OnHit2B, 0, 0, 0, 0, 1);
part_type_color3(global.ps_OnHit2B, 16744448, 16758820, 16437637);
part_type_alpha3(global.ps_OnHit2B, 0.37, 0.40, 0.16);
part_type_blend(global.ps_OnHit2B, 0);
part_type_life(global.ps_OnHit2B, 20, 30);
part_type_speed(global.ps_OnHit2B, 5, 5, 0, 0);
part_type_direction(global.ps_OnHit2B, 0, 360, 0, 0);
part_type_gravity(global.ps_OnHit2B, 0, 0);

//OnHit3B
global.ps_OnHit3B = part_type_create();
part_type_shape(global.ps_OnHit3B, pt_shape_pixel);
part_type_size(global.ps_OnHit3B, 1, 1, 0, 0);
part_type_scale(global.ps_OnHit3B, 2, 2);
part_type_orientation(global.ps_OnHit3B, 0, 0, 0, 0, 0);
part_type_color3(global.ps_OnHit3B, 16744448, 16776960, 16744448);
part_type_alpha3(global.ps_OnHit3B, 1, 1, 1);
part_type_blend(global.ps_OnHit3B, 0);
part_type_life(global.ps_OnHit3B, 20, 30);
part_type_speed(global.ps_OnHit3B, 4, 5, 0, 0);
part_type_direction(global.ps_OnHit3B, 0, 360, 0, 0);
part_type_gravity(global.ps_OnHit3B, 0, 0);

//Creating Emitters
global.ps_OnHit1B = part_emitter_create(global.ps);
global.ps_OnHit2B = part_emitter_create(global.ps);
global.ps_OnHit3B = part_emitter_create(global.ps);

//Creating Particle Types
//doublehit1
global.ps_doublehit1 = part_type_create();
part_type_shape(global.ps_doublehit1, pt_shape_pixel);
part_type_size(global.ps_doublehit1, 12, 12, 0, 0);
part_type_scale(global.ps_doublehit1, 20, 1);
part_type_orientation(global.ps_doublehit1, 0, 0, 0, 0, 1);
part_type_color3(global.ps_doublehit1, 0, 64, 0);
part_type_alpha3(global.ps_doublehit1, 0.4, 0.7, 0.3);
part_type_blend(global.ps_doublehit1, 0);
part_type_life(global.ps_doublehit1, 20, 20);
part_type_speed(global.ps_doublehit1, 20, 20, 0, 0);
part_type_direction(global.ps_doublehit1, 225, 225, 0, 0);
part_type_gravity(global.ps_doublehit1, 0, 0);

//doublehit2
global.ps_doublehit2 = part_type_create();
part_type_shape(global.ps_doublehit2, pt_shape_pixel);
part_type_size(global.ps_doublehit2, 12, 12, 0, 0);
part_type_scale(global.ps_doublehit2, 20, 1);
part_type_orientation(global.ps_doublehit2, 0, 0, 0, 0, 1);
part_type_color3(global.ps_doublehit2, 0, 64, 0);
part_type_alpha3(global.ps_doublehit2, 0.4, 0.7, 0.3);
part_type_blend(global.ps_doublehit2, 0);
part_type_life(global.ps_doublehit2, 20, 20);
part_type_speed(global.ps_doublehit2, 20, 20, 0, 0);
part_type_direction(global.ps_doublehit2, 315, 315, 0, 0);
part_type_gravity(global.ps_doublehit2, 0, 0);

//Creating Emitters
global.ps_doublehit1 = part_emitter_create(global.ps);
global.ps_doublehit2 = part_emitter_create(global.ps);


// Fireball
//Creating Particle Types
//fireball_center
global.ps_fireball_center = part_type_create();
part_type_shape(global.ps_fireball_center, pt_shape_sphere);
part_type_size(global.ps_fireball_center, 1, 1, 0.15, 0);
part_type_scale(global.ps_fireball_center, 1, 1);
part_type_orientation(global.ps_fireball_center, 0, 0, 1, 0, 0);
part_type_color3(global.ps_fireball_center, 786173, 4235519, 255);
part_type_alpha3(global.ps_fireball_center, 1, 0.71, 0);
part_type_blend(global.ps_fireball_center, 1);
part_type_life(global.ps_fireball_center, 30, 50);
part_type_speed(global.ps_fireball_center, 0, 1, 0, 0);
part_type_direction(global.ps_fireball_center, 0, 359, 0, 0);
part_type_gravity(global.ps_fireball_center, 0, 0);

//fireball_particle
global.ps_fireball_particle = part_type_create();
part_type_shape(global.ps_fireball_particle, pt_shape_pixel);
part_type_size(global.ps_fireball_particle, 5, 5, 0, 1);
part_type_scale(global.ps_fireball_particle, 1, 1);
part_type_orientation(global.ps_fireball_particle, 0, 0, 0, 0, 0);
part_type_color3(global.ps_fireball_particle, 65535, 4235519, 255);
part_type_alpha3(global.ps_fireball_particle, 1, 0.50, 0);
part_type_blend(global.ps_fireball_particle, 1);
part_type_life(global.ps_fireball_particle, 60, 100);
part_type_speed(global.ps_fireball_particle, 8, 12, 0, 0);
part_type_direction(global.ps_fireball_particle, 0, 180, 0, 0);
part_type_gravity(global.ps_fireball_particle, 0.20, 270);

//fireball_smoke
global.ps_fireball_smoke = part_type_create();
part_type_shape(global.ps_fireball_smoke, pt_shape_cloud);
//spr_TMC_PL_Cloud_2_spr_0_part = sprite_add("TMC_PL_Cloud_2_spr_0.png", 1, 0, 0, 64, 64);
part_type_sprite(global.ps_fireball_smoke, spr_TMC_PL_Cloud_2_spr_0_part, 1, 0, 0);
part_type_size(global.ps_fireball_smoke, 0.5, 0.5, 0.02, 0);
part_type_scale(global.ps_fireball_smoke, 0.75, 0.75);
part_type_orientation(global.ps_fireball_smoke, 0, 356, 0, 0, 0);
part_type_color3(global.ps_fireball_smoke, 65535, 4235519, 255);
part_type_alpha3(global.ps_fireball_smoke, 0.57, 0.50, 0);
part_type_blend(global.ps_fireball_smoke, 1);
part_type_life(global.ps_fireball_smoke, 40, 80);
part_type_speed(global.ps_fireball_smoke, 3, 5, -0.20, 0);
part_type_direction(global.ps_fireball_smoke, 0, 359, 0, 0);
part_type_gravity(global.ps_fireball_smoke, 0.10, 90);

//fireball_centerB
global.ps_fireball_centerB = part_type_create();
part_type_shape(global.ps_fireball_centerB, pt_shape_sphere);
part_type_size(global.ps_fireball_centerB, 0.20, 1, 0.02, 0);
part_type_scale(global.ps_fireball_centerB, 3, 3);
part_type_orientation(global.ps_fireball_centerB, 0, 359, 1, 0, 0);
part_type_color3(global.ps_fireball_centerB, 13355979, 13882323, 14145495);
part_type_alpha3(global.ps_fireball_centerB, 0, 0.30, 0);
part_type_blend(global.ps_fireball_centerB, 0);
part_type_life(global.ps_fireball_centerB, 70, 90);
part_type_speed(global.ps_fireball_centerB, 0, 1, 0, 0);
part_type_direction(global.ps_fireball_centerB, 0, 359, 0, 0);
part_type_gravity(global.ps_fireball_centerB, 0, 0);

//Creating Emitters
global.ps_fireball_center = part_emitter_create(global.ps);
global.ps_fireball_particle = part_emitter_create(global.ps);
global.ps_fireball_smoke = part_emitter_create(global.ps);
global.ps_fireball_centerB = part_emitter_create(global.ps);


//Creating Particle Types
//rain
global.ps_rain = part_type_create();
part_type_shape(global.ps_rain, pt_shape_pixel);
part_type_size(global.ps_rain, 1, 1, 0, 0.10);
part_type_scale(global.ps_rain, 80, 4);
part_type_orientation(global.ps_rain, 0, 0, 0, 0, 1);
part_type_color3(global.ps_rain, 16777088, 16777088, 16777088);
part_type_alpha3(global.ps_rain, 1, 1, 1);
part_type_blend(global.ps_rain, 1);
part_type_life(global.ps_rain, 100, 100);
part_type_speed(global.ps_rain, 30, 30, 0, 0);
part_type_direction(global.ps_rain, 231, 244, 0, 0);
part_type_gravity(global.ps_rain, 0.20, 270);

//Creating Emitters
global.ps_rain = part_emitter_create(global.ps);

//Creating Particle Types
//occult
global.ps_occult = part_type_create();
part_type_shape(global.ps_occult, pt_shape_pixel);
//spr_sign_part = sprite_add("spr_fx_occult.png", 1, 0, 0, 270, 270);
part_type_sprite(global.ps_occult, spr_fx_occult_part, 1, 0, 0);
part_type_size(global.ps_occult, 1, 1, 0, 0);
part_type_scale(global.ps_occult, 1, 1);
part_type_orientation(global.ps_occult, 0, 0, 1, 0, 0);
part_type_color3(global.ps_occult, 16777215, 16777215, 16777215);
part_type_alpha3(global.ps_occult, 0.3, 1, 0);
part_type_blend(global.ps_occult, 0);
part_type_life(global.ps_occult, room_speed*4, room_speed*4);
part_type_speed(global.ps_occult, 0, 0, 0, 0);
part_type_direction(global.ps_occult, 0, 360, 0, 0);
part_type_gravity(global.ps_occult, 0, 0);

//Creating Emitters
global.ps_occult = part_emitter_create(global.ps);

//Creating Particle Types
//hole
global.ps_hole = part_type_create();
part_type_shape(global.ps_hole, pt_shape_pixel);
part_type_size(global.ps_hole, 1, 1, 0, 0);
part_type_scale(global.ps_hole, 80, 4);
part_type_orientation(global.ps_hole, 0, 0, 0, 0, 1);
part_type_color3(global.ps_hole, 16724889, 16724889, 16724889);
part_type_alpha3(global.ps_hole, 0, 1, 0);
part_type_blend(global.ps_hole, 1);
part_type_life(global.ps_hole, 100, 100);
part_type_speed(global.ps_hole, 3, 10, 0, 0);
part_type_direction(global.ps_hole, 0, 359, 0, 0);
part_type_gravity(global.ps_hole, 0, 0);

//hole2
global.ps_hole2 = part_type_create();
part_type_shape(global.ps_hole2, pt_shape_pixel);
part_type_size(global.ps_hole2, 1, 1, 0, 0);
part_type_scale(global.ps_hole2, 8, 16);
part_type_orientation(global.ps_hole2, 0, 0, 0, 0, 0);
part_type_color3(global.ps_hole2, 16724889, 16724889, 16724889);
part_type_alpha3(global.ps_hole2, 0, 1, 0);
part_type_blend(global.ps_hole2, 1);
part_type_life(global.ps_hole2, 80, 80);
part_type_speed(global.ps_hole2, 10, 15, 0, 0);
part_type_direction(global.ps_hole2, 0, 359, 0, 0);
part_type_gravity(global.ps_hole2, 0, 0);

//hole3
global.ps_hole3 = part_type_create();
part_type_shape(global.ps_hole3, pt_shape_ring);
part_type_size(global.ps_hole3, 1, 1, 1, 0);
part_type_scale(global.ps_hole3, 1, 1);
part_type_orientation(global.ps_hole3, 0, 0, 0, 0, 0);
part_type_color3(global.ps_hole3, 16724889, 16724889, 16724889);
part_type_alpha3(global.ps_hole3, 0, 0.15, 0);
part_type_blend(global.ps_hole3, 1);
part_type_life(global.ps_hole3, 80, 80);
part_type_speed(global.ps_hole3, 0, 0, 0, 0);
part_type_direction(global.ps_hole3, 0, 359, 0, 0);
part_type_gravity(global.ps_hole3, 0, 0);

//Creating Emitters
global.ps_hole = part_emitter_create(global.ps);
global.ps_hole2 = part_emitter_create(global.ps);
global.ps_hole3 = part_emitter_create(global.ps);


// large_hit1
global.large_hit1 = part_type_create();
part_type_shape(global.large_hit1, pt_shape_ring);
part_type_size(global.large_hit1, 1, 1, 0.20, 0);
part_type_scale(global.large_hit1, 1, 1);
part_type_orientation(global.large_hit1, 0, 360, 0, 0, 0);
part_type_color3(global.large_hit1, 64, 6784, 4215807);
part_type_alpha3(global.large_hit1, 0.5, 0.4, 0);
part_type_blend(global.large_hit1, 0);
part_type_life(global.large_hit1, 30, 30);
part_type_speed(global.large_hit1, 0, 0, 0, 0);
part_type_direction(global.large_hit1, 0, 360, 0, 0);
part_type_gravity(global.large_hit1, 0, 0);

//large_hit2
global.large_hit2 = part_type_create();
part_type_shape(global.large_hit2, pt_shape_smoke);
part_type_size(global.large_hit2, 1, 1, 0.15, 0);
part_type_scale(global.large_hit2, 1, 1);
part_type_orientation(global.large_hit2, 0, 360, 0, 0, 0);
part_type_color3(global.large_hit2, 64, 70014, 896);
part_type_alpha3(global.large_hit2, 0.3, 0.2, 0);
part_type_blend(global.large_hit2, 0);
part_type_life(global.large_hit2, 20, 20);
part_type_speed(global.large_hit2, 0, 0, 0, 0);
part_type_direction(global.large_hit2, 0, 360, 0, 0);
part_type_gravity(global.large_hit2, 0, 0);

//large_hit3
global.large_hit3 = part_type_create();
part_type_shape(global.large_hit3, pt_shape_ring);
part_type_size(global.large_hit3, 1, 1, 0.35, 0);
part_type_scale(global.large_hit3, 1, 1);
part_type_orientation(global.large_hit3, 0, 360, 0, 0, 0);
part_type_color3(global.large_hit3, 64, 67990, 140520);
part_type_alpha3(global.large_hit3, 0.3, 0.2, 0);
part_type_blend(global.large_hit3, 0);
part_type_life(global.large_hit3, 30, 30);
part_type_speed(global.large_hit3, 0, 0, 0, 0);
part_type_direction(global.large_hit3, 0, 360, 0, 0);
part_type_gravity(global.large_hit3, 0, 0);

//Creating Emitters
global.large_hit1 = part_emitter_create(global.ps);
global.large_hit2 = part_emitter_create(global.ps);
global.large_hit3 = part_emitter_create(global.ps);


//Creating Particle Types

global.burn_pt0 = part_type_create();
part_type_size(global.burn_pt0,0,0.5,0,0);
part_type_scale(global.burn_pt0,1.02,0.13);
part_type_life(global.burn_pt0,60,60);
part_type_gravity(global.burn_pt0,0,0);
part_type_speed(global.burn_pt0,5,8,-0.05,0.2);
part_type_direction(global.burn_pt0,45,135,0,0);
part_type_orientation(global.burn_pt0,0,19,0.25,3.13,1);
part_type_blend(global.burn_pt0,0);
part_type_alpha3(global.burn_pt0,0,1,0);
part_type_color1(global.burn_pt0,37119);
part_type_shape(global.burn_pt0,7);

global.burn_pt1 = part_type_create();
part_type_size(global.burn_pt1,0,0.10,0,0);
part_type_scale(global.burn_pt1,1,1);
part_type_life(global.burn_pt1,60,60);
part_type_gravity(global.burn_pt1,0,0);
part_type_speed(global.burn_pt1,4,6,-0.03,0.2);
part_type_direction(global.burn_pt1,45,135,0,0);
part_type_orientation(global.burn_pt1,0,0,0,0,1);
part_type_blend(global.burn_pt1,1);
part_type_alpha3(global.burn_pt1,0,1,0);
part_type_color1(global.burn_pt1,32252);
part_type_sprite(global.burn_pt1,spr_TMC_PL_Glow_2_spr,1,1,0);

global.burn_pt2 = part_type_create();
part_type_size(global.burn_pt2,0.5,2,0.02,0);
part_type_scale(global.burn_pt2,1,1);
part_type_life(global.burn_pt2,66,175);
part_type_gravity(global.burn_pt2,0,0);
part_type_speed(global.burn_pt2,1.13,2,0,0);
part_type_direction(global.burn_pt2,45,135,0,0);
part_type_orientation(global.burn_pt2,0,52,0.06,1.31,0);
part_type_blend(global.burn_pt2,0);
part_type_alpha3(global.burn_pt2,0,0.07,0);
part_type_color2(global.burn_pt2,9427709,16777215);
part_type_sprite(global.burn_pt2,spr_TMC_PL_Cloud_2_spr_0_part,1,1,0);

//Creating Emitters
global.burn_pt0 = part_emitter_create(global.ps);
global.burn_pt1 = part_emitter_create(global.ps);
global.burn_pt2 = part_emitter_create(global.ps);


//Creating Particle Types
//Boom
global.toru_hit = part_type_create();
part_type_shape(global.toru_hit, pt_shape_smoke);
part_type_size(global.toru_hit, 1, 2, -0.10, 0);
part_type_scale(global.toru_hit, 1, 1);
part_type_orientation(global.toru_hit, 0, 348, 3, 0, 0);
part_type_color3(global.toru_hit, 2743039, 1442044, 255);
part_type_alpha3(global.toru_hit, 1, 0.51, 0);
part_type_blend(global.toru_hit, 1);
part_type_life(global.toru_hit, 10, 20);
part_type_speed(global.toru_hit, 5, 5, 0, 0);
part_type_direction(global.toru_hit, 0, 360, 0, 0);
part_type_gravity(global.toru_hit, 0, 0);

//Smoke
global.toru_hit2 = part_type_create();
part_type_shape(global.toru_hit2, pt_shape_smoke);
part_type_size(global.toru_hit2, 1, 2, 0.02, 0);
part_type_scale(global.toru_hit2, 1, 1);
part_type_orientation(global.toru_hit2, 0, 0, 0, 0, 0);
part_type_color3(global.toru_hit2, 14483681, 14483679, 10271695);
part_type_alpha3(global.toru_hit2, 0, 0.10, 0);
part_type_blend(global.toru_hit2, 0);
part_type_life(global.toru_hit2, 20, 40);
part_type_speed(global.toru_hit2, 3, 6, 0, 0);
part_type_direction(global.toru_hit2, 0, 360, 0, 0);
part_type_gravity(global.toru_hit2, 0, 0);

//Debris_copy
global.toru_hit3 = part_type_create();
part_type_shape(global.toru_hit3, pt_shape_pixel);
part_type_size(global.toru_hit3, 2, 2, 0, 0);
part_type_scale(global.toru_hit3, 1, 1);
part_type_orientation(global.toru_hit3, 0, 351, 10, 0, 0);
part_type_color3(global.toru_hit3, 16777215, 16777215, 16777215);
part_type_alpha3(global.toru_hit3, 1, 0.56, 0);
part_type_blend(global.toru_hit3, 0);
part_type_life(global.toru_hit3, 30, 60);
part_type_speed(global.toru_hit3, 5, 10, 0, 0);
part_type_direction(global.toru_hit3, -41, 224, 0, 0);
part_type_gravity(global.toru_hit3, 0.20, 270);

//Creating Emitters
global.toru_hit = part_emitter_create(global.ps);
global.toru_hit3 = part_emitter_create(global.ps);
global.toru_hit3 = part_emitter_create(global.ps);



//global.blizzard1
global.blizzard1 = part_type_create();
part_type_size(global.blizzard1,0.02,0.05,0,0.00);
part_type_scale(global.blizzard1,1,1);
part_type_life(global.blizzard1,15,117);
part_type_gravity(global.blizzard1,0.03,270);
part_type_speed(global.blizzard1,29,34.75,0,0.56);
part_type_direction(global.blizzard1,216,234,-0.50,0.56);
part_type_orientation(global.blizzard1,0,359,0.91,2.63,1);
part_type_blend(global.blizzard1,1);
part_type_alpha3(global.blizzard1,1,1,0);
part_type_color3(global.blizzard1,9408399,8816262,8487297);
part_type_sprite(global.blizzard1,spr_particles_snow,0,1,1);

//global.blizzard2
global.blizzard2 = part_type_create();
part_type_size(global.blizzard2,0,1.22,0,0);
part_type_scale(global.blizzard2,5,0.66);
part_type_life(global.blizzard2,60,60);
part_type_gravity(global.blizzard2,0.03,270);
part_type_speed(global.blizzard2,20,40,0,0);
part_type_direction(global.blizzard2,188,221,-0.50,0);
part_type_orientation(global.blizzard2,0,0,0,0,1);
part_type_blend(global.blizzard2,1);
part_type_alpha3(global.blizzard2,0.02,0.02,0.02);
part_type_color3(global.blizzard2,16777215,16777215,16711680);
part_type_shape(global.blizzard2,3);

//Creating Emitters
global.blizzard1 = part_emitter_create(global.ps);
global.blizzard2 = part_emitter_create(global.ps);
