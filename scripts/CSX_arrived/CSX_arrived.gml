///CSX_arrived();
/// @description This triggers everytime player finishes moving from place to place.

//show_message("S_random_battle() in CSX_arrived();");
game_time_one_hour_passes();
scene_recover_stay_1hour();
var_map_set(VAR_MODE,VAR_MODE_NORMAL);
scene_bg_location(location_get_current());
	
if location_get_current() == LOCATION_3 && chunk_mark_get("S_0006_34") && !chunk_mark_get("SL3_FELGOR_JOIN") // MUST TRIGGER OR BUG HAPPENS
	_jump("SL3_FELGOR_JOIN","SL3");

	
else if location_get_current() == LOCATION_6 && !chunk_mark_get("SBS_0001_1") 
	scene_jump("","SBS_0001");
else if location_get_current() == LOCATION_42 && (qj_exists("S_0007_Lorn_Joined_QJ") || qj_exists("S_0007_Lorn_Defeat_Refuse_QJ"))
	scene_jump("","SL42");
else if location_get_current() == LOCATION_3 && chunk_mark_get("S_0007_SL1_Bandit_Beatdown_Finish_8") && !chunk_mark_get("S_0008_MAKEOVER_1")
	_jump("S_0008_MAKEOVER","S_0008");
else if location_get_current() == LOCATION_12 && qj_exists("S_0008_QJ") && !chunk_mark_get("S_0008_SL12_REDSPOTTING_1") 
	scene_jump("S_0008_SL12_REDSPOTTING","S_0008");
else if location_get_current() == LOCATION_18 && qj_exists("S_0008_QJ") && !chunk_mark_get("S_0008_REDPASS_INTRO_1") 
	scene_jump("S_0008_REDPASS","S_0008");
else if obj_control.debug_enable && !obj_control.debug_battle_travel_scenes_enabled
	scene_jump("","CS_prelocation");
/*else if obj_control.var_map[? RBS_IMP_D_3_1_HOUR_COUNTER] >= 5*24 && !chunk_mark_get("RBS_IMP_D_3_1_Continue")
{
	var_map_set(VAR_TEMP_SCENE,"RBS_IMP_D_3_1_Continue");
	scene_jump("Selectedscene","CS_extra_RBS");
}*/
else if !CSX_random_battle()
{
	scene_jump("","CS_prelocation");
}