///battle_char_debuff_apply(char,debuff,level,turns,source_char_id[optional]);
/// @description
/// @param char
/// @param debuff
/// @param level
/// @param turns
/// @param source_char_id[optional]

var char_map = argument[0];
var debuff = argument[1];
var level = argument[2];
var turns = argument[3];
if argument_count > 4 var source_char_id = argument[4];
else var source_char_id = noone;

var debuff_index = ds_list_find_index(char_map[? GAME_CHAR_DEBUFF_LIST],debuff);
if debuff_index < 0
{
	ds_list_add(char_map[? GAME_CHAR_DEBUFF_LIST],debuff);
	ds_list_add(char_map[? GAME_CHAR_DEBUFF_TURN_LIST],turns);
	ds_list_add(char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST],level);
	ds_list_add(char_map[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST],source_char_id);
}
else if ds_list_find_value(char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST],debuff_index) < level
{
	ds_list_replace(char_map[? GAME_CHAR_DEBUFF_TURN_LIST],debuff_index,turns);
	ds_list_replace(char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST],debuff_index,level);
	ds_list_replace(char_map[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST],debuff_index,source_char_id);
}
else if ds_list_find_value(char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST],debuff_index) == level && ds_list_find_value(char_map[? GAME_CHAR_DEBUFF_TURN_LIST],debuff_index) < turns
{
	ds_list_replace(char_map[? GAME_CHAR_DEBUFF_TURN_LIST],debuff_index,turns);
	ds_list_replace(char_map[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST],debuff_index,source_char_id);
}