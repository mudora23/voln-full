///string_copy_prefix(str,copy_until_char);
/// @description
/// @param str
/// @param copy_until_char

var str = argument0;
var copy_until_char = argument1;
var returning_str = "";

for(var i = 1; i <= string_length(str);i++)
{
    var the_char = string_char_at(str,i);
    if the_char != copy_until_char
        returning_str += the_char;
    else
        break;
}
return returning_str;