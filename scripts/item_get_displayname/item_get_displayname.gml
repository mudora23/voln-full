///item_get_displayname(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	var item_map = item_get_map(name);
	var item_des = item_map[? ITEM_DISPLAYNAME];
	//var item_des = "";
	return item_des;
	
}