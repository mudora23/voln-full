///location_get_region(locationName);
/// @description 
/// @param locationName
with obj_control
{
	if ds_map_exists(location_map,argument0)
	{
		return ds_map_find_value(location_map[? argument0],"region");
	}
	return "";
}