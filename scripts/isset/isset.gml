///isset(value[,ignoreblank])

gml_pragma("forceinline");

return (!(is_undefined(argument[0]) || (argument_count == 2 && argument[1] && argument[0] == "")))


