///draw_sprite_in_area(spr,x,y,w,h,color[optional]);
/// @description
/// @param spr
/// @param x
/// @param y
/// @param w
/// @param h
/// @param color[optional]

if is_string(argument[0])
{
	if sprite_exists(asset_get_index(argument[0]))
		var thumbnail_spr = asset_get_index(argument[0]); 
	else if sprite_exists(asset_get_index(string_lower(argument[0])))
		var thumbnail_spr = asset_get_index(string_lower(argument[0]));
	else
		var thumbnail_spr = spr_white;
}
else if sprite_exists(argument[0])
	var thumbnail_spr = argument[0];
else
	var thumbnail_spr = spr_white;
	
var thumbnail_spr_w = sprite_get_width(thumbnail_spr);
var thumbnail_spr_h = sprite_get_height(thumbnail_spr);

var thumbnail_x = argument[1];
var thumbnail_y = argument[2];
var thumbnail_w = argument[3];
var thumbnail_h = argument[4];
if argument_count > 5
	var thumbnail_color = argument[5];
else
	var thumbnail_color = c_white;

var thumbnail_wratio =  thumbnail_w / thumbnail_spr_w;
var thumbnail_hratio = thumbnail_h / thumbnail_spr_h;
var thumbnail_ratio = min(thumbnail_wratio,thumbnail_hratio);

draw_sprite_ext(thumbnail_spr,0,thumbnail_x+(thumbnail_w-(thumbnail_spr_w*thumbnail_ratio))/2,thumbnail_y+(thumbnail_h-(thumbnail_spr_h*thumbnail_ratio))/2,thumbnail_ratio,thumbnail_ratio,0,thumbnail_color,draw_get_alpha());
