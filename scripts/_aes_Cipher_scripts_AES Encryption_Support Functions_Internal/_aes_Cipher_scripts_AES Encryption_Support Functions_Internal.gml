///_aes_Cipher(state, RoundKey)
var rnd = 0, state = argument0, RoundKey = argument1;
var Nk = global._aes_Nk, Nb = global._aes_Nb, Nr = global._aes_Nr, Rcon = global._aes_Rcon;

_aes_AddRoundKey(state, RoundKey, 0)

for(rnd = 1; rnd < Nr; ++rnd)
{
  _aes_SubBytes(state);
  _aes_ShiftRows(state);
  _aes_MixColumns(state);    
  _aes_AddRoundKey(state, RoundKey, rnd);    
}

_aes_SubBytes(state);
_aes_ShiftRows(state);
_aes_AddRoundKey(state, RoundKey, Nr);

