///_aes_unBlockCopy(block)
var a = argument0, b, w = array_length_2d(a, 0), h = array_height_2d(a), i,j;

for (i=0;i<w;++i) {
    for (j=0;j<h;++j) {
        b[(i*w)+j] = a[@i,j];
    }
}

return b;

