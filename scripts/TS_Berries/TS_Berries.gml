///TS_Berries();
/// @description

_label("start");

	
_label("TS_BERRIES");
	_dialog("TS_BERRIES");
	_jump("TS_BERRIES_"+choose("CF1","CB1","CM1","MB1","MM1","MM2","YS1","YL1","YM1"));

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafter");	
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_CF1");
	_dialog("TS_BERRIES_CF1_1",true);
	_choice("Try It","TS_BERRIES_CF1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_CF1_2");
		_var_map_set_cond(BERRIES_EFFECT,"CF1","",percent_chance(40));
		_dialog("TS_BERRIES_CF1_2");
		_dialog("TS_BERRIES_CF1_3");
		_execute(0,CS_using,char_get_player(),ITEM_CF1,var_map_get(BERRIES_EFFECT)=="CF1");
		_dialog("TS_BERRIES_CF1_4");
		_ds_map_replace(char_get_player(),GAME_CHAR_LUST,clamp(char_get_info(char_get_player(),GAME_CHAR_LUST)+10,0,char_get_lust_max(char_get_player())));
		____add_item_with_dialog_and_colorscheme(ITEM_CF1,1);
		_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_CB1");
	_dialog("TS_BERRIES_CB1_1",true);
	_choice("Try It","TS_BERRIES_CB1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_CB1_2");
		_var_map_set_cond(BERRIES_EFFECT,"CB1","",percent_chance(40));
		_dialog("TS_BERRIES_CB1_2");
		_dialog("TS_BERRIES_CB1_3");
		_execute(0,CS_using,char_get_player(),ITEM_CB1,var_map_get(BERRIES_EFFECT)=="CB1");
		____add_item_with_dialog_and_colorscheme(ITEM_CB1,1);
		_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_CM1");
	_dialog("TS_BERRIES_CM1_1",true);
	_choice("Try It","TS_BERRIES_CM1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_CM1_2");
		_var_map_set_cond(BERRIES_EFFECT,"CM1","",percent_chance(40));
		_dialog("TS_BERRIES_CM1_2");
		_dialog("TS_BERRIES_CM1_3");
		_execute(0,CS_using,char_get_player(),ITEM_CM1,var_map_get(BERRIES_EFFECT)=="CM1");
		____add_item_with_dialog_and_colorscheme(ITEM_CM1,1);
		_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_MB1");
	_dialog("TS_BERRIES_MB1_1",true);
	_choice("Try It","TS_BERRIES_MB1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_MB1_2");
		_var_map_set_cond(BERRIES_EFFECT,"MB1","",percent_chance(40));
		_dialog("TS_BERRIES_MB1_2");
		_dialog("TS_BERRIES_MB1_3");
		_execute(0,CS_using,char_get_player(),ITEM_MB1,var_map_get(BERRIES_EFFECT)=="MB1");
		____add_item_with_dialog_and_colorscheme(ITEM_MB1,1);
		_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_MM1");
	_dialog("TS_BERRIES_MM1_1",true);
	_choice("Try It","TS_BERRIES_MM1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_MM1_2");
		_var_map_set_cond(BERRIES_EFFECT,"MM1","",percent_chance(40));
		_dialog("TS_BERRIES_MM1_2");
		_dialog("TS_BERRIES_MM1_3");
		_execute(0,CS_using,char_get_player(),ITEM_MM1,var_map_get(BERRIES_EFFECT)=="MM1");
		____add_item_with_dialog_and_colorscheme(ITEM_MM1,1);
		_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_MM2");
	_dialog("TS_BERRIES_MM2_1",true);
	_choice("Try It","TS_BERRIES_MM2_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_MM2_2");
		_var_map_set_cond(BERRIES_EFFECT,"MM2","",percent_chance(40));
		_dialog("TS_BERRIES_MM2_2");
		_dialog("TS_BERRIES_MM2_3");
		_execute(0,CS_using,char_get_player(),ITEM_MM2,var_map_get(BERRIES_EFFECT)=="MM2");
		____add_item_with_dialog_and_colorscheme(ITEM_MM2,1);
		_jump("Selectedsceneafter");


////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_YS1");
	_dialog("TS_BERRIES_YS1_1",true);
	_choice("Try It","TS_BERRIES_YS1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_YS1_2");
		_var_map_set_cond(BERRIES_EFFECT,"YS1","",percent_chance(40));
		_dialog("TS_BERRIES_YS1_2");
		_dialog("TS_BERRIES_YS1_3");
		_dialog("TS_BERRIES_YS1_4");
		_execute(0,CS_using,char_get_player(),ITEM_YS1,var_map_get(BERRIES_EFFECT)=="YS1");
		____add_item_with_dialog_and_colorscheme(ITEM_YS1,1);
		_jump("Selectedsceneafter");


////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_YL1");
	_dialog("TS_BERRIES_YL1_1",true);
	_choice("Try It","TS_BERRIES_YL1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_YL1_2");
		_var_map_set_cond(BERRIES_EFFECT,"YL1","",percent_chance(40));
		_dialog("TS_BERRIES_YL1_2");
		_dialog("TS_BERRIES_YL1_3");
		_execute(0,CS_using,char_get_player(),ITEM_YL1,var_map_get(BERRIES_EFFECT)=="YL1");
		____add_item_with_dialog_and_colorscheme(ITEM_YL1,1);
		_jump("Selectedsceneafter");


////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES_YM1");
	_dialog("TS_BERRIES_YM1_1",true);
	_choice("Try It","TS_BERRIES_YM1_2");
	_choice("Leave it","Selectedsceneafter");
	
	_border(10); // add lines after this
	
	_label("TS_BERRIES_YM1_2");
		_var_map_set_cond(BERRIES_EFFECT,"YM1","",percent_chance(40));
		_dialog("TS_BERRIES_YM1_2");
		_dialog("TS_BERRIES_YM1_3");
		_execute(0,CS_using,char_get_player(),ITEM_YM1,var_map_get(BERRIES_EFFECT)=="YM1");
		____add_item_with_dialog_and_colorscheme(ITEM_YM1,1);
		_jump("Selectedsceneafter");


////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////






/*
_label("TS_BERRIES");
	_dialog("TS_BERRIES",true);
	_choice("You eat the black berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES_A");
	_choice("You don’t eat the black berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES2");
	_dialog("TS_BERRIES2",true);
	_choice("You eat the blue berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES2_A");
	_choice("You don’t eat the blue berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES2_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES3");
	_dialog("TS_BERRIES3",true);
	_choice("You eat the red berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES3_A");
	_choice("You don’t eat the red berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES3_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES3_A");
	_execute(0,scene_char_exp_gain,char_get_player(),char_get_exp_max(char_get_player())*0.2);
	_dialog("TS_BERRIES3_A");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES4");
	_dialog("TS_BERRIES4",true);
	_choice("You eat the orange berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES4_A");
	_choice("You don’t eat the orange berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES4_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES5_A");
	_execute(0,"battle_char_add_long_term_buff",var_map_get(VAR_PLAYER),LONG_TERM_BUFF_ORANGE_BERRY,24);
	_dialog("TS_BERRIES5_A");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES5");
	_dialog("TS_BERRIES5",true);
	_choice("You eat the purple berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES5_A");
	_choice("You don’t eat the purple berry.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_BERRIES5_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_BERRIES5_A");
	_execute(0,"battle_char_add_long_term_debuff",var_map_get(VAR_PLAYER),LONG_TERM_DEBUFF_PURPLE_BERRY,24);
	_dialog("TS_BERRIES5_A");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
*/




	_show("Pummdu",spr_char_Pum_hh_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("TS_Love_Advice",true);
	_choice("“"+string(char_get_info(char_get_player(),GAME_CHAR_DISPLAYNAME))+", at your service, deym.”","TS_Love_Advice_Intro1");
	_choice("“Tell me your name first.”","TS_Love_Advice_Intro2");
	_choice("“Back away from the wag-swag, jill!”","TS_Love_Advice_Intro3");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Intro1");
	_dialog("TS_Love_Advice_Intro1");
	_dialog("TS_Love_Advice_Intro1_2");
	_show("Pummdu",spr_char_Pum_hh_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("TS_Love_Advice_Intro1_3");
	_dialog("TS_Love_Advice_Intro1_4");
	_jump("TS_Love_Advice_Intro1_5");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Intro2");
	_dialog("TS_Love_Advice_Intro2");
	_dialog("TS_Love_Advice_Intro2_2");
	_dialog("TS_Love_Advice_Intro2_3");
	_dialog("TS_Love_Advice_Intro2_4");
	_dialog("TS_Love_Advice_Intro2_5");
	_jump("TS_Love_Advice_Intro1_5");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Intro3");
	_dialog("TS_Love_Advice_Intro3");
	_dialog("TS_Love_Advice_Intro3_2");
	_dialog("TS_Love_Advice_Intro3_3");
	_dialog("TS_Love_Advice_Intro3_4");
	_dialog("TS_Love_Advice_Intro3_5");
	_jump("TS_Love_Advice_Intro1_5");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


_label("TS_Love_Advice_Intro1_5");
	_dialog("TS_Love_Advice_Intro1_5");
	_dialog("TS_Love_Advice_Intro1_6",true);
	_choice("Dodge her","TS_Love_Advice_Milk1");
	_choice("Let her catch you","TS_Love_Advice_Milk2");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Milk1");
	_dialog("TS_Love_Advice_Milk1");
	_dialog("TS_Love_Advice_Milk1_2");
	_dialog("TS_Love_Advice_Milk1_3");
	_jump("TS_Love_Advice_2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Milk2");
	_dialog("TS_Love_Advice_Milk2");
	_dialog("TS_Love_Advice_Milk2_2");
	_dialog("TS_Love_Advice_Milk2_3");
	_dialog("TS_Love_Advice_Milk2_4");
	_dialog("TS_Love_Advice_Milk2_5");
	_dialog("TS_Love_Advice_Milk2_6");
	_dialog("TS_Love_Advice_Milk2_7");
	_jump("TS_Love_Advice_2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_2");
	_dialog("TS_Love_Advice_2");
	_dialog("TS_Love_Advice_3",true);
	_choice("“Okay, try me.”","TS_Love_Advice_3_Help1");
	_choice("“Please never bother me again.”","TS_Love_Advice_3_Help2");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Help1");
	_dialog("TS_Love_Advice_3_Help1",true);
	_choice("“Pay a boy with sweet words and a gift or two, then go in for the prize.  Works every time.”","TS_Love_Advice_3_Advice1");
	_choice("“You should be of assistance to him whenever you can.  After a while, see if he’s interested.”","TS_Love_Advice_3_Advice2");
	_choice("“Make friends with him first.  Kindness, patience, and love will surely find you someone nice.”","TS_Love_Advice_3_Advice3");
	_choice("“Grab a boy by the orbs.  They love that!”","TS_Love_Advice_3_Advice4");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice1");
	_char_add_opinion(NAME_PUMMDU,3);
	_dialog("TS_Love_Advice_3_Advice1");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice2");
	_char_add_opinion(NAME_PUMMDU,5);
	_dialog("TS_Love_Advice_3_Advice2");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice3");
	_char_add_opinion(NAME_PUMMDU,4);
	_dialog("TS_Love_Advice_3_Advice3");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice4");
	_char_add_opinion(NAME_PUMMDU,1);
	_dialog("TS_Love_Advice_3_Advice4");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice_End");
	_dialog("TS_Love_Advice_3_Advice_End");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice_3_Advice_End_2");
	_jump("Selectedsceneafter","CS_extra_TS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Help2");
	_dialog("TS_Love_Advice_3_Help2");
	_dialog("TS_Love_Advice_3_Help2_2");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice_3_Help2_3");
	_jump("Selectedsceneafter","CS_extra_TS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2");
	_dialog("TS_Love_Advice2");
	_show("Pummdu",spr_char_Pum_hh_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("TS_Love_Advice2_2",true);
	_choice("“You want to talk?”","TS_Love_Advice2_2_A");
	_choice("“I don’t have the time, thank you.”","TS_Love_Advice2_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2_2_A");
	_dialog("TS_Love_Advice2_2_A",true);
	_choice("Invite","TS_Love_Advice2_2_A_A");
	_choice("Dismiss","TS_Love_Advice2_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2_2_A_A");	
	_dialog("TS_Love_Advice2_2_A_A");
	_dialog("TS_Love_Advice2_2_A_A_2",true);
	_choice("“I’d like that very much!”","TS_Love_Advice2_2_A_A_2_A");
	_choice("“No, thanks.”","TS_Love_Advice2_2_A_A_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice2_2_A_A_2_A");
	_dialog("TS_Love_Advice2_2_A_A_2_A");
	_dialog("TS_Love_Advice2_2_A_A_2_A_SEX");
	_jump("TS_Love_Advice2_2_B");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice2_2_A_A_2_B");
	_dialog("TS_Love_Advice2_2_A_A_2_B");
	_dialog("TS_Love_Advice2_2_A_A_2_B_2");
	_dialog("TS_Love_Advice2_2_A_A_2_B_3");
	_dialog("TS_Love_Advice2_2_A_A_2_B_4");
	_dialog("TS_Love_Advice2_2_A_A_2_B_5");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice2_2_A_A_2_B_6");
	_jump("Selectedsceneafter","CS_extra_TS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice2_2_B");
	_dialog("TS_Love_Advice2_2_B");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice2_2_B_2");
	_jump("Selectedsceneafter","CS_extra_TS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////