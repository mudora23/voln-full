///_char_add_opinion(NAME,amount);
/// @description  
/// @param NAME
/// @param amount

if scene_is_current()
{
	char_add_char_opinion(argument[0],obj_control.var_map[? VAR_PLAYER],argument[1]);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);