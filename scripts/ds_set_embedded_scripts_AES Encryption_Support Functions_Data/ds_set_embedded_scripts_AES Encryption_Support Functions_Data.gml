///ds_set_embedded(struture,value,key/index,...)

var _ds = argument[0], c = argument_count;

for (var i = 2; i < c; ++i) {
    if (is_string(argument[i])) {
        if (i == c - 1)
            _ds[? argument[i]] = argument[1];
        else
            _ds = _ds[? argument[i]];
    } else {
        if (i == c - 1)
            _ds[| argument[i]] = argument[1];
        else
            _ds = _ds[| argument[i]];
    }
}
