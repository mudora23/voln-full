///_change_bg(ID,sprite_target,FADE_SPEED,sec);
/// @description
/// @param ID
/// @param sprite_target
/// @param FADE_SPEED
/// @param sec
if scene_is_current()
{
	if obj_control.var_map[? VAR_SCRIPT_PROC_STEP] == 0
	{
		if scene_sprite_list_exists(argument0)
		{
			if !scene_sprite_list_sprite_name_exists(sprite_get_name_ext(argument1))
			{
				scene_sprite_list_change_alpha_ext(argument0,1,argument2);
				scene_sprite_list_change_sprite(argument0,spr_bg_black);
			}
			script_execute_add(argument3,scene_sprite_list_change_sprite,argument0,argument1);
		}
		else
		{
			scene_sprite_list_add(argument0,argument1,5,5,1,1,0,1,argument2,SMOOTH_SLIDE_NORMAL_SEC,ORDER_BG,1);
		}
	}
	
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);