///_jump(label/posi,scr);
/// @description
/// @param label/posi
/// @param scr

if scene_is_current()
{
	if argument_count > 1 scene_jump(argument[0],argument[1]);
	else if argument_count > 0 scene_jump(argument[0]);
	else scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);