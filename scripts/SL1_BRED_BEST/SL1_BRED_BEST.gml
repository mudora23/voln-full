///SL1_BRED_BEST();
/// @description Nirholt - BRED BEST

_label("start");

_label("SL1_BRED_BEST_INTRO");
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_BRED_BEST_INTRO",true);
	_jump("SL1_BRED_BEST_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_RETURN");
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_BRED_BEST_RETURN",true);
	_jump("SL1_BRED_BEST_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_Choice");

	if chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_B_16_B_5") && !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_FINISH") && inventory_item_count(ITEM_Salo_wurm) >= 5
		_choice("Finish the quest.","S_0004_FELGOR_INTRO_EVENT2_FINISH","S_0004");
	else if chunk_mark_get("S_0004_FELGOR_INTRO_EVENT2_6") && !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT2_6_A")
		_choice("Talk about the quest.","S_0004_FELGOR_INTRO_EVENT_B_16_R","S_0004");
	else if chunk_mark_get("S_0004_FELGOR_INTRO_EVENT2_FINISH_7") && !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A")
		_choice("Talk about the quest.","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_R","S_0004");
	else
		_void();
		
	_choice("“Dairy products, please.”","SL1_BRED_BEST_ITEM");
	_choice("“I’m looking for breedərs.”","SL1_BRED_BEST_PET");	
	_choice("[?]“Could I use one of the breedərs, please?”","SL1_BRED_BEST_PET_BORROW");	
	_choice("Leave","SL1_BRED_BEST_EXIT");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_ITEM");
	_dialog("SL1_BRED_BEST_ITEM");
	_jump("SL1_BRED_BEST_ITEM_Menu");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_ITEM_Menu");
	// items
	_choice("Exit","SL1_BRED_BEST_Choice");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET");
	_dialog("SL1_BRED_BEST_PET");
	_jump("SL1_BRED_BEST_PET_STORE");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_STORE");

	_store_add_item(ITEM_Blu_Yim);
	_var_map_set(VAR_EXTRA_AFTER_SCENE,"SL1_BRED_BEST");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"SL1_BRED_BEST_PET_STORE");
	_var_map_set(VAR_EXTRA2_AFTER_SCENE,"SL1_BRED_BEST");
	_var_map_set(VAR_EXTRA2_AFTER_SCENE_LABEL,"SL1_BRED_BEST_PET_STORE_EXIT");
	_jump("start","CS_buying");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_BRED_BEST_PET_STORE_EXIT");
	_jump("SL1_BRED_BEST_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_BORROW");
	_dialog("SL1_BRED_BEST_PET_BORROW",true);
	_jump("SL1_BRED_BEST_PET_BORROW_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_BORROW_Choice");
	_choice("“Okay, sure.”","SL1_BRED_BEST_PET_BORROW_A");
	_choice("“Forget it.”","SL1_BRED_BEST_PET_BORROW_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_BORROW_A");
	_dialog("SL1_BRED_BEST_PET_BORROW_A",true);
	_jump("SL1_BRED_BEST_PET_BORROW_Menu");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_BORROW_Menu");
	// items
	_choice("Exit","SL1_BRED_BEST_PET_BORROW_A_EXIT");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_BORROW_A_EXIT");
	_dialog("SL1_BRED_BEST_PET_BORROW_A_EXIT",true);
	_jump("SL1_BRED_BEST_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_BRED_BEST_PET_BORROW_B");
	_dialog("SL1_BRED_BEST_PET_BORROW_B",true);
	_jump("SL1_BRED_BEST_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////	

_label("SL1_BRED_BEST_EXIT");
	/*if chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_B_16_B_5") && !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_FINISH")
		_jump("S_0004_FELGOR_INTRO_EVENT_FINISH","S_0004");
	else
		_void();*/
	_dialog("SL1_BRED_BEST_EXIT",true);
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////	




		
