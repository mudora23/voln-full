///CS_extra_RBS();
/// @description

_label("start");

_label("Selectedscene");
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	
	_jump("Selectedsceneafter","CS_extra_RBS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedscene_jar_to_item");
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add_ext(VAR_JAR,-1,0,var_map_get(VAR_JAR));
	_dialog("        1 jar is removed from the inventory.");
	_execute(0,inventory_add_item,var_map_get(VAR_TEMP_ITEM),1);
	_dialog("        You receive "+string(item_get_displayname(var_map_get(VAR_TEMP_ITEM)))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_jump("Selectedsceneafter","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("Selectednoscene");
	_jump("Selectednoscene","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafter");
	_jump("Selectedsceneafter","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Noscenestoselect");
	_jump("Noscenestoselect","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafternorelieve");
	_jump("Selectedsceneafternorelieve","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
	// RBS
	_border(1000); // add lines after this
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

_label("RBS_ORK_DV_1_1");
	_dialog("RBS_ORK_DV_1_1",true);
	_choice_selectedscene_RBS("Have her suck your pud","Selectedscene","RBS_ORK_DV_1_2A");
	_choice_selectedscene_RBS("Tit-fikk her","Selectedscene","RBS_ORK_DV_1_2B");
	_choice_selectedscene_RBS("Warm her up with your fingers","Selectedscene","RBS_ORK_DV_1_2C");
	_block();
	
	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_1_2A");
		_dialog("RBS_ORK_DV_1_2A_1");
		_dialog("RBS_ORK_DV_1_2A_2");
		_dialog("RBS_ORK_DV_1_2A_3");
		_jump("Selectedsceneafter");
		_block();
		
	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_1_2B");
		_dialog("RBS_ORK_DV_1_2B_1");
		_dialog("RBS_ORK_DV_1_2B_2");
		_dialog("RBS_ORK_DV_1_2B_3");
		_jump("Selectedsceneafter");
		_block();	
		
	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_1_2C");
		_dialog("RBS_ORK_DV_1_2C_1");
		_dialog("RBS_ORK_DV_1_2C_2",true);
		_choice_selectedscene_RBS("Have her suck your pud","Selectedscene","RBS_ORK_DV_1_2C_3A");
		_choice_selectedscene_RBS("Tit-fikk her","Selectedscene","RBS_ORK_DV_1_2C_3B");
		_block();
		
	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_1_2C_3A");
		_dialog("RBS_ORK_DV_1_2C_3A_1");
		_jump("Selectedsceneafter");
		_block();
	
	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_1_2C_3B");
		_dialog("RBS_ORK_DV_1_2C_3B_1",true);
		_choice_selectedscene_RBS("Keep your word","Selectedscene","RBS_ORK_DV_1_2C_3B_2A");
		_choice_selectedscene_RBS("Be a piece of shit","Selectedscene","RBS_ORK_DV_1_2C_3B_2B");
		_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ORK_DV_2_1");
	_dialog("RBS_ORK_DV_2_1");
	_dialog("RBS_ORK_DV_2_2");
	_dialog("RBS_ORK_DV_2_3",true);
	_choice_selectedscene_RBS("Overpower her","Selectedscene","RBS_ORK_DV_2_4A");
	_choice_selectedscene_RBS("Outmaneuver her","Selectedscene","RBS_ORK_DV_2_4B");
	_choice_selectedscene_RBS("Outsmart her","Selectedscene","RBS_ORK_DV_2_4C");
	_block();

	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_2_4A");
		if irandom_range(1,4)+char_get_skill_level(char_get_player(),SKILL_STR_1)+char_get_skill_level(char_get_player(),SKILL_STR_2)+char_get_skill_level(char_get_player(),SKILL_STR_3)+char_get_skill_level(char_get_player(),SKILL_STR_4)+char_get_skill_level(char_get_player(),SKILL_STR_5)+char_get_skill_level(char_get_player(),SKILL_STR_6)>=4
			_jump("RBS_ORK_DV_2_4_Win");
		else
			_jump("RBS_ORK_DV_2_4_Loss");

	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_2_4B");
		if irandom_range(1,4)+char_get_skill_level(char_get_player(),SKILL_DEX_1)+char_get_skill_level(char_get_player(),SKILL_DEX_2)+char_get_skill_level(char_get_player(),SKILL_DEX_3)+char_get_skill_level(char_get_player(),SKILL_DEX_4)+char_get_skill_level(char_get_player(),SKILL_DEX_5)+char_get_skill_level(char_get_player(),SKILL_DEX_6)>=4
			_jump("RBS_ORK_DV_2_4_Win");
		else
			_jump("RBS_ORK_DV_2_4_Loss");

	_border(10); // add lines after this
	
	_label("RBS_ORK_DV_2_4C");
		if irandom_range(1,4)+char_get_skill_level(char_get_player(),SKILL_WIS_1)/2+char_get_skill_level(char_get_player(),SKILL_WIS_2)/2+char_get_skill_level(char_get_player(),SKILL_WIS_3)/2+char_get_skill_level(char_get_player(),SKILL_WIS_4)/2+char_get_skill_level(char_get_player(),SKILL_WIS_5)/2+char_get_skill_level(char_get_player(),SKILL_WIS_6)/2+char_get_skill_level(char_get_player(),SKILL_INT_1)/2+char_get_skill_level(char_get_player(),SKILL_INT_2)/2+char_get_skill_level(char_get_player(),SKILL_INT_3)/2+char_get_skill_level(char_get_player(),SKILL_INT_4)/2+char_get_skill_level(char_get_player(),SKILL_INT_5)/2+char_get_skill_level(char_get_player(),SKILL_INT_6)/2>=4
			_jump("RBS_ORK_DV_2_4_Win");
		else
			_jump("RBS_ORK_DV_2_4_Loss");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ORK_D_4_1");
	_dialog("RBS_ORK_D_4_1",true);
	_choice("Steal the ork’s virginity.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_D_4_1_Virgin1");
	_choice("Let the ork keep her virginity and instead stick it in her tight snug.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_D_4_1_Virgin2");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ORK_D_5_1");
	_dialog("RBS_ORK_D_5_1");
	_dialog("RBS_ORK_D_5_2");
	_dialog("RBS_ORK_D_5_3");
	_dialog("RBS_ORK_D_5_4");
	_dialog("RBS_ORK_D_5_5");
	_gameover();
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ORK_D_6_1");
	_dialog("RBS_ORK_D_6_1");
	_dialog("RBS_ORK_D_6_2");
	_dialog("RBS_ORK_D_6_3");
	_dialog("RBS_ORK_D_6_4",true);
	_choice_selectedscene_RBS(" Kneel","Selectedscene","RBS_ORK_D_6_5");
	_choice_selectedscene_RBS("“Fikk You!”","Selectedscene","RBS_ORK_D_6_5");
	_choice_selectedscene_RBS("Spit on her","Selectedscene","RBS_ORK_D_6_5");
	_choice_selectedscene_RBS("“Suck my horn!“","Selectedscene","RBS_ORK_D_6_5");
	_choice_selectedscene_RBS("Remain where you are","Selectedscene","RBS_ORK_D_6_5");
	_choice_selectedscene_RBS("“No, YOU kneel!“","Selectedscene","RBS_ORK_D_6_5");
	_block();
	
	_label("RBS_ORK_D_6_5");
		_dialog("RBS_ORK_D_6_5");
		_dialog("RBS_ORK_D_6_6");
		_dialog("RBS_ORK_D_6_7");
		_gameover();
		_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ORK_DV_9_1");
	_dialog("RBS_ORK_DV_9_1",true);
	_choice("Don’t use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_9_1_A");
	_choice("Use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_9_1_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ORK_DV_24_1");
	_dialog("RBS_ORK_DV_24_1",true);
	_choice("Don’t use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_24_1_A");
	_choice("Use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_24_1_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ORK_DV_29_1");
	_dialog("RBS_ORK_DV_29_1",true);
	_choice("Don’t use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_29_1_A");
	_choice("Use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_29_1_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ORK_DV_32_1");
	_dialog("RBS_ORK_DV_32_1",true);
	_choice("Don’t use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_32_1_A");
	_choice("Use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_32_1_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ORK_DV_35_1");
	_dialog("RBS_ORK_DV_35_1",true);
	_choice("Don’t use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_35_1_A");
	_choice("Use ork","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_35_1_B");
	_choice("[!]Play the gurl","Selectedscene_jar_to_item","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ORK_DV_35_1_C");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

	
	