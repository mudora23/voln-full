///var_map_var_map_get(GAME_MAP_KEY,GAME_MAP_MAP_KEY);
/// @description
/// @param GAME_MAP_KEY
/// @param GAME_MAP_MAP_KEY
var var_key = argument[0];
var var_key_key = argument[1];

if ds_map_exists(obj_control.var_map,var_key) && ds_map_exists(ds_map_find_value(obj_control.var_map,var_key),ds_map_find_value(obj_control.var_map,var_key_key))
	return ds_map_find_value(ds_map_find_value(obj_control.var_map,var_key),ds_map_find_value(obj_control.var_map,var_key_key));
else
{
	////show_debug_message("ERROR: var_map_var_map_get(GAME_MAP_KEY,GAME_MAP_MAP_KEY) - key not exists! key1: "+string(var_key)+" key2: "+string(var_key_key));
	return NOT_EXISTS;
}