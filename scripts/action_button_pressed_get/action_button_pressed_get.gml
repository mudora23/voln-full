///action_button_pressed_get();
/// @description
return action_button_keyboard_pressed_get() || action_button_mouse_pressed_get();