///_show(ID,sprite,xnum,xnum_target,ynum,ynum_target,alpha,alpha_target,fade_speed,slide_speed,order,size_ratio);
/// @description  
/// @param ID
/// @param sprite
/// @param xnum
/// @param xnum_target
/// @param ynum
/// @param ynum_target
/// @param alpha
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
/// @param size_ratio

if scene_is_current()
{
	scene_sprite_list_add(argument0,argument1,argument2,argument3,argument4,argument5,argument6,argument7,argument8,argument9,argument10,argument11);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);