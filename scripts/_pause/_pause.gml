///_pause(hardsec,softsec,auto);
/// @description
/// @param hardsec
/// @param softsec
/// @param auto
if scene_is_current()
{
	// clicking
	if (action_button_pressed_get() || action_button_hold_get() >= 0.8) && obj_control.var_map[? VAR_SCRIPT_PROC_STEP]/room_speed >= argument0 && !menu_exists()
		scene_jump_next();
	// non-clicking
	else if obj_control.var_map[? VAR_SCRIPT_PROC_STEP]/room_speed >= argument0 + argument1 && argument2
		scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);