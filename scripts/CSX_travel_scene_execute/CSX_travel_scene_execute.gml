///CSX_travel_scene_execute();
/// @description - return true if there's an event executed; false otherwise.

var player = obj_control.var_map[? VAR_PLAYER];
var ts_list = ds_list_create();

// Berries
if location_get_region(location_get_current()) == REGION_DWI || location_get_region(location_get_current()) == REGION_SESKA
	ds_list_add(ts_list,"TS_Berries","TS_Berries"); // more chances

// Zombie Mistress
if location_get_region(location_get_current()) == REGION_SESKA &&
   obj_control.var_map[? VAR_TIME_HOUR] >= 13 &&
   obj_control.var_map[? VAR_TIME_HOUR] <= 17 &&
   !chunk_mark_get("TS_ZOMBIE_MISTRESS")
	ds_list_add(ts_list,"TS_ZOMBIE_MISTRESS");
	
// Kudi and Elk
if (location_get_current() == LOCATION_4 || location_get_current() == LOCATION_5 || location_get_current() == LOCATION_6 || location_get_current() == LOCATION_7 || location_get_current() == LOCATION_8 || location_get_current() == LOCATION_43 || location_get_current() == LOCATION_44 || location_get_current() == LOCATION_45) &&
   !chunk_mark_get("TS_Kudi_and_Elk")
	ds_list_add(ts_list,"TS_Kudi_and_Elk");
	
// Elf and Imp Orgy
if (location_get_current() == LOCATION_47 || location_get_current() == LOCATION_48 || location_get_current() == LOCATION_49)
	ds_list_add(ts_list,"TS_Elf_and_Imp_Orgy");
	
// Love Advice
if player[? GAME_CHAR_LEVEL] >= 5 &&
   !chunk_mark_get("TS_Love_Advice")
	ds_list_add(ts_list,"TS_Love_Advice");
	
// Love Advice 2
if player[? GAME_CHAR_LEVEL] >= 5 &&
   chunk_mark_get("TS_Love_Advice") &&
   !chunk_mark_get("TS_Love_Advice_3_Help2_3") &&
   !chunk_mark_get("TS_Love_Advice2")
	ds_list_add(ts_list,"TS_Love_Advice2");

// Mushroom
if location_get_current() == LOCATION_40 || location_get_current() == LOCATION_9 || location_get_current() == LOCATION_11 || location_get_current() == LOCATION_12 || location_get_current() == LOCATION_22 || location_get_current() == LOCATION_26
    ds_list_add(ts_list,"TS_Mushroom");
if location_get_current() == LOCATION_40 || location_get_current() == LOCATION_46 || location_get_current() == LOCATION_9 || location_get_current() == LOCATION_11 || location_get_current() == LOCATION_12 || location_get_current() == LOCATION_26 || location_get_current() == LOCATION_39
    ds_list_add(ts_list,"TS_Mushroom2");
if location_get_current() == LOCATION_40 || location_get_current() == LOCATION_46 || location_get_current() == LOCATION_9 || location_get_current() == LOCATION_11 || location_get_current() == LOCATION_12 || location_get_current() == LOCATION_22 || location_get_current() == LOCATION_26 || location_get_current() == LOCATION_39
    ds_list_add(ts_list,"TS_Mushroom4");

// item find
if location_get_current() == LOCATION_6 || location_get_current() == LOCATION_7 || location_get_current() == LOCATION_9 || location_get_current() == LOCATION_39 || location_get_current() == LOCATION_41 || location_get_current() == LOCATION_45
    ds_list_add(ts_list,"TS_Item_Find");

// Tonic find
if location_get_current() == LOCATION_2 || location_get_current() == LOCATION_3 || location_get_current() == LOCATION_4 || location_get_current() == LOCATION_8 || location_get_current() == LOCATION_22 || location_get_current() == LOCATION_23 || location_get_current() == LOCATION_24 || location_get_current() == LOCATION_46
    ds_list_add(ts_list,"TS_Tonic_Find");

// Plant Sex
if location_get_current() == LOCATION_46 && !chunk_mark_get("TS_Plant_Sex")
    ds_list_add(ts_list,"TS_Plant_Sex");


if ds_list_size(ts_list) != 0
{
	var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
	ds_list_shuffle(ts_list);
	ds_list_move_unread_story_chunk_to_front(ts_list);
	var_map_set(VAR_TEMP_SCENE,ts_list[| 0]);
	scene_jump("Travelscene","CS_extra_TS");
	ds_list_destroy(ts_list);
	return true;
}
else
{
	ds_list_destroy(ts_list);
	return false;
}

