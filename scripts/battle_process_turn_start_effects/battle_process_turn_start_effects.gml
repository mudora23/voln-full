///battle_process_turn_start_effects();
/// @description
//scene_clear_ext(false,false,true,true,true);

var time_delay_sec = 0;


var char_map = battle_get_current_action_char();
var buff_list = char_map[? GAME_CHAR_BUFF_LIST];
var buff_turn_list = char_map[? GAME_CHAR_BUFF_TURN_LIST];
var buff_level_list = char_map[? GAME_CHAR_BUFF_LEVEL_LIST];
var buff_source_char_id_list = char_map[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST];
var debuff_list = char_map[? GAME_CHAR_DEBUFF_LIST];
var debuff_turn_list = char_map[? GAME_CHAR_DEBUFF_TURN_LIST];
var debuff_level_list = char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST];
var debuff_source_char_id_list = char_map[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST];

// apply effects if appliable

	// recover stamina
	var amount = min(char_get_stamina_regen_per_turn(char_map), char_get_stamina_max(char_map) - char_map[? GAME_CHAR_STAMINA]);
	char_map[? GAME_CHAR_STAMINA] += amount;
	//if amount > 0 draw_floating_text(char_map[? GAME_CHAR_X_FLOATING_TEXT],char_map[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(amount)),COLOR_STAMINA);
	
	
	for(var i = 0;i<ds_list_size(debuff_list);i++)
	{
		var debuff_name = debuff_list[| i];
		var debuff_turn = debuff_turn_list[| i];
		var debuff_level = debuff_level_list[| i];
		var char_source = char_id_get_char(debuff_source_char_id_list[| i]);
		var char_target = char_map;
		
		if battle_get_current_action_char_owner() == OWNER_PLAYER || battle_get_current_action_char_owner() == OWNER_ALLY
			var color_tag = TAG_COLOR_ALLY_NAME;
		else
			var color_tag = TAG_COLOR_ENEMY_NAME;
	
		
		// burn 3 turns (20/25/30% toru damage per turn)
		if debuff_name == DEBUFF_BURN
		{
			var battlelog_pieces_list = ds_list_create();
			ds_list_add(battlelog_pieces_list,text_add_markup(char_map[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,color_tag));
			ds_list_add(battlelog_pieces_list,text_add_markup(" burns",TAG_FONT_N,TAG_COLOR_DAMAGE));
			
			if debuff_level >= 3 var damage = char_get_toru_damage(char_source) * 0.35;
			else if debuff_level >= 2 var damage = char_get_toru_damage(char_source) * 0.3;
			else var damage = char_get_toru_damage(char_source) * 0.25;
			
			var damage_size_adjust = 1;
			var shake_size_adjust = 1;
			if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);voln_play_sfx(sfx_Hit8);damage_size_adjust+=0.2;shake_size_adjust+=0.2;}
			var armor = char_get_toru_armor(char_target);
			var real_damage = damage * clamp(((100-armor)/100),0,1);
			char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage);
			
			//draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"burn!",COLOR_HP,0.4,0.8*damage_size_adjust);
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"burn! -"+string(round(real_damage)),COLOR_HP,0.5,0.8*damage_size_adjust);
		
			ally_get_hit_port(char_target);
			
			ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
			ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),TAG_FONT_N,TAG_COLOR_DAMAGE));
			ds_list_add(battlelog_pieces_list,text_add_markup(" damage!",TAG_FONT_N,TAG_COLOR_NORMAL));
			
			char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
			char_target[? GAME_CHAR_SHAKE_AMOUNT] = 20*shake_size_adjust;
			
			var battle_log = string_append_from_list(battlelog_pieces_list);
			battlelog_add(battle_log);
			ds_list_destroy(battlelog_pieces_list);	
			
			voln_play_sfx(sfx_burn);
			script_execute_add(time_delay_sec,postfx_burn,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],debuff_level);			
			time_delay_sec += BATTLE_PAUSE_TIME_SEC;
		}

	}
	for(var i = 0;i<ds_list_size(buff_list);i++)
	{

	




	}

// remove effects if worn off
for(var i = 0;i<ds_list_size(debuff_list);i++)
{
	debuff_turn_list[| i]-=0.5;
	if debuff_turn_list[| i] <= 0.5 && debuff_list[| i] != DEBUFF_STUN
	{
		ds_list_delete(debuff_list,i);
		ds_list_delete(debuff_turn_list,i);
		ds_list_delete(debuff_level_list,i);
		ds_list_delete(debuff_source_char_id_list,i);
		
		i--;
	}
}
for(var i = 0;i<ds_list_size(buff_list);i++)
{
	buff_turn_list[| i]-=0.5;
	if buff_turn_list[| i] <= 0.5 && buff_list[| i] != BUFF_HIDE
	{
		ds_list_delete(buff_list,i);
		ds_list_delete(buff_turn_list,i);
		ds_list_delete(buff_level_list,i);
		ds_list_delete(buff_source_char_id_list,i);
		i--;
	}
}

if battle_char_get_debuff_level(char_map,DEBUFF_STUN) > 0
{
	battle_process_wait_CMD();
	
}
else if char_map[? GAME_CHAR_OWNER] == OWNER_PLAYER && char_map[? GAME_CHAR_AI] == AI_NONE
{
	script_execute_add_unique(time_delay_sec,battle_process_player);
}
else
{
	script_execute_add_unique(time_delay_sec,battle_process_AI);
	//script_execute_add_unique(0,battle_process_player);
}