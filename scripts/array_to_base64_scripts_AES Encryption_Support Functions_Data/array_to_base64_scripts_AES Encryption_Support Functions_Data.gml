///array_to_base64(str)
// GMLscripts.com/license
// Modified for AES arrays

var arr, len, pad, tab, b64, i, bin;
arr = argument0;
len = array_length_1d(arr);
pad = "=";
tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
b64 = "";

for (i=0; i<len; i+=3) {
    bin[0] = arr[@ i];
    if !(i+1 >= len)
        bin[1] = arr[@ i + 1];
    if !(i+2 >= len)            
        bin[2] = arr[@ i + 2];
        
    b64 += string_char_at(tab,1+(bin[0]>>2));
    b64 += string_char_at(tab,1+(((bin[0]&3)<<4)|(bin[1]>>4)));
    
    if (i+1 >= len) b64 += pad;
    else b64 += string_char_at(tab,1+(((bin[1]&15)<<2)|(bin[2]>>6)));
    
    if (i+2 >= len) b64 += pad;
    else b64 += string_char_at(tab,1+(bin[2]&63));
}

return b64;

