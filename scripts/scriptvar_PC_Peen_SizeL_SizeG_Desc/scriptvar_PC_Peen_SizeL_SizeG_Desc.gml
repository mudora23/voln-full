///scriptvar_PC_Peen_SizeL_SizeG_Desc();
/// @description
var gnum = var_map_get(PC_Peen_SizeG_N);
var lnum = var_map_get(PC_Peen_SizeL_N);
var returning_str = "";

if gnum < 1 returning_str+= choose("thin, ","narrow, ","slender, ");
if number_in_range_strictly_less(gnum,2,2.8) returning_str+= choose("thick, ","heavy, ","fat, ");
if number_in_range_strictly_less(gnum,2.8,3.5) returning_str+= choose("bloated, ","distended, ","extra-wide, ");
if gnum >= 3.5 returning_str+= choose("horrifying, ","hole-busting, ","orifice-wrecking, ");
if gnum >= 1 && lnum >= 7 returning_str+= "large, ";
if lnum < 4 returning_str+= choose("cute, ","dainty, ");
if lnum < 2 returning_str+= "tiny, ";
if gnum >= 1 && number_in_range_strictly_less(lnum,4,7) returning_str+= choose("good-sized, ","proportionate, ","normal, ");
if lnum >= 7 returning_str+= choose("long, ","low-hanging, ");
if gnum >= 1.7 && lnum >= 9 returning_str+= choose("huge, ","whacky, ","monstrous, ","ludicrous, ");

if string_length(returning_str) > 2
    returning_str = string_copy(returning_str,1,string_length(returning_str)-2);

return returning_str;