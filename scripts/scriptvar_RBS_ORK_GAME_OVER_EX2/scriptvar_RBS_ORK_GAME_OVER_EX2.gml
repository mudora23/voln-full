///scriptvar_RBS_ORK_GAME_OVER_EX2();
/// @description

if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];
	if ally[? GAME_CHAR_GENDER] == GENDER_MALE
		var ally_his_her = "his";
	else if ally[? GAME_CHAR_GENDER] == GENDER_FEMALE
		var ally_his_her = "her";
	else
		var ally_his_her = "it";
		
	return "It dawns on you that you don’t know what’s happened to "+string(ally_name)+".  Probably, "+string(ally_his_her)+"’ll suffer a similar fate.";
}
else
	return "";
