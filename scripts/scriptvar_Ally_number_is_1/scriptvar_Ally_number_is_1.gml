///scriptvar_Ally_number_is_1();
/// @description
return ds_list_size(var_map_get(VAR_ALLY_LIST)) == 1;