///CSX_breeding_scene();
/// @description This triggers before CS_location

var the_list = var_map_get_all_character_map_list();
for(var i = 0 ; i < ds_list_size(the_list);i++)
{
	var the_char = the_list[| i];
	if the_char[? GAME_CHAR_IMPREGNATE]
	{
		if the_char[? GAME_CHAR_IMPREGNATE_HOURS_SINCE] >= 20 * 5 && !the_char[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT]
		{
			var_map_set(VAR_TEMP_CHARID,char_get_char_id(the_char));
			scene_jump(GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT,CS_breeding);
			return true;
		}
		else if the_char[? GAME_CHAR_IMPREGNATE_HOURS_SINCE] >= 20 * 10 && !the_char[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BIGGER]
		{
			var_map_set(VAR_TEMP_CHARID,char_get_char_id(the_char));
			scene_jump(GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BIGGER,CS_breeding);
			return true;
		}
		else if the_char[? GAME_CHAR_IMPREGNATE_HOURS_SINCE] >= 20 * 15 && !the_char[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BORN]
		{
			var_map_set(VAR_TEMP_CHARID,char_get_char_id(the_char));
			var_map_set(VAR_TEMP_CHARID2,the_char[? GAME_CHAR_IMPREGNATE_BY_CHARID]);
			scene_jump(GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BORN,CS_breeding);
			return true;
		}

	}
	else if !the_char[? GAME_CHAR_ADULT] && the_char[? GAME_CHAR_HOURS_SINCE_BORN] >= 20 * 15
	{
		var_map_set(VAR_TEMP_CHARID,char_get_char_id(the_char));
		scene_jump("GAME_CHAR_BECOME_AN_ADULT",CS_breeding);
		return true;	
	}
}
return false;