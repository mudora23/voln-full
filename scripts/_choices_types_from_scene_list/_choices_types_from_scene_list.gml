///_choices_types_from_scene_list(scene_list);
/// @description
/// @param scene_list

if scene_is_current()
{
	var type_list = ds_list_create();
	for(var i = 0; i < ds_list_size(argument0);i++)
		ds_list_add_unique(type_list,scene_get_type(argument0[| i]));
		
	ds_list_sort(type_list,true);
	ds_list_move_value_to_last(type_list,SCENE_TYPE_OTHER);
	//show_message("type_list_size"+string(ds_list_size(type_list)));
	

	
	if ds_list_size(type_list) <= 1
	{
		if ds_list_size(argument0) > 0
		{
			ds_list_move_unread_story_chunk_to_front(argument0);
			var_map_set(VAR_TEMP_SCENE, ds_list_find_value(argument0,0));
			scene_jump("Selectedscene");
		}
		else
			scene_jump("Noscenestoselect");
	}
	else
	{
		for(var i = 0; i < ds_list_size(type_list);i++)
			scene_choice_add(type_list[| i],"Selectedtype","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE_TYPE,type_list[| i]);
				
		scene_jump_next();
	}
	
	ds_list_destroy(type_list);

	

}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);