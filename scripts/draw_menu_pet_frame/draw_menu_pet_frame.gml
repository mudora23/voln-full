///draw_menu_pet_frame(x1,y1,x2,y2,pet_map);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param pet_map

/*
    menu_open_bg_image_alpha
    menu_open_text_image_alpha
    menu_idle_bg_colorscheme = c_lightblack;
    menu_idle_text_colorscheme = c_gray;
    menu_selected_bg_colorscheme = c_gray;
    menu_selected_text_colorscheme = c_lightblack;
*/

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var pet = argument4;
var w = x2-x1;
var h = y2-y1;

var player = char_get_player();

// draw bg
//draw_set_font(font_general_20);
draw_set_alpha(menu_open_bg_image_alpha);
draw_set_color(colorscheme_almostblack);
//draw_rectangle(x1,y1,x2,y2, false);
draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,w,h,draw_get_color(),draw_get_alpha());
draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,w,h,draw_get_color(),draw_get_alpha());

// draw sprite
var padding = 10;
if sprite_exists(asset_get_index(pet[? GAME_CHAR_THUMB]))
    draw_thumb_in_area(asset_get_index(pet[? GAME_CHAR_THUMB]), x1+padding, y1+padding, w-padding*2, w-padding*2,c_white,pet);

// details
var xx = x1+padding;
var yy = y1+w-10;
draw_set_alpha(menu_open_bg_image_alpha);
draw_set_valign(fa_top);
draw_set_color(c_ltgray);
draw_set_font(font_01n_sc);

var text = pet[? GAME_CHAR_DISPLAYNAME]+" (LV "+string(round(pet[? GAME_CHAR_LEVEL]));
if !pet[? GAME_CHAR_ADULT] text += ", bundie";
if pet[? GAME_CHAR_IMPREGNATE] && !pet[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT] text += ", pregnant?!";
if pet[? GAME_CHAR_IMPREGNATE] && pet[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT] text += ", pregnant";
text += ")";

draw_text_transformed(xx,yy,text,0.5,0.5,0);

yy+=25;


var bars_hover = mouse_over_area(x1+padding,yy,w - padding*2,100);
//draw_set_font(font_general_15);
var bar_xx = x1+padding;
var bar_yy = yy;
var bar_width = w - padding*2;
var bar_height = 20;
var bar_value = pet[? GAME_CHAR_VITALITY];
var bar_color = COLOR_VITALITY;
var bar_width_per_value = bar_width / char_get_vitality_max(pet);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(bar_color);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Vitality: "+string(round(bar_value))+"/"+string(round(char_get_vitality_max(pet))),0.5,0.5,0);
}
yy+=25;



var bar_xx = x1+padding;
var bar_yy = yy;
var bar_width = w - padding*2;
var bar_height = 20;
var bar_value = pet[? GAME_CHAR_PET_DEPRAVITY];
var bar_color = COLOR_LUST;
var bar_width_per_value = bar_width / char_get_vitality_max(pet);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(bar_color);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Depravity: "+string(round(bar_value))+"/"+string(100),0.5,0.5,0);
}
yy+=25;



var bar_xx = x1+padding;
var bar_yy = yy;
var bar_width = w - padding*2;
var bar_height = 20;
var bar_value = pet[? GAME_CHAR_PET_SUBMISSION];
var bar_color = COLOR_EFFECT;
var bar_width_per_value = bar_width / char_get_vitality_max(pet);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(bar_color);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Submission: "+string(round(bar_value))+"/"+string(100),0.5,0.5,0);
}
yy+=25;



var bar_xx = x1+padding;
var bar_yy = yy;
var bar_width = w - padding*2;
var bar_height = 20;
var bar_value = pet[? GAME_CHAR_PET_LOYALTY];
var bar_color = COLOR_HEAL;
var bar_width_per_value = bar_width / char_get_vitality_max(pet);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(bar_color);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Loyalty: "+string(round(bar_value))+"/"+string(100),0.5,0.5,0);
}
yy+=25;


yy+=40;
draw_set_alpha(1);
draw_set_valign(fa_center);
draw_set_halign(fa_center);

/*draw_set_color(c_ltgray);
var type_text = "Type:";
if pet[? GAME_CHAR_PET_YOBO] type_text+=" "+string(GAME_CHAR_PET_YOBO);
if pet[? GAME_CHAR_PET_TOY] type_text+=" "+string(GAME_CHAR_PET_TOY);
if pet[? GAME_CHAR_PET_BREEDER] type_text+=" "+string(GAME_CHAR_PET_BREEDER);
draw_text_transformed(xx,yy,+string(type_text),0.5,0.5,0);*/


var button_xx_center = (x1+x2)/2;
var button_yy_center = yy;
var button_width = x2-x1-100;
var button_height = 50;
var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);

if !pet[? GAME_CHAR_PET_YOBO]
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_dkgray);
	draw_text_transformed(button_xx_center,button_yy_center,"Train",1,1,0);
	
	if button_hover
		tooltip = "You cannot train this character.";
}
else if pet[? GAME_CHAR_VITALITY] < 30 || player[? GAME_CHAR_VITALITY] < 30
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_dkgray);
	draw_text_transformed(button_xx_center,button_yy_center,"Train",1,1,0);
	
	if button_hover
		tooltip = "Consume 30 vitality of both the player and the pet to begin training.";
}
else if button_hover
{
    draw_set_color(c_silver);

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());

	draw_set_color(make_color_rgb(15,15,15));
    draw_text_transformed(button_xx_center,button_yy_center,"Train",1,1,0);
    if button_hover && action_button_pressed_get()
    {
        player[? GAME_CHAR_VITALITY] -= 30;
		pet[? GAME_CHAR_VITALITY] -= 30;
		pet[? GAME_CHAR_PET_SUBMISSION] = min(pet[? GAME_CHAR_PET_SUBMISSION] + 10,100);
		pet[? GAME_CHAR_PET_DEPRAVITY] = min(pet[? GAME_CHAR_PET_DEPRAVITY] + 5,100);
		pet[? GAME_CHAR_PET_LOYALTY] = min(pet[? GAME_CHAR_PET_LOYALTY] + 1,100);
		
        action_button_pressed_set(false);
		voln_play_sfx(sfx_mouse_click);
    }

	tooltip = "Consume 30 vitality of both the player and the pet to begin training.";

}
else
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_gray);
    draw_text_transformed(button_xx_center,button_yy_center,"Train",1,1,0);
}


yy+=button_height+10;
var button_xx_center = (x1+x2)/2;
var button_yy_center = yy;
var button_width = x2-x1-100;
var button_height = 50;
var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);

if !pet[? GAME_CHAR_PET_YOBO] 
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_dkgray);
	draw_text_transformed(button_xx_center,button_yy_center,"Promote",1,1,0);

	if button_hover
		tooltip = "You cannot promote this character.";
		
}
else if pet[? GAME_CHAR_PET_SUBMISSION] < 100 || ally_is_full()
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_dkgray);
	draw_text_transformed(button_xx_center,button_yy_center,"Promote",1,1,0);

	if button_hover
		tooltip = "You need at least 100 submission points and an ally slot to promote.";
		
}
else if button_hover
{
    draw_set_color(c_silver);

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());

	draw_set_color(make_color_rgb(15,15,15));
    draw_text_transformed(button_xx_center,button_yy_center,"Promote",1,1,0);
    if button_hover && action_button_pressed_get()
    {

		
		var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
		ds_list_add(ally_list,pet);
		var ally_index = ds_list_size(ally_list)-1;
		ds_list_mark_as_map(ally_list,ally_index);
		
		ds_list_delete_value(obj_control.var_map[? VAR_PET_LIST],pet);
		
        action_button_pressed_set(false);
		voln_play_sfx(sfx_mouse_click);
    }
	

	tooltip = "Promote this pet to become an ally.";
}
else
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_gray);
    draw_text_transformed(button_xx_center,button_yy_center,"Promote",1,1,0);

}


yy+=button_height+10;
var button_xx_center = (x1+x2)/2;
var button_yy_center = yy;
var button_width = x2-x1-100;
var button_height = 50;
var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);

if true//!pet[? GAME_CHAR_PET_TOY]
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_dkgray);
	draw_text_transformed(button_xx_center,button_yy_center,"Sex",1,1,0);
	
	if button_hover
		tooltip = "This feature is not available yet.";
		//tooltip = "You cannot sex with this character.";
}
else if button_hover
{
    draw_set_color(c_silver);

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());

	draw_set_color(make_color_rgb(15,15,15));
    draw_text_transformed(button_xx_center,button_yy_center,"Sex",1,1,0);
    if button_hover && action_button_pressed_get()
    {
        instance_destroy();
        action_button_pressed_set(false);
		voln_play_sfx(sfx_mouse_click);
    }

	draw_text_transformed(button_xx_center,button_yy_center,"Sex",1,1,0);
	
	tooltip = "Sex with this character";
}
else
{
    draw_set_color(make_color_rgb(50,50,50));

	draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
	draw_set_color(c_gray);
    draw_text_transformed(button_xx_center,button_yy_center,"Sex",1,1,0);
}


yy+=button_height+10;





















var delbutton_active = !pet[? GAME_CHAR_IMPORTANT];
if delbutton_active
{
	var delbutton = spr_delete_button;
	var delbutton_width = sprite_get_width(delbutton);
	var delbutton_height = sprite_get_height(delbutton);
	var delbutton_xx = x2 - delbutton_width;
	var delbutton_yy = y1;
	var delbutton_hover = mouse_over_area(delbutton_xx,delbutton_yy,delbutton_width,delbutton_height);


	draw_sprite_ext(delbutton,delbutton_hover && delbutton_active,delbutton_xx,delbutton_yy,1,1,0,c_dkgray,menu_open_text_image_alpha);

	if delbutton_hover
	{
		if !delbutton_active
			tooltip = "You cannot dismiss this pet.";
		else
			tooltip = "Dismiss this pet permanently?";
		
		if delbutton_active && action_button_pressed_get()
		{
			action_button_pressed_set(false);
			menu_open_previous = menu_open;
			menu_open = MENULISTING_DELETE_CHAR;
			menu_char_id_deleting = char_get_char_id(pet);
		}
	}
}
