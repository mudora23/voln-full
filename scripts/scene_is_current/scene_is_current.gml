///scene_is_current();
/// @description
return var_map_get(VAR_SCRIPT_POSI) == var_map_get(VAR_SCRIPT_POSI_FLOATING);