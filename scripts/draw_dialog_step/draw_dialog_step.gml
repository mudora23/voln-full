///draw_dialog_step();
/// @description
with obj_main
{
	if var_map_get(VAR_SCRIPT_DIALOG) != "" && (var_map_get(VAR_MODE) == VAR_MODE_NORMAL || var_map_get(VAR_MODE) == VAR_MODE_SEX_CHOOSING/* || var_map_get(VAR_MODE) == VAR_MODE_STORE*/)
	{
		var textspeed = 2;
		var textfullwidth = obj_main.area_width;
		var textpadding = 30;
		var textlinkbreakpadding = 65;
		var textautolinkbreakpadding = 45;
		var textratio = 0.8;
		var textsurfacewidth = 1100;
		var textsurfaceheight = 6000;
		var textdrawtimes = 1; // not yet
		var textdrawalpha = 1; // not yet
	
	
		if !surface_exists(var_map_get(VAR_SCRIPT_DIALOG_SURFACE)) var_map_set(VAR_SCRIPT_DIALOG_NUMBER,0);

		if var_map_get(VAR_SCRIPT_DIALOG_NUMBER) == 0
		{
		
			/*var folder_name = string_copy_prefix(argument[0],"_");
			var folder_name_encode = base64_encode(folder_name);
		
			if file_exists(working_directory+"data\\"+folder_name_encode+"\\"+base64_encode(argument[0])+".dat")
			{
				//show_debug_message("File exists: "+working_directory+"data\\"+folder_name_encode+"\\"+base64_encode(argument[0])+".dat");
				var file = file_text_open_read(working_directory+"data\\"+folder_name_encode+"\\"+base64_encode(argument[0])+".dat");
				//var arraykey = hex_to_array("2B28AB097EAEF7CF15D2154F16A6883C");
				var encrypted_content = file_text_read_string(file);
				//var encrypted_arrayContent = hex_to_array(encrypted_content);
				//var decrypted_content = aes_decrypt_array(encrypted_arrayContent, arraykey);
				//var content = array_to_string(decrypted_content);
				var_map_set(VAR_SCRIPT_DIALOG,encrypted_content);
				file_text_close(file);
			
				chunk_mark_set(argument[0],true);
			}
			else
			{
				//show_debug_message("File NOT exists: "+working_directory+"data\\"+folder_name_encode+"\\"+base64_encode(argument[0])+".dat");
				var_map_set(VAR_SCRIPT_DIALOG,text_add_markup(argument[0],TAG_FONT_N,TAG_COLOR_NORMAL));
			}*/

			//var_map_set(VAR_SCRIPT_DIALOG_RAW,marked_text_get_raw(var_map_get(VAR_SCRIPT_DIALOG)));
			

			var_map_list_clear(VAR_SCRIPT_DIALOG_LIST_CHAR);
			var_map_list_clear(VAR_SCRIPT_DIALOG_LIST_X);
			var_map_list_clear(VAR_SCRIPT_DIALOG_LIST_Y);
			var_map_list_clear(VAR_SCRIPT_DIALOG_LIST_WIDTH);
			var_map_list_clear(VAR_SCRIPT_DIALOG_LIST_HEIGHT);
			var_map_list_clear(VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES);
		
			var_map_list_set(VAR_SCRIPT_DIALOG_LIST_CHAR,0,"");
			var_map_list_set(VAR_SCRIPT_DIALOG_LIST_WIDTH,0,0);
			var_map_list_set(VAR_SCRIPT_DIALOG_LIST_HEIGHT,0,0);
			var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,0,textpadding);
			var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,0,textpadding);
			var_map_list_set(VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES,0,0);

			var_map_set(VAR_SCRIPT_DIALOG_SPEAKER_DRAWN,false);
			
			if var_map_get(VAR_SCRIPT_DIALOG_SPEAKER) != "" var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,0,textpadding+string_height("M"));
		
			if !surface_exists(var_map_get(VAR_SCRIPT_DIALOG_SURFACE)) var_map_set(VAR_SCRIPT_DIALOG_SURFACE,surface_create(textsurfacewidth,textsurfaceheight));

			surface_set_target(var_map_get(VAR_SCRIPT_DIALOG_SURFACE));
			draw_clear_alpha(c_black,0);
			surface_reset_target();

			//var_map_set(VAR_SCRIPT_DIALOG_SURFACE,surface_create(textsurfacewidth,textsurfaceheight));

			dialog_yoffset_target = 0;
			dialog_yoffset = 0;
		}
	
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

		var old_proc_number = var_map_get(VAR_SCRIPT_DIALOG_NUMBER);
		var_map_set(VAR_SCRIPT_DIALOG_NUMBER,min(var_map_get(VAR_SCRIPT_DIALOG_NUMBER)+textspeed,string_length(var_map_get(VAR_SCRIPT_DIALOG_RAW))));

	
	
		// processing
		for(var i = floor(old_proc_number+1);i<=floor(var_map_get(VAR_SCRIPT_DIALOG_NUMBER));i++)
		{
			if is_undefined(var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,i))
			{
				// descriptor replacing
				/*var chari = string_char_at(var_map_get(VAR_SCRIPT_DIALOG_RAW),i);
				if chari == "{"
				{
				
				
				
				
				
				
				
				}
				*/
			
			
			
			
			
				// processing until the end of the line
				var j = i;
				while j <= /*floor(var_map_get(VAR_SCRIPT_DIALOG_NUMBER))*/string_length(var_map_get(VAR_SCRIPT_DIALOG_RAW))
				{
					var char = string_char_at(var_map_get(VAR_SCRIPT_DIALOG_RAW),j);
					var char_width = string_width(char)*textratio;
					var char_height = string_height(char)*textratio;
					
					// if char j is not processed yet and the previous char is \r .... we set the position of the 1st character in the line after a link break.
					if is_undefined(var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,j)) && string_char_at(var_map_get(VAR_SCRIPT_DIALOG_RAW),j-1) == "\r"
					{

						var_map_list_set(VAR_SCRIPT_DIALOG_LIST_CHAR,j,char);
						var_map_list_set(VAR_SCRIPT_DIALOG_LIST_WIDTH,j,char_width);
						var_map_list_set(VAR_SCRIPT_DIALOG_LIST_HEIGHT,j,char_height);
						var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,j,textpadding);
						var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,j,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,j-1)+textlinkbreakpadding);
						var_map_list_set(VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES,j,0);

						break;

					}
					
					// if char j is not processed yet, we process it.
					else if is_undefined(var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,j))
					{
						// the position of char j would be out of spaces to draw, and char j is not a space
						if var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,j-1) + var_map_list_get(VAR_SCRIPT_DIALOG_LIST_WIDTH,j-1) + char_width > textfullwidth - textpadding && var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,j) != " "
						{
							// search for the beginning of this word, results: k = beginning of this word
							var k = j - 1;
							while(k > 0 && var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,k) != "\r" && var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,k) != " ")
							{
								k--;
							}
							
							// search for the characters of the previous line, results: l = the first char in the previous line; l_nonspace is "l + spaces" in the previous line.
							var l = k - 1;
							var l_nonspace = k - 1;
							
							// end of line spaces
							var l_end_of_line_spaces = 0;
							while(l > 0 && var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,l) == var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,l+1) && var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,l) == " ")
							{
								l--;
								l_end_of_line_spaces++;
							}
							
							while(l > 0 && var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,l) == var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,l+1))
							{
								l--;
								if var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,l) != " "
									l_nonspace--;
							}

								
							
							
							// l_spaces is the total extra spaces the whole line should share.
							// l_space the extra space each non-space char should add.
							var l_spaces = textfullwidth - textpadding - var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,k - 1) - var_map_list_get(VAR_SCRIPT_DIALOG_LIST_WIDTH,k - 1) - 20;
							var l_space = l_spaces / (k - 1 - l_nonspace);
							
							// add spaces if needed. (The first char in a line will never be added.)
							var ll_nonspace = 1;
							if k-l-1 > 0 var l_end_of_line_spaces_offset = (l_end_of_line_spaces * string_width(" ")) / (k-l-1);
							else var l_end_of_line_spaces_offset = 0;
							for(var ll = l+1; ll < k; ll++)
							{
								if var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,ll) != " "
									ll_nonspace++;
								var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,ll,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,ll)+l_space*(ll_nonspace-1)+l_end_of_line_spaces_offset*(ll_nonspace-1));

							}
							
							// for current line
							for(var m = k; m <= j-1; m++)
							{
								if m == k
								{
									var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,m,textpadding);
									var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,m,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,m)+textautolinkbreakpadding);
								
								}
								else
								{
									var char_width_m_prev = var_map_list_get(VAR_SCRIPT_DIALOG_LIST_WIDTH,m-1);
									var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,m,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,m-1)+char_width_m_prev);
									var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,m,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,m-1));
								}
							
							}

						}
							
						if string_char_at(var_map_get(VAR_SCRIPT_DIALOG_RAW),j-1) == "\r"
						{
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_CHAR,j,char);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_WIDTH,j,char_width);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_HEIGHT,j,char_height);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,j,textpadding);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,j,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,j-1)+textlinkbreakpadding);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES,j,0);

							break;
						}
						else
						{
							var char_width_prev = var_map_list_get(VAR_SCRIPT_DIALOG_LIST_WIDTH,j-1);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_CHAR,j,char);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_WIDTH,j,char_width);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_HEIGHT,j,char_height);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_X,j,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,j-1)+char_width_prev);
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_Y,j,var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,j-1));
							var_map_list_set(VAR_SCRIPT_DIALOG_LIST_DRAW_TIMES,j,0);
						}


					}
					
					else
						break;
					j++;
				}
			}
		}

		// drawing

		surface_set_target(var_map_get(VAR_SCRIPT_DIALOG_SURFACE));
		gpu_set_blendmode_ext(bm_src_alpha, bm_dest_alpha);
		draw_set_alpha(textdrawalpha);
		
		if var_map_get(VAR_SCRIPT_DIALOG_SPEAKER) != "" && !var_map_get(VAR_SCRIPT_DIALOG_SPEAKER_DRAWN)
		{
			draw_set_color(COLOR_LOCATION);
			draw_text_transformed(textpadding,textpadding,var_map_get(VAR_SCRIPT_DIALOG_SPEAKER),textratio,textratio,0);
			draw_set_color(COLOR_NORMAL);
			var_map_set(VAR_SCRIPT_DIALOG_SPEAKER_DRAWN,true);
			
		}
		
		for(var i = floor(old_proc_number+1);i<=floor(var_map_get(VAR_SCRIPT_DIALOG_NUMBER));i++)
		{
			//draw_set_color(COLOR_NORMAL);
			text_newmarkups_apply(var_map_get(VAR_SCRIPT_DIALOG),i);
			draw_text_transformed(var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,i),var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,i),var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,i),textratio,textratio,0);
			/*if var_map_list_get(VAR_SCRIPT_DIALOG_LIST_CHAR,i) == " "
			{
				draw_text_transformed(var_map_list_get(VAR_SCRIPT_DIALOG_LIST_X,i),var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,i),"_",textratio,textratio,0);
				
			}*/
		}
		gpu_set_blendmode(bm_normal);
		surface_reset_target();
		draw_reset();

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

	if surface_exists(var_map_get(VAR_SCRIPT_DIALOG_SURFACE))
	{
		draw_surface_in_visible_area(var_map_get(VAR_SCRIPT_DIALOG_SURFACE),x,y-dialog_yoffset,1,1,c_white,1,x,y,area_width,area_height);
		
		

		// DEBUG - chunk name
		/*if obj_control.debug_enable
		{
			
			draw_set_halign(fa_right);
			draw_set_valign(fa_bottom);
			draw_set_font(font_01s_reg);
			draw_set_color(c_white);
			draw_text(obj_main.x+obj_main.area_width-10,obj_main.y+35,obj_control.debug_chunk_name);
			//draw_text(500,500,argument[0]);
			draw_reset();
			
		}*/
	

		
		
		
		// scrolling
		if !menu_exists()
		{
			if (mouse_over_area(x,y,area_width,area_height) && mouse_wheel_down()) || keyboard_check_pressed(vk_pagedown)
				dialog_yoffset_target += 150;
			if (mouse_over_area(x,y,area_width,area_height) && mouse_wheel_up()) || keyboard_check_pressed(vk_pageup)
				dialog_yoffset_target -= 150;
		}
		
		var max_dialog_offset = var_map_list_get(VAR_SCRIPT_DIALOG_LIST_Y,floor(var_map_get(VAR_SCRIPT_DIALOG_NUMBER)))+var_map_list_get(VAR_SCRIPT_DIALOG_LIST_HEIGHT,floor(var_map_get(VAR_SCRIPT_DIALOG_NUMBER)))-area_height+textpadding;
		dialog_yoffset_target = clamp(dialog_yoffset_target,0,max(0,max_dialog_offset));
		dialog_yoffset = smooth_approach_room_speed(dialog_yoffset,dialog_yoffset_target,SMOOTH_SLIDE_NORMAL_SEC);

		
		var scroll_spr = spr_button_scroll;
		var scroll_spr_width = sprite_get_width(scroll_spr);
		var scroll_spr_height = sprite_get_height(scroll_spr);
		if max_dialog_offset > 0
		{
			var scroll_xx = x + area_width + 22;
			var scroll_yy = y + dialog_yoffset / max_dialog_offset * (area_height-scroll_spr_height);
			var scroll_hover_r = mouse_over_area(scroll_xx,scroll_yy,scroll_spr_width,scroll_spr_height) && !menu_exists();
			if mouse_check_button_pressed(mb_left) && scroll_hover_r
				holding_scroll = true;
			
			draw_sprite_ext(spr_gui_brass_connector_c,-1,scroll_xx-5,scroll_yy+ scroll_spr_height/2,-0.3,0.5,0,c_white,image_alpha);
			draw_sprite_ext(spr_button_scroll,-1,scroll_xx,scroll_yy,1,1,0,c_white,image_alpha/2+(scroll_hover_r || holding_scroll)*image_alpha/2);
			
			var scroll_xx = x - 22;
			var scroll_hover_l = mouse_over_area(scroll_xx-scroll_spr_width,scroll_yy,scroll_spr_width,scroll_spr_height) && !menu_exists();
			if mouse_check_button_pressed(mb_left) && scroll_hover_l
				holding_scroll = true;
				
			draw_sprite_ext(spr_gui_brass_connector_c,-1,scroll_xx+5,scroll_yy+ scroll_spr_height/2,-0.3,0.5,0,c_white,image_alpha);
			draw_sprite_ext(spr_button_scroll,-1,scroll_xx,scroll_yy,-1,1,0,c_white,image_alpha/2+(scroll_hover_l || holding_scroll)*image_alpha/2);
			
			if holding_scroll
			{
				var scroll_ratio = clamp((mouse_y - (y+scroll_spr_height/2))/(area_height-scroll_spr_height),0,1);
				dialog_yoffset = max_dialog_offset * scroll_ratio;
				dialog_yoffset_target = dialog_yoffset;
			}
			
			
			
			if !mouse_check_button(mb_left)
				holding_scroll = false;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		// chocies
		if var_map_get(VAR_SCRIPT_DIALOG_NUMBER) >= string_length(var_map_get(VAR_SCRIPT_DIALOG_RAW)) || var_map_get(VAR_SCRIPT_SHOW_CHOICES)
			obj_command_panel.choice_image_alpha_increasing = true;
		else
			obj_command_panel.choice_image_alpha_increasing = false;
		
		
	}
	else
		obj_command_panel.choice_image_alpha_increasing = true; // prevent a bug where this increasing is set to falst for some reason, and the choices are not shown out of dialog
	
		
}





