///draw_floating_text(x,y,string,color,delay_sec[optional],scale[optional]);
/// @descirption
/// @param x
/// @param y
/// @param string
/// @param color
/// @param delay_sec[optional]
/// @param scale[optional]
ds_list_add(obj_floating_text.floating_text_string,argument[2]);
ds_list_add(obj_floating_text.floating_text_x,argument[0]);
ds_list_add(obj_floating_text.floating_text_y,argument[1]);
ds_list_add(obj_floating_text.floating_text_increasing,true);
ds_list_add(obj_floating_text.floating_text_alpha,0);
ds_list_add(obj_floating_text.floating_text_life,1);
ds_list_add(obj_floating_text.floating_text_sign,"");  
ds_list_add(obj_floating_text.floating_text_color,argument[3]); 
if argument_count > 4 var delay = argument[4];
else var delay = 0;
ds_list_add(obj_floating_text.floating_text_delay,delay); 
if argument_count > 5 var scale = argument[5];
else var scale = 1;
ds_list_add(obj_floating_text.floating_text_scale,scale); 
