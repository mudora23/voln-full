///char_get_END(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_END);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_END_1);
amount += char_get_skill_level(argument0,SKILL_END_2);
amount += char_get_skill_level(argument0,SKILL_END_3);
amount += char_get_skill_level(argument0,SKILL_END_4);
amount += char_get_skill_level(argument0,SKILL_END_5);
amount += char_get_skill_level(argument0,SKILL_END_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;