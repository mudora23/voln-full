///player_ally_relieve_all();
/// @description

with obj_control
{
	var ally_list = var_map[? VAR_ALLY_LIST];
    for(var i = 0;i<ds_list_size(ally_list);i++)
    {
        var ally = ally_list[| i];
		ally[? GAME_CHAR_LUST] = 0;
    }
	
	var player = var_map_get(VAR_PLAYER);
	player[? GAME_CHAR_LUST] = 0;
}


