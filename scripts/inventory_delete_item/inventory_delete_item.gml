///inventory_delete_item(name,amount);
/// @description
/// @param name
/// @param amount

with obj_control
{
	var name = argument[0];
	var amount = argument[1];
	var name_list = obj_control.var_map[? VAR_INVENTORY_LIST];
	var amount_list = obj_control.var_map[? VAR_INVENTORY_AMOUNT_LIST];
	var exist_index = ds_list_find_index(name_list,name);
	
	if exist_index >= 0
	{
		amount_list[| exist_index] -= amount;
		if amount_list[| exist_index] <= 0
		{
			ds_list_delete(name_list,exist_index);
			ds_list_delete(amount_list,exist_index);
		}
		
		// battlelog
		var battlelog_pieces_list = ds_list_create();
		ds_list_add(battlelog_pieces_list,text_add_markup("Item used: ",TAG_FONT_N,TAG_COLOR_DAMAGE));
		ds_list_add(battlelog_pieces_list,text_add_markup(item_get_displayname(name),TAG_FONT_N,TAG_COLOR_EFFECT));
		if amount > 1
			ds_list_add(battlelog_pieces_list,text_add_markup(" x"+string(amount),TAG_FONT_N,TAG_COLOR_EFFECT));
		var battle_log = string_append_from_list(battlelog_pieces_list);
		battlelog_add(battle_log);
		ds_list_destroy(battlelog_pieces_list);
		
	}
}