///char_id_remove_char(charid);
/// @description
/// @param charid
var char = char_id_get_char(argument0);
if char != noone ds_map_destroy(char);
var_map_get_all_character_map_list(true);
