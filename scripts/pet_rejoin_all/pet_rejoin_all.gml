///pet_rejoin_all();
/// @description
with obj_control
{
	var pet_list = var_map[? VAR_PET_R_LIST];
    for(var i = 0;i<ds_list_size(pet_list);i++)
    {
        ally_rejoin(char_get_info(pet_list[| i],GAME_CHAR_ID));
		i--;
    }
	
}