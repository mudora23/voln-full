///text_markups_stretch(marked_text,raw_text_posi,amount);
/// @description - if longer, copy and extend X amount of the tags; otherwise, delete X amount of the tags.
/// @param marked_text
/// @param raw_text_posi
/// @param amount
var marked_text = argument0;
var raw_text_posi = argument1;
var amount = argument2;
if amount > 0
{
    var font_tag = string_char_at(marked_text,marked_text_get_posi(raw_text_posi)+1);
    var color_tag = string_char_at(marked_text,marked_text_get_posi(raw_text_posi)+2);
    repeat(amount) marked_text = string_insert("X"+font_tag+color_tag,marked_text,marked_text_get_posi(raw_text_posi));
}
else if amount < 0
{
    repeat(abs(amount)) marked_text = string_delete(marked_text,marked_text_get_posi(raw_text_posi-abs(amount)+1),3);
}
return marked_text;