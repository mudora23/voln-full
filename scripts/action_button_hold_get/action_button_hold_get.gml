///action_button_hold_get();
/// @description
return action_button_keyboard_hold_get() || action_button_mouse_hold_get();