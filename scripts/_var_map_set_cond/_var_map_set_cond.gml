///_var_map_set_cond(KEY,VAL_IF_TRUE,VAL_IF_FALSE,cond);
/// @description  
/// @param KEY
/// @param VAL_IF_TRUE
/// @param VAL_IF_FALSE
/// @param cond

if scene_is_current()
{
	if argument3 var_map_set(argument0,argument1);
	else var_map_set(argument0,argument2);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);