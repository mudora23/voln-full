///scriptvar_PC_Chest_Desc();
/// @description
var num = var_map_get(PC_Chest_Dist_N);
if num == 0 return choose("small","petite","elfin");
else if num == 1 return choose("slender","smallish","androgynous");
else if num == 2 return "normal";
else return choose("wide","hardy");