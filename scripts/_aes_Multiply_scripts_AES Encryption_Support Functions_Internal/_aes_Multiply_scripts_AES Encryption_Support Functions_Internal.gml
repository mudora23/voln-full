///_aes_Multiply(xx, yy)
var xx = argument0, yy = argument1;

return (((yy & 1) * xx) ^
     ((yy>>1 & 1) * _aes_xtime(xx)) ^
     ((yy>>2 & 1) * _aes_xtime(_aes_xtime(xx))) ^
     ((yy>>3 & 1) * _aes_xtime(_aes_xtime(_aes_xtime(xx)))) ^
     ((yy>>4 & 1) * _aes_xtime(_aes_xtime(_aes_xtime(_aes_xtime(xx)))))) mod 256;
