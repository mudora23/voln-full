///_hidecg(ID,fade_speed);
/// @description  
/// @param ID
/// @param fade_speed

if scene_is_current()
{
	scene_cg_list_change_alpha_ext(argument0,0,argument1);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);