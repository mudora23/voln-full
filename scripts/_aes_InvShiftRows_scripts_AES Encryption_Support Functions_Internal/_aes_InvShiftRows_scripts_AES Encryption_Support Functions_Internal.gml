///_aes_InvShiftRows(state)
var temp, state = argument0;

temp=state[@ 3,1];
state[@ 3,1]=state[@ 2,1];
state[@ 2,1]=state[@ 1,1];
state[@ 1,1]=state[@ 0,1];
state[@ 0,1]=temp;

temp=state[@ 0,2];
state[@ 0,2]=state[@ 2,2];
state[@ 2,2]=temp;

temp=state[@ 1,2];
state[@ 1,2]=state[@ 3,2];
state[@ 3,2]=temp;

temp=state[@ 0,3];
state[@ 0,3]=state[@ 1,3];
state[@ 1,3]=state[@ 2,3];
state[@ 2,3]=state[@ 3,3];
state[@ 3,3]=temp;

