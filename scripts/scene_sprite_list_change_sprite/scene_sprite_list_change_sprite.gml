///scene_sprite_list_change_sprite(ID,sprite_target);
/// @description  
/// @param ID
/// @param sprite_target

with obj_control
{
	if sprite_exists(argument[1])
	{
		var sprite_index_target_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_TARGET_NAME];
		var sprite_index_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME];
		var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
		var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	
		if ds_list_find_index(sprite_id_list,argument[0]) >= 0
		{
			var existing_index = ds_list_find_index(sprite_id_list,argument[0]);
			
				sprite_alpha_target_list[| existing_index] = 1;
			//if sprite_index_target_list[| existing_index] != sprite_index_list[| existing_index]
			//{
				sprite_index_target_list[| existing_index] = sprite_get_name(argument[1]);
				//show_debug_message("sprite changing from "+string(sprite_index_list[| existing_index])+" to "+string(sprite_index_target_list[| existing_index]));
			//}
		}
		
	}


}