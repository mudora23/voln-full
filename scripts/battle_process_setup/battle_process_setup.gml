///battle_process_setup();
/// @description
with obj_control
{
	// player's AI mode is reset to "none" in the beginning of each battle
	//ds_map_replace(char_get_player(),GAME_CHAR_AI,AI_NONE);
	
	//scene_clear_ext(false,false,true,true,true);
	//dialog_update_surface();
	var_map_set(VAR_MODE,VAR_MODE_BATTLE);
	
	var_map_set(VAR_SCRIPT_SHOW_CHOICES,true);
	
	var_map_set(VAR_NEXT_SCRIPT,"");
	var_map_set(VAR_NEXT_SCRIPT_POSI_OR_LABEL,"");
	var_map_set(VAR_SCRIPT,"");
	
	scene_dialog_clear();
	
	// Adding all characters in the list
	ds_list_clear(obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST]);
	
	ds_list_add(obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST],obj_control.var_map[? VAR_PLAYER]);
	
	var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
	for(var i = 0; i < ds_list_size(ally_list);i++)
		ds_list_add(obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST],ally_list[| i]);
		
	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	for(var i = 0; i < ds_list_size(enemy_list);i++)
		ds_list_add(obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST],enemy_list[| i]);
	

	// Reset action time
	var order_list = obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST];
	var fastest_speed = 0;
	for(var i = 0; i < ds_list_size(order_list);i++)
	{
		var the_char_map = order_list[| i];
		fastest_speed = max(fastest_speed,char_get_speed(the_char_map));
	}
	for(var i = 0; i < ds_list_size(order_list);i++)
	{
		var the_char_map = order_list[| i];
		the_char_map[? GAME_CHAR_BATTLE_WAITING_TIME] = 100 + (fastest_speed - char_get_speed(the_char_map))*10;
		
		// ordering list position
		ordering_list_in_battle_set_position(the_char_map,noone,true);
	}
	
	obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER] = noone;
	script_execute_add_unique(2,battle_process_next_char);

	//draw_floating_text_debug("------Battle Setup is executed!------");
	//draw_floating_text_debug("Total amount of characters in battle: "+string(ds_list_size(order_list)));

	

	/*var the_msg = "Battle begins...\r";
	
	var char = obj_control.game_map[? GAME_PLAYER];
	the_msg += "Player:\r";
	the_msg += "Level "+string(char[? GAME_CHAR_LEVEL])+"\r";
	the_msg += "STR "+string(char_get_STR(char_map))+"\r";
	the_msg += "FOU "+string(char_get_FOU(char_map))+"\r";
	the_msg += "DEX "+string(char_get_DEX(char_map))+"\r";
	the_msg += "END "+string(char_get_END(char_map))+"\r";
	the_msg += "INT "+string(char_get_INT(char_map))+"\r";
	the_msg += "WIS "+string(char_get_WIS(char_map))+"\r";
	the_msg += "SPI "+string(char_get_SPI(char_map))+"\r";
	the_msg += "\r\r\r";
	
	var char_list = obj_control.game_map[? GAME_ALLY_LIST];
	for(var i = 0; i < ds_list_size(char_list);i++)
	{
		var char = char_list[| i];
		the_msg += "Ally "+string(i)+":\r";
		the_msg += "Level "+string(char[? GAME_CHAR_LEVEL])+"\r";
		the_msg += "STR "+string(char_get_STR(char_map))+"\r";
		the_msg += "FOU "+string(char_get_FOU(char_map))+"\r";
		the_msg += "DEX "+string(char_get_DEX(char_map))+"\r";
		the_msg += "END "+string(char_get_END(char_map))+"\r";
		the_msg += "INT "+string(char_get_INT(char_map))+"\r";
		the_msg += "WIS "+string(char_get_WIS(char_map))+"\r";
		the_msg += "SPI "+string(char_get_SPI(char_map))+"\r";
		the_msg += "\r\r\r";
	}
	
	var char_list = obj_control.game_map[? GAME_ENEMY_LIST];
	for(var i = 0; i < ds_list_size(char_list);i++)
	{
		var char = char_list[| i];
		the_msg += "Enemy "+string(i)+":\r";
		the_msg += "Level "+string(char[? GAME_CHAR_LEVEL])+"\r";
		the_msg += "STR "+string(char_get_STR(char_map))+"\r";
		the_msg += "FOU "+string(char_get_FOU(char_map))+"\r";
		the_msg += "DEX "+string(char_get_DEX(char_map))+"\r";
		the_msg += "END "+string(char_get_END(char_map))+"\r";
		the_msg += "INT "+string(char_get_INT(char_map))+"\r";
		the_msg += "WIS "+string(char_get_WIS(char_map))+"\r";
		the_msg += "SPI "+string(char_get_SPI(char_map))+"\r";
		the_msg += "\r\r\r";
	}
	
	show_message(the_msg);*/
}

