///script_execute_add(delay[sec],scene_script,arg0,arg1...);
/// @description  
/// @param delay[sec]
/// @param scene_script
/// @param arg0
/// @param arg1...
with obj_control
{
	if script_get_index_ext(argument[1]) != noone && is_real(argument[0])
	{
		ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_DELAY],argument[0]);
	    ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST],script_get_name_ext(argument[1]));
		if argument_count > 2 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG0],argument[2]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG0],ARG_NOT_EXISTS);
		if argument_count > 3 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG1],argument[3]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG1],ARG_NOT_EXISTS);
		if argument_count > 4 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG2],argument[4]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG2],ARG_NOT_EXISTS);
		if argument_count > 5 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG3],argument[5]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG3],ARG_NOT_EXISTS);
		if argument_count > 6 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG4],argument[6]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG4],ARG_NOT_EXISTS);
		if argument_count > 7 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG5],argument[7]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG5],ARG_NOT_EXISTS);
		if argument_count > 8 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG6],argument[8]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG6],ARG_NOT_EXISTS);
		if argument_count > 9 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG7],argument[9]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG7],ARG_NOT_EXISTS);
		if argument_count > 10 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG8],argument[10]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG8],ARG_NOT_EXISTS);
		if argument_count > 11 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG9],argument[11]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG9],ARG_NOT_EXISTS);
		if argument_count > 12 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG10],argument[12]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG10],ARG_NOT_EXISTS);
		if argument_count > 13 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG11],argument[13]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG11],ARG_NOT_EXISTS);
		if argument_count > 14 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG12],argument[14]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG12],ARG_NOT_EXISTS);
		if argument_count > 15 ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG13],argument[15]); else ds_list_add(obj_control.var_map[? VAR_EXECUTE_LIST_ARG13],ARG_NOT_EXISTS);
	}

	if obj_control.debug_enable
	{
		var error_message = "SCRIPT_EXECUTE_ADD_ERROR: ";
			

		if script_get_index_ext(argument[1]) == noone error_message += "argument[1]="+string(argument[1])+"; ";
		if !is_real(argument[0]) error_message += "argument[0]="+string(argument[0])+"; ";
			
		if error_message != "SCRIPT_EXECUTE_ADD_ERROR: "
		{
			error_message += " argument[0]:"+string(argument[0]);
			error_message += " argument[1]:"+string(argument[1]);
			if argument_count > 2 error_message += " argument[2]:"+string(argument[2]);
			if argument_count > 3 error_message += " argument[3]:"+string(argument[3]);
			if argument_count > 4 error_message += " argument[4]:"+string(argument[4]);
			if argument_count > 5 error_message += " argument[5]:"+string(argument[5]);
			if argument_count > 6 error_message += " argument[6]:"+string(argument[6]);
			if argument_count > 7 error_message += " argument[7]:"+string(argument[7]);
			if argument_count > 8 error_message += " argument[8]:"+string(argument[8]);
			if argument_count > 9 error_message += " argument[9]:"+string(argument[9]);
			if argument_count > 10 error_message += " argument[10]:"+string(argument[10]);
			if argument_count > 11 error_message += " argument[11]:"+string(argument[11]);
			if argument_count > 12 error_message += " argument[12]:"+string(argument[12]);
			if argument_count > 13 error_message += " argument[13]:"+string(argument[13]);
			if argument_count > 14 error_message += " argument[14]:"+string(argument[14]);
			if argument_count > 15 error_message += " argument[15]:"+string(argument[15]);
			show_debug_message(error_message);
		}
	}








}
