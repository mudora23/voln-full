///storage_exists(name/charid);
/// @description
/// @param name/charid
var char_list = obj_control.var_map[? VAR_STORAGE_LIST];
for(var i = 0;i< ds_list_size(char_list);i++)
{
	var char_map = char_list[| i];
	if is_real(argument0) && char_map[? GAME_CHAR_ID] == argument0
		return true;
	if is_string(argument0) && char_map[? GAME_CHAR_NAME] == argument0
		return true;
}
return false;

