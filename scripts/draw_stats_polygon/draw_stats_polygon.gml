///draw_stats_polygon(centerx,centery,radius,end,fou,str,dex,wis,int,spi,in_menu,player_assign_points);
/// @description
/// @param centerx
/// @param centery
/// @param radius
/// @param end
/// @param fou
/// @param str
/// @param dex
/// @param wis
/// @param int
/// @param spi
/// @param in_menu
/// @param player_assign_points

	// polygon
	var polygon_radius = argument2;
	var polygon_centerx = argument0;
	var polygon_centery = argument1;
	
	var polygon_in_menu = argument10;
	var player_assign_points = argument11;

	var stats_list = ds_list_create(); ds_list_add(stats_list,argument3,argument4,argument5,argument6,argument7,argument8,argument9);
    var stats_name_list = ds_list_create(); ds_list_add(stats_name_list,GAME_CHAR_END,GAME_CHAR_FOU,GAME_CHAR_STR,GAME_CHAR_DEX,GAME_CHAR_WIS,GAME_CHAR_INT,GAME_CHAR_SPI);
    var stats_des_list = ds_list_create(); ds_list_add(stats_des_list,
	                                                   "Endurance ("+string(round(stats_list[| 0]))+") - a measure of maximum health and resistance chance.",
													   "Foundation ("+string(round(stats_list[| 1]))+") - a measure of physical armor and health regeneration.",
													   "Strength ("+string(round(stats_list[| 2]))+") - a measure of physical damage.",
													   "Dexterity ("+string(round(stats_list[| 3]))+") - a measure of dodge chance and speed.",
													   "Wisdom ("+string(round(stats_list[| 4]))+") - a measure of critical chance and hit chance.",
													   "Intellect ("+string(round(stats_list[| 5]))+") - a measure of maximum toru and toru damage.",
													   "Spirit ("+string(round(stats_list[| 6]))+") - a measure of maximum toru, lindo, toru armor and toru regeneration."
													   );
	var stats_des_all = "Endurance ("+string(round(stats_list[| 0]))+"); ";
	stats_des_all += "Foundation ("+string(round(stats_list[| 1]))+"); ";
	stats_des_all += "Strength ("+string(round(stats_list[| 2]))+"); ";
	stats_des_all += "Dexterity ("+string(round(stats_list[| 3]))+"); ";
	stats_des_all += "Wisdom ("+string(round(stats_list[| 4]))+"); ";
	stats_des_all += "Intellect ("+string(round(stats_list[| 5]))+"); ";
	stats_des_all += "Spirit ("+string(round(stats_list[| 6]))+").";
	
	var stats_color_list = ds_list_create(); ds_list_add(stats_color_list,COLOR_HP,COLOR_HP,COLOR_HP,COLOR_STAMINA,COLOR_STAMINA,COLOR_TORU,COLOR_TORU);
	var stats_button_list = ds_list_create(); ds_list_add(stats_button_list,spr_button_stats_end,spr_button_stats_fou,spr_button_stats_str,spr_button_stats_dex,spr_button_stats_wis,spr_button_stats_int,spr_button_stats_spi);
    var total_stats = 0; for(var i = 0; i < ds_list_size(stats_list);i++) total_stats+=stats_list[| i];
	var average_stats = total_stats/ds_list_size(stats_list);
	
	var colorR = 0;
	var colorG = 0;
	var colorB = 0;
	for(var i = 0; i < ds_list_size(stats_list);i++)
	{
		colorR += color_get_red(stats_color_list[| i])*stats_list[| i]/total_stats;
		colorG += color_get_green(stats_color_list[| i])*stats_list[| i]/total_stats;
		colorB += color_get_blue(stats_color_list[| i])*stats_list[| i]/total_stats;
	}
	var polygon_color = make_color_rgb(colorR,colorG,colorB);
	
	var angle_diff = 360/7;
    var current_angle = 38.5;
    
	/*draw_set_color(c_dkdkwhite);
	var current_angle = 38.5;
    for(var i = 0; i < ds_list_size(stats_list);i++)
    {
		var x1 = polygon_centerx+lengthdir_x(1*polygon_radius,current_angle);
		var y1 = polygon_centery+lengthdir_y(1*polygon_radius,current_angle);
		var x2 = polygon_centerx+lengthdir_x(1*polygon_radius,current_angle+angle_diff);
		var y2 = polygon_centery+lengthdir_y(1*polygon_radius,current_angle+angle_diff);
		draw_triangle(polygon_centerx,polygon_centery,x1,y1,x2,y2,false);
		current_angle+=angle_diff;
	}*/
	
	var current_angle = 38.5;
	var current_alpha = draw_get_alpha();
    for(var i = 0; i < ds_list_size(stats_list);i++)
    {
        var current_var = stats_list[| i];
        if i == ds_list_size(stats_list) - 1 var next_var = stats_list[| 0];
        else var next_var = stats_list[| i+1];
        
        var current_ratio = min(polygon_radius,(polygon_radius*0.6) * (current_var/average_stats))/polygon_radius;
        var next_ratio = min(polygon_radius,(polygon_radius*0.6) * (next_var/average_stats))/polygon_radius;
	
		draw_set_color(c_gray);
		for(var ratio = 0.2; ratio <= 1; ratio+=0.2)
		{
			draw_set_alpha(current_alpha*ratio*0.5);
			var x1 = polygon_centerx+lengthdir_x(ratio*polygon_radius,current_angle);
			var y1 = polygon_centery+lengthdir_y(ratio*polygon_radius,current_angle);
			var x2 = polygon_centerx+lengthdir_x(ratio*polygon_radius,current_angle+angle_diff);
			var y2 = polygon_centery+lengthdir_y(ratio*polygon_radius,current_angle+angle_diff);
		
			draw_line_width(x1,y1,x2,y2,1);
		}
		draw_set_alpha(current_alpha*0.5);
		draw_line_width(polygon_centerx,polygon_centery,x1,y1,2);
		
		current_angle+=angle_diff;
	}	
	draw_set_alpha(current_alpha*0.7);
	
	var current_angle = 38.5;
    for(var i = 0; i < ds_list_size(stats_list);i++)
    {
        var current_var = stats_list[| i];
        if i == ds_list_size(stats_list) - 1 var next_var = stats_list[| 0];
        else var next_var = stats_list[| i+1];
        
        var current_ratio = min(polygon_radius,(polygon_radius*0.6) * (current_var/average_stats))/polygon_radius;
        var next_ratio = min(polygon_radius,(polygon_radius*0.6) * (next_var/average_stats))/polygon_radius;
		
        //draw_set_color(c_dkgray);
		//draw_triangle(polygon_centerx,polygon_centery,polygon_centerx+lengthdir_x(polygon_radius,current_angle),polygon_centery+lengthdir_y(polygon_radius,current_angle),polygon_centerx+lengthdir_x(polygon_radius,current_angle+angle_diff),polygon_centery+lengthdir_y(polygon_radius,current_angle+angle_diff),false);
        draw_set_color(polygon_color);
		draw_triangle(polygon_centerx,polygon_centery,polygon_centerx+lengthdir_x(current_ratio*polygon_radius,current_angle),polygon_centery+lengthdir_y(current_ratio*polygon_radius,current_angle),polygon_centerx+lengthdir_x(next_ratio*polygon_radius,current_angle+angle_diff),polygon_centery+lengthdir_y(next_ratio*polygon_radius,current_angle+angle_diff),false);
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		//draw_text_outlined(polygon_centerx+lengthdir_x(polygon_radius+30,current_angle),polygon_centery+lengthdir_y(polygon_radius+30,current_angle),stats_name_list[| i]+"\r("+string(round(current_var))+")",stats_color_list[| i],c_lightblack,1,3,20);
		//draw_sprite_ext(spr_button_polygon,-1,polygon_centerx+lengthdir_x(polygon_radius+10,current_angle),polygon_centery+lengthdir_y(polygon_radius+10,current_angle),1,1,0,stats_color_list[| i],image_alpha);
		//draw_text_outlined(polygon_centerx+lengthdir_x(polygon_radius+10,current_angle),polygon_centery+lengthdir_y(polygon_radius+10,current_angle),stats_name_list[| i],stats_color_list[| i],c_lightblack,1,3,20);
		
		
		/*if player_assign_points && obj_control.game_map[? GAME_UNSPEND_STATS_POINTS] > 0
		{
			var stats_up_xx = polygon_centerx+lengthdir_x(polygon_radius+30,current_angle) + string_width(stats_name_list[| i])/2 + 10;
			var stats_up_yy = polygon_centery+lengthdir_y(polygon_radius+30,current_angle)-10;
			var stats_up_spr = spr_button_increase_stats;
			var stats_up_spr_width = sprite_get_width(spr_button_increase_stats);
			var stats_up_spr_height = sprite_get_height(spr_button_increase_stats);
			var stats_up_hover = mouse_over_area(stats_up_xx,stats_up_yy,stats_up_spr_width,stats_up_spr_height);
			if stats_up_hover && action_button_pressed_get()
			{
				action_button_pressed_set(false);
				var current_stats_current_stats_varvar = stats_name_list[| i];
				
				var char = obj_control.game_map[? GAME_PLAYER]
				var old_stamina_max = char_get_stamina_max(char);
				var old_health_max =char_get_health_max(char);
				var old_toru_max =char_get_toru_max(char);

				char[? current_stats_current_stats_varvar]++;
				obj_control.game_map[? GAME_UNSPEND_STATS_POINTS]--; 
				
				var new_stamina_max = char_get_stamina_max(char);
				var new_health_max =char_get_health_max(char);
				var new_toru_max =char_get_toru_max(char);
				
				char[? GAME_CHAR_STAMINA] += new_stamina_max - old_stamina_max;
				char[? GAME_CHAR_HEALTH] += new_health_max - old_health_max;
				char[? GAME_CHAR_TORU] += new_toru_max - old_toru_max;
				
				
			}
			draw_sprite(stats_up_spr,stats_up_hover,stats_up_xx,stats_up_yy);
			
		
		}*/

        current_angle+=angle_diff;
    }
	
	var current_angle = 38.5;
    for(var i = 0; i < ds_list_size(stats_list);i++)
    {
		var xx = polygon_centerx+lengthdir_x(polygon_radius+10,current_angle);
		var yy = polygon_centery+lengthdir_y(polygon_radius+10,current_angle);
		var button_width = sprite_get_width(stats_button_list[| i])*(polygon_radius+50)/200;
		var button_height = sprite_get_height(stats_button_list[| i])*(polygon_radius+50)/200;
		var button_hover = mouse_over_area_center(xx,yy,button_width,button_height);
		
		draw_sprite_ext(stats_button_list[| i],-1,xx,yy,(polygon_radius+50)/350,(polygon_radius+50)/350,0,stats_color_list[| i],image_alpha-(button_hover*image_alpha/4));
		
		if button_hover
		{
			if polygon_in_menu && menu_exists() && instance_exists(obj_menu)
				obj_menu.tooltip = stats_des_list[| i];
			else if !polygon_in_menu && !menu_exists() && !main_gui_clicking_disabled() && image_alpha > 0
				draw_set_floating_tooltip(stats_des_list[| i],xx,yy-button_height/2-5,fa_center,fa_bottom,false);
		}
		
		//draw_set_font(font_general_20);
		//draw_text_outlined(xx,yy,stats_name_list[| i]/*+"\r("+string(stats_list[| i])+")"*/,stats_color_list[| i],c_lightblack,polygon_radius/150,3,20);
		
		
		//draw_set_font(font_01n_bold);
		//draw_text_transformed_color(xx,yy,stats_name_list[| i],(polygon_radius+50)/230,(polygon_radius+50)/230,0,c_lightblack,c_lightblack,c_lightblack,c_lightblack,image_alpha);
		
		
		/*if player_assign_points && obj_control.var_map[? VAR_PLAYER_UNSPEND_STATS_POINTS] > 0 && button_hover
		{
			draw_sprite_ext(spr_button_polygon_levelup,-1,xx,yy,(polygon_radius+50)/200,(polygon_radius+50)/200,0,c_white,image_alpha-(button_hover*image_alpha/4));
			if action_button_pressed_get()
			{
				action_button_pressed_set(false);
				
				var current_stats_current_stats_varvar = stats_name_list[| i];
				
				var char = obj_control.var_map[? VAR_PLAYER]
				var old_stamina_max = char_get_stamina_max(char);
				var old_health_max =char_get_health_max(char);
				var old_toru_max =char_get_toru_max(char);

				char[? current_stats_current_stats_varvar]++;
				obj_control.var_map[? VAR_PLAYER_UNSPEND_STATS_POINTS]--; 
				
				var new_stamina_max = char_get_stamina_max(char);
				var new_health_max =char_get_health_max(char);
				var new_toru_max =char_get_toru_max(char);
				
				char[? GAME_CHAR_STAMINA] += new_stamina_max - old_stamina_max;
				char[? GAME_CHAR_HEALTH] += new_health_max - old_health_max;
				char[? GAME_CHAR_TORU] += new_toru_max - old_toru_max;
				
			}
		
		}*/

		current_angle+=angle_diff;
	}	
	
	if polygon_in_menu && menu_exists() && instance_exists(obj_menu) && obj_menu.tooltip == "" && mouse_over_area_center(polygon_centerx,polygon_centery,polygon_radius*2,polygon_radius*2)
	{
		obj_menu.tooltip = stats_des_all;
	}
	if !polygon_in_menu && !menu_exists() && !main_gui_clicking_disabled() && obj_control.floating_tooltip == "" && mouse_over_area_center(polygon_centerx,polygon_centery,polygon_radius*2,polygon_radius*2) && image_alpha > 0
	{
		draw_set_floating_tooltip(stats_des_all,mouse_x,mouse_y-5,fa_center,fa_bottom,false);
	}
	ds_list_destroy(stats_list);
	ds_list_destroy(stats_name_list);
	ds_list_destroy(stats_color_list);
	draw_reset();