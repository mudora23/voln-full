///ally_r_get_char(index/name);
/// @description
/// @param index/name
var ally_list = obj_control.var_map[? VAR_ALLY_R_LIST];
if is_string(argument0)
{
	for(var i = 0; i < ds_list_size(ally_list);i++)
	{
		var char_map = ally_list[| i];
		if char_map[? GAME_CHAR_NAME] == argument0
			return char_map;
	}
	return noone;
}
else if ds_list_size(ally_list) > argument0
	return ally_list[| argument0];
else
	return noone;