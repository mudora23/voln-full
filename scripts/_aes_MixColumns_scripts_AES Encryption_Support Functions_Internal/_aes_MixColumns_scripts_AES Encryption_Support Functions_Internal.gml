///_aes_MixColumns(state)
var i, Tmp,Tm,t, state = argument0;

for(i = 0; i < 4; ++i)
{  
    t   = state[@ i,0];
    Tmp = state[@ i,0] ^ state[@ i,1] ^ state[@ i,2] ^ state[@ i,3] ;
    Tm  = state[@ i,0] ^ state[@ i,1] ; Tm = _aes_xtime(Tm);  state[@ i,0] ^= Tm ^ Tmp ;
    Tm  = state[@ i,1] ^ state[@ i,2] ; Tm = _aes_xtime(Tm);  state[@ i,1] ^= Tm ^ Tmp ;
    Tm  = state[@ i,2] ^ state[@ i,3] ; Tm = _aes_xtime(Tm);  state[@ i,2] ^= Tm ^ Tmp ;
    Tm  = state[@ i,3] ^ t ;            Tm = _aes_xtime(Tm);  state[@ i,3] ^= Tm ^ Tmp ;
}

