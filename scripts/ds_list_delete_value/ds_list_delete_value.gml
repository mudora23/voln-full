///ds_list_delete_value(id,value);
/// @description - delete the value if its in the list
/// @param id
/// @param value
var the_index = ds_list_find_index(argument[0],argument[1]);
if the_index >= 0 ds_list_delete(argument[0],the_index);