///pet_is_full();
/// @description
return ds_list_size(var_map_get(VAR_PET_LIST)) >= 4;