///string_to_array(string)
var str = argument0, len = string_length(str), b, i;

for (i = 1; i <= len; ++i) {
    b[i - 1] = ord(string_char_at(str, i));
} 

return b;
