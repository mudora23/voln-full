///scene_set_from_type();
/// @description

var selected_scene_list = ds_list_create();

for(var i = 0; i < ds_list_size(var_map_get(VAR_TEMP_SCENE_LIST));i++)
{
	var the_checking_scene = ds_list_find_value(var_map_get(VAR_TEMP_SCENE_LIST),i);
	
	if var_map_get(VAR_TEMP_SCENE_TYPE) == ""
		ds_list_add_unique(selected_scene_list,the_checking_scene);
	else if scene_get_type(the_checking_scene) == var_map_get(VAR_TEMP_SCENE_TYPE)
		ds_list_add_unique(selected_scene_list,the_checking_scene);
}

ds_list_move_unread_story_chunk_to_front(selected_scene_list);

if ds_list_size(selected_scene_list) > 0
	var_map_set(VAR_TEMP_SCENE, ds_list_find_value(selected_scene_list,0));
else
	scene_jump("Selectednoscene");

ds_list_destroy(selected_scene_list);
