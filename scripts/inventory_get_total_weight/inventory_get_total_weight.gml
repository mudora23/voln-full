///inventory_get_total_weight();
/// @description

with obj_control
{
	var name_list = obj_control.var_map[? VAR_INVENTORY_LIST];
	var amount_list = obj_control.var_map[? VAR_INVENTORY_AMOUNT_LIST];
	var total_weight = 0;
	
	for(var i = 0; i < ds_list_size(name_list);i++)
	{
		var name = name_list[| i];
		var weight = item_get_weight(name);
		var amount = amount_list[| i];
		if amount > 0 total_weight += weight*amount;
	}

	return round(total_weight);
}