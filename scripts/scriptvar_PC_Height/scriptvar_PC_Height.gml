///scriptvar_PC_Height();
/// @description
var num = var_map_get(PC_Height_N);
var returning_str = string(floor(num))+"ş";
if floor(num) != num
	returning_str += " " + string(round((num-floor(num))*10))+"n";
return returning_str;