///_dialog_item(item_name,amount);
/// @descirption
/// @param item_name
/// @param amount
if scene_is_current()
{
	var dialog_list = ds_list_create();
	ds_list_add(dialog_list,text_add_markup("Item received: ",TAG_FONT_N,TAG_COLOR_HEAL));
	ds_list_add(dialog_list,text_add_markup(item_get_displayname(argument[0]),TAG_FONT_N,TAG_COLOR_EFFECT));
	if argument_count <= 1 || argument[1] > 1
		ds_list_add(dialog_list,text_add_markup(" x"+string(argument[1]),TAG_FONT_N,TAG_COLOR_EFFECT));
	var dialog = string_append_from_list(dialog_list);
	_dialog(dialog,false,true);
	ds_list_destroy(dialog_list);
}
else
{
	var_map_add(VAR_SCRIPT_POSI_FLOATING,1); // _dialog will handle this otherwise.
}
