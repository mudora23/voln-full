///scriptvar_PC_Peen_Glans_Desc();
/// @description
if var_map_get(PC_Peen_Glans_Size_N) > var_map_get(PC_Peen_SizeG_N)+0.1
    return choose("bulbous","bulging","bigger");
else if var_map_get(PC_Peen_Glans_Size_N) < var_map_get(PC_Peen_SizeG_N)-0.1
    return choose("slender","narrow","smaller");
else
    return choose("matching","proportional");