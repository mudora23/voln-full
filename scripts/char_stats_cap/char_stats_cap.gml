///char_stats_cap(char);
/// @description
/// @param char
argument0[? GAME_CHAR_VITALITY] = clamp(argument0[? GAME_CHAR_VITALITY],0,char_get_vitality_max(argument0));
argument0[? GAME_CHAR_HEALTH] = clamp(argument0[? GAME_CHAR_HEALTH],0,char_get_health_max(argument0));
argument0[? GAME_CHAR_STAMINA] = clamp(argument0[? GAME_CHAR_STAMINA],0,char_get_stamina_max(argument0));
argument0[? GAME_CHAR_LUST] = clamp(argument0[? GAME_CHAR_LUST],0,char_get_lust_max(argument0));
argument0[? GAME_CHAR_TORU] = clamp(argument0[? GAME_CHAR_TORU],0,char_get_toru_max(argument0));