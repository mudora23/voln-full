///postfx_ps_onhit(x,y);
/// @description -
/// @param x
/// @param y

if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);
part_emitter_region(global.ps, global.ps_OnHit1, xp-1, xp+1, yp-4, yp+4, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_OnHit1, global.ps_OnHit1, 1);
part_emitter_region(global.ps, global.ps_OnHit2, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_OnHit2, global.ps_OnHit2, 10);
part_emitter_region(global.ps, global.ps_OnHit3, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_OnHit3, global.ps_OnHit3, 10);