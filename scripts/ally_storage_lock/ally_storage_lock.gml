///ally_storage_lock(Name,lock);
/// @description
/// @param Index/Name
/// @param lock
with obj_control
{
    var char_map = ally_get_char(argument0);
    char_map[? GAME_CHAR_STORAGE_LOCK] = argument1;
}