///scriptvar_PC_Lips_Size_Dist_Desc();
/// @description
var num = var_map_get(PC_Lips_Size_Dist_N);
if num == 0 return "both about matched in size";
else if num == 1 return "the upper being the larger";
else return "the lower being the larger";