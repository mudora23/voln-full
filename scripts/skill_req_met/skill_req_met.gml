///skill_req_met(char,skill);
/// @description
/// @param char
/// @param skill

var char = argument0;
var skill = argument1;
var skill_map = obj_control.skill_wiki_map[? skill];
var req_list = skill_map[? SKILL_REQ_SKILL_LIST];

if ds_list_size(req_list) == 0 return true;	

for (var i = 0 ; i < ds_list_size(req_list);i++)
{
	var skill_key = req_list[| i];
	var skill_max_level = skill_get_level_max(skill_key);
	if char_get_skill_level(char,skill_key) >= skill_max_level return true;	
}
return false;







