///battle_ally_get_combat_capable_number();
/// @description
var total = 0;
var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
for(var i = 0;i< ds_list_size(ally_list);i++)
{
	if battle_char_get_combat_capable( ally_list[| i])
		total++;
}

return total;
