///story_chunk_get_full_path(chunk_name);
/// @description
/// @param chunk_name

var folder_name = string_copy_prefix(argument[0],"_");
var folder_name_encode = base64_encode(folder_name);
return working_directory+"data\\"+folder_name_encode+"\\"+base64_encode(argument[0])+".dat";
