///char_get_DEX(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_DEX);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_DEX_1);
amount += char_get_skill_level(argument0,SKILL_DEX_2);
amount += char_get_skill_level(argument0,SKILL_DEX_3);
amount += char_get_skill_level(argument0,SKILL_DEX_4);
amount += char_get_skill_level(argument0,SKILL_DEX_5);
amount += char_get_skill_level(argument0,SKILL_DEX_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;