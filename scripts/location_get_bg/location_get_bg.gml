///location_get_bg(LOCATION[optional]);
/// @description
/// @param LOCATION[optional]
with obj_control
{
	if argument_count > 0 var location = location_map[? argument[0]];
	else var location = location_map[? location_get_current()];
	return location[? "bg"];
}