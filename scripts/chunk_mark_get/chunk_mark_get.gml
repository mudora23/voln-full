///chunk_mark_get(chunk_name);
/// @description
/// @param chunk_name
if ds_map_exists(obj_control.var_map[? VAR_CHUNK_MARKED_AS_READ_MAP],argument0)
	return ds_map_find_value(obj_control.var_map[? VAR_CHUNK_MARKED_AS_READ_MAP],argument0);
else
	return false;
