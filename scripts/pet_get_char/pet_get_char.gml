///pet_get_char(index/name);
/// @description
/// @param index/name
var char_list = obj_control.var_map[? VAR_PET_LIST];
if is_string(argument0)
{
	for(var i = 0; i < ds_list_size(char_list);i++)
	{
		var char_map = char_list[| i];
		if char_map[? GAME_CHAR_NAME] == argument0
			return char_map;
	}
	return noone;
}
else if ds_list_size(char_list) > argument0
	return char_list[| argument0];
else
	return noone;