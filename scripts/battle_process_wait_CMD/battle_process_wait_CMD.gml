///battle_process_wait_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
	
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);
	
	//draw_floating_text_debug("------Battle 'Wait CMD script' is executed!------");
}