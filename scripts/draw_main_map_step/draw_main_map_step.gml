with obj_main
{
	var path_and_neighbor_list = obj_control.var_map[? VAR_LOCATION_PATH_AND_NEIGHBOR_LIST];
	
	if obj_control.var_map[? VAR_MODE] == VAR_MODE_MAP
	{
		if floor(map_animated_frame) == 0
			map_animated_frame += (1/room_speed)/1.5;
		else
			map_animated_frame += (1/room_speed)*10;
		
		if floor(map_animated_frame) >= 3
			map_animated_frame = 0;
	
	
		// map bg
		//draw_sprite_ext(spr_map_bg,-1,xoffset,yoffset,1,1,0,c_white,0.8);
	
		draw_map_in_area(map,map_xsize,map_ysize,round(x),round(y),area_width,area_height,round(map_xoffset),round(map_yoffset),1,map_zoom_ratio,map_zoom_ratio);

	
		// restrict the offsets

		map_xoffset = clamp(map_xoffset,0,(map_total_width * map_zoom_ratio - area_width));
		map_yoffset = clamp(map_yoffset,0,(map_total_height * map_zoom_ratio - area_height));
		map_xoffset_target = clamp(map_xoffset_target,0,(map_total_width * map_zoom_ratio_target - area_width));
		map_yoffset_target = clamp(map_yoffset_target,0,(map_total_height * map_zoom_ratio_target - area_height));

	
		// target position transition
		map_zoom_ratio = approach(map_zoom_ratio,map_zoom_ratio_target,FADE_FAST_SEC*(1/room_speed));
		map_xoffset = smooth_approach_room_speed(map_xoffset,map_xoffset_target,10);
		map_yoffset = smooth_approach_room_speed(map_yoffset,map_yoffset_target,10); 
	
		// offsets
		var xoffset = x - map_xoffset;
		var yoffset = y - map_yoffset;
	

		if obj_map_player.path_position != 1
		{
			// neighbor location
			for(var i = 0; i < ds_list_size(path_and_neighbor_list);i+=2)
			{
				var location_path = path_and_neighbor_list[| i];
				var location_name = path_and_neighbor_list[| i+1];
				if location_is_visible(location_name)
				{
					var location_info = obj_control.location_map[? location_name];
					var location_x = location_info[? "x"];
					var location_y = location_info[? "y"];
					var location_hidden = location_is_locked(location_name);//location_info[? "hidden"];
					var location_marker = location_info[? "marker"];
					var location_marker_width = sprite_get_width(location_marker);
					var location_marker_height = sprite_get_height(location_marker);
					var location_hover = mouse_over_area(xoffset+location_x*map_zoom_ratio-location_marker_width/2,yoffset+location_y*map_zoom_ratio-location_marker_height/2,location_marker_width,location_marker_height) && mouse_over_area(x,y,area_width,area_height) && image_alpha > 0;
			
					//reverse if necessary
					if path_get_point_x(location_path,0) == location_x &&
					   path_get_point_y(location_path,0) == location_y
					{
					   path_reverse(location_path);
					}
				
					// path drawing
					var path_length = path_get_length(location_path);
					var path_point_dis = 40;
					for(var jj = 0;jj<1;jj+=path_point_dis/path_length)
					{
						draw_sprite_in_visible_area(spr_map_path_point,0,xoffset+path_get_x( location_path, jj )*map_zoom_ratio-sprite_get_width(spr_map_path_point)/2,yoffset+path_get_y( location_path, jj )*map_zoom_ratio-sprite_get_height(spr_map_path_point)/2,1,1,c_white,1,x,y,area_width,area_height);
					}


					// draw location
					var the_x = xoffset+location_x*map_zoom_ratio-location_marker_width/2;
					var the_y = yoffset+location_y*map_zoom_ratio-location_marker_height/2;
					if location_hover
						draw_sprite_in_visible_area(location_marker,1,the_x,the_y,1,1,c_white,1,x,y,area_width,area_height);
					draw_sprite_in_visible_area(location_marker,!location_is_visited(location_name)*(2+map_animated_frame),the_x,the_y,1,1,c_white,1,x,y,area_width,area_height);
					if location_hover && !menu_exists()
						draw_set_floating_tooltip(location_info[? "name"],the_x+location_marker_width/2,the_y-30,fa_center,fa_bottom,false);
				
					if location_hover && mouse_check_button_pressed(mb_left) && !menu_exists() && obj_map_player.path_position == 0
					{
						scene_choices_list_clear();
						with obj_map_player
						{
							path_start(location_path,3,path_action_stop,true);
							obj_control.target_location = location_name;
						}
		
					}
				
				
				}
		
			}	
			
			// player center forced
			if obj_map_player.path_position > 0 && obj_map_player.path_position < 1
			{
				map_xoffset_target = obj_map_player.x*map_zoom_ratio - area_width/2;
				map_yoffset_target = obj_map_player.y*map_zoom_ratio - area_height/2;
			}
			
			var location_name = obj_control.var_map[? VAR_LOCATION];
			var location_info = obj_control.location_map[? location_name];
			var location_x = location_info[? "x"];
			var location_y = location_info[? "y"];
			var location_hidden = location_is_locked(location_name);//location_info[? "hidden"];
			var location_marker = location_info[? "marker"];
			var location_marker_width = sprite_get_width(location_marker);
			var location_marker_height = sprite_get_height(location_marker);
			var location_hover = mouse_over_area(xoffset+location_x*map_zoom_ratio-location_marker_width/2,yoffset+location_y*map_zoom_ratio-location_marker_height/2,location_marker_width,location_marker_height) && mouse_over_area(x,y,area_width,area_height) && image_alpha > 0;
			
			draw_sprite_in_visible_area(location_marker,0,xoffset+location_x*map_zoom_ratio-location_marker_width/2,yoffset+location_y*map_zoom_ratio-location_marker_height/2,1,1,c_white,1,x,y,area_width,area_height);
			var the_x = xoffset+location_x*map_zoom_ratio-location_marker_width/2;
			var the_y = yoffset+location_y*map_zoom_ratio-location_marker_height/2;
			if location_hover && !menu_exists()
				draw_set_floating_tooltip(location_info[? "name"],the_x+location_marker_width/2,the_y-30,fa_center,fa_bottom,false);
				
		
		
			// ALL other visible locations
			var visible_location_list = obj_control.var_map[? VAR_LOCATION_VISIBLE_LIST];
			for(var i = 0; i < ds_list_size(visible_location_list);i++)
			{
				var the_visible_location = visible_location_list[| i];
				if !location_is_current(the_visible_location) && !location_is_in_path_and_neighbor_list(the_visible_location)
				{
			
					var location_name = the_visible_location;
					var location_info = obj_control.location_map[? location_name];
					var location_x = location_info[? "x"];
					var location_y = location_info[? "y"];
					var location_marker = location_info[? "marker"];
					var location_marker_width = sprite_get_width(location_marker);
					var location_marker_height = sprite_get_height(location_marker);
					var location_hover = mouse_over_area(xoffset+location_x*map_zoom_ratio-location_marker_width/2,yoffset+location_y*map_zoom_ratio-location_marker_height/2,location_marker_width,location_marker_height) && mouse_over_area(x,y,area_width,area_height) && image_alpha > 0;
		
					// draw location
					draw_sprite_in_visible_area(location_marker,!location_is_visited(location_name)*(2+map_animated_frame),xoffset+location_x*map_zoom_ratio-location_marker_width/2,yoffset+location_y*map_zoom_ratio-location_marker_height/2,1,1,c_white,0.7,x,y,area_width,area_height);
					var the_x = xoffset+location_x*map_zoom_ratio-location_marker_width/2;
					var the_y = yoffset+location_y*map_zoom_ratio-location_marker_height/2;
					if location_hover && !menu_exists()
						draw_set_floating_tooltip(location_info[? "name"],the_x+location_marker_width/2,the_y-30,fa_center,fa_bottom,false);
			
			
				}
		
			}
		}
		else
		{
			obj_map_player.path_position = 0;
			/*if file_exists("storyscript.dat") obj_control.storyscript = ds_map_json_load("storyscript.dat");
		
			if ds_map_exists(obj_control.storyscript,"default")
			{
				obj_control.storyscript = voln_story_load(obj_control.storyscript);
		
			}*/
		
		
			obj_control.var_map[? VAR_LOCATION] = obj_control.target_location;
			location_map_update_path_and_neighbor_list();
			location_map_update_visited_and_visible_info();
		
			//game_time_one_hour_passes();
			var_map_set(VAR_MODE,VAR_MODE_NORMAL);
			scene_jump("","CSX_arrived");
			//show_message("scene_jump('','CSX_arrived'); in draw_main_map_step");
		
		}
	
		// player icon
		if obj_map_player.xprevious == obj_map_player.x && obj_map_player.yprevious == obj_map_player.y
		{
			var dir = 0;
			player_icon_image_index_dir = 6;
			player_icon_image_index = 0;
			player_icon_image_index_speed = 0;
		}
		else
		{
			var dir = point_direction(obj_map_player.xprevious,obj_map_player.yprevious,obj_map_player.x,obj_map_player.y);
			if dir >= 338 || dir < 23
				player_icon_image_index_dir = 0;
			else if dir < 68
				player_icon_image_index_dir = 1;
			else if dir < 113
				player_icon_image_index_dir = 2;
			else if dir < 158
				player_icon_image_index_dir = 3;
			else if dir < 203
				player_icon_image_index_dir = 4;
			else if dir < 248
				player_icon_image_index_dir = 5;
			else if dir < 293
				player_icon_image_index_dir = 6;
			else if dir < 338
				player_icon_image_index_dir = 7;
			else
				player_icon_image_index_dir = 8;
			
			player_icon_image_index_speed+=(1/room_speed);
			if player_icon_image_index_speed >= player_icon_image_index_speed_max
			{
				player_icon_image_index++;
				player_icon_image_index_speed = 0;
				if player_icon_image_index >= player_icon_image_index_dir_frame
					player_icon_image_index = 0;
			}

		}
		//if point_in_rectangle(xoffset+obj_map_player.x,yoffset+obj_map_player.y,x,y,area_width,area_height)
		
		if (obj_control.target_location == LOCATION_55 && var_map_get(VAR_LOCATION) == LOCATION_18 && obj_map_player.path_position > 0.15 && obj_map_player.path_position < 0.90) ||
			(obj_control.target_location == LOCATION_18 && var_map_get(VAR_LOCATION) == LOCATION_55 && obj_map_player.path_position > 0.10 && obj_map_player.path_position < 0.85)
			{
				var spr_icon_player = spr_icon_player_ship;
				var spr_icon_player_glow = spr_icon_player_ship_glow;
			}
		else
		{
			var spr_icon_player = spr_icon_player_wagon;
			var spr_icon_player_glow = spr_icon_player_wagon_glow;	
			
		}

		
		var player_cx = xoffset+obj_map_player.x*map_zoom_ratio;
		var player_cy = yoffset+obj_map_player.y*map_zoom_ratio;
		var player_x = player_cx-obj_map_player.sprite_xoffset;
		var player_y = player_cy-obj_map_player.sprite_yoffset;
		var player_width = sprite_get_width(spr_icon_player);
		var player_height = sprite_get_height(spr_icon_player);
		if mouse_over_area_center(player_cx,player_cy,player_width,player_height)
			draw_sprite_in_visible_area(spr_icon_player_glow,player_icon_image_index_dir*player_icon_image_index_dir_frame+player_icon_image_index,player_x,player_y,1,1,c_battle_outline,0.8,x,y,area_width,area_height);
		draw_sprite_in_visible_area(spr_icon_player,player_icon_image_index_dir*player_icon_image_index_dir_frame+player_icon_image_index,player_x,player_y,1,1,c_white,0.8,x,y,area_width,area_height);
	
	
		// map GUI
		var on_map_gui = false;
		var padding = 5;
		var player_icon = spr_map_player_icon;
		var player_icon_width = sprite_get_width(player_icon);
		var player_icon_height = sprite_get_height(player_icon);
		var player_icon_x = x + area_width - padding - sprite_get_width(spr_arrow_right) - padding - player_icon_width;
		var player_icon_y = y + area_height - padding - sprite_get_height(spr_arrow_down) - padding - player_icon_height;
		var hover = mouse_over_area(player_icon_x,player_icon_y,player_icon_width,player_icon_height);
		on_map_gui = on_map_gui || hover;
		draw_sprite_ext(player_icon,hover,player_icon_x,player_icon_y,1,1,0,c_white,map_alpha/4 + hover*map_alpha/4);
		if hover && mouse_check_button_pressed(mb_left) && !menu_exists()
		{
			map_xoffset_target = obj_map_player.x*map_zoom_ratio - area_width/2;
			map_yoffset_target = obj_map_player.y*map_zoom_ratio - area_height/2;
		}
	
			// up
			var spr = spr_arrow_up;
			var spr_width = sprite_get_width(spr);
			var spr_height = sprite_get_height(spr);
			var spr_x = player_icon_x + player_icon_width/2 - spr_width/2;
			var spr_y = player_icon_y - padding - spr_height;
			var hover = mouse_over_area(spr_x,spr_y,spr_width,spr_height);
			on_map_gui = on_map_gui || hover;
			draw_sprite_ext(spr,hover,spr_x,spr_y,1,1,0,c_white,map_alpha/4 + hover*map_alpha/4);
			if hover && mouse_check_button_pressed(mb_left) && !menu_exists() map_yoffset_target -= 100;
	
			// down
			var spr = spr_arrow_down;
			var spr_width = sprite_get_width(spr);
			var spr_height = sprite_get_height(spr);
			var spr_x = player_icon_x + player_icon_width/2 - spr_width/2;
			var spr_y = player_icon_y + player_icon_height + padding;
			var hover = mouse_over_area(spr_x,spr_y,spr_width,spr_height);
			on_map_gui = on_map_gui || hover;
			draw_sprite_ext(spr,hover,spr_x,spr_y,1,1,0,c_white,map_alpha/4 + hover*map_alpha/2);
			if hover && mouse_check_button_pressed(mb_left) && !menu_exists() map_yoffset_target += 100;
		
			// left
			var spr = spr_arrow_left;
			var spr_width = sprite_get_width(spr);
			var spr_height = sprite_get_height(spr);
			var spr_x = player_icon_x - padding - spr_width;
			var spr_y = player_icon_y + player_icon_height/2 - spr_height/2;
			var hover = mouse_over_area(spr_x,spr_y,spr_width,spr_height);
			on_map_gui = on_map_gui || hover;
			draw_sprite_ext(spr,hover,spr_x,spr_y,1,1,0,c_white,map_alpha/4 + hover*map_alpha/2);
			if hover && mouse_check_button_pressed(mb_left) && !menu_exists() map_xoffset_target -= 100;
		
			// right
			var spr = spr_arrow_right;
			var spr_width = sprite_get_width(spr);
			var spr_height = sprite_get_height(spr);
			var spr_x = player_icon_x + player_icon_width + padding;
			var spr_y = player_icon_y + player_icon_height/2 - spr_height/2;
			var hover = mouse_over_area(spr_x,spr_y,spr_width,spr_height);
			on_map_gui = on_map_gui || hover;
			draw_sprite_ext(spr,hover,spr_x,spr_y,1,1,0,c_white,map_alpha/4 + hover*map_alpha/2);
			if hover && mouse_check_button_pressed(mb_left) && !menu_exists() map_xoffset_target += 100;
		
			// zoom
			/*if mouse_over_area(x,y,area_width,area_height)
			{
				if mouse_wheel_down()
				{
					if map_zoom_ratio > 0.3
					{
						map_zoom_ratio_target-=0.1;
						map_zoom_ratio-=0.1;
						map_xoffset -= map_total_width*0.1;
						map_xoffset_target -= map_total_width*0.1;
						map_yoffset -= map_total_height*0.1;
						map_yoffset_target -= map_total_height*0.1;
					}
				}
				if mouse_wheel_up()
				{
					if map_zoom_ratio < 1
					{
						map_zoom_ratio_target+=0.1;
						map_zoom_ratio+=0.1;
						map_xoffset += map_total_width*0.1;
						map_xoffset_target += map_total_width*0.1;
						map_yoffset += map_total_height*0.1;
						map_yoffset_target += map_total_height*0.1;
					}
				}

			}	*/

		
		// drag and drop
		if mouse_check_button_pressed(mb_left) && mouse_over_area(x,y,area_width,area_height) && image_alpha > 0 && !on_map_gui && !menu_exists()
			map_holding = true;
		if !mouse_check_button(mb_left)
			map_holding = false;
		if map_holding
		{
			map_xoffset -= mouse_x - obj_control.mouse_xprevious;
			map_yoffset -= mouse_y - obj_control.mouse_yprevious;
			
			map_xoffset = clamp(map_xoffset,0,(map_total_width * map_zoom_ratio - area_width));
			map_yoffset = clamp(map_yoffset,0,(map_total_height * map_zoom_ratio - area_height));
		
			map_xoffset_target = map_xoffset;
			map_yoffset_target = map_yoffset;
		}
	
	
		// Center the ship
		if obj_control.var_map[? VAR_LOCATION] != obj_control.target_location && !map_holding && mouse_check_button_pressed(mb_left) && mouse_over_area(player_icon_x,player_icon_y,player_icon_width,player_icon_height) && !menu_exists()
		{
			map_xoffset_target = obj_map_player.x*map_zoom_ratio - area_width/2;
			map_yoffset_target = obj_map_player.y*map_zoom_ratio - area_height/2;
			
			map_xoffset_target = clamp(map_xoffset_target,0,(map_total_width * map_zoom_ratio - area_width));
			map_xoffset_target = clamp(map_xoffset_target,0,(map_total_height * map_zoom_ratio - area_height));
	
		}
		
		
		

	}
}