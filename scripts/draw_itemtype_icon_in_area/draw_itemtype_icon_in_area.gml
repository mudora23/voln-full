///draw_itemtype_icon_in_area(x1,y1,w,h,itemtype);
///@description
///@param x1
///@param y1
///@param w
///@param h
///@param itemtype
if argument4 == ITEMTYPE_TONIC
	var spr = spr_itemtype_tonic;
else if argument4 == ITEMTYPE_FOOD
	var spr = spr_itemtype_food;
else
	var spr = spr_itemtype_misc;

draw_sprite_stretched(spr, 0, argument0, argument1, argument2, argument3);