///game_time_15min_passes(sleeping?);
/// @description
with obj_control
{
	obj_control.var_map[? VAR_HOURS_SINCE_LAST_SLEEP] += 0.25;
	obj_control.var_map[? VAR_HOURS_SINCE_LAST_REST] += 0.25;
	obj_control.var_map[? VAR_HOURS_SINCE_LAST_BLESSING] += 0.25;
	
	var inventory_total_weight = inventory_get_total_weight();
	var inventory_max_weight = inventory_get_max_weight();
	var inventory_overweight_ratio = inventory_total_weight / inventory_max_weight;
	if inventory_overweight_ratio > 1 var vitality_penalty_ratio = 1 + inventory_overweight_ratio;
	else var vitality_penalty_ratio = 1;
	

	//if ds_map_exists(var_map,RBS_IMP_D_3_1_HOUR_COUNTER) obj_control.var_map[? RBS_IMP_D_3_1_HOUR_COUNTER] += 0.25;

    obj_control.var_map[? VAR_TIME_HOUR]+=0.25;
    while obj_control.var_map[? VAR_TIME_HOUR] >= 20
    {
        obj_control.var_map[? VAR_TIME_HOUR]-=20;
        obj_control.var_map[? VAR_TIME_DAY]++;
        while ((obj_control.var_map[? VAR_TIME_MONTH] == 7 || obj_control.var_map[? VAR_TIME_MONTH] == 8) && obj_control.var_map[? VAR_TIME_DAY] >= 72) ||
              ((obj_control.var_map[? VAR_TIME_MONTH] != 7 && obj_control.var_map[? VAR_TIME_MONTH] != 8) && obj_control.var_map[? VAR_TIME_DAY] >= 36)
        {
            if (obj_control.var_map[? VAR_TIME_MONTH] == 7 || obj_control.var_map[? VAR_TIME_MONTH] == 8)
            {
                obj_control.var_map[? VAR_TIME_DAY]-=72;
                obj_control.var_map[? VAR_TIME_MONTH]++;
            }
            else
            {
                obj_control.var_map[? VAR_TIME_DAY]-=36;
                obj_control.var_map[? VAR_TIME_MONTH]++;
            }
            while obj_control.var_map[? VAR_TIME_MONTH] > 10
            {
                obj_control.var_map[? VAR_TIME_MONTH]-=10;
                obj_control.var_map[? VAR_TIME_YEAR]++;
            }
        }
    }
	
	var all_char_list = var_map_get_all_character_map_list();
	for(var i = 0; i < ds_list_size(all_char_list);i++)
	{
		var char_map = all_char_list[| i];
		
		battle_char_remove_all_long_term_debuff(char_map,0.25);
		battle_char_remove_all_long_term_buff(char_map,0.25);
		if argument_count < 1 || !argument[0]
			char_map[? GAME_CHAR_VITALITY] = max(char_map[? GAME_CHAR_VITALITY] - 1*vitality_penalty_ratio,0);
		
		if char_map[? GAME_CHAR_IMPREGNATE]
			char_map[? GAME_CHAR_IMPREGNATE_HOURS_SINCE] += 0.25;
			
		char_map[? GAME_CHAR_HOURS_SINCE_BORN] += 0.25;	
			
	}
	ds_list_destroy(all_char_list);
	
}