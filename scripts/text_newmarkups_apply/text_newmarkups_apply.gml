///text_newmarkups_apply(marked_text,raw_text_posi);
/// @description - Load markups within the range.
/// @param marked_text
/// @param raw_text_posi

var marked_text = argument[0];
var raw_text_posi = argument[1];
var marked_text_posi = marked_text_get_posi(raw_text_posi);
var marked_text_font_posi = marked_text_posi + 1;
var marked_text_color_posi = marked_text_posi + 2;

var text_font = string_char_at(marked_text,marked_text_font_posi);
var text_color = string_char_at(marked_text,marked_text_color_posi);

if real(text_font) == TAG_FONT_N draw_set_font(obj_control.font_01_reg); 
else if real(text_font) == TAG_FONT_B draw_set_font(obj_control.font_01_reg_bo);
else if real(text_font) == TAG_FONT_I draw_set_font(obj_control.font_01_reg_it);
else if real(text_font) == TAG_FONT_SC draw_set_font(obj_control.font_01_sc);
else if real(text_font) == TAG_FONT_IB draw_set_font(obj_control.font_01_reg_bo_it);
else if real(text_font) == TAG_FONT_BSC draw_set_font(obj_control.font_01_sc_bo);
else if real(text_font) == TAG_FONT_ISC draw_set_font(obj_control.font_01_sc_it);
else if real(text_font) == TAG_FONT_IBSC draw_set_font(obj_control.font_01_sc_bo_it);

if real(text_color) == TAG_COLOR_NORMAL draw_set_color(COLOR_NORMAL);
else if real(text_color) == TAG_COLOR_DAMAGE draw_set_color(COLOR_DAMAGE);
else if real(text_color) == TAG_COLOR_EFFECT draw_set_color(COLOR_EFFECT);
else if real(text_color) == TAG_COLOR_LOCATION draw_set_color(COLOR_LOCATION);
else if real(text_color) == TAG_COLOR_HEAL draw_set_color(COLOR_HEAL);
else if real(text_color) == TAG_COLOR_ENEMY_NAME draw_set_color(COLOR_ENEMY_NAME);
else if real(text_color) == TAG_COLOR_FLIRT draw_set_color(COLOR_FLIRT);
else if real(text_color) == TAG_COLOR_ALLY_NAME draw_set_color(COLOR_ALLY_NAME);
else if real(text_color) == TAG_COLOR_TORU draw_set_color(COLOR_TORU);

/*show_message("text_newmarkups_apply--- ");
show_message("marked_text: "+string(marked_text));
show_message("raw_text_posi: "+string(raw_text_posi));
show_message("raw_text_posi: "+string(raw_text_posi));*/













