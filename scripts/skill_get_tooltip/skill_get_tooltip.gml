///skill_get_tooltip(skill_name);
/// @desription
/// @param skill_name

var skill_map = obj_control.skill_wiki_map[? argument0];
if skill_map[? SKILLDISPLAYNAME] != ""
{
	if skill_map[? SKILLDES] == ""
		return skill_map[? SKILLDISPLAYNAME];
	else
		return skill_map[? SKILLDISPLAYNAME] + "   -   "+skill_map[? SKILLDES];
}
else
	return "";