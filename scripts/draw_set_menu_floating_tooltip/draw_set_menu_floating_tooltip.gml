///draw_set_menu_floating_tooltip(text,x,y,halign,valign,fancy);
/// @description
/// @param text
/// @param x
/// @param y
/// @param halign
/// @param valign
/// @param fancy

obj_control.menu_floating_tooltip = argument0;
obj_control.menu_floating_tooltip_x = argument1;
obj_control.menu_floating_tooltip_y = argument2;
obj_control.menu_floating_tooltip_halign = argument3;
obj_control.menu_floating_tooltip_valign = argument4;
obj_control.menu_floating_tooltip_fancy = argument5;