///CS_extra_RBS();
/// @description

_label("start");

_label("Afterbattle");
	if var_map_get(BATTLE_END_TYPE) == GAME_DOMINANT_VICTORY
		_jump("Afterbattle_DV");
	else if var_map_get(BATTLE_END_TYPE) == GAME_SEDUCTIVE_VICTORY
		_jump("Afterbattle_SV");
	else if var_map_get(BATTLE_END_TYPE) == GAME_DOMINANT_DEFEAT
		_jump("Afterbattle_DD");
	else if var_map_get(BATTLE_END_TYPE) == GAME_SEDUCTIVE_DEFEAT
		_jump("Afterbattle_SD");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Afterbattle_DV");
	if enemy_get_number() > 1
		_dialog("        Dominant Victory! You have the option to sex or dismiss some of the enemies.",true);
	else
		_dialog("        Dominant Victory! You have the option to sex or dismiss the enemy.",true);

	_execute(0,scene_choices_list_clear);
			
	if enemy_get_sex_number() == enemy_get_number()
		_choice("Sex all","Afterbattle_2");
	else if enemy_get_sex_number() == 2
		_choice("Sex both","Afterbattle_2");
	else if enemy_get_sex_number() > 0
		_choice("Sex","Afterbattle_2");
	else if enemy_get_number() > 2
		_choice("Dismiss all","Afterbattle_2");
	else if enemy_get_number() == 2
		_choice("Dismiss both","Afterbattle_2");
	else
		_choice("Dismiss","Afterbattle_2");

		
	_execute(0,"enemy_set_random_gender");
	
	_var_map_set(VAR_MODE,VAR_MODE_SEX_CHOOSING);
	
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Afterbattle_SV");
	_jump("Afterbattle_2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Afterbattle_DD");
	_jump("Afterbattle_2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Afterbattle_SD");
	_jump("Afterbattle_2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("Afterbattle_2");
	_var_map_set(VAR_MODE,VAR_MODE_NORMAL);
	_ds_list_clear(var_map_get(VAR_TEMP_SCENE_LIST));

	_var_map_set(VAR_EXTRA_AFTER_SCENE,"CS_prelocation");

	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"");
	_execute(0,"scene_list_all_battle_scene_list_init",var_map_get(VAR_TEMP_SCENE_LIST));
	_choices_types_from_scene_list(var_map_get(VAR_TEMP_SCENE_LIST));
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedtype");
	_execute(0,"scene_set_from_type");
	_jump("Selectedscene");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedscene");
	if string_count("RBS_ELF",var_map_get(VAR_TEMP_SCENE)) > 0
		_jump("Selectedscene","CS_extra_RBS_ELF");
	else if string_count("RBS_ORK",var_map_get(VAR_TEMP_SCENE)) > 0
		_jump("Selectedscene","CS_extra_RBS_ORK");
	else if string_count("RBS_IMP",var_map_get(VAR_TEMP_SCENE)) > 0
		_jump("Selectedscene","CS_extra_RBS_IMP");
	else if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedscene_jar_to_item");
	if string_count("RBS_ELF",var_map_get(VAR_TEMP_SCENE)) > 0
		_jump("Selectedscene","CS_extra_RBS_ELF");
	else if string_count("RBS_ORK",var_map_get(VAR_TEMP_SCENE)) > 0
		_jump("Selectedscene","CS_extra_RBS_ORK");
	else if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add_ext(VAR_JAR,-1,0,var_map_get(VAR_JAR));
	_dialog("        1 jar is removed from the inventory.");
	_execute(0,inventory_add_item,var_map_get(VAR_TEMP_ITEM),1);
	_dialog("        You receive "+string(item_get_displayname(var_map_get(VAR_TEMP_ITEM)))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectednoscene");
	_jump("Finish");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafter");
	_execute(0,player_ally_relieve_all);
_label("Noscenestoselect");
_label("Selectedsceneafternorelieve");
	if percent_chance(50) && (var_map_get(BATTLE_END_TYPE) == GAME_SEDUCTIVE_DEFEAT || var_map_get(BATTLE_END_TYPE) == GAME_DOMINANT_DEFEAT) && var_map_get(VAR_TEMP_SCENE) != "RBS_IMP_D_3_1"
		_gameover();
	else if enemy_get_sex_number() == 0
		_jump("Finish");
	else if inventory_item_count(ITEM_A_CHAIN_LEAD) == 0 || inventory_item_count(ITEM_SHACKLES) == 0
		_jump("Capturingmissing");
	else if pet_is_full()
		_jump("Capturingfull");
	else
		_jump("Capturing");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Capturingmissing");
	_dialog("CS_CAPTURING_MISSING");
	_jump("Finish");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Capturingfull");
	_dialog("CS_CAPTURING_FULL");
	_jump("Finish");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


_label("Capturing");
	_execute(0,scene_choices_list_clear);
			
	if enemy_get_capture_number() >= 1
		_choice("Capture","Aftercapturing");
	else
		_choice("Dismiss","Finish");

	_var_map_set(VAR_MODE,VAR_MODE_CAPTURE_CHOOSING);

	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Aftercapturing");
	_var_map_set(VAR_MODE,VAR_MODE_NORMAL);
	_execute(0,inventory_delete_item,ITEM_SHACKLES,1);
	_colorscheme(COLORSCHEME_GETITEM);
	_dialog("CS_CAPTURING_SHACKLES_USED");
	if enemy_get_capture_number() >= 1 && percent_chance(50)
		_jump("CapturedSuccess");
	else
		_jump("CapturedFail");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("CapturedSuccess");
	if pet_is_full()
		_dialog("CS_CAPTURING_SUCCESS_FULL");
	else
		_dialog("CS_CAPTURING_SUCCESS");
	_execute(0,pet_add_char_from_captured_enemy_list);
	_colorscheme(COLORSCHEME_NORMAL);
	
	
	if enemy_get_capture_and_impregnate_number() > 0
	{
		_ds_map_replace(char_id_get_char(var_map_get(VAR_TEMP_CHARID)),GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_ATTEMPT,true);
		_dialog("S_PREGNANT_ATTEMPT");
	}
	else
	{
		_void();
		_void();
	}
	
	_jump("Finish");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("CapturedFail");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("CS_CAPTURING_FAILURE");
	_jump("Finish");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Finish");
	_var_map_set(VAR_MODE,VAR_MODE_NORMAL);
	_ds_list_clear(var_map_get(VAR_ENEMY_LIST));
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
	// RBS
	_border(1000); // add lines after this
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

	
	
	
	
	
	
	
	