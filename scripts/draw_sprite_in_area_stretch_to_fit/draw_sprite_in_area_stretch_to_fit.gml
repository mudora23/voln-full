///draw_sprite_in_area_stretch_to_fit(spr,img,x,y,w,h,color,alpha);
/// @description
/// @param spr
/// @param img
/// @param x
/// @param y
/// @param w
/// @param h
/// @param color
/// @param alpha

var spr = argument0;
var img = argument1;
var xx = argument2;
var yy = argument3;
var w = argument4;
var h = argument5;
var color = argument6;
var alpha = argument7;

var xratio = w / sprite_get_width(spr);
var yratio = h / sprite_get_height(spr);

draw_sprite_ext(spr,img,xx,yy,xratio,yratio,0,color,alpha);
