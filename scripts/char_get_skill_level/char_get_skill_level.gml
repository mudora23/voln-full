///char_get_skill_level(char,skill);
/// @description
/// @param char
/// @param skill
var char_map = argument0;
var char_default_skill_map = char_map[? GAME_CHAR_SKILLS_MAP];
var char_bonus_skill_map = char_map[? GAME_CHAR_BONUS_SKILLS_MAP];

if !ds_map_exists(char_default_skill_map,argument1)
	var default_level = 0;
else
	var default_level = ds_map_find_value(char_default_skill_map,argument1);

if !ds_map_exists(char_bonus_skill_map,argument1)
	var bonus_level = 0;
else
	var bonus_level = ds_map_find_value(char_bonus_skill_map,argument1);
	
return max(default_level,bonus_level);