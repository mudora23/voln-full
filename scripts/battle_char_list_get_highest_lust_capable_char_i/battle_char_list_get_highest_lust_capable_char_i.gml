///char_list_get_closest_lustmax_capable_char_index(char_list);
/// @description
/// @param char_list

var current_char_index = noone;
var current_lust_ratio = 0;

for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];
	if battle_char_get_combat_capable(char)
	{
		if current_char_index == noone || battle_char_get_effective_lust_ratio(char) > current_lust_ratio
		{
			current_char_index = i;
			current_lust_ratio = battle_char_get_effective_lust_ratio(char);
		}

	}
}

return current_char_index;