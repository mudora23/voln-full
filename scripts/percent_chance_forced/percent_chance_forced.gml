///percent_chance_forced(forced,percent_chance);
///@description returns forced condition if forced; if forced is noone, returns true or false based on the chance.
///@param forced
///@param percent_chance

return (argument0 == noone && percent_chance(argument1)) || (argument0 != noone && argument0);
