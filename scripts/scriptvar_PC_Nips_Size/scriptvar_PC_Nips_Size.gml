///scriptvar_PC_Nips_Size();
/// @description
var num = var_map_get(PC_Nips_Size_N);
var returning_str = string(floor(num));
if num - floor(num) >= 0.1
	returning_str += "." + string(floor((num-floor(num))*10));
returning_str += "n";
return returning_str;