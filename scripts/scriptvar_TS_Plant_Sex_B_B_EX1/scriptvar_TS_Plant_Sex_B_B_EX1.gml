///scriptvar_TS_Plant_Sex_B_B_EX1();
/// @description

if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];
	if ally[? GAME_CHAR_GENDER] == GENDER_MALE
		var ally_his_her = "his";
	else if ally[? GAME_CHAR_GENDER] == GENDER_FEMALE
		var ally_his_her = "her";
	else
		var ally_his_her = "it";

	return string(ally_name)+" gawks at you.  Apparently, "+string(ally_his_her)+"’d been watching your performance, and more interestingly, your choice of intimate company.";
}
else if ally_get_number() > 1
{
	return "Your companions gawk at you.  Apparently, they’d been watching your performance, and more interestingly, your choice of intimate company.";
	
}
else
	return "";
