///battle_char_remove_all_long_term_buff(char,hours);
/// @description
/// @param char
/// @param hours
var char_map = argument0;
var char_buff_list = char_map[? GAME_CHAR_LONG_TERM_BUFF_LIST];
var char_buff_hours_list = char_map[? GAME_CHAR_LONG_TERM_BUFF_HOURS_LIST];
var char_buff_hours = argument1;

for(var i = 0; i < ds_list_size(char_buff_list);i++)
{
	char_buff_hours_list[| i] = char_buff_hours_list[| i]-char_buff_hours;
	if char_buff_hours_list[| i] <= 0
	{
		ds_list_delete(char_buff_list,i);
		ds_list_delete(char_buff_hours_list,i);
	}
}
