///scene_cg_list_add(ID,cg,alpha,alpha_target,fade_speed,slide_speed,order,halign,valign,slide_from_halign,slide_from_valign,hstick,vstick);
/// @description  
/// @param ID
/// @param cg
/// @param alpha
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
/// @param halign
/// @param valign
/// @param slide_from_halign
/// @param slide_from_valign
/// @param hstick
/// @param vstick

with obj_control
{
	var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	var cg_index_list = obj_control.var_map[? VAR_CG_LIST_INDEX_NAME];
	var cg_index_target_list = obj_control.var_map[? VAR_CG_LIST_INDEX_TARGET_NAME];
	var cg_x_list = obj_control.var_map[? VAR_CG_LIST_X];
	var cg_x_target_list = obj_control.var_map[? VAR_CG_LIST_X_TARGET];
	var cg_y_list = obj_control.var_map[? VAR_CG_LIST_Y];
	var cg_y_target_list = obj_control.var_map[? VAR_CG_LIST_Y_TARGET];
	var cg_alpha_list = obj_control.var_map[? VAR_CG_LIST_ALPHA];
	var cg_alpha_target_list = obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET];
	var cg_fade_speed_list = obj_control.var_map[? VAR_CG_LIST_FADE_SPEED];
	var cg_slide_speed_list = obj_control.var_map[? VAR_CG_LIST_SLIDE_SPEED];
	var cg_order_list = obj_control.var_map[? VAR_CG_LIST_ORDER];
	var cg_size_ratio_list = obj_control.var_map[? VAR_CG_LIST_SIZE_RATIO];
	
	if ds_list_find_index(cg_id_list,argument[0]) < 0
	{
		ds_list_add(cg_id_list,argument[0]);
		if sprite_exists(argument[1])
		{
			ds_list_add(cg_index_list,sprite_get_name(argument[1]));
			ds_list_add(cg_index_target_list,sprite_get_name(argument[1]));
			var cg = argument[1];
		}
		else
		{
			ds_list_add(cg_index_list,sprite_get_name(spr_bg_black_full));
			ds_list_add(cg_index_target_list,sprite_get_name(spr_bg_black_full));
			var cg = spr_bg_black_full;
		}
		
		var halign = argument[7];
		var valign = argument[8];
		var slide_from_halign = argument[9];
		var slide_from_valign = argument[10];
		
		if argument[11]
		{
			ds_map_add_unique(obj_control.var_map[? VAR_CG_HALIGN_MAP],sprite_get_name(cg),halign);
		}
		else
		{
			ds_map_add_unique(obj_control.var_map[? VAR_CG_HALIGN_MAP],sprite_get_name(cg),fa_none);
		}
		if argument[12]
		{
			ds_map_add_unique(obj_control.var_map[? VAR_CG_VALIGN_MAP],sprite_get_name(cg),valign);
		}
		else
		{
			ds_map_add_unique(obj_control.var_map[? VAR_CG_VALIGN_MAP],sprite_get_name(cg),fa_none);
		}
		
		if halign == fa_left
		{
			if valign = fa_top
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = 0;
				var yy = 0;
			}
			else if valign = fa_bottom
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = 0;
				var yy = room_height - sprite_get_height(cg)*raito;
			}
			else
			{
				var raito = room_height / sprite_get_height(cg);
				var xx = 0;
				var yy = 0;
			}
		}
		else if halign = fa_right
		{
			if valign = fa_top
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = room_width - sprite_get_width(cg)*raito;
				var yy = 0;
			}
			else if valign = fa_bottom
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = room_width - sprite_get_width(cg)*raito;
				var yy = room_height - sprite_get_height(cg)*raito;
			}
			else
			{
				var raito = room_height / sprite_get_height(cg);
				var xx = room_width - sprite_get_width(cg)*raito;
				var yy = 0;
			}
		}
		else
		{
			if valign = fa_top
			{
				var raito = room_width - sprite_get_width(cg)*raito;
				var xx = 0;
				var yy = 0;
			}
			else if valign = fa_bottom
			{
				var raito = room_width - sprite_get_width(cg)*raito;
				var xx = 0;
				var yy = room_height - sprite_get_height(cg)*raito;
			}
			else
			{
				var raito = max(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = room_width/2 - sprite_get_width(cg)*raito/2;
				var yy = room_height/2 - sprite_get_height(cg)*raito/2;
			}
		}
		
		if slide_from_halign == fa_left
			var xx_start = -sprite_get_width(cg)*raito;
		else if slide_from_halign == fa_right
			var xx_start = room_width;
		else
			var xx_start = xx;
			
		if slide_from_valign == fa_top
			var yy_start = -sprite_get_height(cg)*raito;
		else if slide_from_valign == fa_bottom
			var yy_start = room_height;
		else
			var yy_start = yy;

		ds_list_add(cg_x_list,xx_start);
		ds_list_add(cg_x_target_list,xx);
		ds_list_add(cg_y_list,yy_start);
		ds_list_add(cg_y_target_list,yy);
		ds_list_add(cg_alpha_list,argument[2]);
		ds_list_add(cg_alpha_target_list,argument[3]);
		ds_list_add(cg_fade_speed_list,argument[4]);
		ds_list_add(cg_slide_speed_list,argument[5]);
		ds_list_add(cg_order_list,argument[6]);
		ds_list_add(cg_size_ratio_list,raito);
	}
	else
	{
		scene_cg_list_change(argument[0],argument[1],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12]);
		
	}

}
