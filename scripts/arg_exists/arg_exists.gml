///arg_exists(arg);
/// @description

#macro ARG_NOT_EXISTS -9876

if !is_string(argument0) && is_real(argument0) && argument0 == ARG_NOT_EXISTS
	return false;
else
	return true;