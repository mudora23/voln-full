///ordering_list_in_battle_step();
/// @description
with obj_control
{
	var action_list = var_map_get(VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST);

	for(var i = 0; i < ds_list_size(action_list);i++)
	{
		var char = action_list[| i];
		char[? GAME_CHAR_X_ORDERING] = smooth_approach_room_speed(char[? GAME_CHAR_X_ORDERING],char[? GAME_CHAR_X_TARGET_ORDERING],SMOOTH_SLIDE_NORMAL_SEC);
		char[? GAME_CHAR_Y_ORDERING] = smooth_approach_room_speed(char[? GAME_CHAR_Y_ORDERING],char[? GAME_CHAR_Y_TARGET_ORDERING],SMOOTH_SLIDE_NORMAL_SEC);
		
		
		var spr_w = 100;
		var spr_h = 100;
		
		var bars_height = 40;
		
		var spr_hover = mouse_over_area(char[? GAME_CHAR_X_ORDERING],char[? GAME_CHAR_Y_ORDERING],spr_w,spr_h+bars_height);
		
		if char[? GAME_CHAR_OWNER] == OWNER_PLAYER draw_set_color(COLOR_ALLY_NAME);
		else if char[? GAME_CHAR_OWNER] == OWNER_ALLY draw_set_color(COLOR_ALLY_NAME);
		else draw_set_color(COLOR_ENEMY_NAME);
		
		if spr_hover || obj_control.hovered_char_on_screen == char draw_set_alpha(0.5);
		else draw_set_alpha(0.2);
		
		draw_rectangle_in_area(char[? GAME_CHAR_X_ORDERING],char[? GAME_CHAR_Y_ORDERING],char[? GAME_CHAR_X_ORDERING]+spr_w,char[? GAME_CHAR_Y_ORDERING]+spr_h,obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height);
		draw_sprite_in_area_stretch_to_fit_in_visible_area(asset_get_index(char[? GAME_CHAR_THUMB]),-1,char[? GAME_CHAR_X_ORDERING],char[? GAME_CHAR_Y_ORDERING],spr_w,spr_h,c_white,1,obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height);




		// only works when cutting from the right?

		var bar_width = spr_w;
		var bar_height = bars_height/2;
		var bar_xx=char[? GAME_CHAR_X_ORDERING];
		var bar_yy=char[? GAME_CHAR_Y_ORDERING]+spr_h;
		
		var bar_color = COLOR_HP;
		var bar_color_dark = COLOR_HP_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_get_health_max(char);
		var bar_value = char[? GAME_CHAR_HEALTH];
		var bar_value_visual = char[? GAME_CHAR_HEALTH_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		
		
		var original_area_x = bar_xx;
		var original_area_y = bar_yy;
		var original_area_width = bar_width;
		var original_area_height = bar_height;
		if original_area_x + original_area_width > obj_main.x + obj_main.area_width
			var new_area_width = max(0,obj_main.x + obj_main.area_width - original_area_x);
		else
			var new_area_width = original_area_width;
	    draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,original_area_x,original_area_y,new_area_width,original_area_height);
		
		var original_area_x = bar_xx;
		var original_area_y = bar_yy;
		var original_area_width = bar_width_per_value*min(bar_value,bar_value_visual);
		var original_area_height = bar_height;
		if original_area_x + original_area_width > obj_main.x + obj_main.area_width
			var new_area_width = max(0,obj_main.x + obj_main.area_width - original_area_x);
		else
			var new_area_width = original_area_width;
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,original_area_x,original_area_y,new_area_width,original_area_height);
	    
		
		var original_area_x = bar_xx+bar_width_per_value*min(bar_value,bar_value_visual);
		var original_area_y = bar_yy;
		var original_area_width = bar_width_per_value*abs(bar_value-bar_value_visual);
		var original_area_height = bar_height;
		if original_area_x + original_area_width > obj_main.x + obj_main.area_width
			var new_area_width = max(0,obj_main.x + obj_main.area_width - original_area_x);
		else
			var new_area_width = original_area_width;
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,original_area_x,original_area_y,new_area_width,original_area_height);


		var bar_xx=char[? GAME_CHAR_X_ORDERING];
		var bar_yy=char[? GAME_CHAR_Y_ORDERING]+spr_h+bar_height;
		
		var bar_color = COLOR_LUST;
		var bar_color_dark = COLOR_LUST_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_get_lust_max(char);
		var bar_value = char[? GAME_CHAR_LUST];
		var bar_value_visual = char[? GAME_CHAR_LUST_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;

		var original_area_x = bar_xx;
		var original_area_y = bar_yy;
		var original_area_width = bar_width;
		var original_area_height = bar_height;
		if original_area_x + original_area_width > obj_main.x + obj_main.area_width
			var new_area_width = max(0,obj_main.x + obj_main.area_width - original_area_x);
		else
			var new_area_width = original_area_width;
	    draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,original_area_x,original_area_y,new_area_width,original_area_height);
		
		var original_area_x = bar_xx;
		var original_area_y = bar_yy;
		var original_area_width = bar_width_per_value*min(bar_value,bar_value_visual);
		var original_area_height = bar_height;
		if original_area_x + original_area_width > obj_main.x + obj_main.area_width
			var new_area_width = max(0,obj_main.x + obj_main.area_width - original_area_x);
		else
			var new_area_width = original_area_width;
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,original_area_x,original_area_y,new_area_width,original_area_height);
	    
		
		var original_area_x = bar_xx+bar_width_per_value*min(bar_value,bar_value_visual);
		var original_area_y = bar_yy;
		var original_area_width = bar_width_per_value*abs(bar_value-bar_value_visual);
		var original_area_height = bar_height;
		if original_area_x + original_area_width > obj_main.x + obj_main.area_width
			var new_area_width = max(0,obj_main.x + obj_main.area_width - original_area_x);
		else
			var new_area_width = original_area_width;
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,original_area_x,original_area_y,new_area_width,original_area_height);


		// draw buff debuff icons
		draw_reset();
			
		var buff_list = char[? GAME_CHAR_BUFF_LIST];
		var buff_turn_list = char[? GAME_CHAR_BUFF_TURN_LIST];
		var debuff_list = char[? GAME_CHAR_DEBUFF_LIST];
		var debuff_turn_list = char[? GAME_CHAR_DEBUFF_TURN_LIST];
		var buff_debuff_icon_scaling = 0.6;
		var buff_debuff_icon_width = obj_control.buff_debuff_icon_sizeS*buff_debuff_icon_scaling;
		var buff_debuff_icon_height = obj_control.buff_debuff_icon_sizeS*buff_debuff_icon_scaling;
		var buff_debuff_icon_xx = char[? GAME_CHAR_X_ORDERING]+spr_w-buff_debuff_icon_width;
		var buff_debuff_icon_yy = char[? GAME_CHAR_Y_ORDERING];
		var buff_debuff_icon_padding = 10;
		
		for(var j = 0;j<ds_list_size(debuff_list);j++)
		{			
			var debuff = debuff_list[| j];
			//show_debug_message("debuff: "+string(debuff));
			var debuff_icon = short_term_buff_debuff_get_icon(debuff);
			//show_debug_message("debuff_icon: "+sprite_get_name(debuff_icon));
			draw_sprite_ext(debuff_icon,0,buff_debuff_icon_xx+sprite_get_xoffset(debuff_icon)*buff_debuff_icon_scaling,buff_debuff_icon_yy+sprite_get_yoffset(debuff_icon)*buff_debuff_icon_scaling,buff_debuff_icon_scaling,buff_debuff_icon_scaling,0,c_white,1);
			//draw_sprite_in_area(debuff_icon,buff_debuff_icon_xx+sprite_get_xoffset(debuff_icon),buff_debuff_icon_yy+sprite_get_yoffset(debuff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
			if debuff_turn_list[| j] > 1 draw_text_outlined(buff_debuff_icon_xx,buff_debuff_icon_yy,ceil(debuff_turn_list[| j]),c_silver,c_lightblack,0.4,1,3);
			if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(debuff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
			buff_debuff_icon_xx-=buff_debuff_icon_width+buff_debuff_icon_padding;
		}
		
		for(var j = 0;j<ds_list_size(buff_list);j++)
		{			
			var buff = buff_list[| j];
			var buff_icon = short_term_buff_debuff_get_icon(buff);
			draw_sprite_ext(buff_icon,0,buff_debuff_icon_xx+sprite_get_xoffset(buff_icon)*buff_debuff_icon_scaling,buff_debuff_icon_yy+sprite_get_yoffset(buff_icon)*buff_debuff_icon_scaling,buff_debuff_icon_scaling,buff_debuff_icon_scaling,0,c_white,1);
			//draw_sprite_in_area(buff_icon,buff_debuff_icon_xx+sprite_get_xoffset(buff_icon),buff_debuff_icon_yy+sprite_get_yoffset(buff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
			if buff_turn_list[| j] > 1 draw_text_outlined(buff_debuff_icon_xx,buff_debuff_icon_yy,ceil(buff_turn_list[| j]),c_silver,c_lightblack,0.4,1,3);
			if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(buff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
			buff_debuff_icon_xx-=buff_debuff_icon_width+buff_debuff_icon_padding;
		}	
			
			
		draw_reset();
		
		
		
		
		
		
		
		
		
		
		if mouse_over_area(obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height) &&
		   mouse_over_area(char[? GAME_CHAR_X_ORDERING],char[? GAME_CHAR_Y_ORDERING],spr_w,spr_h) && !menu_exists()
		{
			obj_control.hovered_char_on_screen_delay = 2;
			obj_control.hovered_char_on_screen = char;
		}

	}

}