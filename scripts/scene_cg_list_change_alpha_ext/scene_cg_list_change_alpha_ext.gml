///scene_cg_list_change_alpha_ext(ID,alpha_target,fade_speed);
/// @description  
/// @param ID
/// @param alpha_target
/// @param fade_speed

with obj_control
{
	var cg_alpha_target_list = obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET];
	var cg_fade_speed_list = obj_control.var_map[? VAR_CG_LIST_FADE_SPEED];
	var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	
	if ds_list_find_index(cg_id_list,argument[0]) >= 0
	{
		var existing_index = ds_list_find_index(cg_id_list,argument[0]);
		
		cg_alpha_target_list[| existing_index] = argument[1];
		cg_fade_speed_list[| existing_index] = argument[2];
	
	}

}