///floating_text_step();
/// @description
for(var i = 0; i < ds_list_size(obj_floating_text.floating_text_string);i++)
{
	if obj_floating_text.floating_text_delay[| i] > 0
		obj_floating_text.floating_text_delay[| i] -= (1/room_speed);
	else
	{
		obj_floating_text.floating_text_life[| i] -= (1/room_speed);
		obj_floating_text.floating_text_y[| i] -= 200*(1/room_speed);
		
		if obj_floating_text.floating_text_increasing[| i]
		{
			obj_floating_text.floating_text_alpha[| i] = approach(obj_floating_text.floating_text_alpha[| i], 0.8, FADE_FAST_SEC*(1/room_speed));
			
			if obj_floating_text.floating_text_life[| i] <= 1/FADE_FAST_SEC obj_floating_text.floating_text_increasing[| i] = false;
		}
		else
		{
			obj_floating_text.floating_text_alpha[| i] = approach(obj_floating_text.floating_text_alpha[| i], 0, FADE_FAST_SEC*(1/room_speed));
		}
	}
    
    // deleting
    if obj_floating_text.floating_text_alpha[| i] <= 0 && !obj_floating_text.floating_text_increasing[| i]
    {
        ds_list_delete(obj_floating_text.floating_text_string,i);
        ds_list_delete(obj_floating_text.floating_text_x,i);
        ds_list_delete(obj_floating_text.floating_text_y,i);
        ds_list_delete(obj_floating_text.floating_text_increasing,i);
        ds_list_delete(obj_floating_text.floating_text_alpha,i);
        ds_list_delete(obj_floating_text.floating_text_life,i);
        ds_list_delete(obj_floating_text.floating_text_sign,i);
		ds_list_delete(obj_floating_text.floating_text_color,i);
		ds_list_delete(obj_floating_text.floating_text_delay,i);
		ds_list_delete(obj_floating_text.floating_text_scale,i);
        i--;
    }
    else
    {
        draw_set_font(font_01b_reg);
        draw_set_valign(fa_center);
        draw_set_halign(fa_center);
        draw_set_alpha(obj_floating_text.floating_text_alpha[| i]); 

        /*if obj_floating_text.floating_text_sign[| i] == "+"
            var the_color = make_color_rgb(58, 130, 59);
        else if obj_floating_text.floating_text_sign[| i] == "-"
            var the_color = make_color_rgb(109, 55, 48);
        else
            var the_color = c_black;*/
			
			
			
			
        draw_text_outlined(obj_floating_text.floating_text_x[| i],obj_floating_text.floating_text_y[| i],obj_floating_text.floating_text_string[| i],obj_floating_text.floating_text_color[| i],c_lightblack,obj_floating_text.floating_text_scale[| i],5,20);
        //draw_text_transformed_color(obj_floating_text.floating_text_x[| i],obj_floating_text.floating_text_y[| i],obj_floating_text.floating_text_string[| i],obj_floating_text.floating_text_scale[| i],obj_floating_text.floating_text_scale[| i],0,obj_floating_text.floating_text_color[| i],obj_floating_text.floating_text_color[| i],obj_floating_text.floating_text_color[| i],obj_floating_text.floating_text_color[| i],obj_floating_text.floating_text_alpha[| i]);
	
		
		
		
		
		draw_reset();
    }
}