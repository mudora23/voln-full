///battle_char_list_get_average_level(char_list);
/// @description
/// @param char_list

var average_level = 0;
for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];
	average_level+=char[? GAME_CHAR_LEVEL]/ds_list_size(argument0);

}
return average_level;