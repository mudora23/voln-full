///postfx_ps_toruhit(x,y);
/// @description -
/// @param x
/// @param y


if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);
part_emitter_region(global.ps, global.toru_hit, xp-6, xp+10, yp-5, yp+11, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.toru_hit, global.toru_hit, 5);
part_emitter_region(global.ps, global.toru_hit2, xp-11, xp+5, yp-7, yp+9, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.toru_hit2, global.toru_hit2, 10);
part_emitter_region(global.ps, global.toru_hit3, xp-7, xp+9, yp-6, yp+10, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.toru_hit3, global.toru_hit3, 5);