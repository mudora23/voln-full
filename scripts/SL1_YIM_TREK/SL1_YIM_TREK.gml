///SL1_YIM_TREK();
/// @description Nirholt - YIM TREK

_label("start");

_label("SL1_YIM_TREK_INTRO");
	_dialog("SL1_YIM_TREK_INTRO",true);
	_jump("SL1_YIM_TREK_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_YIM_TREK_Choice");
	_choice("Concession area","SL1_YIM_TREK_CONCESSION_AREA");
	_choice("[?]Place a bet","SL1_YIM_TREK_BET");
	_choice("[?]Sit for a while","SL1_YIM_TREK_SIT");
	_choice("Leave","SL1_YIM_TREK_EXIT");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_YIM_TREK_RETURN");
	_dialog("SL1_YIM_TREK_RETURN",true);
	_jump("SL1_YIM_TREK_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_YIM_TREK_CONCESSION_AREA");
	_dialog("SL1_YIM_TREK_CONCESSION_AREA",true);
	_jump("SL1_YIM_TREK_CONCESSION_AREA_Menu");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_YIM_TREK_CONCESSION_AREA_Menu");
	// items	
	_choice("Buy","SL1_YIM_TREK_STORE");
	_choice("Exit","SL1_YIM_TREK_Choice");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_YIM_TREK_STORE");
	_store_add_item(ITEM_PIE_LEMMUS);
	_store_add_item(ITEM_CANDY_JUDDOVIN);
	_var_map_set(VAR_EXTRA_AFTER_SCENE,"SL1_YIM_TREK");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"SL1_YIM_TREK_STORE");
	_var_map_set(VAR_EXTRA2_AFTER_SCENE,"SL1_YIM_TREK");
	_var_map_set(VAR_EXTRA2_AFTER_SCENE_LABEL,"SL1_YIM_TREK_Choice");
	_jump("start","CS_buying");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////	

_label("SL1_YIM_TREK_BET");
	_dialog("SL1_YIM_TREK_BET",true);
	_jump("SL1_YIM_TREK_BET_Menu");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_YIM_TREK_BET_Menu");
	// items
	_choice("Exit","SL1_YIM_TREK_Choice");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_YIM_TREK_EXIT");
	_dialog("SL1_YIM_TREK_EXIT");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////