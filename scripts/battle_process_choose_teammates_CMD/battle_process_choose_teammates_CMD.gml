///battle_process_choose_teammates_CMD(CMD,extra_arg[optional]);
/// @description
/// @param CMD
with obj_control
{
	scene_choices_list_clear();
	obj_control.allies_choice_enable = true;
	
	var char_map = battle_get_current_action_char();
	var char_CMD = script_get_index_ext(argument[0]);
	
	
		var char_list = var_map[? VAR_ALLY_LIST];
		
		if argument_count > 1
			scene_choiceCMD_add(char_map[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char_map),argument[1]);
		else
			scene_choiceCMD_add(char_map[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char_map));

		for(var i = 0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if battle_char_get_combat_capable(char)
			{
				if argument_count > 1
					scene_choiceCMD_add(char[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char),argument[1]);
				else
					scene_choiceCMD_add(char[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char));
			}
		}
		scene_choiceCMD_add("Cancel",battle_process_player);
	
	
	
	/*
	
	if char_map[? GAME_CHAR_OWNER] == OWNER_PLAYER && char_map[? GAME_CHAR_AI] == AI_NONE
	{
		
		var char_list = var_map[? VAR_ALLY_LIST];
		
		if argument_count > 1
			scene_choiceCMD_add(char_map[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char_map),argument[1]);
		else
			scene_choiceCMD_add(char_map[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char_map));

		for(var i = 0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if battle_char_get_combat_capable(char)
			{
				if argument_count > 1
					scene_choiceCMD_add(char[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char),argument[1]);
				else
					scene_choiceCMD_add(char[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(char));
			}
		}
		scene_choiceCMD_add("Cancel",battle_process_player);

	}

	else if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
	{
		var char_list = var_map[? VAR_ENEMY_LIST];
		
		if char_CMD == "battle_process_flirt_atk_CMD"
			var the_idnex = battle_char_list_get_highest_lust_capable_char_i(char_list);
		else
			var the_idnex = battle_char_list_get_lowest_hp_capable_char_i(char_list);
	
		if the_idnex >= 0 script_execute(char_CMD,the_idnex);
		else battle_process_wait_CMD();
		
	}
	else
	{
		var char_list = ds_list_create();
		for(var i = 0; i < ds_list_size(var_map[? VAR_ALLY_LIST]);i++)
			ds_list_add(char_list,ds_list_find_value(var_map[? VAR_ALLY_LIST],i));
		ds_list_add(char_list,var_map_get(VAR_PLAYER));

		if char_CMD == "battle_process_flirt_atk_CMD"
			var the_idnex = battle_char_list_get_highest_lust_capable_char_i(char_list);
		else
			var the_idnex = battle_char_list_get_lowest_hp_capable_char_i(char_list);
	
		if the_idnex >= 0 script_execute(char_CMD,the_idnex);
		else battle_process_wait_CMD();
		
		ds_list_destroy(char_list);
	}*/
}