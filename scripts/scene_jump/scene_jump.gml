///scene_jump(label/posi,scr);
/// @description
/// @param label/posi
/// @param scr

if !is_string(argument[0])
	var_map_set(VAR_NEXT_SCRIPT_POSI_OR_LABEL,argument[0]);
else if is_string(argument[0]) && argument[0] != ""
	var_map_set(VAR_NEXT_SCRIPT_POSI_OR_LABEL,string_upper(argument[0]));

if argument_count > 1
{
	if is_string(argument[1]) && asset_get_type(argument[1]) == asset_script
		var_map_set(VAR_NEXT_SCRIPT,argument[1]);
	else if !is_string(argument[1]) && script_exists(argument[1])
		var_map_set(VAR_NEXT_SCRIPT,script_get_name(argument[1]));
}