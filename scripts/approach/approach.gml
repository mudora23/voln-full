///approach(current, target, amount)
/// @description - Move a value towards a target value by a given amount.
/// @param current
/// @param target
/// @param amount

if (argument0 < argument1)
{
    return min(argument0+argument2, argument1); 
}
else
{
    return max(argument0-argument2, argument1);
}


