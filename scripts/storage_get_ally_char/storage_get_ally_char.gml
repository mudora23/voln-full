///storage_get_ally_char(i);
/// @description
/// @param i
var storage_team = var_map_get(VAR_STORAGE_LIST);
var storage_size = ds_list_size(storage_team);
var i = 0;
for(var ii =0; i < storage_size;ii++)
{
	var char = storage_team[| ii];
	if char[? GAME_CHAR_TYPE] == GAME_CHAR_ALLY 
	{
		if i == argument0 return char;
		i++;
	}

}
return noone;






