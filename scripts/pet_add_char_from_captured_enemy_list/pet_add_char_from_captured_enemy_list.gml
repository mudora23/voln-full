///pet_add_char_from_captured_enemy_list();
/// @description
if !pet_is_full()
{
	var pet_list = obj_control.var_map[? VAR_PET_LIST];
	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	
	for(var i = 0 ; i < ds_list_size(enemy_list);i++)
	{
		var enemy = enemy_list[| i];
		if enemy[? GAME_CHAR_CAPTURE]
		{

			//var new_char = ds_map_create();
			//ds_map_copy(new_char,enemy);
	        //ds_list_add(pet_list,new_char);
			
			//ds_list_mark_as_map(pet_list,ds_list_size(pet_list)-1);
			
			var new_char = pet_add_char(enemy[? GAME_CHAR_NAME],false,enemy[? GAME_CHAR_LEVEL]);
			new_char[? GAME_CHAR_IMPREGNATE] = enemy[? GAME_CHAR_IMPREGNATE];
			new_char[? GAME_CHAR_IMPREGNATE_BY_CHARID] = enemy[? GAME_CHAR_IMPREGNATE_BY_CHARID];
			new_char[? GAME_CHAR_GENDER] = enemy[? GAME_CHAR_GENDER];
		}
	}
}
else
{
	var pet_list = obj_control.var_map[? VAR_PET_LIST];
	var storage_list = obj_control.var_map[? VAR_STORAGE_LIST];
	
	for(var i = 0 ; i < ds_list_size(enemy_list);i++)
	{
		var enemy = enemy_list[| i];
		if enemy[? GAME_CHAR_CAPTURE]
		{
			//var new_char = ds_map_create();
			//ds_map_copy(new_char,enemy);
	        //ds_list_add(storage_list,new_char);

			//ds_list_mark_as_map(storage_list,ds_list_size(storage_list)-1);
			
			var new_char = storage_add_char(enemy[? GAME_CHAR_NAME],false,enemy[? GAME_CHAR_LEVEL]);
			new_char[? GAME_CHAR_IMPREGNATE] = enemy[? GAME_CHAR_IMPREGNATE];
			new_char[? GAME_CHAR_IMPREGNATE_BY_CHARID] = enemy[? GAME_CHAR_IMPREGNATE_BY_CHARID];
			new_char[? GAME_CHAR_GENDER] = enemy[? GAME_CHAR_GENDER];
		}
	}	
}