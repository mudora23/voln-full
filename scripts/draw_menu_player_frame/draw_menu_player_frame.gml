///draw_menu_ally_frame(x1,y1,x2,y2,ally_map);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param ally_map

/*
    menu_open_bg_image_alpha
    menu_open_text_image_alpha
    menu_idle_bg_colorscheme = c_lightblack;
    menu_idle_text_colorscheme = c_gray;
    menu_selected_bg_colorscheme = c_gray;
    menu_selected_text_colorscheme = c_lightblack;
*/

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var ally = argument4;
var w = x2-x1;
var h = y2-y1;

// draw bg
//draw_set_font(font_general_20);
draw_set_alpha(menu_open_bg_image_alpha);
draw_set_color(colorscheme_almostblack);
//draw_rectangle(x1,y1,x2,y2, false);
draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,w,h,draw_get_color(),draw_get_alpha());
draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,w,h,draw_get_color(),draw_get_alpha());

// draw sprite
var padding = 10;
if sprite_exists(asset_get_index(ally[? GAME_CHAR_THUMB]))
	draw_thumb_in_area(asset_get_index(ally[? GAME_CHAR_THUMB]), x1+padding, y1+padding, w-padding*2, w-padding*2,c_white,ally);

// details
var xx = x1+15;
var yy = y1+w-padding;
draw_set_alpha(menu_open_bg_image_alpha);
draw_set_color(c_ltgray);
draw_set_valign(fa_top);
draw_text_transformed(xx,yy,ally[? GAME_CHAR_DISPLAYNAME]+" (LV "+string(round(ally[? GAME_CHAR_LEVEL]))+")",0.5,0.5,0);
yy+=25;






var bars_hover = mouse_over_area(x1+padding,yy,w - padding*2,120);
//draw_set_font(font_general_15);
var bar_xx = x1+padding;
var bar_yy = yy;
var bar_width = w - padding*2;
var bar_height = 20;
var bar_value = ally[? GAME_CHAR_VITALITY];
var bar_color = COLOR_VITALITY;
var bar_width_per_value = bar_width / char_get_vitality_max(ally);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(COLOR_VITALITY);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Vitality: "+string(round(bar_value))+"/"+string(round(char_get_vitality_max(ally))),0.5,0.5,0);
}
yy+=25;


var bar_xx = x1+padding;
var bar_yy = yy;
var bar_width = w - padding*2;
var bar_height = 20;
var bar_value = ally[? GAME_CHAR_HEALTH];
var bar_color = COLOR_HP;
var bar_width_per_value = bar_width / char_get_health_max(ally);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(COLOR_HP);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Health: "+string(round(bar_value))+"/"+string(round(char_get_health_max(ally))),0.5,0.5,0);
}
yy+=25;

var bar_yy = yy;
var bar_value = ally[? GAME_CHAR_LUST];
var bar_color = COLOR_LUST;
var bar_width_per_value = bar_width / char_get_lust_max(ally);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(COLOR_LUST);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Lust: "+string(round(bar_value))+"/"+string(round(char_get_lust_max(ally))),0.5,0.5,0);
}
yy+=25;

/*var bar_yy = yy;
var bar_value = ally[? GAME_CHAR_STAMINA];
var bar_color = COLOR_STAMINA;
var bar_width_per_value = bar_width / char_get_stamina_max(ally);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
yy+=25;*/

var bar_yy = yy;
var bar_value = ally[? GAME_CHAR_TORU];
var bar_color = COLOR_TORU;
var bar_width_per_value = bar_width / char_get_toru_max(ally);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(COLOR_TORU);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Toru: "+string(round(bar_value))+"/"+string(round(char_get_toru_max(ally))),0.5,0.5,0);
}
yy+=25;

var bar_yy = yy;
var bar_value = ally[? GAME_CHAR_EXP];
var bar_color = COLOR_EXP;
var bar_width_per_value = bar_width / char_get_exp_max(ally);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,menu_open_text_image_alpha,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
if bars_hover
{
	draw_set_color(c_lightblack);
	draw_set_alpha(0.9);
	draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
	draw_set_color(COLOR_EXP);
	draw_set_alpha(1);
	draw_text_transformed(bar_xx,bar_yy,"Exp: "+string(round(bar_value))+"/"+string(round(char_get_exp_max(ally))),0.5,0.5,0);
}
yy+=25;



draw_stats_polygon(x1+w/2,yy+90,60,char_get_END(ally),char_get_FOU(ally),char_get_STR(ally),char_get_DEX(ally),char_get_WIS(ally),char_get_INT(ally),char_get_SPI(ally),true,true);







