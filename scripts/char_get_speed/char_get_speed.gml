///char_get_speed(char_map);
/// @description
/// @param char_map

var number = char_get_DEX(argument0);
if battle_char_get_buff_level(argument0,BUFF_SPEED_UP) == 1 number *= 2;
else if battle_char_get_buff_level(argument0,BUFF_SPEED_UP) == 2 number *= 3;
else if battle_char_get_buff_level(argument0,BUFF_SPEED_UP) >= 3 number *= 4;

if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;
//if char_get_skill_exists(argument0,SKILL_I_AM_THE_MAIN_CHARACTER) number+=10;
return number;