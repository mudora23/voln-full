///isset_default(value,default)

gml_pragma("forceinline");

if (isset(argument0))
    return argument0;
else
    return argument1;

