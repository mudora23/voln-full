///_dialog(text,auto_continue?[optional],with_tags[optional]);
/// @descirption
/// @param text
/// @param auto_continue?[optional]
/// @param with_tags[optional])
if scene_is_current()
{
	if var_map_get(VAR_SCRIPT_PROC_NUMBER) == 0
	{
		var_map_set(VAR_SCRIPT_DIALOG_NUMBER,0);
		var_map_add(VAR_SCRIPT_PROC_NUMBER,1);
		var_map_set(VAR_SCRIPT_SHOW_CHOICES,0);
		
		var_map_set(VAR_SCRIPT_DIALOG_SPEAKER,"");
		
		if argument_count > 2 && argument[2]
			var content = argument[0];
		else
			var content = story_chunk_load(argument[0]);
		
		var raw_marked_string_list = story_execute(content);

		var_map_set(VAR_SCRIPT_DIALOG_RAW,raw_marked_string_list[| 0]);
		var_map_set(VAR_SCRIPT_DIALOG,raw_marked_string_list[| 1]);
		var_map_set(VAR_SCRIPT_DIALOG_SPEAKER,raw_marked_string_list[| 2]);
		
		if os_type == os_windows && obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD] && var_map_get(VAR_SCRIPT_DIALOG_RAW) != "" && var_map_get(VAR_SCRIPT_DIALOG_RAW) != clipboard_get_text()
			clipboard_set_text(var_map_get(VAR_SCRIPT_DIALOG_RAW));
		
		// DEBUG - chunk name
		if obj_control.debug_enable
		{
			if argument_count > 2 && argument[2] obj_control.debug_chunk_name = raw_marked_string_list[| 0];
			else obj_control.debug_chunk_name = argument[0];
		}
		
		ds_list_destroy(raw_marked_string_list);
		
		chunk_mark_set(argument[0],true);

	}
	
	
	//draw_set_halign(fa_right);
	//draw_set_valign(fa_bottom);
	//draw_set_font(font_01s_reg);
	//draw_set_color(c_white);
	//draw_text(obj_main.x+obj_main.area_width,obj_main.y-20,argument[0]);
	//draw_text(500,500,argument[0]);
	//draw_reset();
	
	// jump
	if !menu_exists() && !obj_main.holding_scroll && !obj_main.mouse_on_clipboard
	{
		var next = false;
		
		if var_map_get(VAR_SCRIPT_DIALOG_NUMBER) >= string_length(var_map_get(VAR_SCRIPT_DIALOG_RAW))
		{
			if (action_button_mouse_pressed_get() || action_button_mouse_hold_get() >= 0.2) && (mouse_over_area(x,y,area_width,area_height) || mouse_over_area(obj_command_panel.x,obj_command_panel.y,obj_command_panel.area_width,obj_command_panel.area_height))
				next = true;
			else if (action_button_keyboard_pressed_get() || action_button_keyboard_hold_get() >= 0.2)
				next = true;
			else if (argument_count > 1 && argument[1])
				next = true;
		}
		else if var_map_get(VAR_SCRIPT_PROC_STEP)/room_speed >= 0.5
		{
			if (action_button_mouse_pressed_get() || action_button_mouse_hold_get() >= 0.2) && (mouse_over_area(x,y,area_width,area_height) || mouse_over_area(obj_command_panel.x,obj_command_panel.y,obj_command_panel.area_width,obj_command_panel.area_height))
				next = true;
			else if (action_button_keyboard_pressed_get() || action_button_keyboard_hold_get() >= 0.2)
				next = true;
		}
			 
		if next
		{
			scene_jump_next();
			action_button_pressed_set(false);
			action_button_hold_set(0);
			var_map_set(VAR_SCRIPT_SHOW_CHOICES,1);
		
		}
	}
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);
