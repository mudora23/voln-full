///char_level_up_until_sync_with_player(char);
/// @description
/// @param char

var player_level = char_get_info(obj_control.var_map[? VAR_PLAYER],GAME_CHAR_LEVEL);
var char_level = char_get_info(argument0,GAME_CHAR_LEVEL);

if player_level > char_level

char_level_up(argument0,player_level - char_level);