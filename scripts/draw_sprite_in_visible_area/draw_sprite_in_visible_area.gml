///draw_sprite_in_visible_area(spr,img,x,y,xscale,yscale,color,alpha,area_x,area_y,area_w,area_h);
/// @description
/// @param spr
/// @param img
/// @param x
/// @param y
/// @param xscale
/// @param yscale
/// @param color
/// @param alpha
/// @param area_x
/// @param area_y
/// @param area_w
/// @param area_h

if rectangle_in_rectangle(argument2, argument3, argument2+sprite_get_width(argument0)*argument4, argument3+sprite_get_height(argument0)*argument5, argument8, argument9, argument8+argument10, argument9+argument11)
{
    var draw_left = clamp((argument8 - argument2)/argument4, 0,sprite_get_width(argument0));
    var draw_top = clamp((argument9 - argument3)/argument5, 0,sprite_get_height(argument0));
    var draw_width = min(sprite_get_width(argument0) - draw_left,max((argument8+argument10-argument2)/argument4,0),argument10/argument4);
    var draw_height = min(sprite_get_height(argument0) - draw_top,max((argument9+argument11-argument3)/argument5,0),argument11/argument5);
    var draw_xx = max(argument2,argument8);
    var draw_yy = max(argument3,argument9);
	
    draw_sprite_part_ext(argument0,argument1,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,argument4,argument5,argument6,argument7);
}

