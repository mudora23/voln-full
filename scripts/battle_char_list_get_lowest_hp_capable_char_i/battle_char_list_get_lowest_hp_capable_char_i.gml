///battle_char_list_get_lowest_hp_capable_char_i(char_list);
/// @description
/// @param char_list

var current_char_index = noone;
var current_real_ratio = 0;


for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];

	if battle_char_get_combat_capable(char)
	{
		if current_char_index == noone || battle_char_get_effective_hp_ratio(char) < current_real_ratio
		{
			current_char_index = i;
			current_real_ratio = battle_char_get_effective_hp_ratio(char);
		}

	}
}

return current_char_index;