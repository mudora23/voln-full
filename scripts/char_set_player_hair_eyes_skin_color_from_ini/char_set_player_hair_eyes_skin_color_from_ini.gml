///char_set_player_hair_eyes_skin_color_from_ini();
/// @description

ini_open(working_directory+"col.ini");

var player = char_get_player();
player[? GAME_CHAR_COLOR_HAIR] = ini_read_real(PC_Hair_Color_N, string(var_map_get(PC_Hair_Color_N)), 16777215);
player[? GAME_CHAR_COLOR_EYE] = ini_read_real(PC_Eye_Color_N, string(var_map_get(PC_Eye_Color_N)), 16777215);
player[? GAME_CHAR_COLOR_SKIN] = ini_read_real(PC_Skin_Color_N, string(var_map_get(PC_Skin_Color_N)), 16777215);

ini_close();