///array_duplicate(array)
var a, b = argument0, s = array_length_1d(b); 
a = array_create(s);
array_copy(a, 0, b, 0, s);
return a;
