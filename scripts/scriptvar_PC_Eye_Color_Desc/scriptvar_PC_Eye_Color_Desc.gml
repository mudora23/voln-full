///scriptvar_PC_Eye_Color_Desc();
/// @description
var num = var_map_get(PC_Eye_Color_N);
if num == 0 return "grey";
else if num == 1 return "ice-blue";
else if num == 2 return "blue";
else if num == 3 return "green";
else if num == 4 return "brown";
else if num == 5 return choose("black","void-like","inky");
else if num == 6 return choose("red","sanguine");
else if num == 7 return "pink";
else if num == 8 return choose("red-purple","hot purple");
else if num == 9 return choose("cool purple","indigo");
else if num == 10 return "yellow";
else if num == 11 return choose("tawny","beige");
else return "orange";