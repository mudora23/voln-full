///battle_char_buff_apply(char,buff,level,turns,source_char_id[optional]);
/// @description
/// @param char
/// @param buff
/// @param level
/// @param turns
/// @param source_char_id[optional]

var char_map = argument[0];
var buff = argument[1];
var level = argument[2];
var turns = argument[3];
if argument_count > 4 var source_char_id = argument[4];
else var source_char_id = noone;

var buff_index = ds_list_find_index(char_map[? GAME_CHAR_BUFF_LIST],buff);
if buff_index < 0
{
	ds_list_add(char_map[? GAME_CHAR_BUFF_LIST],buff);
	ds_list_add(char_map[? GAME_CHAR_BUFF_TURN_LIST],turns);
	ds_list_add(char_map[? GAME_CHAR_BUFF_LEVEL_LIST],level);
	ds_list_add(char_map[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST],source_char_id);
}
else if ds_list_find_value(char_map[? GAME_CHAR_BUFF_LEVEL_LIST],buff_index) < level
{
	ds_list_replace(char_map[? GAME_CHAR_BUFF_TURN_LIST],buff_index,turns);
	ds_list_replace(char_map[? GAME_CHAR_BUFF_LEVEL_LIST],buff_index,level);
	ds_list_replace(char_map[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST],buff_index,source_char_id);
	
}
else if ds_list_find_value(char_map[? GAME_CHAR_BUFF_LEVEL_LIST],buff_index) == level && ds_list_find_value(char_map[? GAME_CHAR_BUFF_TURN_LIST],buff_index) < turns
{
	ds_list_replace(char_map[? GAME_CHAR_BUFF_TURN_LIST],buff_index,turns);
	ds_list_replace(char_map[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST],buff_index,source_char_id);
}