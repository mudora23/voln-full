///postfx_ps_onhit(x,y);
/// @description -
/// @param x
/// @param y

if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);
part_emitter_region(global.ps, global.ps_occult, xp, xp, yp, yp, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_occult, global.ps_occult, 1);