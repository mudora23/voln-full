///ordering_list_in_battle_set_position(char,index,offscreen);
/// @description
/// @param char
/// @param index
/// @param offscreen

var char = argument[0];
if char != noone
{
	if argument[1] < 0
	{
		char[? GAME_CHAR_X_TARGET_ORDERING] = room_width;
		char[? GAME_CHAR_Y_TARGET_ORDERING] = obj_main.y + obj_main.area_height - 190;
	}
	else
	{
		char[? GAME_CHAR_X_TARGET_ORDERING] = obj_main.x + 50 + argument[1] * 120;
		char[? GAME_CHAR_Y_TARGET_ORDERING] = obj_main.y + obj_main.area_height - 190;
	}
	if argument[2]
	{
		char[? GAME_CHAR_X_ORDERING] = room_width;
		char[? GAME_CHAR_Y_ORDERING] = char[? GAME_CHAR_Y_TARGET_ORDERING];
	}
}
