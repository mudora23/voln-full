///char_set_random_clothes_colors_from_ini(char_map,key1,key2,key3,key4,ini_section);
/// @description
/// @param char_map
/// @param key1
/// @param key2
/// @param key3
/// @param key4
/// @param ini_section


ini_open(working_directory+"col.ini");
var num_of_sets = ini_read_real(argument5, "total", 0);
do
{
	var num = irandom_range(1, num_of_sets);
	var four_colors = ini_read_string(argument5, string(num), "16777215-16777215-16777215-16777215");
}
until num_of_sets <= 1 || four_colors != string(argument0[? argument1])+"-"+string(argument0[? argument2])+"-"+string(argument0[? argument3])+"-"+string(argument0[? argument4])

var char = 1;
var key1 = "";
while string_char_at(four_colors,char) != "-"
{
	key1 += string_char_at(four_colors,char);
	char++;
}
argument0[? argument1] = real(key1);
char++;
var key2 = "";
while string_char_at(four_colors,char) != "-"
{
	key2 += string_char_at(four_colors,char);
	char++;
}
argument0[? argument2] = real(key2);
char++;
var key3 = "";
while string_char_at(four_colors,char) != "-"
{
	key3 += string_char_at(four_colors,char);
	char++;
}
argument0[? argument3] = real(key3);
char++;
var key4 = "";
while string_length(four_colors) >= char
{
	key4 += string_char_at(four_colors,char);
	char++;
}
argument0[? argument4] = real(key4);

ini_close();