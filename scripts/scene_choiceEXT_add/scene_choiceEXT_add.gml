///scene_choiceEXT_add(name,label,scene,extrascript,extrascript_arg0...extrascript_arg6);
/// @description
/// @param name
/// @param label
/// @param scene
/// @param extrascript
/// @param extrascript_arg0...extrascript_arg6

if argument_count > 8
	scene_choice_add(argument[0],argument[1],argument[2],"",true,true,true,"",argument[3],argument[4],argument[5],argument[6],argument[7],argument[8]);
else if argument_count > 7
	scene_choice_add(argument[0],argument[1],argument[2],"",true,true,true,"",argument[3],argument[4],argument[5],argument[6],argument[7]);
else if argument_count > 6
	scene_choice_add(argument[0],argument[1],argument[2],"",true,true,true,"",argument[3],argument[4],argument[5],argument[6]);
else if argument_count > 5
	scene_choice_add(argument[0],argument[1],argument[2],"",true,true,true,"",argument[3],argument[4],argument[5]);
else if argument_count > 4
	scene_choice_add(argument[0],argument[1],argument[2],"",true,true,true,"",argument[3],argument[4]);
else if argument_count > 3
	scene_choice_add(argument[0],argument[1],argument[2],"",true,true,true,"",argument[3]);
else if argument_count > 2
	scene_choice_add(argument[0],argument[1],argument[2]);
else if argument_count > 1
	scene_choice_add(argument[0],argument[1]);