///S_0002();
/// @description
	
_label("start");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Dungeon_01_Blue,FADE_NORMAL_SEC,1.5);
	_dialog("S_0002_1");
	_dialog("S_0002_2");
	_execute(0,postfx_punch_in,room_width/2,100,0.03);
	_show("ku",spr_char_Ku_n_n,5,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0002_3",true);
	_choice("“Who are you?”","S_0002_4_1A");
	_choice("“Why are you here?”","S_0002_5_1B");
	_choice("“Such a pretty gurl shouldn’t be wandering the Leg alone.”","S_0002_6_1C");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0002_4_1A");
	_change("ku",spr_char_Ku_n_n);
	_dialog("S_0002_4_1A",true);
	_jump("S_0002_Choice2");
	
_label("S_0002_5_1B");
	_change("ku",spr_char_Ku_n_n);
	_dialog("S_0002_5_1B",true);
	_jump("S_0002_Choice2");
	
_label("S_0002_6_1C");
	_change("ku",spr_char_Ku_n_ss);
	_dialog("S_0002_6_1C",true);
	_jump("S_0002_Choice2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0002_Choice2");
	_choice("“How did you know my name?”","S_0002_7_2A","","S_0002_7_2A");
	_choice("“Why should I go with you?”","S_0002_9_2B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0002_7_2A");
	_dialog("S_0002_7_2A");
	_dialog("S_0002_8_2A",true);
	_jump("S_0002_Choice2");
	
_label("S_0002_9_2B");
	_dialog("S_0002_9_2B");
	_dialog("S_0002_10");
	_movex("ku",XNUM_LEFT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0002_11");
	_dialog("S_0002_12");
	_dialog("S_0002_13");
	_dialog("S_0002_14");
	_dialog("S_0002_15");
	_dialog("S_0002_16");
	_dialog("S_0002_17");
	_dialog("S_0002_18");
	_dialog("S_0002_19",true);
	_jump("S_0002_Choice3");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0002_Choice3");
	_choice("“Where are we going?”","S_0002_20_3A","","S_0002_20_3A");
	_choice("“Tell me more about your friend.”","S_0002_22_3B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0002_20_3A");
	_dialog("S_0002_20_3A");
	_dialog("S_0002_21_3A",true);
	_jump("S_0002_Choice3");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0002_22_3B");
	_dialog("S_0002_22_3B");
	_dialog("S_0002_23");
	_dialog("S_0002_24");
	_dialog("S_0002_25");
	_dialog("S_0002_26");
	_dialog("S_0002_27");
	_dialog("S_0002_28");
	_dialog("S_0002_29");
	_dialog("S_0002_30");
	_hide("ku",FADE_NORMAL_SEC);
	_dialog("S_0002_31");
	_dialog("S_0002_32");
	_dialog("S_0002_33");
	_hide("zeyd",FADE_NORMAL_SEC);
	_dialog("S_0002_33-1");
	_dialog("S_0002_34");
	_dialog("S_0002_35");
	_jump("",S_0003);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////