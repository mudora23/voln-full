///battlelog_add_unique(dialog_with_tag);
/// @description - adding a new piece of battlelog to the queue.
/// @param battlelog_with_tag
if var_map_get(VAR_BATTLELOG_ACTIVE)
	with obj_battlelog
	{
		ds_list_add_unique(battlelog_queue_list,argument0);
		ds_list_add_unique(var_map_get(VAR_BATTLELOG_COPY_LIST),argument0);
	}