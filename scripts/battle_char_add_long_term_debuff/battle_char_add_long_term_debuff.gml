///battle_char_add_long_term_debuff(char,debuff,hours);
/// @description
/// @param char
/// @param debuff
/// @param hours
var char_map = argument0;
var char_debuff_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_LIST];
var char_debuff_hours_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_HOURS_LIST];
var char_debuff = argument1;
var char_debuff_hours = argument2;

var the_index = ds_list_find_index(char_map[? GAME_CHAR_LONG_TERM_DEBUFF_LIST],char_debuff);
if the_index >= 0
{
	char_debuff_hours_list[| the_index] = max(char_debuff_hours_list[| the_index],char_debuff_hours);
}
else
{
	ds_list_add(char_debuff_list,char_debuff);
	ds_list_add(char_debuff_hours_list,char_debuff_hours);
}

