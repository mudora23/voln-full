///draw_text_spacing(x,y,text,spacing);
/// @description
/// @param x
/// @param y
/// @param text
/// @param spacing
var xx = argument0;
var yy = argument1;
for(var i = 1; i <= string_length(argument2);i++)
{
	var char = string_char_at(argument2,i);
	draw_text(xx,yy,char);
	if char == "\r"
	{
		xx = argument0;
		yy += string_height(char);
	}
	else
	{
		xx += string_width(char);
		xx += argument3;
	}
}

