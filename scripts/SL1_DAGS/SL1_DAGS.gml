///SL1_DAGS();
/// @description Nirholt - DAGS

_label("start");

_label("SL1_DAGS_INTRO");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("SL1_DAGS_INTRO");
	_jump("SL1_DAGS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_RETURN");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("SL1_DAGS_RETURN");
	_jump("SL1_DAGS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Nirholt_Dags_01_crowd,FADE_NORMAL_SEC,1.5);
	if chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_B_16") && !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_B_16_B")
		_choice("Head upstairs","S_0004_FELGOR_INTRO_EVENT_B_16_R",S_0004);
	else
		_void();
		
	if chunk_mark_get("S_0004_FELGOR_INTRO_EVENT") && !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_B")
		_choice("Head upstairs","S_0004_FELGOR_INTRO_EVENT",S_0004);
	else
		_void();
	
	if qj_exists("S_0007_QJ")
		_choice("Scan for interesting characters","SL1_DAGS_SCAN_Bandit_Beatdown");
	else
		_choice("[?]Scan for interesting characters","SL1_DAGS_SCAN");
	
	_choice("Exit","SL1_DAGS_EXIT");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_SCAN");
	// items
	_choice("Exit","SL1_DAGS_SCAN_EXIT");	
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_SCAN_Bandit_Beatdown");
	_show("viktur",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_2");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_3");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_4");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_5",true);
	_jump("SL1_DAGS_SCAN_Bandit_Beatdown_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_SCAN_Bandit_Beatdown_Choice");
	_choice("Accept","SL1_DAGS_SCAN_Bandit_Beatdown_Accept");	
	_choice("Reject","SL1_DAGS_SCAN_Bandit_Beatdown_Reject");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_SCAN_Bandit_Beatdown_Reject");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_Reject",true);
	_jump("SL1_DAGS_SCAN_Bandit_Beatdown_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_SCAN_Bandit_Beatdown_Accept");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_Accept");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_Accept_2");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_Accept_3");
	_dialog("SL1_DAGS_SCAN_Bandit_Beatdown_Accept_4",true);
	_qj_remove("S_0007_QJ");
	_qj_add("S_0007_Accept_QJ");
	_jump("SL1_DAGS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_SCAN_EXIT");
	_dialog("SL1_DAGS_SCAN_EXIT");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_ITEM");
	_choice("Exit","SL1_DAGS_ITEM_EXIT");	
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_ITEM_EXIT");
	_dialog("SL1_DAGS_ITEM_EXIT");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_DAGS_EXIT");
	_play_sfx(sfx_Wooden_Door_Close_1);
	_dialog("SL1_DAGS_EXIT");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////




