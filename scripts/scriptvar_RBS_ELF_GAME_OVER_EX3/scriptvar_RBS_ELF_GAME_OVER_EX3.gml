///scriptvar_RBS_ELF_GAME_OVER_EX3();
/// @description
if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];
	
	if ally[? GAME_CHAR_GENDER] == GENDER_MALE
		var ally_his_her = "his";
	else if ally[? GAME_CHAR_GENDER] == GENDER_FEMALE
		var ally_his_her = "her";
	else
		var ally_his_her = "it";

	var returning_string =  "You can see similar effects in your companions.  "+string(ally_his_her)+"’s thicker and almost as antsy as you.";
	
	if ally_his_her == "her"
		returning_string += "  It seems likely that some of "+string(ally_name)+"’s growth is due to pregnancy.";
		
	return returning_string;
}
else if ally_get_number() > 1
{

	var returning_string = "You can see similar effects in your companions.  They’re thicker and almost as antsy as you.";
	
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var female_count = 0;
	var ally_name = "";
	for(var i = 0; i < ds_list_size(ally_list);i++)
	{
		var ally = ally_list[| i];
		if ally[? GAME_CHAR_GENDER] == GENDER_FEMALE
		{
			var ally_name = ally[? GAME_CHAR_DISPLAYNAME];
			female_count++;
		}
	}
	
	if female_count == 1
		returning_string += "  It seems likely that some of "+string(ally_name)+"’s growth is due to pregnancy.  ";
	else if female_count > 1
		returning_string += "  It seems likely that some of the gurls’ growth is due to pregnancy.  ";
	else
		returning_string += "  ";
		
	return returning_string;
}
else
{
	return "";
}