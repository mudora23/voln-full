///location_map_add_location(location,x,y,hidden,marker,name,region,bg);
/// @description
/// @param location
/// @param x
/// @param y
/// @param hidden
/// @param marker
/// @param name
/// @param region
/// @param bg
with obj_control
{
    ds_map_add_map(location_map,argument[0],ds_map_create());
	ds_map_add(location_map[? argument[0]],"x",argument[1]);
	ds_map_add(location_map[? argument[0]],"y",argument[2]);
	//ds_map_add(location_map[? argument[0]],"hidden",argument[3]);
	ds_map_add(location_map[? argument[0]],"marker",argument[4]);
	ds_map_add(location_map[? argument[0]],"name",argument[5]);
	ds_map_add(location_map[? argument[0]],"region",argument[6]);
	ds_map_add(location_map[? argument[0]],"bg",argument[7]);
}
