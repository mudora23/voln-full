///location_get_current();
/// @description
with obj_control
{
	return obj_control.var_map[? VAR_LOCATION];
}