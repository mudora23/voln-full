///S_0007();
/// @description

_label("start");

_label("S_0007");
	_qj_remove("S_0007_Accept_QJ");
	_execute(0,location_unlock,LOCATION_42);
	_dialog("S_0007",true);
	_choice("Watch","S_0007_Watch");
	_choice("Interupt","S_0007_Interupt");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_Watch");
	_dialog("S_0007_Watch");
	_jump("S_0007_2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_Interupt");
	_dialog("S_0007_Interupt");
	_jump("S_0007_2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2");
	_show("lorn",spr_char_Lorn_s_n,10,5,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0007_2",true);
	_choice("“Yes”","S_0007_2_A");
	_choice("“Fuck off”","S_0007_2_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_A");
	_dialog("S_0007_2_A");
	_dialog("S_0007_2_A_2",true);
	_choice("“and you, mine.”","S_0007_2_A_2_A");
	_choice("“fuck off”","S_0007_2_A_2_A_C");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_A_2_A");
	_dialog("S_0007_2_A_2_A",true);
	_jump("S_0007_2_A_2_A_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_A_2_A_Choice");
	if !chunk_mark_get("S_0007_2_A_2_A_A")
		_choice("“How much treasure?”","S_0007_2_A_2_A_A");
	else
		_void();
	_choice("“Sure, come along.”","S_0007_2_A_2_A_B");
	_choice("“Fuck off.”","S_0007_2_A_2_A_C");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_A_2_A_A");
	_dialog("S_0007_2_A_2_A_A",true);
	_jump("S_0007_2_A_2_A_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_A_2_A_B");
	_dialog("S_0007_2_A_2_A_B",true);
	
	_colorscheme(COLORSCHEME_GETITEM);
	if ally_is_full()
		_dialog("        Lorn joined but the team was full. Lorn will wait in town.");
	else
		_dialog("        Lorn joined.");
	_execute(0,ally_add_char,NAME_LORN,true,max(char_get_info(char_get_player(),GAME_CHAR_LEVEL)+2,7));
	_colorscheme(COLORSCHEME_NORMAL);
	
	_qj_add("S_0007_Lorn_Joined_QJ");
	_jump("SL44_Menu","SL44");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_A_2_A_C");
	_dialog("S_0007_2_A_2_A_C");
	_movex("lorn",10,SMOOTH_SLIDE_NORMAL_SEC);
	_hide("lorn",FADE_NORMAL_SEC);
	_dialog("S_0007_2_A_2_A_C_2");
	
	_qj_add("S_0007_Lorn_Defeat_Refuse_QJ");
	_jump("SL44_Menu","SL44");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B");
	_dialog("S_0007_2_B",true);
	_choice("Attempt to humble him","S_0007_2_B_A");
	_choice("Apologize","S_0007_Apologize");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A");
	_dialog("S_0007_2_B_A");
	_dialog("S_0007_2_B_A_2",true);
	_choice("Challenge","S_0007_2_B_A_2_A");
	_choice("Apologize","S_0007_Apologize");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A");
	_dialog("S_0007_2_B_A_2_A");
	_execute(0,"S_0007_2_B_A_2_A_Battle");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A_Battle_Loss");
	_dialog("S_0007_2_B_A_2_A_Battle_Loss");
	_gameover();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A_Battle_Won");
	_execute(0,ally_rejoin_all);
	_dialog("S_0007_2_B_A_2_A_Battle_Won");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2",true);
	
	if inventory_item_count(ITEM_SHACKLES) >= 1 && inventory_item_count(ITEM_A_CHAIN_LEAD) >= 1
		_choice("Claim him","S_0007_2_B_A_2_A_Battle_Won_2_Fate1");
	_choice("Use him for your pleasure","S_0007_2_B_A_2_A_Battle_Won_2_Fate2");
	_choice("Banish him forever","S_0007_2_B_A_2_A_Battle_Won_2_Fate3");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A_Battle_Won_2_Fate1");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1_2");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1_3");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1_4");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1_5");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1_6");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate1_7");

	_colorscheme(COLORSCHEME_GETITEM);
	if pet_is_full()
		_dialog("        Lorn joined as a pet but the team was full. Lorn will wait in town.");
	else
		_dialog("        Lorn joined as a pet.");
	_execute(0,pet_add_char,NAME_LORN,true,max(char_get_info(char_get_player(),GAME_CHAR_LEVEL)+2,7));
	_colorscheme(COLORSCHEME_NORMAL);

	_qj_add("S_0007_Lorn_Defeat_Refuse_QJ");
	_jump("SL44_Menu","SL44");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A_Battle_Won_2_Fate2");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate2");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate2_2");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate2_3");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate2_4");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate2_5",true);

	_choice("“I suppose I could use a pump-toy aboard.”","S_0007_2_B_A_2_A_Battle_Won_2_Fate2_5_A");
	_choice("Reject","S_0007_2_B_A_2_A_Battle_Won_2_Fate3");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A_Battle_Won_2_Fate2_5_A");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate2_5_A");

	_colorscheme(COLORSCHEME_GETITEM);
	if ally_is_full()
		_dialog("        Lorn joined but the team was full. Lorn will wait in town.");
	else
		_dialog("        Lorn joined.");
	_execute(0,ally_add_char,NAME_LORN,true,max(char_get_info(char_get_player(),GAME_CHAR_LEVEL)+2,7));
	_colorscheme(COLORSCHEME_NORMAL);

	_qj_add("S_0007_Lorn_Joined_QJ");
	_jump("SL44_Menu","SL44");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_2_B_A_2_A_Battle_Won_2_Fate3");
	_dialog("S_0007_2_B_A_2_A_Battle_Won_2_Fate3");
	
	_qj_add("S_0007_Lorn_Defeat_Refuse_QJ");
	_jump("SL44_Menu","SL44");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_Apologize");
	_dialog("S_0007_Apologize");
	_dialog("S_0007_Apologize_2");
	_dialog("S_0007_Apologize_3");
	_dialog("S_0007_Apologize_4");
	_dialog("S_0007_Apologize_5",true);
	_choice("Accept","S_0007_ApJoin1");
	_choice("Reject","S_0007_2_A_2_A_C");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_ApJoin1");
	_dialog("S_0007_ApJoin1");

	_colorscheme(COLORSCHEME_GETITEM);
	if ally_is_full()
		_dialog("        Lorn joined but the team was full. Lorn will wait in town.");
	else
		_dialog("        Lorn joined.");
	_execute(0,ally_add_char,NAME_LORN,true,max(char_get_info(char_get_player(),GAME_CHAR_LEVEL)+2,7));
	_char_add_opinion(NAME_FELGOR,5);
	_colorscheme(COLORSCHEME_NORMAL);

	_qj_add("S_0007_Lorn_Joined_QJ");
	_jump("SL44_Menu","SL44");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0007_SL1_Bandit_Beatdown_Finish");
	_qj_remove("S_0007_SL1_Bandit_Beatdown_Finish_QJ");
	_qj_remove("S_0007_Lorn_Joined_QJ");
	_qj_remove("S_0007_Lorn_Defeat_Refuse_QJ");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish");
	_show("viktur",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_2");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_3");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_4");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_5");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_6");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_7");
	_dialog("S_0007_SL1_Bandit_Beatdown_Finish_8");
	_jump("MENU","SL1");





