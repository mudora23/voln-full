///storage_take_pet(i);
/// @description
/// @param i
var pet_list = obj_control.var_map[? VAR_PET_LIST];
var storage_list = obj_control.var_map[? VAR_STORAGE_LIST];
if argument0 < storage_get_pet_team_size()
{
	var char_map = storage_get_pet_char(argument0);
	ds_list_add(pet_list,char_map);
    ds_list_mark_as_map(pet_list,ds_list_size(pet_list)-1);
    storage_delete_pet_char(argument0);
}
