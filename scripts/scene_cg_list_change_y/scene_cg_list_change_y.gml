///scene_cg_list_change_y(ID,y_target);
/// @description  
/// @param ID
/// @param y_target

with obj_control
{
	if sprite_exists(argument[1])
	{
		var cg_y_target_list = obj_control.var_map[? VAR_CG_LIST_Y_TARGET];
		var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	
		if ds_list_find_index(cg_id_list,argument[0]) >= 0
		{
			var existing_index = ds_list_find_index(cg_id_list,argument[0]);
		
			cg_y_target_list[| existing_index] = argument[1];
	
		}
		
	}


}