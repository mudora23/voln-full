///location_map_update_path_and_neighbor_list(location[optional]);
/// @description - return a list of (path1,neighbor1,path2,neighbor2,...)
/// @param location
with obj_control
{
	var path_and_neighbor_list = obj_control.var_map[? VAR_LOCATION_PATH_AND_NEIGHBOR_LIST];
	ds_list_clear(path_and_neighbor_list);
	
	if argument_count > 0 var oName = argument[0];
	else var oName = obj_control.var_map[? VAR_LOCATION];
	var oMap = location_map[? oName];
	var oX = oMap[? "x"];
	var oY = oMap[? "y"];
	
	//show_debug_message("Current location xy: "+string(oX)+":"+string(oY));
	
    var location_name = ds_map_find_first(location_map);
	
    while(!is_undefined(location_name))
    {
		if location_name != oName
		{
			var location_info = ds_map_find_value(location_map,location_name);
			var location_x = location_info[? "x"];
			var location_y = location_info[? "y"];
			var path_number = 1;
			
			//show_debug_message("Checking location...."+location_name+" ("+string(location_x)+":"+string(location_y)+")");
			
			// the search begins from path_1, path_2,... until a path does not exist!
			while (asset_get_index("path_"+string(path_number))>=0)
			{
				var path = asset_get_index("path_"+string(path_number));
				//show_debug_message("path_"+string(path_number)+" ("+string(path_get_point_x(path,0))+":"+string(path_get_point_y(path,0))+" - "+string(path_get_point_x(path,path_get_number(path)-1))+":"+string(path_get_point_y(path,path_get_number(path)-1))+")");
				if path_get_number(path) >= 2
				{
					if (path_get_point_x(path,0) == location_x && path_get_point_y(path,0) == location_y) ||
					   (path_get_point_x(path,0) == oX && path_get_point_y(path,0) == oY)
					{
					
						if (path_get_point_x(path,path_get_number(path)-1) == location_x && path_get_point_y(path,path_get_number(path)-1) == location_y) ||
						   (path_get_point_x(path,path_get_number(path)-1) == oX && path_get_point_y(path,path_get_number(path)-1) == oY)
						{
							ds_list_add(path_and_neighbor_list,path,location_name);
							//show_debug_message("adding location..."+location_name);
							break;

						}
					
					}

				}
				path_number++;
			}
        }
        location_name = ds_map_find_next(location_map,location_name);
    }
	
	//show_debug_message("path_and_neighbor_list: "+ds_list_phrase_as_string(path_and_neighbor_list));
	//obj_control.var_map[? VAR_LOCATION_PATH_AND_NEIGHBOR_LIST] = path_and_neighbor_list;
	return path_and_neighbor_list;
	
}
