///draw_rectangle_in_area(x1,y1,x2,y2,area_x,area_y,area_width,area_height);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param area_x
/// @param area_y
/// @param area_width
/// @param area_height

var rec_x1 = max(argument0,argument4);
var rec_y1 = max(argument1,argument5);
var rec_x2 = min(argument2,argument4+argument6);
var rec_y2 = min(argument3,argument5+argument7);

if rec_x1 < rec_x2 && rec_y1 < rec_y2 draw_rectangle(rec_x1,rec_y1,rec_x2,rec_y2,false);
