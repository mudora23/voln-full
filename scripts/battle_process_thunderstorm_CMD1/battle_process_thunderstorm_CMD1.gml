///scene_script_battle_process_thunderstorm_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
	
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	
	skill_spend_cost(char_source,SKILL_THUNDERSTORM,char_get_skill_level(char_source,SKILL_THUNDERSTORM));




	var target_list = ds_list_create();
	
	if battle_get_current_action_char_owner() == OWNER_PLAYER || battle_get_current_action_char_owner() == OWNER_ALLY
	{
		
		var char_list = var_map[? VAR_ENEMY_LIST];
		//show_debug_message("Casting thunderstorm for player/ally...adding enemies to the list...");
		for(var i = 0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if battle_char_get_combat_capable(char)
			{
				//show_debug_message("adding enemy..."+string(char[? GAME_CHAR_NAME]));
				//show_debug_message("its SPI is..."+string(char_get_SPI(char)));
				ds_list_add(target_list,char);
			}
		}
	}
	else
	{
	
		var char_list = var_map[? VAR_ALLY_LIST];
		for(var i = 0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if battle_char_get_combat_capable(char) ds_list_add(target_list,char);
		}
		var char = var_map[? VAR_PLAYER];
		if battle_char_get_combat_capable(char) ds_list_add(target_list,char);
	}

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" casts",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" Thunderstorm",TAG_FONT_N,TAG_COLOR_TORU));
	ds_list_add(battlelog_pieces_list,text_add_markup(".",TAG_FONT_N,TAG_COLOR_NORMAL));

	script_execute_add(0,postfx_color_cut,1,2,1,c_black,0.8);
	script_execute_add(0,postfx_rain,4,200);
	
	var num = 1;
	repeat(8)
	{
		var time = random_range(1.5,2.5);
		script_execute_add(time,postfx_color_cut,0,0,0.2,c_white,1);
		script_execute_add(time,postfx_lightning,random_range(room_width*num/9,room_width*(num+1)/9),room_height);
		num++;
		
	}

	//show_debug_message("Casting thunderstorm for all enemies in the list... the list size is..."+string(ds_list_size(target_list)));
	
	for(var i = 0; i < ds_list_size(target_list);i++)
	{
		var char_target = target_list[| i];
		//show_debug_message("Enemy Name:"+string(char_target[? GAME_CHAR_NAME]));
		//show_debug_message("It's toru armor (1) "+string(char_get_toru_armor(char_target)));
		
		// performing
		var dodge_chance = char_get_dodge_chance(char_target);
		var hit_chance = char_get_hit_chance(char_source);
		var resistance_chance = char_get_resistance_chance(char_target);
	

		//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_TORU);
	
		if percent_chance(80-dodge_chance+hit_chance)
		{
			if char_get_skill_level(char_source,SKILL_THUNDERSTORM) == 3
			{
				if percent_chance(20)
				{
					if percent_chance(resistance_chance-hit_chance)
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,0.8,0.8);
					else
						battle_char_debuff_apply(char_target,DEBUFF_BURN,3,3,char_get_char_id(char_source));
				}
				var damage = char_get_toru_damage(char_source)* random_range(2.5,3.0);
			}
			else if char_get_skill_level(char_source,SKILL_THUNDERSTORM) == 2
			{
				if percent_chance(10)
				{
					if percent_chance(resistance_chance-hit_chance)
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,0.8,0.8);
					else
						battle_char_debuff_apply(char_target,DEBUFF_BURN,3,3,char_get_char_id(char_source));
				}
				var damage = char_get_toru_damage(char_source)* random_range(2.0,2.3);
			}
			else
			{
				if percent_chance(5)
				{
					if percent_chance(resistance_chance-hit_chance)
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,0.8,0.8);
					else
						battle_char_debuff_apply(char_target,DEBUFF_BURN,3,3,char_get_char_id(char_source));
				}
				var damage = char_get_toru_damage(char_source)* random_range(1.5,1.8);
			}
			
			var damage_size_adjust = 1;
			var shake_size_adjust = 1;
			if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);script_execute_add(3,voln_play_sfx,sfx_Hit8);damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
			
			//show_debug_message("It's toru armor (2) "+string(char_get_toru_armor(char_target)));
			
			var armor = char_get_toru_armor(char_target);
			var real_damage = damage * clamp(((100-armor)/100),0,1);
			
			script_execute_add(3,ds_map_replace_i,char_target,GAME_CHAR_HEALTH,max(0,char_target[? GAME_CHAR_HEALTH] - real_damage));
		
	
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP,3,1.1*damage_size_adjust);
			
			ally_get_hit_port(char_target);
		
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" for",0));
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" "+string(round(real_damage)),1));
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" damage!",0));
		
			char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
			script_execute_add(3,ds_map_replace_i,char_target,GAME_CHAR_SHAKE_AMOUNT,25*shake_size_adjust);
			
			var debug_message = "Thunderstorm is performed,";
			debug_message+= " source: ";
			debug_message+= string(damage);
			debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
			debug_message+= " target: ";
			debug_message+= string(armor);
			debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";

		}
		else
		{
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,3,0.7);
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" and misses!",0));
		}


	}



	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	
	ds_list_destroy(target_list);
	
	voln_play_sfx(sfx_Thunderstorm2);
	
	script_execute_add(3,voln_play_sfx,sfx_Clean_Hit_1);
	script_execute_add(5,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}