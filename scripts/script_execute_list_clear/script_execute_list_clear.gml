///script_execute_list_clear();
/// @description
with obj_control
{
	
	////// TOO SLOW (I want to run this script on load to prevent correcpted save)
	
    /*var key = ds_map_find_first(var_map);
    while(!is_undefined(key))
    {
        if string_count("VAR_EXECUTE_LIST", key) > 0
			if ds_exists(obj_control.var_map[? key],ds_type_list)
				ds_list_clear(obj_control.var_map[? key]);
        
        key = ds_map_find_next(var_map,key);
    }*/
	
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_DELAY]);
	    ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG0]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG1]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG2]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG3]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG4]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG5]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG6]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG7]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG8]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG9]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG10]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG11]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG12]);
		ds_list_clear(obj_control.var_map[? VAR_EXECUTE_LIST_ARG13]);
}