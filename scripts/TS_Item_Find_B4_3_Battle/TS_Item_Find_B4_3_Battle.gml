///TS_Elf_and_Imp_Orgy_B_BATTLE();
/// @description
with obj_control
{
	//var_map_set(VAR_MODE,VAR_MODE_BATTLE);

	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);

	create_enemies(ally_get_number()+1,5,0,NAME_ELF_BANDITS,NAME_GREEN_ORK,NAME_IMP);
	
	
	script_execute_add(0,battle_process_setup);
	//show_message("battle_process_setup in SBS_0001_Battle");

}	