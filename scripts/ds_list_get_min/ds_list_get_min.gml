///ds_list_get_min(id);
/// @description
/// @param id
var the_min = NOT_EXISTS;
for(var i = 0; i < ds_list_size(argument0);i++)
{
	if the_min != NOT_EXISTS
	{
		the_min = min(the_min,argument0[| i]);
	}
	else
		the_min = argument0[| i];
}
return the_min;