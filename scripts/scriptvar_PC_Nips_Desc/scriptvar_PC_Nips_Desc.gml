///scriptvar_PC_Nips_Desc();
/// @description
var num = var_map_get(PC_Nips_N);
if num == 0 return "normal";
else if num == 1 return choose("puffy","mounded");
else return choose("bulbous","feminine","femmy");