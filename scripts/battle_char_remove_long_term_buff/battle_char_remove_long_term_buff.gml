///battle_char_remove_long_term_buff(char,buff,hours);
/// @description
/// @param char
/// @param buff
/// @param hours
var char_map = argument0;
var char_buff_list = char_map[? GAME_CHAR_LONG_TERM_BUFF_LIST];
var char_buff_hours_list = char_map[? GAME_CHAR_LONG_TERM_BUFF_HOURS_LIST];
var char_buff = argument1;
var char_buff_hours = argument2;

var the_index = ds_list_find_index(char_map[? GAME_CHAR_LONG_TERM_BUFF_LIST],char_buff);
if the_index >= 0
{
	char_buff_hours_list[| the_index] = char_buff_hours_list[| the_index]-char_buff_hours;
	if char_buff_hours_list[| the_index] <= 0
	{
		ds_list_delete(char_buff_list,the_index);
		ds_list_delete(char_buff_hours_list,the_index);
	}
}
