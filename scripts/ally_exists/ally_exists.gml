///ally_exists(name/charid);
/// @description
/// @param name/charid
var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
for(var i = 0;i< ds_list_size(ally_list);i++)
{
	var char_map = ally_list[| i];
	if is_real(argument0) && char_map[? GAME_CHAR_ID] == argument0
		return true;
	if is_string(argument0) && char_map[? GAME_CHAR_NAME] == argument0
		return true;
}
return false;
