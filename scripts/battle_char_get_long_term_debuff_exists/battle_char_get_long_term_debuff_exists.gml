///battle_char_get_long_term_buff_exists(char,debuff);
/// @description
/// @param char
/// @param debuff
var char_map = argument0;
var char_debuff_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_LIST];
var char_debuff = argument1;
return ds_list_value_exists(char_debuff_list,char_debuff);