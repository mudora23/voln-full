///string_overwrite(original_string,string_index,new_string);
/// @description
/// @param original_string
/// @param string_index
/// @param new_string

var return_string = argument0;

return_string = string_delete(return_string,argument1,string_length(argument2));
return_string = string_insert(return_string,argument2,argument1);

return return_string;