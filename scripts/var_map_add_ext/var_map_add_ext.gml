///var_map_add_ext(KEY,AMOUNT,MIN,MAX);
/// @description
/// @param KEY
/// @param AMOUNT
/// @param MIN
/// @param MAX
var var_key = argument[0];
var var_amount = argument[1];

if var_key == VAR_SCRIPT_POSI
	obj_control.var_map[? VAR_SCRIPT_PROC_NUMBER] = 0;
	
obj_control.var_map[? var_key] = clamp(obj_control.var_map[? var_key]+var_amount,argument[2],argument[3]);