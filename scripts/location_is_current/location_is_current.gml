///location_is_current(LOCATION);
/// @description
with obj_control
{
	return argument[0] == obj_control.var_map[? VAR_LOCATION];
}