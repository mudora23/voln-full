///var_map_add(KEY,AMOUNT);
/// @description
/// @param KEY
/// @param AMOUNT
var var_key = argument[0];
var var_amount = argument[1];

if var_key == VAR_SCRIPT_POSI
	obj_control.var_map[? VAR_SCRIPT_PROC_NUMBER] = 0;
	
obj_control.var_map[? var_key]+=var_amount;
