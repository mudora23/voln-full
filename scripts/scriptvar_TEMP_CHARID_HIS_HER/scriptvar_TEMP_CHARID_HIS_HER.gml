///scriptvar_TEMP_CHARID_HIS_HER();
/// @description
var the_char_id = var_map_get(VAR_TEMP_CHARID);
var the_char = char_id_get_char(the_char_id);
if the_char != noone
{
	if the_char[? GAME_CHAR_GENDER] == GENDER_FEMALE
		return "her";
	else
		return "his";
}
else
	return "";