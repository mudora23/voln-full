///char_wiki_map_init();
/// @description
with obj_control
{
	
	char_wiki_map = ds_map_create();

	#macro NAME_ANY "NAME_ANY"

	#macro NAME_PLAYER "NAME_PLAYER"
	var name = NAME_PLAYER;
	var nametype = NAME_PLAYER; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_player_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_player_thumb));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Udonmania");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,10);
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2);
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	
	ds_map_add(char_map,GAME_CHAR_AI,AI_NONE);
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	
	#macro NAME_PLAYER_OFFSPRING "NAME_PLAYER_OFFSPRING"
	var name = NAME_PLAYER_OFFSPRING;
	var nametype = NAME_PLAYER_OFFSPRING;
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_player_offspring_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_player_offspring));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Your offspring");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_LRR,10);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,5);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,false);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_THIEF_1,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_THIEF_2,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_THIEF_3,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_THIEF_4,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WARRIOR_1,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WARRIOR_2,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WARRIOR_3,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WARRIOR_4,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WIZARD_1,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WIZARD_2,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WIZARD_3,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_WIZARD_4,c_white);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,c_white);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,c_white);
	
	#macro NAME_ZEYD "NAME_ZEYD"
	var name = NAME_ZEYD;
	var nametype = NAME_ZEYD; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Zeyd_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Zeyd_r_n));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Zeyd");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_LRR,30);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,5);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	#macro NAME_KUDI "NAME_KUDI"
	var name = NAME_KUDI;
	var nametype = NAME_KUDI; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Ku_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Ku_n_n));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Kûdi Brė");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_AI,AI_SUPPORTIVE);
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	#macro NAME_PUMMDU "NAME_PUMMDU"
	var name = NAME_PUMMDU;
	var nametype = NAME_PUMMDU; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Ku_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Ku_n_n));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Pummdu");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	#macro NAME_EST "NAME_EST"
	var name = NAME_EST;
	var nametype = NAME_EST; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Est_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Est_st_mel));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Est");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_AI,AI_SUPPORTIVE);
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	
	#macro NAME_LORN "NAME_LORN"
	var name = NAME_LORN;
	var nametype = NAME_LORN; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Lorn_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Lorn_s_n));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Lorn");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_LRR,10);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	#macro NAME_FELGOR "NAME_FELGOR"
	var name = NAME_FELGOR;
	var nametype = NAME_FELGOR; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Felg_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Felg_c_n));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Felgor");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.55);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.55);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.55);
	ds_map_add(char_map,GAME_CHAR_LRR,10);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_AI,AI_SUPPORTIVE);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	#macro NAME_BALWAG "NAME_BALWAG"
	var name = NAME_BALWAG;
	var nametype = NAME_BALWAG; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Bal_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Bal_r_ss));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Bålwåg");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,10);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	
	#macro NAME_JAM "NAME_JAM"
	var name = NAME_JAM;
	var nametype = NAME_JAM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Bal_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Bal_r_ss));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Jam");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,100);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,10);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5);
	
	ds_map_add(char_map,GAME_CHAR_LGR,20);
	
	ds_map_add(char_map,GAME_CHAR_UNIQUE,true);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_IMPORTANT,true);
	
	
	#macro NAME_ELF_BANDITS "NAME_ELF_BANDITS"
	#macro NAME_ELF_BANDITS_1 "NAME_ELF_BANDITS_1"
	var name = NAME_ELF_BANDITS_1;
	var nametype = NAME_ELF_BANDITS; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_elf_bandits_1_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_elf_bandits_1));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Elf Bandit");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,6);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,50);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,8893633);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,4484154);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,13686757);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Whip1,sfx_Whip2,sfx_Whip3);

	#macro NAME_ELF_BANDITS_2 "NAME_ELF_BANDITS_2"
	var name = NAME_ELF_BANDITS_2;
	var nametype = NAME_ELF_BANDITS; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_elf_bandits_2_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_elf_bandits_2));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Elf Bandit");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,6);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,50);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,11848399);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,7975482);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,14738159);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Hit7);
	
	#macro NAME_ELF_BANDITS_3 "NAME_ELF_BANDITS_3"
	var name = NAME_ELF_BANDITS_3;
	var nametype = NAME_ELF_BANDITS; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_elf_bandits_3_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_elf_bandits_3));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Elf Bandit");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,6);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,50);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,7248304);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,6714962);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,13028831);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Spin1);
	
	#macro NAME_ELF_BANDITS_1B "NAME_ELF_BANDITS_1B"
	var name = NAME_ELF_BANDITS_1B;
	var nametype = NAME_ELF_BANDITS; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_elf_bandits_1b_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_elf_bandits_1b));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Elf Bandit");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,6);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,50);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,4674922);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,5202760);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,13226462);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Whip1,sfx_Whip2,sfx_Whip3);

	#macro NAME_ELF_BANDITS_2B "NAME_ELF_BANDITS_2B"
	var name = NAME_ELF_BANDITS_2B;
	var nametype = NAME_ELF_BANDITS; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_elf_bandits_2b_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_elf_bandits_2b));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Elf Bandit");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,6);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,50);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,14211288);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,11381813);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,14738159);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Hit7);
	
	#macro NAME_ELF_BANDITS_3B "NAME_ELF_BANDITS_3B"
	var name = NAME_ELF_BANDITS_3B;
	var nametype = NAME_ELF_BANDITS; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_elf_bandits_3b_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_elf_bandits_3b));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Elf Bandit");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,6);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,50);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add(char_map,GAME_CHAR_COLOR_HAIR,9478342);
	ds_map_add(char_map,GAME_CHAR_COLOR_EYE,6254415);
	ds_map_add(char_map,GAME_CHAR_COLOR_SKIN,12699615);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Spin1);
	
	
	#macro NAME_GREEN_ORK "NAME_GREEN_ORK"
	#macro NAME_GREEN_ORK_1 "NAME_GREEN_ORK_1"
	var name = NAME_GREEN_ORK_1;
	var nametype = NAME_GREEN_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_ork_1_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_ork_1));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Green Huntər");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,10);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,15);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,100);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_GREEN_ORK_2 "NAME_GREEN_ORK_2"
	var name = NAME_GREEN_ORK_2;
	var nametype = NAME_GREEN_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_ork_2_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_ork_2));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Green Pråj");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,10);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,15);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,100);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_GREEN_ORK_3 "NAME_GREEN_ORK_3"
	var name = NAME_GREEN_ORK_3;
	var nametype = NAME_GREEN_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_ork_3_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_ork_3));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Green Toru’n");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,10);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,15);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,100);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_GREEN_ORK_4 "NAME_GREEN_ORK_4"
	var name = NAME_GREEN_ORK_4;
	var nametype = NAME_GREEN_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_ork_4_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_ork_4));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Green Fist");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,10);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,15);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,100);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_GREEN_ORK_5 "NAME_GREEN_ORK_5"
	var name = NAME_GREEN_ORK_5;
	var nametype = NAME_GREEN_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_ork_5_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_ork_5));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Green Fist");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,10);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,15);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,100);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_RED_ORK "NAME_RED_ORK"
	#macro NAME_RED_ORK_1 "NAME_RED_ORK_1"
	var name = NAME_RED_ORK_1;
	var nametype = NAME_RED_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_r_ork_1_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_r_ork_1));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Red Huntər");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,20);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.55);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,150);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_RED_ORK_2 "NAME_RED_ORK_2"
	var name = NAME_RED_ORK_2;
	var nametype = NAME_RED_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_r_ork_2_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_r_ork_2));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Red Pråj");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,20);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,150);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_RED_ORK_3 "NAME_RED_ORK_3"
	var name = NAME_RED_ORK_3;
	var nametype = NAME_RED_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_r_ork_3_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_r_ork_3));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Red Toru’n");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,20);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.45);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_FEMALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,150);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_RED_ORK_4 "NAME_RED_ORK_4"
	var name = NAME_RED_ORK_4;
	var nametype = NAME_RED_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_r_ork_4_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_r_ork_4));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Red Fist");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,20);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,150);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	#macro NAME_RED_ORK_5 "NAME_RED_ORK_5"
	var name = NAME_RED_ORK_5;
	var nametype = NAME_RED_ORK; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_r_ork_5_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_r_ork_5));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Red Fist");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,20);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.6);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.35);
	ds_map_add(char_map,GAME_CHAR_LRR,0);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,150);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,true);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);

	
	#macro NAME_IMP "NAME_IMP"
	#macro NAME_IMP_1 "NAME_IMP_1"
	var name = NAME_IMP_1;
	var nametype = NAME_IMP; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_imp_1_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_imp_1));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Imp");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,5);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,10);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.55);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,80);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Hit7);

	
	#macro NAME_IMP_2 "NAME_IMP_2"
	var name = NAME_IMP_2;
	var nametype = NAME_IMP; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_imp_2_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_imp_2));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Imp");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,5);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,10);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.5);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.3);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GENDER,GENDER_MALE);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,80);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_Hit7);
	
	
	#macro NAME_GRUM "NAME_GRUM"
	#macro NAME_GRUM_BLUE "NAME_GRUM_BLUE"
	var name = NAME_GRUM_BLUE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_blue_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_blue));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_INDIGO "NAME_GRUM_INDIGO"
	var name = NAME_GRUM_INDIGO;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_indigo_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_indigo));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_ROYALPURPLE "NAME_GRUM_ROYALPURPLE"
	var name = NAME_GRUM_ROYALPURPLE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_royalpurple_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_royalpurple));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_VIOLET "NAME_GRUM_VIOLET"
	var name = NAME_GRUM_VIOLET;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_violet_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_violet));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_PRIMROSE "NAME_GRUM_PRIMROSE"
	var name = NAME_GRUM_PRIMROSE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_primrose_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_primrose));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_REDPINK "NAME_GRUM_REDPINK"
	var name = NAME_GRUM_REDPINK;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_redpink_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_redpink));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_RED "NAME_GRUM_RED"
	var name = NAME_GRUM_RED;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_red_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_red));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_REDORANGE "NAME_GRUM_REDORANGE"
	var name = NAME_GRUM_REDORANGE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_redorange_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_redorange));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	
	#macro NAME_GRUM_ORANGE "NAME_GRUM_ORANGE"
	var name = NAME_GRUM_ORANGE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_orange_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_orange));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_YELLOWORANGE "NAME_GRUM_YELLOWORANGE"
	var name = NAME_GRUM_YELLOWORANGE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_yelloworange_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_yelloworange));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_ICEBLUE "NAME_GRUM_ICEBLUE"
	var name = NAME_GRUM_ICEBLUE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_iceblue_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_iceblue));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_AQUAMARINE "NAME_GRUM_AQUAMARINE"
	var name = NAME_GRUM_AQUAMARINE;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_aquamarine_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_aquamarine));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_TEAL "NAME_GRUM_TEAL"
	var name = NAME_GRUM_TEAL;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_teal_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_teal));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_SEAGREEN "NAME_GRUM_SEAGREEN"
	var name = NAME_GRUM_SEAGREEN;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_seagreen_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_seagreen));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_GREEN "NAME_GRUM_GREEN"
	var name = NAME_GRUM_GREEN;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_green_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_green));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_YELLOWGREEN "NAME_GRUM_YELLOWGREEN"
	var name = NAME_GRUM_YELLOWGREEN;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_yellowgreen_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_yellowgreen));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_GRUM_YELLOW "NAME_GRUM_YELLOW"
	var name = NAME_GRUM_YELLOW;
	var nametype = NAME_GRUM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_grum_yellow_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_grum_yellow));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Grum");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,12);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,24);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,200);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,true);
	
	ds_map_add_list(char_map,"battle_process_phy_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_phy_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	ds_map_add_list(char_map,"battle_process_toru_atk_CMD",ds_list_create());
	ds_list_add(char_map[? "battle_process_toru_atk_CMD"],sfx_grum_attack1,sfx_grum_attack2);
	
	#macro NAME_YIM "NAME_YIM"
	var name = NAME_YIM;
	var nametype = NAME_YIM; 
	ds_map_add_map(char_wiki_map,name,ds_map_create());
	var char_map = char_wiki_map[? name];
	ds_map_add(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_Yim_thumb));
	ds_map_add(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_Yim_n));
	ds_map_add(char_map,GAME_CHAR_NAME,name);
	ds_map_add(char_map,GAME_CHAR_NAME_TYPE,nametype);
	ds_map_add(char_map,GAME_CHAR_DISPLAYNAME,"Blu Yim");
	ds_map_add(char_map,GAME_CHAR_LEVEL,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MIN,1);
	ds_map_add(char_map,GAME_CHAR_LEVEL_MAX,1);
	ds_map_add(char_map,GAME_CHAR_STR_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_FOU_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_DEX_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_END_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_INT_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_WIS_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_SPI_RATE,0.4);
	ds_map_add(char_map,GAME_CHAR_LRR,100);
	
	ds_map_add(char_map,GAME_CHAR_GOLD_DROP,0);
	
	ds_map_add(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);
	
	ds_map_add(char_map,GAME_CHAR_TYPE,GAME_CHAR_PET);
	ds_map_add(char_map,GAME_CHAR_PET_YOBO,false);
	ds_map_add(char_map,GAME_CHAR_PET_TOY,true);
	ds_map_add(char_map,GAME_CHAR_PET_BREEDER,false);
	

}