///scriptvar_RBS_IMP_GAME_OVER_EX1();
/// @description

if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];
	return "The sounds of "+string(ally_name)+"’s resistance is all you can hear, even as your present occupier churns your guts…";
}
else
	return "";
