///seconds_to_timestamp(seconds);
/// @description
/// @param seconds
/*
    return a time stamp in 'h:mm:ss' string format
    argument0 - time stamp in seconds 
*/
var time_stamp_seconds = argument0;
var HOURS, MINUTES, SECONDS, MS;
//Converts a portion of the total seconds into hours
HOURS = floor(time_stamp_seconds / 3600);
//Converts a portion of the total seconds into minutes
MINUTES = floor(time_stamp_seconds / 60 mod 60);
if(MINUTES < 10) MINUTES = "0"+string(MINUTES);
//Converts a portion of the total seconds into seconds
SECONDS = floor(time_stamp_seconds mod 60);
if(SECONDS < 10) SECONDS = "0"+string(SECONDS);
//Creates the time in M:S format
if HOURS > 0 MS = string(HOURS)+":"+string(MINUTES)+":"+string(SECONDS);
else MS = string(MINUTES)+":"+string(SECONDS);

return MS;


