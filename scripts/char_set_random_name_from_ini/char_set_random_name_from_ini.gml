///char_set_random_name_from_ini(char);
/// @description
/// @param char

ini_open(working_directory+"name.ini");

var char_type_code = "elf";
if argument0[? GAME_CHAR_SEX] == GENDER_FEMALE var gender_code = "F";
else var gender_code = "M";

var section_name = char_type_code+gender_code;
var num_of_names = ini_read_real(section_name, "total", 0);
do
{
	var num = irandom_range(1, num_of_names);
	var new_name = ini_read_string(section_name, string(num), "Undefined name");
}
until num_of_names <= 1 || argument0[? GAME_CHAR_DISPLAYNAME] != new_name

argument0[? GAME_CHAR_DISPLAYNAME] = new_name;

ini_close();