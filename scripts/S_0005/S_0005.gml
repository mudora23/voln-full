///S_0005();
/// @description

_label("start");

_label("S_0005_1A");
	_dialog("S_0005_2");
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_3");
	_dialog("S_0005_4");
	_dialog("S_0005_5");
	_dialog("S_0005_6");
	_jump("MENU","SL1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0005_1B");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0005_2");
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_3");
	_dialog("S_0005_4");
	_dialog("S_0005_5");
	_dialog("S_0005_6");
	_jump("S_0005_7");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_7");
	_dialog("S_0005_7");
	_dialog("S_0005_8");
	_dialog("S_0005_9");
	_dialog("S_0005_10");
	_movex("ku",XNUM_LEFT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("bal",spr_char_Bal_r_ss,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_11");
	_dialog("S_0005_12");
	_dialog("S_0005_13");
	
	_choice("Enter","S_0005_13_A");
	_choice("Leave","MENU","SL1");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_6_1");
	_dialog("S_0005_6_1");
	_jump("MENU","SL1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_13_A");
	_dialog("S_0005_13_A");
	_hide_all();
	_main_gui(VAR_GUI_MIN);
	_dialog("S_0005_13_A_2");
	_main_gui(VAR_GUI_NORMAL);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE");
	_change_bg("bg",spr_bg_Nirholt_HD_01_crowd,FADE_NORMAL_SEC,1.5);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_Choice");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_HELP")
		_choice("Help desk","S_0005_SL1_HUN_DAOA_INSIDE_HELP");
	else
		_choice("Help desk","S_0005_SL1_HUN_DAOA_INSIDE_HELP2");
	
	if !chunk_mark_get("S_0005_WON") && !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_DANCE")
		_choice("Dancefloor","S_0005_SL1_HUN_DAOA_INSIDE_DANCE");
	else if !chunk_mark_get("S_0005_WON")
		_choice("Dancefloor","S_0005_SL1_HUN_DAOA_INSIDE_DANCE2");
	else
		_void();

	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BAR")
		_choice("Bar","S_0005_SL1_HUN_DAOA_INSIDE_BAR");
	else
		_choice("Bar","S_0005_SL1_HUN_DAOA_INSIDE_BAR2");

	if chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BAR") && !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BACK")
		_choice("Backroom","S_0005_SL1_HUN_DAOA_INSIDE_BACK");
	else if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BAR")
		_choice("Backroom","S_0005_SL1_HUN_DAOA_INSIDE_BACK2");
	else
		_void();

	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_WASH")
		_choice("Washroom","S_0005_SL1_HUN_DAOA_INSIDE_WASH");
	else
		_choice("Washroom","S_0005_SL1_HUN_DAOA_INSIDE_WASH2");
		
	if chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_HELP") &&
       chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_DANCE") &&
       chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BAR") &&
       chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BACK") &&
       chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_WASH") &&
       chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C")
	   {
		if chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_MYST")
			_choice("Mysterious door","S_0005_SL1_HUN_DAOA_INSIDE_MYST");
		else
			_choice("Mysterious door","S_0005_SL1_HUN_DAOA_INSIDE_MYST2");
	   }
	else
		_void();
	
	_choice("Leave","S_0005_SL1_HUN_DAOA_INSIDE_EXIT");   
	_block();
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("ph",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("ph",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_HELP_A") && !chunk_mark_get("S_0005_WON")
		_choice("“I’m looking for a colleague of mine.”","S_0005_SL1_HUN_DAOA_INSIDE_HELP_A");
	else
		_void();

	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_HELP_B")
		_choice("“What kinds of services does this place offer?”","S_0005_SL1_HUN_DAOA_INSIDE_HELP_B");
	else
		_void();

	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_HELP_C")
		_choice("“What’s your best drink?”","S_0005_SL1_HUN_DAOA_INSIDE_HELP_C");
	else
		_void();

	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_HELP_D")
		_choice("“With a body like yours, I’m surprised you don’t work as a dancer.”","S_0005_SL1_HUN_DAOA_INSIDE_HELP_D");
	else
		_void();

	_choice("Leave","S_0005_SL1_HUN_DAOA_INSIDE_HELP_EXIT");   
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP2");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("ph",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP_A",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP_B",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP_C");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP_C",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP_D");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP_D",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_HELP_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_HELP_EXIT");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_HELP_EXIT",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0005_SL1_HUN_DAOA_INSIDE_DANCE");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("torrie",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_Choice");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("torrie",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_choice("Continue dance.","S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A");  
	_choice("Get to the point.","S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_4");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_5");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_6");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_7");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_8");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_9");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_10");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_A_11",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B_4");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE_B_5",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_DANCE2");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("torrie",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE2_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_DANCE2_3",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BAR");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BAR");
	_show("dort",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BAR_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BAR_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BAR_4",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BAR2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BAR2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_WASH")
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH",true);
	_show("nigz",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_WASH_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_WASH_Choice");
	_choice("[#]“Come get it, big boy.”","S_0005_SL1_HUN_DAOA_INSIDE_WASH_A");  
	_choice("“No, thanks.”","S_0005_SL1_HUN_DAOA_INSIDE_WASH_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A_4");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A_5");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_A_6",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_WASH_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH_B_2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_WASH2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_WASH2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("driks",spr_char_anon_neutral_s,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_3");
	_void();//_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_4");
	_void();//_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_5");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_6");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_7");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_8");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_9");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_Choice");

	if !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_A") && !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_B")
	{
		_choice("“How do I get an invitation?”","S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_A");
		_choice("You grab your juŋk.  “Reserve this!”","S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_B");
	}
	else
	{
		_void();
		_void();
	}
	
	_choice("Attempt to make a bribe of your sprum.","S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C");	
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_A_2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_B");
	_execute(0,char_add_char_opinion,NAME_KUDI,obj_control.var_map[? VAR_PLAYER],1);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_B_2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_Choice");
	_choice("Pay him publicly.","S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A");  
	_choice("Arrange to pay him later.","S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A_4");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A_5");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A_6");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_A_7");
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_3");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_B");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_B_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_B_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_2_B_4");
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_3");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_3");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK_10_C_3",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_BACK2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_BACK2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_MYST");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_MYST_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_MYST_Choice");
	_choice("“I’m ready.”","S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY");  
	_choice("Leave","S_0005_SL1_HUN_DAOA_INSIDE_MYST_LEAVE");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_3");
	_showcg("zeyd_CG",spr_cg_S_0007,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,1,fa_right,fa_center,fa_right,fa_none,true,true);
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_4");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_5");
	_hidecg_all();
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_6");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_7");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_8");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY_9");
	_execute(0,"S_0005_Battle");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_MYST_LEAVE");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST_LEAVE",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_MYST2");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_MYST2",true);
	_jump("S_0005_SL1_HUN_DAOA_INSIDE_MYST2_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_MYST2_Choice");
	_choice("Go inside","S_0005_SL1_HUN_DAOA_INSIDE_MYST_READY");  
	_choice("Don’t go inside","S_0005_SL1_HUN_DAOA_INSIDE_MYST_LEAVE");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_SL1_HUN_DAOA_INSIDE_EXIT");
	_dialog("S_0005_SL1_HUN_DAOA_INSIDE_EXIT");
	_jump("MENU","SL1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON");
	_qj_remove("S_0005_FINDING_ZEYD_QJ");
	_dialog("S_0005_WON");
	_dialog("S_0005_WON_2");
	_dialog("S_0005_WON_3");
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_WON_4");
	_dialog("S_0005_WON_5");
	_movex("ku",XNUM_LEFT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("bal",spr_char_Bal_r_ss,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0005_WON_6");
	_dialog("S_0005_WON_7");
	_dialog("S_0005_WON_8");
	_dialog("S_0005_WON_9");
	_dialog("S_0005_WON_10");
	_dialog("S_0005_WON_11");
	_dialog("S_0005_WON_12");
	_dialog("S_0005_WON_13");
	_dialog("S_0005_WON_14",true);
	_execute(0,ally_rejoin,NAME_KUDI);
	_jump("S_0005_WON_14_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_Choice");
	if !chunk_mark_get("S_0005_WON_14_A")
		_choice("“Some strange creatures were molesting my friend.”","S_0005_WON_14_A");
	else
		_void();
		
	if !chunk_mark_get("S_0005_WON_14_B")
		_choice("“Kûdi said we interrupted some sort of ritual.”","S_0005_WON_14_B");
	else
		_void();
		
	_choice("“I’d best get going.”","S_0005_WON_14_C");

	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_A");
	_dialog("S_0005_WON_14_A");
	_dialog("S_0005_WON_14_A_2");
	_dialog("S_0005_WON_14_A_3",true);
	_jump("S_0005_WON_14_A_3_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_A_3_Choice");
	_choice("“What’s a jeŋ?”","S_0005_WON_14_C","","S_0005_WON_14_A_3_A");
	_choice("“What kind of powers does she have?”","S_0005_WON_14_A_3_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_A_3_A");
	_dialog("S_0005_WON_14_A_3_A",true);
	_jump("S_0005_WON_14_A_3_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_A_3_B");
	_dialog("S_0005_WON_14_A_3_B",true);
	_jump("S_0005_WON_14_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_B");
	_dialog("S_0005_WON_14_B",true);
	_jump("S_0005_WON_14_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0005_WON_14_C");
	_dialog("S_0005_WON_14_C");
	_execute(0,ally_rejoin,NAME_ZEYD);
	_execute(0,char_level_up_until_sync_with_player,ally_get_char(NAME_ZEYD));
	_qj_add("S_0006_QJ");
	_jump("MENU","SL1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0005_LOSS");
	_dialog("S_0005_LOSS");	
	_gameover();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	