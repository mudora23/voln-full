///skill_spend_cost(char,SKILL,level);
/// @descirption
/// @param char
/// @param SKILL
/// @param level

var char = argument0;
var skill = argument1;
var skill_map = obj_control.skill_wiki_map[? skill];
var level = argument2;

if level >= 1
{
	if ds_list_size(skill_map[? SKILL_VITALITY_LIST]) >= level
		char[? GAME_CHAR_VITALITY] = max(char[? GAME_CHAR_VITALITY] - ds_list_find_value(skill_map[? SKILL_VITALITY_LIST],level-1),0);
	if ds_list_size(skill_map[? SKILL_TORU_LIST]) >= level
		char[? GAME_CHAR_TORU] = max(char[? GAME_CHAR_TORU] - ds_list_find_value(skill_map[? SKILL_TORU_LIST],level-1),0);
	if ds_list_size(skill_map[? SKILL_STAMINA_LIST]) >= level
		char[? GAME_CHAR_STAMINA] = max(char[? GAME_CHAR_STAMINA] - ds_list_find_value(skill_map[? SKILL_STAMINA_LIST],level-1),0);
}