///skill_get_inherit_chance(skill_name);
/// @desription
/// @param skill_name

var skill_map = obj_control.skill_wiki_map[? argument0];
if ds_map_exists(skill_map,SKILL_INHERIT_CHANCE)
	return skill_map[? SKILL_INHERIT_CHANCE];
else
	return 0;