///CS_buying();
/// @description

_label("start");
	_choice("Exit","CS_exit","","",true,true,true,"",scene_store_clear);
	_var_map_set(VAR_MODE,VAR_MODE_STORE);
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("CS_buying");	
	_dialog("",true);
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,var_map_get(VAR_TEMP_ITEM),1);
	_var_map_add_ext(VAR_GOLD,-item_get_price(var_map_get(VAR_TEMP_ITEM)),0,var_map_get(VAR_GOLD));
	_dialog("        You bought \""+item_get_displayname(var_map_get(VAR_TEMP_ITEM))+"\".");
	_colorscheme(COLORSCHEME_NORMAL);
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("CS_buying10");	
	_dialog("",true);
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,var_map_get(VAR_TEMP_ITEM),10);
	_var_map_add_ext(VAR_GOLD,-item_get_price(var_map_get(VAR_TEMP_ITEM))*10,0,var_map_get(VAR_GOLD));
	_dialog("        You bought \""+item_get_displayname(var_map_get(VAR_TEMP_ITEM))+"\" x10.");
	_colorscheme(COLORSCHEME_NORMAL);
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("CS_exit");
	_jump(var_map_get(VAR_EXTRA2_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA2_AFTER_SCENE));
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	