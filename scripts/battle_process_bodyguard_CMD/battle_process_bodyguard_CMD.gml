///battle_process_bodyguard_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
			
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);


	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	if char_source[? GAME_CHAR_GENDER] == GENDER_FEMALE ds_list_add(battlelog_pieces_list,text_add_markup(" bodyguards her allies.",TAG_FONT_N,TAG_COLOR_EFFECT));
	else ds_list_add(battlelog_pieces_list,text_add_markup(" bodyguards his allies.",TAG_FONT_N,TAG_COLOR_EFFECT));

	skill_spend_cost(char_source,SKILL_BODYGUARD,char_get_skill_level(char_source,SKILL_BODYGUARD));
	
	
	battle_char_buff_apply(char_source,BUFF_BODYGUARD,char_get_skill_level(battle_get_current_action_char(),BUFF_BODYGUARD),2);
	
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);

	voln_play_sfx(sfx_bodyguard);
	
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);

}