///_aes_InvMixColumns(state)
var i, state = argument0, a,b,c,d;
for(i=0;i<4;++i)
{ 
  a = state[@ i, 0];
  b = state[@ i, 1];
  c = state[@ i, 2];
  d = state[@ i, 3];

  state[@ i, 0] = _aes_Multiply(a, $0e) ^ _aes_Multiply(b, $0b) ^ _aes_Multiply(c, $0d) ^ _aes_Multiply(d, $09);
  state[@ i, 1] = _aes_Multiply(a, $09) ^ _aes_Multiply(b, $0e) ^ _aes_Multiply(c, $0b) ^ _aes_Multiply(d, $0d);
  state[@ i, 2] = _aes_Multiply(a, $0d) ^ _aes_Multiply(b, $09) ^ _aes_Multiply(c, $0e) ^ _aes_Multiply(d, $0b);
  state[@ i, 3] = _aes_Multiply(a, $0b) ^ _aes_Multiply(b, $0d) ^ _aes_Multiply(c, $09) ^ _aes_Multiply(d, $0e);
}
