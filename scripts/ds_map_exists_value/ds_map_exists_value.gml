///ds_map_exists_value(id,key,value)
//returns true if the key exists and is the proposed value
var m = argument[0], k = argument[1];
if (ds_map_exists(m, k) && m[? k] == argument[2])
    return true;
return false ;


