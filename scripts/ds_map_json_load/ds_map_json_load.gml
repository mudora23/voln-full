///ds_map_json_load(filename);
/// @description
/// @param filename
if file_exists(argument[0])
{
    var file = file_text_open_read(argument[0]);
    var str = file_text_read_string(file);
    file_text_close(file);
    return json_decode(str);
}