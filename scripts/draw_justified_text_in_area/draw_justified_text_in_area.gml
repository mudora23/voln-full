///draw_justified_text_in_area(text,surface,x1,y1,x2,y2,padding,lineheight_spacing,linebreakheight_spacing,scale);
/// @description
/// @param text
/// @param surface
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param padding
/// @param lineheight_spacing
/// @param linebreakheight_spacing
/// @param scale

var text = argument0;
var surface = argument1;
var x1 = argument2;
var y1 = argument3;
var x2 = argument4;
var y2 = argument5;
var padding = argument6;
var scale = argument9;
var lineheight_spacing = argument7 + string_height("M")*scale;
var linebreakheight_spacing = argument8 + string_height("M")*scale;

if surface == noone || !surface_exists(surface)
{
	var old_alpha = draw_get_alpha();
	draw_set_alpha(1);
	
    // words list
    var widthmax = x2-x1-padding*2;
    var curWidth = 0;
    var curCharCount = 0;
    var words_list = string_to_words_list(text,widthmax);
    var linebreak_list = ds_list_create();
    var linespaces_list = ds_list_create();
    var string_posi = 0;
    var text_tag_map = noone;

    for (var ii=0;ii<ds_list_size(words_list);ii++)
    {
        var words = words_list[| ii];
        //if text_tag_map != noone
        //    text_tag_map_tag_apply(text_tag_map,string_posi);
        
        if words == "\r"
        {
            ds_list_add(linebreak_list,string_posi);
            ds_list_add(linespaces_list,0);
            curWidth = string_width(words)*scale;
            curCharCount = string_length(words);
        }
        else if words == "" && curWidth + string_width(words)*scale <= widthmax
        {
            if curWidth = 0 {curWidth = string_width(" ")*scale; curCharCount = 1;}
            else {curWidth += string_width(" ")*scale; curCharCount += 1;}
        }
        else if curWidth + string_width(" ")*scale+string_width(words)*scale <= widthmax
        {
            if curWidth = 0 {curWidth = string_width(words)*scale; curCharCount = string_length(words);}
            else{ curWidth += string_width(" "+words)*scale; curCharCount += 1+string_length(words);}
        }
        else
        {
            ds_list_add(linebreak_list,string_posi);
			
            ds_list_add(linespaces_list,(widthmax-curWidth)/curCharCount);
            curWidth = string_width(words)*scale;
            curCharCount = string_length(words);
        }
        
        if words != "\r"
            string_posi += 1 + string_length(words);

    }

    // drawing
	if surface_exists(surface) surface_free(surface);
    surface = surface_create(x2-x1,y2-y1);
    surface_set_target(surface);
    draw_clear_alpha(c_black,0);
    var the_char_x = padding;
    var the_char_y = padding;
    var the_char_line = 1;

    for(var i = 1; i <= string_length(text);i++)
    {
        var the_char = string_char_at(text,i);
        var the_char_width = string_width(the_char)*scale;
        if ds_list_size(linespaces_list) >= the_char_line
            var the_char_line_space = linespaces_list[| the_char_line-1];
        else
            var the_char_line_space = 0;
        
        draw_text_transformed(the_char_x,the_char_y,the_char,scale,scale,0);
		
        the_char_x += the_char_width + the_char_line_space;
        
        var linebreak_list_index = ds_list_find_index(linebreak_list,i);
        if linebreak_list_index >= 0
        {
            the_char_line++;
            the_char_x = padding;
            the_char_y += lineheight_spacing;
        }
        if the_char == "\r"
            the_char_y += linebreakheight_spacing - lineheight_spacing;
        

    }

    surface_reset_target();
    
    ds_list_destroy(words_list);
    ds_list_destroy(linebreak_list);
    ds_list_destroy(linespaces_list);
	
	draw_set_alpha(old_alpha);
}

draw_surface_ext(surface,x1,y1,1,1,0,c_white,draw_get_alpha());
return surface;