///scene_sprite_list_change_y(ID,y_target,slide_speed);
/// @description  
/// @param ID
/// @param ynum_target
/// @param slide_speed

with obj_control
{

	var sprite_y_target_list = obj_control.var_map[? VAR_SPRITE_LIST_Y_TARGET];
	var sprite_slide_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_SLIDE_SPEED];
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	var sprite_index_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME];
	var sprite_size_ratio_list = obj_control.var_map[? VAR_SPRITE_LIST_SIZE_RATIO];
	
	if ds_list_find_index(sprite_id_list,argument[0]) >= 0
	{
		var existing_index = ds_list_find_index(sprite_id_list,argument[0]);
		
		sprite_y_target_list[| existing_index] = ynum_to_y(sprite_index_list[| existing_index],sprite_size_ratio_list[| existing_index],argument[1]);
		sprite_slide_speed_list[| existing_index] = argument[2];
	}


}
