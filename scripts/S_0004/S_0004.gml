///S_0004();
/// @description


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////// section 1 ///////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
_label("start");
_label("S_0004_MEET_HUT_INTRO_EVENT");
	_qj_remove("S_0004_QJ");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT");
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_2");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_3");
	_play_sfx(sfx_Wooden_Door_Open_2);
	_change_bg("bg",spr_bg_Nirholt_MH_01,FADE_NORMAL_SEC,1.5);
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4",true);
	_jump("S_0004_Choice1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_Choice1");
	_choice("“It’s okay.”","S_0004_MEET_HUT_INTRO_EVENT_4_A","","S_0004_MEET_HUT_INTRO_EVENT_4_A");
	_choice("“Remind me why we’re here.”","S_0004_MEET_HUT_INTRO_EVENT_4_B");
	_choice("“Where’s Zeyd?”","S_0004_MEET_HUT_INTRO_EVENT_4_C","","S_0004_MEET_HUT_INTRO_EVENT_4_C");
	_choice("“That thing you did in the woods, would you do it again?”","S_0004_MEET_HUT_INTRO_EVENT_4_D");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_MEET_HUT_INTRO_EVENT_4_A");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_A",true);
	_jump("S_0004_Choice1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_MEET_HUT_INTRO_EVENT_4_B");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_B");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_B_2",true);
	_jump("S_0004_Choice2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_Choice2");
	_choice("“Who is your friend?”","S_0004_MEET_HUT_INTRO_EVENT_4_B_2_A","","S_0004_MEET_HUT_INTRO_EVENT_4_B_2_A");
	_choice("“Then let’s get going.”","S_0004_MEET_HUT_INTRO_EVENT_4_B_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_MEET_HUT_INTRO_EVENT_4_C");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_C",true);
	_jump("S_0004_Choice1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_MEET_HUT_INTRO_EVENT_4_D");
	_change("ku",spr_char_Ku_n_ss);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_D");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_D_2");
	_showcg("ku_CG",spr_cg_Ku_sex02_1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,1,fa_right,fa_center,fa_right,fa_none,false,true);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_D_2_2");
	_hidecg_all();
	_change("ku",spr_char_Ku_n_n);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_D_2_2_hide",true);
	_choice("“Then let’s get going.”","S_0004_MEET_HUT_INTRO_EVENT_4_B_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_MEET_HUT_INTRO_EVENT_4_B_2_A");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_B_2_A",true);
	_jump("S_0004_Choice2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_MEET_HUT_INTRO_EVENT_4_B_2_B");
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_B_2_B");
	_play_sfx(sfx_Wooden_Door_Close_1);
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change("bg",spr_bg_black);
	_main_gui(VAR_GUI_MIN);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_4_B_2_B_2");
	_execute(0,ally_rejoin,NAME_KUDI);
	_main_gui(VAR_GUI_NORMAL);
	_change("bg",spr_bg_Nirholt_Streets_01_midsun);
	_dialog("S_0004_MEET_HUT_INTRO_EVENT_5");
	_jump("S_0004_JAMS_INTRO_EVENT");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_JAMS_INTRO_EVENT");
	_execute(0,chunk_mark_set,"SL1_JAMS_INTRO",true);
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_JAMS_INTRO_EVENT");
	_change_bg("bg",spr_bg_Nirholt_HD_02,FADE_NORMAL_SEC,1.5);
	_dialog("S_0004_JAMS_INTRO_EVENT_2");
	_dialog("S_0004_JAMS_INTRO_EVENT_3");
	_dialog("S_0004_JAMS_INTRO_EVENT_4");
	_dialog("S_0004_JAMS_INTRO_EVENT_5");
	_dialog("S_0004_JAMS_INTRO_EVENT_6");
	_dialog("S_0004_JAMS_INTRO_EVENT_7");
	_qj_add("S_0004_JAMS_INTRO_EVENT_7_QJ");
	_dialog("S_0004_JAMS_INTRO_EVENT_8");
	_dialog("S_0004_JAMS_INTRO_EVENT_9");
	_dialog("S_0004_JAMS_INTRO_EVENT_10");
	_qj_remove("S_0004_JAMS_INTRO_EVENT_7_QJ");
	_change_bg("bg",spr_bg_Nirholt_Streets_01_midsun,FADE_NORMAL_SEC,1.5);
	_jump("S_0004_DAGS_INTRO_EVENT");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_DAGS_INTRO_EVENT");
	_execute(0,chunk_mark_set,"SL1_DAGS_INTRO",true);
	_dialog("S_0004_DAGS_INTRO_EVENT");
	_play_sfx(sfx_Wooden_Door_Open_2);
	_change_bg("bg",spr_bg_Nirholt_Dags_01_crowd,FADE_NORMAL_SEC,1.5);
	_dialog("S_0004_DAGS_INTRO_EVENT_2");
	_dialog("S_0004_DAGS_INTRO_EVENT_3");
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_DAGS_INTRO_EVENT_4");
	_dialog("S_0004_DAGS_INTRO_EVENT_5");
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add(VAR_JAR,1);
	_dialog("        You receive 1 jar!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("S_0004_DAGS_INTRO_EVENT_7");
	_dialog("S_0004_DAGS_INTRO_EVENT_8");
	_jump("S_0004_FELGOR_INTRO_EVENT");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0004_FELGOR_INTRO_EVENT",true);
	_choice("Don’t sneak past.","S_0004_FELGOR_INTRO_EVENT_A");
	_choice("Attempt to sneak past.","S_0004_FELGOR_INTRO_EVENT_B");
	_block();
	
_label("S_0004_FELGOR_INTRO_EVENT_A");
	_qj_add("S_0004_FELGOR_INTRO_EVENT_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT_A");
	_jump("SL1_DAGS_RETURN","SL1_DAGS");
	
_label("S_0004_FELGOR_INTRO_EVENT_B");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT_QJ");
	_change_bg("bg",spr_bg_Nirholt_Dags_01_crowd,FADE_NORMAL_SEC,1.5);
	_dialog("S_0004_FELGOR_INTRO_EVENT_B");
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_2");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_3");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_4");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_5");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_6");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_7");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_8");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_9");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_10");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_11");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_12");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_13");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_14");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_15");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16",true);
	_jump("S_0004_FELGOR_INTRO_EVENT_B_16_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT_B_16_Choice");
	_choice("“Why do you need a strong steed?”","S_0004_FELGOR_INTRO_EVENT_B_16_A","","S_0004_FELGOR_INTRO_EVENT_B_16_A");
	_choice("“I’ve decided to help you, Your Norliŋskip.”","S_0004_FELGOR_INTRO_EVENT_B_16_B");
	_choice("“I’m not going to help you.”","S_0004_FELGOR_INTRO_EVENT_B_16_C");
	_block();
	
_label("S_0004_FELGOR_INTRO_EVENT_B_16_A");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_A",true);
	_jump("S_0004_FELGOR_INTRO_EVENT_B_16_Choice");
	
_label("S_0004_FELGOR_INTRO_EVENT_B_16_B");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT_B_16_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_B_2");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_B_3");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_B_4");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_B_5");
	_qj_add("S_0004_FELGOR_INTRO_EVENT_B_16_B_5_QJ");
	_jump("MENU",SL1);
	
_label("S_0004_FELGOR_INTRO_EVENT_B_16_C");
	_qj_add("S_0004_FELGOR_INTRO_EVENT_B_16_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_C");
	_jump("SL1_DAGS_RETURN","SL1_DAGS");
	
_label("S_0004_FELGOR_INTRO_EVENT_B_16_R");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_R");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_R_2",true);
	_choice("“Yes.”","S_0004_FELGOR_INTRO_EVENT_B_16_B");
	_choice("“No.”","S_0004_FELGOR_INTRO_EVENT_B_16_R_2_B");
	_block();
	
_label("S_0004_FELGOR_INTRO_EVENT_B_16_R_2_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_R_2_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT_B_16_R_2_B_2");
	_jump("SL1_DAGS_RETURN","SL1_DAGS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2");
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_execute(0,chunk_mark_set,"SL1_BRED_BEST_INTRO",true);
	_dialog("S_0004_FELGOR_INTRO_EVENT2");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_2");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_3");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_4");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_5");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6",true);
	_choice("“Yes.”","S_0004_FELGOR_INTRO_EVENT2_6_A");
	_choice("“No.”","S_0004_FELGOR_INTRO_EVENT2_6_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_6_A");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT2_6_QJ");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT_B_16_B_5_QJ");
	_change("felg",spr_char_Felg_c_s);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_A");
	_change("felg",spr_char_Felg_c_n);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_A_2");
	_qj_add("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ");
	_jump("MENU",SL1);
	
_label("S_0004_FELGOR_INTRO_EVENT2_6_B");
	_qj_add("S_0004_FELGOR_INTRO_EVENT2_6_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_B_2");
	_jump("MENU",SL1);
	
_label("S_0004_FELGOR_INTRO_EVENT2_6_R");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_R");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_R_2",true);
	_choice("“Fine, I’ll do it.”","S_0004_FELGOR_INTRO_EVENT2_6_A");
	_choice("“I’m leaving.”","S_0004_FELGOR_INTRO_EVENT2_6_R_B");
	_block();
	
_label("S_0004_FELGOR_INTRO_EVENT2_6_R_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_R_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_6_R_B_2");
	_jump("MENU",SL1);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0004_FELGOR_INTRO_EVENT2_FINISH");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ");
	_colorscheme(COLORSCHEME_GETITEM);
	_delete_item(ITEM_Salo_wurm,5);
	_dialog("5 salo wurms are removed from the inventory.");
	_colorscheme(COLORSCHEME_NORMAL);
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_2");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_3");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_4");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_5",true);
	_choice("“Uhh, no thanks.  ”","S_0004_FELGOR_INTRO_EVENT2_FINISH_6_A");
	_choice("“...  Fikk yes!”","S_0004_FELGOR_INTRO_EVENT2_FINISH_6_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_6_A");	
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_6_A");
	_jump("S_0004_FELGOR_INTRO_EVENT2_FINISH_7");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_6_B");	
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_6_B_1");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_6_B_2");
	_jump("S_0004_FELGOR_INTRO_EVENT2_FINISH_7");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7");	
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7",true);
	_choice("“Very well.”","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A");
	_choice("“You’re not getting my sprum.”","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ");
	_qj_remove("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2",true);
	_choice("“No, thanks.  I’ve got a hand of my own.”","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_A");
	_choice("“Sure, you do the work.”","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_A");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_A");
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add(VAR_JAR,1);
	_dialog("        You receive 1 jar!");
	_colorscheme(COLORSCHEME_NORMAL);
	_hide("burt",FADE_NORMAL_SEC);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_A_3");
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add(VAR_JAR,-1);
	_dialog("        1 jar is removed from the inventory.");
	_colorscheme(COLORSCHEME_NORMAL);
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_A_4");
	_hide("burt",FADE_NORMAL_SEC);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_A_5");
	_jump("S_0004_FELGOR_INTRO_EVENT_FINISH");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B_2");
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add(VAR_JAR,1);
	_dialog("        You receive 1 jar!");
	_colorscheme(COLORSCHEME_NORMAL);
	_hide("burt",FADE_NORMAL_SEC);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B_3");
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add(VAR_JAR,-1);
	_dialog("        1 jar is removed from the inventory.");
	_colorscheme(COLORSCHEME_NORMAL);
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B_4");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B_5");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B_6");
	_hide("burt",FADE_NORMAL_SEC);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A_2_B_7");
	_jump("S_0004_FELGOR_INTRO_EVENT_FINISH");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_B");
	_qj_add("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_B");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_B_2");
	_jump("MENU",SL1);

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_R");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("burt",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_R",true);
	_choice("“No.”","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_R_A");
	_choice("“I suppose I have.”","S_0004_FELGOR_INTRO_EVENT2_FINISH_7_A");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_R_A");
	_dialog("S_0004_FELGOR_INTRO_EVENT2_FINISH_7_R_A");
	_jump("MENU",SL1);

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0004_FELGOR_INTRO_EVENT_FINISH");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_qj_add("S_0005_FINDING_ZEYD_QJ");
	_dialog("S_0004_FELGOR_INTRO_EVENT_FINISH");
	_dialog("S_0004_FELGOR_INTRO_EVENT_FINISH_2");
	_dialog("S_0004_FELGOR_INTRO_EVENT_FINISH_3");
	_dialog("S_0004_FELGOR_INTRO_EVENT_FINISH_4");
	_dialog("S_0004_FELGOR_INTRO_EVENT_FINISH_5");
	_jump("MENU",SL1);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	