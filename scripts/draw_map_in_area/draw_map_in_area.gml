///draw_map_in_area(map,map_xsize,map_ysize,area_x,area_y,area_w,area_h,map_xoffset,map_yoffset,alpha,xscale,yscale);
/// @description
/// @param map
/// @param map_xsize
/// @param map_ysize
/// @param area_x
/// @param area_y
/// @param area_w
/// @param area_h
/// @param map_xoffset
/// @param map_yoffset
/// @param alpha
/// @param xscale
/// @param yscale

var map = argument0;
var map_xsize = argument1;
var map_ysize = argument2;
var map_w = sprite_get_width(map);
var map_h = sprite_get_height(map);
var area_x = argument3;
var area_y = argument4;
var area_w = argument5;
var area_h = argument6;
var map_xoffset = argument7;
var map_yoffset = argument8;
var alpha = argument9;
var xscale = argument10;
var yscale = argument11;

for(var img_index_y = 0; img_index_y < map_ysize; img_index_y++)
{
    for(var img_index_x = 0; img_index_x < map_xsize; img_index_x++)
    {
        var img_index = img_index_y * map_xsize + img_index_x;
        var img_x = (img_index_x*map_w*xscale)-map_xoffset+area_x;
        var img_y = (img_index_y*map_h*yscale)-map_yoffset+area_y;
        draw_sprite_in_visible_area(map,img_index,/*floor(*/img_x/*)*/,/*floor(*/img_y/*)*/,xscale,yscale,c_white,alpha,area_x,area_y,area_w,area_h);
		
    }
}