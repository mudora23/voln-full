///draw_borders_in_area(topleft,topright,botleft,botright,toploop,botloop,leftloop,rightloop,x,y,w,h,alpha,halign,valign,color[optional]);
/// @description
/// @param topleft
/// @param topright
/// @param botleft
/// @param botright
/// @param toploop
/// @param botloop
/// @param leftloop
/// @param rightloop
/// @param x
/// @param y
/// @param w
/// @param h
/// @param alpha
/// @param halign
/// @param valign
/// @param color[optional]

var topleft = argument[0];
var topright = argument[1];
var botleft = argument[2];
var botright = argument[3];

var toploop = argument[4];
var botloop = argument[5];
var leftloop = argument[6];
var rightloop = argument[7];

var x0 = argument[8];
var y0 = argument[9];
var w = argument[10];
var h = argument[11];

var alpha = argument[12];

var halign = argument[13];
var valign = argument[14];
if argument_count > 15 var col = argument[15];
else var col = c_white;

var x_0_top = x0;
var x_1_top = x_0_top + sprite_get_width(topleft)-sprite_get_xoffset(topleft)-1;
var x_3_top = x_0_top + w+1;
var x_2_top = x_3_top - sprite_get_xoffset(topright);

var x_0_bot = x0;
var x_1_bot = x_0_bot + sprite_get_width(botleft)-sprite_get_xoffset(botleft)-1;
var x_3_bot = x_0_bot + w+1;
var x_2_bot = x_3_bot - sprite_get_xoffset(botright);

var y_left_0 = y0;
var y_left_1 = y_left_0 + sprite_get_height(topleft)-sprite_get_yoffset(topleft)-1;
var y_left_3 = y_left_0 + h+1;
var y_left_2 = y_left_3 - sprite_get_yoffset(botleft);

var y_right_0 = y0;
var y_right_1 = y_right_0 + sprite_get_height(topright)-sprite_get_yoffset(topright)-1;
var y_right_3 = y_right_0 + h+1;
var y_right_2 = y_right_3 - sprite_get_yoffset(botright);

// top/bot seemless
var current_loop = toploop;
var current_loop_w = sprite_get_width(current_loop);
var current_loop_h = sprite_get_height(current_loop);
var ww = x_2_top - x_1_top;

if halign == fa_left var x_start = x_1_top;
else if halign == fa_right var x_start = x_2_top - (ceil(ww / current_loop_w)*current_loop_w);
else var x_start = x_1_top + ww/2 - (ceil(ww/2 / current_loop_w)*current_loop_w);

var draw_top = 0;
var draw_height = current_loop_h;

for(var xx = x_start; xx < x_2_top; xx += current_loop_w)
{
	if xx < x_1_top
	{
		var draw_left = x_1_top - xx;
		var draw_width = current_loop_w - draw_left + 1;
		var draw_xx = x_1_top;
	}
	else if xx > x_2_top - current_loop_w
	{
		var draw_left = 0;
		var draw_width = x_1_top + ww - xx + 1;
		var draw_xx = xx;
	}
	else
	{
		var draw_left = 0;
		var draw_width = current_loop_w+1;
		var draw_xx = xx;
	}
	
	// top
	var draw_yy = y_left_0-sprite_get_yoffset(toploop);
	draw_sprite_part_ext(toploop,-1,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,1,1,col,alpha);

	// bot
	var draw_yy = y_left_3-sprite_get_yoffset(botloop);
	draw_sprite_part_ext(botloop,-1,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,1,1,col,alpha);
}




// left/right seemless
var current_loop = leftloop;
var current_loop_w = sprite_get_width(current_loop);
var current_loop_h = sprite_get_height(current_loop);
var hh = y_left_2 - y_left_1;

if valign == fa_top var y_start = y_left_1;
else if valign == fa_bottom var y_start = y_left_2 - (ceil(hh / current_loop_h)*current_loop_h);
else var y_start =  y_left_1 + hh/2 - (ceil(hh/2 / current_loop_h)*current_loop_h);

var draw_left = 0;
var draw_width = current_loop_w;

for(var yy = y_start; yy < y_left_2; yy += current_loop_h)
{
	if yy < y_left_1
	{
		var draw_top = y_left_1 - yy;
		var draw_height = current_loop_h - draw_top + 1;
		var draw_yy = y_left_1;
	}
	else if yy > y_left_2 - current_loop_h
	{
		var draw_top = 0;
		var draw_height = y_left_1 + hh - yy + 1;
		var draw_yy = yy;
	}
	else
	{
		var draw_top = 0;
		var draw_height = current_loop_h+1;
		var draw_yy = yy;
	}
	
	// left
	var draw_xx = x_0_top-sprite_get_xoffset(leftloop);
	draw_sprite_part_ext(current_loop,-1,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,1,1,col,alpha);

	// right
	var draw_xx = x_3_top-sprite_get_xoffset(rightloop);
	draw_sprite_part_ext(rightloop,-1,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,1,1,col,alpha);
}


// draw corners
draw_sprite_ext(topleft,0,x0,y0,1,1,0,col,alpha);
draw_sprite_ext(topright,0,x0+w,y0,1,1,0,col,alpha);
draw_sprite_ext(botleft,0,x0,y0+h,1,1,0,col,alpha);
draw_sprite_ext(botright,0,x0+w,y0+h,1,1,0,col,alpha);
