///scriptvar_PC_LegsTh_Desc();
/// @description
var num = var_map_get(PC_LegsTh_N);
if num == 0 return choose("slim","elegant","slender");
else if num == 1 return "normal";
else if num == 2 return choose("shapely","toned");
else return choose("thick","curvy","femmy");