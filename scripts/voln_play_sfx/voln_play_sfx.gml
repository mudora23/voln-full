///voln_play_sfx(sfx);
/// @description
/// @param sfx
var the_sfx = audio_play_sound(audio_get_index_ext(argument0),1,false);
audio_sound_gain(the_sfx, obj_control.settings_map[? SETTINGS_SFX_LEVEL]/100, 0);