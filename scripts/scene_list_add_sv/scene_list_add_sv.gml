///BS_END_HANDLER_AUTO_SV(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Elfs
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_F_SV_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_F_SV_2_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FF_SV_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FF_SV_2_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFF_SV_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFF_SV_2_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFFF_SV_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFFF_SV_2_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMF_SV_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMF_SV_2_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMFF_SV_1_1");	
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE)
	ds_list_add(possible_list,"RBS_ELF_MMM_SV_1_1");	
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMMF_SV_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE)
	ds_list_add(possible_list,"RBS_ELF_MMMM_SV_1_1");
	
return possible_list;