///scene_cg_sprites_list_clear();
/// @description
with obj_control
{
    //var key = ds_map_find_first(game_map);
	
/*    while(!is_undefined(key))
    {
        if string_count("GAME_SCENE_SPRITE_LIST", key) > 0
            ds_list_clear(game_map[? key]);
            
        key = ds_map_find_next(game_map,key);
    }*/
	
	
	
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	
	for(var i = 0; i < ds_list_size(sprite_alpha_target_list);i++)
	{
		sprite_alpha_target_list[| i] = 0;
	}
	
	
	
	var cg_alpha_target_list = obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET];
	
	for(var i = 0; i < ds_list_size(cg_alpha_target_list);i++)
	{
		cg_alpha_target_list[| i] = 0;
	}
}