///scene_sprite_list_add(ID,sprite,xnum,xnum_target,ynum,ynum_target,alpha,alpha_target,fade_speed,slide_speed,order,size_ratio);
/// @description  
/// @param ID
/// @param sprite
/// @param xnum
/// @param xnum_target
/// @param ynum
/// @param ynum_target
/// @param alpha
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
/// @param size_ratio

with obj_control
{
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	var sprite_index_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME];
	var sprite_index_target_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_TARGET_NAME];
	var sprite_x_list = obj_control.var_map[? VAR_SPRITE_LIST_X];
	var sprite_x_target_list = obj_control.var_map[? VAR_SPRITE_LIST_X_TARGET];
	var sprite_y_list = obj_control.var_map[? VAR_SPRITE_LIST_Y];
	var sprite_y_target_list = obj_control.var_map[? VAR_SPRITE_LIST_Y_TARGET];
	var sprite_alpha_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA];
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	var sprite_fade_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_FADE_SPEED];
	var sprite_slide_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_SLIDE_SPEED];
	var sprite_order_list = obj_control.var_map[? VAR_SPRITE_LIST_ORDER];
	var sprite_size_ratio_list = obj_control.var_map[? VAR_SPRITE_LIST_SIZE_RATIO];

	
	if ds_list_find_index(sprite_id_list,argument[0]) < 0
	{
		ds_list_add(sprite_id_list,argument[0]);
		if sprite_exists(argument[1])
		{
			ds_list_add(sprite_index_list,sprite_get_name(argument[1]));
			ds_list_add(sprite_index_target_list,sprite_get_name(argument[1]));
		}
		else
		{
			ds_list_add(sprite_index_list,"");
			ds_list_add(sprite_index_target_list,"");
		}
		
		ds_list_add(sprite_x_list,xnum_to_x(argument[1],argument[11],argument[2]));
		ds_list_add(sprite_x_target_list,xnum_to_x(argument[1],argument[11],argument[3]));
		ds_list_add(sprite_y_list,ynum_to_y(argument[1],argument[11],argument[4]));
		ds_list_add(sprite_y_target_list,ynum_to_y(argument[1],argument[11],argument[5]));
		ds_list_add(sprite_alpha_list,argument[6]);
		ds_list_add(sprite_alpha_target_list,argument[7]);
		ds_list_add(sprite_fade_speed_list,argument[8]);
		ds_list_add(sprite_slide_speed_list,argument[9]);
		ds_list_add(sprite_order_list,argument[10]);
		ds_list_add(sprite_size_ratio_list,argument[11]);

	}
	else
	{
		scene_sprite_list_change(argument[0],argument[1],argument[3],argument[5],argument[7],argument[8],argument[9],argument[10],argument[11]);
	}

}