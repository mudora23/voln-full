///enemy_get_sex_number_type();
/// @descirption - return the number of types of enemies the player has had sex with.
var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
var enemytype_list = ds_list_create();

for(var i = 0; i < ds_list_size(enemy_list);i++)
{
	var enemy = enemy_list[| i];
	if enemy[? GAME_CHAR_SEX]
	{
		ds_list_add_unique(enemytype_list,enemy[? GAME_CHAR_NAME_TYPE]);
	}
}

var number = ds_list_size(enemytype_list);
ds_list_destroy(enemytype_list);
return number;