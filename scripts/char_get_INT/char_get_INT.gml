///char_get_INT(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_INT);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_INT_1);
amount += char_get_skill_level(argument0,SKILL_INT_2);
amount += char_get_skill_level(argument0,SKILL_INT_3);
amount += char_get_skill_level(argument0,SKILL_INT_4);
amount += char_get_skill_level(argument0,SKILL_INT_5);
amount += char_get_skill_level(argument0,SKILL_INT_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;