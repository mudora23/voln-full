///CS_storage();
/// @description
var ally_team_size = ds_list_size(var_map_get(VAR_ALLY_LIST));
var pet_team = var_map_get(VAR_PET_LIST);
var pet_team_size = ds_list_size(pet_team);
var storage_team = var_map_get(VAR_STORAGE_LIST);
var storage_size = ds_list_size(storage_team);
var storage_ally_team_size = storage_get_ally_team_size();
var storage_pet_team_size = storage_get_pet_team_size();


_label("start");
	if (ally_team_size > 0 || pet_team_size > 0) && storage_size <= 3
		_choice("Keep","keep");
	else
		_choice("[?]Keep","keep");
	if (ally_team_size < 4 && storage_ally_team_size > 0) || (pet_team_size < 4 && storage_pet_team_size > 0)
		_choice("Take","take");
	else
		_choice("[?]Take","take");
	_choice("Exit","exit");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("keep");
	if ally_team_size > 0 && storage_size <= 3
		_choice("Keep ally","keep ally");
	else
		_choice("[?]Keep ally","keep ally");
	if pet_team_size > 0 && storage_size <= 3
		_choice("Keep pet","keep pet");
	else
		_choice("[?]Keep pet","keep pet");
	_choice("Cancel","start");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("take");
	if ally_team_size < 4 && storage_ally_team_size > 0
		_choice("Take ally","take ally");
	else
		_choice("[?]Take ally","take ally");
	if pet_team_size < 4 && storage_pet_team_size > 0
		_choice("Take pet","take pet");
	else
		_choice("[?]Take pet","take pet");
	_choice("Cancel","start");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("keep ally");
	if ally_team_size > 0 && storage_size < 4
	{
		if ally_storage_get_lock(char_get_info(ally_get_char(0),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(ally_get_char(0),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(ally_get_char(0),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(ally_get_char(0),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(ally_get_char(0),GAME_CHAR_LEVEL))+")",storage_keep_ally,0);
	}
	else
		_void();
	if ally_team_size > 1 && storage_size < 4
	{
		if ally_storage_get_lock(char_get_info(ally_get_char(1),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(ally_get_char(1),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(ally_get_char(1),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(ally_get_char(1),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(ally_get_char(1),GAME_CHAR_LEVEL))+")",storage_keep_ally,1);
	}
	else
		_void();
	if ally_team_size > 2 && storage_size < 4
	{
		if ally_storage_get_lock(char_get_info(ally_get_char(2),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(ally_get_char(2),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(ally_get_char(2),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(ally_get_char(2),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(ally_get_char(2),GAME_CHAR_LEVEL))+")",storage_keep_ally,2);
	}
	else
		_void();
	if ally_team_size > 3 && storage_size < 4
	{
		if ally_storage_get_lock(char_get_info(ally_get_char(3),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(ally_get_char(3),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(ally_get_char(3),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(ally_get_char(3),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(ally_get_char(3),GAME_CHAR_LEVEL))+")",storage_keep_ally,3);
	}
	else
		_void();
	_choice("Cancel","start");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("keep pet");
	if pet_team_size > 0 && storage_size < 4
	{
		if pet_storage_get_lock(char_get_info(pet_get_char(0),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(pet_get_char(0),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(pet_get_char(0),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(pet_get_char(0),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(pet_get_char(0),GAME_CHAR_LEVEL))+")",storage_keep_pet,0);
	}
	else
		_void();
	if pet_team_size > 1 && storage_size < 4
	{
		if pet_storage_get_lock(char_get_info(pet_get_char(1),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(pet_get_char(1),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(pet_get_char(1),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(pet_get_char(1),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(pet_get_char(1),GAME_CHAR_LEVEL))+")",storage_keep_pet,1);
	}
	else
		_void();
	if pet_team_size > 2 && storage_size < 4
	{
		if pet_storage_get_lock(char_get_info(pet_get_char(2),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(pet_get_char(2),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(pet_get_char(2),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(pet_get_char(2),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(pet_get_char(2),GAME_CHAR_LEVEL))+")",storage_keep_pet,2);
	}
	else
		_void();
	if pet_team_size > 3 && storage_size < 4
	{
		if pet_storage_get_lock(char_get_info(pet_get_char(3),GAME_CHAR_NAME))
			_choice("[?]"+char_get_info(pet_get_char(3),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,STORAGE_UNAVAILBLE);
		else
			_choice(char_get_info(pet_get_char(3),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(pet_get_char(3),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(pet_get_char(3),GAME_CHAR_LEVEL))+")",storage_keep_pet,3);
	}
	else
		_void();
	_choice("Cancel","start");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("take ally");
	if storage_ally_team_size > 0 && ally_team_size < 4
		_choice(char_get_info(storage_get_ally_char(0),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_ally_char(0),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_ally_char(0),GAME_CHAR_LEVEL))+")",storage_take_ally,0);
	else
		_void();
	if storage_ally_team_size > 1 && ally_team_size < 4
		_choice(char_get_info(storage_get_ally_char(1),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_ally_char(1),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_ally_char(1),GAME_CHAR_LEVEL))+")",storage_take_ally,1);
	else
		_void();
	if storage_ally_team_size > 2 && ally_team_size < 4
		_choice(char_get_info(storage_get_ally_char(2),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_ally_char(2),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_ally_char(2),GAME_CHAR_LEVEL))+")",storage_take_ally,2);
	else
		_void();
	if storage_ally_team_size > 3 && ally_team_size < 4
		_choice(char_get_info(storage_get_ally_char(3),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_ally_char(3),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_ally_char(3),GAME_CHAR_LEVEL))+")",storage_take_ally,3);
	else
		_void();
	_choice("Cancel","start");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("take pet");
	if storage_pet_team_size > 0 && pet_team_size < 4
		_choice(char_get_info(storage_get_pet_char(0),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_pet_char(0),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_pet_char(0),GAME_CHAR_LEVEL))+")",storage_take_pet,0);
	else
		_void();
	if storage_pet_team_size > 1 && pet_team_size < 4
		_choice(char_get_info(storage_get_pet_char(1),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_pet_char(1),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_pet_char(1),GAME_CHAR_LEVEL))+")",storage_take_pet,1);
	else
		_void();
	if storage_pet_team_size > 2 && pet_team_size < 4
		_choice(char_get_info(storage_get_pet_char(2),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_pet_char(2),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_pet_char(2),GAME_CHAR_LEVEL))+")",storage_take_pet,2);
	else
		_void();
	if storage_pet_team_size > 3 && pet_team_size < 4
		_choice(char_get_info(storage_get_pet_char(3),GAME_CHAR_DISPLAYNAME),"start","","",true,true,true,char_get_info(storage_get_pet_char(3),GAME_CHAR_DISPLAYNAME)+" (LV"+string(char_get_info(storage_get_pet_char(3),GAME_CHAR_LEVEL))+")",storage_take_pet,3);
	else
		_void();
	_choice("Cancel","start");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("exit");
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////




