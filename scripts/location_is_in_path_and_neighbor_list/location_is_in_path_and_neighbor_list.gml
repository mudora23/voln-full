///location_is_in_path_and_neighbor_list(LOCATION[optional]);
/// @description
with obj_control
{
	if argument_count > 0
		return ds_list_find_index(obj_control.var_map[? VAR_LOCATION_PATH_AND_NEIGHBOR_LIST],argument[0])>=0;
	else
		return ds_list_find_index(obj_control.var_map[? VAR_LOCATION_PATH_AND_NEIGHBOR_LIST],obj_control.var_map[? VAR_LOCATION])>=0;
}