///xnum_to_x(sprite,size_ratio,xnum);
/// @description - 9x9 position to real x position
/// @param sprite
/// @param size_ratio
/// @param xnum

//Calculate sprite width and offset
if is_string(argument0)
	var sprite_body_width = sprite_get_width(asset_get_index(argument0))*argument1;
else
	var sprite_body_width = sprite_get_width(argument0)*argument1;

var xreal_left = 0;
var xreal_right = room_width-sprite_body_width;
var xreal_jump = (xreal_right - xreal_left) / 8; // 1 = most left; 9 = most right; 8 jumps in total from left to right
var xnum_jumps = argument2 - 1;

if (xnum_jumps < 0)
    return xreal_left + sprite_body_width * xnum_jumps;
else if (xnum_jumps > 8)
    return xreal_right + sprite_body_width * (xnum_jumps - 8);
else
    return xreal_left + xreal_jump*xnum_jumps;