///SL42();
/// @description Klan Kyo

_label("start");
	if !chunk_mark_get("SL42_S_0007_Won") && !chunk_mark_get("SL42_S_0007_Loss") && chunk_mark_get("S_0007")
		_jump("SL42_S_0007");
	else
		_jump("SL42_1");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL42_S_0007");
	_execute(0,"SL42_S_0007_Battle");
	_block();
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL42_S_0007_Won");
	_qj_add("S_0007_SL1_Bandit_Beatdown_Finish_QJ");
	_dialog("SL42_S_0007_Won");
	_map();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL42_S_0007_Loss");	
	_dialog("SL42_S_0007_Loss");
	_gameover();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL42_1");	
	_map();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////