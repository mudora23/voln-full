///audio_get_index_ext(audio_in_any_form);
/// @description
/// @param audio_in_any_form
if is_string(argument0)
{
	if asset_get_type(argument0) == asset_sound
		return asset_get_index(argument0);
	else
		return noone;
}
else
{
	if audio_exists(argument0)
		return argument0;
	else
		return noone;
	
}