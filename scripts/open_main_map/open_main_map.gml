///open_main_map();

var_map_set(VAR_MODE,VAR_MODE_MAP);

scene_dialog_clear();

location_map_update_visited_and_visible_info();
location_map_update_path_and_neighbor_list();


scene_sprites_list_clear_except("bg",FADE_NORMAL_SEC);
//scene_sprite_list_add("bg",location_get_bg(),5,5,1,1,0,1,FADE_NORMAL_SEC,1,ORDER_BG,1);
scene_choices_list_clear();

if chunk_mark_get("SBS_0001_1") scene_choiceCMD_add("Stay",CSX_arrived);

with obj_map_player
{
	var location_name = obj_control.var_map[? VAR_LOCATION];
	var location_map = obj_control.location_map[? location_name];
	var location_x = location_map[? "x"];
	var location_y = location_map[? "y"];
	x = location_x;
	y = location_y;
}
with obj_main
{

	map_xoffset_target = obj_map_player.x*map_zoom_ratio - area_width/2;
	map_yoffset_target = obj_map_player.y*map_zoom_ratio - area_height/2;
	map_xoffset_target = clamp(map_xoffset_target,0,(map_total_width * map_zoom_ratio_target - area_width));
	map_yoffset_target = clamp(map_yoffset_target,0,(map_total_height * map_zoom_ratio_target - area_height));
		
	map_xoffset = map_xoffset_target;
	map_yoffset = map_yoffset_target;
}