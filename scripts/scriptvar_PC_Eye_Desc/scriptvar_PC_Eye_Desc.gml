///scriptvar_PC_Eye_Desc();
/// @description
var num = var_map_get(PC_Eye_N);
if num == 0 return choose("beady","squinty","little");
else if num == 2 return choose("big","expressive");
else if num == 3 return choose("huge","oversized");
else return "";