///BS_END_HANDLER(game_end_type);
/// @description
/// @param game_end_type
/*
obj_control.game_map[? GAME_BATTLE_END_TYPE] = argument[0];

scene_clear_ext(false,true,true,true,true);
scene_layout_normal();

game_map_replace(BATTLE_END_DISMISS_TIMER,noone);
game_map_replace(BATTLE_END_CAPTURE_TIMER,noone);

obj_control.game_map[? SCENE_BATTLE_GAME_OVER_PERCENT] = 0;
obj_control.game_map[? SCENE_BATTLE_GAME_OVER_BS_CHUNK] = "";

// scripted endings

if (argument[0] == GAME_DOMINANT_VICTORY || argument[0] == GAME_SEDUCTIVE_VICTORY) &&
	asset_get_type(obj_control.var_map[? BATTLE_WIN_SCENE]) == asset_script
	{
		scene_jump(obj_control.var_map[? BATTLE_WIN_SCENE_LABEL],obj_control.var_map[? BATTLE_WIN_SCENE]);
	}
else if (argument[0] == GAME_DOMINANT_DEFEAT || argument[0] == GAME_SEDUCTIVE_DEFEAT) &&
	asset_get_type(obj_control.game_map[? BATTLE_LOSS_SCENE]) == asset_script
	{
		scene_jump(obj_control.var_map[? BATTLE_LOSS_SCENE_LABEL],obj_control.var_map[? BATTLE_LOSS_SCENE]);
	}



else if argument[0] == GAME_DOMINANT_VICTORY
{
	var enemy_list = obj_control.game_map[? GAME_ENEMY_LIST];
    
    // gender count
    var male_old = 0;
    var female_old = 0;
    var androgynous_old = 0;
    var male = 0;
    var female = 0;
    
	for(var i = 0; i < ds_list_size(enemy_list);i++)
	{
		var enemy = enemy_list[| i];
        
        // count
        if enemy[? GAME_CHAR_GENDER] == GENDER_MALE male_old++;
        else if enemy[? GAME_CHAR_GENDER] == GENDER_FEMALE female_old++;
        else androgynous_old++;
 
        // Androgynous gender will be chosen randomly in a DV scene
        if enemy[? GAME_CHAR_GENDER] == GENDER_NONE
            enemy[? GAME_CHAR_GENDER] = choose(GENDER_FEMALE,GENDER_MALE);
        
        // count
        if enemy[? GAME_CHAR_GENDER] == GENDER_MALE male++;
        else if enemy[? GAME_CHAR_GENDER] == GENDER_FEMALE female++;
       

	}
    
    if androgynous_old > 0
    {
        var gender_text = "";
        
        // Enemy intro text
        if androgynous_old == 1
            gender_text += "        At your command, the androgynous enemy reveals its gender.\r";
        else
            gender_text += "        At your command, the androgynous enemies reveal themselves.\r";
        
        if male == male_old && female - female_old > 1
            gender_text += "        They’re gurls.\r";
        else if female == female_old && male - male_old > 1
            gender_text += "        They’re boys.\r";
        else if male - male_old == 1 && female - female_old == 1
            gender_text += "        One’s a boy, the other’s a gurl.\r";
        else if female - female_old == 1 && male - male_old > 1
            gender_text += "        One’s a gurl.  The others are boys.\r";
        else if male - male_old == 1 && female - female_old > 1
            gender_text += "        One’s a boy.  The others are gurls.\r";   

        
		scene_dialog_add(gender_text);
		
        //scene_script_execute_add_unique(0.5,scene_dialog_set_update_surface);
        //scene_script_execute_add_unique(0.6,scene_set_next_scene,"BS_END_HANDLER_2");
        scene_dialog_set_update_surface();
        scene_set_next_scene(BS_END_HANDLER_2);

    }
    else
        BS_END_HANDLER_2();


}
else if argument[0] == GAME_SEDUCTIVE_VICTORY
{
    
    BS_END_HANDLER_AUTO();
    
}
else
{
    BS_END_HANDLER_AUTO();   
}

obj_control.var_map[? BATTLE_WIN_SCENE] = "";
obj_control.var_map[? BATTLE_WIN_SCENE_LABEL] = "";
obj_control.var_map[? BATTLE_LOSS_SCENE] = "";
obj_control.var_map[? BATTLE_LOSS_SCENE_LABEL] = "";
*/