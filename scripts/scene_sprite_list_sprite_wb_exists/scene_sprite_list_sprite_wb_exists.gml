///scene_sprite_list_sprite_wb_exists();
/// @description
for(var i = 0; i < ds_list_size(obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME]);i++)
{
	var sprite_string = ds_list_find_value(obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME],i);
	if string_copy(sprite_string,string_length(sprite_string) - 2,3) == "_wb"
		return true;	
}

return false;