///S_0007_2_B_A_2_A_Battle();
/// @description
with obj_control
{
	
	scene_sprites_list_clear_except("bg",FADE_NORMAL_SEC);
	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);
	
	ally_dismiss_all();
	enemy_add_char(NAME_LORN,false,max(char_get_info(char_get_player(),GAME_CHAR_LEVEL)+1,7));
	
	
	
	var_map[? BATTLE_WIN_SCENE] = "S_0007";
	var_map[? BATTLE_WIN_SCENE_LABEL] = "S_0007_2_B_A_2_A_Battle_Won";
	var_map[? BATTLE_LOSS_SCENE] = "S_0007";
	var_map[? BATTLE_LOSS_SCENE_LABEL] = "S_0007_2_B_A_2_A_Battle_Loss";
	
	script_execute_add(0,battle_process_setup);
	//show_message("battle_process_setup in S_0007_2_B_A_2_A_Battle");
	//scene_script_execute_add(0,scene_script_battle_process_setup);
}	