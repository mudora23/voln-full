///S_0008();
/// @description

_label("start");

_label("S_0008_FELGORKUDI_JOIN");
	_dialog("S_0008_FELGORKUDI_JOIN_1");
	_show("felg",spr_char_Felg_c_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_FELGORKUDI_JOIN_2");
	_show("ku",spr_char_Ku_n_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_FELGORKUDI_JOIN_3");
	_dialog("S_0008_FELGORKUDI_JOIN_4");
	_dialog("S_0008_FELGORKUDI_JOIN_5",true);
	_choice("“Yes, I’ll go make preparations.”","Choice1");
	_choice("“No, I’m ready to travel now.”","Choice2");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("Choice1");
	_dialog("S_0008_FELGORKUDI_JOIN_6_A");
	_dialog("S_0008_FELGORKUDI_JOIN_7_A");
	_dialog("S_0008_FELGORKUDI_JOIN_8");
	_dialog("S_0008_FELGORKUDI_JOIN_9");
	_dialog("S_0008_FELGORKUDI_JOIN_10");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	if ally_exists(NAME_KUDI) && ally_exists(NAME_FELGOR) && ally_exists(NAME_ZEYD)
		_jump("MENU","SL1");
	else
		_jump("SL1_OLD_YOJI_RETURN_OPEN","SL1_OLD_YOJI");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("Choice2");
	_dialog("S_0008_FELGORKUDI_JOIN_6_B");
	_dialog("S_0008_FELGORKUDI_JOIN_7_B");
	_dialog("S_0008_FELGORKUDI_JOIN_8");
	_dialog("S_0008_FELGORKUDI_JOIN_9");
	_dialog("S_0008_FELGORKUDI_JOIN_10");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	if ally_exists(NAME_KUDI) && ally_exists(NAME_FELGOR) && ally_exists(NAME_ZEYD)
		_map();
	else
		_jump("SL1_OLD_YOJI_RETURN_OPEN","SL1_OLD_YOJI");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0008_MAKEOVER");
	_change_bg("bg",location_get_bg(),FADE_NORMAL_SEC,1.5);
	_dialog("S_0008_MAKEOVER_1");
	_dialog("S_0008_MAKEOVER_2");
	_show("felg",spr_char_Felg_c_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_MAKEOVER_3");
	_dialog("S_0008_MAKEOVER_4");
	_dialog("S_0008_MAKEOVER_5");
	_dialog("S_0008_MAKEOVER_6");
	_dialog("S_0008_MAKEOVER_7");
	_show("ku",spr_char_Ku_n_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_MAKEOVER_8");
	_dialog("S_0008_MAKEOVER_9");
	_dialog("S_0008_MAKEOVER_10");
	_dialog("S_0008_MAKEOVER_11");
	_dialog("S_0008_MAKEOVER_12");
	_dialog("S_0008_MAKEOVER_13");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0008_MAKEOVER_14");
	_show("felg",spr_char_Felg_c_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_MAKEOVER_15");
	_dialog("S_0008_MAKEOVER_16");
	_dialog("S_0008_MAKEOVER_17");
	_show("ku",spr_char_Ku_n_cs,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_execute(0,clothes_sets_set_image_index,"spr_char_Ku",1);
	_dialog("S_0008_MAKEOVER_18");
	_dialog("S_0008_MAKEOVER_19");
	_dialog("S_0008_MAKEOVER_20");
	_execute(0,location_unlock,39,40,41,8,9,11,12,13,14,18,55);
	_qj_add("S_0008_QJ");
	_map();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0008_SL12_REDSPOTTING");
	_execute(0,"S_0008_SL12_REDSPOTTING_Battle");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0008_SL12_REDSPOTTING_WON");
	_change_bg("bg",location_get_bg(),FADE_NORMAL_SEC,1.5);
	_dialog("S_0008_SL12_REDSPOTTING_1");
	_show("felg",spr_char_Felg_c_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_SL12_REDSPOTTING_2");
	_show("ku",spr_char_Ku_n_cs,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_SL12_REDSPOTTING_3");
	_dialog("S_0008_SL12_REDSPOTTING_4");
	_dialog("S_0008_SL12_REDSPOTTING_5");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0008_SL12_REDSPOTTING_6");
	_dialog("S_0008_SL12_REDSPOTTING_7");
	_jump("","CS_prelocation");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0008_SL12_REDSPOTTING_LOSS");
	_gameover();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0008_REDPASS");
	_qj_remove("S_0008_QJ");
	_change_bg("bg",spr_bg_Incomplete_01,FADE_NORMAL_SEC,1.5);
	_dialog("S_0008_REDPASS_INTRO_1");
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_INTRO_2");
	_dialog("S_0008_REDPASS_1");
	_dialog("S_0008_REDPASS_2");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0008_REDPASS_3");
	_show("redork",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_4");
	_dialog("S_0008_REDPASS_5",true);
	_choice("“We’re more durable than you know!”","S_0008_REDPASSCON");
	_choice("“I don’t take kindly to being insulted, bro.”","S_0008_REDPASSCON");
	_choice("“You’ll show proper respect when addressing me, lassie.”","S_0008_REDPASSCON");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0008_REDPASSCON");
	_dialog("S_0008_REDPASS_6");
	_dialog("S_0008_REDPASS_7");
	_dialog("S_0008_REDPASS_8");
	_dialog("S_0008_REDPASS_9");
	_dialog("S_0008_REDPASS_10");
	_movex("redork",XNUM_RIGHT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("ku",spr_char_Ku_n_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_11");
	_dialog("S_0008_REDPASS_12");
	_dialog("S_0008_REDPASS_13");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_void();//_change_bg("bg",spr_bg_Incomplete_01,FADE_NORMAL_SEC,1.5);
	_dialog("S_0008_REDPASS_14");
	_show("hac",spr_char_anon_female_b,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_15");
	_dialog("S_0008_REDPASS_16");
	_show("felg",spr_char_Felg_c_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_17");
	_dialog("S_0008_REDPASS_18");
	_dialog("S_0008_REDPASS_19");
	_dialog("S_0008_REDPASS_20");
	_dialog("S_0008_REDPASS_21");
	_dialog("S_0008_REDPASS_22");
	_dialog("S_0008_REDPASS_23");
	_hide("felg",FADE_NORMAL_SEC);
	_movex("hac",XNUM_CENTER,SMOOTH_SLIDE_NORMAL_SEC);
	_dialog("S_0008_REDPASS_24");
	_dialog("S_0008_REDPASS_25");
	_dialog("S_0008_REDPASS_26");
	_dialog("S_0008_REDPASS_27");
	_dialog("S_0008_REDPASS_28");
	_movex("hac",XNUM_RIGHT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("paya",spr_char_anon_female_b,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_29");
	_dialog("S_0008_REDPASS_30");
	_dialog("S_0008_REDPASS_31");
	_dialog("S_0008_REDPASS_32");
	_dialog("S_0008_REDPASS_33");
	_dialog("S_0008_REDPASS_34");
	_dialog("S_0008_REDPASS_35");
	_dialog("S_0008_REDPASS_36");
	_dialog("S_0008_REDPASS_37");
	_dialog("S_0008_REDPASS_38");
	_dialog("S_0008_REDPASS_39");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Camp_Tent_01,FADE_NORMAL_SEC,1.5);
	_dialog("S_0008_REDPASS_40");
	_dialog("S_0008_REDPASS_41",true);
	_choice("“I won’t pork a gurl!  I’m getting Zeyd.”","S_0008_REDPASSGAY");
	_choice("“She kawaii af, I’m tearing that poon up.”","S_0008_REDPASSSTR");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0008_REDPASSGAY");
	_show("zeyd",spr_char_Zeyd_f_n,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_GAY_1");
	_dialog("S_0008_REDPASS_GAY_2");
	_dialog("S_0008_REDPASS_GAY_3");
	_dialog("S_0008_REDPASS_GAY_4");
	_dialog("S_0008_REDPASS_GAY_5");
	_dialog("S_0008_REDPASS_GAY_6");
	_dialog("S_0008_REDPASS_GAY_7");
	_qj_add("S_0008_FINISHED_QJ");
	_var_map_set(VAR_LOCATION,LOCATION_55);
	_jump("",CS_prelocation);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0008_REDPASSSTR");
	_show("paya",spr_char_anon_female_b,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0008_REDPASS_STR_1");
	_dialog("S_0008_REDPASS_STR_2");
	_dialog("S_0008_REDPASS_STR_3");
	_dialog("S_0008_REDPASS_STR_4");
	_dialog("S_0008_REDPASS_STR_5");
	_qj_add("S_0008_FINISHED_QJ");
	_var_map_set(VAR_LOCATION,LOCATION_55);
	_jump("",CS_prelocation);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
