///var_map_clamp(KEY,MIN,MAX);
/// @description
/// @param KEY
/// @param MIN
/// @param MAX
var var_key = argument[0];
var var_min = argument[1];
var var_max = argument[2];

var var_val = var_map_get(var_key);
if var_val > var_max var_map_set(var_key,var_max);
if var_val < var_min var_map_set(var_key,var_min);