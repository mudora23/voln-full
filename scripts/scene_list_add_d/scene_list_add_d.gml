///BS_END_HANDLER_AUTO_D(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Elfs
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_F_D_1_1");

//if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
//	ds_list_add(possible_list,"RBS_ELF_FF_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFF_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFF_D_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFF_D_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_FFFF_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE)
	ds_list_add(possible_list,"RBS_ELF_M_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MFF_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MFFF_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MFFF_D_2_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE)
	ds_list_add(possible_list,"RBS_ELF_MM_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMF_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMFF_D_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE)
	ds_list_add(possible_list,"RBS_ELF_MMM_D_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ELF_MMMF_D_1_1");

if char_map_list_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE)
	ds_list_add(possible_list,"RBS_ELF_MMMM_D_1_1");
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Orks
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY)
	ds_list_add(possible_list,"RBS_ORK_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_4,GENDER_ANY)
	ds_list_add(possible_list,"RBS_ORK_D_2_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_5,GENDER_ANY)
	ds_list_add(possible_list,"RBS_ORK_D_2_1");

if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_MALE,NAME_GREEN_ORK_2,GENDER_FEMALE)
	ds_list_add(possible_list,"RBS_ORK_D_3_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY)
	ds_list_add(possible_list,"RBS_ORK_D_4_1");
	
//if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_ANY)
//	ds_list_add(possible_list,"RBS_ORK_D_5_1");
//	
//if char_map_list_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY)
//	ds_list_add(possible_list,"RBS_ORK_D_6_1");
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Imps
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
if char_map_list_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
	ds_list_add(possible_list,"RBS_IMP_D_1_1");
	
if char_map_list_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
	ds_list_add(possible_list,"RBS_IMP_D_2_1");	
	
if ally_exists(NAME_ZEYD) && char_map_list_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY) && !chunk_mark_get("RBS_IMP_D_3_1")
	ds_list_add(possible_list,"RBS_IMP_D_3_1");	
	
if ally_exists(NAME_ZEYD) && char_map_list_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY,NAME_IMP,GENDER_ANY) && chunk_mark_get("RBS_IMP_D_3_1")
	ds_list_add(possible_list,"RBS_IMP_D_4_1");	
	
	
return possible_list;