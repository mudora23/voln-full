///scene_cgs_step();
with obj_control
{
	var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	var cg_index_list = obj_control.var_map[? VAR_CG_LIST_INDEX_NAME];
	var cg_index_target_list = obj_control.var_map[? VAR_CG_LIST_INDEX_TARGET_NAME];
	var cg_x_list = obj_control.var_map[? VAR_CG_LIST_X];
	var cg_x_target_list = obj_control.var_map[? VAR_CG_LIST_X_TARGET];
	var cg_y_list = obj_control.var_map[? VAR_CG_LIST_Y];
	var cg_y_target_list = obj_control.var_map[? VAR_CG_LIST_Y_TARGET];
	var cg_alpha_list = obj_control.var_map[? VAR_CG_LIST_ALPHA];
	var cg_alpha_target_list = obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET];
	var cg_fade_speed_list = obj_control.var_map[? VAR_CG_LIST_FADE_SPEED];
	var cg_slide_speed_list = obj_control.var_map[? VAR_CG_LIST_SLIDE_SPEED];
	var cg_order_list = obj_control.var_map[? VAR_CG_LIST_ORDER];
	var cg_size_ratio_list = obj_control.var_map[? VAR_CG_LIST_SIZE_RATIO];
	
	// order sorting
	scene_cg_list_sort();
	
	if ds_list_size(cg_id_list) > 0
		obj_control.var_map[? VAR_CG_DIM_ALPHA] = approach(obj_control.var_map[? VAR_CG_DIM_ALPHA],0.7,FADE_NORMAL_SEC/room_speed);
	else
	{
		obj_control.var_map[? VAR_CG_DIM_ALPHA] = approach(obj_control.var_map[? VAR_CG_DIM_ALPHA],0,FADE_NORMAL_SEC/room_speed);
		
		ds_map_clear(obj_control.var_map[? VAR_CG_HALIGN_MAP]);
		ds_map_clear(obj_control.var_map[? VAR_CG_VALIGN_MAP]);	
	}
	
	draw_set_alpha(obj_control.var_map[? VAR_CG_DIM_ALPHA]);
	draw_set_color(c_black);
	draw_rectangle(0,0,room_width,room_height,false);
	
	
	for(var i = 0;i < ds_list_size(cg_id_list);i++)
	{
		var cg_id_i = cg_id_list[| i];
		var cg_index_i = cg_index_list[| i];
		var cg_index_target_i = cg_index_target_list[| i];
		var cg_x_i = cg_x_list[| i];
		var cg_x_target_i = cg_x_target_list[| i];
		var cg_y_i = cg_y_list[| i];
		var cg_y_target_i = cg_y_target_list[| i];
		var cg_alpha_i = cg_alpha_list[| i];
		var cg_alpha_target_i = cg_alpha_target_list[| i];
		var cg_fade_speed_i = cg_fade_speed_list[| i];
		var cg_slide_speed_i = cg_slide_speed_list[| i];
		var cg_order_i = cg_order_list[| i];
		var cg_size_ratio_i = cg_size_ratio_list[| i];
	
	
		// processing
		cg_x_list[| i] = smooth_approach_room_speed(cg_x_i,cg_x_target_i,cg_slide_speed_i);
		cg_y_list[| i] = smooth_approach_room_speed(cg_y_i,cg_y_target_i,cg_slide_speed_i);

		cg_alpha_list[| i] = approach(cg_alpha_i,cg_alpha_target_i,cg_fade_speed_i*(1/room_speed));

		if cg_index_i != cg_index_target_i
		{
			if cg_alpha_i != 0
			{
				cg_alpha_target_list[| i] = 0;
			}
			else
			{
				cg_index_list[| i] = cg_index_target_i;
				cg_alpha_list[| i] = 1;
				cg_alpha_target_list[| i] = 1;
			}
		}
		
		// drawing
		if asset_get_type(cg_index_list[| i]) == asset_sprite
		{
			draw_sprite_in_visible_area(asset_get_index(cg_index_list[| i]),-1,cg_x_list[| i],cg_y_list[| i],cg_size_ratio_i,cg_size_ratio_i,c_white,cg_alpha_list[| i],0,0,room_width,room_height);
			var spr_xx = cg_x_list[| i];
			var spr_yy = cg_y_list[| i];
			var spr_width = sprite_get_width(asset_get_index(cg_index_list[| i]))*cg_size_ratio_i;
			var spr_height = sprite_get_height(asset_get_index(cg_index_list[| i]))*cg_size_ratio_i;
			
			if !menu_exists() && mouse_over_area(spr_xx+spr_width*0.2,spr_yy,spr_width*0.6,min(spr_height,room_height-cg_y_list[| i]))
				obj_control.mouse_on_examine_sprite = asset_get_index(cg_index_list[| i]);
		}
		
		if cg_index_list[| i] != cg_index_target_list[| i]
		{
			draw_sprite_in_visible_area(asset_get_index(cg_index_target_list[| i]),-1,cg_x_list[| i],cg_y_list[| i],cg_size_ratio_i,cg_size_ratio_i,c_white,1-cg_alpha_list[| i],0,0,room_width,room_height);
			//show_debug_message("cg changing... new cg name: "+string(cg_index_target_list[| i])+" current alpha: "+string(1-cg_alpha_list[| i]));
		}
		
		// clean up
		if cg_alpha_list[| i] == 0 && cg_alpha_target_list[| i] == 0 && cg_index_i == cg_index_target_i
		{
		
			ds_list_delete(cg_id_list,i);
			ds_list_delete(cg_index_list,i);
			ds_list_delete(cg_index_target_list,i);
			ds_list_delete(cg_x_list,i);
			ds_list_delete(cg_x_target_list,i);
			ds_list_delete(cg_y_list,i);
			ds_list_delete(cg_y_target_list,i);
			ds_list_delete(cg_alpha_list,i);
			ds_list_delete(cg_alpha_target_list,i);
			ds_list_delete(cg_fade_speed_list,i);
			ds_list_delete(cg_slide_speed_list,i);
			ds_list_delete(cg_order_list,i);
			ds_list_delete(cg_size_ratio_list,i);
			
			i--;

		}

	}
	
}
