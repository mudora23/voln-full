///pet_dismiss(ID/Name);
/// @description
/// @param ID/Name
with obj_control
{
	if !is_string(argument0) var char_map = char_id_get_char(argument0);
	else var char_map = pet_get_char(argument0);
	
    var pet_list = var_map[? VAR_PET_LIST];
    var pet_r_list = var_map[? VAR_PET_R_LIST];
    var pet_index = ds_list_find_index(pet_list,char_map);
    if pet_index>=0
    {
		var new_char = ds_map_create();
		ds_map_copy(new_char,pet_list[| pet_index]);
        ds_list_add(pet_r_list,new_char);
        ds_list_mark_as_map(pet_r_list,ds_list_size(pet_r_list)-1);
        ds_list_delete(pet_list,pet_index);
    }
	
	
	
}
