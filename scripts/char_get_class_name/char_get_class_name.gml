///char_get_class_name(char);
/// @description
/// @param char
var might_stats = (char_get_STR(argument0)+char_get_FOU(argument0)+char_get_END(argument0))/3;
var toru_stats = (char_get_INT(argument0)+char_get_SPI(argument0))/2;
var stealth_stats = (char_get_DEX(argument0)+char_get_WIS(argument0))/2;
var max_stats = max(might_stats,toru_stats,stealth_stats);

if might_stats == max_stats
	return "warrior";
else if toru_stats == max_stats
	return "wizard";
else
	return "thief";

	
//return "thief";	
//return "wizard";
//return "warrior";	
	