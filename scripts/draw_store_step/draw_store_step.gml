///draw_store_step();
////show_debug_message(var_map_get(VAR_MODE)==VAR_MODE_STORE);

draw_reset();

/*var store_list_name = var_map_get(VAR_STORE_LIST_NAME);
var store_list_surface = var_map_get(VAR_STORE_LIST_SURFACE);
var store_list_alpha = var_map_get(VAR_STORE_LIST_ALPHA);
var padding = 100;
var item_width = (obj_main.area_width - padding*3)/2;
var item_height = 150;

for(var i = 0; i < ds_list_size(store_list_name);i++)
{
	if !surface_exists(store_list_surface[| i])
	{
		// create a surface every x steps
		if true//var_map_get(VAR_SCRIPT_PROC_NUMBER) % 10 == 0
		{
			if i % 2 == 0 var item_x = padding;
			else var item_x = padding * 2 + item_width;
			var item_y = padding + floor(i/2) * (item_height + padding);
			
			store_list_surface[| i] = surface_create(item_width,item_height);
			surface_set_target(store_list_surface[| i]);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_clear_alpha(c_black,0);
			gpu_set_blendmode_ext(bm_src_alpha, bm_one);
			draw_text_ext(50,50,"e qwe qwe qwe qwe qwasfasf asf qawfqwfqwfqw asf afqwrfq fqw fq fqw fasf asf qawrf af aw af",30,200);
			draw_sprite(spr_icon_player_wagon,-1,50,50);
			gpu_set_blendmode(bm_normal);
			surface_reset_target();
			break;
		}
	}
}
*/






if var_map_get(VAR_MODE) == VAR_MODE_STORE
{
	draw_reset();

	var store_list_name = var_map_get(VAR_STORE_LIST_NAME);
	var store_list_surface = var_map_get(VAR_STORE_LIST_SURFACE);
	var store_list_alpha = var_map_get(VAR_STORE_LIST_ALPHA);
	var padding = 20;
	var item_width = (obj_main.area_width - padding*3)/2;
	var item_height = 220;
	
	var max_dialog_offset = 0;


	for(var i = 0; i < ds_list_size(store_list_name);i++)
	{
		if !surface_exists(store_list_surface[| i])
		{
			// create a surface every x steps
			if true//var_map_get(VAR_SCRIPT_PROC_NUMBER) % 10 == 0
			{
				if i % 2 == 0 var item_x = padding;
				else var item_x = padding * 2 + item_width;
				var item_y = padding + floor(i/2) * (item_height + padding);
			
				store_list_surface[| i] = surface_create(item_width,item_height);
				surface_set_target(store_list_surface[| i]);
				//draw_set_alpha(1);
				//draw_set_color(c_white);
				//draw_clear_alpha(c_black,0);
				//gpu_set_blendmode_ext(bm_src_alpha, bm_one);
				draw_shop_item(store_list_name[| i],0,0,item_width,item_height);
				//gpu_set_blendmode(bm_normal);
				surface_reset_target();
				break;
			}
		}
	}




	for(var i = 0; i < ds_list_size(store_list_name);i++)
	{
		if surface_exists(store_list_surface[| i])
		{
			if i % 2 == 0 
				var item_x = padding;
			else 
				var item_x = padding * 2 + item_width;
			var item_y = padding + floor(i/2) * (item_height + padding);
			
			var hover = mouse_over_area(obj_main.x+item_x,obj_main.y+item_y-obj_main.dialog_yoffset,item_width,item_height) && mouse_over_area(obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height);
			
			ds_list_replace(var_map_get(VAR_STORE_LIST_ALPHA),i,ds_list_find_value(var_map_get(VAR_STORE_LIST_ALPHA),i)+1/room_speed);
			
			if item_get_price(store_list_name[| i]) <= var_map_get(VAR_GOLD) && hover && !menu_exists()
			{
				draw_surface_in_visible_area(store_list_surface[| i],obj_main.x+item_x,obj_main.y+item_y-obj_main.dialog_yoffset,1,1,c_white,ds_list_find_value(var_map_get(VAR_STORE_LIST_ALPHA),i),obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height);
				if action_button_pressed_get()
				{
					voln_play_sfx(sfx_wood_crate_impact);
					
					var_map_set(VAR_TEMP_ITEM,store_list_name[| i]);
					scene_choices_list_clear();
					
					if !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_FINISH") && store_list_name[| i] == ITEM_Blu_Yim
						scene_choice_add("[?]Buy \""+string(item_get_displayname(store_list_name[| i]))+"\"","CS_buying","CS_buying","",true,true,true,ITEM_UNAVAILBLE,scene_store_clear);
					else 
						scene_choice_add("Buy \""+string(item_get_displayname(store_list_name[| i]))+"\"","CS_buying","CS_buying","",true,true,true,"",scene_store_clear);

					
					if !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_FINISH") && store_list_name[| i] == ITEM_Blu_Yim
						scene_choice_add("[?]Buy \""+string(item_get_displayname(store_list_name[| i]))+"\"","CS_buying","CS_buying","",true,true,true,ITEM_UNAVAILBLE,scene_store_clear);
					else if item_get_price(store_list_name[| i]) * 10 <= var_map_get(VAR_GOLD)
						scene_choice_add("Buy \""+string(item_get_displayname(store_list_name[| i]))+"\" x10","CS_buying10","CS_buying","",true,true,true,"",scene_store_clear);
					else
						scene_choice_add("[?]Buy \""+string(item_get_displayname(store_list_name[| i]))+"\" x10","CS_buying10","CS_buying","",true,true,true,"",scene_store_clear);
					
					
					scene_choice_add("Exit","CS_exit","CS_buying","",true,true,true,"",scene_store_clear);
					
					
				}
			}
			else if item_get_price(store_list_name[| i]) > var_map_get(VAR_GOLD)
				draw_surface_in_visible_area(store_list_surface[| i],obj_main.x+item_x,obj_main.y+item_y-obj_main.dialog_yoffset,1,1,c_gray,ds_list_find_value(var_map_get(VAR_STORE_LIST_ALPHA),i),obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height);
			else
				draw_surface_in_visible_area(store_list_surface[| i],obj_main.x+item_x,obj_main.y+item_y-obj_main.dialog_yoffset,1,1,c_dkdkwhite,ds_list_find_value(var_map_get(VAR_STORE_LIST_ALPHA),i),obj_main.x,obj_main.y,obj_main.area_width,obj_main.area_height);
			
			var max_dialog_offset = max(max_dialog_offset,item_y + padding + item_height - obj_main.area_height);
			
			
		
		}
	}
	
	
	
	
			dialog_yoffset_target = clamp(dialog_yoffset_target,0,max(0,max_dialog_offset));
			dialog_yoffset = smooth_approach_room_speed(dialog_yoffset,dialog_yoffset_target,SMOOTH_SLIDE_NORMAL_SEC);
	
			// scrolling
			if !menu_exists()
			{
				if (mouse_over_area(x,y,area_width,area_height) && mouse_wheel_down()) || keyboard_check_pressed(vk_pagedown)
					dialog_yoffset_target += 150;
				if (mouse_over_area(x,y,area_width,area_height) && mouse_wheel_up()) || keyboard_check_pressed(vk_pageup)
					dialog_yoffset_target -= 150;
			}
	
	
			var scroll_spr = spr_button_scroll;
			var scroll_spr_width = sprite_get_width(scroll_spr);
			var scroll_spr_height = sprite_get_height(scroll_spr);
			if max_dialog_offset > 0
			{
				var scroll_xx = x + area_width + 22;
				var scroll_yy = y + dialog_yoffset / max_dialog_offset * (area_height-scroll_spr_height);
				var scroll_hover_r = mouse_over_area(scroll_xx,scroll_yy,scroll_spr_width,scroll_spr_height);
				if mouse_check_button_pressed(mb_left) && scroll_hover_r && !menu_exists()
					holding_scroll = true;
			
				draw_sprite_ext(spr_gui_brass_connector_c,-1,scroll_xx-5,scroll_yy+ scroll_spr_height/2,-0.3,0.5,0,c_white,image_alpha);
				draw_sprite_ext(spr_button_scroll,-1,scroll_xx,scroll_yy,1,1,0,c_white,image_alpha/2+(scroll_hover_r || holding_scroll)*image_alpha/2);
			
				var scroll_xx = x - 22;
				var scroll_hover_l = mouse_over_area(scroll_xx-scroll_spr_width,scroll_yy,scroll_spr_width,scroll_spr_height);
				if mouse_check_button_pressed(mb_left) && scroll_hover_l && !menu_exists()
					holding_scroll = true;
				
				draw_sprite_ext(spr_gui_brass_connector_c,-1,scroll_xx+5,scroll_yy+ scroll_spr_height/2,-0.3,0.5,0,c_white,image_alpha);
				draw_sprite_ext(spr_button_scroll,-1,scroll_xx,scroll_yy,-1,1,0,c_white,image_alpha/2+(scroll_hover_l || holding_scroll)*image_alpha/2);
			
				if holding_scroll
				{
					var scroll_ratio = clamp((mouse_y - (y+scroll_spr_height/2))/(area_height-scroll_spr_height),0,1);
					dialog_yoffset = max_dialog_offset * scroll_ratio;
					dialog_yoffset_target = dialog_yoffset;
				}
			
			
			
				if !mouse_check_button(mb_left)
					holding_scroll = false;
			}
	
	
	
	
	
	
	
	
	

}
