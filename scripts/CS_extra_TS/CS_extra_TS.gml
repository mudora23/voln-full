///CS_extra_TS();
/// @description

_label("start");

_label("Travelscene");

	_var_map_set(VAR_EXTRA_AFTER_SCENE,"CS_prelocation");

	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"");
	_ds_list_clear(var_map_get(VAR_ENEMY_LIST));
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else if asset_get_type(var_map_get(VAR_TEMP_SCENE)) == asset_script
		_jump("",var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedtype");
	_execute(0,"scene_set_from_type");
	_jump("Selectedscene");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedscene");
	_ds_list_clear(var_map_get(VAR_ENEMY_LIST));
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectednoscene");
_label("Noscenestoselect");
	_ds_list_clear(var_map_get(VAR_ENEMY_LIST));
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafter");	
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
	// TS
	_border(1000); // add lines after this
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

_label("TS_ZOMBIE_MISTRESS");
	_dialog("TS_ZOMBIE_MISTRESS",true);
	_choice("“This won’t go well for me, will it?”","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_ZOMBIE_MISTRESS_A");
	_choice("“Uh… can we talk?”","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_ZOMBIE_MISTRESS_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_ZOMBIE_MISTRESS_A");
	_dialog("TS_ZOMBIE_MISTRESS_A");
	_execute(0,"TS_ZOMBIE_MISTRESS_A_BATTLE");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_ZOMBIE_MISTRESS_A_BATTLE_WON");
	_dialog("TS_ZOMBIE_MISTRESS_A_BATTLE_WON");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_ZOMBIE_MISTRESS_A_BATTLE_LOSS");
	_dialog("TS_ZOMBIE_MISTRESS_A_BATTLE_LOSS");
	_gameover();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_ZOMBIE_MISTRESS_B");
	_dialog("TS_ZOMBIE_MISTRESS_B",true);
	_choice("“My thirst drove me here, in search of water.”","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_ZOMBIE_MISTRESS_B_A");
	_choice("“You misunderstand.  I came here to meet an oro like you.”","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_ZOMBIE_MISTRESS_B_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Mushroom");
	_dialog("TS_Mushroom",true);
	_choice("Pick it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Mushroom_A");
	_choice("Don’t pick it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Mushroom_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Mushroom_A");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_MUSHROOM_KORKORT,1);
	_dialog("        You receive "+string(item_get_displayname(ITEM_MUSHROOM_KORKORT))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("TS_Mushroom_A");
	_jump("Selectedsceneafter");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Mushroom2");
	_dialog("TS_Mushroom2",true);
	_choice("Keep it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Mushroom2_A");
	_choice("Don’t keep it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Mushroom2_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Mushroom2_A");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_MUSHROOM_LIRROT,1);
	_dialog("        You receive "+string(item_get_displayname(ITEM_MUSHROOM_LIRROT))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("TS_Mushroom2_A");
	_jump("Selectedsceneafter");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Mushroom4");
	_dialog("TS_Mushroom4",true);
	_choice("Pick it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Mushroom4_A");
	_choice("Don’t pick it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Mushroom4_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Mushroom4_A");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_MUSHROOM_OLKE,1);
	_dialog("        You receive "+string(item_get_displayname(ITEM_MUSHROOM_OLKE))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("TS_Mushroom4_A");
	_jump("Selectedsceneafter");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Elf_and_Imp_Orgy");
	_dialog("TS_Elf_and_Imp_Orgy_1");
	_dialog("TS_Elf_and_Imp_Orgy_2",true);
	_choice("“Sneak away”","TS_Elf_and_Imp_Orgy_3");
	_choice("“Join the fun”","TS_Elf_and_Imp_Orgy_C");
	_block();
	
	_border(10); // add lines after this
	
	_label("TS_Elf_and_Imp_Orgy_3");
		if success_roll(char_get_skill_level(char_get_player(),SKILL_DEX_1)+char_get_skill_level(char_get_player(),SKILL_DEX_2)+char_get_skill_level(char_get_player(),SKILL_DEX_3)+char_get_skill_level(char_get_player(),SKILL_DEX_4)+char_get_skill_level(char_get_player(),SKILL_DEX_5)+char_get_skill_level(char_get_player(),SKILL_DEX_6))
			_jump("TS_Elf_and_Imp_Orgy_3_Success");
		else
			_jump("TS_Elf_and_Imp_Orgy_3_Fail");
			
	_border(10); // add lines after this
	
	_label("TS_Elf_and_Imp_Orgy_3_Fail");
		_dialog("TS_Elf_and_Imp_Orgy_3_Fail",true);
		_choice("Flee","TS_Elf_and_Imp_Orgy_A");
		_choice("Fight","TS_Elf_and_Imp_Orgy_B");
		_choice("Fikk","TS_Elf_and_Imp_Orgy_C");
		_block();

	_border(10); // add lines after this

	_label("TS_Elf_and_Imp_Orgy_A");
		_dialog("TS_Elf_and_Imp_Orgy_A");
		_execute(0,"TS_Elf_and_Imp_Orgy_A_BATTLE");
		_block();
		
	_border(10); // add lines after this	
		
	_label("TS_Elf_and_Imp_Orgy_A_Loss");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_1");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_2");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_3");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_4");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_5");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_6");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_7");
		if ally_exists(NAME_ZEYD) || char_get_char_opinion(NAME_ZEYD,char_get_player()) < 1 // if zeyd is beaten already or not friends with the player
			_gameover();
		else
			_jump("TS_Elf_and_Imp_Orgy_A_Loss_8");
			
	_border(10); // add lines after this	
	
	_label("TS_Elf_and_Imp_Orgy_A_Loss_8");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_8",true);
		_choice("“Get me out of here!“","TS_Elf_and_Imp_Orgy_A_Loss_9A");
		_choice("“Leave me.  I’m happy here.“","TS_Elf_and_Imp_Orgy_A_Loss_9B");
		_choice("“Join me.”","TS_Elf_and_Imp_Orgy_A_Loss_9C");
		
	_border(10); // add lines after this		
		
	_label("TS_Elf_and_Imp_Orgy_A_Loss_9A");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_9A");
		_gameover();
		
	_border(10); // add lines after this	
		
	_label("TS_Elf_and_Imp_Orgy_A_Loss_9B");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_9B_1");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_9B_2");
		_gameover();	
		
	_border(10); // add lines after this	
		
	_label("TS_Elf_and_Imp_Orgy_A_Loss_9C");
		_dialog("TS_Elf_and_Imp_Orgy_A_Loss_9C");
		_gameover();	
		
	_border(10); // add lines after this

	_label("TS_Elf_and_Imp_Orgy_A_Won");
		_dialog("TS_Elf_and_Imp_Orgy_A_Won");
		_jump("Selectedsceneafter");

	_border(10); // add lines after this

	_label("TS_Elf_and_Imp_Orgy_B");
		_dialog("TS_Elf_and_Imp_Orgy_B");
		_execute(0,"TS_Elf_and_Imp_Orgy_B_BATTLE");
		_block();
	
	_border(10); // add lines after this

	_label("TS_Elf_and_Imp_Orgy_B_Won");
		_dialog("TS_Elf_and_Imp_Orgy_B_Won");
		_jump("Selectedsceneafter");

	_border(10); // add lines after this

	_label("TS_Elf_and_Imp_Orgy_B_Loss");
		_dialog("TS_Elf_and_Imp_Orgy_B_Loss_1");
		_dialog("TS_Elf_and_Imp_Orgy_B_Loss_2");
		_dialog("TS_Elf_and_Imp_Orgy_B_Loss_3");
		_dialog("TS_Elf_and_Imp_Orgy_B_Loss_4");
		_dialog("TS_Elf_and_Imp_Orgy_B_Loss_5");
		_dialog("TS_Elf_and_Imp_Orgy_B_Loss_6");
		_gameover();
		
	_border(10); // add lines after this	
		
	_label("TS_Elf_and_Imp_Orgy_C");
		_dialog("TS_Elf_and_Imp_Orgy_C_1");
		_dialog("TS_Elf_and_Imp_Orgy_C_2");
		_dialog("TS_Elf_and_Imp_Orgy_C_3");
		_dialog("TS_Elf_and_Imp_Orgy_C_4");
		_dialog("TS_Elf_and_Imp_Orgy_C_5");
		_dialog("TS_Elf_and_Imp_Orgy_C_6");
		_dialog("TS_Elf_and_Imp_Orgy_C_7");
		_dialog("TS_Elf_and_Imp_Orgy_C_8");
		_jump("Selectedsceneafter");
		
	_border(10); // add lines after this	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice");
	_jump("TS_Love_Advice","TS_Love_Advice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2");
	_jump("TS_Love_Advice2","TS_Love_Advice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Item_Find");
	_dialog("TS_Item_Find",true);
	_choice("Forget it","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Item_Find_A");
	_choice("Search the hole","TS_Item_Find_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Item_Find_B");
	if percent_chance(20)
		_jump("TS_Item_Find_B1");
	else if percent_chance(40)
		_jump("TS_Item_Find_B2");
	else if percent_chance(60)
		_jump("TS_Item_Find_B3");
	else if percent_chance(80)
		_jump("TS_Item_Find_B4");
	else if percent_chance(100)
		_jump("TS_Item_Find_B5");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Item_Find_B1");
	_dialog("TS_Item_Find_B1");
	_main_gui(VAR_GUI_MIN);
	_execute(0,var_map_add,VAR_GOLD,10000);
	_execute(0,voln_play_sfx,sfx_get_gold);
	_dialog("Gold received: 10,000");
	_main_gui(VAR_GUI_NORMAL);
	_dialog("TS_Item_Find_B1_3");
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Item_Find_B2");
	_dialog("TS_Item_Find_B2");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_REDBERRIES_JARRED,1);
	_dialog("        You receive "+string(item_get_displayname(ITEM_REDBERRIES_JARRED))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("TS_Item_Find_B2_3");
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Item_Find_B3");
	_dialog("TS_Item_Find_B3");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_LEWD_BANDIT_COSTUME,1);
	_dialog("        You receive "+string(item_get_displayname(ITEM_LEWD_BANDIT_COSTUME))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("TS_Item_Find_B3_3");
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Item_Find_B4");
	_dialog("TS_Item_Find_B4");
	_execute(0,"TS_Item_Find_B4_3_Battle");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Item_Find_B5");
	_dialog("TS_Item_Find_B5");
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Tonic_Find");
	_dialog("TS_Tonic_Find",true);
	_choice("Keep it","TS_Tonic_Find_A");
	_choice("Leave it","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Tonic_Find_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Tonic_Find_A");
	_dialog("TS_Tonic_Find_A");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_DEENSKIP_TONIC_GOOD,1);
	_dialog("        You receive "+string(item_get_displayname(ITEM_DEENSKIP_TONIC_GOOD))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_dialog("TS_Tonic_Find_A_3");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Tonic_Find_B");
	_dialog("TS_Tonic_Find_B");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Plant_Sex");
	_dialog("TS_Plant_Sex",true);
	_choice("Yes","TS_Plant_Sex_B");
	_choice("No","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Plant_Sex_A");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Plant_Sex_B");
	_dialog("TS_Plant_Sex_B",true);
	_choice("No, thanks.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Plant_Sex_B_A");
	_choice("Hump it.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"TS_Plant_Sex_B_B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Plant_Sex_B_B");
	_dialog("TS_Plant_Sex_B_B_1");
	_dialog("TS_Plant_Sex_B_B_2");
	_execute(0,player_ally_relieve_all);
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Kudi_and_Elk");
	_dialog("TS_Kudi_and_Elk_1");
	_dialog("TS_Kudi_and_Elk_2");
	_dialog("TS_Kudi_and_Elk_3");
	_dialog("TS_Kudi_and_Elk_4");
	_dialog("TS_Kudi_and_Elk_5");
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


