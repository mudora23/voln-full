///skill_add_missing_keys(skill_map);
/// @description
/// @param skill_map
with obj_control
{
	var skill_map = argument0;
	ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"");
	ds_map_add_unique(skill_map,SKILLDES,"");
	ds_map_add_unique(skill_map,SKILLICON,spr_button_start);
	ds_map_add_unique(skill_map,SKILLCATEGORY,"");
	ds_map_add_unique(skill_map,SKILLTEXT,"");
	ds_map_add_unique(skill_map,SKILLTEXTCOL,c_white);
	ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,c_white);
	//ds_map_add_unique(skill_map,SKILLX,0); // no x,y means not in the skill tree
	//ds_map_add_unique(skill_map,SKILLY,0); // no x,y means not in the skill tree
	ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
	ds_map_add_unique_list(skill_map,SKILL_VITALITY_LIST,ds_list_create());
	ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
	ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
}