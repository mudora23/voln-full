///battle_char_get_effective_hp_ratio(char);
/// @description 
/// @param char

var char = argument0;
var hp_ratio = char[? GAME_CHAR_HEALTH] / char_get_health_max(char);
var armor = (char_get_phy_armor(char) + char_get_toru_armor(char))/2;
var real_hp_ratio = hp_ratio * (armor/100) * (char_get_dodge_chance(char)/100);

return real_hp_ratio;

