///skill_get_level_max(skill);
/// @description
/// @param skill
var skill_map = obj_control.skill_wiki_map[? argument0];
if ds_map_exists(skill_map,SKILL_REQ_LEVEL_LIST) return ds_list_size(skill_map[? SKILL_REQ_LEVEL_LIST]);
else return 1;