///script_execute_ext(script,arg0,arg1...);
/// @description
/// @param script
/// @param arg0
/// @param arg1...
if argument_count > 0 && script_get_index_ext(argument[0]) == noone
{
	//show_debug_message("script_execute_ext... script not vaild. script: "+string(argument[0]));
	//exit;
}

else if argument_count > 15 && arg_exists(argument[15])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14],argument[15]);
else if argument_count > 14 && arg_exists(argument[14])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14]);
else if argument_count > 13 && arg_exists(argument[13])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13]);
else if argument_count > 12 && arg_exists(argument[12])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12]);
else if argument_count > 11 && arg_exists(argument[11])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
else if argument_count > 10 && arg_exists(argument[10])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10]);
else if argument_count > 9 && arg_exists(argument[9])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9]);
else if argument_count > 8 && arg_exists(argument[8])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8]);
else if argument_count > 7 && arg_exists(argument[7])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7]);
else if argument_count > 6 && arg_exists(argument[6])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5],argument[6]);
else if argument_count > 5 && arg_exists(argument[5])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4],argument[5]);
else if argument_count > 4 && arg_exists(argument[4])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3],argument[4]);
else if argument_count > 3 && arg_exists(argument[3])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2],argument[3]);   
else if argument_count > 2 && arg_exists(argument[2])
	script_execute(script_get_index_ext(argument[0]),argument[1],argument[2]);     
else if argument_count > 1 && arg_exists(argument[1])
	script_execute(script_get_index_ext(argument[0]),argument[1]);        
else if argument_count > 0
	script_execute(script_get_index_ext(argument[0]));   




