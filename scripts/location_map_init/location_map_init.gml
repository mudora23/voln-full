///location_map_init();
/// @description - this map will be not modiflied in game.
with obj_control
{
    location_map = ds_map_create();
	
	#macro REGION_DWI "Dwi Region"
	#macro REGION_SESKA "Seska Region"
	#macro REGION_KLORV "Klorv Region"
	#macro REGION_FILSA_TAR "Filsa Tąŕ Region"
	#macro REGION_HUDDARI "Huddąri Region"
	#macro REGION_MARA_HAM "Mąra Hąm Region"
	#macro REGION_KRIM "Krim Region"
	#macro REGION_DAR_SANDE "Dąŕ Sąnde Region"
	#macro REGION_JEVA "Jėvä Region"
	#macro REGION_SEGA_SARA "Sėga Sąŕa"
	#macro REGION_RAYN "Rąyn Region"
	
	#macro LOCATION_1 "LOCATION_1"
	#macro LOCATION_2 "LOCATION_2"
	#macro LOCATION_3 "LOCATION_3"
	#macro LOCATION_4 "LOCATION_4"
	#macro LOCATION_5 "LOCATION_5"
	#macro LOCATION_6 "LOCATION_6"
	#macro LOCATION_7 "LOCATION_7"
	#macro LOCATION_8 "LOCATION_8"
	#macro LOCATION_9 "LOCATION_9"
	#macro LOCATION_10 "LOCATION_10"
	#macro LOCATION_11 "LOCATION_11"
	#macro LOCATION_12 "LOCATION_12"
	#macro LOCATION_13 "LOCATION_13"
	#macro LOCATION_14 "LOCATION_14"
	#macro LOCATION_15 "LOCATION_15"
	#macro LOCATION_16 "LOCATION_16"
	#macro LOCATION_17 "LOCATION_17"
	#macro LOCATION_18 "LOCATION_18"
	#macro LOCATION_19 "LOCATION_19"
	#macro LOCATION_20 "LOCATION_20"
	#macro LOCATION_21 "LOCATION_21"
	#macro LOCATION_22 "LOCATION_22"
	#macro LOCATION_23 "LOCATION_23"
	#macro LOCATION_24 "LOCATION_24"
	#macro LOCATION_25 "LOCATION_25"
	#macro LOCATION_26 "LOCATION_26"
	#macro LOCATION_27 "LOCATION_27"
	#macro LOCATION_28 "LOCATION_28"
	#macro LOCATION_29 "LOCATION_29"
	#macro LOCATION_30 "LOCATION_30"
	#macro LOCATION_31 "LOCATION_31"
	#macro LOCATION_32 "LOCATION_32"
	#macro LOCATION_33 "LOCATION_33"
	#macro LOCATION_34 "LOCATION_34"
	#macro LOCATION_35 "LOCATION_35"
	#macro LOCATION_36 "LOCATION_36"
	#macro LOCATION_37 "LOCATION_37"
	#macro LOCATION_38 "LOCATION_38"
	#macro LOCATION_39 "LOCATION_39"
	#macro LOCATION_40 "LOCATION_40"
	#macro LOCATION_41 "LOCATION_41"
	#macro LOCATION_42 "LOCATION_42"
	#macro LOCATION_43 "LOCATION_43"
	#macro LOCATION_44 "LOCATION_44"
	#macro LOCATION_45 "LOCATION_45"
	#macro LOCATION_46 "LOCATION_46"
	#macro LOCATION_47 "LOCATION_47"
	#macro LOCATION_48 "LOCATION_48"
	#macro LOCATION_49 "LOCATION_49"
	#macro LOCATION_50 "LOCATION_50"
	#macro LOCATION_51 "LOCATION_51"
	#macro LOCATION_52 "LOCATION_52"
	#macro LOCATION_53 "LOCATION_53"
	#macro LOCATION_54 "LOCATION_54"
	#macro LOCATION_55 "LOCATION_55"
	#macro LOCATION_56 "LOCATION_56"
	#macro LOCATION_57 "LOCATION_57"
	
	location_map_add_location(LOCATION_1,1605,853,false,MARKER_GREEN,"Nirholt",REGION_DWI,spr_bg_Nirholt_Distant_01_midsun);
	location_map_add_location(LOCATION_2,1859,765,false,MARKER_YELLOW,"Nir Feld",REGION_DWI,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_3,1717,919,false,MARKER_YELLOW,"Dleyn",REGION_DWI,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_4,1817,989,false,MARKER_YELLOW,"Krąna Prat",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_5,1945,1141,false,MARKER_YELLOW,"Krąna Glen",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_6,2109,1095,false,MARKER_YELLOW,"Suŋk Nir",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_7,2285,1140,false,MARKER_YELLOW,"Suŋk Deep",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_8,2257,1292,false,MARKER_YELLOW,"Dweyni",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_9,2357,1386,false,MARKER_YELLOW,"Lor Pek",REGION_MARA_HAM,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_10,2585,1438,true,MARKER_YELLOW,"Post Mėla",REGION_MARA_HAM,spr_bg_Forest_Deep_01_midsun);
	location_map_add_location(LOCATION_11,2647,1160,false,MARKER_YELLOW,"Lor Embi",REGION_MARA_HAM,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_12,2847,1030,false,MARKER_YELLOW,"Silstra",REGION_MARA_HAM,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_13,3041,860,false,MARKER_YELLOW,"Hirdri",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_14,3306,675,false,MARKER_RED,"Redwildər Vä",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_15,3194,505,true,MARKER_RED,"Redwildər Gąr",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_16,3218,247,false,MARKER_RED,"Redwildər Delta",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_17,3332,347,false,MARKER_RED,"Redwildər Blûn",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_18,3662,571,true,MARKER_RED,"Redwildər Port",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_19,3110,1052,false,MARKER_RED,"Redwildər Årbi",REGION_KRIM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_20,2934,1170,false,MARKER_RED,"Redwildər Dwi",REGION_MARA_HAM,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_21,3066,1238,false,MARKER_RED,"Grėju",REGION_DAR_SANDE,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_22,3066,1396,false,MARKER_RED,"Dąŕ Hąn",REGION_JEVA,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_23,3022,1638,false,MARKER_RED,"Hüŕi",REGION_JEVA,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_24,2820,1744,false,MARKER_RED,"Tąrf",REGION_SEGA_SARA,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_25,2794,1600,true,MARKER_PINK,"Hova",REGION_SEGA_SARA,spr_bg_Cave_Interior_01_midsun);
	location_map_add_location(LOCATION_26,2718,1905,false,MARKER_RED,"Pąrsus Pąk",REGION_HUDDARI,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_27,2570,1903,false,MARKER_RED,"Lirig Nås",REGION_SEGA_SARA,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_28,2458,1989,false,MARKER_RED,"Dėfu",REGION_HUDDARI,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_29,2492,2165,false,MARKER_RED,"Bąrjor Nąl",REGION_HUDDARI,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_30,2442,2315,false,MARKER_GREEN,"Mėdnu",REGION_HUDDARI,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_31,2328,1843,false,MARKER_RED,"Miŋksi",REGION_HUDDARI,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_32,2177,1907,false,MARKER_RED,"Tor Sembus",REGION_FILSA_TAR,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_33,2084,2000,false,MARKER_RED,"Tor Borholt",REGION_FILSA_TAR,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_34,1964,1915,false,MARKER_RED,"Änin Feld",REGION_KLORV,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_35,1900,2027,false,MARKER_RED,"Tor Tąndər",REGION_KLORV,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_36,1730,2000,false,MARKER_YELLOW,"Pąm Dląk",REGION_KLORV,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_37,1650,1885,false,MARKER_GREEN,"Giş",REGION_KLORV,spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_38,1512,2067,true,MARKER_PINK,"Dlu Feld",REGION_KLORV,spr_bg_Cave_Interior_01_midsun);
	location_map_add_location(LOCATION_39,1826,1767,false,MARKER_YELLOW,"Kwinṡ",REGION_KLORV,spr_bg_Forest_Deep_01_midsun);
	location_map_add_location(LOCATION_40,2016,1639,false,MARKER_YELLOW,"Rummuk åy",REGION_KLORV,spr_bg_Forest_Deep_01_midsun);
	location_map_add_location(LOCATION_41,2162,1525,false,MARKER_YELLOW,"Rund",REGION_MARA_HAM,spr_bg_Forest_Deep_01_midsun);
	location_map_add_location(LOCATION_42,1914,1521,true,MARKER_PINK,"Klan Kyo",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_43,1836,1305,false,MARKER_YELLOW,"Tweyn Ki",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_44,1776,1443,false,MARKER_YELLOW,"Tweyn Bąl",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_45,1616,1503,false,MARKER_YELLOW,"Tweyn Mir",REGION_SESKA,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_46,1602,1649,false,MARKER_YELLOW,"Hitsmit",REGION_KLORV,spr_bg_Forest_Deep_01_midsun);
	location_map_add_location(LOCATION_47,1320,1781,false,MARKER_YELLOW,"Råyn Mir",REGION_RAYN,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_48,1076,1927,false,MARKER_YELLOW,"Tor Jin",REGION_RAYN,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_49,1024,2147,false,MARKER_YELLOW,"Råyn Feld",REGION_RAYN,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_50,892,2295,false,MARKER_GREEN,"Jaksie Råyn",REGION_RAYN,spr_bg_Forest_Brook_01_midsun);
	location_map_add_location(LOCATION_51,470,1347,true,MARKER_WHITE,"Slint","","");
	location_map_add_location(LOCATION_52,3411,959,true,MARKER_WHITE,"Drėsos","","");
	location_map_add_location(LOCATION_53,1266,1237,true,MARKER_WHITE,"Tąrka Nąl","","");
	location_map_add_location(LOCATION_54,3671,2141,true,MARKER_WHITE,"Omu","","");
	location_map_add_location(LOCATION_55,3753,805,true,MARKER_YELLOW,"Rivərvelt","",spr_bg_Forest_Ruins_01_midsun);
	location_map_add_location(LOCATION_56,4277,1589,true,MARKER_WHITE,"Iru Deep","","");
	location_map_add_location(LOCATION_57,1286,316,true,MARKER_WHITE,"Klan Mirzåy","","");
    //location_map_add_location(LOCATION_57,1286,316,true,MARKER_WHITE,"Gėsta","","");
	location_map_add_location(LOCATION_59,3937,759,true,MARKER_GREEN,"Nåyr","",spr_bg_Dungeon_01); #macro LOCATION_59 "LOCATION_59"
	location_map_add_location(LOCATION_60,3866,596,true,MARKER_YELLOW,"Nordrivər","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_60 "LOCATION_60"
	location_map_add_location(LOCATION_61,3662,905,true,MARKER_YELLOW,"Nir Mąr","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_61 "LOCATION_61"
	location_map_add_location(LOCATION_62,3600,1074,true,MARKER_YELLOW,"Mid Mąr","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_62 "LOCATION_62"
	location_map_add_location(LOCATION_63,3520,1202,true,MARKER_YELLOW,"Deep Mąr","",spr_bg_Forest_Nest_01_midsun); #macro LOCATION_63 "LOCATION_63"
	location_map_add_location(LOCATION_64,3478,1328,true,MARKER_YELLOW,"Vlennər Nir","",spr_bg_Forest_Ruins_01_midsun); #macro LOCATION_64 "LOCATION_64"
	location_map_add_location(LOCATION_65,3410,1451,true,MARKER_YELLOW,"Vlennər Plut","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_65 "LOCATION_65"
	location_map_add_location(LOCATION_66,3500,1608,true,MARKER_YELLOW,"Post Mendo","",spr_bg_Forest_Ruins_01_midsun); #macro LOCATION_66 "LOCATION_66"
	location_map_add_location(LOCATION_67,3470,1915,true,MARKER_YELLOW,"Tovi Plut","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_67 "LOCATION_67"
	location_map_add_location(LOCATION_68,3412,2100,true,MARKER_YELLOW,"Tovi Feld","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_68 "LOCATION_68"
	location_map_add_location(LOCATION_69,3104,2180,true,MARKER_YELLOW,"Forkrivər Bell","",spr_bg_Forest_Brook_01_midsun); #macro LOCATION_69 "LOCATION_69"
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}