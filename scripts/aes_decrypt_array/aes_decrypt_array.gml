///aes_decrypt_array(stringArray,keyArray)
var state_t, state, RoundKey,
    key = (argument[1]), plaintext = (argument[0]),
    KEYLEN = array_length_1d(key), PTLEN = array_length_1d(plaintext),
    remainder = PTLEN mod KEYLEN, input = array_create(KEYLEN),
    RoundKey = array_create(176), Iv = array_duplicate(global._aes_IV),
    output, input_unsquare, input_square, i;

if (KEYLEN != global._aes_KEYLEN)
    show_error("Invalid Key Length", true);
if (!is_array(key) || !is_array(plaintext))
    show_error("Only AES arrays can be passed to the decrypt function", true);
    
//pad - should not happen in DC mode
while ((array_length_1d(plaintext) mod KEYLEN) != 0) {
    plaintext[@ array_length_1d(plaintext)] = ord(" ");
}   
PTLEN = array_length_1d(plaintext);
output = array_create(PTLEN);

_aes_KeyExpansion(RoundKey, key);

for(i = 0; i < PTLEN; i += KEYLEN)
{
    array_copy(input, 0, plaintext, i, KEYLEN);
    
    input_square = _aes_BlockCopy(input);   
    _aes_InvCipher(input_square, RoundKey);
    input_unsquare = _aes_unBlockCopy(input_square);
    
    _aes_XorWithIv(input_unsquare, Iv); 
    
    array_copy(output, i, input_unsquare, 0, KEYLEN);
    Iv = array_duplicate(input); 
}

return output;

