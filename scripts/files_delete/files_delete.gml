///files_delete(root,whitelist)
//arg1 whitelist is map with keys as filenames

var f = file_find_first(argument0 + "\\*", 0), fname, a1=argument1, a0=argument0;

while (f != "") {
    if (!ds_map_exists(argument1, argument0 + "/" + f))
        if (file_exists(working_directory + argument0 + "/" + f))
            file_delete(working_directory + argument0 + "/" + f);
    f = file_find_next();
}
file_find_close();


