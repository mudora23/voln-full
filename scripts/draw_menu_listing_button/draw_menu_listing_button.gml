///draw_menu_listing_button(x1,y1,x2,y2,listing_name,idle_bg_colorscheme,idle_text_colorscheme,selected_bg_colorscheme,selected_text_colorscheme,enable);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param listing_name
/// @param idle_bg_colorscheme
/// @param idle_text_colorscheme
/// @param selected_bg_colorscheme
/// @param selected_text_colorscheme

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var listing_name = argument4;
var menu_idle_bg_colorscheme = argument5;
var menu_idle_text_colorscheme = argument6;
var menu_selected_bg_colorscheme = argument7;
var menu_selected_text_colorscheme = argument8;
var enable = argument9;
var hover = mouse_over_area(x1,y1,x2-x1,y2-y1);
if !enable
{
    var menu_bg_colorscheme = menu_idle_bg_colorscheme;
    var menu_text_colorscheme = c_dkgray;
}
else if menu_open == listing_name || hover
{
    var menu_bg_colorscheme = menu_selected_bg_colorscheme;
    var menu_text_colorscheme = menu_selected_text_colorscheme;
}
else
{
    var menu_bg_colorscheme = menu_idle_bg_colorscheme;
    var menu_text_colorscheme = menu_idle_text_colorscheme;
}
if enable && hover && action_button_pressed_get() && !inventory_using_item
{
    action_button_pressed_set(false);

	voln_play_sfx(sfx_mouse_click);
    menu_open = listing_name;
    menu_open_bg_image_alpha = 0;
    menu_open_text_image_alpha = 0;
	
	inventory_surface = noone;
	inventory_creating_step = 0;
	inventory_yoffset = 0;
	inventory_yoffset_target = 0;
}
draw_set_color(menu_bg_colorscheme);
draw_set_alpha(menu_bg_image_alpha);
//draw_rectangle(x1,y1,x2,y2, false);
draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
draw_set_color(menu_text_colorscheme);
draw_set_alpha(menu_text_image_alpha);
draw_text_transformed(x1+25, (y1 + y2) / 2,listing_name,0.6,0.6,0);


if hover && !enable && listing_name == MENULISTING_SAVE
	tooltip = "Saving is disabled at the moment";
else if hover && listing_name == MENULISTING_SAVE
	tooltip = "Save the game";
else if hover && listing_name == MENULISTING_LOAD
	tooltip = "Load a game";
else if hover && listing_name == MENULISTING_ITEMS
	tooltip = "Inventory";
else if hover && listing_name == MENULISTING_APPEARANCE
	tooltip = "Character's appearance";
else if hover && listing_name == MENULISTING_ATTRIBUTES_AND_SKILLS
	tooltip = "Character's attributes and skills";
else if hover && listing_name == MENULISTING_ALLIES
	tooltip = "Allies' information";
else if hover && listing_name == MENULISTING_PETS
	tooltip = "Pets' information";
else if hover && listing_name == MENULISTING_RESUME
	tooltip = "Resumne the game";
else if hover && listing_name == MENULISTING_OPTIONS
	tooltip = "Customize the game";
else if hover && listing_name == MENULISTING_EXIT
	tooltip = "Exit the game";
else if hover && listing_name == MENULISTING_QUEST_JOURNAL
	tooltip = "The quest journal";