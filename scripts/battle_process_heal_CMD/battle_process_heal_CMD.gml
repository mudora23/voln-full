///battle_process_heal_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();

			
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	
	skill_spend_cost(char_source,SKILL_HEAL,char_get_skill_level(char_source,SKILL_HEAL));
	

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" heals",TAG_FONT_N,TAG_COLOR_HEAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));

	// performing
	var dodge_chance = 0;
	var hit_chance = 100;
	
	//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_TORU);
	
	//ps_fireball(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
	
	if percent_chance(80-dodge_chance+hit_chance)
	{
		if char_get_skill_level(char_source,SKILL_HEAL) == 3
			var damage = char_get_lindo(char_source)* 1.30 + char_get_health_max(char_target)*0.20;
		else if char_get_skill_level(char_source,SKILL_HEAL) == 2
			var damage = char_get_lindo(char_source)* 1.15 + char_get_health_max(char_target)*0.15;
		else
			var damage = char_get_lindo(char_source)* 1.00 + char_get_health_max(char_target)*0.10;
			
		var damage_size_adjust = 1;
		var shake_size_adjust = 1;
		if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);voln_play_sfx(sfx_Hit8);damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
		var armor = 0;
		var real_damage = damage * clamp(((100-armor)/100),0,1);
		char_target[? GAME_CHAR_HEALTH] = min(char_target[? GAME_CHAR_HEALTH] + real_damage,char_get_health_max(char_target));
		
		
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(real_damage)),COLOR_HEAL,0.5,1.2*damage_size_adjust);
		
		//ally_get_hit_port(char_target);
		
		ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),TAG_FONT_N,TAG_COLOR_HEAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" HP.",TAG_FONT_N,TAG_COLOR_HEAL));
		
		char_target[? GAME_CHAR_COLOR] = COLOR_HEAL;
		char_target[? GAME_CHAR_SHAKE_AMOUNT] = 0;
			
		/*var debug_message = "Fireball is performed,";
		debug_message+= " source: ";
		debug_message+= string(damage);
		debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
		debug_message+= " target: ";
		debug_message+= string(armor);
		debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";*/

	}
	else
	{
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,0,0.7);
		ds_list_add(battlelog_pieces_list,text_add_markup(" and misses!",TAG_FONT_N,TAG_COLOR_NORMAL));
	}	
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);

	voln_play_sfx(sfx_Buff_Heal_2);
	
	script_execute_add(BATTLE_PAUSE_TIME_LONG_SEC,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}