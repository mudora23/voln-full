///voln_load(slot[1-6]);
/// @description
/// @param slot[1-6]
if file_exists("game"+string(argument0)+".dat")
{
	voln_play_sfx(sfx_game_load);
	
	var_map_destroy();
    obj_control.var_map = ds_map_json_load("game"+string(argument0)+".dat");
	var_map_add_missing_keys();
	location_map_update_path_and_neighbor_list();
	location_map_update_visited_and_visible_info();
	
	var_map_set(VAR_SCRIPT_DIALOG_SURFACE,noone);
	
	var choices_list_surface = var_map_get(VAR_CHOICES_LIST_SURFACE);
	for(var i = 0; i < ds_list_size(choices_list_surface);i++)
		if surface_exists(choices_list_surface[| i])
		{
			surface_free(choices_list_surface[| i]);
			choices_list_surface[| i] = noone;
		}
	
	
	
	var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
	for(var i = 0; i < ds_list_size(ally_list);i++)
		game_char_map_add_missing_keys(ally_list[| i],OWNER_ALLY);
		
	var ally_r_list = obj_control.var_map[? VAR_ALLY_R_LIST];
	for(var i = 0; i < ds_list_size(ally_r_list);i++)
		game_char_map_add_missing_keys(ally_r_list[| i],OWNER_ALLY);
		
	var pet_list = obj_control.var_map[? VAR_PET_LIST];
	for(var i = 0; i < ds_list_size(pet_list);i++)
		game_char_map_add_missing_keys(pet_list[| i],OWNER_ALLY);
		
	var pet_r_list = obj_control.var_map[? VAR_PET_R_LIST];
	for(var i = 0; i < ds_list_size(pet_r_list);i++)
		game_char_map_add_missing_keys(pet_r_list[| i],OWNER_ALLY);
		
	var storage_list = obj_control.var_map[? VAR_STORAGE_LIST];
	for(var i = 0; i < ds_list_size(storage_list);i++)
		game_char_map_add_missing_keys(storage_list[| i]);
	
	game_char_map_add_missing_keys(obj_control.var_map[? VAR_PLAYER],OWNER_PLAYER);

	
	
    instance_destroy();
    room_goto(room_game);
    obj_control.loading = true;
	
} 
