///_var_map_add_ext(KEY,AMOUNT,MIN,MAX);
/// @description  
/// @param KEY
/// @param AMOUNT
/// @param MIN
/// @param MAX

if scene_is_current()
{
	var_map_add_ext(argument0,argument1,argument2,argument3);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);