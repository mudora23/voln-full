///_aes_AddRoundKey(state, RoundKey, round)
var Nk = global._aes_Nk, Nb = global._aes_Nb;
var i,j, state = argument0, RoundKey = argument1, rnd = argument2;


var rkk;

for(i=0;i<4;++i)
{
  for(j = 0; j < 4; ++j)
  {
    rkk = RoundKey[@ rnd * Nb * 4 + i * Nb + j];
    state[@i, j] ^= rkk;
  }
}


