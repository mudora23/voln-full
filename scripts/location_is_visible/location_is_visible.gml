///location_is_visible(LOCATION);
/// @description
with obj_control
{
	return ds_list_find_index(obj_control.var_map[? VAR_LOCATION_VISIBLE_LIST],argument[0])>=0;
}