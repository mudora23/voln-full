///scriptvar_Ally_number_is_more_than_0();
/// @description
return ds_list_size(var_map_get(VAR_ALLY_LIST)) > 0;