///aes_generate_iv([len])

var a, len = 16, i;

if (argument_count > 0) len = argument[0];

for (i=0;i<len;++i){
    a[i] = irandom(255);
}

return a;

