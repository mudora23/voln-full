///char_map_list_team_exists(char_map_list,name1,gender1,name2,gender2,...);
/// @descirption
/// @param char_map_list
/// @param name1
/// @param gender1
/// @param name2
/// @param gender2,...


var char_map_list = argument[0];

var char_name_list = ds_list_create();
var char_gender_list = ds_list_create();

if argument_count > 1 ds_list_add(char_name_list,argument[1]);
if argument_count > 2 ds_list_add(char_gender_list,argument[2]);
if argument_count > 3 ds_list_add(char_name_list,argument[3]);
if argument_count > 4 ds_list_add(char_gender_list,argument[4]);
if argument_count > 5 ds_list_add(char_name_list,argument[5]);
if argument_count > 6 ds_list_add(char_gender_list,argument[6]);
if argument_count > 7 ds_list_add(char_name_list,argument[7]);
if argument_count > 8 ds_list_add(char_gender_list,argument[8]);
if argument_count > 9 ds_list_add(char_name_list,argument[9]);
if argument_count > 10 ds_list_add(char_gender_list,argument[10]);
if argument_count > 11 ds_list_add(char_name_list,argument[11]);
if argument_count > 12 ds_list_add(char_gender_list,argument[12]);
if argument_count > 13 ds_list_add(char_name_list,argument[13]);
if argument_count > 14 ds_list_add(char_gender_list,argument[14]);



for(var i = 0; i < ds_list_size(char_map_list);i++)
{
	var the_char = char_map_list[| i];
	var the_name = the_char[? GAME_CHAR_NAME];
	var the_nametype = the_char[? GAME_CHAR_NAME_TYPE];
	var the_gender = the_char[? GAME_CHAR_GENDER];

	for(var j = 0; j < ds_list_size(char_gender_list);j++)
	{
		var the_checking_name = char_name_list[| j];
		var the_checking_gender = char_gender_list[| j];

		if (the_checking_name == NAME_ANY || the_name == the_checking_name || the_nametype == the_checking_name) && (the_checking_gender == GENDER_ANY || the_gender == GENDER_NONE || the_gender == the_checking_gender)
		{
			ds_list_delete(char_name_list, j);
			ds_list_delete(char_gender_list, j);
			j--;
			break;
		}
	}


}

if ds_list_size(char_name_list) == 0 && ds_list_size(char_gender_list) == 0
{
	ds_list_destroy(char_name_list);
	ds_list_destroy(char_gender_list);
	return true;
}
else
{
	ds_list_destroy(char_name_list);
	ds_list_destroy(char_gender_list);
	return false;
}

