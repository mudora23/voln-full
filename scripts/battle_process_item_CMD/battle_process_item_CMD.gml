///battle_process_item_CMD(char_index,item_name);
/// @description
with obj_control
{
	scene_choices_list_clear();
	
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	
	
	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" uses ",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(item_get_displayname(argument[1]),TAG_FONT_N,TAG_COLOR_HEAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(".",TAG_FONT_N,TAG_COLOR_NORMAL));
	
	// performing
	CS_using(char_target,argument[1]);
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);

	
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}