///S_0009();
/// @description

_label("start");

_label("S_0009_GREENVILLAGE");
	_qj_remove("S_0008_FINISHED_QJ");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Incomplete_01,FADE_NORMAL_SEC,1.5);
	_dialog("S_0009_GREENVILLAGE_1");
	_show("zeyd",spr_char_Zeyd_f_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_2");
	_show("ku",spr_char_Ku_n_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_3");
	_dialog("S_0009_GREENVILLAGE_4");
	_dialog("S_0009_GREENVILLAGE_5");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Nirholt_Streets_01_midsun,FADE_NORMAL_SEC,1.5);
	_dialog("S_0009_GREENVILLAGE_6");
	_show("ku",spr_char_Ku_n_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_7");
	_show("felg",spr_char_Felg_c_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_8");
	_dialog("S_0009_GREENVILLAGE_9");
	_show("zeyd",spr_char_Zeyd_f_n,XNUM_CENTER,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_10");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_11");
	_show("crazyork",spr_char_ork_5,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_12");
	_dialog("S_0009_GREENVILLAGE_13");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_14");
	_show("greenchief",spr_char_anon_female_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_15");
	_dialog("S_0009_GREENVILLAGE_16");
	_dialog("S_0009_GREENVILLAGE_17");
	_dialog("S_0009_GREENVILLAGE_18");
	_dialog("S_0009_GREENVILLAGE_19");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_20");
	_show("ku",spr_char_Ku_n_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_21");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_22");
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_23");
	
	if ally_exists(NAME_LORN)
		_jump("S_0009_GREENVILLAGE_24");
	else
		_jump("S_0009_GREENVILLAGE_26");
	
_label("S_0009_GREENVILLAGE_24");
	_show("lorn",spr_char_Lorn_s_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_24");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_25");
	_jump("S_0009_GREENVILLAGE_26");
	
_label("S_0009_GREENVILLAGE_26");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_26");
	_dialog("S_0009_GREENVILLAGE_27");
	_dialog("S_0009_GREENVILLAGE_28");
	
	if chunk_mark_get("S_0008_REDPASS_GAY_1")
		_jump("S_0009_GREENVILLAGE_28B_1");
	else
		_jump("S_0009_GREENVILLAGE_28A_1");
		
_label("S_0009_GREENVILLAGE_28A_1");	
	_dialog("S_0009_GREENVILLAGE_28A_1");
	_dialog("S_0009_GREENVILLAGE_28A_2");	
	_jump("S_0009_GREENVILLAGE_29");
		
_label("S_0009_GREENVILLAGE_28B_1");	
	_dialog("S_0009_GREENVILLAGE_28B_1");
	_dialog("S_0009_GREENVILLAGE_28B_2");	
	_dialog("S_0009_GREENVILLAGE_28B_3");	
	_jump("S_0009_GREENVILLAGE_29");

_label("S_0009_GREENVILLAGE_29");
	_show("greenchief",spr_char_anon_female_b,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_29");
	_show("crazyork",spr_char_ork_5,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_30");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_31");
	
	if ally_exists(NAME_LORN)
		_jump("S_0009_GREENVILLAGE_32");
	else
		_jump("S_0009_GREENVILLAGE_33");
	
_label("S_0009_GREENVILLAGE_32");
	_show("lorn",spr_char_Lorn_s_n,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GREENVILLAGE_32");
	
_label("S_0009_GREENVILLAGE_33");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GREENVILLAGE_33");
	_jump("S_0009_DEMAND_ENTRANCE");

	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0009_DEMAND_ENTRANCE");
	_qj_add("S_0009_DEMAND_ENTRANCE_QJ");
	_change_bg("bg",spr_bg_Forest_Brook_01_midsun,FADE_NORMAL_SEC,1.5);
	_dialog("S_0009_DEMAND_ENTRANCE_1");
	_show("felg",spr_char_Felg_c_n,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE_2");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_DEMAND_ENTRANCE_3");
	_dialog("S_0009_DEMAND_ENTRANCE_4");
	_change_bg("bg",spr_bg_Nirholt_Distant_01_midsun,FADE_NORMAL_SEC,1.5);
	_pause(2,0,true);
	_show("felg",spr_char_Felg_c_n,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE_5");
	_dialog("S_0009_DEMAND_ENTRANCE_6");
	_dialog("S_0009_DEMAND_ENTRANCE_7");
	_dialog("S_0009_DEMAND_ENTRANCE_8");
	_dialog("S_0009_DEMAND_ENTRANCE_9");
	_movex("felg",XNUM_LEFT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("gorumguard",spr_char_anon_male_b,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE_10");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_DEMAND_ENTRANCE_11");
	_show("gorumguard",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE_12");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Forest_Brook_01_midsun,FADE_NORMAL_SEC,1.5);
	_dialog("S_0009_DEMAND_ENTRANCE_13");
	_show("ku",spr_char_Ku_n_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE_14");
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE_15");
	_dialog("S_0009_DEMAND_ENTRANCE_16");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_DEMAND_ENTRANCE_17");
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_NimfSearch_1");
	_dialog("S_0009_NimfSearch_2",true);
	
	_choice("“Get down on the forest floor with Zeyd for some fun.”","S_0009_NimfSearch_3A_1");
	_choice("“See if there’s anything nearby you could have sex with.”","S_0009_NimfSearch_3B_1");
	_choice("“You can relax when this search is over.”","S_0009_NimfSearch_3C_1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0009_NimfSearch_3A_1");
	_dialog("S_0009_NimfSearch_3A_1");
	_jump("S_0009_NimfSearch_3A_2");
	
_label("S_0009_NimfSearch_3A_2");
	_dialog("S_0009_NimfSearch_3A_2");
	_dialog("S_0009_NimfSearch_3A_3");
	_dialog("S_0009_NimfSearch_3A_4");
	_dialog("S_0009_NimfSearch_3A_5");
	_jump("S_0009_NimfSearch_4");
	
_label("S_0009_NimfSearch_3B_1");
	_dialog("S_0009_NimfSearch_3B_1");
	_dialog("S_0009_NimfSearch_3B_2");
	_dialog("S_0009_NimfSearch_3B_3");

	_choice("Fuck ‘em","S_0009_NimfSearch_3B_4A_1");
	_choice("Fuck him","S_0009_NimfSearch_3B_4B_1");
	_block();
	
_label("S_0009_NimfSearch_3B_4A_1");
	_dialog("S_0009_NimfSearch_3B_4A_1");
	_dialog("S_0009_NimfSearch_3B_4A_2");
	_dialog("S_0009_NimfSearch_3B_4A_3");
	_jump("S_0009_NimfSearch_4");
	
_label("S_0009_NimfSearch_3B_4B_1");
	_dialog("S_0009_NimfSearch_3B_4B_1");
	_jump("S_0009_NimfSearch_3A_2");
	
_label("S_0009_NimfSearch_3C_1");
	_dialog("S_0009_NimfSearch_3C_1");
	_dialog("S_0009_NimfSearch_3C_2");
	_jump("S_0009_NimfSearch_4");
	
_label("S_0009_NimfSearch_4");
	_dialog("S_0009_NimfSearch_4");
	_dialog("S_0009_NimfSearch_5");
	_dialog("S_0009_NimfSearch_6");
	_dialog("S_0009_NimfSearch_7");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_NimfSearch_8",true);
	
	_choice("“Watch the nimfs and dreyks play while you play yourself.”","S_0009_NimfSearch_9A");
	_choice("“Just sit.  And wait.  And be pissy.”","S_0009_NimfSearch_9B");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0009_NimfSearch_9A");
	_dialog("S_0009_NimfSearch_9A_1");
	_dialog("S_0009_NimfSearch_9A_2");
	_dialog("S_0009_NimfSearch_9A_3");
	_jump("S_0009_NimfSearch_10");
	
_label("S_0009_NimfSearch_9B");
	_dialog("S_0009_NimfSearch_9B");
	_jump("S_0009_NimfSearch_10");
	
_label("S_0009_NimfSearch_10");
	_dialog("S_0009_NimfSearch_10");
	_show("eldernimf",spr_char_anon_male_m,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_NimfSearch_11");
	_dialog("S_0009_NimfSearch_12");
	_dialog("S_0009_NimfSearch_13");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_NimfSearch_14");
	_show("ku",spr_char_Ku_n_n,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_NimfSearch_15");
	_dialog("S_0009_NimfSearch_16");
	_dialog("S_0009_NimfSearch_17");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_NimfSearch_18");
	_show("muu",spr_char_anon_neutral_s,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_NimfSearch_19");
	_dialog("S_0009_NimfSearch_20");
	_dialog("S_0009_NimfSearch_21");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_NimfSearch_22");

	_dialog("S_0009_DEMAND_ENTRANCE2_1");
	_show("gorumguard",spr_char_anon_male_b,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE2_2");
	_dialog("S_0009_DEMAND_ENTRANCE2_3");
	_show("felg",spr_char_Felg_c_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_DEMAND_ENTRANCE2_4");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Nirholt_Streets_01_midsun,FADE_NORMAL_SEC,1.5);
	_dialog("S_0009_DEMAND_ENTRANCE2_5");
	_jump("S_0009_GORUM");

	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0009_GORUM");
	_qj_remove("S_0009_DEMAND_ENTRANCE_QJ");
	_qj_add("S_0009_GORUM_QJ");
	_dialog("S_0009_GORUM_1");
	_show("felg",spr_char_Felg_c_n,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM_2");
	_dialog("S_0009_GORUM_3");
	_dialog("S_0009_GORUM_4");
	_qj_remove("S_0009_GORUM_QJ");
	_qj_add("S_0009_GORUM_4_QJ");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GORUM_5");
	_dialog("S_0009_GORUM_6");
	_change_bg("bg",spr_bg_Cult_RitualPit_01,FADE_NORMAL_SEC,1.5);
	_pause(2,0,true);
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM_7");
	_show("felg",spr_char_Felg_c_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM_8");
	_dialog("S_0009_GORUM_9");
	_show("lorn",spr_char_Lorn_s_n,XNUM_CENTER,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM_10");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GORUM_11");
	_change_bg("bg",spr_bg_Nirholt_Distant_01_midsun,FADE_NORMAL_SEC,1.5);
	_pause(2,0,true);
	_show("felgor",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM_12");
	_dialog("S_0009_GORUM_13");
	_dialog("S_0009_GORUM_14");
	_qj_remove("S_0009_GORUM_4_QJ");
	_qj_add("S_0009_GORUM_14_QJ");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GORUM_15",true);
	_choice("“It’s snuggling time!”","S_0009_GORUM_Choice1_1");
	_choice("“It’s clobbering time!”","S_0009_GORUM_Choice1_2");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0009_GORUM_Choice1_1");
	_dialog("S_0009_GORUM_S_1");
	_dialog("S_0009_GORUM_S_2");
	_dialog("S_0009_GORUM_S_3");
	_dialog("S_0009_GORUM_S_4");
	_dialog("S_0009_GORUM_S_5");
	_jump("S_0009_GORUM2");

_label("S_0009_GORUM_Choice1_2");
	_dialog("S_0009_GORUM_F_1");
	_jump("S_0009_GORUM2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0009_GORUM2");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GORUM2_1");
	_qj_remove("S_0009_GORUM_14_QJ");
	_qj_add("S_0009_GORUM2_1_QJ");
	_show("gil",spr_char_anon_neutral_s,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_2");
	_dialog("S_0009_GORUM2_3");
	_show("ilnora",spr_char_anon_female_b,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_4");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0009_GORUM2_5");
	_show("gil",spr_char_anon_neutral_s,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_6");
	_dialog("S_0009_GORUM2_7");
	_show("felgor",spr_char_Felg_c_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_8");
	_dialog("S_0009_GORUM2_9");
	_dialog("S_0009_GORUM2_10");
	_dialog("S_0009_GORUM2_11");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_12");
	
	if ally_exists(NAME_LORN)
		_jump("S_0009_GORUM2_13");
	else
		_jump("S_0009_GORUM2_14");

_label("S_0009_GORUM2_13");
	_show("lorn",spr_char_Lorn_s_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_13");
	
_label("S_0009_GORUM2_14");
	_dialog("S_0009_GORUM2_14");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_show("gil",spr_char_anon_neutral_s,0,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_15");
	_dialog("S_0009_GORUM2_16");
	_dialog("S_0009_GORUM2_17");
	_dialog("S_0009_GORUM2_18");
	_movex("gil",XNUM_LEFT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("felgor",spr_char_Felg_c_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0009_GORUM2_19");
	_dialog("S_0009_GORUM2_20",true);
	_choice("“I’m with you, Felgor.”","S_0009_GORUM2_Choice1_1");
	_choice("“I’m sorry, Felgor, but you aren’t fit to lead.”","S_0009_GORUM2_Choice1_2");
	_block();
	

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0009_GORUM2_Choice1_1");
	_dialog("S_0009_GORUM2_FEL_1");
	_dialog("S_0009_GORUM2_FEL_2");
	_dialog("S_0009_GORUM2_FEL_3");
	_qj_remove("S_0009_GORUM2_1_QJ");
	_qj_add("S_0009_GORUM2_FEL_3_QJ");
	_jump("S_0009_GORUM2_Finish");

_label("S_0009_GORUM2_Choice1_2");
	_dialog("S_0009_GORUM2_BRO_1");
	_dialog("S_0009_GORUM2_BRO_2");
	_qj_remove("S_0009_GORUM2_1_QJ");
	_qj_add("S_0009_GORUM2_BRO_2_QJ");
	_jump("S_0009_GORUM2_Finish");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0009_GORUM2_Finish");
	_execute(0,location_unlock,LOCATION_61,LOCATION_62,LOCATION_63,LOCATION_64,LOCATION_65,LOCATION_66,LOCATION_67,LOCATION_68,LOCATION_69);
	_void();
	_void();
	_map();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
