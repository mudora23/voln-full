///ds_map_add_unique_map(id,key,value);
/// @description
/// @param id
/// @param key
/// @param value
if !ds_map_exists(argument[0],argument[1])
{
	return ds_map_add_map(argument[0],argument[1],argument[2]);
}
else
{
	return ds_map_destroy(argument[2]);
}