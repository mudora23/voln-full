///S_heal_all();
/// @description - in town now

with obj_control
{
	var the_list = var_map_get_all_character_map_list();
	for(var i = 0; i < ds_list_size(the_list);i++)
	{
		var the_char = the_list[| i];
		the_char[? GAME_CHAR_HEALTH] = char_get_health_max(the_char);
		the_char[? GAME_CHAR_LUST] = 0;
		the_char[? GAME_CHAR_VITALITY] = char_get_vitality_max(the_char);
		the_char[? GAME_CHAR_COLOR_TARGET] = c_white;
	}

	
}