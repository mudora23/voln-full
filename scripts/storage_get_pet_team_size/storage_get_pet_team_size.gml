///storage_get_pet_team_size();
/// @description
var storage_team = var_map_get(VAR_STORAGE_LIST);
var storage_size = ds_list_size(storage_team);
var size = 0;
for(var i =0; i < storage_size;i++)
{
	var char = storage_team[| i];
	if char[? GAME_CHAR_TYPE] == GAME_CHAR_PET size++; 
}
return size;