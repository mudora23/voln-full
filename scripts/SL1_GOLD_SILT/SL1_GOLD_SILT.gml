///SL1_GOLD_SILT();
/// @description Nirholt - GOLD SILT

_label("start");

_label("SL1_GOLD_SILT_INTRO");
	_show("ork",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_GOLD_SILT_INTRO",true);
	_jump("SL1_GOLD_SILT_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_GOLD_SILT_RETURN");
	_show("ork",spr_char_anon_male_b,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_GOLD_SILT_RETURN",true);
	_jump("SL1_GOLD_SILT_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_GOLD_SILT_Choice");
	_choice("Buy","SL1_GOLD_SILT_STORE");
	_choice("Exit","SL1_GOLD_SILT_EXIT");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_GOLD_SILT_STORE");
	if chunk_mark_get("SL43_OLD_YOJI_2")
	{
		_store_add_item(ITEM_A_CHAIN_LEAD);
		_store_add_item(ITEM_SHACKLES);
	}
	else
	{
		_void();
		_void();
	}
	_store_add_item(ITEM_TEEG_SALTED);
	_store_add_item(ITEM_PORK_SALTED);
	_store_add_item(ITEM_BUN_STALE);
	_store_add_item(ITEM_BEEF_JERKED);
	_store_add_item(ITEM_BUN_FRESH);
	_store_add_item(ITEM_CAKE_KOMAR);
	_store_add_item(ITEM_CAKE_FANCY);
	_store_add_item(ITEM_MILK_LUMKU);
	_store_add_item(ITEM_MESO_EY_TONIC_POOR);
	_store_add_item(ITEM_MESO_EY_TONIC_GOOD);
	_store_add_item(ITEM_VITALITY_TONIC_POOR);
	_store_add_item(ITEM_DEENSKIP_TONIC_POOR);
	_store_add_item(ITEM_DEENSKIP_TONIC_GOOD);
	_store_add_item(ITEM_KOMAR_GOOD);
	_store_add_item(ITEM_CANDY_FRU);
	_store_add_item(ITEM_GUM_MINT);
	_store_add_item(ITEM_MUSK_EXTRACT_ELF);
	_store_add_item(ITEM_REDBERRIES_JARRED);
	
	_var_map_set(VAR_EXTRA_AFTER_SCENE,"SL1_GOLD_SILT");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"SL1_GOLD_SILT_STORE");
	_var_map_set(VAR_EXTRA2_AFTER_SCENE,"SL1_GOLD_SILT");
	_var_map_set(VAR_EXTRA2_AFTER_SCENE_LABEL,"SL1_GOLD_SILT_EXIT");
	_jump("start","CS_buying");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_GOLD_SILT_EXIT");
	_hide_all_except("ork",FADE_NORMAL_SEC);
	_dialog("SL1_GOLD_SILT_EXIT");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////




