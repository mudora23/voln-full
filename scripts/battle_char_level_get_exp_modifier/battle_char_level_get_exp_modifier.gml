///char_level_get_exp_modifier(winner_level,loser_level);
/// @description
/// @param winner_level
/// @param loser_level
var winner_level = argument[0];
var loser_level = argument[1];

if winner_level > loser_level
{
	return 1/((winner_level-loser_level+1)*0.6);
}
else if winner_level < loser_level
{
	return 1+((loser_level - winner_level)*0.3);
}
else
	return 1;
