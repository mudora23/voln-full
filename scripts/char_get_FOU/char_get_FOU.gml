///char_get_FOU(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_FOU);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_FOU_1);
amount += char_get_skill_level(argument0,SKILL_FOU_2);
amount += char_get_skill_level(argument0,SKILL_FOU_3);
amount += char_get_skill_level(argument0,SKILL_FOU_4);
amount += char_get_skill_level(argument0,SKILL_FOU_5);
amount += char_get_skill_level(argument0,SKILL_FOU_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;