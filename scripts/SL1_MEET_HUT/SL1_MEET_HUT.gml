///SL1_MEET_HUT();
/// @description Nirholt - MEET HUT

_label("start");

_label("SL1_MEET_HUT_INTRO");
	_dialog("SL1_MEET_HUT_INTRO");
	_jump("S_0004_MEET_HUT_INTRO_EVENT","S_0004");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_RETURN");
	_change_bg("bg",spr_bg_Nirholt_MH_01,FADE_NORMAL_SEC,1.5);
	_dialog("SL1_MEET_HUT_RETURN");
	if chunk_mark_get("S_0005_WON_14_C")
		_jump("SL1_MEET_HUT_OPEN");
	else
		_jump("SL1_MEET_HUT");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT");
	_dialog("SL1_MEET_HUT");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN");
	_choice("[?]Buy food","SL1_MEET_HUT_OPEN_FOOD");
	_choice("Talk to Kûdi","SL1_MEET_HUT_OPEN_TALK");
	_choice("Leave","SL1_MEET_HUT_OPEN_LEAVE");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_FOOD");
	_choice("Leave","SL1_MEET_HUT_OPEN");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_TALK");
	_dialog("SL1_MEET_HUT_OPEN_TALK",true);
	_jump("SL1_MEET_HUT_OPEN_TALK_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_TALK_Choice");
	if !chunk_mark_get("SL1_MEET_HUT_OPEN_TALK_CHAT")
		_choice("Chat","SL1_MEET_HUT_OPEN_TALK_CHAT");
	else
		_choice("Suggest sex","SS_MEET_HUT","CS_extra_SS");
	_choice("Leave","SL1_MEET_HUT_OPEN");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_TALK_CHAT");
	_dialog("SL1_MEET_HUT_OPEN_TALK_CHAT",true);
	_execute(0,char_add_char_opinion,NAME_KUDI,obj_control.var_map[? VAR_PLAYER],1); 
	_jump("SL1_MEET_HUT_OPEN_TALK_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_TALK_SUGGEST");
	_dialog("SL1_MEET_HUT_OPEN_TALK_SUGGEST",true);
	_jump("SL1_MEET_HUT_OPEN_TALK_SUGGEST_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_TALK_SUGGEST_Choice");
	_choice("Leave","SL1_MEET_HUT_OPEN_TALK_Choice");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_MEET_HUT_OPEN_LEAVE");
	_dialog("SL1_MEET_HUT_OPEN_LEAVE");
	_play_sfx(sfx_Wooden_Door_Close_1);
	_dialog("SL1_MEET_HUT_OPEN_LEAVE_2",true);
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////