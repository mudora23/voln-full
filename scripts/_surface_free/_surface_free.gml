///_surface_free(surface);
/// @description
/// @param surface
if scene_is_current()
{
	if surface_exists(argument0) surface_free(argument0);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);