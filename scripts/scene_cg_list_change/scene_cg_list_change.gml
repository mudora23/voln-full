///scene_cg_list_change(ID,cg,alpha_target,fade_speed,slide_speed,order,halign,valign,slide_from_halign,slide_from_valign,hstick,vstick);
/// @description  
/// @param ID
/// @param cg
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
/// @param halign
/// @param valign
/// @param slide_from_halign
/// @param slide_from_valign
/// @param hstick
/// @param vstick

with obj_control
{
	var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	var cg_index_list = obj_control.var_map[? VAR_CG_LIST_INDEX_NAME];
	var cg_index_target_list = obj_control.var_map[? VAR_CG_LIST_INDEX_TARGET_NAME];
	var cg_x_list = obj_control.var_map[? VAR_CG_LIST_X];
	var cg_x_target_list = obj_control.var_map[? VAR_CG_LIST_X_TARGET];
	var cg_y_list = obj_control.var_map[? VAR_CG_LIST_Y];
	var cg_y_target_list = obj_control.var_map[? VAR_CG_LIST_Y_TARGET];
	var cg_alpha_list = obj_control.var_map[? VAR_CG_LIST_ALPHA];
	var cg_alpha_target_list = obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET];
	var cg_fade_speed_list = obj_control.var_map[? VAR_CG_LIST_FADE_SPEED];
	var cg_slide_speed_list = obj_control.var_map[? VAR_CG_LIST_SLIDE_SPEED];
	var cg_order_list = obj_control.var_map[? VAR_CG_LIST_ORDER];
	var cg_size_ratio_list = obj_control.var_map[? VAR_CG_LIST_SIZE_RATIO];
	
	if ds_list_find_index(cg_id_list,argument[0]) < 0
	{
		scene_cg_list_add(argument[0],argument[1],argument[2],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
	}
	else
	{
		var existing_index = ds_list_find_index(cg_id_list,argument[0]);
		
		if sprite_exists(argument[1])
		{
			cg_index_target_list[| existing_index] = sprite_get_name(argument[1]);
			var cg = argument[1];
		}
		else
		{
			cg_index_target_list[| existing_index] = sprite_get_name(spr_bg_black_full);
			var cg = spr_bg_black_full;
		}
		
		var halign = argument[6];
		var valign = argument[7];
		
		ds_map_add_unique(obj_control.var_map[? VAR_CG_HALIGN_MAP],sprite_get_name(cg),halign);
		ds_map_add_unique(obj_control.var_map[? VAR_CG_VALIGN_MAP],sprite_get_name(cg),valign);
		
		if halign == fa_left
		{
			if valign = fa_top
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = 0;
				var yy = 0;
			}
			else if valign = fa_bottom
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = 0;
				var yy = room_height - sprite_get_height(cg)*raito;
			}
			else
			{
				var raito = room_height / sprite_get_height(cg);
				var xx = 0;
				var yy = 0;
			}
		}
		else if halign = fa_right
		{
			if valign = fa_top
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = room_width - sprite_get_width(cg)*raito;
				var yy = 0;
			}
			else if valign = fa_bottom
			{
				var raito = min(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = room_width - sprite_get_width(cg)*raito;
				var yy = room_height - sprite_get_height(cg)*raito;
			}
			else
			{
				var raito = room_height / sprite_get_height(cg);
				var xx = room_width - sprite_get_width(cg)*raito;
				var yy = 0;
			}
		}
		else
		{
			if valign = fa_top
			{
				var raito = room_width - sprite_get_width(cg)*raito;
				var xx = 0;
				var yy = 0;
			}
			else if valign = fa_bottom
			{
				var raito = room_width - sprite_get_width(cg)*raito;
				var xx = 0;
				var yy = room_height - sprite_get_height(cg)*raito;
			}
			else
			{
				var raito = max(room_height / sprite_get_height(cg), room_width / sprite_get_width(cg));
				var xx = room_width/2 - sprite_get_width(cg)*raito/2;
				var yy = room_height/2 - sprite_get_height(cg)*raito/2;
			}
		}
		
		cg_x_target_list[| existing_index] = xx;
		cg_y_target_list[| existing_index] = yy;
		cg_alpha_target_list[| existing_index] = argument[2];
		cg_fade_speed_list[| existing_index] = argument[3];
		cg_slide_speed_list[| existing_index] = argument[4];
		cg_order_list[| existing_index] = argument[5];
		cg_size_ratio_list[| existing_index] = raito;
		
	}

}

