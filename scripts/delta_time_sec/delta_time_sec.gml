///delta_time_sec();
/// @description - delta time in seconds.
return delta_time / 1000000;