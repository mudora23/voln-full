///battle_process_choose_CMD(same_side?,char_CMD,extra_arg[optional]); 
/// @description
/// @param same_side?
/// @param char_CMD
/// @param extra_arg[optional]
with obj_control
{
	scene_choices_list_clear();
	obj_control.enemies_choice_enable = true;
	
	var char_map = battle_get_current_action_char();
	var same_side = argument[0];
	var char_CMD = script_get_index_ext(argument[1]);

	if same_side var list_of_characters = battle_char_create_same_side_list(char_map);
	else var list_of_characters = battle_char_create_opposite_side_list(char_map);
	
	if same_side var bodyguard_char = noone;
	else var bodyguard_char = battle_char_list_get_bodyguard_char(list_of_characters);
	
	for(var i = 0; i < ds_list_size(list_of_characters);i++)
	{
		var the_char = list_of_characters[| i];
		if bodyguard_char == noone || bodyguard_char == the_char
		{
			if battle_char_get_combat_capable(the_char) && (same_side || !battle_char_get_buff_level(the_char,BUFF_HIDE))
			{
				if argument_count > 2
					scene_choiceCMD_add(the_char[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(the_char),argument[2]);
				else
					scene_choiceCMD_add(the_char[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(the_char));
			}
		}
	}
	
	
	
	scene_choiceCMD_add("Cancel",battle_process_player);

}