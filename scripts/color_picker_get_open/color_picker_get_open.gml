///color_picker_get_open();
/// @description

with obj_color_selector
	if open return true;
return false;