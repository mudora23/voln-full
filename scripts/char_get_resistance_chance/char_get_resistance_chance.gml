///char_get_resistance_chance(char_map);
/// @description
/// @param char_map

var number = 8*power(char_get_END(argument0),0.5);

if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;

number += char_get_info(argument0,GAME_CHAR_LEVEL);

return min(100,number);
