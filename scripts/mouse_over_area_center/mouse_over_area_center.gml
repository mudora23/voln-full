/// mouse_over_area_center(x,y,w,h);
/// @description
/// @param x
/// @param y
/// @param w
/// @param h
/*
    hover a certain area of the room
*/
return  mouse_x >= argument0-argument2/2 &&
        mouse_y >= argument1-argument3/2 &&
        mouse_x <= argument0+argument2/2 &&
        mouse_y <= argument1+argument3/2
        


