///scene_sprite_list_sort();
/// @description
var i,j,k;
var order_list = obj_control.var_map[? VAR_CG_LIST_ORDER];
var ascend = false;
for (i = 1; i < ds_list_size(order_list); ++i)
{
    k = order_list[| i];
    j = i - 1;
    while j >= 0 && ((!ascend && k > order_list[| j]) || (ascend && k < order_list[| j]))
    {
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_ID],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_INDEX_NAME],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_INDEX_TARGET_NAME],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_X],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_X_TARGET],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_Y],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_Y_TARGET],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_ALPHA],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_FADE_SPEED],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_SLIDE_SPEED],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_SLIDE_SPEED],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_ORDER],j+1,j);
		ds_list_swap(obj_control.var_map[? VAR_CG_LIST_SIZE_RATIO],j+1,j);
        --j;
    }
}