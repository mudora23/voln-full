///_show(ID,sprite_with_border,alpha,alpha_target,fade_speed,slide_speed,order);
/// @description  
/// @param ID
/// @param sprite_with_border
/// @param alpha
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order

if scene_is_current()
{
	scene_spritewb_list_add(argument0,argument1,argument2,argument3,argument4,argument5,argument6);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);