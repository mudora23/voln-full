///draw_saveload_slot(x1,y1,x2,y2,['Save'/'Load'],number[1-6]);
/// @description
/// @param 'Save'/'Load'
/// @param number[1-6]

draw_set_alpha(menu_open_text_image_alpha);
//draw_set_font(font_general_30_b_small_caps);

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var type = argument4;
var slot = argument5;
var xpadding = 30;
var slot_hover = mouse_over_area(x1,y1,x2-x1,y2-y1);
var the_info_map = obj_control.info_list_of_map[| argument5];




if the_info_map != noone
{
	var delbutton = spr_delete_button;
	var delbutton_width = sprite_get_width(delbutton);
	var delbutton_height = sprite_get_height(delbutton);
	var delbutton_xx = x2 - delbutton_width;
	var delbutton_yy = y1;
	var delbutton_hover = mouse_over_area(delbutton_xx,delbutton_yy,delbutton_width,delbutton_height);
}
else
	var delbutton_hover = false;


if slot_hover && !delbutton_hover && (type == "Save" || the_info_map != noone)
{
    var bg_color = c_silver;
    var text_color = colorscheme_almostblack;
}
else
{
    var bg_color = colorscheme_almostblack;
    var text_color = c_gray; 
}

draw_set_color(bg_color);
//draw_rectangle(x1,y1,x2,y2,false);
draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,x2-x1,y2-y1,bg_color,draw_get_alpha());
draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,x2-x1,y2-y1,bg_color,draw_get_alpha());

if the_info_map == noone
{
    draw_set_halign(fa_center);
    draw_set_valign(fa_center);
    draw_set_color(text_color);
    draw_text((x1+x2)/2,(y1+y2)/2,"Empty Slot");
}
else
{
    var the_info_thumb = the_info_map[? "thumb"];
    var ypadding = (y2-y1-sprite_get_height(the_info_thumb))/2;
    draw_sprite(the_info_thumb,-1,x1+xpadding,y1+ypadding);
    
    var info_x = x1+xpadding*2+sprite_get_width(the_info_thumb);
    var info_y = y1+ypadding;
    draw_set_halign(fa_left);
    draw_set_valign(fa_top);
    draw_set_color(text_color);
    draw_text(info_x,info_y,string(the_info_map[? "player"])+"\r"+"Playtime: "+seconds_to_timestamp(the_info_map[? "playtime"]));
}





if the_info_map != noone
{

	draw_sprite_ext(delbutton,delbutton_hover,delbutton_xx,delbutton_yy,1,1,0,c_dkgray,menu_open_text_image_alpha);

	if delbutton_hover
	{
		tooltip = "Delete this save permanently?";
		
		if action_button_pressed_get()
		{
			action_button_pressed_set(false);
			menu_open_previous = menu_open;
			menu_open = MENULISTING_DELETE_SAVE;
			menu_save_deleting = slot;
		}
	}
	
}

if !delbutton_hover && slot_hover && type == "Save" && action_button_pressed_get()
{
    action_button_pressed_set(false);
	voln_play_sfx(sfx_mouse_click);
    voln_save(slot);
    savefiles_info_setup(slot);
}
if !delbutton_hover && slot_hover && type == "Load" && action_button_pressed_get()
{
    action_button_pressed_set(false);
	voln_play_sfx(sfx_mouse_click);
    voln_load(slot);
}


	
	