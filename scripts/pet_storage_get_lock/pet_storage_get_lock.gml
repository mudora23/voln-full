///pet_storage_get_lock(Name);
/// @description
/// @param Index/Name
with obj_control
{
    var char_map = pet_get_char(argument0);
    return char_map[? GAME_CHAR_STORAGE_LOCK];
}