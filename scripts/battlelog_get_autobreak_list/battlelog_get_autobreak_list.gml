///battlelog_get_autobreak_list(dialog_with_tag,widthmax,text_size);
/// @description
/// @param dialog_with_tag
/// @param widthmax
/// @param text_size
with obj_battlelog
{
	var dialog_with_tag = argument[0];
	var dialog_raw = marked_text_get_raw(dialog_with_tag);
	var widthmax = argument[1];
	var text_size = argument[2];
	var curWidth = 0;
	
	var words_list = string_to_words_list(dialog_raw,widthmax);
	var auto_break_list = ds_list_create();
	var string_posi = 0;

	//draw_set_font(font_battlelog);

	for (var ii=0;ii<ds_list_size(words_list);ii++)
	{
		var words = words_list[| ii];
	
		if words == "\r"
		{
			curWidth = string_width(words)*text_size;
		}
		else if words == "" && curWidth + string_width(words)*text_size <= widthmax
		{
			if curWidth = 0 curWidth = string_width(" ")*text_size;
			else curWidth += string_width(" ")*text_size;
		}
		else if curWidth + string_width(" ")*text_size+string_width(words)*text_size <= widthmax
		{
			if curWidth = 0 curWidth = string_width(words)*text_size;
			else curWidth += string_width(" "+words)*text_size;
		}
		else
		{
			curWidth = string_width(words)*text_size;
			ds_list_add(auto_break_list,marked_text_get_posi(string_posi));
		}
	
		if words != "\r"
			string_posi += 1 + string_length(words);

	}
	//show_debug_message("battlelog_get_autobreak_list: "+ds_list_phrase_as_string(auto_break_list));
	return auto_break_list;
}