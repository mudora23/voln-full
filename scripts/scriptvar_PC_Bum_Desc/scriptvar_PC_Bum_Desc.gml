///scriptvar_PC_Bum_Desc();
/// @description
var num = var_map_get(PC_Bum_N);
if num == 0 && var_map_get(PC_Bum_Size_N) < 3 return choose("toned","tight","high");
else if num == 0 return choose("round","gurly","trappy","plum-shaped");
else if num == 1 && var_map_get(PC_Bum_Size_N) < 3 return choose("well-shaped","shapely","pert");
else if num == 1 return choose("round","gurly","trappy","plum-shaped");
else if num == 2 return choose("round","gurly","trappy","plum-shaped");
else return choose("thick","soft","plump","effeminate");