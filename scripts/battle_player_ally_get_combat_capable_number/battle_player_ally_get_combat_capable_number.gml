///battle_player_ally_get_combat_capable_number();
/// @description
return battle_player_get_combat_capable() + battle_ally_get_combat_capable_number();