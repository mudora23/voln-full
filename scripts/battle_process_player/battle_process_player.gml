///battle_process_player();
/// @description
with obj_control
{
	//var_map_set(VAR_MODE,VAR_MODE_BATTLE);
	scene_choices_list_clear();
	
	
	scene_choiceCMD_add("Attack",battle_process_choose_CMD,false,"battle_process_phy_atk_CMD");
	
	if char_get_info(battle_get_current_action_char(),GAME_CHAR_TORU) >= 10
		scene_choice_add("Toru attack","",noone,"",true,true,true,"",battle_process_choose_CMD,false,"battle_process_toru_atk_CMD"); 
	else
		scene_choice_add("[?]Toru attack","",noone,"",true,true,true,"No enough mana.",battle_process_choose_CMD,false,"battle_process_toru_atk_CMD");
		
		
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_HEAL,char_get_skill_level(battle_get_current_action_char(),SKILL_HEAL))
		scene_choice_add("“Heal”","",noone,"",true,true,true,skill_get_tooltip(SKILL_HEAL),battle_process_choose_CMD,true,"battle_process_heal_CMD");
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_HEAL)
		scene_choice_add("[?]“Heal”","",noone,"",true,true,true,"No enough mana.",battle_process_choose_CMD,true,"battle_process_heal_CMD");	
	
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_CLEANSE,char_get_skill_level(battle_get_current_action_char(),SKILL_CLEANSE))
		scene_choice_add("“Cleanse”","",noone,"",true,true,true,skill_get_tooltip(SKILL_CLEANSE),battle_process_choose_CMD,true,"battle_process_cleanse_CMD");
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_CLEANSE)
		scene_choice_add("[?]“Cleanse”","",noone,"",true,true,true,"No enough mana.",battle_process_choose_CMD,true,"battle_process_cleanse_CMD");
		
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_BARRIER,char_get_skill_level(battle_get_current_action_char(),SKILL_BARRIER))
		scene_choice_add("“Barrier”","",noone,"",true,true,true,skill_get_tooltip(SKILL_BARRIER),battle_process_barrier_CMD);
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_BARRIER)
		scene_choice_add("[?]“Barrier”","",noone,"",true,true,true,"No enough mana.",battle_process_barrier_CMD);
	
	
	
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_FIREBALL,char_get_skill_level(battle_get_current_action_char(),SKILL_FIREBALL))
		scene_choice_add("“Fireball”","",noone,"",true,true,true,skill_get_tooltip(SKILL_FIREBALL),battle_process_choose_CMD,false,"battle_process_fireball_CMD"); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_FIREBALL)
		scene_choice_add("[?]“Fireball”","",noone,"",true,true,true,"No enough mana.",battle_process_choose_CMD,false,"battle_process_fireball_CMD");



	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_OCCULT,char_get_skill_level(battle_get_current_action_char(),SKILL_OCCULT))
		scene_choice_add("“Occult”","",noone,"",true,true,true,skill_get_tooltip(SKILL_OCCULT),battle_process_occult_CMD); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_OCCULT)
		scene_choice_add("[?]“Occult”","",noone,"",true,true,true,"No enough mana.",battle_process_occult_CMD);



	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_THUNDERSTORM,char_get_skill_level(battle_get_current_action_char(),SKILL_THUNDERSTORM))
		scene_choice_add("“Thunderstorm”","",noone,"",true,true,true,skill_get_tooltip(SKILL_THUNDERSTORM),battle_process_thunderstorm_CMD); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_THUNDERSTORM)
		scene_choice_add("[?]“Thunderstorm”","",noone,"",true,true,true,"No enough mana.",battle_process_thunderstorm_CMD);

	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_BLIZZARD,char_get_skill_level(battle_get_current_action_char(),SKILL_BLIZZARD))
		scene_choice_add("“Blizzard”","",noone,"",true,true,true,skill_get_tooltip(SKILL_BLIZZARD),battle_process_blizzard_CMD); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_BLIZZARD)
		scene_choice_add("[?]“Blizzard”","",noone,"",true,true,true,"No enough mana.",battle_process_blizzard_CMD);
	
	
	
	
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_DOUBLE_HIT,char_get_skill_level(battle_get_current_action_char(),SKILL_DOUBLE_HIT))
		scene_choice_add("“Double Hit”","",noone,"",true,true,true,skill_get_tooltip(SKILL_DOUBLE_HIT),battle_process_choose_CMD,false,"battle_process_double_hit_CMD"); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_DOUBLE_HIT)
		scene_choice_add("[?]“Double Hit”","",noone,"",true,true,true,"No enough stamina.",battle_process_choose_CMD,false,"battle_process_double_hit_CMD");
		
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_TACKLE,char_get_skill_level(battle_get_current_action_char(),SKILL_TACKLE))
		scene_choice_add("“Tackle”","",noone,"",true,true,true,skill_get_tooltip(SKILL_TACKLE),battle_process_choose_CMD,false,"battle_process_tackle_CMD"); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_TACKLE)
		scene_choice_add("[?]“Tackle”","",noone,"",true,true,true,"No enough stamina.",battle_process_choose_CMD,false,"battle_process_tackle_CMD");
		
		
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_WRECK,char_get_skill_level(battle_get_current_action_char(),SKILL_WRECK))
		scene_choice_add("“Wreck”","",noone,"",true,true,true,skill_get_tooltip(SKILL_WRECK),battle_process_choose_CMD,false,"battle_process_wreck_CMD"); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_WRECK)
		scene_choice_add("[?]“Wreck”","",noone,"",true,true,true,"No enough stamina.",battle_process_choose_CMD,false,"battle_process_wreck_CMD");
		
		
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_HIDE,char_get_skill_level(battle_get_current_action_char(),SKILL_HIDE))
		scene_choice_add("“Hide”","",noone,"",true,true,true,skill_get_tooltip(SKILL_HIDE),battle_process_hide_CMD); 
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_HIDE)
		scene_choice_add("[?]“Hide”","",noone,"",true,true,true,"No enough stamina.",battle_process_hide_CMD);

	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_SPEED_UP,char_get_skill_level(battle_get_current_action_char(),SKILL_SPEED_UP))
		scene_choice_add("“Speed Up”","",noone,"",true,true,true,skill_get_tooltip(SKILL_SPEED_UP),battle_process_choose_CMD,true,"battle_process_speed_up_CMD");
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_SPEED_UP)
		scene_choice_add("[?]“Speed Up”","",noone,"",true,true,true,"No enough stamina.",battle_process_choose_CMD,true,"battle_process_speed_up_CMD");
		
	if skill_get_cost_enough(battle_get_current_action_char(),SKILL_DEFENSE_UP,char_get_skill_level(battle_get_current_action_char(),SKILL_DEFENSE_UP))
		scene_choice_add("“Defense Up”","",noone,"",true,true,true,skill_get_tooltip(SKILL_DEFENSE_UP),battle_process_choose_CMD,true,"battle_process_defense_up_CMD");
	else if char_get_skill_level(battle_get_current_action_char(),SKILL_DEFENSE_UP)
		scene_choice_add("[?]“Defense Up”","",noone,"",true,true,true,"No enough mana.",battle_process_choose_CMD,true,"battle_process_defense_up_CMD");
		
		
	
	scene_choiceCMD_add("Flirt",battle_process_choose_CMD,false,"battle_process_flirt_atk_CMD");
	
	scene_choiceCMD_add("Item",battle_process_choose_item_CMD);
	
	if obj_control.debug_enable scene_choiceCMD_add("DEBUG - KILL ALL",battle_process_kill_all_CMD);
	
	if obj_control.debug_enable scene_choiceCMD_add("DEBUG - SEDUCT ALL",battle_process_seduct_all_CMD);

	scene_choiceCMD_add("Wait.",battle_process_wait_CMD);
	
	obj_command_panel.choice_image_alpha_increasing = true;
	
	//draw_floating_text_debug("------Battle 'Player CMD script' is executed!------");
}