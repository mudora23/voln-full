///char_get_phy_damage(char_map);
/// @description
/// @param char_map
var number = power(char_get_STR(argument0)/7,1.2)*100+30;
if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;
if battle_char_get_buff_level(argument0,BUFF_HIDE) number*=1.2;
return number;