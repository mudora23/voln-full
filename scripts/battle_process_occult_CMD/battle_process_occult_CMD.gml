///scene_script_battle_process_occult_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
	
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	
	skill_spend_cost(char_source,SKILL_OCCULT,char_get_skill_level(char_source,SKILL_OCCULT));

	var target_list = battle_char_create_opposite_side_list(char_source);

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" casts",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" Occult",TAG_FONT_N,TAG_COLOR_TORU));
	ds_list_add(battlelog_pieces_list,text_add_markup(". ",TAG_FONT_N,TAG_COLOR_NORMAL));
	
	script_execute_add(0,postfx_color_cut,0.5,2,1.5,c_black,0.2);
	script_execute_add(0,postfx_ps_scr_occult,room_width/2,room_height/2);
	script_execute_add(0,postfx_hole,3,room_width/2,room_height/2,60,60,3);
	script_execute_add(0,posifx_radial_blur,room_width/2,room_height/2,0.3,3,0.3,0.03);

		
	for(var i = 0; i < ds_list_size(target_list);i++)
	{
		var char_target = target_list[| i];
		

		
		// performing
		var dodge_chance = char_get_dodge_chance(char_target);
		var hit_chance = char_get_hit_chance(char_source);
	

		//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_TORU);
	
		var misses = 0;
		var crit = 0;
	
		if percent_chance(80-dodge_chance+hit_chance)
		{

			if char_get_skill_level(char_source,SKILL_OCCULT) == 3
				var damage = char_get_toru_damage(char_source)* random_range(1.6,2.0);
			else if char_get_skill_level(char_source,SKILL_OCCULT) == 2
				var damage = char_get_toru_damage(char_source)* random_range(1.4,1.6);
			else
				var damage = char_get_toru_damage(char_source)* random_range(1.2,1.4);
			var damage_size_adjust = 1;
			var shake_size_adjust = 1;
			if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);crit++;damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
			var armor = char_get_toru_armor(char_target);
			var real_damage = damage * clamp(((100-armor)/100),0,1);
			
			script_execute_add(3,ds_map_replace_i,char_target,GAME_CHAR_HEALTH,max(0,char_target[? GAME_CHAR_HEALTH] - real_damage));

			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]-20,"-"+string(round(real_damage)),COLOR_HP,3,1.1*damage_size_adjust);
			
			ally_get_hit_port(char_target);
		
			//ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
			//ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),TAG_FONT_N,TAG_COLOR_DAMAGE));
			//ds_list_add(battlelog_pieces_list,text_add_markup(" damage",TAG_FONT_N,TAG_COLOR_NORMAL));
		
			char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
			script_execute_add(3,ds_map_replace_i,char_target,GAME_CHAR_SHAKE_AMOUNT,25*shake_size_adjust);

		}
		else
		{
			misses++;
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]-20,"MISSED",COLOR_HP,3,0.7);
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" and misses!",0));
		}
		
		
		if percent_chance(80-dodge_chance+hit_chance)
		{



			if char_get_skill_level(char_source,SKILL_OCCULT) >= 3
			{
				if percent_chance(70)
				{
					if percent_chance(char_get_resistance_chance(char_target)-hit_chance)
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,3.8,0.8);
					else
					{
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Tempt!",COLOR_LUST,3.8,0.8);
						script_execute_add(3.8,battle_char_debuff_apply,char_target,DEBUFF_TEMPT,3,2,char_get_char_id(char_source));
					}
				}
				var damage = char_get_lust_damage(char_source)* random_range(1.1,1.3);
			}
			else if char_get_skill_level(char_source,SKILL_OCCULT) == 2
			{
				if percent_chance(60)
				{
					if percent_chance(char_get_resistance_chance(char_target)-hit_chance)
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,3.8,0.8);
					{
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Tempt!",COLOR_LUST,3.8,0.8);
						script_execute_add(3.8,battle_char_debuff_apply,char_target,DEBUFF_TEMPT,2,2,char_get_char_id(char_source));
					}
				}
				var damage = char_get_lust_damage(char_source)* random_range(0.9,1.1);
			}
			else
			{
				if percent_chance(50)
				{
					if percent_chance(char_get_resistance_chance(char_target)-hit_chance)
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,3.8,0.8);
					else
					{
						draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Tempt!",COLOR_LUST,3.8,0.8);
						script_execute_add(3.8,battle_char_debuff_apply,char_target,DEBUFF_TEMPT,1,2,char_get_char_id(char_source));
					}
				}
				var damage = char_get_lust_damage(char_source)* random_range(0.7,0.9);
			}


			var damage_size_adjust = 1;
			var shake_size_adjust = 1;
			if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);crit++;damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
			var armor = char_get_lust_armor(char_target);
			var real_damage = damage * clamp(((100-armor)/100),0,1);
			
			script_execute_add(3,ds_map_replace_i,char_target,GAME_CHAR_LUST,max(0,char_target[? GAME_CHAR_LUST] + real_damage));

			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]+20,"+"+string(round(real_damage)),COLOR_LUST,3.5,1.1*damage_size_adjust);
			
			ally_get_hit_port(char_target);
		
			//ds_list_add(battlelog_pieces_list,text_add_markup(", and",TAG_FONT_N,TAG_COLOR_NORMAL));
			//ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),TAG_FONT_N,TAG_COLOR_FLIRT));
			//ds_list_add(battlelog_pieces_list,text_add_markup(" lust damage",TAG_FONT_N,TAG_COLOR_NORMAL));
		
			char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
			script_execute_add(3,ds_map_replace_i,char_target,GAME_CHAR_SHAKE_AMOUNT,25*shake_size_adjust);


		}
		else
		{
			misses++;
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]+20,"MISSED",COLOR_LUST,3.5,0.7);
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" and misses!",0));
		}
		
		if misses == 2 ds_list_add(battlelog_pieces_list,text_add_markup(" and misses.",TAG_FONT_N,TAG_COLOR_NORMAL));
		else
		{
			ds_list_add(battlelog_pieces_list,text_add_markup(".",TAG_FONT_N,TAG_COLOR_NORMAL));
			script_execute_add(3,voln_play_sfx,sfx_Clean_Hit_1);
		}
		if crit
		{
			script_execute_add(3,voln_play_sfx,sfx_Hit8);
		}

	}
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	
	ds_list_destroy(target_list);
	
	voln_play_sfx(sfx_Charge9_dark);
	script_execute_add(5,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}