///_jump_if_unread(label/posi,scr,only_if_this_chunk_is_unread);
/// @description
/// @param label/posi
/// @param scr
/// @param only_if_this_chunk_is_unread

if scene_is_current()
{
	if argument_count <= 2 || !chunk_mark_get(argument[2])
	{
		if argument_count > 1 scene_jump(argument[0],argument[1]);
		else if argument_count > 0 scene_jump(argument[0]);
		else scene_jump_next();
	}
	else scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);