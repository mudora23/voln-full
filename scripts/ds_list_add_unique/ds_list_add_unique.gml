///ds_list_add_unique(id,value);
/// @description - add the value to the list if its not in the list
/// @param id
/// @param value
if !ds_list_value_exists(argument[0],argument[1])
	ds_list_add(argument[0],argument[1]);