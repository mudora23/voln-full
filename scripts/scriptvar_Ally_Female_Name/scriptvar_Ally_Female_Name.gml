///scriptvar_Ally_Female_Name();
/// @description

if ally_get_number() >= 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var name_list = ds_list_create();
	for(var i = 0; i < ds_list_size(ally_list);i++)
	{
		var ally = ally_list[| i];
		if ally[? GAME_CHAR_GENDER] == GENDER_FEMALE
			ds_list_add(name_list, ally[? GAME_CHAR_DISPLAYNAME]);
	}
	
	if ds_list_size(name_list) > 0
	{
		ds_list_shuffle(name_list);
		var the_name = name_list[| 0];
	}
	else the_name = "";
	
	ds_list_destroy(name_list);

	return the_name;
}
else
	return "";
