///S_0008_SL12_REDSPOTTING_Battle();
/// @description
with obj_control
{
	
	scene_sprites_list_clear_except("bg",FADE_NORMAL_SEC);
	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);
	

	create_enemies(ally_get_number()+1,10,0,NAME_RED_ORK);

	var_map[? BATTLE_WIN_SCENE] = "S_0008";
	var_map[? BATTLE_WIN_SCENE_LABEL] = "S_0008_SL12_REDSPOTTING_WON";
	var_map[? BATTLE_LOSS_SCENE] = "S_0008";
	var_map[? BATTLE_LOSS_SCENE_LABEL] = "S_0008_SL12_REDSPOTTING_LOSS";
	
	script_execute_add(0,battle_process_setup);

}	