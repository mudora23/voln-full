///battle_char_get_effective_lust_ratio(char);
/// @description 
/// @param char

var char = argument0;
var lust_ratio = char[? GAME_CHAR_LUST] / char_get_lust_max(char);
var armor = char_get_lust_armor(char);
var real_lust_ratio = lust_ratio * ((100-armor)/100) * (1-char_get_dodge_chance(char)/100);

return real_lust_ratio;