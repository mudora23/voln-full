///game_map_get(GAME_MAP_KEY);
/// @description
/// @param GAME_MAP_KEY
var var_key = argument[0];
if ds_map_exists(obj_control.var_map,var_key)
	return obj_control.var_map[? var_key];
else
{
	//show_debug_message("ERROR: var_map_get(VAR_MAP_KEY) - key not exists! key: "+string(var_key));
	return NOT_EXISTS;
}