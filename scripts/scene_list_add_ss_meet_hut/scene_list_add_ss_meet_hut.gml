///scene_list_add_ss_meet_hut(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];

if ally_exists(NAME_KUDI) && char_get_char_opinion(NAME_KUDI,char_get_player()) >= 0
	ds_list_add(possible_list,"SS_MEET_HUT_1_1");
if ally_exists(NAME_KUDI) && char_get_char_opinion(NAME_KUDI,char_get_player()) >= 2
	ds_list_add(possible_list,"SS_MEET_HUT_2_1");
if ally_exists(NAME_KUDI) && pet_exists(NAME_YIM)
	ds_list_add(possible_list,"SS_MEET_HUT_3_1");
if ally_exists(NAME_KUDI) && char_get_char_opinion(NAME_KUDI,char_get_player()) >= 3
	ds_list_add(possible_list,"SS_MEET_HUT_4_1");
if ally_exists(NAME_KUDI) && pet_exists(NAME_YIM) && char_get_char_opinion(NAME_KUDI,char_get_player()) >= 10
	ds_list_add(possible_list,"SS_MEET_HUT_5_1");

return possible_list;

