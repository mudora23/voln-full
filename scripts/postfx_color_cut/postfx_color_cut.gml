///postfx_color_cut(fade_out_sec,stay_color_sec,fade_in_sec,color,alpha);
/// @description  
/// @param fade_out_sec
/// @param stay_color_sec
/// @param fade_in_sec
/// @param color
/// @param alpha

with obj_control
{
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_NAME_LIST],POSTFX_COLOR_CUT);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_LIST],argument[0]+argument[1]+argument[2]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_MAX_LIST],argument[0]+argument[1]+argument[2]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG0_LIST],argument[0]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG1_LIST],argument[1]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG2_LIST],argument[2]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG3_LIST],argument[3]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG4_LIST],argument[4]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG5_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG6_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG7_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG8_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG9_LIST],0);

}



