///battle_char_list_debuff_apply(char_list,debuff,level,turns,source_char_id[optional]);
/// @description
/// @param char_list
/// @param debuff
/// @param level
/// @param turns
/// @param source_char_id[optional]
for(var i = 0; i < ds_list_size(argument[0]);i++)
{
	if argument_count > 4
		battle_char_debuff_apply(ds_list_find_value(argument[0],i),argument[1],argument[2],argument[3],argument[4]);
	else
		battle_char_debuff_apply(ds_list_find_value(argument[0],i),argument[1],argument[2],argument[3]);
}