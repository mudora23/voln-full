///postfx_step();
with obj_control
{
	var name_list = obj_control.var_map[? VAR_POSTFX_LIST_NAME_LIST];
	var life_list = obj_control.var_map[? VAR_POSTFX_LIST_LIFE_LIST];
	var life_max_list = obj_control.var_map[? VAR_POSTFX_LIST_LIFE_MAX_LIST];
	var arg0_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG0_LIST];
	var arg1_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG1_LIST];
	var arg2_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG2_LIST];
	var arg3_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG3_LIST];
	var arg4_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG4_LIST];
	var arg5_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG5_LIST];
	var arg6_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG6_LIST];
	var arg7_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG7_LIST];
	var arg8_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG8_LIST];
	var arg9_list = obj_control.var_map[? VAR_POSTFX_LIST_ARG9_LIST];

	for(var i = 0; i < ds_list_size(name_list);i++)
	{
		// processing
		life_list[| i]-=(1/room_speed);
	
		// clear if over
		if life_list[| i] <= 0
		{
			// clear rain if over
			//if name_list[| i] == POSTFX_RAIN
			//{
			//	part_particles_clear(global.ps);
			//
			//}

			ds_list_delete(name_list,i);
			ds_list_delete(life_list,i);
			ds_list_delete(life_max_list,i);
			ds_list_delete(arg0_list,i);
			ds_list_delete(arg1_list,i);
			ds_list_delete(arg2_list,i);
			ds_list_delete(arg3_list,i);
			ds_list_delete(arg4_list,i);
			ds_list_delete(arg5_list,i);
			ds_list_delete(arg6_list,i);
			ds_list_delete(arg7_list,i);
			ds_list_delete(arg8_list,i);
			ds_list_delete(arg9_list,i);
			i--;
		}
		else
		{
			// POSTFX_PUNCH_IN
			// arg0 x
			// arg1 y
			// arg2 amount
			#macro POSTFX_PUNCH_IN "POSTFX_PUNCH_IN"
			if name_list[| i] == POSTFX_PUNCH_IN
			{
				var process_ratio = (life_max_list[| i] - life_list[| i])/life_max_list[| i];
			
				var uni_time = shader_get_uniform(shd_radial_blur,"time");
				var var_time_var = life_max_list[| i] - life_list[| i];
				var uni_center_pos = shader_get_uniform(shd_radial_blur,"center_pos");
				var var_center_pos_x = arg0_list[| i];
				var var_center_pos_y = arg1_list[| i];
				var uni_resolution = shader_get_uniform(shd_radial_blur,"resolution");
				var var_resolution_x = window_get_width(); 
				var var_resolution_y = window_get_height();
				var var_resolution_ratio = window_get_size_ratio();
				var uni_radial_blur_offset = shader_get_uniform(shd_radial_blur,"radial_blur_offset");
				var ratio = ratio_to_a_point(process_ratio,0.2,0,1);
				var var_radial_blur_offset = arg2_list[| i] * ratio;
				var uni_radial_brightness = shader_get_uniform(shd_radial_blur,"radial_brightness");
				var var_radial_brightness = 1 + arg2_list[| i]* ratio;
				var shader_enabled = true;
				var full_screen_effect = true;

				if shader_enabled shader_set(shd_radial_blur);
				    shader_set_uniform_f(uni_time, var_time_var);
				    shader_set_uniform_f(uni_center_pos, var_center_pos_x, var_center_pos_y);
				    shader_set_uniform_f(uni_resolution, var_resolution_x, var_resolution_y);
				    shader_set_uniform_f(uni_radial_blur_offset, var_radial_blur_offset);
				    shader_set_uniform_f(uni_radial_brightness, var_radial_brightness);
					draw_surface_ext(application_surface,0,0,var_resolution_ratio,var_resolution_ratio,0,c_white,1);
				shader_reset();
			

			}
		
			// POSTFX_RADIAL_BLUR
			// arg0 x
			// arg1 y
			// arg2 fade_in_sec
			// arg3 stay_sec
			// arg4 fade_out_sec
			// arg5 amount
			#macro POSTFX_RADIAL_BLUR "POSTFX_RADIAL_BLUR"
			if name_list[| i] == POSTFX_RADIAL_BLUR
			{
				var exist_time = life_max_list[| i] - life_list[| i];
				if exist_time < arg2_list[| i]
				{
					var ratio = exist_time / arg2_list[| i];
				}
				else if exist_time > arg2_list[| i] + arg3_list[| i]
				{
					var ratio = 1-((exist_time - arg2_list[| i] - arg3_list[| i]) / arg4_list[| i]);
				}
				else
					var ratio = 1;
			
				var uni_time = shader_get_uniform(shd_radial_blur,"time");
				var var_time_var = life_max_list[| i] - life_list[| i];
				var uni_center_pos = shader_get_uniform(shd_radial_blur,"center_pos");
				var var_center_pos_x = arg0_list[| i];
				var var_center_pos_y = arg1_list[| i];
				var uni_resolution = shader_get_uniform(shd_radial_blur,"resolution");
				var var_resolution_x = window_get_width(); 
				var var_resolution_y = window_get_height();
				var var_resolution_ratio = window_get_size_ratio();
				var uni_radial_blur_offset = shader_get_uniform(shd_radial_blur,"radial_blur_offset");
				//var ratio = ratio_to_a_point(process_ratio,0.2,0,1);
				var var_radial_blur_offset = arg5_list[| i] * ratio;
				var uni_radial_brightness = shader_get_uniform(shd_radial_blur,"radial_brightness");
				var var_radial_brightness = 1 + arg5_list[| i]* ratio;
				var shader_enabled = true;
				var full_screen_effect = true;

				if shader_enabled shader_set(shd_radial_blur);
				    shader_set_uniform_f(uni_time, var_time_var);
				    shader_set_uniform_f(uni_center_pos, var_center_pos_x, var_center_pos_y);
				    shader_set_uniform_f(uni_resolution, var_resolution_x, var_resolution_y);
				    shader_set_uniform_f(uni_radial_blur_offset, var_radial_blur_offset);
				    shader_set_uniform_f(uni_radial_brightness, var_radial_brightness);
					draw_surface_ext(application_surface,0,0,var_resolution_ratio,var_resolution_ratio,0,c_white,1);
				shader_reset();
			

			}
		
		
		
			// POSTFX_WHITE_CUT
			// arg0 fade_out_sec
			// arg1 stay_white_sec
			// arg2 fade_in_sec
			#macro POSTFX_WHITE_CUT "POSTFX_WHITE_CUT"
			if name_list[| i] == POSTFX_WHITE_CUT
			{
				var exist_time = life_max_list[| i] - life_list[| i];
				if exist_time < arg0_list[| i]
				{
					var alpha = exist_time / arg0_list[| i];
				}
				else if exist_time > arg0_list[| i] + arg1_list[| i]
				{
					var alpha = 1-((exist_time - arg0_list[| i] - arg1_list[| i]) / arg2_list[| i]);
				}
				else
					var alpha = 1;
				
				draw_set_alpha(alpha);
				draw_rectangle_color(0,0,room_width*window_get_size_ratio(),room_height*window_get_size_ratio(),c_dkwhite,c_dkwhite,c_dkwhite,c_dkwhite,false);
				draw_reset();

		
			}
		
			// POSTFX_COLOR_CUT
			// arg0 fade_out_sec
			// arg1 stay_color_sec
			// arg2 fade_in_sec
			// arg3 color
			// arg4 alpha
			#macro POSTFX_COLOR_CUT "POSTFX_COLOR_CUT"
			if name_list[| i] == POSTFX_COLOR_CUT
			{
				var exist_time = life_max_list[| i] - life_list[| i];
				var alpha_max = arg4_list[| i];
				if exist_time < arg0_list[| i]
				{
					var alpha = exist_time / arg0_list[| i];
				}
				else if exist_time > arg0_list[| i] + arg1_list[| i]
				{
					var alpha = 1-((exist_time - arg0_list[| i] - arg1_list[| i]) / arg2_list[| i]);
				}
				else
					var alpha = 1;
				
				draw_set_alpha(alpha*alpha_max);
				var color = arg3_list[| i];
				draw_rectangle_color(0,0,room_width*window_get_size_ratio(),room_height*window_get_size_ratio(),color,color,color,color,false);
				draw_reset();

		
			}
		
			// POSTFX_SHAKE
			// arg0 shake_fade_in_sec
			// arg1 stay_shake_sec
			// arg2 shake_fade_out_sec
			// arg3 shake_amount
			#macro POSTFX_SHAKE "POSTFX_SHAKE"
			if name_list[| i] == POSTFX_SHAKE
			{
				var var_resolution_x = window_get_width(); 
				var var_resolution_y = window_get_height();
				var var_resolution_ratio = window_get_size_ratio();
				var exist_time = life_max_list[| i] - life_list[| i];
				if exist_time < arg0_list[| i]
				{
					var ratio = exist_time / arg0_list[| i];
				}
				else if exist_time > arg0_list[| i] + arg1_list[| i]
				{
					var ratio = 1-((exist_time - arg0_list[| i] - arg1_list[| i]) / arg2_list[| i]);
				}
				else
					var ratio = 1;
				var amount = arg3_list[| i];
				draw_surface_ext(application_surface,random_range(-amount*ratio,amount*ratio),random_range(-amount*ratio,amount*ratio),var_resolution_ratio,var_resolution_ratio,0,c_white,1);
		
			}
		
			// POSTFX_LIGHTNING
			// arg0 x
			// arg1 y
			#macro POSTFX_LIGHTNING "POSTFX_LIGHTNING"
			if name_list[| i] == POSTFX_LIGHTNING
			{
				var xx = arg0_list[| i]; 
				var yy = arg1_list[| i];
				var spr_name = arg2_list[| i];
				var img_number = arg3_list[| i];
				var alpha = life_list[| i] / life_max_list[| i];
			
				if asset_get_type(spr_name) == asset_sprite
				{
					
					draw_sprite_ext(asset_get_index(spr_name),img_number,xx*window_get_size_ratio(),yy*window_get_size_ratio(),window_get_size_ratio(),window_get_size_ratio(),0,c_white,alpha);
				}
		
			}
		
			// POSTFX_RAIN
			// arg0 amount_per_sec
			#macro POSTFX_RAIN "POSTFX_RAIN"
			if name_list[| i] == POSTFX_RAIN
			{
				var amount_per_sec = arg0_list[| i]; 
				var life = life_list[| i];
				var life_max = life_max_list[| i];
				//if life + (1/room_speed) == life_max
				//{
				//	advanced_times = room_speed * 2;
				//}
				//else
				//{
				//	advanced_times =  1;
				//}
			
				//part_system_automatic_update(global.ps, false);
			
				//repeat (advanced_times)
			
				//{
					part_emitter_region(global.ps, global.ps_rain, 0, room_width*2, -10, -10, ps_shape_line, ps_distr_linear); // Position is not actually based on "draw GUI"
					part_emitter_burst(global.ps, global.ps_rain, global.ps_rain, round_chance(amount_per_sec*(1/room_speed)));
				//	if advanced_times > 1 part_system_update(global.ps);
				//}
			
				//part_system_automatic_update(global.ps, true);

			}
		
		
			// POSTFX_HOLE
			// arg0 x
			// arg1 y
			// arg2 amount1_per_sec
			// arg3 amount2_per_sec
			// arg4 amount3_per_sec
			#macro POSTFX_HOLE "POSTFX_HOLE"
			if name_list[| i] == POSTFX_HOLE
			{
				var xp = arg0_list[| i]; 
				var yp = arg1_list[| i]; 
				var amount1_per_sec = arg2_list[| i]; 
				var amount2_per_sec = arg3_list[| i]; 
				var amount3_per_sec = arg4_list[| i]; 
				var life = life_list[| i];
				var life_max = life_max_list[| i];

				part_emitter_region(global.ps, global.ps_hole, xp, xp, yp, yp, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.ps_hole, global.ps_hole, round_chance(amount1_per_sec*(1/room_speed)));
				part_emitter_region(global.ps, global.ps_hole2, xp, xp, yp, yp, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.ps_hole2, global.ps_hole2, round_chance(amount2_per_sec*(1/room_speed)));
				part_emitter_region(global.ps, global.ps_hole3, xp, xp, yp, yp, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.ps_hole3, global.ps_hole3, round_chance(amount3_per_sec*(1/room_speed)));

			}
		
		
			// POSTFX_DOUBLEHIT
			// arg0 x
			// arg1 y
			#macro POSTFX_DOUBLEHIT "POSTFX_DOUBLEHIT"
			if name_list[| i] == POSTFX_DOUBLEHIT
			{
				var xp = arg0_list[| i]; 
				var yp = arg1_list[| i]; 

				part_emitter_region(global.ps, global.ps_doublehit1, xp+116, xp+118, yp-113, yp-111, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.ps_doublehit1, global.ps_doublehit1, 1);
			
				life_list[| i] = 0;

			}
			
			// POSTFX_DOUBLEHIT
			// arg0 x
			// arg1 y
			#macro POSTFX_BURN "POSTFX_BURN"
			if name_list[| i] == POSTFX_BURN
			{
				var xp = arg0_list[| i]; 
				var yp = arg1_list[| i]; 

				part_emitter_region(global.ps, global.burn_pt0, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.burn_pt0, global.burn_pt0, 1);
				part_emitter_region(global.ps, global.burn_pt1, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.burn_pt1, global.burn_pt1, 1);
				part_emitter_region(global.ps, global.burn_pt2, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
				part_emitter_burst(global.ps, global.burn_pt2, global.burn_pt2, 1);
			
				//life_list[| i] = 0;

			}
			
			
			#macro POSTFX_BLIZZARD "POSTFX_BLIZZARD"
			if name_list[| i] == POSTFX_BLIZZARD
			{
				var amount_per_sec = arg0_list[| i]; 
				var life = life_list[| i];
				var life_max = life_max_list[| i];
				
				part_emitter_region(global.ps, global.blizzard1, 0, room_width*2, -10, -10, ps_shape_line, ps_distr_linear); // Position is not actually based on "draw GUI"
				part_emitter_burst(global.ps, global.blizzard1, global.blizzard1, round_chance(amount_per_sec*(1/room_speed)));
				part_emitter_region(global.ps, global.blizzard2, 0, room_width*2, -10, -10, ps_shape_line, ps_distr_linear); // Position is not actually based on "draw GUI"
				part_emitter_burst(global.ps, global.blizzard2, global.blizzard2, round_chance(amount_per_sec*(1/room_speed)));
				


			}
	
	
		}
	}














	// testing
	//if keyboard_check(ord("B"))
	//	postfx_ps_burn(mouse_x,mouse_y);



	///postfix_punch_in(x,y,amount[0.05]);

}