///scriptvar_PC_Nads_Size_Total_N();
/// @description
var num = var_map_get(PC_Nads_N) * var_map_get(PC_Nads_Size_N);
var returning_str = string(floor(num));
if num - floor(num) >= 0.1
	returning_str += "." + string(floor((num-floor(num))*10));
return returning_str;