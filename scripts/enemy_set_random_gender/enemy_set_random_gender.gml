///enemy_set_random_gender();
for(var i = 0; i < enemy_get_number();i++)
{
	var enemy = enemy_get_char(i);
	if enemy[? GAME_CHAR_GENDER] == GENDER_NONE
		enemy[? GAME_CHAR_GENDER] = choose(GENDER_MALE,GENDER_FEMALE);
}

