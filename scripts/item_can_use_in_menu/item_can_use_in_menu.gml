///item_can_use_in_menu(name);
/// @description
/// @param name

with obj_control
{

	var name = argument[0];
	var item_map = item_get_map(name);
	if item_map != noone
		var item_use_in_menu = item_map[? ITEM_USE_IN_MENU];
	else
		var item_use_in_menu = false;
	
	return item_use_in_menu;
	
}