///_aes_BlockCopy(from)
var w = floor(sqrt(array_length_1d(argument0)));

var i,j,a=argument0,b;

for (i=0;i<w;++i) {
    for (j=0;j<w;++j) {
        b[i,j] = a[@ (i*w)+j];
    }
}
return b;

