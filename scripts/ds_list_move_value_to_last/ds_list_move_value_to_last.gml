///ds_list_move_value_to_last(list,value);
/// @description
/// @param list
/// @param value
var the_index = ds_list_find_index(argument0,argument1);
if the_index >= 0
{
	ds_list_delete(argument0,the_index);
	ds_list_add(argument0,argument1);
}