///scene_sprites_step();
with obj_control
{
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	var sprite_index_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME];
	var sprite_index_target_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_TARGET_NAME];
	var sprite_x_list = obj_control.var_map[? VAR_SPRITE_LIST_X];
	var sprite_x_target_list = obj_control.var_map[? VAR_SPRITE_LIST_X_TARGET];
	var sprite_y_list = obj_control.var_map[? VAR_SPRITE_LIST_Y];
	var sprite_y_target_list = obj_control.var_map[? VAR_SPRITE_LIST_Y_TARGET];
	var sprite_alpha_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA];
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	var sprite_fade_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_FADE_SPEED];
	var sprite_slide_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_SLIDE_SPEED];
	var sprite_order_list = obj_control.var_map[? VAR_SPRITE_LIST_ORDER];
	var sprite_size_ratio_list = obj_control.var_map[? VAR_SPRITE_LIST_SIZE_RATIO];

	// order sorting
	scene_sprite_list_sort();
	
	for(var i = 0;i < ds_list_size(sprite_id_list);i++)
	{
		var sprite_id_i = sprite_id_list[| i];
		var sprite_index_i = sprite_index_list[| i];
		var sprite_index_target_i = sprite_index_target_list[| i];
		var sprite_x_i = sprite_x_list[| i];
		var sprite_x_target_i = sprite_x_target_list[| i];
		var sprite_y_i = sprite_y_list[| i];
		var sprite_y_target_i = sprite_y_target_list[| i];
		var sprite_alpha_i = sprite_alpha_list[| i];
		var sprite_alpha_target_i = sprite_alpha_target_list[| i];
		var sprite_fade_speed_i = sprite_fade_speed_list[| i];
		var sprite_slide_speed_i = sprite_slide_speed_list[| i];
		var sprite_order_i = sprite_order_list[| i];
		var sprite_size_ratio_i = sprite_size_ratio_list[| i];
	
		// processing
		sprite_x_list[| i] = smooth_approach_room_speed(sprite_x_i,sprite_x_target_i,sprite_slide_speed_i);
		sprite_y_list[| i] = smooth_approach_room_speed(sprite_y_i,sprite_y_target_i,sprite_slide_speed_i);
		

		
		////show_debug_message("sprite_alpha_i: "+string(sprite_alpha_i));
		////show_debug_message("sprite_alpha_target_i: "+string(sprite_alpha_target_i));
		////show_debug_message("sprite_fade_speed_i: "+string(sprite_fade_speed_i));
		////show_debug_message("sprite_fade_speed_i*(1/room_speed): "+string(sprite_fade_speed_i*(1/room_speed)));
		
		sprite_alpha_list[| i] = approach(sprite_alpha_i,sprite_alpha_target_i,sprite_fade_speed_i*(1/room_speed));

		if sprite_index_i != sprite_index_target_i
		{
			if sprite_alpha_i != 0
			{
				sprite_alpha_target_list[| i] = 0;
			}
			else
			{
				sprite_index_list[| i] = sprite_index_target_i;
				sprite_alpha_list[| i] = 1;
				sprite_alpha_target_list[| i] = 1;
			}
		}
		
		if scene_cg_list_size() > 0
			var ymax = room_height;
		else
			var ymax = obj_main.y+obj_main.area_height;

		// drawing
		if asset_get_type(sprite_index_list[| i]) == asset_sprite
		{
			
			draw_sprite_in_visible_area(asset_get_index(sprite_index_list[| i]),clothes_sets_get_image_index(sprite_index_list[| i]),sprite_x_list[| i],sprite_y_list[| i],sprite_size_ratio_list[| i],sprite_size_ratio_list[| i],c_white,sprite_alpha_list[| i]*cond(sprite_id_list[| i]=="bg",obj_control.bg_gui_image_alpha,1),0,0,room_width,ymax);
			var spr_xx = sprite_x_list[| i];
			var spr_yy = sprite_y_list[| i];
			var spr_width = sprite_get_width(asset_get_index(sprite_index_list[| i]))*sprite_size_ratio_list[| i];
			var spr_height = sprite_get_height(asset_get_index(sprite_index_list[| i]))*sprite_size_ratio_list[| i];

			if !menu_exists() && mouse_over_area(spr_xx+spr_width*0.2,spr_yy,spr_width*0.6,min(spr_height,obj_main.y-sprite_y_list[| i])) && string_count("spr_bg",sprite_index_list[| i]) == 0
				obj_control.mouse_on_examine_sprite = asset_get_index(sprite_index_list[| i]);
		}
		
		if sprite_index_list[| i] != sprite_index_target_list[| i]
		{
			draw_sprite_in_visible_area(asset_get_index(sprite_index_target_list[| i]),clothes_sets_get_image_index(sprite_index_target_list[| i]),sprite_x_list[| i],sprite_y_list[| i],sprite_size_ratio_list[| i],sprite_size_ratio_list[| i],c_white,1-sprite_alpha_list[| i]*cond(sprite_id_list[| i]=="bg",obj_control.bg_gui_image_alpha,1),0,0,room_width,ymax);
			//show_debug_message("srpite changing... new sprite name: "+string(sprite_index_target_list[| i])+" current alpha: "+string(1-sprite_alpha_list[| i]));
		
			var spr_target_width = sprite_get_width(asset_get_index(sprite_index_target_list[| i]))*sprite_size_ratio_list[| i];
			var spr_target_height = sprite_get_height(asset_get_index(sprite_index_target_list[| i]))*sprite_size_ratio_list[| i];

		}
		
		// draw border if needed
		if asset_get_type(sprite_index_list[| i]) == asset_sprite
			if string_copy(sprite_index_list[| i],string_length(sprite_index_list[| i]) - 2,3) == "_wb"
				draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,sprite_x_list[| i],sprite_y_list[| i],spr_width,spr_height,sprite_alpha_list[| i],fa_right,fa_bottom);
		if sprite_index_list[| i] != sprite_index_target_list[| i]
			if string_copy(sprite_index_target_list[| i],string_length(sprite_index_target_list[| i]) - 2,3) == "_wb"
				draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,sprite_x_list[| i],sprite_y_list[| i],spr_target_width,spr_target_height,1-sprite_alpha_list[| i],fa_right,fa_bottom);

		//show_debug_message(string_copy(sprite_index_list[| i],string_length(sprite_index_list[| i]) - 2,3));
		
		// clean up
		if sprite_alpha_list[| i] == 0 && sprite_alpha_target_list[| i] == 0 && sprite_index_i == sprite_index_target_i
		{
		
			ds_list_delete(sprite_id_list,i);
			ds_list_delete(sprite_index_list,i);
			ds_list_delete(sprite_index_target_list,i);
			ds_list_delete(sprite_x_list,i);
			ds_list_delete(sprite_x_target_list,i);
			ds_list_delete(sprite_y_list,i);
			ds_list_delete(sprite_y_target_list,i);
			ds_list_delete(sprite_alpha_list,i);
			ds_list_delete(sprite_alpha_target_list,i);
			ds_list_delete(sprite_fade_speed_list,i);
			ds_list_delete(sprite_slide_speed_list,i);
			ds_list_delete(sprite_order_list,i);
			ds_list_delete(sprite_size_ratio_list,i);

			i--;

		}

	}

}