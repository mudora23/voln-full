///item_get_map(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	
	if ds_map_exists(item_wiki_map,name)
		var item_map = item_wiki_map[? name];
	else
		var item_map = noone;
	
	return item_map;
	
}