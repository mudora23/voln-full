///smooth_approach_room_speed(current, target, ratio_per_sec);
/// @description
/// @param current
/// @param target
/// @param ratio_per_sec

var diff = argument1-argument0;
if abs(diff) < 0.00005
{
   return argument1;
}
else 
{
   return argument0+sign(diff)*abs(diff)*argument2*(1/room_speed);
}


