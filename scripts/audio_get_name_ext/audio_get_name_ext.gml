///audio_get_name_ext(audio_in_any_form);
/// @description
/// @param audio_in_any_form
if is_string(argument0)
{
	if asset_get_type(argument0) == asset_sound
		return argument0;
	else
		return "";
}
else
{
	if script_exists(argument0)
		return audio_get_name(argument0);
	else
		return "";
}	