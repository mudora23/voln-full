///scene_get_type(story_chunk);
/// @description  
/// @param story_chunk

var type_map = obj_control.scene_list_type_wiki_map;

if ds_map_exists(type_map,argument0) return type_map[? argument0];
else return SCENE_TYPE_OTHER;
