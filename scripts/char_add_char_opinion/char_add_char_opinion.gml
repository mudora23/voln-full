///char_add_char_opinion(whos_opinion_name,opinion_about_whom_map,amount);
/// @description
/// @param whos_opinion_name
/// @param opinion_about_whom_map
/// @param amount

var whos_opinion_name = argument0;
var opinion_about_whom_map = argument1;
var amount = argument2;

var opinion_map = opinion_about_whom_map[? GAME_CHAR_OPINION_MAP];

if ds_map_exists(opinion_map,whos_opinion_name)
	opinion_map[? whos_opinion_name] += amount;
else
	opinion_map[? whos_opinion_name] = amount;


