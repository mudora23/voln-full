///ally_rejoin(ID/Name);
/// @description
/// @param ID/Name
with obj_control
{
	if !ally_is_full()
	{
		if !is_string(argument0) var char_map = char_id_get_char(argument0);
		else var char_map = ally_r_get_char(argument0);
	    var ally_list = var_map[? VAR_ALLY_LIST];
	    var ally_r_list = var_map[? VAR_ALLY_R_LIST];
	    var ally_r_index = ds_list_find_index(ally_r_list,char_map);
	    if ally_r_index>=0
	    {
			var new_char = ds_map_create();
			ds_map_copy(new_char,ally_r_list[| ally_r_index]);
	        ds_list_add(ally_list,new_char);

	        ds_list_mark_as_map(ally_list,ds_list_size(ally_list)-1);
	        ds_list_delete(ally_r_list,ally_r_index);
	    }
	}
	else
	{
		if !is_string(argument0) var char_map = char_id_get_char(argument0);
		else var char_map = ally_r_get_char(argument0);
	    var storage_list = var_map[? VAR_STORAGE_LIST];
	    var ally_r_list = var_map[? VAR_ALLY_R_LIST];
	    var ally_r_index = ds_list_find_index(ally_r_list,char_map);
	    if ally_r_index>=0
	    {

			var new_char = ds_map_create();
			ds_map_copy(new_char,ally_r_list[| ally_r_index]);
	        ds_list_add(storage_list,new_char);
			
			
	        ds_list_mark_as_map(storage_list,ds_list_size(storage_list)-1);
	        ds_list_delete(ally_r_list,ally_r_index);
	    }	
		
	}
}