///char_get_toru_regen(char_map);
/// @description
/// @param char_map
var number = (3+char_get_SPI(argument0)*1)/100*char_get_toru_max(argument0);
if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;
return number;