///char_get_char_opinion(whos_opinion_name,opinion_about_whom_map);
/// @description
/// @param whos_opinion_name
/// @param opinion_about_whom_map

var whos_opinion_name = argument0;
var opinion_about_whom_map = argument1;

var opinion_map = opinion_about_whom_map[? GAME_CHAR_OPINION_MAP];

if ds_map_exists(opinion_map,whos_opinion_name)
	return opinion_map[? whos_opinion_name];
else
	return 0;