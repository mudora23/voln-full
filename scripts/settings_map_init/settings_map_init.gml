///settings_map_init();
/// @description
if file_exists("settings.dat")
    settings_map = ds_map_json_load("settings.dat");
else
    settings_map = ds_map_create();

#macro SETTINGS_FULLSCREEN "SETTINGS_FULLSCREEN"
ds_map_add_unique(settings_map,SETTINGS_FULLSCREEN,true);

#macro SETTINGS_VSYNC "SETTINGS_VSYNC"
ds_map_add_unique(settings_map,SETTINGS_VSYNC,true);

#macro SETTINGS_AA "SETTINGS_AA"
ds_map_add_unique(settings_map,SETTINGS_AA,2);

#macro SETTINGS_MUSIC_LEVEL "SETTINGS_MUSIC_LEVEL"
ds_map_add_unique(settings_map,SETTINGS_MUSIC_LEVEL,60);

#macro SETTINGS_SFX_LEVEL "SETTINGS_SFX_LEVEL"
ds_map_add_unique(settings_map,SETTINGS_SFX_LEVEL,40);

#macro SETTINGS_COPY_TO_CLIPBOARD "SETTINGS_COPY_TO_CLIPBOARD"
ds_map_add_unique(settings_map,SETTINGS_COPY_TO_CLIPBOARD,false);

#macro SETTINGS_PLAYTIMETOTAL "SETTINGS_PLAYTIMETOTAL"
ds_map_add_unique(settings_map,SETTINGS_PLAYTIMETOTAL,0);



ds_map_json_save(settings_map,"settings.dat");

obj_control.settings_map_display_reset_needed = true;
