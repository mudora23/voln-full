///battle_process_phy_atk_CMD(char_id);
/// @description
/// @param char_id
with obj_control
{
	scene_choices_list_clear();
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	var font_tag = TAG_FONT_N;


	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],font_tag,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" attacks",font_tag,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],font_tag,char_target_color_tag));

	// performing
	var dodge_chance = char_get_dodge_chance(char_target);
	var hit_chance = char_get_hit_chance(char_source);
	
	if percent_chance(80-dodge_chance+hit_chance)
	{
		var damage = char_get_phy_damage(char_source) * random_range(0.9,1.1);
		var damage_size_adjust = 1;
		var shake_size_adjust = 1;
		if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);voln_play_sfx(sfx_Hit8);damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
		var armor = char_get_phy_armor(char_target);
		var real_damage = damage * clamp(((100-armor)/100),0,1);
		
		ds_list_add(battlelog_pieces_list,text_add_markup(" for",font_tag,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),font_tag,TAG_COLOR_DAMAGE));
		ds_list_add(battlelog_pieces_list,text_add_markup(" damage!",font_tag,TAG_COLOR_NORMAL));
		

		char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage);
		

		
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP,0,damage_size_adjust);
		ally_get_hit_port(char_target);
		
		var real_lust_amount = min(2,char_target[? GAME_CHAR_LUST]);
		char_target[? GAME_CHAR_LUST] = char_target[? GAME_CHAR_LUST] - real_lust_amount;
		
		if real_lust_amount > 0
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_lust_amount)),COLOR_LUST,0.2);

		
		
		
		char_target[? GAME_CHAR_COLOR] = COLOR_HP;
		char_target[? GAME_CHAR_SHAKE_AMOUNT] = 20*shake_size_adjust;

		var debug_message = "Basic phy atk is performed,";
		debug_message+= " source: ";
		debug_message+= string(damage);
		debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
		debug_message+= " target: ";
		debug_message+= string(armor);
		debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";
	
	}
	else
	{
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,0,0.7);
		ds_list_add(battlelog_pieces_list,text_add_markup(" and misses!",TAG_FONT_N,TAG_COLOR_NORMAL));
	}
	
	postfx_ps_onhit(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
		
		
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	
	var selected_sfx = battle_char_get_sfx_from_wiki(char_source,battle_process_phy_atk_CMD);
	if selected_sfx != noone && audio_exists(selected_sfx) voln_play_sfx(selected_sfx);
	else voln_play_sfx(choose(sfx_normal_hit1,sfx_normal_hit2,sfx_normal_hit3)); 
	
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);
	
	////show_debug_message("------Battle 'phy ATK CMD script' is executed!------");
}