///scriptvar_PC_Height_Desc();
/// @description
var num = var_map_get(PC_Height_N);
if num < 4.2
	return choose("you’re pitifully small","you’re a serious tripping hazard","you’re a dwarf’s dwarf","you’re teensie-weensie","you’re liable to slip into a crack in the floor and vanish forever","you’re unbelievably tiny");
else if num < 4.9
	return choose("you’re just tiny","you’re unusually runtish","you’re about the height of a prepubescent kid","you’re easy to misplace","you’re technically a dwarf","you don’t exactly cut an imposing figure");
else if num < 5.5
	return choose("you’re about the size of the average elf","you’re short, by the yåru standard anyway","you wouldn’t be of much use in contact sports","you’re fun-sized","you’re on the short side","you know most yåries are about your size");
else if num < 6
	return choose("you’re of average height","you’re of middling height","you’re as long as you need to be","you’re bigger than the average yårie, so that’s something","you’re not small, but not too big either","you’re longer than the average elf, height-wise anyway");
else if num < 7
	return choose("you cut an imposing figure","you’re easy to respect","you’re no runt","you’re long enough to war with the best of them","you’ve got a long reach","most people are shorter than you");
else
	return choose("you’re massive","you loom over most anyone","you’re inconveniently large","the world feels smaller than it should","you’re amazingly big","you’d better get used to ducking","folks tend to gawk");
