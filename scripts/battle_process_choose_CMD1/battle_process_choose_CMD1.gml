///battle_process_choose_CMD(CMD);
/// @description
/// @param CMD
with obj_control
{
	scene_choices_list_clear();
	obj_control.enemies_choice_enable = true;
	
	var char_map = battle_get_current_action_char();
	var char_CMD = script_get_index_ext(argument[0]);
	
	
		var enemy_list = var_map[? VAR_ENEMY_LIST];
		var bodyguard_char = battle_char_list_get_bodyguard_char(enemy_list);
		for(var i = 0; i < ds_list_size(enemy_list);i++)
		{
			var enemy = enemy_list[| i];
			if bodyguard_char == noone || bodyguard_char == enemy
			{
				if battle_char_get_combat_capable(enemy) && !battle_char_get_buff_level(enemy,BUFF_HIDE)
					scene_choiceCMD_add(enemy[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(enemy));
			}
		}
		scene_choiceCMD_add("Cancel",battle_process_player);
		
		
	/*
	
	if char_map[? GAME_CHAR_OWNER] == OWNER_PLAYER && char_map[? GAME_CHAR_AI] == AI_NONE
	{
		var enemy_list = var_map[? VAR_ENEMY_LIST];
		for(var i = 0; i < ds_list_size(enemy_list);i++)
		{
			var enemy = enemy_list[| i];
			if battle_char_get_combat_capable(enemy) && !battle_char_get_buff_level(enemy,BUFF_HIDE)
				scene_choiceCMD_add(enemy[? GAME_CHAR_DISPLAYNAME],char_CMD,char_get_char_id(enemy));
		}
		scene_choiceCMD_add("Cancel",battle_process_player);
		
		//show_debug_message("------Battle 'choose (Player) CMD script' is executed!------");
		//show_debug_message("------Battle 'choose (Player) CMD script' char_CMD script is "+string(argument[0]));
	}
	else if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
	{
		var char_list = ds_list_create();
		for(var i = 0; i < ds_list_size(var_map[? VAR_ALLY_LIST]);i++)
			ds_list_add(char_list,ds_list_find_value(var_map[? VAR_ALLY_LIST],i));
		ds_list_add(char_list,var_map_get(VAR_PLAYER));

		if char_CMD == "battle_process_flirt_atk_CMD"
			var the_idnex = battle_clist_get_highest_lust_targetcapable_char_i(char_list);
		else
			var the_idnex = battle_clist_get_lowest_hp_targetcapable_char_i(char_list);
	
		if the_idnex >= 0 script_execute(char_CMD,the_idnex);
		else battle_process_wait_CMD();
		
		ds_list_destroy(char_list);
	}

	else
	{
		var char_list = var_map[? VAR_ENEMY_LIST];
		
		if char_CMD == "battle_process_flirt_atk_CMD"
			var the_idnex = battle_clist_get_highest_lust_targetcapable_char_i(char_list);
		else
			var the_idnex = battle_clist_get_lowest_hp_targetcapable_char_i(char_list);
	
		if the_idnex >= 0 script_execute(char_CMD,the_idnex);
		else battle_process_wait_CMD();
		
	}
	*/
}
