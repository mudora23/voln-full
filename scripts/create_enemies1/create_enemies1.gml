///create_enemies(enemy_number,variance_chance,power_level_offset,enemy1,enemy2...);
/// @description
/// @param enemy_number
/// @param variance_chance
/// @param power_level_offset
/// @param enemy1
/// @param enemy2...
with obj_control
{
    var variance_chance = argument[1];
    var enemy_number = argument[0] + percent_chance(10) * choose(-1,1);
    enemy_number = clamp(enemy_number,1,5);
    var power_level_offset = argument[2];
    
    // power level
    var ally_team_power_level = 0;
    var char = obj_control.var_map[? VAR_PLAYER];
    ally_team_power_level += char[? GAME_CHAR_LEVEL];
    
    var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
    for(var i = 0; i < ds_list_size(ally_list); i++)
    {
        var char = ally_list[| i];
        ally_team_power_level += char[? GAME_CHAR_LEVEL];
    }
    ally_team_power_level*=(1+0.1*ally_get_number());
    
    var target_enemy_power_level = ally_team_power_level + power_level_offset;
    var target_one_enemy_power_level = (target_enemy_power_level / (0.9+0.1*enemy_number)) / enemy_number;

    
    // selection
    var selection_list = ds_list_create();
    if argument_count > 3 ds_list_add(selection_list,argument[3]);
    if argument_count > 4 ds_list_add(selection_list,argument[4]);
    if argument_count > 5 ds_list_add(selection_list,argument[5]);
    if argument_count > 6 ds_list_add(selection_list,argument[6]);
    if argument_count > 7 ds_list_add(selection_list,argument[7]);
    if argument_count > 8 ds_list_add(selection_list,argument[8]);
    if argument_count > 9 ds_list_add(selection_list,argument[9]);
    if argument_count > 10 ds_list_add(selection_list,argument[10]);
    if argument_count > 11 ds_list_add(selection_list,argument[11]);
    if argument_count > 12 ds_list_add(selection_list,argument[12]);
    if argument_count > 13 ds_list_add(selection_list,argument[13]);
    if argument_count > 14 ds_list_add(selection_list,argument[14]);
    if argument_count > 15 ds_list_add(selection_list,argument[15]);
    ds_list_shuffle(selection_list);
    
	//show_debug_message("Generating enemies: selection list size "+string(ds_list_size(selection_list)));
	
    // get enemy_number sets of enemies

    while ds_list_size(selection_list) < enemy_number
    {
		
        var current_size = ds_list_size(selection_list);
        for(var i = 0; i < current_size; i++)
        {
            ds_list_add(selection_list,selection_list[| i]);
        }
    }
	//show_debug_message("Generating enemies: selection list size is no longer less than enemy_number");
    
    while ds_list_size(selection_list) > enemy_number
    {
        ds_list_delete(selection_list,ds_list_size(selection_list)-1);  
    }
	//show_debug_message("Generating enemies: selection list size is no longer larger than enemy_number");

    ds_list_shuffle(selection_list);
    
    
    // select list -> slect list (GAME_CHAR_NAME)
    var selection_char_name_list = ds_list_create();
    
    while(ds_list_size(selection_list) > 0)
    {
	
		var key = ds_map_find_first(char_wiki_map);
        while(!is_undefined(key))
        {
            var char_map = char_wiki_map[? key];
            
            var char_name_index = ds_list_find_index(selection_list,char_map[? GAME_CHAR_NAME]);
            var char_name_type_index = ds_list_find_index(selection_list,char_map[? GAME_CHAR_NAME_TYPE]);
            
            if char_name_index >= 0
            {
                ds_list_add(selection_char_name_list,char_map[? GAME_CHAR_NAME]);
                ds_list_delete(selection_list,char_name_index);
            }
            else if char_name_type_index >= 0
            {
                ds_list_add(selection_char_name_list,char_map[? GAME_CHAR_NAME]);
                ds_list_delete(selection_list,char_name_type_index);
            }
   
            key = ds_map_find_next(char_wiki_map,key);
        }
    }
	//show_debug_message("Generating enemies: selection list is clear");
    
    // adding enemies
	var enemy_level_sum_check = 0;
	var enemy_num_sum_check = 0;
	
    for(var i = 0; i < ds_list_size(selection_char_name_list); i++)
    {
        var the_char_name = selection_char_name_list[| i];
        var the_char = char_wiki_map[? the_char_name];
        var the_char_level_min = the_char[? GAME_CHAR_LEVEL_MIN];
        var the_char_level_max = the_char[? GAME_CHAR_LEVEL_MAX];

		var the_char_level_to_add = clamp(target_one_enemy_power_level,the_char_level_min,the_char_level_max);
        enemy_add_char(the_char_name,false,the_char_level_to_add);
		
		enemy_level_sum_check += the_char_level_to_add;
		enemy_num_sum_check++;
		
		if enemy_level_sum_check * (0.9+enemy_num_sum_check*0.1) >= ally_team_power_level
			break;
    }

	ds_list_destroy(selection_list);
	ds_list_destroy(selection_char_name_list);
}