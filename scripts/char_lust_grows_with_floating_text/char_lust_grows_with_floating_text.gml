///char_lust_grows_with_floating_text(char,amount);
var char = argument0;
var amount = argument1;
char[? GAME_CHAR_LUST] = min(100,char[? GAME_CHAR_LUST] + amount);	
draw_floating_text(char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(amount)),COLOR_LUST);