///CS_location();
/// @description

_label("start");
	//_execute(0,_show_message(),"CS_location-start"); 
	//_show_message("CS_location-start");
	//_execute(0,"scene_bg_location",location_get_current());

	/*if asset_get_type("SL"+string(obj_control.var_map[? VAR_LOCATION])) == asset_script
		_jump("SL"+string(obj_control.var_map[? VAR_LOCATION]));
	else */if file_exists(story_chunk_get_full_path(string(obj_control.var_map[? VAR_LOCATION])+"_DESC"))
		_dialog(string(obj_control.var_map[? VAR_LOCATION])+"_DESC",true);
	else if file_exists(story_chunk_get_full_path(location_get_region(obj_control.var_map[? VAR_LOCATION])+"_DESC"))
		_dialog(location_get_region(obj_control.var_map[? VAR_LOCATION])+"_DESC",true);
	else
		_dialog("        You find a safe spot to park the wagon.",true);
	
	_choice("Camping...","Camping");
	_choice("Stay","",noone,"",true,true,true,"",CSX_arrived);
	_choice("Move","",noone,"",true,true,true,"",open_main_map);

	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Camping");
	if obj_control.debug_enable
		_choice("DEBUG - Full Heal","DEBUG - Full Heal");
	else
		_void();
		
	if obj_control.debug_enable
		_choice("DEBUG - Level Up","DEBUG - Level Up");
	else
		_void();
	if var_map_get(VAR_HOURS_SINCE_LAST_REST) > 5
		_choice("Rest","Rest");
	else
		_choice("[?]Rest","Rest");
	
	if var_map_get(VAR_HOURS_SINCE_LAST_SLEEP) > 5
		_choice("Sleep","Sleep");
	else
		_choice("[?]Sleep","Sleep");
	
	if var_map_get(VAR_HOURS_SINCE_LAST_SLEEP) > 10
		_choice("Sleep with...","Sleepwith");
	else
		_choice("[?]Sleep with...","Sleepwith");
		
	_choice("Hump","Hump");

	_choice("Stay","",noone,"",true,true,true,"",CSX_arrived);
		
	_choice("Move","",noone,"",true,true,true,"",open_main_map);
	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Rest");
	if ally_get_number() > 1
		_dialog("        You take shelter in the wagon, with your companions, dozing for a time…",true);
	else if ally_get_number() == 1
		_dialog("        You take shelter in the wagon, with "+string(char_get_info(ally_get_char(0),GAME_CHAR_DISPLAYNAME))+", dozing for a time…",true);
	else
		_dialog("        You take shelter in the wagon, dozing for a time…",true);
	_main_gui(VAR_GUI_MIN);
	_pause(2,0,true);
	_execute(0,"game_time_one_hour_passes");
	_execute(0,"game_time_one_hour_passes");
	_execute(0,"scene_recover_rest_2hours");
	_var_map_set(VAR_HOURS_SINCE_LAST_REST,0);
	_main_gui(VAR_GUI_NORMAL);
	_dialog("        You wake, refreshed.",true);
	_jump("Camping");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Sleep");
	if ally_get_number() > 1
		_dialog("        You take shelter in the wagon, together with your companions.  Sleep finds you swiftly…",true);
	else if ally_get_number() == 1
		_dialog("        You take shelter in the wagon, together with "+string(char_get_info(ally_get_char(0),GAME_CHAR_DISPLAYNAME))+".  Sleep finds you swiftly…",true);
	else
		_dialog("        You take shelter in the wagon.  Sleep finds you swiftly…",true);
	_main_gui(VAR_GUI_MIN);
	_play_sfx(sfx_sleep);
	_pause(3,0,true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"game_time_one_hour_passes",true);
	_execute(0,"scene_recover_sleep_8hours");
	_var_map_set(VAR_HOURS_SINCE_LAST_SLEEP,0);
	_main_gui(VAR_GUI_NORMAL);
	_dialog("        You wake from peaceful slumber.",true);
	_jump("Camping");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Hump");
	_jump("Camphump","CS_extra_SS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Sleepwith");
	_jump("Campsleep","CS_extra_SS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("DEBUG - Full Heal");
	_execute(0,"S_heal_all");
	_jump("Camping");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("DEBUG - Level Up");
	_execute(0,"S_levelup_all");
	_jump("Camping");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

	//_map();