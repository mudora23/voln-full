///scene_char_exp_gain(char,amount);
/// @description
/// @param char
/// @param amount

argument0[? GAME_CHAR_EXP]+= argument1;

while(argument0[? GAME_CHAR_EXP] >= char_get_exp_max(argument0))
{
	argument0[? GAME_CHAR_EXP] -= char_get_exp_max(argument0);
	char_level_up(argument0,1);
	if !audio_is_playing(sfx_level_up)
		voln_play_sfx(sfx_level_up);
	draw_floating_text(argument0[? GAME_CHAR_X_FLOATING_TEXT],argument0[? GAME_CHAR_Y_FLOATING_TEXT],"LEVEL UP!",COLOR_HEAL);
}
