///scene_bg_location(LOCATION_X);
/// @description  
/// @param LOCATION_X
with obj_control
{
	var location = location_map[? argument0];
	var BG_NAME = location[? "bg"];
	scene_bg(BG_NAME);
}