///scriptvar_PC_Nads_Desc();
/// @description
var num = var_map_get(PC_Nads_N);
if num == 1 return choose("a single","a lone","only one");
else if num == 2 return choose("a pair of","twin","a couple of","two matching","a set of");
else if num == 3 return choose("three matching","a trio of","three");
else if num == 4 return choose("two pair of","four");
else if num == 5 return "five";
else if num == 6 return choose("three pair of", "six");
else return "many";