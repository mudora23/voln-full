///scene_list_add_ss_camp_hump(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];

if ally_exists(NAME_KUDI) &&  ds_map_find_value(char_get_player(),GAME_CHAR_LUST) >= 10 && var_map_get(VAR_TIME_HOUR) >= 11 && var_map_get(VAR_TIME_HOUR)<= 20 && obj_control.var_map[? VAR_HOURS_SINCE_LAST_REST] >= 5
	ds_list_add(possible_list,"SS_CAMP_HUMP_1_1");
if ally_exists(NAME_ZEYD) && ds_map_find_value(char_get_player(),GAME_CHAR_LUST) >= 10 && var_map_get(VAR_TIME_HOUR) >= 11 && var_map_get(VAR_TIME_HOUR)<= 20 && obj_control.var_map[? VAR_HOURS_SINCE_LAST_REST] >= 5
	ds_list_add(possible_list,"SS_CAMP_HUMP_2_1");
if ally_exists(NAME_KUDI) && var_map_get(VAR_JAR) > 0
	ds_list_add(possible_list,"SS_CAMP_HUMP_3_1");
if ally_exists(NAME_BALWAG) && ally_exists(NAME_ZEYD)
	ds_list_add(possible_list,"SS_CAMP_HUMP_4_1");
if ally_exists(NAME_FELGOR)
	ds_list_add(possible_list,"SS_CAMP_HUMP_5_1");
if ally_exists(NAME_KUDI) && char_get_char_opinion(NAME_KUDI,char_get_player()) >= 5
	ds_list_add(possible_list,"SS_CAMP_HUMP_6_1");
if ally_exists(NAME_BALWAG) && ally_exists(NAME_ZEYD)
	ds_list_add(possible_list,"SS_CAMP_HUMP_7_1");
if ally_exists(NAME_KUDI)
	ds_list_add(possible_list,"SS_CAMP_HUMP_8_1");
if ally_exists(NAME_BALWAG) && ally_exists(NAME_ZEYD)
	ds_list_add(possible_list,"SS_CAMP_HUMP_9_1");
if ally_exists(NAME_ZEYD) && char_get_char_opinion(NAME_ZEYD,char_get_player()) >= 5
	ds_list_add(possible_list,"SS_CAMP_HUMP_10_1");

return possible_list;

