///char_level_up(char,times);
/// @description
/// @param char
/// @param times

var char_map = argument0;
var level_times = argument1;

char_map[? GAME_CHAR_LEVEL]+=level_times;

// recommand total points: 20+level*5;
var char_level = char_map[? GAME_CHAR_LEVEL];

ds_map_add_unique(char_map,GAME_CHAR_POINTS_PER_LEVELUP,3);

var points_per_level = char_get_info(char_map,GAME_CHAR_POINTS_PER_LEVELUP);
var points_total = char_get_info(char_map,GAME_CHAR_STR) + char_get_info(char_map,GAME_CHAR_FOU) + char_get_info(char_map,GAME_CHAR_DEX) + char_get_info(char_map,GAME_CHAR_END) + char_get_info(char_map,GAME_CHAR_INT) + char_get_info(char_map,GAME_CHAR_WIS) + char_get_info(char_map,GAME_CHAR_SPI);

//var level_factor = (20+char_level*5)/9;
var level_differnet = level_times;

if level_differnet != 0
{
	if char_map == obj_control.var_map[? VAR_PLAYER]
	{
		//obj_control.var_map[? VAR_PLAYER_UNSPEND_STATS_POINTS] += points_per_level*level_times;
		obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS] += 1*level_times;
	}
	
	//char_map[? GAME_CHAR_COLOR] = COLOR_HEAL;
	//char_map[? GAME_CHAR_COLOR_TARGET] = COLOR_NORMAL;
	
	char_stats_recalculate(char_map);
	char_skills_recalculate(char_map);
	
	char_set_info(char_map,GAME_CHAR_HEALTH,char_get_health_max(char_map));
	char_set_info(char_map,GAME_CHAR_TORU,char_get_toru_max(char_map));
	char_set_info(char_map,GAME_CHAR_STAMINA,char_get_stamina_max(char_map));
	
	
	
}

	