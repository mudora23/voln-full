///battle_char_list_get_bodyguard_char(char_list);
/// @description
/// @param char_list

var the_char_list = ds_list_create();

for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];
	if battle_char_get_combat_capable(char) && battle_char_get_buff_level(char,BUFF_BODYGUARD) > 0
	{
		ds_list_add(the_char_list,char);
	}
}

if ds_list_size(the_char_list) > 0
{
	ds_list_shuffle(the_char_list);
	var the_char = the_char_list[| 0];
	ds_list_destroy(the_char_list);
	return the_char;
}
else
{
	ds_list_destroy(the_char_list);
	return noone;
}
