///scene_CG_list_init();
/// @description 
with obj_control
{
	#macro VAR_CG_LIST_ID "VAR_CG_LIST_ID"
	#macro VAR_CG_LIST_INDEX_NAME "VAR_CG_LIST_INDEX_NAME"
	#macro VAR_CG_LIST_INDEX_TARGET_NAME "VAR_CG_LIST_INDEX_TARGET_NAME"
	#macro VAR_CG_LIST_X "VAR_CG_LIST_X"
	#macro VAR_CG_LIST_X_TARGET "VAR_CG_LIST_X_TARGET"
	#macro VAR_CG_LIST_Y "VAR_CG_LIST_Y"
	#macro VAR_CG_LIST_Y_TARGET "VAR_CG_LIST_Y_TARGET"
	#macro VAR_CG_LIST_ALPHA "VAR_CG_LIST_ALPHA"
	#macro VAR_CG_LIST_ALPHA_TARGET "VAR_CG_LIST_ALPHA_TARGET"
	#macro VAR_CG_LIST_FADE_SPEED "VAR_CG_LIST_FADE_SPEED"
	#macro VAR_CG_LIST_SLIDE_SPEED "VAR_CG_LIST_SLIDE_SPEED"
	#macro VAR_CG_LIST_ORDER "VAR_CG_LIST_ORDER"
	#macro VAR_CG_LIST_SIZE_RATIO "VAR_CG_LIST_SIZE_RATIO"
	#macro VAR_CG_DIM_ALPHA "VAR_CG_DIM_ALPHA"
	
	ds_map_add_unique_list(var_map,VAR_CG_LIST_ID,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_INDEX_NAME,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_INDEX_TARGET_NAME,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_X,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_X_TARGET,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_Y,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_Y_TARGET,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_ALPHA,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_ALPHA_TARGET,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_FADE_SPEED,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_SLIDE_SPEED,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_ORDER,ds_list_create());
	ds_map_add_unique_list(var_map,VAR_CG_LIST_SIZE_RATIO,ds_list_create());
	
	ds_map_add_unique(var_map,VAR_CG_DIM_ALPHA,0);
	
	#macro VAR_CG_HALIGN_MAP "VAR_CG_HALIGN_MAP"
	ds_map_add_unique_map(var_map,VAR_CG_HALIGN_MAP,ds_map_create());
	
	#macro VAR_CG_VALIGN_MAP "VAR_CG_VALIGN_MAP"
	ds_map_add_unique_map(var_map,VAR_CG_VALIGN_MAP,ds_map_create());
}
