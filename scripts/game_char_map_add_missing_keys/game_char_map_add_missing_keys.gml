///game_char_map_add_missing_keys(char_map,char_owner);
/// @description
/// @param char_map
/// @param char_owner
var char_map = argument[0];

with obj_control
{
	// add missing keys
	
	ds_map_add_unique(char_map,GAME_CHAR_ID,char_generate_new_id()); #macro GAME_CHAR_ID "GAME_CHAR_ID"
	ds_map_add_unique(char_map,GAME_CHAR_MOM_ID,noone); #macro GAME_CHAR_MOM_ID "GAME_CHAR_MOM_ID"
	ds_map_add_unique(char_map,GAME_CHAR_DAD_ID,noone); #macro GAME_CHAR_DAD_ID "GAME_CHAR_DAD_ID"
	
	// time based stats buff or de-buff
	ds_map_add_unique_list(char_map,GAME_CHAR_BUFF_LIST,ds_list_create()); #macro GAME_CHAR_BUFF_LIST "GAME_CHAR_BUFF_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_BUFF_TURN_LIST,ds_list_create()); #macro GAME_CHAR_BUFF_TURN_LIST "GAME_CHAR_BUFF_TURN_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_BUFF_LEVEL_LIST,ds_list_create()); #macro GAME_CHAR_BUFF_LEVEL_LIST "GAME_CHAR_BUFF_LEVEL_LIST"
	ds_map_add_unique_list(char_map,GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST,ds_list_create()); #macro GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST "GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST"
	ds_map_add_unique_list(char_map,GAME_CHAR_DEBUFF_LIST,ds_list_create()); #macro GAME_CHAR_DEBUFF_LIST "GAME_CHAR_DEBUFF_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_DEBUFF_TURN_LIST,ds_list_create()); #macro GAME_CHAR_DEBUFF_TURN_LIST "GAME_CHAR_DEBUFF_TURN_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_DEBUFF_LEVEL_LIST,ds_list_create()); #macro GAME_CHAR_DEBUFF_LEVEL_LIST "GAME_CHAR_DEBUFF_LEVEL_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST,ds_list_create()); #macro GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST "GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST"
		// DEFINE HERE -> short_term_buff_debuff_wiki_map_init();
		
	
	// time based stats buff or de-buff
	ds_map_add_unique_list(char_map,GAME_CHAR_LONG_TERM_BUFF_LIST,ds_list_create()); #macro GAME_CHAR_LONG_TERM_BUFF_LIST "GAME_CHAR_LONG_TERM_BUFF_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_LONG_TERM_BUFF_HOURS_LIST,ds_list_create()); #macro GAME_CHAR_LONG_TERM_BUFF_HOURS_LIST "GAME_CHAR_LONG_TERM_BUFF_HOURS_LIST" 
		#macro LONG_TERM_BUFF_ORANGE_BERRY "LONG_TERM_BUFF_ORANGE_BERRY"
		
	ds_map_add_unique_list(char_map,GAME_CHAR_LONG_TERM_DEBUFF_LIST,ds_list_create()); #macro GAME_CHAR_LONG_TERM_DEBUFF_LIST "GAME_CHAR_LONG_TERM_DEBUFF_LIST" 
	ds_map_add_unique_list(char_map,GAME_CHAR_LONG_TERM_DEBUFF_HOURS_LIST,ds_list_create()); #macro GAME_CHAR_LONG_TERM_DEBUFF_HOURS_LIST "GAME_CHAR_LONG_TERM_DEBUFF_HOURS_LIST" 
		#macro LONG_TERM_DEBUFF_PURPLE_BERRY "LONG_TERM_DEBUFF_PURPLE_BERRY"
	
	
	ds_map_add_unique_map(char_map,GAME_CHAR_SKILLS_MAP,ds_map_create()); #macro GAME_CHAR_SKILLS_MAP "GAME_CHAR_SKILLS_MAP" 
	ds_map_add_unique_map(char_map,GAME_CHAR_BONUS_SKILLS_MAP,ds_map_create()); #macro GAME_CHAR_BONUS_SKILLS_MAP "GAME_CHAR_BONUS_SKILLS_MAP" 

	ds_map_add_unique(char_map,GAME_CHAR_THUMB,sprite_get_name(spr_char_player_thumb)); #macro GAME_CHAR_THUMB "GAME_CHAR_THUMB"
	ds_map_add_unique(char_map,GAME_CHAR_SPR,sprite_get_name(spr_char_anon_neutral_s)); #macro GAME_CHAR_SPR "GAME_CHAR_SPR"
	ds_map_add_unique(char_map,GAME_CHAR_NAME,"Undefined"); #macro GAME_CHAR_NAME "GAME_CHAR_NAME"
	ds_map_add_unique(char_map,GAME_CHAR_NAME_TYPE,"Undefined"); #macro GAME_CHAR_NAME_TYPE "GAME_CHAR_NAME_TYPE"
	ds_map_add_unique(char_map,GAME_CHAR_DISPLAYNAME,ds_map_find_value(char_map,GAME_CHAR_NAME)); #macro GAME_CHAR_DISPLAYNAME "GAME_CHAR_DISPLAYNAME"
	
	ds_map_add_unique(char_map,GAME_CHAR_EXP,0); #macro GAME_CHAR_EXP "GAME_CHAR_EXP"
	
	ds_map_add_unique(char_map,GAME_CHAR_UNIQUE,false); #macro GAME_CHAR_UNIQUE "GAME_CHAR_UNIQUE"
	ds_map_add_unique(char_map,GAME_CHAR_SUPERIOR,false); #macro GAME_CHAR_SUPERIOR "GAME_CHAR_SUPERIOR"

	ds_map_add_unique(char_map,GAME_CHAR_STR_RATE,0.4); #macro GAME_CHAR_STR_RATE "GAME_CHAR_STR_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_FOU_RATE,0.4); #macro GAME_CHAR_FOU_RATE "GAME_CHAR_FOU_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_DEX_RATE,0.4); #macro GAME_CHAR_DEX_RATE "GAME_CHAR_DEX_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_END_RATE,0.4); #macro GAME_CHAR_END_RATE "GAME_CHAR_END_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_INT_RATE,0.4); #macro GAME_CHAR_INT_RATE "GAME_CHAR_INT_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_WIS_RATE,0.4); #macro GAME_CHAR_WIS_RATE "GAME_CHAR_WIS_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_SPI_RATE,0.4); #macro GAME_CHAR_SPI_RATE "GAME_CHAR_SPI_RATE"

	ds_map_add_unique(char_map,GAME_CHAR_BONUS_STR_RATE,0); #macro GAME_CHAR_BONUS_STR_RATE "GAME_CHAR_BONUS_STR_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_FOU_RATE,0); #macro GAME_CHAR_BONUS_FOU_RATE "GAME_CHAR_BONUS_FOU_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_DEX_RATE,0); #macro GAME_CHAR_BONUS_DEX_RATE "GAME_CHAR_BONUS_DEX_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_END_RATE,0); #macro GAME_CHAR_BONUS_END_RATE "GAME_CHAR_BONUS_END_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_INT_RATE,0); #macro GAME_CHAR_BONUS_INT_RATE "GAME_CHAR_BONUS_INT_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_WIS_RATE,0); #macro GAME_CHAR_BONUS_WIS_RATE "GAME_CHAR_BONUS_WIS_RATE"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_SPI_RATE,0); #macro GAME_CHAR_BONUS_SPI_RATE "GAME_CHAR_BONUS_SPI_RATE"
	
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_STR_FLAT,0); #macro GAME_CHAR_BONUS_STR_FLAT "GAME_CHAR_BONUS_STR_FLAT"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_FOU_FLAT,0); #macro GAME_CHAR_BONUS_FOU_FLAT "GAME_CHAR_BONUS_FOU_FLAT"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_DEX_FLAT,0); #macro GAME_CHAR_BONUS_DEX_FLAT "GAME_CHAR_BONUS_DEX_FLAT"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_END_FLAT,0); #macro GAME_CHAR_BONUS_END_FLAT "GAME_CHAR_BONUS_END_FLAT"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_INT_FLAT,0); #macro GAME_CHAR_BONUS_INT_FLAT "GAME_CHAR_BONUS_INT_FLAT"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_WIS_FLAT,0); #macro GAME_CHAR_BONUS_WIS_FLAT "GAME_CHAR_BONUS_WIS_FLAT"
	ds_map_add_unique(char_map,GAME_CHAR_BONUS_SPI_FLAT,0); #macro GAME_CHAR_BONUS_SPI_FLAT "GAME_CHAR_BONUS_SPI_FLAT"
	
	ds_map_add_unique(char_map,GAME_CHAR_STR,0); #macro GAME_CHAR_STR "STR"
	ds_map_add_unique(char_map,GAME_CHAR_FOU,0); #macro GAME_CHAR_FOU "FOU"
	ds_map_add_unique(char_map,GAME_CHAR_DEX,0); #macro GAME_CHAR_DEX "DEX"
	ds_map_add_unique(char_map,GAME_CHAR_END,0); #macro GAME_CHAR_END "END"
	ds_map_add_unique(char_map,GAME_CHAR_INT,0); #macro GAME_CHAR_INT "INT"
	ds_map_add_unique(char_map,GAME_CHAR_WIS,0); #macro GAME_CHAR_WIS "WIS"
	ds_map_add_unique(char_map,GAME_CHAR_SPI,0); #macro GAME_CHAR_SPI "SPI"
	ds_map_add_unique(char_map,GAME_CHAR_LGR,10); #macro GAME_CHAR_LGR "LGR"
	ds_map_add_unique(char_map,GAME_CHAR_LRR,0); #macro GAME_CHAR_LRR "LRR"
	
	
	ds_map_add_unique(char_map,GAME_CHAR_LEVEL,1); #macro GAME_CHAR_LEVEL "GAME_CHAR_LEVEL"
	ds_map_add_unique(char_map,GAME_CHAR_LEVEL_MIN,1); #macro GAME_CHAR_LEVEL_MIN "GAME_CHAR_LEVEL_MIN"
	ds_map_add_unique(char_map,GAME_CHAR_LEVEL_MAX,100); #macro GAME_CHAR_LEVEL_MAX "GAME_CHAR_LEVEL_MAX"
	
	ds_map_add_unique(char_map,GAME_CHAR_GOLD_DROP,0); #macro GAME_CHAR_GOLD_DROP "GAME_CHAR_GOLD_DROP"
	
	ds_map_add_unique(char_map,GAME_CHAR_POINTS_PER_LEVELUP,2.5); #macro GAME_CHAR_POINTS_PER_LEVELUP "GAME_CHAR_POINTS_PER_LEVELUP"
	
	ds_map_add_unique(char_map,GAME_CHAR_GENDER,GENDER_NONE); #macro GAME_CHAR_GENDER "GAME_CHAR_GENDER"
		#macro GENDER_NONE "none"
		#macro GENDER_MALE "male"
		#macro GENDER_FEMALE "female"
		#macro GENDER_ANY "any"
		
		
	ds_map_add_unique(char_map,GAME_CHAR_SELECTED,true); #macro GAME_CHAR_SELECTED "GAME_CHAR_SELECTED"
	ds_map_add_unique(char_map,GAME_CHAR_SEX,false); #macro GAME_CHAR_SEX "GAME_CHAR_SEX"
	ds_map_add_unique(char_map,GAME_CHAR_CAPTURE,false); #macro GAME_CHAR_CAPTURE "GAME_CHAR_CAPTURE"
	ds_map_add_unique(char_map,GAME_CHAR_AI,AI_AGGRESSIVE); #macro GAME_CHAR_AI "GAME_CHAR_AI"
		#macro AI_AGGRESSIVE "Aggressive"
		#macro AI_SEDUCTIVE "Seductive"
		#macro AI_SUPPORTIVE "Supportive"
		#macro AI_CONSERVATIVE "Conservative"
		#macro AI_NONE "None"
		
	ds_map_add_unique(char_map,GAME_CHAR_IMPORTANT,false); #macro GAME_CHAR_IMPORTANT "GAME_CHAR_IMPORTANT"


	ds_map_add_unique(char_map,GAME_CHAR_HOURS_SINCE_BORN,15*20); #macro GAME_CHAR_HOURS_SINCE_BORN "GAME_CHAR_HOURS_SINCE_BORN"
	
	//impregnate
	if !ds_map_exists(char_map,GAME_CHAR_IMPREGNATE)
		game_char_map_reset_impregnate_keys(char_map);
	
	ds_map_add_unique(char_map,GAME_CHAR_ADULT,true); #macro GAME_CHAR_ADULT "GAME_CHAR_ADULT" 

	ds_map_add_unique(char_map,GAME_CHAR_HEALTH,char_get_health_max(char_map)); #macro GAME_CHAR_HEALTH "GAME_CHAR_HEALTH" 
	ds_map_add_unique(char_map,GAME_CHAR_STAMINA,char_get_stamina_max(char_map)); #macro GAME_CHAR_STAMINA "GAME_CHAR_STAMINA" 
	ds_map_add_unique(char_map,GAME_CHAR_LUST,0); #macro GAME_CHAR_LUST "GAME_CHAR_LUST" 
	ds_map_add_unique(char_map,GAME_CHAR_TORU,char_get_toru_max(char_map)); #macro GAME_CHAR_TORU "GAME_CHAR_TORU" 
	ds_map_add_unique(char_map,GAME_CHAR_VITALITY,char_get_vitality_max(char_map)); #macro GAME_CHAR_VITALITY "GAME_CHAR_VITALITY"
	
	if argument_count > 1
	{
		ds_map_add_unique(char_map,GAME_CHAR_OWNER,argument[1]); #macro GAME_CHAR_OWNER "GAME_CHAR_OWNER" 
	        #macro OWNER_PLAYER "OWNER_PLAYER"
	        #macro OWNER_ALLY "OWNER_ALLY"
	        #macro OWNER_ENEMY "OWNER_ENEMY"
	}
	
	ds_map_add_unique(char_map,GAME_CHAR_BATTLE_WAITING_TIME,char_get_speed(char_map)); #macro GAME_CHAR_BATTLE_WAITING_TIME "GAME_CHAR_BATTLE_WAITING_TIME" 
	
	// battle positions
	ds_map_add_unique(char_map,GAME_CHAR_X,room_width); #macro GAME_CHAR_X "GAME_CHAR_X" 
	ds_map_add_unique(char_map,GAME_CHAR_X_TARGET,0); #macro GAME_CHAR_X_TARGET "GAME_CHAR_X_TARGET" 
	ds_map_add_unique(char_map,GAME_CHAR_Y,0); #macro GAME_CHAR_Y "GAME_CHAR_Y" 
	ds_map_add_unique(char_map,GAME_CHAR_Y_TARGET,0); #macro GAME_CHAR_Y_TARGET "GAME_CHAR_Y_TARGET" 
	ds_map_add_unique(char_map,GAME_CHAR_ALPHA,0); #macro GAME_CHAR_ALPHA "GAME_CHAR_ALPHA" 
	ds_map_add_unique(char_map,GAME_CHAR_ALPHA_TARGET,0); #macro GAME_CHAR_ALPHA_TARGET "GAME_CHAR_ALPHA_TARGET" 
	ds_map_add_unique(char_map,GAME_CHAR_COLOR,c_white); #macro GAME_CHAR_COLOR "GAME_CHAR_COLOR" 
	ds_map_add_unique(char_map,GAME_CHAR_COLOR_TARGET,c_white); #macro GAME_CHAR_COLOR_TARGET "GAME_CHAR_COLOR_TARGET" 
	ds_map_add_unique(char_map,GAME_CHAR_SHAKE_AMOUNT,0); #macro GAME_CHAR_SHAKE_AMOUNT "GAME_CHAR_SHAKE_AMOUNT" 
	ds_map_add_unique(char_map,GAME_CHAR_HEALTH_VISUAL,char_map[? GAME_CHAR_HEALTH]); #macro GAME_CHAR_HEALTH_VISUAL "GAME_CHAR_HEALTH_VISUAL" 
	ds_map_add_unique(char_map,GAME_CHAR_LUST_VISUAL,char_map[? GAME_CHAR_LUST]); #macro GAME_CHAR_LUST_VISUAL "GAME_CHAR_LUST_VISUAL" 

	ds_map_add_unique(char_map,GAME_CHAR_X_FLOATING_TEXT,room_width/2); #macro GAME_CHAR_X_FLOATING_TEXT "GAME_CHAR_X_FLOATING_TEXT" 
	ds_map_add_unique(char_map,GAME_CHAR_Y_FLOATING_TEXT,room_height/2); #macro GAME_CHAR_Y_FLOATING_TEXT "GAME_CHAR_Y_FLOATING_TEXT" 	
	
	ds_map_add_unique(char_map,GAME_CHAR_X_ORDERING,0); #macro GAME_CHAR_X_ORDERING "GAME_CHAR_X_ORDERING" 
	ds_map_add_unique(char_map,GAME_CHAR_Y_ORDERING,0); #macro GAME_CHAR_Y_ORDERING "GAME_CHAR_Y_ORDERING" 	
	ds_map_add_unique(char_map,GAME_CHAR_X_TARGET_ORDERING,0); #macro GAME_CHAR_X_TARGET_ORDERING "GAME_CHAR_X_TARGET_ORDERING" 
	ds_map_add_unique(char_map,GAME_CHAR_Y_TARGET_ORDERING,0); #macro GAME_CHAR_Y_TARGET_ORDERING "GAME_CHAR_Y_TARGET_ORDERING" 	
	
	ds_map_add_unique_map(char_map,GAME_CHAR_OPINION_MAP,ds_map_create()); #macro GAME_CHAR_OPINION_MAP "GAME_CHAR_OPINION_MAP"
	
	// pets
	ds_map_add_unique(char_map,GAME_CHAR_TYPE,GAME_CHAR_ALLY); #macro GAME_CHAR_TYPE "GAME_CHAR_TYPE"
		#macro GAME_CHAR_ALLY "GAME_CHAR_ALLY"
		#macro GAME_CHAR_PET "GAME_CHAR_PET"
		
	ds_map_add_unique(char_map,GAME_CHAR_PET_YOBO,true); #macro GAME_CHAR_PET_YOBO "yobo"
	ds_map_add_unique(char_map,GAME_CHAR_PET_TOY,true); #macro GAME_CHAR_PET_TOY "toy"
	ds_map_add_unique(char_map,GAME_CHAR_PET_BREEDER,true); #macro GAME_CHAR_PET_BREEDER "breeder"

	ds_map_add_unique(char_map,GAME_CHAR_PET_DEPRAVITY,0); #macro GAME_CHAR_PET_DEPRAVITY "Depravity"
	ds_map_add_unique(char_map,GAME_CHAR_PET_SUBMISSION,0); #macro GAME_CHAR_PET_SUBMISSION "Submission"
	ds_map_add_unique(char_map,GAME_CHAR_PET_LOYALTY,0); #macro GAME_CHAR_PET_LOYALTY "Loyalty"
	
	// from breeding
	ds_map_add_unique(char_map,GAME_CHAR_FROM_BREEDING,false); #macro GAME_CHAR_FROM_BREEDING "GAME_CHAR_FROM_BREEDING"
	
	
	// color
	ds_map_add_unique(char_map,GAME_CHAR_COLOR_HAIR,c_white); #macro GAME_CHAR_COLOR_HAIR "GAME_CHAR_COLOR_HAIR"
	ds_map_add_unique(char_map,GAME_CHAR_COLOR_EYE,c_white); #macro GAME_CHAR_COLOR_EYE "GAME_CHAR_COLOR_EYE"
	ds_map_add_unique(char_map,GAME_CHAR_COLOR_SKIN,c_white); #macro GAME_CHAR_COLOR_SKIN "GAME_CHAR_COLOR_SKIN"
	#macro GAME_CHAR_COLOR_WIZARD_1 "GAME_CHAR_COLOR_WIZARD_1"
	#macro GAME_CHAR_COLOR_WIZARD_2 "GAME_CHAR_COLOR_WIZARD_2"
	#macro GAME_CHAR_COLOR_WIZARD_3 "GAME_CHAR_COLOR_WIZARD_3"
	#macro GAME_CHAR_COLOR_WIZARD_4 "GAME_CHAR_COLOR_WIZARD_4"
	#macro GAME_CHAR_COLOR_WARRIOR_1 "GAME_CHAR_COLOR_WARRIOR_1"
	#macro GAME_CHAR_COLOR_WARRIOR_2 "GAME_CHAR_COLOR_WARRIOR_2"
	#macro GAME_CHAR_COLOR_WARRIOR_3 "GAME_CHAR_COLOR_WARRIOR_3"
	#macro GAME_CHAR_COLOR_WARRIOR_4 "GAME_CHAR_COLOR_WARRIOR_4"
	#macro GAME_CHAR_COLOR_THIEF_1 "GAME_CHAR_COLOR_THIEF_1"
	#macro GAME_CHAR_COLOR_THIEF_2 "GAME_CHAR_COLOR_THIEF_2"
	#macro GAME_CHAR_COLOR_THIEF_3 "GAME_CHAR_COLOR_THIEF_3"
	#macro GAME_CHAR_COLOR_THIEF_4 "GAME_CHAR_COLOR_THIEF_4"
	
	// others
	ds_map_add_unique(char_map,GAME_CHAR_STORAGE_LOCK,false); #macro GAME_CHAR_STORAGE_LOCK "GAME_CHAR_STORAGE_LOCK"
	
	char_stats_recalculate(char_map);
	char_skills_recalculate(char_map);
	
	char_set_info(char_map,GAME_CHAR_HEALTH,char_get_health_max(char_map));
	char_set_info(char_map,GAME_CHAR_TORU,char_get_toru_max(char_map));
	char_set_info(char_map,GAME_CHAR_STAMINA,char_get_stamina_max(char_map));
	
	


}

