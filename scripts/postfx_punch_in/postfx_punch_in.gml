///postfx_punch_in(x,y,amount[0.05]);
/// @description  
/// @param x
/// @param y
/// @param amount[0.05]
with obj_control
{
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_NAME_LIST],POSTFX_PUNCH_IN);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_LIST],argument[2]*6);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_MAX_LIST],argument[2]*6);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG0_LIST],argument[0]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG1_LIST],argument[1]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG2_LIST],argument[2]);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG3_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG4_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG5_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG6_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG7_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG8_LIST],0);
	ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG9_LIST],0);

}



