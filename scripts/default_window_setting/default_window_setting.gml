/// default_window_setting();
/// @description

var display_height = min(1080,display_get_height());
var display_width = min(1920,display_get_width());
if (display_width / 16 < display_height / 9)
{
    //igv_control_set_value(IGV_CONTROL_WINDOW_SIZE_W,display_width*0.9);
    //igv_control_set_value(IGV_CONTROL_WINDOW_SIZE_H,display_width*0.9/16*9);
    //igv_control_set_value(IGV_CONTROL_WINDOW_POS_X,10);
    //igv_control_set_value(IGV_CONTROL_WINDOW_POS_Y,40);
    //window_set_size(igv_control_get_value(IGV_CONTROL_WINDOW_SIZE_W), igv_control_get_value(IGV_CONTROL_WINDOW_SIZE_H));
    //window_set_position(igv_control_get_value(IGV_CONTROL_WINDOW_POS_X),igv_control_get_value(IGV_CONTROL_WINDOW_POS_Y));
	
    window_set_size(display_width*0.9, display_width*0.9/16*9);
    window_set_position(10,40);
}
else
{

    //igv_control_set_value(IGV_CONTROL_WINDOW_SIZE_W,display_height*0.9/9*16);
    //igv_control_set_value(IGV_CONTROL_WINDOW_SIZE_H,display_height*0.9);
    //igv_control_set_value(IGV_CONTROL_WINDOW_POS_X,10);
    //igv_control_set_value(IGV_CONTROL_WINDOW_POS_Y,40);
    //window_set_size(igv_control_get_value(IGV_CONTROL_WINDOW_SIZE_W), igv_control_get_value(IGV_CONTROL_WINDOW_SIZE_H));
    //window_set_position(igv_control_get_value(IGV_CONTROL_WINDOW_POS_X),igv_control_get_value(IGV_CONTROL_WINDOW_POS_Y));
    window_set_size(display_height*0.9/9*16, display_height*0.9);
    window_set_position(10,40);
}        


