///_execute_if(cond,script,arg0,arg1,arg2...);
/// @description  
/// @param cond
/// @param script
/// @param arg0
/// @param arg1
/// @param arg2...

if scene_is_current()
{
	if argument0
	{
		var args;
		for(var i = 2; i < argument_count; i++) args[i-2] = argument[i];
		script_execute_array(argument[1],args);
		args = 0; // destroy
	}
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);
