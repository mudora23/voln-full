///draw_shop_item(item_name,x,y,w,h);
/// @descirption
/// @param item_name
/// @param x
/// @param y
/// @param w
/// @param h
var item_name = argument0;
var x1 = argument1;
var y1 = argument2;
var w = argument3;
var h = argument4;
var x2 = x1+w;
var y2 = y1+h;



draw_set_color(c_lightblack);
//draw_set_color(c_white);
draw_set_alpha(1);

//draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
//draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
//draw_sprite_stretched_ext(spr_texture_paper,irandom(sprite_get_number(spr_texture_paper)-1),x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
draw_sprite_stretched_ext(spr_gui_board,0,x1,y1,x2-x1,y2-y1,c_white,draw_get_alpha());

draw_set_color(c_yellow);
draw_set_font(font_01_25_sc);

var padding_x = 45;
var padding_y = 25;

draw_itemtype_icon_in_area(x1 + padding_x,padding_y,string_height("W"),string_height("W"),item_get_type(item_name));
draw_text(x1 + padding_x+string_height("W")+10,padding_y,item_get_displayname(item_name));

draw_set_font(font_01_20_sc);
draw_set_valign(fa_top);

if var_map_get(VAR_GOLD) >= item_get_price(item_name) draw_set_color(c_lime);
else draw_set_color(make_color_rgb(255, 160, 160));

draw_sprite_stretched(spr_icon_coin_still_full,0,x1 + padding_x ,y2-padding_y-string_height("W"),string_height("W"),string_height("W"));
draw_text(x1 + padding_x + string_height("W")+10,y2-padding_y-string_height("W"),string(item_get_price(item_name)));

draw_set_color(c_white);
draw_sprite_stretched(spr_gui_scale,0,x1 + w/2,y2-padding_y-string_height("W"),string_height("W"),string_height("W"));
draw_text(x1 + w/2 + string_height("W")+10,y2-padding_y-string_height("W"),string(item_get_weight(item_name))+" lb");

draw_set_font(font_01s_reg);
draw_set_valign(fa_center);
draw_set_halign(fa_center);
draw_text_ext(x1+w/2,y1+h/2,item_get_des(item_name),30,w-padding_x*2);









