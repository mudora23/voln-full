///scene_label_exists(label);
/// @description
/// @param label
return ds_map_exists(obj_control.var_map[? VAR_SCRIPT_LABEL_MAP],string_upper(argument0));