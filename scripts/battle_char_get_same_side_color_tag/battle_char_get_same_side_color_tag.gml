///battle_char_get_same_side_color_tag(char);
/// @param char
if argument0[? GAME_CHAR_OWNER] == OWNER_PLAYER || argument0[? GAME_CHAR_OWNER] == OWNER_ALLY
{
	if battle_char_get_debuff_level(argument0,DEBUFF_TEMPT) > 0 return TAG_COLOR_ENEMY_NAME;
	else return TAG_COLOR_ALLY_NAME;
}
else
{
	if battle_char_get_debuff_level(argument0,DEBUFF_TEMPT) > 0 return TAG_COLOR_ALLY_NAME;
	else return TAG_COLOR_ENEMY_NAME;
}