///char_get_SPI(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_SPI);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_SPI_1);
amount += char_get_skill_level(argument0,SKILL_SPI_2);
amount += char_get_skill_level(argument0,SKILL_SPI_3);
amount += char_get_skill_level(argument0,SKILL_SPI_4);
amount += char_get_skill_level(argument0,SKILL_SPI_5);
amount += char_get_skill_level(argument0,SKILL_SPI_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;