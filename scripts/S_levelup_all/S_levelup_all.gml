///S_levelup_all();
/// @description - in town now

with obj_control
{
	var the_list = var_map_get_all_character_map_list();
	for(var i = 0; i < ds_list_size(the_list);i++)
	{
		var the_char = the_list[| i];
		char_level_up(the_char,1);
	}

		
	voln_play_sfx(sfx_level_up);
}