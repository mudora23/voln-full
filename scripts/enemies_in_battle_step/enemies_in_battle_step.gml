///enemies_in_battle_step();
var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
draw_set_alpha(image_alpha);
for(var i = 0; i < ds_list_size(enemy_list);i++)
{
    var enemy = enemy_list[| i];
    
    // set target info
	var margin = 100;
    var x1 = obj_main.x + margin + i * (obj_main.area_width - margin*2) / ds_list_size(enemy_list);
    var x2 = obj_main.x + margin + (i+1) * (obj_main.area_width - margin*2) / ds_list_size(enemy_list);
    var spr = asset_get_index(enemy[? GAME_CHAR_SPR]); if spr < 0 spr = noone;
    var xscale = CHAR_SPR_RATIO_BATTLE_NORMAL;
    var yscale = CHAR_SPR_RATIO_BATTLE_NORMAL;
    var spr_width = sprite_get_width(spr)*xscale;
    var spr_height = sprite_get_height(spr)*yscale;
    
	enemy[? GAME_CHAR_Y] = 580 - spr_height;
    enemy[? GAME_CHAR_Y_TARGET] = 580 - spr_height;
    enemy[? GAME_CHAR_X_TARGET] = (x1 + x2)/2 - spr_width/2;
    enemy[? GAME_CHAR_ALPHA_TARGET] = 1;
    
    // transitions
    enemy[? GAME_CHAR_X] = smooth_approach_room_speed(enemy[? GAME_CHAR_X], enemy[? GAME_CHAR_X_TARGET], SMOOTH_SLIDE_NORMAL_SEC);
    enemy[? GAME_CHAR_Y] = smooth_approach_room_speed(enemy[? GAME_CHAR_Y], enemy[? GAME_CHAR_Y_TARGET], SMOOTH_SLIDE_NORMAL_SEC);
    enemy[? GAME_CHAR_ALPHA] = approach(enemy[? GAME_CHAR_ALPHA], enemy[? GAME_CHAR_ALPHA_TARGET], FADE_NORMAL_SEC/room_speed);
    var color_r = color_get_red(enemy[? GAME_CHAR_COLOR]);
    var color_g = color_get_green(enemy[? GAME_CHAR_COLOR]);
    var color_b = color_get_blue(enemy[? GAME_CHAR_COLOR]);
    var color_r_target = color_get_red(enemy[? GAME_CHAR_COLOR_TARGET]);
    var color_g_target = color_get_green(enemy[? GAME_CHAR_COLOR_TARGET]);
    var color_b_target = color_get_blue(enemy[? GAME_CHAR_COLOR_TARGET]);
    color_r = smooth_approach_room_speed(color_r,color_r_target,SMOOTH_SLIDE_SLOW_SEC);
    color_g = smooth_approach_room_speed(color_g,color_g_target,SMOOTH_SLIDE_SLOW_SEC);
    color_b = smooth_approach_room_speed(color_b,color_b_target,SMOOTH_SLIDE_SLOW_SEC);
    var current_color = make_color_rgb(color_r,color_g,color_b);
	enemy[? GAME_CHAR_COLOR] = current_color;
	enemy[? GAME_CHAR_HEALTH_VISUAL] = smooth_approach_room_speed(enemy[? GAME_CHAR_HEALTH_VISUAL], enemy[? GAME_CHAR_HEALTH], SMOOTH_SLIDE_NORMAL_SEC);
    enemy[? GAME_CHAR_LUST_VISUAL] = smooth_approach_room_speed(enemy[? GAME_CHAR_LUST_VISUAL], enemy[? GAME_CHAR_LUST], SMOOTH_SLIDE_NORMAL_SEC);
	enemy[? GAME_CHAR_X_FLOATING_TEXT] = enemy[? GAME_CHAR_X]+spr_width/2;
	enemy[? GAME_CHAR_Y_FLOATING_TEXT] = enemy[? GAME_CHAR_Y]+spr_height/3;
	enemy[? GAME_CHAR_SHAKE_AMOUNT] = smooth_approach_room_speed(enemy[? GAME_CHAR_SHAKE_AMOUNT], 0, SMOOTH_SLIDE_NORMAL_SEC);
	
    // drawing
    var xx = enemy[? GAME_CHAR_X]+random_range(-enemy[? GAME_CHAR_SHAKE_AMOUNT],enemy[? GAME_CHAR_SHAKE_AMOUNT]);
    var yy = enemy[? GAME_CHAR_Y]+random_range(-enemy[? GAME_CHAR_SHAKE_AMOUNT],enemy[? GAME_CHAR_SHAKE_AMOUNT]);
    var hover = mouse_over_area(x1,yy,x2-x1,obj_main.y-yy) && !menu_exists();
	
	if hover
	{
		obj_control.hovered_char_on_screen_delay = 2;
		obj_control.hovered_char_on_screen = enemy;	
	}
	
	if !menu_exists() && hover
		obj_control.mouse_on_examine_sprite = spr;
	
	hover = hover || obj_control.hovered_char_on_screen == enemy;
	
    var current_alpha = enemy[? GAME_CHAR_ALPHA];

	
    if battle_get_current_action_char() == enemy || hover || (obj_control.enemies_choice_enable && obj_control.current_enemies_choice == i)
    {
		if battle_char_get_buff_level(enemy,BUFF_HIDE) <= 0
			draw_sprite_in_visible_area(spr,1,xx,yy,xscale,yscale,c_battle_outline,current_alpha,0,0,room_width,obj_main.y);
    }
	draw_sprite_in_visible_area(spr,-1,xx,yy,xscale,yscale,current_color,current_alpha,0,0,room_width,obj_main.y);


	
	
	if var_map_get(VAR_MODE) == VAR_MODE_SEX_CHOOSING
	{
		if enemy[? GAME_CHAR_SEX]
		{
			enemy[? GAME_CHAR_COLOR] = c_white;
			enemy[? GAME_CHAR_COLOR_TARGET] = c_white;
			draw_sprite_ext(spr_button_tick,-1,enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)/2,1,1,0,c_white,1);
			draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+")",fa_center,fa_top,false,COLOR_DAMAGE);
		}

		else
		{
			enemy[? GAME_CHAR_COLOR] = c_lightblack;
			enemy[? GAME_CHAR_COLOR_TARGET] = c_lightblack;
			draw_sprite_ext(spr_button_tick,-1,enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)/2,1,1,0,c_lightblack,1);
			draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+")",fa_center,fa_top,false,COLOR_DAMAGE);
		}
			
		if action_button_pressed_get() && hover
		{
			action_button_pressed_set(false);
			enemy[? GAME_CHAR_SEX] = !enemy[? GAME_CHAR_SEX];

			scene_choices_list_clear();

			if enemy_get_sex_number() == enemy_get_number()
				scene_choice_add("Sex all","Afterbattle_2");
			else if enemy_get_sex_number() == 2
				scene_choice_add("Sex both","Afterbattle_2");
			else if enemy_get_sex_number() > 0
				scene_choice_add("Sex","Afterbattle_2");
			else if enemy_get_number() > 2
				scene_choice_add("Dismiss all","Afterbattle_2");
			else if enemy_get_number() == 2
				scene_choice_add("Dismiss both","Afterbattle_2");
			else
				scene_choice_add("Dismiss","Afterbattle_2");
		}	
	}
	
	if var_map_get(VAR_MODE) == VAR_MODE_CAPTURE_CHOOSING
	{
		if !enemy[? GAME_CHAR_SEX]
		{
			enemy[? GAME_CHAR_COLOR] = c_black;
			enemy[? GAME_CHAR_COLOR_TARGET] = c_black;
			//draw_sprite_ext(spr_button_tick,-1,enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)/2,1,1,0,c_white,1);
			//draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+")",fa_center,fa_top,false,COLOR_DAMAGE);
		}
		else if enemy[? GAME_CHAR_CAPTURE]
		{
			enemy[? GAME_CHAR_COLOR] = c_white;
			enemy[? GAME_CHAR_COLOR_TARGET] = c_white;
			draw_sprite_ext(spr_button_tick,-1,enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)/2,1,1,0,c_white,1);
			if enemy[? GAME_CHAR_IMPREGNATE]
				draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+") (possibly pregnant)",fa_center,fa_top,false,COLOR_DAMAGE);
			else
				draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+")",fa_center,fa_top,false,COLOR_DAMAGE);
		}
		else
		{
			enemy[? GAME_CHAR_COLOR] = c_lightblack;
			enemy[? GAME_CHAR_COLOR_TARGET] = c_lightblack;
			draw_sprite_ext(spr_button_tick,-1,enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)/2,1,1,0,c_lightblack,1);
			if enemy[? GAME_CHAR_IMPREGNATE]
				draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+") (possibly pregnant)",fa_center,fa_top,false,COLOR_DAMAGE);
			else
				draw_tooltip(enemy[? GAME_CHAR_X]+sprite_get_width(spr)*xscale/2,10+sprite_get_height(spr_button_tick)+10,enemy[? GAME_CHAR_DISPLAYNAME]+" ("+enemy[? GAME_CHAR_GENDER]+")",fa_center,fa_top,false,COLOR_DAMAGE);
		}
			
		if action_button_pressed_get() && hover && enemy[? GAME_CHAR_SEX]
		{
			action_button_pressed_set(false);
			enemy[? GAME_CHAR_CAPTURE] = !enemy[? GAME_CHAR_CAPTURE];

			if enemy[? GAME_CHAR_CAPTURE]
			{
				for(var ii = 0; ii < ds_list_size(enemy_list);ii++)
				{
					if i != ii
					{
						var enemy_ii = enemy_list[| ii];
						enemy_ii[? GAME_CHAR_CAPTURE] = false;
					}
				}
			}

			scene_choices_list_clear();

			if enemy_get_capture_number() >= 1
				scene_choice_add("Capture","Aftercapturing");
			else
				scene_choice_add("Dismiss","Aftercapturing");

		}	

	}
	
	if enemy[? GAME_CHAR_HEALTH_VISUAL] != enemy[? GAME_CHAR_HEALTH] ||
	   enemy[? GAME_CHAR_LUST_VISUAL] != enemy[? GAME_CHAR_LUST] ||
	   hover ||
	   (obj_control.enemies_choice_enable && obj_control.current_enemies_choice == i)
		var show_detail = (var_map_get(VAR_MODE) == VAR_MODE_BATTLE);
	else
		var show_detail = false;
		
		
	// bars
	var bar_padding = 20;
	var bar_xx=x1+bar_padding;
		
		// Level and Name
		var text_xx = bar_xx+5;
		var text_yy = 18;
			
		if show_detail
		{
			draw_set_font(obj_control.font_01_sc_bo);
			draw_tooltip(text_xx,text_yy,"LV"+string(round(enemy[? GAME_CHAR_LEVEL]))+" "+enemy[? GAME_CHAR_DISPLAYNAME],fa_left,fa_top,false,COLOR_DAMAGE);
		}
				
	var bar_width = x2-x1-bar_padding*2;
	var bar_height = 20;
	var bar_yy=50;
	var bar_text_size_ratio = 1;
	var bar_text_outline_width = 2;
	var bar_text_outline_fid = 10;
		
	if show_detail
	{
		var bar_color = COLOR_HP;
		var bar_color_dark = COLOR_HP_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_get_health_max(enemy);
		var bar_value = enemy[? GAME_CHAR_HEALTH];
		var bar_value_visual = enemy[? GAME_CHAR_HEALTH_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar1_text = "Health: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
		draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height);
        
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height-1,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);

	}


	bar_yy+=20;
			
	if show_detail
	{
				
		var bar_color = COLOR_LUST;
		var bar_color_dark = COLOR_LUST_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_get_lust_max(enemy);
		var bar_value = enemy[? GAME_CHAR_LUST];
		var bar_value_visual = enemy[? GAME_CHAR_LUST_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar1_text = "Lust: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
		draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height);
        
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height-1,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
	
	}


	
	// draw buff debuff icons
	draw_reset();
	draw_set_alpha(0.6+0.4*show_detail);
			
	var buff_list = enemy[? GAME_CHAR_BUFF_LIST];
	var buff_turn_list = enemy[? GAME_CHAR_BUFF_TURN_LIST];
	var debuff_list = enemy[? GAME_CHAR_DEBUFF_LIST];
	var debuff_turn_list = enemy[? GAME_CHAR_DEBUFF_TURN_LIST];
	var buff_debuff_icon_width = obj_control.buff_debuff_icon_sizeS;
	var buff_debuff_icon_height = obj_control.buff_debuff_icon_sizeS;
	var buff_debuff_icon_xx = bar_xx+bar_width-buff_debuff_icon_width;
	var buff_debuff_icon_yy = bar_yy+bar_height;
	var buff_debuff_icon_padding = 20;
		
	for(var j = 0;j<ds_list_size(debuff_list);j++)
	{			
		var debuff = debuff_list[| j];
		//show_debug_message("debuff: "+string(debuff));
		var debuff_icon = short_term_buff_debuff_get_icon(debuff);
		//show_debug_message("debuff_icon: "+sprite_get_name(debuff_icon));
		draw_sprite(debuff_icon,0,buff_debuff_icon_xx+sprite_get_xoffset(debuff_icon),buff_debuff_icon_yy+sprite_get_yoffset(debuff_icon));
		//draw_sprite_in_area(debuff_icon,buff_debuff_icon_xx+sprite_get_xoffset(debuff_icon),buff_debuff_icon_yy+sprite_get_yoffset(debuff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
		if debuff_turn_list[| j] > 1 draw_text_outlined(buff_debuff_icon_xx,buff_debuff_icon_yy,ceil(debuff_turn_list[| j]),c_silver,c_lightblack,0.5,1,3);
		if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(debuff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
		buff_debuff_icon_xx-=buff_debuff_icon_width+buff_debuff_icon_padding;
	}
		
	for(var j = 0;j<ds_list_size(buff_list);j++)
	{			
		var buff = buff_list[| j];
		var buff_icon = short_term_buff_debuff_get_icon(buff);
		draw_sprite(buff_icon,0,buff_debuff_icon_xx+sprite_get_xoffset(buff_icon),buff_debuff_icon_yy+sprite_get_yoffset(buff_icon));
		//draw_sprite_in_area(buff_icon,buff_debuff_icon_xx+sprite_get_xoffset(buff_icon),buff_debuff_icon_yy+sprite_get_yoffset(buff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
		if buff_turn_list[| j] > 1 draw_text_outlined(buff_debuff_icon_xx,buff_debuff_icon_yy,ceil(buff_turn_list[| j]),c_silver,c_lightblack,0.5,1,3);
		if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(buff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
		buff_debuff_icon_xx-=buff_debuff_icon_width+buff_debuff_icon_padding;
	}	
			
			
	draw_reset();
	
	
	/*
	    gpu_set_fog(true,c_white,0,0);
        draw_sprite_in_visible_area(spr,-1,xx,yy,xscale,yscale,c_white,current_alpha*obj_control.flashing_alpha,0,0,room_width,obj_main.y);
        gpu_set_fog(false,c_black,0,0);
	*/
    /*if hover
        draw_tooltip_all_stats(xx+spr_width+padding,yy+padding,enemy);*/

}
draw_reset();