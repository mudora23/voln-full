///battle_process_bam2_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();

	
	var char_source = battle_get_current_action_char();

	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	
	skill_spend_cost(char_source,SKILL_BAM2,char_get_skill_level(char_source,SKILL_BAM2));

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" casts",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" Bam!",TAG_FONT_N,TAG_COLOR_EFFECT));
	ds_list_add(battlelog_pieces_list,text_add_markup(" at",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));

	// performing
	var dodge_chance = char_get_dodge_chance(char_target);
	var hit_chance = char_get_hit_chance(char_source);
	

	
	//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_TORU);
	
	
		postfx_ps_onhit(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
		postfx_ps_largehit(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
		script_execute_add(0.2,postfx_ps_largehit,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
	
	if percent_chance(80-dodge_chance+hit_chance)
	{

		if char_get_skill_level(char_source,SKILL_BAM2) >= 3
			var damage = char_get_phy_damage(char_source)* random_range(1.6,2.0);
		else if char_get_skill_level(char_source,SKILL_BAM2) == 2
			var damage = char_get_phy_damage(char_source)* random_range(1.3,1.6);
		else
			var damage = char_get_phy_damage(char_source)* random_range(1.1,1.3);
			
		if char_get_skill_level(char_source,SKILL_BAM2) >= 3
			var damage2 = char_get_toru_damage(char_source)* random_range(0.4,0.5);
		else if char_get_skill_level(char_source,SKILL_BAM2) == 2
			var damage2 = char_get_toru_damage(char_source)* random_range(0.3,0.4);
		else
			var damage2 = char_get_toru_damage(char_source)* random_range(0.2,0.3);
			
		var damage_size_adjust = 1;
		var shake_size_adjust = 1;
		if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);voln_play_sfx(sfx_Hit8);damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
		var armor = char_get_phy_armor(char_target);
		var armor2 = char_get_toru_armor(char_target);
		var real_damage = damage * clamp(((100-armor)/100),0,1) + damage2 * clamp(((100-armor2)/100),0,1);
		char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage);
		
		
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP,0.5,1.2*damage_size_adjust);
		
		ally_get_hit_port(char_target);
		
		ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),TAG_FONT_N,TAG_COLOR_DAMAGE));
		ds_list_add(battlelog_pieces_list,text_add_markup(" damage!",TAG_FONT_N,TAG_COLOR_NORMAL));
		
		char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
		char_target[? GAME_CHAR_SHAKE_AMOUNT] = 30*shake_size_adjust;
			
		var debug_message = "Bam is performed,";
		debug_message+= " source: ";
		debug_message+= string(damage);
		debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
		debug_message+= " target: ";
		debug_message+= string(armor);
		debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";

	}
	else
	{
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,0,0.7);
		ds_list_add(battlelog_pieces_list,text_add_markup(" and misses!",TAG_FONT_N,TAG_COLOR_NORMAL));
	}	
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	voln_play_sfx(sfx_Hit7);

	script_execute_add(BATTLE_PAUSE_TIME_LONG_SEC,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}