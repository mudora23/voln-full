///item_get_price(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	
	var item_map = item_get_map(name);
	
	
	if ds_map_exists(item_map,ITEM_PRICE)
		var item_price = item_map[? ITEM_PRICE];
	else
		var item_price = 0;
	
	return item_price;
	
}