///S_questionaire();
/// @description

_label("start");

	_main_gui(VAR_GUI_MIN);
	_dialog("S_START_1");
	_dialog("S_START_Q0",true);
	_player_name_input(480,430,"Q1");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Q1");
	_dialog("S_START_Q1",true);
	_choice("Normal","Q2","","",true,true,true,"",extrascript_normal_male);
	_choice("Tough","Q2","","",true,true,true,"",extrascript_tough_male);
	_choice("Effeminate","Q2","","",true,true,true,"",extrascript_effeminate_male);
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Q2");
	_dialog("S_START_Q2",true);
	_choice("Grey","Q3","","",true,true,true,"",var_map_set,PC_Eye_Color_N,0);
	_choice("Ice-blue","Q3","","",true,true,true,"",var_map_set,PC_Eye_Color_N,1);
	_choice("Blue","Q3","","",true,true,true,"",var_map_set,PC_Eye_Color_N,2);
	_choice("Green","Q3","","",true,true,true,"",var_map_set,PC_Eye_Color_N,3);
	_choice("Brown","Q3","","",true,true,true,"",var_map_set,PC_Eye_Color_N,4);
	_choice("Black","Q3","","",true,true,true,"",var_map_set,PC_Eye_Color_N,5);
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Q3");
	_dialog("S_START_Q3",true);
	_choice("White","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,0);
	_choice("Silvər","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,1);
	_choice("Blond","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,2);
	_choice("Red-blond","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,3);
	_choice("Tawny","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,4);
	_choice("Brown","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,5);
	_choice("Auburn","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,6);
	_choice("Red","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,7);
	_choice("Black","Q4","","",true,true,true,"",var_map_set,PC_Hair_Color_N,8);
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Q4");
	_dialog("S_START_Q4",true);
	_choice("Silky","Q5","","",true,true,true,"",var_map_set,PC_Hair_N,0);
	_choice("Wavy","Q5","","",true,true,true,"",var_map_set,PC_Hair_N,1);
	_choice("Ringleted","Q5","","",true,true,true,"",var_map_set,PC_Hair_N,2);
	_choice("Voluminous","Q5","","",true,true,true,"",var_map_set,PC_Hair_N,3);
	_choice("Tightly curled","Q5","","",true,true,true,"",var_map_set,PC_Hair_N,4);
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Q5");
	_dialog("S_START_Q5",true);
	_choice("Pale","",S_0001,"",true,true,true,"",var_map_set,PC_Skin_Color_N,1);
	_choice("Pinkish","",S_0001,"",true,true,true,"",var_map_set,PC_Skin_Color_N,2);
	_choice("Tannish","",S_0001,"",true,true,true,"",var_map_set,PC_Skin_Color_N,3);
	_choice("Tan","",S_0001,"",true,true,true,"",var_map_set,PC_Skin_Color_N,4);
	_choice("Brown","",S_0001,"",true,true,true,"",var_map_set,PC_Skin_Color_N,5);
	_choice("Blackish","",S_0001,"",true,true,true,"",var_map_set,PC_Skin_Color_N,6);
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


