///char_stats_recalculate(char);
/// @description
/// @param char

var char = argument0;
if char != noone && !is_undefined(char)
{
	var baselevel_stats = 5;
	var charlevel = char[? GAME_CHAR_LEVEL];
	var char_name = char[? GAME_CHAR_NAME];

	var wiki_char = obj_control.char_wiki_map[? char_name];
	
	if char[? GAME_CHAR_FROM_BREEDING]
		var default_stats_char = char;
	else
		var default_stats_char = wiki_char;

	char[? GAME_CHAR_STR] = (default_stats_char[? GAME_CHAR_STR_RATE] + char[? GAME_CHAR_BONUS_STR_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_STR_FLAT];	
	char[? GAME_CHAR_FOU] = (default_stats_char[? GAME_CHAR_FOU_RATE] + char[? GAME_CHAR_BONUS_FOU_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_FOU_FLAT];
	char[? GAME_CHAR_DEX] = (default_stats_char[? GAME_CHAR_DEX_RATE] + char[? GAME_CHAR_BONUS_DEX_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_DEX_FLAT];
	char[? GAME_CHAR_END] = (default_stats_char[? GAME_CHAR_END_RATE] + char[? GAME_CHAR_BONUS_END_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_END_FLAT];
	char[? GAME_CHAR_INT] = (default_stats_char[? GAME_CHAR_INT_RATE] + char[? GAME_CHAR_BONUS_INT_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_INT_FLAT];
	char[? GAME_CHAR_WIS] = (default_stats_char[? GAME_CHAR_WIS_RATE] + char[? GAME_CHAR_BONUS_WIS_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_WIS_FLAT];
	char[? GAME_CHAR_SPI] = (default_stats_char[? GAME_CHAR_SPI_RATE] + char[? GAME_CHAR_BONUS_SPI_RATE])*(charlevel+baselevel_stats) + char[? GAME_CHAR_BONUS_SPI_FLAT];


}

