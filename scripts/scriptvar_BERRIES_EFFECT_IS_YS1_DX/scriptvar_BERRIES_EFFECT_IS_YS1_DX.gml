///scriptvar_BERRIES_EFFECT_IS_YS1_DX();
/// @description

if scriptvar_BERRIES_EFFECT_IS_YS1()
	return "  The feeling is focused in your "+scriptvar_PC_Nads_Size_Desc()+" nads.  It feels like they’re pulsing, pumping, gently as the pressure increases.";
else
	return "";