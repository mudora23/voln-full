///battle_char_get_sfx_from_wiki(char,battle_CMD);
/// @description
/// @param char
/// @param CMD

if argument0 < 0 return noone;

var char = argument0;
var battle_CMD = argument1;
var wiki_map = obj_control.char_wiki_map;

var wiki_char = ds_map_find_value(wiki_map,char[? GAME_CHAR_NAME]);
if ds_map_exists(wiki_char,script_get_name_ext(battle_CMD))
{
	var selected_sfx = ds_list_find_value(wiki_char[? script_get_name_ext(battle_CMD)],irandom_range(0,ds_list_size(wiki_char[? script_get_name_ext(battle_CMD)])-1));
	if selected_sfx != noone && !is_undefined(selected_sfx)
		return selected_sfx;
	else
		return noone;
}
else
	return noone;


