///_hidecg_all_except(ID,ID2...,fade_speed);
/// @description  
/// @param ID
/// @param ID2...
/// @param fade_speed

if scene_is_current()
{
	var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	var cg_fade_speed_list = obj_control.var_map[? VAR_CG_LIST_FADE_SPEED];
	var cg_alpha_target_list = obj_control.var_map[? VAR_CG_LIST_ALPHA_TARGET];
	
	for(var i = 0; i < ds_list_size(cg_id_list);i++)
	{
		if argument_count > 1 && cg_id_list[| i] == argument[0] ||
		   argument_count > 2 && cg_id_list[| i] == argument[1] ||
		   argument_count > 3 && cg_id_list[| i] == argument[2] ||
		   argument_count > 4 && cg_id_list[| i] == argument[3] ||
		   argument_count > 5 && cg_id_list[| i] == argument[4] ||
		   argument_count > 6 && cg_id_list[| i] == argument[5] ||
		   argument_count > 7 && cg_id_list[| i] == argument[6] ||
		   argument_count > 8 && cg_id_list[| i] == argument[7] ||
		   argument_count > 9 && cg_id_list[| i] == argument[8] ||
		   argument_count > 10 && cg_id_list[| i] == argument[9] ||
		   argument_count > 11 && cg_id_list[| i] == argument[10] ||
		   argument_count > 12 && cg_id_list[| i] == argument[11] ||
		   argument_count > 13 && cg_id_list[| i] == argument[12] ||
		   argument_count > 14 && cg_id_list[| i] == argument[13] ||
		   argument_count > 15 && cg_id_list[| i] == argument[14]
		{
			cg_fade_speed_list[| i] = argument[argument_count-1];
			cg_alpha_target_list[| i] = 0;
		}
	}
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);