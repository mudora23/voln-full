///SL37();
/// @description Giş

_label("start");
	if !chunk_mark_get("SL37_Nun_Blessing_First")
		_jump("SL37_Nun_Blessing_First");
	else
		_jump("SL37_Nun_Blessing_Recurring");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL37_Nun_Blessing_First");
	_dialog("SL37_Nun_Blessing_First");
	_show("est",spr_char_Est_st_mel,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL37_Nun_Blessing_First_2");
	_dialog("SL37_Nun_Blessing_First_3");
	_dialog("SL37_Nun_Blessing_First_4");
	_dialog("SL37_Nun_Blessing_First_5");
	_dialog("SL37_Nun_Blessing_First_6");
	_dialog("SL37_Nun_Blessing_First_7");
	_dialog("SL37_Nun_Blessing_First_8",true);
	_jump("SL37_Nun_Blessing_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL37_Nun_Blessing_Recurring");
	_dialog("SL37_Nun_Blessing_Recurring");
	_show("est",spr_char_Est_st_mel,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL37_Nun_Blessing_Recurring_2",true);
	if var_map_get(VAR_HOURS_SINCE_LAST_BLESSING) >= 3
		_choice("“I’d like to make another donation.”","SL37_Nun_Blessing_Donate");
	else
		_choice("[?]“I’d like to make another donation.”","SL37_Nun_Blessing_Donate");
	_choice("“Very well.”","SL37_Nun_Blessing_Refuse");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL37_Nun_Blessing_Choice");
	if var_map_get(VAR_HOURS_SINCE_LAST_BLESSING) >= 3
		_choice("Donate","SL37_Nun_Blessing_Donate");
	else
		_choice("[?]Donate","SL37_Nun_Blessing_Donate");
	if var_map_get(VAR_HOURS_SINCE_LAST_BLESSING) >= 3
		_choice("Refuse","SL37_Nun_Blessing_Refuse");
	else
		_choice("Leave","SL37_Exit");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL37_Nun_Blessing_Donate");
	_var_map_set(VAR_HOURS_SINCE_LAST_BLESSING,0);
	if !chunk_mark_get("SL37_Nun_Blessing_Donate_1")
		_char_add_opinion(NAME_EST,5);
	else
		_void();
	_dialog("SL37_Nun_Blessing_Donate_1");
	_dialog("SL37_Nun_Blessing_Donate_2");
	_dialog("SL37_Nun_Blessing_Donate_3");
	_execute(0,scene_recover_rest_2hours);
	_jump("SL37_Exit");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL37_Nun_Blessing_Refuse");
	if !chunk_mark_get("SL37_Nun_Blessing_Donate_1") && !chunk_mark_get("SL37_Nun_Blessing_Refuse_1")
		_char_add_opinion(NAME_EST,-5);
	else
		_void();
	_dialog("SL37_Nun_Blessing_Refuse_1");
	_dialog("SL37_Nun_Blessing_Refuse_2");
	_jump("SL37_Exit");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL37_Exit");	
	_map();
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////		
		
		
		