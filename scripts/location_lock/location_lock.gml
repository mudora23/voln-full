///location_lock(LOCATION,LOCATION...);
/// @description
/// @param LOCATION
/// @param LOCATION...

with obj_control
{
	var i = 0;
	while argument_count > i
	{
		if string_count("LOCATION_",argument[i]) == 0
			ds_list_delete_value(var_map_get(VAR_LOCATION_UNLOCKED_LIST),"LOCATION_"+string(argument[i]));
		else
			ds_list_delete_value(var_map_get(VAR_LOCATION_UNLOCKED_LIST),argument[i]);
		i++;
	}
}


