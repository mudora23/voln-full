///scriptvar_PC_Skin_Color_Desc();
/// @description
var num = var_map_get(PC_Skin_Color_N);
if num == 0 return choose("milk-white","unnaturally white","brilliant white","snow-colored");
else if num == 1 return choose("pale","sallow","pasty","wan");
else if num == 2 return choose("pinkish","light","bright","light pink");
else if num == 3 return choose("tannish","cream-colored","beigish","yellowish");
else if num == 4 return choose("tan","brownish","rust-colored");
else if num == 5 return choose("brown","dark","swarthy","mud-colored");
else if num == 6 return choose("blackish","dusky","inky","shadowy");
else if num == 7 return choose("grey","ash-grey","deep grey","ashen");
else if num == 8 return choose("purplish","purple-grey");
else if num == 9 return choose("plum-colored","purple");
else if num == 10 return choose("muddy green","greenish","greenish brown","glaucous");
else if num == 11 return choose("green","viridescent","virescent","grass-colored");
else if num == 12 return choose("yellow","ambər-colored","gold-colored");
else if num == 13 return choose("tawny","fulvous","orangish");
else if num == 14 return choose("red","sanguine","rubicund","blood-colored");
else return choose("pink","blush-colored","floral pink","flowery pink");