///scriptvar_PC_Peen_SizeL_Desc();
/// @description
var gnum = var_map_get(PC_Peen_SizeG_N);
var lnum = var_map_get(PC_Peen_SizeL_N);
var returning_str = "";

if lnum < 4 && gnum <= 1 returning_str+= choose("pud, ","snag, ","diŋkər, ","twig, ");
else if lnum < 4 returning_str+= choose("pud, ","snag, ","diŋkər, ");
if number_in_range_strictly_less(lnum,4,7) && gnum <= 1 returning_str+= choose("horn, ","beef, ","bellind, ","rammər, ","dork, ","peen, ","twig, ");
else if number_in_range_strictly_less(lnum,4,7) returning_str+= choose("horn, ","beef, ","bellind, ","rammər, ","dork, ","peen, ");
if lnum >= 7 && gnum <= 1 returning_str+= choose("porkər, ","runt-leg, ","gun, ","twig, ");
else if lnum >= 7 returning_str+= choose("porkər, ","runt-leg, ","gun, ");

if string_length(returning_str) > 2
    returning_str = string_copy(returning_str,1,string_length(returning_str)-2);

return returning_str;