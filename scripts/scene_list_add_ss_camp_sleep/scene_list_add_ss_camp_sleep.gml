///scene_list_add_ss_camp_sleep(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];

if ally_exists(NAME_KUDI) && char_get_char_opinion(NAME_KUDI,char_get_player()) >= 1
	ds_list_add(possible_list,"SS_CAMP_SLEEP_1_1");
if ally_exists(NAME_BALWAG) && char_get_char_opinion(NAME_BALWAG,char_get_player()) >= 1
	ds_list_add(possible_list,"SS_CAMP_SLEEP_2_1");


return possible_list;

