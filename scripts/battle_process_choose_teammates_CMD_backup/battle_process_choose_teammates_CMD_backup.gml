///battle_process_choose_teammates_CMD(CMD,extra_arg[optional]);
/// @description
/// @param CMD
with obj_control
{
	scene_choices_list_clear();
	obj_control.allies_choice_enable = true;
	
	var char_map = battle_get_current_action_char();
	var char_CMD = script_get_index_ext(argument[0]);
	var player = var_map[? VAR_PLAYER];
	
	if char_map[? GAME_CHAR_OWNER] == OWNER_PLAYER
	{
		var ally_list = var_map[? VAR_ALLY_LIST];
		if battle_char_get_combat_capable(player)
		{
			if argument_count > 1
				scene_choiceCMD_add(player[? GAME_CHAR_DISPLAYNAME],char_CMD,ds_list_size(ally_list),argument[1]);
			else
				scene_choiceCMD_add(player[? GAME_CHAR_DISPLAYNAME],char_CMD,ds_list_size(ally_list));
		}
		
		for(var i = 0; i < ds_list_size(ally_list);i++)
		{
			var ally = ally_list[| i];
			if battle_char_get_combat_capable(ally)
			{
				if argument_count > 1
					scene_choiceCMD_add(ally[? GAME_CHAR_DISPLAYNAME],char_CMD,i,argument[1]);
				else
					scene_choiceCMD_add(ally[? GAME_CHAR_DISPLAYNAME],char_CMD,i);
			}
		}
		scene_choiceCMD_add("Cancel",battle_process_player);
		
		//show_debug_message("------Battle 'choose (Player) CMD script' is executed!------");
		//show_debug_message("------Battle 'choose (Player) CMD script' char_CMD script is "+string(argument[0]));
	}
	
	
	
	else
	{
		
		
		if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
			var choosing_list = var_map[? VAR_ENEMY_LIST];
		else
			var choosing_list = var_map[? VAR_ALLY_LIST];
			
		var possible_list = ds_list_create();
		
		for(var i = 0; i < ds_list_size(choosing_list);i++)
		{
			var choosing_list_char = choosing_list[| i];
			if battle_char_get_combat_capable(choosing_list_char)
				ds_list_add(possible_list,i);
		}
		
		if char_map[? GAME_CHAR_OWNER] == OWNER_ALLY
		{
			var player_map = var_map[? VAR_PLAYER];
			if battle_char_get_combat_capable(player_map)
				ds_list_add(possible_list,ds_list_size(choosing_list));
		}
		
		if ds_list_size(possible_list) == 0
		{
			battle_process_wait_CMD();
			//if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY || char_map[? GAME_CHAR_OWNER] == OWNER_ALLY
				//scene_script_execute_add_unique(0,scene_script_battle_process_AI);
			//else
				//scene_script_execute_add_unique(0,scene_script_battle_process_player);
		}
		else
		{
			if (char_map[? GAME_CHAR_OWNER] == OWNER_ALLY && percent_chance(80)) ||
			   (char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY && percent_chance(100/battle_player_ally_get_combat_capable_number()))
			{
				if char_map[? GAME_CHAR_OWNER] == OWNER_ALLY
				{
					var the_char = battle_char_list_get_lowest_hp_capable_char(var_map[? VAR_ALLY_LIST]);
					if battle_char_get_combat_capable(player) && (the_char == noone || the_char[? GAME_CHAR_HEALTH] > player[? GAME_CHAR_HEALTH])
					{
						if argument_count > 1
							script_execute(char_CMD,ds_list_size(var_map[? VAR_ALLY_LIST]),argument[1]);
						else
							script_execute(char_CMD,ds_list_size(var_map[? VAR_ALLY_LIST]));
					}
					else if the_char != noone
					{
						if argument_count > 1
							script_execute(char_CMD,battle_char_list_get_lowest_hp_capable_char_i(var_map[? VAR_ALLY_LIST]),argument[1]);
						else	
							script_execute(char_CMD,battle_char_list_get_lowest_hp_capable_char_i(var_map[? VAR_ALLY_LIST]));
					}
					else
						battle_process_wait_CMD();
				}
				if char_map[? GAME_CHAR_OWNER] == OWNER_ENEMY
				{
					var the_char = battle_char_list_get_lowest_hp_capable_char(var_map[? VAR_ENEMY_LIST]);
					if the_char != noone
					{
						if argument_count > 1
							script_execute(char_CMD,battle_char_list_get_lowest_hp_capable_char_i(var_map[? VAR_ENEMY_LIST]),argument[1]);
						else
							script_execute(char_CMD,battle_char_list_get_lowest_hp_capable_char_i(var_map[? VAR_ENEMY_LIST]));
					}
					else
						battle_process_wait_CMD();
				}

			}
			else
			{
				ds_list_shuffle(possible_list);
				if argument_count > 1
					script_execute(char_CMD,possible_list[| 0],argument[1]);
				else
					script_execute(char_CMD,possible_list[| 0]);
			}
		}
		
		ds_list_destroy(possible_list);
		
		//show_debug_message("------Battle 'choose (AI) CMD script' is executed!------");
	}	
		
	/*for(var i = 0; i < ds_list_size(choosing_list);i++)
	{
		var char_map = choosing_list[| i];
	}*/
	
	

}