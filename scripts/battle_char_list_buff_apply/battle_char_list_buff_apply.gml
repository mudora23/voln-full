///battle_char_list_buff_apply(char_list,buff,level,turns,source_char_id[optional]);
/// @description
/// @param char_list
/// @param buff
/// @param level
/// @param turns
/// @param source_char_id[optional]
for(var i = 0; i < ds_list_size(argument[0]);i++)
{
	if argument_count > 4
		battle_char_buff_apply(ds_list_find_value(argument[0],i),argument[1],argument[2],argument[3],argument[4]);
	else
		battle_char_buff_apply(ds_list_find_value(argument[0],i),argument[1],argument[2],argument[3]);
}