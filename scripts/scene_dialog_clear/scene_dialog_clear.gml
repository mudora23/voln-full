///scene_dialog_clear();
/// @description
with obj_control
{
	var_map_set(VAR_SCRIPT_DIALOG,"");
	var_map_set(VAR_SCRIPT_DIALOG_RAW,"");
	var_map_set(VAR_SCRIPT_DIALOG_SPEAKER,"");
	if surface_exists(var_map_get(VAR_SCRIPT_DIALOG_SURFACE))
	{
		surface_free(var_map_get(VAR_SCRIPT_DIALOG_SURFACE));
		var_map_set(VAR_SCRIPT_DIALOG_SURFACE,noone);
	}
	obj_control.debug_chunk_name = "";
}