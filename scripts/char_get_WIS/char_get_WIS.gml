///char_get_WIS(char);
/// @description
/// @param char
var amount = char_get_info(argument0,GAME_CHAR_WIS);

if char_get_skill_level(argument0,SKILL_START) amount+= 3;

amount += char_get_skill_level(argument0,SKILL_WIS_1);
amount += char_get_skill_level(argument0,SKILL_WIS_2);
amount += char_get_skill_level(argument0,SKILL_WIS_3);
amount += char_get_skill_level(argument0,SKILL_WIS_4);
amount += char_get_skill_level(argument0,SKILL_WIS_5);
amount += char_get_skill_level(argument0,SKILL_WIS_6);

if argument0[? GAME_CHAR_SUPERIOR] amount*= 1.2;

return amount;