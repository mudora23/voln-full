///location_map_update_visited_and_visible_info();
/// @description
with obj_control
{
	var oName = obj_control.var_map[? VAR_LOCATION];
	var path_and_neighbor_list = obj_control.var_map[? VAR_LOCATION_PATH_AND_NEIGHBOR_LIST];
	
	// update visited information
	ds_list_add_unique(obj_control.var_map[? VAR_LOCATION_VISITED_LIST],oName);
		
	// update visible information
	ds_list_add_unique(obj_control.var_map[? VAR_LOCATION_VISIBLE_LIST],oName);
	
	
	for(var i = 0; i < ds_list_size(path_and_neighbor_list);i+=2)
	{
		var location_name = path_and_neighbor_list[| i+1];
		var location_info = location_map[? location_name];
		var location_hidden = location_is_locked(location_name);//location_info[? "hidden"];
		if !location_hidden
			ds_list_add_unique(obj_control.var_map[? VAR_LOCATION_VISIBLE_LIST],location_name);
	}
}