///draw_floating_text_debug(string);

ds_list_add(obj_floating_text.floating_text_string,"<DEBUG> "+string(argument[0]));
ds_list_add(obj_floating_text.floating_text_x,room_width/2);
ds_list_add(obj_floating_text.floating_text_y,room_height/2);
ds_list_add(obj_floating_text.floating_text_increasing,true);
ds_list_add(obj_floating_text.floating_text_alpha,0);
ds_list_add(obj_floating_text.floating_text_life,1);
ds_list_add(obj_floating_text.floating_text_sign,"");  
ds_list_add(obj_floating_text.floating_text_color,c_fuchsia); 
ds_list_add(obj_floating_text.floating_text_delay,0);
ds_list_add(obj_floating_text.floating_text_scale,1); 
