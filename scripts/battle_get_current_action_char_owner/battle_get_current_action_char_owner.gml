///char_get_current_action_char_owner();
/// @description
var char = battle_get_current_action_char();
if char != noone
	return char[? GAME_CHAR_OWNER];
else
	return "";
