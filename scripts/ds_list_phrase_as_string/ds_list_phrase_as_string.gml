///ds_list_phrase_as_string(list);
/// @description
/// @param list
var __list = argument[0];
var __string = "{";
for(var i = 0; i < ds_list_size(__list); i++)
{
	if !is_string(__list[| i])
		__string += string(round(__list[| i]))+", ";
	else
		__string += string(__list[| i])+", ";
}
__string += "}";

return __string;