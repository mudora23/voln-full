///draw_char_info_debug(char);
/// @description
/// @param char


var xx = room_width-300;
var yy = 111;

draw_set_color(c_lightblack);
draw_set_alpha(0.95);
draw_rectangle(xx,yy,room_width,room_height,false);

var the_char = argument0;

draw_set_color(c_white);
draw_sprite(sprite_get_index_ext(the_char[? GAME_CHAR_THUMB]),0,xx,yy);

yy+= 320;
xx+= 150;
draw_set_halign(fa_center);
draw_set_valign(fa_top);
draw_set_font(font_01s_reg);

draw_text(xx, yy, "ID: "+string(the_char[? GAME_CHAR_ID]));
yy += 30;
draw_text(xx, yy, "Parent ID (Mom): "+string(the_char[? GAME_CHAR_MOM_ID]));
yy += 30;
draw_text(xx, yy, "Parent ID (Dad): "+string(the_char[? GAME_CHAR_DAD_ID]));
yy += 30;
draw_text(xx, yy, "Name: "+the_char[? GAME_CHAR_NAME]);
yy += 30;
draw_text(xx, yy, "Dispaly Name: "+the_char[? GAME_CHAR_DISPLAYNAME]);
yy += 30;
draw_text(xx, yy, "Gender: "+the_char[? GAME_CHAR_GENDER]);
yy += 30;
draw_text(xx, yy, "Adult: "+string(the_char[? GAME_CHAR_ADULT]));
yy += 30;
draw_text(xx, yy, "Level: "+string(the_char[? GAME_CHAR_LEVEL]));
yy += 30;
draw_text(xx, yy, "Vitality: "+string(round(the_char[? GAME_CHAR_VITALITY]))+"/"+string(round(char_get_vitality_max(the_char))));
yy += 30;
draw_text(xx, yy, "Health: "+string(round(the_char[? GAME_CHAR_HEALTH]))+"/"+string(round(char_get_health_max(the_char))));
yy += 30;
draw_text(xx, yy, "Lust: "+string(round(the_char[? GAME_CHAR_LUST]))+"/"+string(round(char_get_lust_max(the_char))));
yy += 30;
draw_text(xx, yy, "Toru: "+string(round(the_char[? GAME_CHAR_TORU]))+"/"+string(round(char_get_toru_max(the_char))));
yy += 30;
draw_text(xx, yy, "Stamina: "+string(round(the_char[? GAME_CHAR_STAMINA]))+"/"+string(round(char_get_stamina_max(the_char))));
yy += 30;
draw_text(xx, yy, "Exp: "+string(round(the_char[? GAME_CHAR_EXP]))+"/"+string(round(char_get_exp_max(the_char))));
yy += 30;
draw_text(xx, yy, "Impregnate: "+string(the_char[? GAME_CHAR_IMPREGNATE]));
yy += 30;
draw_text(xx, yy, "Impregnate by: "+string(the_char[? GAME_CHAR_IMPREGNATE_BY_CHARID]));
yy += 30;
draw_text(xx, yy, "Impregnate (Confirm): "+string(the_char[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT]));
yy += 30;
draw_text(xx, yy, "Impregnate (Bigger): "+string(the_char[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BIGGER]));
yy += 30;
draw_text(xx, yy, "Impregnate (Born): "+string(the_char[? GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BORN]));
yy += 30;
draw_text(xx, yy, "# of Skills: "+string(ds_map_size(the_char[? GAME_CHAR_SKILLS_MAP])));
yy += 30;
draw_text(xx, yy, "# of Bonus Skills: "+string(ds_map_size(the_char[? GAME_CHAR_BONUS_SKILLS_MAP])));
yy += 30;

draw_set_font(font_01n_reg);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

