///var_map_list_clear(GAME_MAP_KEY);
/// @description
/// @param GAME_MAP_KEY
var var_key = argument[0];
if ds_map_exists(obj_control.var_map,var_key)
	ds_list_clear(obj_control.var_map[? var_key]);
else
{
	//show_debug_message("ERROR: var_map_list_clear(VAR_MAP_KEY) - key not exists! key: "+string(var_key));
}