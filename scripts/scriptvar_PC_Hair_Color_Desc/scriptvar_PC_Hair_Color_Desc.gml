///scriptvar_PC_Hair_Color_Desc();
/// @description
var num = var_map_get(PC_Hair_Color_N);
if num == 0 return "white";
else if num == 1 return "silvər";
else if num == 2 return "blond";
else if num == 3 return "red-blond";
else if num == 4 return "tawny";
else if num == 5 return "brown";
else if num == 6 return "auburn";
else if num == 7 return "red";
else if num == 8 return "black";
else if num == 9 return "orange";
else if num == 10 return "yellow";
else if num == 11 return "green";
else if num == 12 return "blue"
else if num == 13 return "indigo"
else if num == 14 return "purple";
else return "pink";