///storage_add_char(name,OWNER,unique?,level[optional]);
/// @description
/// @param name
/// @param unique?
/// @param level[optional]
var char_list = obj_control.var_map[? VAR_STORAGE_LIST];
if !argument[2] || !storage_exists(argument[0])
{
	ds_list_add(char_list,ds_map_create());
	var char_index = ds_list_size(char_list)-1;
	ds_list_mark_as_map(char_list,char_index);
	
	if argument_count > 2
		game_char_map_init_from_wiki(char_list[| char_index],argument[0],argument[1],argument[3]);
	else
		game_char_map_init_from_wiki(char_list[| char_index],argument[0],argument[1]);

	char_stats_recalculate(char_list[| char_index]);
	char_skills_recalculate(char_list[| char_index]);
	
	char_set_info(char_list[| char_index],GAME_CHAR_HEALTH,char_get_health_max(char_list[| char_index]));
	char_set_info(char_list[| char_index],GAME_CHAR_TORU,char_get_toru_max(char_list[| char_index]));
	char_set_info(char_list[| char_index],GAME_CHAR_STAMINA,char_get_stamina_max(char_list[| char_index]));
	
	return char_list[| char_index];

}

return noone;