///battle_process_next_char();
/// @description
with obj_control
{
	
	// ordering list position
	ordering_list_in_battle_set_position(battle_get_current_action_char(),noone,true);	
	
	
	
	
	
	//scene_clear_ext(false,false,true,true,true);
	
	var char_map = obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER];
	if char_map >= 0
	{
		var debug_message = "Action time updated: "; // debug
		
		// time passes
		var order_list = obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST];
		for(var i = 0; i < ds_list_size(order_list);i++)
		{
			var char_map_i = order_list[| i];
			if char_map_i != char_map
				char_map_i[? GAME_CHAR_BATTLE_WAITING_TIME] -= char_map[? GAME_CHAR_BATTLE_WAITING_TIME];
			
			debug_message += string(char_map_i[? GAME_CHAR_DISPLAYNAME])+"(action "+string(char_map_i[? GAME_CHAR_BATTLE_WAITING_TIME])+"; owner "+string(char_map_i[? GAME_CHAR_OWNER])+");"; // debug
		}
		
		//draw_floating_text_debug(debug_message); // debug
		
		// Reset the action time for the current char
		var fastest_speed = 0;
		for(var i = 0; i < ds_list_size(order_list);i++)
		{
			var the_char_map = order_list[| i];
			fastest_speed = max(fastest_speed,char_get_speed(the_char_map));
		}
		char_map[? GAME_CHAR_BATTLE_WAITING_TIME] = 100 + (fastest_speed - char_get_speed(char_map))*10;
		
		
		//draw_floating_text_debug("Action time reset: "+string(char_map[? GAME_CHAR_DISPLAYNAME])+"(action "+string(char_map[? GAME_CHAR_BATTLE_WAITING_TIME])+"; owner "+string(char_map[? GAME_CHAR_OWNER])+");"); // debug
	}
	
	// Re-ordering characters order
	var order_list = obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST];
	var debug_message = "Before quicksort: (";
		for(var i = 0; i < ds_list_size(order_list);i++)
		{
			var char_map_i = order_list[| i];
			debug_message+=string(char_map_i[? GAME_CHAR_BATTLE_WAITING_TIME])+"("+string(char_map_i[? GAME_CHAR_OWNER])+"),";
		}
	debug_message+=")";
	//draw_floating_text_debug(debug_message); // debug
	ds_list_insertion_sort_mapkey(order_list,true,GAME_CHAR_BATTLE_WAITING_TIME);
	var debug_message = "After quicksort: (";
		for(var i = 0; i < ds_list_size(order_list);i++)
		{
			var char_map_i = order_list[| i];
			debug_message+=string(char_map_i[? GAME_CHAR_BATTLE_WAITING_TIME])+"("+string(char_map_i[? GAME_CHAR_OWNER])+"),";
			
			// ordering list position
			ordering_list_in_battle_set_position(char_map_i,i,false);
		}
	debug_message+=")";
	//draw_floating_text_debug(debug_message); // debug

	// Set the current action char map
	obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER] = order_list[| 0];
	
	// The character's turn start
	script_execute_add_unique(0,battle_process_turn_start);
	
	var char_map = obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER];
	//draw_floating_text_debug("------Battle 'Next character script' is executed!------");
	//draw_floating_text_debug("Next char's name: "+string(char_map[? GAME_CHAR_NAME]));
	//draw_floating_text_debug("Next char's owner: "+string(char_map[? GAME_CHAR_OWNER]));

}