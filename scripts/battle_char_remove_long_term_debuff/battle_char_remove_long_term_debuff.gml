///battle_char_remove_long_term_debuff(char,debuff,hours);
/// @description
/// @param char
/// @param debuff
/// @param hours
var char_map = argument0;
var char_debuff_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_LIST];
var char_debuff_hours_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_HOURS_LIST];
var char_debuff = argument1;
var char_debuff_hours = argument2;

var the_index = ds_list_find_index(char_map[? GAME_CHAR_LONG_TERM_DEBUFF_LIST],char_debuff);
if the_index >= 0
{
	char_debuff_hours_list[| the_index] = char_debuff_hours_list[| the_index]-char_debuff_hours;
	if char_debuff_hours_list[| the_index] <= 0
	{
		ds_list_delete(char_debuff_list,the_index);
		ds_list_delete(char_debuff_hours_list,the_index);
	}
}
