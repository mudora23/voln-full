///scriptvar_RBS_ORK_DV_1_2B_2_EX1();
///@description
if var_map_get(PC_Peen_SizeL_N) >= 4
{
	var res = "Your ";
	res += scriptvar_PC_Peen_SizeL_SizeG_Desc();
	res += " mast reaches all the way to her mouth.  She begins to get into the act, craning her head forward, and taking your ";
	res += scriptvar_PC_Peen_Glans_Shape_Desc();
	res += " bulb in her mouth, and gently sucking on it.";
	return res;
}

return "";