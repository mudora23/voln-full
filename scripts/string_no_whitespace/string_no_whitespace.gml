///string_no_whitespace(string);
/// @description
/// @param string

var returning_string = "";
var the_string = argument0;
for(var posi = 1; posi <= string_length(the_string); posi++)
{
    var the_char = string_char_at(the_string,posi);
    if the_char != " " &&
       the_char != "\n" &&
       the_char != "\r" &&
       the_char != "\b" &&
       the_char != "\f" &&
       the_char != "\t" &&
       the_char != "\v" &&
       the_char != "\a"
        {
            returning_string += the_char;
        }
}
return returning_string;