///clothes_sets(spr_substring,image_index);
/// @description
/// @param spr_substring
/// @param image_index

var clothes_sets_map = obj_control.var_map[? VAR_CLOTHES_SETS_MAP];
clothes_sets_map[? argument0] = argument1;

