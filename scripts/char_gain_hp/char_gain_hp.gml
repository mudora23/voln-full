///char_gain_hp(char,amount);
///@description char gains and loses hp within the hp cap
///@param char
///@param amount
char_set_info(argument0,GAME_CHAR_HEALTH,clamp(char_get_info(argument0,GAME_CHAR_HEALTH)+argument1,0,char_get_health_max(argument0))); 