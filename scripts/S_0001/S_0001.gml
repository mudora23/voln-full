///S_0001();
/// @description

_label("start");
	_main_gui(VAR_GUI_MIN_WITH_BG);//_main_gui(VAR_GUI_NORMAL);
	_change_bg("bg",spr_bg_Camp_Tent_01_night,FADE_NORMAL_SEC,1.5);
	_dialog("S_0001_2");
	_dialog("S_0001_3");
	_dialog("S_0001_4");
	_dialog("S_0001_5");
	_show("zeyd",spr_char_Zeyd_r_n,10,5,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0001_6");
	_dialog("S_0001_7");
	_dialog("S_0001_8",true);
	_jump("S_0001_Choice1");
	
_label("S_0001_Choice1");
	_choice("“Fine.”","S_0001_9_1A","","S_0001_9_1A");
	_choice("“Piss off.”","S_0001_11_1B");
	_choice("“You look like a gurl.”","S_0001_13_1C","","S_0001_13_1C");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0001_9_1A");
	_dialog("S_0001_9_1A");
	_dialog("S_0001_10_1A",true);
	_jump("S_0001_Choice1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0001_11_1B");
	_dialog("S_0001_11_1B");
	_dialog("S_0001_12_1B",true);
	_choice("Leave quickly.","S_0001_14_2A");
	_choice("Attempt to seduce.","S_0001_15_2B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0001_13_1C");
	_dialog("S_0001_13_1C",true);
	_jump("S_0001_Choice1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0001_14_2A");
	_dialog("S_0001_14_2A");
	_jump("",S_0002);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0001_15_2B");
	_dialog("S_0001_15_2B");
	_dialog("S_0001_16_2B",true);
	_choice("“We both need sated.”","S_0001_17_3A");
	_choice("“You thought I meant it?”","S_0001_27_3B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0001_17_3A");
	_dialog("S_0001_17_3A");
	_dialog("S_0001_18_3A");
	_dialog("S_0001_19_3A");
	_dialog("S_0001_20_3A");
	_dialog("S_0001_21_3A");
	_dialog("S_0001_21-1_3A");
	_dialog("S_0001_21-2_3A");
	_dialog("S_0001_21-3_3A");
	_dialog("S_0001_21-4_3A",true);
	_choice("“You don’t own me.”","S_0001_22_4A");
	_choice("“I marked you too. Or didn’t you notice?”","S_0001_24_4B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0001_22_4A");
	_dialog("S_0001_22_4A");
	_dialog("S_0001_23_4A");
	_jump("S_0001_26");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0001_24_4B");
	_dialog("S_0001_24_4B");
	_dialog("S_0001_25_4B");
	
_label("S_0001_26");
	_dialog("S_0001_26");
	_execute(0,char_add_char_opinion,NAME_ZEYD,obj_control.var_map[? VAR_PLAYER],1);
	_jump("",S_0002);

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0001_27_3B");
	_dialog("S_0001_27_3B");
	_jump("",S_0002);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////	
