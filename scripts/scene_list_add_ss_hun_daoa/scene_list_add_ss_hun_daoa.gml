///scene_list_add_ss_hun_daoa(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];

if ally_exists(NAME_ZEYD) && char_get_char_opinion(NAME_ZEYD,char_get_player()) >= 5
	ds_list_add(possible_list,"SS_HUN_DAOA_1_1");
if ally_exists(NAME_BALWAG) && ally_exists(NAME_ZEYD) && ally_exists(NAME_KUDI)
	ds_list_add(possible_list,"SS_HUN_DAOA_2_1");
if ally_exists(NAME_BALWAG) && ally_exists(NAME_ZEYD) && char_get_char_opinion(NAME_ZEYD,char_get_player()) >= 5
	ds_list_add(possible_list,"SS_HUN_DAOA_3_1");
if ally_exists(NAME_ZEYD)
	ds_list_add(possible_list,"SS_HUN_DAOA_4_1");
if ally_exists(NAME_BALWAG) && char_get_char_opinion(NAME_BALWAG,char_get_player()) >= 5
	ds_list_add(possible_list,"SS_HUN_DAOA_5_1");
	
return possible_list;

