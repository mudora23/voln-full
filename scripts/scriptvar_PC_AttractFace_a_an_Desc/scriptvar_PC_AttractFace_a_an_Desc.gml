///scriptvar_PC_AttractFace_a_an_Desc();
/// @description
var num = var_map_get(PC_AttractFace_N);
if num < -80 return choose("a hideous","a monstrous","a grotesque","a disgracefully ugly");
else if num < -60 return choose("an ugly","an unsightly","a repulsive","a beastly");
else if num < -40 return choose("a bent-looking","a coarse-featured","a freakish","a bizarre");
else if num < -20 return choose("a weird-looking","an odd-looking","a homely","an awkward-looking","an unusual");
else if num < 0 return choose("a plain","an angular","a severe-looking","a hard-featured");
else if num == 0 return choose("an average","an unremarkable");
else if num <= 10 return choose("a pleasant-looking","a well-proportioned");
else if num <= 20 return choose("a lovely","a cute","a nice");
else if num <= 40 return choose("a charming","a pretty","an attractive","a comely");
else if num <= 60 return choose("an excellent","an elegant","a wonderful","an adorable");
else if num <= 80 return choose("a beautiful","a glamorous","a gorgeous","a delightfully pretty");
else return choose("an exquisitely beautiful","a ravishing","a perfect","a superb");