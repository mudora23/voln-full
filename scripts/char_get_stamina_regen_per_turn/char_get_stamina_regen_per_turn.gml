///char_get_stamina_regen_per_turn(char_map);
/// @description
/// @param char_map
var char_map = argument[0];
return 10*char_get_vitality_penalty_ratio(argument0);