///game_time_get_month_name(month);
/// @description
/// @param month
if argument0 == 1 return "Årbûn";
if argument0 == 2 return "Dillånz";
if argument0 == 3 return "Ånzl";
if argument0 == 4 return "Nepri";
if argument0 == 5 return "Vådn";
if argument0 == 6 return "Håym";
if argument0 == 7 return "Hessuleyn";
if argument0 == 8 return "Belne";
if argument0 == 9 return "Vunnorum";
if argument0 == 10 return "Gûlum";
return "Unknown";