///scene_cg_list_change_cg(ID,cg_target);
/// @description  
/// @param ID
/// @param cg_target

with obj_control
{
	if sprite_exists(argument[1])
	{
		var cg_index_target_list = obj_control.var_map[? VAR_CG_LIST_INDEX_TARGET_NAME];
		var cg_index_list = obj_control.var_map[? VAR_CG_LIST_INDEX_NAME];
		var cg_id_list = obj_control.var_map[? VAR_CG_LIST_ID];
	
		if ds_list_find_index(cg_id_list,argument[0]) >= 0
		{
			var existing_index = ds_list_find_index(cg_id_list,argument[0]);
		
			cg_index_target_list[| existing_index] = sprite_get_name(argument[1]);
			//show_debug_message("cg changing from "+string(cg_index_list[| existing_index])+" to "+string(cg_index_target_list[| existing_index]));
	
		}
		
	}


}