///battle_process_kill_all_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
	
	var toru_cost = 0;

	var char_source = battle_get_current_action_char();

	var target_list = ds_list_create();
	
	if battle_get_current_action_char_owner() == OWNER_PLAYER || battle_get_current_action_char_owner() == OWNER_ALLY
	{
		var char_source_color_tag = TAG_COLOR_ALLY_NAME;
		var char_target_color_tag = TAG_COLOR_ENEMY_NAME;
		
		var char_list = var_map[? VAR_ENEMY_LIST];
		for(var i = 0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if battle_char_get_combat_capable(char) ds_list_add(target_list,char_list[| i]);
		}
	}
	else
	{
		var char_source_color_tag = TAG_COLOR_ENEMY_NAME;
		var char_target_color_tag = TAG_COLOR_ALLY_NAME;
	
		var char_list = var_map[? VAR_ALLY_LIST];
		for(var i = 0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if battle_char_get_combat_capable(char) ds_list_add(target_list,char_list[| i]);
		}
		var char = var_map[? VAR_PLAYER];
		if battle_char_get_combat_capable(char) ds_list_add(target_list,char_list[| i]);
	}

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" casts",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" KILL ALL",TAG_FONT_N,TAG_COLOR_TORU));
	ds_list_add(battlelog_pieces_list,text_add_markup(".",TAG_FONT_N,TAG_COLOR_NORMAL));
	
	for(var i = 0; i < ds_list_size(target_list);i++)
	{
		var char_target = target_list[| i];

		// performing
		var dodge_chance = char_get_dodge_chance(char_target);
		var hit_chance = char_get_hit_chance(char_source);
	
		var real_cost = min(char_source[? GAME_CHAR_TORU],toru_cost)
		char_source[? GAME_CHAR_TORU] -= real_cost;
		//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_TORU);
	
		if percent_chance(100)
		{
			var damage = 10000000000;
			var real_damage = char_target[? GAME_CHAR_HEALTH];
			var armor = 0;
			
			script_execute_add(1,ds_map_replace_i,char_target,GAME_CHAR_HEALTH,0);
		
	
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP,1,1);
			
			ally_get_hit_port(char_target);
		
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" for",0));
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" "+string(round(real_damage)),1));
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" damage!",0));
		
			char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
			
			var debug_message = "KILL ALL is performed,";
			debug_message+= " source: ";
			debug_message+= string(damage);
			debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
			debug_message+= " target: ";
			debug_message+= string(armor);
			debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";

		}
		else
		{
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,1,0.7);
			//ds_list_add(battlelog_pieces_list,battlelog_tag(" and misses!",0));
		}


	}



	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	
	voln_play_sfx(sfx_Charge9_dark);
	script_execute_add(2,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}