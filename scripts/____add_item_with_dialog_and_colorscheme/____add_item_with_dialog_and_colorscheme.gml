///____add_item_with_dialog_and_colorscheme(name,amount);
/// @description  
/// @param name
/// @param amount

_colorscheme(COLORSCHEME_GETITEM);
_execute(0,inventory_add_item,argument0,argument1);
_dialog_item(argument0,argument1);
_colorscheme(COLORSCHEME_NORMAL);