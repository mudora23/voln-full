///ally_get_hit_port(char_map);
/// @description
/// @param char_map
with obj_control
{
	var char_map = argument[0];
    var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
    var ally_hit_index = ds_list_find_index(ally_list,char_map);
    if ally_hit_index>=0
    {
        for(var i = 0; i < instance_number(obj_ally);i++)
		{
			var inst = instance_find(obj_ally,i);
			if inst.ally_index = ally_hit_index
				inst.alter_image_alpha = 3;
		
		}
    }
}
