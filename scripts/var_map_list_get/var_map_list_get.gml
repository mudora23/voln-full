///var_map_list_get(GAME_MAP_KEY,GAME_MAP_LIST_INDEX);
/// @description
/// @param GAME_MAP_KEY
/// @param GAME_MAP_LIST_INDEX
var var_key = argument[0];
var var_key_index = argument[1];

if ds_map_exists(obj_control.var_map,var_key) && !is_undefined(ds_list_find_value(ds_map_find_value(obj_control.var_map,var_key),var_key_index))
	return ds_list_find_value(ds_map_find_value(obj_control.var_map,var_key),var_key_index);
else
{
	////show_debug_message("ERROR: var_map_var_map_get(GAME_MAP_KEY,GAME_MAP_MAP_KEY) - key not exists! key1: "+string(var_key)+" index1: "+string(var_key_index));
	return NOT_EXISTS;
}