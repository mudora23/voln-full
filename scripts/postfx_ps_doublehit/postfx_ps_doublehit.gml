///postfx_ps_doublehit(x,y);
/// @description -
/// @param x
/// @param y

if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);
part_emitter_region(global.ps, global.ps_doublehit1, xp+116, xp+118, yp-113, yp-111, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_doublehit1, global.ps_doublehit1, 1);

//scene_script_execute_add(0.2,ps_doublehit_2,xp,yp);
//scene_script_execute_add(0.1,ps_doublehit_2,argument0,argument1);

