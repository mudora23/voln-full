///draw_sprite_image_in_area(spr,image_index,x,y,w,h);
/// @description
/// @param spr
/// @param image_index
/// @param x
/// @param y
/// @param w
/// @param h

if is_string(argument0)
{
	if sprite_exists(asset_get_index(argument0))
		var thumbnail_spr = asset_get_index(argument0); 
	else if sprite_exists(asset_get_index(string_lower(argument0)))
		var thumbnail_spr = asset_get_index(string_lower(argument0));
	else
		var thumbnail_spr = spr_white;
}
else if sprite_exists(argument0)
	var thumbnail_spr = argument0;
else
	var thumbnail_spr = spr_white;
	
var thumbnail_spr_w = sprite_get_width(thumbnail_spr);
var thumbnail_spr_h = sprite_get_height(thumbnail_spr);

var thumbnail_x = argument2;
var thumbnail_y = argument3;
var thumbnail_w = argument4;
var thumbnail_h = argument5;

var thumbnail_wratio =  thumbnail_w / thumbnail_spr_w;
var thumbnail_hratio = thumbnail_h / thumbnail_spr_h;
var thumbnail_ratio = min(thumbnail_wratio,thumbnail_hratio);

draw_sprite_ext(thumbnail_spr,argument1,thumbnail_x+(thumbnail_w-(thumbnail_spr_w*thumbnail_ratio))/2,thumbnail_y+(thumbnail_h-(thumbnail_spr_h*thumbnail_ratio))/2,thumbnail_ratio,thumbnail_ratio,0,c_white,draw_get_alpha());
