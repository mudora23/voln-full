///scriptvar_TEMP_CHARID_NAME();
/// @description
var the_char_id = var_map_get(VAR_TEMP_CHARID);
var the_char = char_id_get_char(the_char_id);
if the_char != noone
	return the_char[? GAME_CHAR_DISPLAYNAME];
else
	return "";