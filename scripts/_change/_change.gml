///_change(ID,sprite_target);
/// @description  
/// @param ID
/// @param sprite_target

if scene_is_current()
{
	scene_sprite_list_change_sprite(argument0,argument1);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);