///center_the_ship();
if instance_exists(obj_mini_map) && instance_exists(obj_map_player)
{
	obj_mini_map.map_xoffset_target = obj_map_player.x*obj_mini_map.map_zoom_ratio - obj_mini_map.area_width/2;
	obj_mini_map.map_yoffset_target = obj_map_player.y*obj_mini_map.map_zoom_ratio - obj_mini_map.area_height/2;
}
else
{
	script_execute_add(0.1,center_the_ship);
}