///hex_to_array(string)
//takes a string of hex octets and returns a string
var s = argument0, i, b = array_create(0), s_l = string_length(s);

for (i = 1; i <= s_l; i += 2)
    b[array_length_1d(b)] = hex_to_dec(string_copy(s, i, 2));
    
return b;
