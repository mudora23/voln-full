///enemy_get_select_number();
/// @descirption - Return the number of enemies the player has had selected with
var number = 0;
var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];

for(var i = 0; i < ds_list_size(enemy_list);i++)
{
	var enemy = enemy_list[| i];
	if enemy[? GAME_CHAR_SELECTED] number++;
}
return number;