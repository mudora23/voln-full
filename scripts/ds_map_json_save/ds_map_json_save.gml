///ds_map_json_save(map,filename);
/// @description
/// @param map
/// @param filename
var str = json_encode(argument[0]);
var file = file_text_open_write(argument[1]);
file_text_write_string(file, str);
file_text_close(file);