///scriptvar_PC_Skin_Desc();
/// @description
var num = var_map_get(PC_Skin_N);
if num < 1 return choose("sleek","silky","petal-soft","perfectly smooth");
else if num < 2  return choose("smooth","soft","velvety");
else if num < 3  return choose("rough","battle-hardened","leathery");
else  return choose("scarred","patchy","batttle-tortured","callused");
