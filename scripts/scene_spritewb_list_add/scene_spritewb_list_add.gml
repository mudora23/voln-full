///scene_spritewb_list_add(ID,sprite_with_border,alpha,alpha_target,fade_speed,slide_speed,order);
/// @description  
/// @param ID
/// @param sprite_with_border
/// @param alpha
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
with obj_control
{
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	var sprite_index_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME];
	var sprite_index_target_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_TARGET_NAME];
	var sprite_x_list = obj_control.var_map[? VAR_SPRITE_LIST_X];
	var sprite_x_target_list = obj_control.var_map[? VAR_SPRITE_LIST_X_TARGET];
	var sprite_y_list = obj_control.var_map[? VAR_SPRITE_LIST_Y];
	var sprite_y_target_list = obj_control.var_map[? VAR_SPRITE_LIST_Y_TARGET];
	var sprite_alpha_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA];
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	var sprite_fade_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_FADE_SPEED];
	var sprite_slide_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_SLIDE_SPEED];
	var sprite_order_list = obj_control.var_map[? VAR_SPRITE_LIST_ORDER];
	var sprite_size_ratio_list = obj_control.var_map[? VAR_SPRITE_LIST_SIZE_RATIO];

	
	if ds_list_find_index(sprite_id_list,argument[0]) < 0
	{
		ds_list_add(sprite_id_list,argument[0]);
		if sprite_exists(argument[1])
		{
			ds_list_add(sprite_index_list,sprite_get_name(argument[1]));
			ds_list_add(sprite_index_target_list,sprite_get_name(argument[1]));
		}
		else
		{
			ds_list_add(sprite_index_list,"");
			ds_list_add(sprite_index_target_list,"");
		}
		
		var spr_w = sprite_get_width(argument[1]);
		var spr_h = sprite_get_height(argument[1]);
		var spr_max_w = 1042;
		var spr_max_h = 460;
		var spr_max_w_ratio = min(1,spr_max_w / spr_w);
		var spr_max_h_ratio = min(1,spr_max_h / spr_h);

		var spr_real_ratio = min(spr_max_w_ratio,spr_max_h_ratio);
		var spr_real_w = spr_w * spr_real_ratio;
		var spr_real_h = spr_h * spr_real_ratio;
		var spr_real_x = room_width / 2 - spr_real_w / 2;
		var spr_real_x_target = spr_real_x;
		var spr_real_y = -spr_real_h - 50;
		var spr_real_y_target = (spr_max_h-spr_real_h)/2 + 20;

		ds_list_add(sprite_x_list,spr_real_x);
		ds_list_add(sprite_x_target_list,spr_real_x_target);
		ds_list_add(sprite_y_list,spr_real_y);
		ds_list_add(sprite_y_target_list,spr_real_y_target);
		ds_list_add(sprite_alpha_list,argument[2]);
		ds_list_add(sprite_alpha_target_list,argument[3]);
		ds_list_add(sprite_fade_speed_list,argument[4]);
		ds_list_add(sprite_slide_speed_list,argument[5]);
		ds_list_add(sprite_order_list,argument[6]);
		ds_list_add(sprite_size_ratio_list,spr_real_ratio);

	}
	else
	{
		/*scene_sprite_list_change(argument[0],argument[1],argument[3],argument[5],argument[7],argument[8],argument[9],argument[10],argument[11]);*/
	}

}