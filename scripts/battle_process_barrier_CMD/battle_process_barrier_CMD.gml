///battle_process_barrier_CMD();
/// @description

with obj_control
{
	scene_choices_list_clear();
	var char_source = battle_get_current_action_char();

	if char_get_skill_level(char_source,SKILL_BARRIER) >= 3
		var turns = 5;
	else if char_get_skill_level(char_source,SKILL_BARRIER) >= 2
		var turns = 4;
	else
		var turns = 3;
		

	skill_spend_cost(char_source,SKILL_BARRIER,char_get_skill_level(char_source,SKILL_BARRIER));
	
	var list_of_characters = battle_char_create_same_side_list(char_source);
	

	battle_char_list_buff_apply(list_of_characters,BUFF_PHY_ARMOR_UP,char_get_skill_level(battle_get_current_action_char(),SKILL_BARRIER),turns);
	battle_char_list_buff_apply(list_of_characters,BUFF_TORU_ARMOR_UP,char_get_skill_level(battle_get_current_action_char(),SKILL_BARRIER),turns);

	
	ds_list_destroy(list_of_characters);
		
	var char_source_color_tag = battle_char_get_color_tag(battle_get_current_action_char_owner());
			
	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" imbues the team",TAG_FONT_N,TAG_COLOR_HEAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" with might!",TAG_FONT_N,TAG_COLOR_NORMAL));	


	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add_unique(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	
	voln_play_sfx(sfx_Buff1);
	
	script_execute_add_unique(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);

}