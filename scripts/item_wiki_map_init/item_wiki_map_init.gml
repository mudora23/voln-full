///item_wiki_map_init();
/// @description
with obj_control
{
	item_wiki_map = ds_map_create();
	
	#macro ITEM_NAME "ITEM_NAME"
	#macro ITEM_DISPLAYNAME "ITEM_DISPLAYNAME"
	#macro ITEM_TYPE "ITEM_TYPE"
	#macro ITEM_DES "ITEM_DES"
	#macro ITEM_DES_PLAYER_USE "ITEM_DES_PLAYER_USE"
	#macro ITEM_DES_ALLY_USE "ITEM_DES_ALLY_USE"
	#macro ITEM_USE_IN_MENU "ITEM_USE_IN_MENU"
	#macro ITEM_USE_IN_COMBAT "ITEM_USE_IN_COMBAT"
	#macro ITEM_WEIGHT "ITEM_WEIGHT"
	#macro ITEM_PRICE "ITEM_PRICE"

/////////////////////////////////////////////////////////////////////////////
//                   Finished items
/////////////////////////////////////////////////////////////////////////////

	#macro ITEM_PH "ITEM_PH"
	var name = ITEM_PH; // cost: 2; resotres 35 HP, restores 10 Stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Item Name PH (Item type)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Item PH description.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"Item PH player use descriptions.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"Item PH ally use descriptions.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10);
		
	#macro ITEM_TEEG_SALTED "ITEM_TEEG_SALTED"
	var name = ITEM_TEEG_SALTED; // cost: 2; resotres 35 HP, restores 10 Stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"teeg (salted)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Briny and greasy, cheap and filling.  Everything a traveler needs.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);
		
	#macro ITEM_PORK_SALTED "ITEM_PORK_SALTED"
	var name = ITEM_PORK_SALTED; // cost: 3; resotres 50 HP; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"pork (salted)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"The flavor of pig.  You either like it… or you love it.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,300);

	#macro ITEM_BUN_STALE "ITEM_BUN_STALE"
	var name = ITEM_BUN_STALE; // cost: 1; resotres 20 HP, restores 10 Stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"bun (stale)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Whole-grain.  It’s a bit old, but it should help a lagging body.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);

	#macro ITEM_BEEF_JERKED "ITEM_BEEF_JERKED"
	var name = ITEM_BEEF_JERKED; // cost: 3; resotres 40 HP, restores 10 Stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"beef (jerked)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Hardy and delicious sun-dried meat.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,300);

	#macro ITEM_BUN_FRESH "ITEM_BUN_FRESH"
	var name = ITEM_BUN_FRESH; // cost: 2; resotres 25 HP, restores 25 Stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"bun (fresh)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Straight from the bakery, fresh and soft.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);

	#macro ITEM_VEJ_LEATHER "ITEM_VEJ_LEATHER"
	var name = ITEM_VEJ_LEATHER; // cost: 2; resotres 5 HP, restores 30 Stamina
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"vej leather");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A snack for the health-conscious adventurer.  Tastes okay.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);

	#macro ITEM_CAKE_KOMAR "ITEM_CAKE_KOMAR"
	var name = ITEM_CAKE_KOMAR; // cost: 10; restores Stamina by 10; 80% chance of +1 fat; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"cake (komår)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"An indulgent dessert, made with ‘the finest’ komår.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"The delicious cake is too good to resist.  So rich and sweet.  The flavor has your palette thoroughly satisfied.  Maybe you ate too much…");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);

	#macro ITEM_CAKE_FANCY "ITEM_CAKE_FANCY"
	var name = ITEM_CAKE_FANCY; // cost: 500; restores Stamina by 10; 60% chance +1 fat; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"cake (fancy)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A fancy cake for a fancy person.  It’s flavored with a secret blend of spices.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You eat the cake, delighting in the creamy frosting, perfectly sweetened.  The cake itself is moist enough to nearly melt in your mouth.  What an indulgence.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,1000);
		
	#macro ITEM_PIE_LEMMUS "ITEM_PIE_LEMMUS"
	var name = ITEM_PIE_LEMMUS; // cost: 15; restores Stamina by 10; 40% chance of +1 fat; Nirholt, Yim Trek, Consession area
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"pie (lemmus)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Sweet and sour lemmus pie.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"The tangy pie tastes divine.  You eat the whole thing yourself.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,150);
		
	#macro ITEM_PIE_FANCY "ITEM_PIE_FANCY"
	var name = ITEM_PIE_FANCY; // cost: 500; heals 10 HP, restores 35 Stamina, 25% chance of +1 fat
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"pie (fancy)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A fancy pie for a fancy person.  Smells like it’s stuffed with meat.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You feast on the pie, finishing the thing.  The meat inside is tender and flavored perfectly to your taste.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,1000);
		
	#macro ITEM_MILK_LUMKU "ITEM_MILK_LUMKU"
	var name = ITEM_MILK_LUMKU; // cost: 1; heals 10 HP, restores Stamina by 10; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"milk (lumku)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Tasty milk.  Useful for coating the stomach.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You drink the milk thirstily.  It has a lovely finish.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_MILK_PUMMDUS "ITEM_MILK_PUMMDUS"
	var name = ITEM_MILK_PUMMDUS; // cost: 300; increases Lust by 10, restores 50 Stamina
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"milk (Pummdu’s)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Thick, rich milk from a local source…");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You glug the milk hungrily.  It goes down smooth, leaving a creamy aftertaste.  Tastes like Pummdu, somehow.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,3000);
		
	#macro ITEM_POULTICE_AND_BANDAGE_KIT "poultice and bandage kit"
	var name = ITEM_POULTICE_AND_BANDAGE_KIT;

	#macro ITEM_SLEEP_TONIC "sleep tonic"
	var name = ITEM_SLEEP_TONIC;
	
	#macro ITEM_MESO_EY_TONIC_POOR "ITEM_MESO_EY_TONIC_POOR"
	var name = ITEM_MESO_EY_TONIC_POOR; // cost: 500; heals 200 HP; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"meso-ey tonic (poor)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Heals the body, but from the look of it, it won’t be easy to keep down.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You pop open a meso-ey tonic, sucking the frothing contents from the bottle, swallowing.  The potion sets to work instantly, alleviating pain.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a meso-ey tonic, healing a little HP.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,500);
		
	#macro ITEM_MESO_EY_TONIC_GOOD "ITEM_MESO_EY_TONIC_GOOD"
	var name = ITEM_MESO_EY_TONIC_GOOD; // cost: 2000; heals 1000 HP; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"meso-ey tonic (good)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A useful in-field treatment to restore needed strength.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You drain the little bottle, gulping a bitter mouthful.  You surge with strength, aches and bruises vanishing.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a meso-ey tonic, healing some HP.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_MESO_EY_TONIC_EXQUISITE "ITEM_MESO_EY_TONIC_EXQUISITE"
	var name = ITEM_MESO_EY_TONIC_EXQUISITE; // cost: 10000; heals ALL HP
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"meso-ey tonic (exquisite)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"The finest grade of miraculous medicine.  Watch your wounds close before your very eyes!");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"Drinking the frothing potion, you feel white energy pulse through you.  Your every fiber is mended perfectly!");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a meso-ey tonic, healing all HP!");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);

	#macro ITEM_VITALITY_TONIC_POOR "ITEM_VITALITY_TONIC_POOR"
	var name = ITEM_VITALITY_TONIC_POOR; // cost: 250; heals 50 Stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"vitality tonic (poor)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Try to drink it without smelling it.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You choke down the sour vitality tonic.  Yuck.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a vitality tonic, restoring a little vitality.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,500);

	#macro ITEM_VITALITY_TONIC_GOOD "ITEM_VITALITY_TONIC_GOOD"
	var name = ITEM_VITALITY_TONIC_GOOD; // cost: 1000; heals 150 Stamina; Nirholt, Lellurd’s
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"vitality tonic (good)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"This should fix the zen-time yawns!");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You swallow the tube’s contents, shuddering at the sour flavor.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a vitality tonic, restoring some vitality.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_VITALITY_TONIC_EXQUISITE "ITEM_VITALITY_TONIC_EXQUISITE"
	var name = ITEM_VITALITY_TONIC_EXQUISITE; // cost: 5000; heals all Stamina
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"vitality tonic (exquisite)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"The finest grade of drugs for reviving your verve.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You swallow the tube’s sour contents, instantly empowered.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a vitality tonic, restoring all vitality!");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);
		
	#macro ITEM_DEENSKIP_TONIC_POOR "ITEM_DEENSKIP_TONIC_POOR"
	var name = ITEM_DEENSKIP_TONIC_POOR; // cost: 1000; reduce lust by 25; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"deenskip tonic (poor)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Someone’s homemade attempt at replicating a deen’s cure for unclean desires.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You pop out the tube’s cork, and the smell assails you.  Willing yourself to drink the contents, you slurp down the stinky contents.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a deenskip tonic, reducing their Lust by 25.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,500);
		
	#macro ITEM_DEENSKIP_TONIC_GOOD "ITEM_DEENSKIP_TONIC_GOOD"
	var name = ITEM_DEENSKIP_TONIC_GOOD; // cost: 5000; reduce lust by 50; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"deenskip tonic (good)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Used by the deenskip for dealing with pesky natural urges.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You glug the tube’s contents, grimacing.  Smells like piss, tastes worse.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a deenskip tonic, reducing their Lust by 50.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_DEENSKIP_TONIC_EXQUISITE "ITEM_DEENSKIP_TONIC_EXQUISITE"
	var name = ITEM_DEENSKIP_TONIC_EXQUISITE; // cost: 15000; reduces Lust to 0, cures "Tempt"
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"deenskip tonic (exquisite)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A fine grade of appetite suppressant, used by the devoutest of deens.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You slurp down the tube of piss-scented wine.  Instantly, perspective is restored.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE," drinks a deenskip tonic, reducing their Lust to 0.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,true);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);
		
	#macro ITEM_KOMAR_GOOD "ITEM_KOMAR_GOOD"
	var name = ITEM_KOMAR_GOOD; // cost: 50; increase lust by 5; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"komår (good)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Bright gray with a spicy, floral scent.  You’re tempted to taste it.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"The flavor is subtely earthy, rich and warm like nothing else.  The experience feels deliciously intimate.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,500);
		
	#macro ITEM_KOMAR_EXQUISITE "ITEM_KOMAR_EXQUISITE"
	var name = ITEM_KOMAR_EXQUISITE; // cost: 1000; increase lust by 10; Nirholt, Lellurd’s
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"komår (exquisite)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_TONIC);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Gray and waxy, this candy looks unsavory, but the smell is amazing.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"This is truly fine komår, snapping off, almost instantly melting in your mouth.  The way the flavor lingers!  You only wish there was more to spare.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);
		
	#macro ITEM_CANDY_FRU "ITEM_CANDY_FRU"
	var name = ITEM_CANDY_FRU; // cost: 1; restores 5 stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"candy (fru)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A kid’s treat, but undeniably tasty.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"With each pellet you flick into your mouth, you’re rewarded with a burst of fun, fruity flavor.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_CANDY_JUDDOVIN "ITEM_CANDY_JUDDOVIN"
	var name = ITEM_CANDY_JUDDOVIN; // cost: 2; restores 5 stamina; Nirholt, Yim Trek, Consession area
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"candy (Juddovi’n)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"It looks like a cluster of candied nuts.  Smells good.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"It tastes as good as it looks, but tends to stick in the teeth.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);
		
	#macro ITEM_GUM_MINT "ITEM_GUM_MINT"
	var name = ITEM_GUM_MINT; // cost: 3; restores 5 stamina; Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"gum (mint)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Basic mint gum with ‘long-lasting’ flavor.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"It’s good gum, but the taste fades quickly.  After chewing it for a while, you spit out the flavorless wad.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,300);
		
	#macro ITEM_GUM_FANCY "ITEM_GUM_FANCY"
	var name = ITEM_GUM_FANCY; // cost: 25; restores 5 stamina; Nirholt, Lellurd’s
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"gum (fancy)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"An expensive treat for those mindful of oral hygene.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"It tastes like regular gum to you, maybe a bit softer.  You chew it till the flavor fades.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,250);
		
	#macro ITEM_MUSK_EXTRACT_ELF "ITEM_MUSK_EXTRACT_ELF"
	var name = ITEM_MUSK_EXTRACT_ELF; // cost: 8000; increases Lust by 25, 50% chance +0.1 testicle size; Nirholt, Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"musk extract (elf)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"What a stink!  Why do you have this?");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You can’t believe you’re doing this as you swallow mouthful after think mouthful.  The smell is so strong it burns your nose, nearly making you gag.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,80000);
		
	#macro ITEM_MUSK_EXTRACT_DUSKIE "ITEM_MUSK_EXTRACT_DUSKIE"
	var name = ITEM_MUSK_EXTRACT_DUSKIE; // cost: 10000; increases Lust by 5; 20% chance +0.1 peen length, 20% chance +0.1 peen girth
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"musk extract (Duskie)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Smells like a Duskie’s body.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You choke down the oily fluid, shuddering at the lingering bitterness of it.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100000);
		
	#macro ITEM_SPRUM_DUSKIE "ITEM_SPRUM_DUSKIE"
	var name = ITEM_SPRUM_DUSKIE; // cost: 1000; increases Lust by 25; 10% chance +0.1 testicle size, 40% chance +0.1 spurm qauntity
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"sprum (Duskie)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Some of the yellowish glop Duskies shoot so copiously.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"Disturbingly enough, the flavor isn’t so bad.  Kinda mild and milky.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);
		
	#macro ITEM_SPRUM_ELF "ITEM_SPRUM_ELF"
	var name = ITEM_SPRUM_ELF;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"sprum (Elf)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);
		
	#macro ITEM_SPRUM_GREEN_ORK "ITEM_SPRUM_GREEN_ORK"
	var name = ITEM_SPRUM_GREEN_ORK;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"sprum (Green ork)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,10000);

	#macro ITEM_SPRUM_SELF "ITEM_SPRUM_SELF"
	var name = ITEM_SPRUM_SELF; // cost: 50000; increases Lust by 5; Nirholt, Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"sprum (self)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Your own seed.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You proceed to drink your own sprum, judging the flavor and freshness to be adequate.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,500000);
		
	#macro ITEM_GURN_ELF "ITEM_GURN_ELF"
	var name = ITEM_GURN_ELF;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"gurn (Elf)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_GURN_GREEN_ORK "ITEM_GURN_GREEN_ORK"
	var name = ITEM_GURN_GREEN_ORK;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"gurn (Green ork)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
		
	#macro ITEM_REDBERRIES_JARRED "ITEM_REDBERRIES_JARRED"
	var name = ITEM_REDBERRIES_JARRED; // cost: 500; dereases Lust by 5; Nirholt, Gold Silt
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"redberries (jarred)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A collection of ripened redberries just waiting to be eaten.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"These little frus are crunchier than they look, tarter too.  The bitter aftertaste doesn’t help.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,5000);
		
	#macro ITEM_MUSHROOM_KORKORT "ITEM_MUSHROOM_KORKORT"
	var name = ITEM_MUSHROOM_KORKORT; // cost: ?; 10% chance of PC_height +0.1, 80% chance of Toru -5, 10% chance of PC_weight -0.1
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Korkort");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A luminescent, teal mushroom.  Smells sweet.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"You eat the little mushroom, and it tastes fine, until several chews in, when it starts getting really spicy.  You force yourself to swallow it anyway.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_MUSHROOM_LIRROT "ITEM_MUSHROOM_LIRROT"
	var name = ITEM_MUSHROOM_LIRROT; // cost: ?; 40% chance of PC_peenSize_L +0.5, 20% chance of PC_peenSize_G +0.2, 10% chance of lose all Vitality, 30% chance of -60% HP
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Lirrot");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A square-topped mushroom, colored purple-black and reeking of rot.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"This mushroom tastes musky and hardy with a nutty finish.  Altogether, a pleasant food for those who can suffer the smell.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_MUSHROOM_OLKE "ITEM_MUSHROOM_OLKE"
	var name = ITEM_MUSHROOM_OLKE; // cost: ?; 90% chance of PC_Nads_size +0.1, 10% chance of PC_Nads_size +0.2
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Olke");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A blue, ball-shaped mushroom.  Smells astringent and salty.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"Much to your displeasure the thing bursts between your teeth.  You managed to gulp the gooey contents.  They have a pungent floral taste.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_LEWD_BANDIT_COSTUME "ITEM_LEWD_BANDIT_COSTUME"
	var name = ITEM_LEWD_BANDIT_COSTUME; // cost: 
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"lewd bandit costume");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A slutty costume made in the fashion of bandits’ clothes.  They’re tailored for a small frame.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_A_CHAIN_LEAD "ITEM_A_CHAIN_LEAD"
	var name = ITEM_A_CHAIN_LEAD; // cost: 
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"a chain lead");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,3);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEM_SHACKLES "ITEM_SHACKLES"
	var name = ITEM_SHACKLES; // cost: 
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"shackles");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);	
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_CF1 "ITEM_CF1"
	var name = ITEM_CF1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"tleen");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"This is a bumpy, leather-skinned fru with a teal sheen and an overwhelmingly sweet smell.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_CB1 "ITEM_CB1"
	var name = ITEM_CB1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"grimbu berie");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"This fru has sticky, teal fur on it.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_CM1 "ITEM_CM1"
	var name = ITEM_CM1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"tira nån");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A beautiful, pale bluish-green mushroom of a miniature size.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_MB1 "ITEM_MB1"
	var name = ITEM_MB1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"wort berie");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"It looks like a berry made of flesh, but it feels like the skin of a plant.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_MM1 "ITEM_MM1"
	var name = ITEM_MM1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Palumorm");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A translucent, pink mushroom with an unusual luster.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_MM2 "ITEM_MM2"
	var name = ITEM_MM2; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Beef Sega");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"It has the color and smell of meat, but the little stem on the bottom tells you it’s a mushroom.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_YS1 "ITEM_YS1"
	var name = ITEM_YS1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Yenno");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A translucent, yellow globe filled with candy-colored seeds.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_YL1 "ITEM_YL1"
	var name = ITEM_YL1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Sun Fuzz");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Brilliant yellow leaves of a tiny size, soft as fur, with the flavor of sweet rum.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);
		
	#macro ITEM_YM1 "ITEM_YM1"
	var name = ITEM_YM1; // berries
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Ji Jiru");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Nut-sized, yellow mushrooms with a spiral shape and a long, green root.");
		ds_map_add(item_wiki_map[? name],ITEM_DES_PLAYER_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_DES_ALLY_USE,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,100);

/////////////////////////////////////////////////////////////////////////////
//                   PH items
/////////////////////////////////////////////////////////////////////////////
	
	#macro ITEMTYPE_WEAPON "Weapons"
		#macro ITEM_SPEAR "ITEM_SPEAR"
		var name = ITEM_SPEAR;
		ds_map_add_map(item_wiki_map,name,ds_map_create());
			ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Spear");
			ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
			ds_map_add(item_wiki_map[? name],ITEM_DES,"");
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
			ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,10);
			ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
			
		#macro ITEM_SWORD "ITEM_SWORD"
		var name = ITEM_SWORD;
		ds_map_add_map(item_wiki_map,name,ds_map_create());
			ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Sword");
			ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
			ds_map_add(item_wiki_map[? name],ITEM_DES,"");
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
			ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,15);
			ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
	#macro ITEMTYPE_MATERIALS "Materials"
		#macro ITEM_ELF_GLIM "ITEM_ELF_GLIM"
		var name = ITEM_ELF_GLIM;
		ds_map_add_map(item_wiki_map,name,ds_map_create());
			ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Elf glim");
			ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
			ds_map_add(item_wiki_map[? name],ITEM_DES,"");
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
			ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
			ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);
		
		#macro ITEM_GREEN_NUT "ITEM_GREEN_NUT"
		var name = ITEM_GREEN_NUT;
		ds_map_add_map(item_wiki_map,name,ds_map_create());
			ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Green nut");
			ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
			ds_map_add(item_wiki_map[? name],ITEM_DES,"");
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
			ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
			ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.5);
			ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);

	#macro ITEMTYPE_FOOD "Food"
	
	#macro ITEMTYPE_TONIC "Tonic"
	
	#macro ITEMTYPE_CONSUMABLE "Consumable"
	
	#macro ITEMTYPE_PET "Pet"
	

		
	#macro ITEM_HOSTELRY_BIR "ITEM_HOSTELRY_BIR"
	var name = ITEM_HOSTELRY_BIR;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"hostelry bir");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);

	#macro ITEM_Bread "ITEM_Bread"
	var name = ITEM_Bread;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"bread");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"Whole-grain.  This should help a lagging body.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);
		
	#macro ITEM_Candy_FRU "ITEM_Candy_FRU"
	var name = ITEM_Candy_FRU;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"candy (fru)");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_FOOD);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A kid’s treat, but undeniably tasty.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,true);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.1);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,200);
		
	#macro ITEM_Salo_wurm "ITEM_Salo_wurm"
	var name = ITEM_Salo_wurm;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"salo wurm");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_CONSUMABLE);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"A type of grub enjoyed by yims.  Its clean and slimy with a sour scent, almost like a pickle.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0.2);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);

	#macro ITEM_Blu_Yim "ITEM_Blu_Yim"
	var name = ITEM_Blu_Yim;
	ds_map_add_map(item_wiki_map,name,ds_map_create());
		ds_map_add(item_wiki_map[? name],ITEM_DISPLAYNAME,"Blu Yim");
		ds_map_add(item_wiki_map[? name],ITEM_TYPE,ITEMTYPE_PET);
		ds_map_add(item_wiki_map[? name],ITEM_DES,"One of the hardier types of yim, said to specialize in climbing steep terrain.");
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_MENU,false);
		ds_map_add(item_wiki_map[? name],ITEM_USE_IN_COMBAT,false);
		ds_map_add(item_wiki_map[? name],ITEM_WEIGHT,0);
		ds_map_add(item_wiki_map[? name],ITEM_PRICE,2000);

}
