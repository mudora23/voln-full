///TS_Love_Advice();
/// @description

_label("start");

_label("TS_Love_Advice");
	_show("Pummdu",spr_char_Pum_hh_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("TS_Love_Advice",true);
	_choice("“"+string(char_get_info(char_get_player(),GAME_CHAR_DISPLAYNAME))+", at your service, deym.”","TS_Love_Advice_Intro1");
	_choice("“Tell me your name first.”","TS_Love_Advice_Intro2");
	_choice("“Back away from the wag-swag, jill!”","TS_Love_Advice_Intro3");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Intro1");
	_dialog("TS_Love_Advice_Intro1");
	_dialog("TS_Love_Advice_Intro1_2");
	_show("Pummdu",spr_char_Pum_hh_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("TS_Love_Advice_Intro1_3");
	_dialog("TS_Love_Advice_Intro1_4");
	_jump("TS_Love_Advice_Intro1_5");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Intro2");
	_dialog("TS_Love_Advice_Intro2");
	_dialog("TS_Love_Advice_Intro2_2");
	_dialog("TS_Love_Advice_Intro2_3");
	_dialog("TS_Love_Advice_Intro2_4");
	_dialog("TS_Love_Advice_Intro2_5");
	_jump("TS_Love_Advice_Intro1_5");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Intro3");
	_dialog("TS_Love_Advice_Intro3");
	_dialog("TS_Love_Advice_Intro3_2");
	_dialog("TS_Love_Advice_Intro3_3");
	_dialog("TS_Love_Advice_Intro3_4");
	_dialog("TS_Love_Advice_Intro3_5");
	_jump("TS_Love_Advice_Intro1_5");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


_label("TS_Love_Advice_Intro1_5");
	_dialog("TS_Love_Advice_Intro1_5");
	_dialog("TS_Love_Advice_Intro1_6",true);
	_choice("Dodge her","TS_Love_Advice_Milk1");
	_choice("Let her catch you","TS_Love_Advice_Milk2");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Milk1");
	_dialog("TS_Love_Advice_Milk1");
	_dialog("TS_Love_Advice_Milk1_2");
	_dialog("TS_Love_Advice_Milk1_3");
	_jump("TS_Love_Advice_2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_Milk2");
	_dialog("TS_Love_Advice_Milk2");
	_dialog("TS_Love_Advice_Milk2_2");
	_dialog("TS_Love_Advice_Milk2_3");
	_dialog("TS_Love_Advice_Milk2_4");
	_dialog("TS_Love_Advice_Milk2_5");
	_dialog("TS_Love_Advice_Milk2_6");
	_dialog("TS_Love_Advice_Milk2_7");
	_jump("TS_Love_Advice_2");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_2");
	_dialog("TS_Love_Advice_2");
	_dialog("TS_Love_Advice_3",true);
	_choice("“Okay, try me.”","TS_Love_Advice_3_Help1");
	_choice("“Please never bother me again.”","TS_Love_Advice_3_Help2");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Help1");
	_dialog("TS_Love_Advice_3_Help1",true);
	_choice("“Pay a boy with sweet words and a gift or two, then go in for the prize.  Works every time.”","TS_Love_Advice_3_Advice1");
	_choice("“You should be of assistance to him whenever you can.  After a while, see if he’s interested.”","TS_Love_Advice_3_Advice2");
	_choice("“Make friends with him first.  Kindness, patience, and love will surely find you someone nice.”","TS_Love_Advice_3_Advice3");
	_choice("“Grab a boy by the orbs.  They love that!”","TS_Love_Advice_3_Advice4");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice1");
	_char_add_opinion(NAME_PUMMDU,3);
	_dialog("TS_Love_Advice_3_Advice1");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice2");
	_char_add_opinion(NAME_PUMMDU,5);
	_dialog("TS_Love_Advice_3_Advice2");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice3");
	_char_add_opinion(NAME_PUMMDU,4);
	_dialog("TS_Love_Advice_3_Advice3");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice4");
	_char_add_opinion(NAME_PUMMDU,1);
	_dialog("TS_Love_Advice_3_Advice4");
	_jump("TS_Love_Advice_3_Advice_End");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Advice_End");
	_dialog("TS_Love_Advice_3_Advice_End");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice_3_Advice_End_2");
	_jump("Selectedsceneafter","CS_extra_TS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice_3_Help2");
	_dialog("TS_Love_Advice_3_Help2");
	_dialog("TS_Love_Advice_3_Help2_2");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice_3_Help2_3");
	_jump("Selectedsceneafter","CS_extra_TS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2");
	_dialog("TS_Love_Advice2");
	_show("Pummdu",spr_char_Pum_hh_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("TS_Love_Advice2_2",true);
	_choice("“You want to talk?”","TS_Love_Advice2_2_A");
	_choice("“I don’t have the time, thank you.”","TS_Love_Advice2_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2_2_A");
	_dialog("TS_Love_Advice2_2_A",true);
	_choice("Invite","TS_Love_Advice2_2_A_A");
	_choice("Dismiss","TS_Love_Advice2_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("TS_Love_Advice2_2_A_A");	
	_dialog("TS_Love_Advice2_2_A_A");
	_dialog("TS_Love_Advice2_2_A_A_2",true);
	_choice("“I’d like that very much!”","TS_Love_Advice2_2_A_A_2_A");
	_choice("“No, thanks.”","TS_Love_Advice2_2_A_A_2_B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice2_2_A_A_2_A");
	_dialog("TS_Love_Advice2_2_A_A_2_A");
	_dialog("TS_Love_Advice2_2_A_A_2_A_SEX");
	_jump("TS_Love_Advice2_2_B");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice2_2_A_A_2_B");
	_dialog("TS_Love_Advice2_2_A_A_2_B");
	_dialog("TS_Love_Advice2_2_A_A_2_B_2");
	_dialog("TS_Love_Advice2_2_A_A_2_B_3");
	_dialog("TS_Love_Advice2_2_A_A_2_B_4");
	_dialog("TS_Love_Advice2_2_A_A_2_B_5");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice2_2_A_A_2_B_6");
	_jump("Selectedsceneafter","CS_extra_TS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("TS_Love_Advice2_2_B");
	_dialog("TS_Love_Advice2_2_B");
	_hide("Pummdu",FADE_NORMAL_SEC);
	_dialog("TS_Love_Advice2_2_B_2");
	_jump("Selectedsceneafter","CS_extra_TS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////