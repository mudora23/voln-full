///draw_surface_in_visible_area(surface,x,y,xscale,yscale,color,alpha,area_x,area_y,area_w,area_h);
/// @description
/// @param surface
/// @param x
/// @param y
/// @param xscale
/// @param yscale
/// @param color
/// @param alpha
/// @param area_x
/// @param area_y
/// @param area_w
/// @param area_h

var surface = argument0;
if surface_exists(surface)
{
	var surface_x = argument1;
	var surface_y = argument2;
	var surface_xscale = argument3;
	var surface_yscale = argument4
	var surface_w = surface_get_width(surface);
	var surface_h = surface_get_height(surface);
	var surface_wscale = surface_w*surface_xscale;
	var surface_hscale = surface_h*surface_yscale;
	var surface_color = argument5;
	var alpha = argument6;
	var area_x = argument7;
	var area_y = argument8;
	var area_w = argument9;
	var area_h = argument10;


	if rectangle_in_rectangle(surface_x, surface_y, surface_x+surface_wscale, surface_y+surface_hscale, area_x, area_y, area_x+area_w, area_y+area_h)
	{

	    var draw_left = clamp((area_x - surface_x)/surface_xscale, 0, surface_w);
	    var draw_top = clamp((area_y - surface_y)/surface_yscale, 0, surface_h);
	    var draw_width = /*surface_w - draw_left;*/min(surface_w - draw_left,max((area_x+area_w-surface_x)/surface_xscale,0),(area_w/surface_xscale)/*+draw_left*/);
	    var draw_height = /*surface_h - draw_top;*/min(surface_h - draw_top,max((area_y+area_h-surface_y)/surface_yscale,0),(area_h/surface_yscale)/*+draw_top*/);
	    var draw_xx = max(surface_x,area_x);
	    var draw_yy = max(surface_y,area_y);
	    //var draw_xx = area_x;
	    //var draw_yy = area_y;
	
		draw_surface_part_ext(surface,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,surface_xscale,surface_yscale,surface_color,alpha)
	}
}
