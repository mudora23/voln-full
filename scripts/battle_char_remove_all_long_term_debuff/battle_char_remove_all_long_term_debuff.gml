///battle_char_remove_all_long_term_debuff(char,hours);
/// @description
/// @param char
/// @param hours
var char_map = argument0;
var char_debuff_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_LIST];
var char_debuff_hours_list = char_map[? GAME_CHAR_LONG_TERM_DEBUFF_HOURS_LIST];
var char_debuff_hours = argument1;

for(var i = 0; i < ds_list_size(char_debuff_list);i++)
{
	char_debuff_hours_list[| i] = char_debuff_hours_list[| i]-char_debuff_hours;
	if char_debuff_hours_list[| i] <= 0
	{
		ds_list_delete(char_debuff_list,i);
		ds_list_delete(char_debuff_hours_list,i);
	}
}
