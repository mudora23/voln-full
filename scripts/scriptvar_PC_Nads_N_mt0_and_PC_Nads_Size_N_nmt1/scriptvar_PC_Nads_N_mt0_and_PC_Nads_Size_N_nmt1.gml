///scriptvar_PC_Nads_N_mt0_and_PC_Nads_Size_N_nmt1();
/// @description
return var_map_get(PC_Nads_N) > 0 && var_map_get(PC_Nads_Size_N) <= 1;
