///SL42_S_0007_Battle();
/// @description
with obj_control
{
    //scene_mode(GAME_MODE_BATTLE);
    //scene_clear_ext(false,true,true,true,true);
	//scene_dialog_set_update_surface();
	//var_map_set(VAR_MODE,VAR_MODE_BATTLE);
	
    /*var player_ally_total_power_level = 0;
    var player_ally_number = 0;
    var char = game_map[? GAME_PLAYER];
    player_ally_total_power_level += char[? GAME_CHAR_LEVEL];
    player_ally_number++;
    var char_list = game_map[? GAME_ALLY_LIST];
    for(var i = 0; i < ds_list_size(char_list);i++)
    {
        var char = char_list[| i];
        player_ally_total_power_level += char[? GAME_CHAR_LEVEL];
        player_ally_number++;
    }
    
    var player_ally_level_average = player_ally_total_power_level / player_ally_number;
	*/
	
	
	//enemy_add_char(NAME_GREEN_ORK_1,false,round_chance(player_ally_total_power_level/6));
	//enemy_add_char(NAME_GREEN_ORK_2,false,round_chance(player_ally_total_power_level/6));
	//enemy_add_char(NAME_GREEN_ORK_4,false,ceil(player_ally_total_power_level/5));
	//enemy_add_char(NAME_GREEN_ORK_3,false,round_chance(player_ally_total_power_level/6));
	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);
	var average_level = battle_char_list_get_average_level_ext(obj_control.var_map[? VAR_ALLY_LIST],char_get_player());
	
	
	
	
	if ally_get_number() == 4
	{
		enemy_add_char(choose(NAME_ELF_BANDITS_1,NAME_ELF_BANDITS_2,NAME_ELF_BANDITS_3),false,7);
		enemy_add_char(choose(NAME_ELF_BANDITS_1B,NAME_ELF_BANDITS_2B,NAME_ELF_BANDITS_3B),false,7);
		enemy_add_char(choose(NAME_ELF_BANDITS_1B,NAME_ELF_BANDITS_2B,NAME_ELF_BANDITS_3B),false,7);	
		enemy_add_char(choose(NAME_ELF_BANDITS_1,NAME_ELF_BANDITS_2,NAME_ELF_BANDITS_3),false,7);
	}
	else if ally_get_number() == 3
	{
		enemy_add_char(choose(NAME_ELF_BANDITS_1,NAME_ELF_BANDITS_2,NAME_ELF_BANDITS_3),false,6);
		enemy_add_char(choose(NAME_ELF_BANDITS_1B,NAME_ELF_BANDITS_2B,NAME_ELF_BANDITS_3B),false,7);
		enemy_add_char(choose(NAME_ELF_BANDITS_1B,NAME_ELF_BANDITS_2B,NAME_ELF_BANDITS_3B),false,7);	
		enemy_add_char(choose(NAME_ELF_BANDITS_1,NAME_ELF_BANDITS_2,NAME_ELF_BANDITS_3),false,6);
	}
	else
	{
		enemy_add_char(choose(NAME_ELF_BANDITS_1,NAME_ELF_BANDITS_2,NAME_ELF_BANDITS_3),false,6);
		enemy_add_char(choose(NAME_ELF_BANDITS_1B,NAME_ELF_BANDITS_2B,NAME_ELF_BANDITS_3B),false,6);
		enemy_add_char(choose(NAME_ELF_BANDITS_1B,NAME_ELF_BANDITS_2B,NAME_ELF_BANDITS_3B),false,6);	
		enemy_add_char(choose(NAME_ELF_BANDITS_1,NAME_ELF_BANDITS_2,NAME_ELF_BANDITS_3),false,6);	
	}

	
	
	var_map[? BATTLE_WIN_SCENE] = "SL42";
	var_map[? BATTLE_WIN_SCENE_LABEL] = "SL42_S_0007_Won";
	var_map[? BATTLE_LOSS_SCENE] = "SL42";
	var_map[? BATTLE_LOSS_SCENE_LABEL] = "SL42_S_0007_Loss";
	
	script_execute_add(0,battle_process_setup);

}	




































///scr_SL1_HUN_DAOA_INSIDE_MYST_READY_S_0005();
//scene_seen(scr_SL1_HUN_DAOA_INSIDE_MYST_READY);
//scene_clear_ext(false,false,true,true,true);
//char_set_char_opinion(NAME_KUDI,obj_control.game_map[? GAME_PLAYER],1);
//scene_GUI(GAME_GUI_N);
//scene_bg(spr_bg_Nirholt_HD_01_empty);

//var xx = sprite_position_get_x(spr_char_Ku_n_n,fa_right);
//var yy = y;
//scene_sprite_list_add("Ku",spr_char_Ku_n_n,room_width,xx,yy,yy,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,1);
//scene_sprite_list_change_x("Ku",room_width);
//scene_sprite_list_change_alpha("Ku",0);

	
//scene_dialog_set_name("ku");
//scene_dialog_set_name("zeyd");
//scene_dialog_set_name("Bålwåg");

//scene_dialog_add("SL1_HUN_DAOA_INSIDE_MYST_READY_9");
//scene_dialog_set_update_surface();
	
//scr_SL1_HUN_DAOA_INSIDE_Choice();
//scene_set_next_scene(scr_SL1_HUN_DAOA_INSIDE_MYST_READY_S_0005);

//if !scene_is_seen(scr_S_0005_13_A_2)
//scene_choice_add("“I’m ready.”",scr_SL1_HUN_DAOA_INSIDE_MYST_READY);
//scene_choice_add("Leave",scr_SL1_HUN_DAOA_INSIDE_MYST_LEAVE);
