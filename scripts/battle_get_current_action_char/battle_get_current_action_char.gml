///battle_get_current_action_char();
/// @description
if !ds_map_exists(obj_control.var_map,VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER)
	return noone;
else
	return obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER];
