///draw_sprite_in_area_fill(tile,x,y,w,h,color,alpha,halign,valign);
/// @description
/// @param tile
/// @param x
/// @param y
/// @param w
/// @param h
/// @param color
/// @param alpha
/// @param halign
/// @param valign

var tile = argument0;
var tile_w = sprite_get_width(tile);
var tile_h = sprite_get_height(tile);
var x0 = argument1;
var y0 = argument2;
var w = argument3;
var h = argument4;
var color = argument5;
var alpha = argument6;
var halign = argument7;
var valign = argument8;

if halign == fa_left var x_start = x0;
else if halign == fa_right var x_start = x0 + w - (ceil(w / tile_w)*tile_w);
else var x_start = x0 + w/2 - (ceil(w/2 / tile_w)*tile_w);

if valign == fa_top var y_start = y0;
else if valign == fa_bottom var y_start = y0 + h - (ceil(h / tile_h)*tile_h);
else var y_start = y0 + h/2 - (ceil(h/2 / tile_h)*tile_h);


for(var xx = x_start; xx < x0 + w; xx += tile_w)
{
	for(var yy = y_start; yy < y0 + h; yy += tile_h)
	{
		if xx < x0
		{
			var draw_left = x0 - xx;
			var draw_width = tile_w - draw_left+1;
			var draw_xx = x0;
		}
		else if xx > x0 + w - tile_w
		{
			var draw_left = 0;
			var draw_width = x0 + w - xx+1;
			var draw_xx = xx;
		}
		else
		{
			var draw_left = 0;
			var draw_width = tile_w+1;
			var draw_xx = xx;
		}
		if yy < y0
		{
			var draw_top = y0 - yy;
			var draw_height = tile_h - draw_top+1;
			var draw_yy = y0;
		}
		else if yy > y0 + h - tile_h
		{
			var draw_top = 0;
			var draw_height = y0 + h - yy+1;
			var draw_yy = yy;
		}
		else
		{
			var draw_top = 0;
			var draw_height = tile_h+1;
			var draw_yy = yy;
		}
		//draw_width = min(draw_width,w);
		//draw_height = min(draw_height,h);
		draw_sprite_part_ext(tile,0,draw_left,draw_top,draw_width,draw_height,draw_xx,draw_yy,1,1,color,alpha);
	}
}
