///scene_choiceCMD_add(name,extrascript,extrascript_arg0...extrascript_arg6);
/// @description
/// @param name
/// @param extrascript
/// @param extrascript_arg0...extrascript_arg6

if argument_count > 6
	scene_choice_add(argument[0],"",noone,"",true,true,true,"",argument[1],argument[2],argument[3],argument[4],argument[5],argument[6]);
else if argument_count > 5
	scene_choice_add(argument[0],"",noone,"",true,true,true,"",argument[1],argument[2],argument[3],argument[4],argument[5]);
else if argument_count > 4
	scene_choice_add(argument[0],"",noone,"",true,true,true,"",argument[1],argument[2],argument[3],argument[4]);
else if argument_count > 3
	scene_choice_add(argument[0],"",noone,"",true,true,true,"",argument[1],argument[2],argument[3]);
else if argument_count > 2
	scene_choice_add(argument[0],"",noone,"",true,true,true,"",argument[1],argument[2]);
else if argument_count > 1
	scene_choice_add(argument[0],"",noone,"",true,true,true,"",argument[1]);