///number_in_range_strictly_less(numToCheck,numA,numB);
/// @description
/// @param numToCheck
/// @param numA
/// @param numB
var minNum = min(argument1,argument2);
var maxNum = max(argument1,argument2);
return argument0 >= minNum && argument0 < maxNum;
