///CS_extra_SS();
/// @description

_label("start");

_label("Camphump");
	_ds_list_clear(var_map_get(VAR_TEMP_SCENE_LIST));

	_var_map_set(VAR_EXTRA_AFTER_SCENE,"CS_location");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"Camping");
	
	_execute(0,"scene_list_add_ss_camp_hump",var_map_get(VAR_TEMP_SCENE_LIST));
	_choices_types_from_scene_list(var_map_get(VAR_TEMP_SCENE_LIST));
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Campsleep");
	_ds_list_clear(var_map_get(VAR_TEMP_SCENE_LIST));

	_var_map_set(VAR_EXTRA_AFTER_SCENE,"CS_location");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"Camping");
	
	_execute(0,"scene_list_add_ss_camp_sleep",var_map_get(VAR_TEMP_SCENE_LIST));
	_choices_types_from_scene_list(var_map_get(VAR_TEMP_SCENE_LIST));
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_MEET_HUT");
	_ds_list_clear(var_map_get(VAR_TEMP_SCENE_LIST));

	_var_map_set(VAR_EXTRA_AFTER_SCENE,"SL1_MEET_HUT");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"SL1_MEET_HUT_OPEN");
	
	_execute(0,"scene_list_add_ss_meet_hut",var_map_get(VAR_TEMP_SCENE_LIST));
	_choices_types_from_scene_list(var_map_get(VAR_TEMP_SCENE_LIST));
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_HUN_DAOA");
	_ds_list_clear(var_map_get(VAR_TEMP_SCENE_LIST));

	_var_map_set(VAR_EXTRA_AFTER_SCENE,"SL1_HUN_DAOA");
	_var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"SL1_HUN_DAOA_ENTER_Choice");
	
	_execute(0,"scene_list_add_ss_hun_daoa",var_map_get(VAR_TEMP_SCENE_LIST));
	_choices_types_from_scene_list(var_map_get(VAR_TEMP_SCENE_LIST));
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedtype");
	_execute(0,"scene_set_from_type");
	_jump("Selectedscene");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedscene");
	_ds_list_clear(var_map_get(VAR_ENEMY_LIST));
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
		
	//_show_message(" VAR_EXTRA_AFTER_SCENE_LABEL: "+var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL)+" VAR_EXTRA_AFTER_SCENE: "+var_map_get(VAR_EXTRA_AFTER_SCENE))	
	_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectednoscene");
	_ds_list_clear(var_map_get(VAR_ENEMY_LIST));
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafter");
	_execute(0,player_ally_relieve_all);
	_label("Noscenestoselect");
_label("Selectedsceneafternorelieve");
	_jump(var_map_get(VAR_EXTRA_AFTER_SCENE_LABEL),var_map_get(VAR_EXTRA_AFTER_SCENE));

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
	// SS
	_border(1000); // add lines after this
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

_label("SS_MEET_HUT_2_1");
	_char_add_opinion(NAME_KUDI,1);
	_dialog("SS_MEET_HUT_2_1");
	_jump("Selectedsceneafter");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_MEET_HUT_4_1");
	_char_add_opinion(NAME_KUDI,1);
	_dialog("SS_MEET_HUT_4_1");
	_jump("Selectedsceneafter");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_CAMP_HUMP_1_1");
	_main_gui(VAR_GUI_MIN);
	_dialog("SS_CAMP_HUMP_1_1");
	_pause(2,0,true);
	_execute(0,"game_time_one_hour_passes");
	_execute(0,"game_time_one_hour_passes");
	_execute(0,"scene_recover_rest_2hours");
	_var_map_set(VAR_HOURS_SINCE_LAST_REST,0);
	_main_gui(VAR_GUI_NORMAL);
	_char_add_opinion(NAME_KUDI,1);
	_jump("Selectedsceneafter");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_CAMP_HUMP_2_1");
	_main_gui(VAR_GUI_MIN);
	_dialog("SS_CAMP_HUMP_2_1");
	_pause(2,0,true);
	_execute(0,"game_time_one_hour_passes");
	_execute(0,"game_time_one_hour_passes");
	_execute(0,"scene_recover_rest_2hours");
	_var_map_set(VAR_HOURS_SINCE_LAST_REST,0);
	_main_gui(VAR_GUI_NORMAL);
	_char_add_opinion(NAME_KUDI,1);
	_jump("Selectedsceneafter");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_CAMP_HUMP_8_1");
	_dialog("SS_CAMP_HUMP_8_1");
	_execute(0,player_ally_relieve_all_except,NAME_PLAYER);
	_execute(0,char_lust_grows_with_floating_text,char_get_player(),20);
	_jump("Selectedsceneafternorelieve");	
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_CAMP_SLEEP_1_1");
	_char_add_opinion(NAME_KUDI,round_chance(0.3));
	_dialog("SS_CAMP_SLEEP_1_1");
	_jump("Selectedsceneafter");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SS_HUN_DAOA_ZEYD_2_1");
	_char_add_opinion(NAME_BALWAG,1);
	_dialog("SS_HUN_DAOA_ZEYD_2_1");
	_jump("Selectedsceneafter");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////









