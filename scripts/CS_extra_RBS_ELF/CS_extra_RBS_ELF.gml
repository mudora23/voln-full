///CS_extra_RBS();
/// @description

_label("start");

_label("Selectedscene");
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	
	_jump("Selectedsceneafter","CS_extra_RBS");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedscene_jar_to_item");
	if scene_label_exists(var_map_get(VAR_TEMP_SCENE))
		_jump(var_map_get(VAR_TEMP_SCENE));
	else
		_dialog(var_map_get(VAR_TEMP_SCENE));
	_colorscheme(COLORSCHEME_GETITEM);
	_var_map_add_ext(VAR_JAR,-1,0,var_map_get(VAR_JAR));
	_dialog("        1 jar is removed from the inventory.");
	_execute(0,inventory_add_item,var_map_get(VAR_TEMP_ITEM),1);
	_dialog("        You receive "+string(item_get_displayname(var_map_get(VAR_TEMP_ITEM)))+"!");
	_colorscheme(COLORSCHEME_NORMAL);
	_jump("Selectedsceneafter","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("Selectednoscene");
	_jump("Selectednoscene","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafter");
	_jump("Selectedsceneafter","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Noscenestoselect");
	_jump("Noscenestoselect","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("Selectedsceneafternorelieve");
	_jump("Selectedsceneafternorelieve","CS_extra_RBS");
	
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
	// RBS
	_border(1000); // add lines after this
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_D_1_1");
	_dialog("RBS_ELF_F_D_1_1",true);
	_choice_selectedscene_RBS("Try to seduce her.","Selectedscene","RBS_ELF_F_D_1_Choice1_seduce");
	_choice_selectedscene_RBS("Spit at her “Fikk you knife-ear!“","Selectedscene","RBS_ELF_F_D_1_Choice1_insult");
	_block();	
	
	_border(10); // add lines after this

	_label("RBS_ELF_F_D_1_Choice1_seduce");
		_dialog("RBS_ELF_F_D_1_2A");
		_jump("RBS_ELF_F_D_1_Unfork1");
		
	_border(10); // add lines after this

	_label("RBS_ELF_F_D_1_Choice1_insult");
		_dialog("RBS_ELF_F_D_1_2B");
		_jump("RBS_ELF_F_D_1_Unfork1");
		
	_border(10); // add lines after this
	
	_label("RBS_ELF_F_D_1_Unfork1");
		_dialog("RBS_ELF_F_D_1_3");
		_dialog("RBS_ELF_F_D_1_4");
		_dialog("RBS_ELF_F_D_1_5");
		_dialog("RBS_ELF_F_D_1_6");
		_dialog("RBS_ELF_F_D_1_7");
		_gameover();
		_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_DV_1_1");
	_dialog("RBS_ELF_F_DV_1_1",true);
	_choice_selectedscene_RBS("“I’d rather fly solo”","Selectedscene","RBS_ELF_F_DV_1_2A");
	_choice_selectedscene_RBS("Smile and nod","Selectedscene","RBS_ELF_F_DV_1_2B_1");
	_block();	
	
	_border(10); // add lines after this

	_label("RBS_ELF_F_DV_1_2B_1");
		_dialog("RBS_ELF_F_DV_1_2B_1");
		_dialog("RBS_ELF_F_DV_1_2B_2");
		_dialog("RBS_ELF_F_DV_1_2B_3");
		_dialog("RBS_ELF_F_DV_1_2B_4");
		_dialog("RBS_ELF_F_DV_1_2B_5");
		_jump("Selectedsceneafter");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


_label("RBS_ELF_F_DV_2_1");
	_dialog("RBS_ELF_F_DV_2_1");
	_dialog("RBS_ELF_F_DV_2_2",true);
	_choice_selectedscene_RBS("Cum Inside her","Selectedscene","RBS_ELF_F_DV_2_3A");
	_choice_selectedscene_RBS("Cum over her tits","Selectedscene","RBS_ELF_F_DV_2_3B");
	_choice_selectedscene_RBS(" Cum on her face","Selectedscene","RBS_ELF_F_DV_2_3C");
	_block();	
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_DV_3_1");
	_dialog("RBS_ELF_F_DV_3_1");
	_dialog("RBS_ELF_F_DV_3_2");
	_dialog("RBS_ELF_F_DV_3_3",true);
	_choice_selectedscene_RBS("Do it in here.","Selectedscene","RBS_ELF_F_DV_3_4A");
	_choice_selectedscene_RBS("Switch to her kundər.","Selectedscene","RBS_ELF_F_DV_3_4B");
	_choice_selectedscene_RBS("Pull out and make a mess.","Selectedscene","RBS_ELF_F_DV_3_4C");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_DV_4_1");
	_dialog("RBS_ELF_F_DV_4_1");
	_dialog("RBS_ELF_F_DV_4_2");
	_dialog("RBS_ELF_F_DV_4_3");
	_dialog("RBS_ELF_F_DV_4_4");
	_dialog("RBS_ELF_F_DV_4_5");
	_jump("Selectedsceneafter");	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_DV_5_1");
	_dialog("RBS_ELF_F_DV_5_1");
	_dialog("RBS_ELF_F_DV_5_2");
	_dialog("RBS_ELF_F_DV_5_3");
	_dialog("RBS_ELF_F_DV_5_4");
	_jump("Selectedsceneafter");	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ELF_F_DV_6_1");
	_dialog("RBS_ELF_F_DV_6_1");
	_dialog("RBS_ELF_F_DV_6_2",true);
	_choice_selectedscene_RBS("Spend it on her face.","Selectedscene","RBS_ELF_F_DV_6_3A");
	_choice_selectedscene_RBS("Spend it in her mouth.","Selectedscene","RBS_ELF_F_DV_6_3B");
	_choice_selectedscene_RBS("Spend it in her throat.","Selectedscene","RBS_ELF_F_DV_6_3C");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_DV_8_1");
	_dialog("RBS_ELF_F_DV_8_1");
	_dialog("RBS_ELF_F_DV_8_2");
	_dialog("RBS_ELF_F_DV_8_3");
	_dialog("RBS_ELF_F_DV_8_4",true);
	_choice("Don’t use elf","Selectedsceneafter");
	_choice_selectedscene_RBS("Use elf","Selectedscene_jar_to_item","RBS_ELF_F_DV_8_5A");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_DV_9_1");
	_dialog("RBS_ELF_F_DV_9_1");
	_dialog("RBS_ELF_F_DV_8_2",true);
	_choice_selectedscene_RBS("Finger and fondle the elf","Selectedscene","RBS_ELF_F_DV_9_3A_1");
	_choice_selectedscene_RBS("Switch positions and get jerked off","Selectedscene","RBS_ELF_F_DV_9_3B");
	_block();
	
	_border(10); // add lines after this

	_label("RBS_ELF_F_DV_9_3A_1");
		_dialog("RBS_ELF_F_DV_9_3A_1");
		_dialog("RBS_ELF_F_DV_9_3A_2");
		_jump("Selectedsceneafter");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_SV_1_1");
	_dialog("RBS_ELF_F_SV_1_1");
	_dialog("RBS_ELF_F_SV_1_2");
	_dialog("RBS_ELF_F_SV_1_3");
	_dialog("RBS_ELF_F_SV_1_4");
	_jump("Selectedsceneafter");	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_F_SV_2_1");
	_dialog("RBS_ELF_F_SV_2_1");
	_dialog("RBS_ELF_F_SV_2_2");
	_dialog("RBS_ELF_F_SV_2_3");
	_jump("Selectedsceneafter");	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_M_D_1_1");
	_dialog("RBS_ELF_M_D_1_1");
	_dialog("RBS_ELF_M_D_1_2");
	_dialog("RBS_ELF_M_D_1_3");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_M_DV_1_1");
	_dialog("RBS_ELF_M_DV_1_1");
	_dialog("RBS_ELF_M_DV_1_2");
	_dialog("RBS_ELF_M_DV_1_3",true);
	_choice_selectedscene_RBS("Cum into his mouth","Selectedscene","RBS_ELF_M_DV_1_4A");
	_choice_selectedscene_RBS("Cum in his mouth","Selectedscene","RBS_ELF_M_DV_1_4B");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_M_DV_2_1");
	_dialog("RBS_ELF_M_DV_2_1");
	_dialog("RBS_ELF_M_DV_2_2");
	_dialog("RBS_ELF_M_DV_2_3",true);
	_choice_selectedscene_RBS("“Make him know his place”","Selectedscene","RBS_ELF_M_DV_2_4A");
	_choice_selectedscene_RBS("“Mercy is the power of the Strong”","Selectedscene","RBS_ELF_M_DV_2_4B");
	_block();
	
	_border(10); // add lines after this

	_label("RBS_ELF_M_DV_2_4B");
		_dialog("RBS_ELF_M_DV_2_4B");
		_dialog("RBS_ELF_M_DV_2_4B_2",true);
		_choice_selectedscene_RBS("Spend it inside.","Selectedscene","RBS_ELF_M_DV_2_4B_3A");
		_choice_selectedscene_RBS("Spend it outside.","Selectedscene","RBS_ELF_M_DV_2_4B_3B");
		_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ELF_M_DV_3_1");
	_dialog("RBS_ELF_M_DV_3_1");
	_dialog("RBS_ELF_M_DV_3_2");
	_dialog("RBS_ELF_M_DV_3_3",true);
	_choice_selectedscene_RBS("One more time.","Selectedscene","RBS_ELF_M_DV_3_4A");
	_choice("No, I’m done.","Selectedsceneafter");
	_block();
	
	_border(10); // add lines after this

	_label("RBS_ELF_M_DV_3_4A");
		_dialog("RBS_ELF_M_DV_3_4A");
		_dialog("RBS_ELF_M_DV_3_4A_2");
		_jump("Selectedsceneafter");
		_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_M_DV_4_1");
	_dialog("RBS_ELF_M_DV_4_1",true);
	_choice_selectedscene_RBS("Jerk him off yourself","Selectedscene","RBS_ELF_M_DV_4_2A");
	_choice_selectedscene_RBS("Fik him in the bum as he jerks himself","Selectedscene","RBS_ELF_M_DV_4_2B");
	_block();
	
	_border(10); // add lines after this

	_label("RBS_ELF_M_DV_4_2A");
		_dialog("RBS_ELF_M_DV_4_2A");
		_dialog("RBS_ELF_M_DV_4_2A_2");
		_jump("RBS_ELF_M_DV_4_3");
		_block();
		
	_border(10); // add lines after this
	
	_label("RBS_ELF_M_DV_4_2B");
		if percent_chance(70) _jump("RBS_ELF_M_DV_4_2B_1A");
		else _jump("RBS_ELF_M_DV_4_2B_1B");
	
	_border(10); // add lines after this
	
	_label("RBS_ELF_M_DV_4_2B_1A");
		_dialog("RBS_ELF_M_DV_4_2B_1A");
		_jump("RBS_ELF_M_DV_4_2B_2");
		
	_border(10); // add lines after this
	
	_label("RBS_ELF_M_DV_4_2B_1B");
		_dialog("RBS_ELF_M_DV_4_2B_1B");
		_jump("RBS_ELF_M_DV_4_2B_2");
		
	_border(10); // add lines after this
	
	_label("RBS_ELF_M_DV_4_2B_2");
		_dialog("RBS_ELF_M_DV_4_2B_2");
		_jump("RBS_ELF_M_DV_4_3");
		
	_border(10); // add lines after this
	
	_label("RBS_ELF_M_DV_4_3");
		_colorscheme(COLORSCHEME_GETITEM);
		_add_item(ITEM_SPRUM_ELF,1);
		_dialog("        You receive "+string(item_get_displayname(ITEM_SPRUM_ELF))+"!");
		_colorscheme(COLORSCHEME_NORMAL);
		_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_M_DV_6_1");
	_dialog("RBS_ELF_M_DV_6_1");
	_dialog("RBS_ELF_M_DV_6_2");
	_dialog("RBS_ELF_M_DV_6_3");
	_dialog("RBS_ELF_M_DV_6_4");
	_jump("Selectedsceneafter");	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FF_D_1_1");
	_dialog("RBS_ELF_FF_D_1_1",true);
	_choice_selectedscene_RBS("“Gurls, there’s enough of me to share!“","Selectedscene","RBS_ELF_FF_D_1_2_TALK");
	_choice_selectedscene_RBS("“Try to crawl away as they argue“","Selectedscene","RBS_ELF_FF_D_1_2_CRAWL");
	_block();
	
	_border(10); // add lines after this

	_label("RBS_ELF_FF_D_1_2_TALK");
		_dialog("RBS_ELF_FF_D_1_2A");
		_jump("RBS_ELF_FF_D_1_3");

	_border(10); // add lines after this

	_label("RBS_ELF_FF_D_1_2_CRAWL");
		_dialog("RBS_ELF_FF_D_1_2B");
		_jump("RBS_ELF_FF_D_1_3");

	_border(10); // add lines after this

	_label("RBS_ELF_FF_D_1_3");
		_dialog("RBS_ELF_FF_D_1_3");
		_dialog("RBS_ELF_FF_D_1_4");
		_dialog("RBS_ELF_FF_D_1_5");
		_dialog("RBS_ELF_FF_D_1_6");
		_dialog("RBS_ELF_FF_D_1_7");
		_dialog("RBS_ELF_FF_D_1_8");
		_gameover();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FF_DV_1_1");
	_dialog("RBS_ELF_FF_DV_1_1");
	_dialog("RBS_ELF_FF_DV_1_2");
	_dialog("RBS_ELF_FF_DV_1_3");
	_dialog("RBS_ELF_FF_DV_1_4");
	_dialog("RBS_ELF_FF_DV_1_5");
	_dialog("RBS_ELF_FF_DV_1_6");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FF_DV_2_1");
	_dialog("RBS_ELF_FF_DV_2_1");
	_dialog("RBS_ELF_FF_DV_2_2");
	_dialog("RBS_ELF_FF_DV_2_3");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FF_SV_1_1");
	_dialog("RBS_ELF_FF_SV_1_1");
	_dialog("RBS_ELF_FF_SV_1_2");
	_dialog("RBS_ELF_FF_SV_1_3");
	_dialog("RBS_ELF_FF_SV_1_4");
	_dialog("RBS_ELF_FF_SV_1_5");
	_dialog("RBS_ELF_FF_SV_1_6");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FF_SV_2_1");
	_dialog("RBS_ELF_FF_SV_2_1");
	_dialog("RBS_ELF_FF_SV_2_2");
	_dialog("RBS_ELF_FF_SV_2_3");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ELF_MF_DV_1_1");
	_dialog("RBS_ELF_MF_DV_1_1");
	_dialog("RBS_ELF_MF_DV_1_2");
	_dialog("RBS_ELF_MF_DV_1_3");
	_dialog("RBS_ELF_MF_DV_1_4",true);
	_choice_selectedscene_RBS("Group facial.","Selectedscene","RBS_ELF_MF_DV_1_5A");
	_choice_selectedscene_RBS("Attempt to impregnate.","Selectedscene","RBS_ELF_MF_DV_1_5B");
	_block();	
	
	_border(10); // add lines after this

	_label("RBS_ELF_MF_DV_1_5B");
		_dialog("RBS_ELF_MF_DV_1_5B");
		_execute(0,scene_impregnate_random);
		_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("RBS_ELF_MF_DV_2_1");
	_dialog("RBS_ELF_MF_DV_2_1",true);
	_choice("Spend it on her face.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MF_DV_2_2_1A");
	_choice("Spend it on his face.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MF_DV_2_3_1B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MM_DV_1_1");
	_dialog("RBS_ELF_MM_DV_1_1");
	_dialog("RBS_ELF_MM_DV_1_2");
	_dialog("RBS_ELF_MM_DV_1_3");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FFF_DV_1_1");
	_dialog("RBS_ELF_FFF_DV_1_1");
	_dialog("RBS_ELF_FFF_DV_1_2");
	_dialog("RBS_ELF_FFF_DV_1_3");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FFF_SV_1_1");
	_dialog("RBS_ELF_FFF_SV_1_1");
	_dialog("RBS_ELF_FFF_SV_1_2");
	_dialog("RBS_ELF_FFF_SV_1_3");
	_dialog("RBS_ELF_FFF_SV_1_4");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_FFF_SV_2_1");
	_dialog("RBS_ELF_FFF_SV_2_1");
	_dialog("RBS_ELF_FFF_SV_2_2");
	_dialog("RBS_ELF_FFF_SV_2_3");
	_dialog("RBS_ELF_FFF_SV_2_4");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////








	
_label("RBS_ELF_MFF_DV_1_1");
	_dialog("RBS_ELF_MFF_DV_1_1",true);
	_choice("Spend it in his mouth.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MFF_DV_1_2_1A");
	_choice("Spend it on his face.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MFF_DV_1_3_1B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MFF_DV_2_1");
	_dialog("RBS_ELF_MFF_DV_2_1",true);
	_choice("Spend it in her butt.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MFF_DV_2_2_1A");
	_choice("Spend it on the other gurl’s tads.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MFF_DV_2_3_1B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////	

_label("RBS_ELF_MFFF_DV_1_1");
	_dialog("RBS_ELF_MFFF_DV_1_1",true);
	_choice("Spend it on the two nearest faces.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MFFF_DV_1_2_1A");
	_choice("Spend it in her throat.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MFFF_DV_1_3_1B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMF_DV_1_1");
	_dialog("RBS_ELF_MMF_DV_1_1",true);
	_choice("Spend it in her throat.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMF_DV_1_2_1A");
	_choice("Attempt to impregnate her.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMF_DV_1_3_1B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMF_DV_1_3_1B");
	_dialog("RBS_ELF_MMF_DV_1_3_1B");
	_execute(0,scene_impregnate_random);
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMF_DV_2_1");
	_dialog("RBS_ELF_MMF_DV_2_1",true);
	_choice("Spend it on their faces.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMF_DV_2_2_1A");
	_choice("Attempt to impregnate her.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMF_DV_2_3_1B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMF_DV_2_3_1B");
	_dialog("RBS_ELF_MMF_DV_2_3_1B");
	_execute(0,scene_impregnate_random);
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMFF_DV_2_1");
	_dialog("RBS_ELF_MMFF_DV_2_1",true);
	_choice("Spend it on her face.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMFF_DV_2_2_1A");
	_choice("Push deeper this time, and spend it in her throat.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMFF_DV_2_3_1B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMM_DV_1_1");
	_dialog("RBS_ELF_MMM_DV_1_1");
	_dialog("RBS_ELF_MMM_DV_1_2");
	_dialog("RBS_ELF_MMM_DV_1_3",true);
	_choice_selectedscene_RBS("Cum in the Elf’s Snug","Selectedscene","RBS_ELF_MMM_DV_1_4A");
	_choice_selectedscene_RBS("Burst over a trio of Elfin ass","Selectedscene","RBS_ELF_MMM_DV_1_4B");
	_choice_selectedscene_RBS("Jizz across their faces","Selectedscene","RBS_ELF_MMM_DV_1_4C");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMM_DV_2_1");
	_dialog("RBS_ELF_MMM_DV_2_1");
	_dialog("RBS_ELF_MMM_DV_2_2");
	_dialog("RBS_ELF_MMM_DV_2_3");
	_dialog("RBS_ELF_MMM_DV_2_4");
	_dialog("RBS_ELF_MMM_DV_2_5");
	_dialog("RBS_ELF_MMM_DV_2_6");
	_dialog("RBS_ELF_MMM_DV_2_7");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMM_SV_1_1");
	_dialog("RBS_ELF_MMM_SV_1_1");
	_dialog("RBS_ELF_MMM_SV_1_2");
	_dialog("RBS_ELF_MMM_SV_1_3");
	_dialog("RBS_ELF_MMM_SV_1_4");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////













_label("RBS_ELF_MMMF_DV_1_1");
	_dialog("RBS_ELF_MMMF_DV_1_1",true);
	_choice("Spend it in his butt.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMMF_DV_1_2_1A");
	_choice("Pull out, and attempt to impregnate the elf-gurl.","Selectedscene","","",true,true,true,"",var_map_set,VAR_TEMP_SCENE,"RBS_ELF_MMMF_DV_1_3_1B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////



_label("RBS_ELF_MMMF_DV_1_3_1B");
	_dialog("RBS_ELF_MMMF_DV_1_3_1B");
	_execute(0,scene_impregnate_random);
	_jump("Selectedsceneafter");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RBS_ELF_MMMM_DV_2_1");
	_dialog("RBS_ELF_MMMM_DV_2_1");
	_dialog("RBS_ELF_MMMM_DV_2_2",true);
	_choice_selectedscene_RBS("Spend it in the caboose.","Selectedscene","RBS_ELF_MMMM_DV_2_3A");
	_choice_selectedscene_RBS("Spend it on their faces.","Selectedscene","RBS_ELF_MMMM_DV_2_3B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

	
_label("RBS_ELF_MMMM_SV_1_1");
	_dialog("RBS_ELF_MMMM_SV_1_1");
	_dialog("RBS_ELF_MMMM_SV_1_2");
	_dialog("RBS_ELF_MMMM_SV_1_3");
	_dialog("RBS_ELF_MMMM_SV_1_4");
	_jump("Selectedsceneafter");
	_block();	
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////