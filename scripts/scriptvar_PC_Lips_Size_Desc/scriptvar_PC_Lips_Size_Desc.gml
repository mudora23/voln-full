///scriptvar_PC_Lips_Size_Desc();
/// @description
var num = var_map_get(PC_Lips_Size_N);
if num == 0 return choose("very thin","delicate");
else if num == 1 return choose("thin","slight");
else if num == 2 return choose("normally-sized","commonly-sized");
else if num == 3 return choose("full","thick");
else if num == 4 return choose("pillowy","padded");
else return choose("stuffed","bulging");