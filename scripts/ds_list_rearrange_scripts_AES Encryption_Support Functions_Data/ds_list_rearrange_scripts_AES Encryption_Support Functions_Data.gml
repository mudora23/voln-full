///ds_list_rearrange(list, id[,copy])
//arranges a list of maps to a map with id as index
var l = argument[0], s = ds_list_size(l), m = dm(), i, dat, copy = true, n;
if (argument_count > 2) copy = argument[2];

for (i = 0; i < s; ++i) {
    dat = l[| i];
    if (copy) {
        n = dm(); ds_map_copy(n, dat);
        ds_map_add_map(m, dat[? argument[1]], n); 
    } else {
        ds_map_add_map(m, dat[? argument[1]], dat); 
    }
}

return m;


