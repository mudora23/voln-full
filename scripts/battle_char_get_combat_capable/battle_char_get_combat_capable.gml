///battle_get_combat_capable(char);
/// @description
/// @param char
return argument0[? GAME_CHAR_HEALTH] > 0 && argument0[? GAME_CHAR_LUST] < char_get_lust_max(argument0);