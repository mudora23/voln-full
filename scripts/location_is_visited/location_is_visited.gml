///location_is_visited(LOCATION);
/// @description
with obj_control
{
	return ds_list_find_index(obj_control.var_map[? VAR_LOCATION_VISITED_LIST],argument[0])>=0;
}