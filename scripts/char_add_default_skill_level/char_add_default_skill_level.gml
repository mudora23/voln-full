///char_add_default_skill_level(char_map,skill,amount);
/// @description
/// @param char_map
/// @param skill
/// @param amount

var skills_map = argument0[? GAME_CHAR_SKILLS_MAP];

if !ds_map_exists(skills_map,argument1)
	skills_map[? argument1] = argument2;
else
	skills_map[? argument1] = skills_map[? argument1]+argument2;
	

