///scene_cg_list_size();
/// @description  

with obj_control
{
	return ds_list_size(obj_control.var_map[? VAR_CG_LIST_ID]);

}