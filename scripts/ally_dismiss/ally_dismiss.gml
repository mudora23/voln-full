///ally_dismiss(ID/Name);
/// @description
/// @param ID/Name
with obj_control
{
	if !is_string(argument0) var char_map = char_id_get_char(argument0);
	else var char_map = ally_get_char(argument0);
	
    var ally_list = var_map[? VAR_ALLY_LIST];
    var ally_r_list = var_map[? VAR_ALLY_R_LIST];
    var ally_index = ds_list_find_index(ally_list,char_map);
    if ally_index>=0
    {
		var new_char = ds_map_create();
		ds_map_copy(new_char,ally_list[| ally_index]);
        ds_list_add(ally_r_list,new_char);
        ds_list_mark_as_map(ally_r_list,ds_list_size(ally_r_list)-1);
        ds_list_delete(ally_list,ally_index);
    }
	
	
	
}