///ally_dismiss_all();
/// @description
with obj_control
{
	var ally_list = var_map[? VAR_ALLY_LIST];
    for(var i = 0;i<ds_list_size(ally_list);i++)
    {
        ally_dismiss(char_get_info(ally_list[| i],GAME_CHAR_ID));
		i--;
    }
	
}