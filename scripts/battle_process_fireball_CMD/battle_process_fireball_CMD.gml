///scene_script_battle_process_fireball_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();


			
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	
	skill_spend_cost(char_source,SKILL_FIREBALL,char_get_skill_level(char_source,SKILL_FIREBALL));

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" casts",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" Fireball",TAG_FONT_N,TAG_COLOR_TORU));
	ds_list_add(battlelog_pieces_list,text_add_markup(" at",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));

	// performing
	var dodge_chance = char_get_dodge_chance(char_target);
	var hit_chance = char_get_hit_chance(char_source);
	

	//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_TORU);
	
	postfx_ps_fireball(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
	
	if percent_chance(80-dodge_chance+hit_chance)
	{
		if char_get_skill_level(char_source,SKILL_FIREBALL) >= 3
		{
			if percent_chance(70)
			{
				if percent_chance(char_get_resistance_chance(char_target)-hit_chance)
					draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,0.8,0.8);
				else
				{
					draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Burn!",COLOR_EFFECT,0.8,0.8);
					script_execute_add(0.8,battle_char_debuff_apply,char_target,DEBUFF_BURN,3,3,char_get_char_id(char_source));
				}
			}
			var damage = char_get_toru_damage(char_source)* random_range(2.5,3.5);
		}
		else if char_get_skill_level(char_source,SKILL_FIREBALL) == 2
		{
			if percent_chance(60)
			{
				if percent_chance(char_get_resistance_chance(char_target)-hit_chance)
					draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,0.8,0.8);
				else
				{
					draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Burn!",COLOR_EFFECT,0.8,0.8);
					script_execute_add(0.8,battle_char_debuff_apply,char_target,DEBUFF_BURN,2,3,char_get_char_id(char_source));
				}
			}
			var damage = char_get_toru_damage(char_source)* random_range(1.8,2.5);
		}
		else
		{
			if percent_chance(50)
			{
				if percent_chance(char_get_resistance_chance(char_target)-hit_chance)
					draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Resist!",COLOR_EFFECT,0.8,0.8);
				else
				{
					draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"Burn!",COLOR_EFFECT,0.8,0.8);
					script_execute_add(0.8,battle_char_debuff_apply,char_target,DEBUFF_BURN,1,3,char_get_char_id(char_source));
				}
			}
			var damage = char_get_toru_damage(char_source)* random_range(1.2,1.8);
		}
		var damage_size_adjust = 1;
		var shake_size_adjust = 1;
		if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);voln_play_sfx(sfx_Hit8);damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
		var armor = char_get_toru_armor(char_target);
		var real_damage = damage * clamp(((100-armor)/100),0,1);
		char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage);
		
		
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP,0.5,1.2*damage_size_adjust);
		
		ally_get_hit_port(char_target);
		
		ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage)),TAG_FONT_N,TAG_COLOR_DAMAGE));
		ds_list_add(battlelog_pieces_list,text_add_markup(" damage!",TAG_FONT_N,TAG_COLOR_NORMAL));
		
		char_target[? GAME_CHAR_COLOR] = COLOR_DAMAGE;
		char_target[? GAME_CHAR_SHAKE_AMOUNT] = 30*shake_size_adjust;
			
		var debug_message = "Fireball is performed,";
		debug_message+= " source: ";
		debug_message+= string(damage);
		debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
		debug_message+= " target: ";
		debug_message+= string(armor);
		debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";

	}
	else
	{
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,0,0.7);
		ds_list_add(battlelog_pieces_list,text_add_markup(" and misses!",TAG_FONT_N,TAG_COLOR_NORMAL));
	}	
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	voln_play_sfx(sfx_FireBall_Cast);

	script_execute_add(BATTLE_PAUSE_TIME_LONG_SEC,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Toru ATK CMD script' is executed!------");

}