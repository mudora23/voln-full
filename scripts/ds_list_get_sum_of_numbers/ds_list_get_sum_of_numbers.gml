///ds_list_get_sum_of_numbers(id);
/// @description
/// @param id
var sum = 0;
for(var i = 0; i < ds_list_size(argument0);i++)
	sum += argument0[| i];
return sum;