///_player_offspring_customize(char_id);
/// @descirption
/// @param char_id
if scene_is_current()
{
	if !instance_exists(obj_player_offspring_customize)
	{
		var inst = instance_create_depth(0,0,DEPTH_MAIN_ITEM,obj_player_offspring_customize);
		inst.char_id = argument[0];	
		inst.current_script_posi = var_map_get(VAR_SCRIPT_POSI);
	}
}
var_map_add(VAR_SCRIPT_POSI_FLOATING,1);