///scene_list_add_dv(possible_list);
/// @description
/// @param possible_list
var possible_list = argument[0];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Elfs
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_ANY)
{
	if ally_exists(NAME_ZEYD) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		{
			ds_list_add(possible_list,"RBS_ELF_F_DV_1_1");
			if percent_chance(30) return ds_list_with_value(possible_list,"RBS_ELF_F_DV_1_1");
		}

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_2_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_3_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_4_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_5_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_6_1");
	
	if ally_exists(NAME_BALWAG) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_7_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) && obj_control.var_map[? VAR_JAR] >= 1 &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_8_1");
	
	if ally_exists(NAME_BALWAG) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE) &&
	  (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_F_DV_9_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_FF_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_FF_DV_2_1");
	
	if ally_exists(NAME_BALWAG) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_FF_DV_3_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_FFF_DV_1_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_FFFF_DV_1_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_M_DV_1_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_M_DV_2_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_M_DV_3_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE) && obj_control.var_map[? VAR_JAR] >= 1 &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_M_DV_4_1");
	
	if ally_exists(NAME_LORN) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 1)
		ds_list_add(possible_list,"RBS_ELF_M_DV_5_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_MF_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_MF_DV_2_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_MFF_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_MFF_DV_2_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_MFFF_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_MM_DV_1_1");
	
	if ally_exists(NAME_KUDI) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_MM_DV_2_1");
	
	if ally_exists(NAME_LORN) && ally_exists(NAME_BALWAG) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 2)
		ds_list_add(possible_list,"RBS_ELF_MM_DV_3_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_MMF_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_MMF_DV_2_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_MMFF_DV_1_1");	
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_MMFF_DV_2_1");

	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_MMM_DV_1_1");
	
	if ally_exists(NAME_KUDI) && char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() == 3)
		ds_list_add(possible_list,"RBS_ELF_MMM_DV_2_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_FEMALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_MMMF_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_MMMM_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE,NAME_ELF_BANDITS,GENDER_MALE) &&
	   (enemy_get_sex_number_type() > 1 || enemy_get_sex_number() >= 4)
		ds_list_add(possible_list,"RBS_ELF_MMMM_DV_2_1");
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Orks
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK,GENDER_ANY) ||
   char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK,GENDER_ANY)
{
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_1,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_1,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_2_1");	
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_3_1");		
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_4_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_5_1");
	
	if ally_exists(NAME_ZEYD) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_6_1");
	
	if ally_exists(NAME_ZEYD) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_1,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_7_1");
	
	if ally_exists(NAME_ZEYD) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_8_1");
	
	if obj_control.var_map[? VAR_JAR] >= 1 && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_9_1");
	
	if ally_exists(NAME_BALWAG) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_10_1");
	
	if ally_exists(NAME_KUDI) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_11_1");
	
	if ally_exists(NAME_KUDI) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_12_1");
	
	if ally_exists(NAME_ZEYD) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_13_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_14_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_15_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_16_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_17_1");
	
	if ally_exists(NAME_KUDI) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_3,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_3,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_18_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_2,GENDER_MALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_2,GENDER_MALE)
		ds_list_add(possible_list,"RBS_ORK_DV_19_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_2,GENDER_FEMALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_2,GENDER_FEMALE)
		ds_list_add(possible_list,"RBS_ORK_DV_20_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_1,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_21_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_4,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_4,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_22_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_3,GENDER_ANY,NAME_GREEN_ORK_4,GENDER_ANY,NAME_GREEN_ORK_4,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_3,GENDER_ANY,NAME_RED_ORK_4,GENDER_ANY,NAME_RED_ORK_4,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_23_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_MALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_MALE)
		ds_list_add(possible_list,"RBS_ORK_DV_24_1");
	
	if ally_exists(NAME_JAM) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_MALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_MALE))
		ds_list_add(possible_list,"RBS_ORK_DV_25_1");
	
	if ally_exists(NAME_JAM) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_MALE,NAME_GREEN_ORK_2,GENDER_MALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_MALE,NAME_RED_ORK_2,GENDER_MALE))
		ds_list_add(possible_list,"RBS_ORK_DV_26_1");
	
	if ally_exists(NAME_BALWAG) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_MALE,NAME_GREEN_ORK_2,GENDER_MALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_MALE,NAME_RED_ORK_2,GENDER_MALE))
		ds_list_add(possible_list,"RBS_ORK_DV_27_1");
	
	if ally_exists(NAME_BALWAG) && ally_exists(NAME_JAM) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_MALE,NAME_GREEN_ORK_2,GENDER_MALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_MALE,NAME_RED_ORK_2,GENDER_MALE))
		ds_list_add(possible_list,"RBS_ORK_DV_28_1");
	
	if obj_control.var_map[? VAR_JAR] >= 1 && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_FEMALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_FEMALE))
		ds_list_add(possible_list,"RBS_ORK_DV_29_1");
	
	if ally_exists(NAME_JAM) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_FEMALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_FEMALE))
		ds_list_add(possible_list,"RBS_ORK_DV_30_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_2,GENDER_FEMALE,NAME_GREEN_ORK_2,GENDER_FEMALE) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_2,GENDER_FEMALE,NAME_RED_ORK_2,GENDER_FEMALE)
		ds_list_add(possible_list,"RBS_ORK_DV_31_1");
	
	if obj_control.var_map[? VAR_JAR] >= 1 && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_1,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_32_1");
	
	if ally_exists(NAME_KUDI) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_1,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_33_1");
	
	if ally_exists(NAME_LORN) && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_1,GENDER_ANY,NAME_GREEN_ORK_1,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_1,GENDER_ANY,NAME_RED_ORK_1,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_34_1");
	
	if obj_control.var_map[? VAR_JAR] >= 1 && (char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_4,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_4,GENDER_ANY))
		ds_list_add(possible_list,"RBS_ORK_DV_35_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_4,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_4,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_36_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_GREEN_ORK_4,GENDER_ANY,NAME_GREEN_ORK_4,GENDER_ANY) || char_map_list_sex_team_exists(enemy_get_list(),NAME_RED_ORK_4,GENDER_ANY,NAME_RED_ORK_4,GENDER_ANY)
		ds_list_add(possible_list,"RBS_ORK_DV_37_1");
}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											Imps
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
{
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
		ds_list_add(possible_list,"RBS_IMP_DV_1_1");
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
		ds_list_add(possible_list,"RBS_IMP_DV_2_1");	
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
		ds_list_add(possible_list,"RBS_IMP_DV_3_1");	
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
		ds_list_add(possible_list,"RBS_IMP_DV_4_1");		
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
		ds_list_add(possible_list,"RBS_IMP_DV_5_1");		
	
	//if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
	//	ds_list_add(possible_list,"RBS_IMP_DV_6_1");		
	
	//if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
	//	ds_list_add(possible_list,"RBS_IMP_DV_7_1");		
	
	//if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
	//	ds_list_add(possible_list,"RBS_IMP_DV_8_1");	
	
	//if ally_exists(NAME_ZEYD) && char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY)
	//	ds_list_add(possible_list,"RBS_IMP_DV_9_1");		
	
	if char_map_list_sex_team_exists(enemy_get_list(),NAME_IMP,GENDER_ANY,NAME_IMP,GENDER_ANY)
		ds_list_add(possible_list,"RBS_IMP_DV_10_1");	
}	
	
return possible_list;

