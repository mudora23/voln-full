///storage_keep_pet(i);
/// @description
/// @param i
var pet_list = obj_control.var_map[? VAR_PET_LIST];
var storage_list = obj_control.var_map[? VAR_STORAGE_LIST];
if argument0 < ds_list_size(pet_list)
{
	var char_map = ally_get_char(argument0);
	ds_list_add(storage_list,pet_list[| argument0]);
    ds_list_mark_as_map(storage_list,ds_list_size(storage_list)-1);
    ds_list_delete(pet_list,argument0);
}
