///short_term_buff_debuff_wiki_map_init();
/// @description
with obj_control
{
	short_term_buff_debuff_wiki_map = ds_map_create();
		
	#macro SHORT_TERM_BUFF_DEBUFF_TOOLTIP "SHORT_TERM_BUFF_DEBUFF_TOOLTIP"
	#macro SHORT_TERM_BUFF_DEBUFF_ICON "SHORT_TERM_BUFF_DEBUFF_ICON"
		
	#macro BUFF_PHY_ARMOR_UP "BUFF_PHY_ARMOR_UP"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_PHY_ARMOR_UP,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_PHY_ARMOR_UP];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Physical defense buff.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_phy_armor);

	#macro BUFF_TORU_ARMOR_UP "BUFF_TORU_ARMOR_UP"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_TORU_ARMOR_UP,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_TORU_ARMOR_UP];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Toru’n defense buff.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_toru_armor);
		
	#macro BUFF_PHY_ARMOR_UP2 "BUFF_PHY_ARMOR_UP2"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_PHY_ARMOR_UP2,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_PHY_ARMOR_UP2];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Advanced physical defense buff.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_phy_armor);

	#macro BUFF_TORU_ARMOR_UP2 "BUFF_TORU_ARMOR_UP2"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_TORU_ARMOR_UP2,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_TORU_ARMOR_UP2];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Advanced toru’n defense buff.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_toru_armor);
		
	#macro BUFF_HIDE "BUFF_HIDE"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_HIDE,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_HIDE];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Merge with the shadows.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_hide);
		
	#macro BUFF_BODYGUARD "BUFF_BODYGUARD"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_BODYGUARD,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_BODYGUARD];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Enemies can only target this character.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_bodyguard);
		
	#macro BUFF_SPEED_UP "BUFF_SPEED_UP"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,BUFF_SPEED_UP,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? BUFF_SPEED_UP];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Speed buff.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_buff_speed_up);
		
	#macro DEBUFF_PHY_ARMOR_DOWN "DEBUFF_PHY_ARMOR_DOWN"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_PHY_ARMOR_DOWN,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_PHY_ARMOR_DOWN];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Physical defense deficit.  Lowers resistance against common attacks.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_phy_armor);
		
	#macro DEBUFF_TORU_ARMOR_DOWN "DEBUFF_TORU_ARMOR_DOWN"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_TORU_ARMOR_DOWN,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_TORU_ARMOR_DOWN];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Toru’n defense deficit.  Lowers resistance against spells.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_toru_armor);
		
	#macro DEBUFF_BLIND "DEBUFF_BLIND"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_BLIND,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_BLIND];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Blindness.  Accuracy lowered.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_blind);	
		
	#macro DEBUFF_POISON "DEBUFF_POISON"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_POISON,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_POISON];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Poison.  This inflicts damage per turn.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_poison);		
		
	#macro DEBUFF_SILENCE "DEBUFF_SILENCE"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_SILENCE,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_SILENCE];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Silence.  Refrains effected from casting spells.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_silence);		
		
	#macro DEBUFF_STUN "DEBUFF_STUN"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_STUN,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_STUN];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Stun.  Causes a chance of turn being skipped.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_stun);	
		
	#macro DEBUFF_TEMPT "DEBUFF_TEMPT"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_TEMPT,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_TEMPT];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Tempt.  Increases the gain of lust over time.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_tempt);	
		
	#macro DEBUFF_WOUND "DEBUFF_WOUND"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_WOUND,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_WOUND];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Wound.  This saps strength per turn.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_wound);	
		
	#macro DEBUFF_BURN "DEBUFF_BURN"
	ds_map_add_unique_map(short_term_buff_debuff_wiki_map,DEBUFF_BURN,ds_map_create());
		var skill_map = short_term_buff_debuff_wiki_map[? DEBUFF_BURN];
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_TOOLTIP,"Burn.  This burns and inflicts damage per turn.");
		ds_map_add_unique(skill_map,SHORT_TERM_BUFF_DEBUFF_ICON,spr_debuff_burn);	
		

		
}	