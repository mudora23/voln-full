///draw_char_part(font_spr,x,y,char,xscale,yscale,color,alpha,area_x1,area_y1,area_w,area_h);
/// @description
/// @param font_spr
/// @param x
/// @param y
/// @param char
/// @param xscale
/// @param yscale
/// @param color
/// @param alpha
/// @param area_x1
/// @param area_y1
/// @param area_w
/// @param area_h

draw_sprite_in_visible_area(argument[0],string_pos(argument[3],FONT_MAP_STRING) - 1,argument[1],argument[2],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
