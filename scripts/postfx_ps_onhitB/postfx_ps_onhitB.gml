///postfx_ps_onhitB(x,y);
/// @description -
/// @param x
/// @param y


if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);
part_emitter_region(global.ps, global.ps_OnHit1B, xp-1, xp+1, yp-4, yp+4, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_OnHit1B, global.ps_OnHit1B, 1);
part_emitter_region(global.ps, global.ps_OnHit2B, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_OnHit2B, global.ps_OnHit2B, 10);
part_emitter_region(global.ps, global.ps_OnHit3B, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_OnHit3B, global.ps_OnHit3B, 10);