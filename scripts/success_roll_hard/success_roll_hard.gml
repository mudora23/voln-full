///success_roll_hard(number);
/// @description
/// @param number
var success = 0;
repeat(argument0)
    success+=percent_chance(30);
return success;
