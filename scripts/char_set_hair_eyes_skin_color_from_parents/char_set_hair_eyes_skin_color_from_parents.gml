///char_set_hair_eyes_skin_color_from_parents(char_map);
/// @description
/// @param char_map
var dad = char_id_get_char(argument0[? GAME_CHAR_DAD_ID]);
var mom = char_id_get_char(argument0[? GAME_CHAR_MOM_ID]);
var variance = 0.05; 

// update colors for player
if dad == char_get_player() || mom == char_get_player()
	char_set_player_hair_eyes_skin_color_from_ini();


argument0[? GAME_CHAR_COLOR_HAIR] = merge_color(dad[? GAME_CHAR_COLOR_HAIR],mom[? GAME_CHAR_COLOR_HAIR],random_range(variance,-variance));
argument0[? GAME_CHAR_COLOR_EYE] = merge_color(dad[? GAME_CHAR_COLOR_EYE],mom[? GAME_CHAR_COLOR_EYE],random_range(variance,-variance));
argument0[? GAME_CHAR_COLOR_SKIN] = merge_color(dad[? GAME_CHAR_COLOR_SKIN],mom[? GAME_CHAR_COLOR_SKIN],random_range(variance,-variance));


