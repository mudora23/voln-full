///posifix_radial_blur(x,y,fade_in_sec,stay_sec,fade_out_sec,amount[0.02]);
/// @description  
/// @param x
/// @param y
/// @param fade_in_sec
/// @param stay_sec
/// @param fade_out_sec
/// @param amount[0.02]

ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_NAME_LIST],POSTFX_RADIAL_BLUR);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_LIST],argument[2]+argument[3]+argument[4]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_MAX_LIST],argument[2]+argument[3]+argument[4]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG0_LIST],argument[0]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG1_LIST],argument[1]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG2_LIST],argument[2]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG3_LIST],argument[3]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG4_LIST],argument[4]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG5_LIST],argument[5]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG6_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG7_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG8_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG9_LIST],0);



