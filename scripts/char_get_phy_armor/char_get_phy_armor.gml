///char_get_phy_armor(char_map);
/// @description
/// @param char_map

var number = 8*power(char_get_FOU(argument0),0.5)+10;
if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;

if battle_char_get_buff_level(argument0,BUFF_PHY_ARMOR_UP) == 1 number += 20;
else if battle_char_get_buff_level(argument0,BUFF_PHY_ARMOR_UP) == 2 number += 30;
else if battle_char_get_buff_level(argument0,BUFF_PHY_ARMOR_UP) >= 3 number += 40;

if battle_char_get_buff_level(argument0,BUFF_PHY_ARMOR_UP2) == 1 number += 40;
else if battle_char_get_buff_level(argument0,BUFF_PHY_ARMOR_UP2) == 2 number += 50;
else if battle_char_get_buff_level(argument0,BUFF_PHY_ARMOR_UP2) >= 3 number += 60;

if battle_char_get_buff_level(argument0,BUFF_BODYGUARD) == 1 number += 10;
else if battle_char_get_buff_level(argument0,BUFF_BODYGUARD) == 2 number += 20;
else if battle_char_get_buff_level(argument0,BUFF_BODYGUARD) >= 3 number += 30;

//if char_get_skill_exists(argument0,SKILL_I_AM_THE_MAIN_CHARACTER) armor+=10;
return min(90,number);