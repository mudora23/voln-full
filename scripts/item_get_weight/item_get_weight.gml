///item_get_weight(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	var item_map = item_get_map(name);
	
	if ds_map_exists(item_map,ITEM_WEIGHT)
		var item_weight = item_map[? ITEM_WEIGHT];
	else
		var item_weight = 0;
	
	return item_weight;
	
}