///battle_char_list_get_combat_capable_number(char_list);
/// @description
var total = 0;
for(var i = 0;i< ds_list_size(argument0);i++)
{
	if battle_char_get_combat_capable( argument0[| i])
		total++;
}

return total;
