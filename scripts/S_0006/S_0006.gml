///S_0006();
/// @description

_label("start");

_label("S_0006_1");
	_dialog("S_0006_1");
	_jump("MENU","SL1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0006_2");
	_qj_remove("S_0006_QJ");
	_dialog("S_0006_2");
	_dialog("S_0006_3");
	_dialog("S_0006_4");
	_change_bg("bg",spr_bg_Nirholt_HD_02,FADE_NORMAL_SEC,1.5);
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0006_5");
	_dialog("S_0006_6");
	_dialog("S_0006_7");
	_dialog("S_0006_8");
	_dialog("S_0006_9");
	_dialog("S_0006_10");
	_dialog("S_0006_11");
	_dialog("S_0006_12");
	_dialog("S_0006_13");
	_dialog("S_0006_14");
	_dialog("S_0006_15");
	_dialog("S_0006_16");
	_dialog("S_0006_17");
	_dialog("S_0006_18");
	_dialog("S_0006_19");
	_dialog("S_0006_20");
	_hide_all();
	_main_gui(VAR_GUI_MIN);
	_dialog("S_0006_21");
	_main_gui(VAR_GUI_NORMAL);
	_change_bg("bg",spr_bg_Nirholt_HD_02,FADE_NORMAL_SEC,1.5);
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0006_22");
	_dialog("S_0006_23");
	_dialog("S_0006_24");
	_dialog("S_0006_25");
	_dialog("S_0006_26");
	_dialog("S_0006_27");
	_movex("felg",XNUM_LEFT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("zeyd",spr_char_Zeyd_f_n,10,XNUM_RIGHT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0006_28");
	_dialog("S_0006_29");
	_dialog("S_0006_30");
	_dialog("S_0006_31");
	_dialog("S_0006_32");
	_dialog("S_0006_33");
	_dialog("S_0006_34"); // don't remove this
	_void();//_dialog("S_0006_35");
	_execute(0,ally_storage_lock,NAME_KUDI,false);
	_execute(0,ally_storage_lock,NAME_ZEYD,false);
	_void();//_execute(0,ally_dismiss_all);
	_qj_add("S_0007_QJ");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////










