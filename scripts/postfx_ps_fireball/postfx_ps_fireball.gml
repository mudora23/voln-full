///postfx_ps_fireball(x,y);
/// @description -
/// @param x
/// @param y

if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);
part_emitter_region(global.ps, global.ps_fireball_center, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_fireball_center, global.ps_fireball_center, 10);
part_emitter_region(global.ps, global.ps_fireball_particle, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_fireball_particle, global.ps_fireball_particle, 30);
part_emitter_region(global.ps, global.ps_fireball_smoke, xp, xp, yp, yp, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_fireball_smoke, global.ps_fireball_smoke, 30);
part_emitter_region(global.ps, global.ps_fireball_centerB, xp-8, xp+8, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_fireball_centerB, global.ps_fireball_centerB, 5);