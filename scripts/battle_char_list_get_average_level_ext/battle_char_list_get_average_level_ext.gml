///battle_char_list_get_average_level_ext(char_list,extra_char);
/// @description
/// @param char_list
/// @param extra_char

var new_list = ds_list_create();

for(var i = 0; i < ds_list_size(argument0);i++)
{
	ds_list_add(new_list,argument0[| i]);

}
ds_list_add(new_list,argument1);
var average_level = battle_char_list_get_average_level(new_list);
ds_list_destroy(new_list);
return average_level;