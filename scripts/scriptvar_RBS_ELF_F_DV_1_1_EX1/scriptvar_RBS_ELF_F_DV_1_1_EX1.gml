///scriptvar_RBS_ELF_F_DV_1_1_EX1();
/// @description

if var_map_get(PC_Nads_N) > 0
	return ", his tongue sliding up in front of your "+scriptvar_PC_Nads_Size_Desc()+" "+scriptvar_PC_Nads_Size_Name()+scriptvar_PC_Nads_nPlur()+scriptvar_PC_Nads_Size_Pad_Desc();
else
	return "";