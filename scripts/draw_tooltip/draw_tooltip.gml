///draw_tooltip(x,y,string,halign[optional],valign[optional],fancy[optional],color[optional]);
/// @description
/// @param x
/// @param y
/// @param string
/// @param halign[optional]
/// @param valign[optional]
/// @param fancy[optional]
/// @param color[optional]





var text_xx = argument[0];
var text_yy = argument[1];
var text = argument[2];

if text == "" exit;

draw_set_font(font_01s_reg);

var padding = 5;
var margin = 10;
var text_width = string_width_ext(string_hash_to_newline(text),25,1500)+padding*2;
var text_height = string_height_ext(string_hash_to_newline(text),25,1500)+padding*2;
if argument_count > 3 var halign = argument[3]; else var halign = fa_left;
if argument_count > 4 var valign = argument[4]; else var valign = fa_top;
if argument_count > 5 var fancy = argument[5]; else var fancy = false;
if argument_count > 6 var color = argument[6]; else var color = c_dkwhite;
if halign == fa_left
{
	var xx = text_xx;
	var xx2 = text_xx+text_width;
}
else if halign == fa_right
{
	var xx = text_xx-text_width;
	var xx2 = text_xx;
}
else
{
	var xx = text_xx-text_width/2;
	var xx2 = text_xx+text_width/2;
}
if valign == fa_top
{
	var yy = text_yy;
	var yy2 = text_yy+text_height;
}
else if valign == fa_bottom
{
	var yy = text_yy-text_height;
	var yy2 = text_yy;
}
else
{
	var yy = text_yy-text_height/2;
	var yy2 = text_yy+text_height/2;
}



if xx < margin
{
    var xoffset = margin - xx;
    xx += xoffset;
    xx2 += xoffset;
}
if yy < margin
{
    var yoffset = margin - yy;
    yy += yoffset;
    yy2 += yoffset;
}
if xx2 > 1920-margin
{
    var xoffset = xx2 - 1920+margin;
    xx -= xoffset;
    xx2 -= xoffset;
}
if yy2 > 1080-margin
{
    var yoffset = yy2 - 1080+margin;
    yy -= yoffset;
    yy2 -= yoffset;
}





// BG
var old_alpha = draw_get_alpha();
var old_color = draw_get_color();;
draw_set_alpha(0.8);
draw_set_color(c_lightblack);
if fancy
{
	draw_sprite_in_area_fill(spr_gui_bg_seemless,xx,yy,xx2-xx,yy2-yy,make_color_rgb(obj_control.colorR,obj_control.colorG,obj_control.colorB),image_alpha,fa_right,fa_bottom);
	gpu_set_blendmode_ext(bm_dest_color, bm_zero);
	draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,xx,yy,xx2-xx,yy2-yy,c_gray,image_alpha);
	gpu_set_blendmode(bm_normal);
}
else
{
	draw_rectangle_color(xx,yy,xx2,(yy+yy2)/2,c_black,c_black,c_lightblack,c_lightblack,false);
	draw_rectangle_color(xx,(yy+yy2)/2,xx2,yy2,c_lightblack,c_lightblack,c_black,c_black,false);
	//draw_rectangle(xx,yy,xx2,yy2,false);
}
draw_set_alpha(1);
//draw_set_color(c_white);
//draw_rectangle(xx,yy,xx2,yy2,true);

// Text
draw_set_alpha(old_alpha);
draw_set_color(color);//draw_set_color(old_color);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_text_ext(xx+padding,yy+padding,string_hash_to_newline(text),25,1500);
//draw_text(xx+padding,yy+padding,string_hash_to_newline(text));
//draw_text_ext_transformed(xx+padding,yy+padding,string_hash_to_newline(text),20,500,1,1,1);
// reset
draw_reset();

return yy;