///scene_impregnate_random();
/// @description

var num_list = ds_list_create();
var enemy_list = var_map_get(VAR_ENEMY_LIST);
for(var i = 0 ; i < ds_list_size(enemy_list);i++)
{
	var enemy = enemy_list[| i];
	if enemy[? GAME_CHAR_SEX] && enemy[? GAME_CHAR_GENDER] == GENDER_FEMALE
		ds_list_add(num_list,i);
}

if ds_list_size(num_list) > 0
{
	ds_list_shuffle(num_list);
	var the_num = num_list[| 0];
	
	var enemy = enemy_list[| the_num];
	enemy[? GAME_CHAR_IMPREGNATE] = true;
	enemy[? GAME_CHAR_IMPREGNATE_BY_CHARID] = char_get_char_id(char_get_player());
	//show_message("enemy[? GAME_CHAR_IMPREGNATE_BY_CHARID]: "+string(enemy[? GAME_CHAR_IMPREGNATE_BY_CHARID]));
	var_map_set(VAR_TEMP_CHARID,enemy[? GAME_CHAR_ID]);
	
}

ds_list_destroy(num_list);