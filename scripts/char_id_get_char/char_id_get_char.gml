///char_id_get_char(charid);
/// @description
var the_list = var_map_get_all_character_map_list();
for(var i = 0; i < ds_list_size(the_list);i++)
{
	var the_char = the_list[| i];	
	if the_char[? GAME_CHAR_ID] == argument0
		return the_char;
}
return noone;
	
