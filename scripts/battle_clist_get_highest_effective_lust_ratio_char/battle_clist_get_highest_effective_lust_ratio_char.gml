///battle_clist_get_highest_effective_lust_ratio_char(char_list);
/// @description 
/// @param char_list

var real_lust_ratio = 0;
var return_char = noone;

for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];

	if battle_char_get_combat_capable(char)
	{
		if return_char == noone || battle_char_get_effective_lust_ratio(char) > real_lust_ratio
		{
			return_char = char;
			real_lust_ratio = battle_char_get_effective_lust_ratio(char);
		}

	}
}

return return_char;