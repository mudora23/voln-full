///script_get_index_ext(script_in_any_form);
/// @description
/// @param script_in_any_form
if is_string(argument0)
{
	if asset_get_type(argument0) == asset_script
		return asset_get_index(argument0);
	else
		return noone;
}
else
{
	if script_exists(argument0)
		return argument0;
	else
		return noone;
	
}