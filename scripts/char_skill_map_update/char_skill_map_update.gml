///char_skill_map_update(char,skill_map);
/// @description
/// @param char
/// @param skill_map
var skill_key = ds_map_find_first(argument1);
while !is_undefined(skill_key)
{
	var current_level = argument1[? skill_key];
	var key_map = obj_control.skill_wiki_map[? skill_key];
	
	if ds_map_exists(key_map,SKILL_REQ_LEVEL_LIST)
	{
		var key_req_level_list = key_map[? SKILL_REQ_LEVEL_LIST];
		while ds_list_size(key_req_level_list) > current_level
		{
			if key_req_level_list[| current_level] <= argument0[? GAME_CHAR_LEVEL]
			{
				current_level++;
				argument1[? skill_key] = current_level;
			}
			else
				break;
		}
	}
	else
	{
		argument1[? skill_key] = 1;
	}
	
	var skill_key = ds_map_find_next(argument1,skill_key);
}