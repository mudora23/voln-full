///round_chance(number);
/// @description
/// @param number
var number = argument0;
var number_floor = floor(number);
var number_chance = number - number_floor;
return number_floor + percent_chance(number_chance*100)
