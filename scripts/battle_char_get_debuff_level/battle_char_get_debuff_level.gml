///battle_char_get_debuff_level(char,debuff);
/// @description
/// @param char
/// @param debuff
var char_map = argument0;
var char_debuff_list = char_map[? GAME_CHAR_DEBUFF_LIST];
var char_debuff_level_list = char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST];
var char_debuff = argument1;
var char_debuff_index = ds_list_find_index(char_debuff_list,char_debuff);
if char_debuff_index >= 0
	return char_debuff_level_list[| char_debuff_index];
else
	return 0;