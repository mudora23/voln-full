///_aes_InvCipher(state, RoundKey)
var rnd = 0, state = argument0, RoundKey = argument1;
var Nk = global._aes_Nk, Nb = global._aes_Nb, Nr = global._aes_Nr, Rcon = global._aes_Rcon;

_aes_AddRoundKey(state, RoundKey, Nr);

for(rnd=Nr-1;rnd>0;rnd--)
{
    _aes_InvShiftRows(state);
    _aes_InvSubBytes(state);
    _aes_AddRoundKey(state, RoundKey, rnd);
    _aes_InvMixColumns(state);
}

_aes_InvShiftRows(state);
_aes_InvSubBytes(state);
_aes_AddRoundKey(state, RoundKey, 0)

