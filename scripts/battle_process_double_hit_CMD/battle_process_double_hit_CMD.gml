///scene_script_battle_process_double_hit_CMD();
/// @description
/// @param chosen_index
with obj_control
{


	scene_choices_list_clear();
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	
	skill_spend_cost(char_source,SKILL_DOUBLE_HIT,char_get_skill_level(char_source,SKILL_DOUBLE_HIT));




	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" attacks twice, ",TAG_FONT_N,TAG_COLOR_NORMAL));
	
	// performing
	var dodge_chance = char_get_dodge_chance(char_target);
	var hit_chance = char_get_hit_chance(char_source);

	script_execute_add(0,postfx_ps_doublehit,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
	script_execute_add(0.2,postfx_ps_doublehit_2,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
	
	// 1st hit
	if percent_chance(80-dodge_chance+hit_chance) var hit1 = true;
	else var hit1 = false;
	if percent_chance(80-dodge_chance+hit_chance) var hit2 = true;
	else var hit2 = false;
	
	
	var real_damage = 0;
	var real_damage2 = 0;
	var real_lust_amount = 0;
	var real_lust_amount2 = 0;
	
	if hit1
	{
		if char_get_skill_level(char_source,SKILL_DOUBLE_HIT) == 3
			var damage = char_get_phy_damage(char_source)* random_range(0.9,1.1);
		else if char_get_skill_level(char_source,SKILL_DOUBLE_HIT) == 2
			var damage = char_get_phy_damage(char_source)* random_range(0.8,1.0);
		else
			var damage = char_get_phy_damage(char_source)* random_range(0.7,0.9);
			
		if percent_chance(char_get_crit_chance(char_source)) damage*=char_get_crit_multiply_ratio(char_source);
		var armor = char_get_phy_armor(char_target);
		var real_damage = damage * clamp(((100-armor)/100),0,1);
		
		char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage);
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP);
	
		var real_lust_amount = min(2,char_target[? GAME_CHAR_LUST]);
		char_target[? GAME_CHAR_LUST] = char_target[? GAME_CHAR_LUST] - real_lust_amount;
		
		if real_lust_amount > 0
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_lust_amount)),COLOR_LUST,0.2);

	}
	else
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,0,0.7);
	
	if hit2
	{
		if char_get_skill_level(char_source,SKILL_DOUBLE_HIT) == 3
			var damage = char_get_phy_damage(char_source)* random_range(0.9,1.1);
		else if char_get_skill_level(char_source,SKILL_DOUBLE_HIT) == 2
			var damage = char_get_phy_damage(char_source)* random_range(0.8,1.0);
		else
			var damage = char_get_phy_damage(char_source)* random_range(0.7,0.9);
			
		if percent_chance(char_get_crit_chance(char_source)) damage*=char_get_crit_multiply_ratio(char_source);
		var armor = char_get_phy_armor(char_target);
		var real_damage2 = damage * clamp(((100-armor)/100),0,1);
		
		char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage2);
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage2)),COLOR_HP,0.2);
	
		var real_lust_amount2 = min(2,char_target[? GAME_CHAR_LUST]);
		char_target[? GAME_CHAR_LUST] = char_target[? GAME_CHAR_LUST] - real_lust_amount2;
		
		if real_lust_amount2 > 0
			draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_lust_amount2)),COLOR_LUST,0.2);

	}
	else
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,0.2,0.7);
	
	if hit1 || hit2
	{
		
		ally_get_hit_port(char_target);
		
		char_target[? GAME_CHAR_COLOR] = COLOR_HP;
		char_target[? GAME_CHAR_SHAKE_AMOUNT] = 30;

		ds_list_add(battlelog_pieces_list,text_add_markup(" hitting the",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));	
		ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(real_damage+real_damage2)),TAG_FONT_N,TAG_COLOR_DAMAGE));
		ds_list_add(battlelog_pieces_list,text_add_markup(" damage!",TAG_FONT_N,TAG_COLOR_NORMAL));
	}
	else
		ds_list_add(battlelog_pieces_list,text_add_markup(" and misses!",TAG_FONT_N,TAG_COLOR_NORMAL));
	

	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);


	voln_play_sfx(sfx_Strike03); 
	script_execute_add(0.2,voln_play_sfx,sfx_Strike03);
	
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);

}