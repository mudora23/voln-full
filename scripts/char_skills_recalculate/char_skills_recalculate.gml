///char_skills_recalculate(char_map);
/// @description
/// @param char_map
var char_map = argument0;


// skills auto learning...
if char_map == char_get_player()
{
	char_set_default_skill_level(char_get_player(),SKILL_START,1);
}
else if char_map != noone
{
	var is_ally = ds_list_find_index(obj_control.var_map[? VAR_ALLY_LIST],char_map) >= 0;
	
	// for battlelog messages
	if is_ally && instance_exists(obj_battlelog)
	{
		var ally_skill_map_copy = ds_map_create();
		ds_map_copy(ally_skill_map_copy,char_map[? GAME_CHAR_SKILLS_MAP]);
		
		var ally_bonus_skill_map_copy = ds_map_create();
		ds_map_copy(ally_bonus_skill_map_copy,char_map[? GAME_CHAR_BONUS_SKILLS_MAP]);
	}

	// re-calculate default skills
	ds_map_clear(char_map[? GAME_CHAR_SKILLS_MAP]);
	
	// re-set bonus skills to level 0 (for future balacing updates)
	var key = ds_map_find_first(char_map[? GAME_CHAR_BONUS_SKILLS_MAP]);
	while !is_undefined(key)
	{
		ds_list_replace(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],key,0);
		var key = ds_map_find_next(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],key);
	}
	
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	/// learn skills based on character default skills set.
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
	// Double hit
	if char_map[? GAME_CHAR_NAME] == NAME_ZEYD
		char_set_default_skill_level(char_map,SKILL_DOUBLE_HIT,0);
	
	/// Hide
	if char_map[? GAME_CHAR_NAME] == NAME_ZEYD
		char_set_default_skill_level(char_map,SKILL_HIDE,0);
		
	/// Defense up
	if char_map[? GAME_CHAR_NAME] == NAME_KUDI
		char_set_default_skill_level(char_map,SKILL_DEFENSE_UP,0);
		
	/// Barrier
	if char_map[? GAME_CHAR_NAME] == NAME_KUDI
		char_set_default_skill_level(char_map,SKILL_BARRIER,0);
		
	/// Speed up
	if char_map[? GAME_CHAR_NAME] == NAME_FELGOR
		char_set_default_skill_level(char_map,SKILL_SPEED_UP,0);
		
	/// Fireball
	if char_map[? GAME_CHAR_NAME] == NAME_KUDI
		char_set_default_skill_level(char_map,SKILL_FIREBALL,0);
		
	/// Endless whip
	if char_map[? GAME_CHAR_NAME] == NAME_ELF_BANDITS_1 || char_map[? GAME_CHAR_NAME] == NAME_ELF_BANDITS_1B
		char_set_default_skill_level(char_map,SKILL_ENDLESS_WHIP,0);

	// Bam	
	if char_map[? GAME_CHAR_NAME] == NAME_FELGOR || char_map[? GAME_CHAR_NAME] == NAME_LORN ||
	   char_map[? GAME_CHAR_NAME] == NAME_ELF_BANDITS_2 || char_map[? GAME_CHAR_NAME] == NAME_ELF_BANDITS_2B ||
	   char_map[? GAME_CHAR_NAME] == NAME_ELF_BANDITS_3 || char_map[? GAME_CHAR_NAME] == NAME_ELF_BANDITS_3B ||
	   char_map[? GAME_CHAR_NAME_TYPE] == NAME_GREEN_ORK || char_map[? GAME_CHAR_NAME_TYPE] == NAME_RED_ORK ||
	   char_map[? GAME_CHAR_NAME_TYPE] == NAME_IMP || char_map[? GAME_CHAR_NAME_TYPE] == NAME_GRUM
		char_set_default_skill_level(char_map,SKILL_BAM,0);
		
	// Bam 2
	if char_map[? GAME_CHAR_NAME] == NAME_LORN
		char_set_default_skill_level(char_map,SKILL_BAM2,0);
		
	// Heal
	if char_map[? GAME_CHAR_NAME] == NAME_FELGOR
		char_set_default_skill_level(char_map,SKILL_HEAL,0);
	
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	/// Remove skills from bonus skill map if existed in default skill map (for future potential updates)
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	ds_map_delete_duplicate(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],char_map[? GAME_CHAR_SKILLS_MAP]);

	
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	/// levelup skills based on character levels.
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	char_skill_map_update(char_map,char_map[? GAME_CHAR_SKILLS_MAP]);
	char_skill_map_update(char_map,char_map[? GAME_CHAR_BONUS_SKILLS_MAP]);






	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	/// battlelog
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	if is_ally && instance_exists(obj_battlelog)
	{
		var skill_name = ds_map_find_first(char_map[? GAME_CHAR_SKILLS_MAP]);
		while !is_undefined(skill_name)
		{
			if ds_map_find_value(char_map[? GAME_CHAR_SKILLS_MAP],skill_name) > 0
				if !ds_map_exists(ally_skill_map_copy,skill_name) ||
				   ds_map_find_value(char_map[? GAME_CHAR_SKILLS_MAP],skill_name) != ds_map_find_value(ally_skill_map_copy,skill_name)
				   {
						// battlelog
						var battlelog_pieces_list = ds_list_create();
						ds_list_add(battlelog_pieces_list,text_add_markup(char_map[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,TAG_COLOR_ALLY_NAME));
						ds_list_add(battlelog_pieces_list,text_add_markup(" has just learnt ",TAG_FONT_N,TAG_COLOR_NORMAL));
						ds_list_add(battlelog_pieces_list,text_add_markup(skill_get_displayname(skill_name)+" (LV"+string(ds_map_find_value(char_map[? GAME_CHAR_SKILLS_MAP],skill_name))+")",TAG_FONT_N,TAG_COLOR_EFFECT));
						var battle_log = string_append_from_list(battlelog_pieces_list);
						battlelog_add(battle_log);
						ds_list_destroy(battlelog_pieces_list);
				   
				   }
			var skill_name = ds_map_find_next(char_map[? GAME_CHAR_SKILLS_MAP],skill_name);
		}
		ds_map_destroy(ally_skill_map_copy);
		
		
		var skill_name = ds_map_find_first(char_map[? GAME_CHAR_BONUS_SKILLS_MAP]);
		while !is_undefined(skill_name)
		{
			if ds_map_find_value(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],skill_name) > 0
				if !ds_map_exists(ally_bonus_skill_map_copy,skill_name) ||
				   ds_map_find_value(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],skill_name) != ds_map_find_value(ally_bonus_skill_map_copy,skill_name)
				   {
						// battlelog
						var battlelog_pieces_list = ds_list_create();
						ds_list_add(battlelog_pieces_list,text_add_markup(char_map[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,TAG_COLOR_ALLY_NAME));
						ds_list_add(battlelog_pieces_list,text_add_markup(" has just learnt ",TAG_FONT_N,TAG_COLOR_NORMAL));
						ds_list_add(battlelog_pieces_list,text_add_markup(skill_get_displayname(skill_name)+" (LV"+string(ds_map_find_value(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],skill_name))+")",TAG_FONT_N,TAG_COLOR_EFFECT));
						var battle_log = string_append_from_list(battlelog_pieces_list);
						battlelog_add(battle_log);
						ds_list_destroy(battlelog_pieces_list);
				   
				   }
			var skill_name = ds_map_find_next(char_map[? GAME_CHAR_BONUS_SKILLS_MAP],skill_name);
		}
		ds_map_destroy(ally_bonus_skill_map_copy);
	}
	
	
	
	
	
	
	
	
	
}












/*
/// Flirt auto learn
	if char_map[? GAME_CHAR_NAME] != NAME_ZEYD && 
	   char_map[? GAME_CHAR_NAME] != NAME_IMP_1 && 
	   char_map[? GAME_CHAR_NAME] != NAME_IMP_2
		ds_map_replace(char_map[? GAME_CHAR_SKILLS_MAP],SKILL_FLIRT,1);



/// Passive - thief
if char_map[? GAME_CHAR_NAME] == NAME_ZEYD
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_THIEF);

*/





// player show off skills
/*if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_HEAL);

}
if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_WRECK);

}
if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_HIDE);

}

if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_I_AM_THE_MAIN_CHARACTER);

}

if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_DOUBLE_HIT);

}

if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_FIREBALL);

}

if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_OCCULT);

}

if char_map == obj_control.game_map[? GAME_PLAYER]
{
	ds_list_add_unique(char_map[? GAME_CHAR_SKILLS_LIST],SKILL_THUNDERSTORM);

}*/





