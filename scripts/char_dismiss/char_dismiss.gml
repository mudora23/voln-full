///char_dismiss(ID);
/// @description
/// @param ID

var char = char_id_get_char(argument0);
if char[? GAME_CHAR_TYPE] == GAME_CHAR_ALLY ally_dismiss(argument0);
else if char[? GAME_CHAR_TYPE] == GAME_CHAR_PET pet_dismiss(argument0);