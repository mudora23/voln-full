///cancel_button_pressed_get();
/// @description
return cancel_button_keyboard_pressed_get() || cancel_button_mouse_pressed_get();