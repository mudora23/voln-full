///scene_sprite_list_change(ID,sprite_target,xnum_target,ynum_target,alpha_target,fade_speed,slide_speed,order,size_ratio);
/// @description  
/// @param ID
/// @param sprite_target
/// @param xnum_target
/// @param ynum_target
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
/// @param size_ratio

with obj_control
{
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	var sprite_index_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_NAME];
	var sprite_index_target_list = obj_control.var_map[? VAR_SPRITE_LIST_INDEX_TARGET_NAME];
	var sprite_x_list = obj_control.var_map[? VAR_SPRITE_LIST_X];
	var sprite_x_target_list = obj_control.var_map[? VAR_SPRITE_LIST_X_TARGET];
	var sprite_y_list = obj_control.var_map[? VAR_SPRITE_LIST_Y];
	var sprite_y_target_list = obj_control.var_map[? VAR_SPRITE_LIST_Y_TARGET];
	var sprite_alpha_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA];
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	var sprite_fade_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_FADE_SPEED];
	var sprite_slide_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_SLIDE_SPEED];
	var sprite_order_list = obj_control.var_map[? VAR_SPRITE_LIST_ORDER];
	var sprite_size_ratio_list = obj_control.var_map[? VAR_SPRITE_LIST_SIZE_RATIO];
	
	if ds_list_find_index(sprite_id_list,argument[0]) < 0
	{
		scene_sprite_list_add(argument[0],argument[1],argument[2],argument[2],argument[3],argument[3],argument[4],argument[4],argument[5],argument[6],argument[7],argument[8]);
	}
	else
	{
		var existing_index = ds_list_find_index(sprite_id_list,argument[0]);

		if sprite_exists(argument[1])
		{
			sprite_index_target_list[| existing_index] = sprite_get_name(argument[1]);
		}
		else
			sprite_index_target_list[| existing_index] = "";
		sprite_x_target_list[| existing_index] = xnum_to_x(argument[1],sprite_size_ratio_list[| existing_index],argument[2]);
		sprite_y_target_list[| existing_index] = ynum_to_y(argument[1],sprite_size_ratio_list[| existing_index],argument[3]);
		sprite_alpha_target_list[| existing_index] = argument[4];
		sprite_fade_speed_list[| existing_index] = argument[5];
		sprite_slide_speed_list[| existing_index] = argument[6];
		sprite_order_list[| existing_index] = argument[7];
		sprite_size_ratio_list[| existing_index] = argument[8];
	}

}

