///_label(label);
/// @description
/// @param label
scene_add_label(string_upper(argument0), obj_control.var_map[? VAR_SCRIPT_POSI_FLOATING]);

if scene_is_current()
{
	//show_debug_message("label reached... "+string(argument0));
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);