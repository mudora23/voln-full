///scriptvar_PC_Hair_DescA();
/// @description
if var_map_get(PC_Hair_Length_N) >= 1
{
    var num = var_map_get(PC_Hair_N);
    if num == 0 return "Silky";
    else if num == 1 return "wavy";
    else if num == 2 return "ringleted";
    else if num == 3 return "voluminous";
    else if num == 4 && var_map_get(PC_Hair_Length_N) < 3 return "tightly curled";
    else return "";
}
else return "";