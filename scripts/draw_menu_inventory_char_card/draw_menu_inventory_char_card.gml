///draw_menu_inventory_char_card(char,card_padding,card_x1,card_y1,card_x2,card_y2);
/// @description
/// @param char
/// @param card_padding
/// @param card_x1
/// @param card_y1
/// @param card_x2
/// @param card_y2
var char = argument[0];
var card_padding = argument[1];
var card_x1 = argument[2];
var card_y1 = argument[3];
var card_x2 = argument[4];
var card_y2 = argument[5];
var card_w = card_x2 - card_x1;
var card_h = card_y2 - card_y1;
var thumb_x1 = card_x1+card_padding;
var thumb_y1 = card_y1+card_padding;
var thumb_h = card_y2 - card_y1 - card_padding*2;
var thumb_w = thumb_h;
var thumb_x2 = card_x1+thumb_w;
var thumb_y2 = card_y1+thumb_h;
var card_hover = mouse_over_area(card_x1,card_y1,card_w,card_h);
if card_hover
	var card_alpha_adj = 1;
else
	var card_alpha_adj = inventory_using_item_alpha;
// Card BG
if inventory_using_item
	draw_set_color(c_dkgray);
else
	draw_set_color(c_almostblack);
draw_sprite_stretched_ext(spr_button_choice,2,card_x1,card_y1,card_w,card_h,draw_get_color(),draw_get_alpha()*card_alpha_adj);
draw_sprite_stretched_ext(spr_button_choice,3,card_x1,card_y1,card_w,card_h,draw_get_color(),draw_get_alpha()*card_alpha_adj);

draw_set_color(c_white);

// Card thumb
draw_sprite_in_area_stretch_to_fit(asset_get_index(char[? GAME_CHAR_THUMB]),0,thumb_x1,thumb_y1,thumb_w,thumb_h,c_white,draw_get_alpha()*card_alpha_adj);


// details
var xx = thumb_x2+card_padding;
var yy = thumb_y1;
//draw_set_font(font_general_15);
draw_set_alpha(draw_get_alpha()*card_alpha_adj);
draw_set_color(c_ltgray);
draw_text_transformed(xx,card_y1,char[? GAME_CHAR_DISPLAYNAME]+" (LV "+string(char[? GAME_CHAR_LEVEL])+")",0.65,0.65,0);
yy+=20;
//draw_set_font(font_general_10);
//draw_text(xx,yy,"Vitality: "+string(char[? GAME_CHAR_ENERGY])+" / "+string(char_get_energy_max(char)));
//yy+=15;
var bar_xx = thumb_x2+card_padding;
var bar_yy = yy;
var bar_width = card_w - thumb_w - card_padding*3;
var bar_height = 15;
var bar_value = char[? GAME_CHAR_VITALITY];
var bar_color = COLOR_VITALITY;
var bar_width_per_value = bar_width / char_get_vitality_max(char);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
yy+=18;

var bar_xx = thumb_x2+card_padding;
var bar_yy = yy;
var bar_width = card_w - thumb_w - card_padding*3;
var bar_value = char[? GAME_CHAR_HEALTH];
var bar_color = COLOR_HP;
var bar_width_per_value = bar_width / char_get_health_max(char);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
yy+=18;

var bar_yy = yy;
var bar_value = char[? GAME_CHAR_LUST];
var bar_color = COLOR_LUST;
var bar_width_per_value = bar_width / char_get_lust_max(char);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
yy+=18;

var bar_yy = yy;
var bar_value = char[? GAME_CHAR_TORU];
var bar_color = COLOR_TORU;
var bar_width_per_value = bar_width / char_get_toru_max(char);
draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_ltgray,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width,bar_height-1);
draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,draw_get_alpha()*card_alpha_adj,bar_xx,bar_yy,bar_width_per_value*bar_value,bar_height-1);
yy+=18;












// using items
if inventory_using_item && action_button_pressed_get() && mouse_over_area(card_x1,card_y1,card_w,card_h)
{
	CS_using(char,inventory_using_item_name);
	//show_popup_box("M pressed. Popup box testing testing testing testing testing testing testing testing testing testing");
	
	action_button_pressed_set(false);
	inventory_using_item = false;
	inventory_using_item_name = "";
	inventory_using_item_alpha = 1;
	inventory_using_item_alpha_increasing = false;
	
	surface_free(inventory_surface);
	inventory_surface = noone;
}
else if inventory_using_item && cancel_button_pressed_get()
{
	cancel_button_pressed_set(false);
	inventory_using_item = false;
	inventory_using_item_name = "";
	inventory_using_item_alpha = 1;
	inventory_using_item_alpha_increasing = false;
}















