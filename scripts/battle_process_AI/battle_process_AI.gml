///scene_script_battle_process_AI_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
	
	
	var action_char = battle_get_current_action_char();
	var opposite_side_list = battle_char_create_opposite_side_list(action_char);
	var same_side_list = battle_char_create_same_side_list(action_char);

		var lowest_hp_char_same_side = battle_char_list_get_lowest_hp_capable_char(same_side_list);
		var lowest_hp_ratio_same_side = lowest_hp_char_same_side[? GAME_CHAR_HEALTH] / char_get_health_max(lowest_hp_char_same_side);

		var capable_char_number_opposite_side = battle_char_list_get_combat_capable_number(opposite_side_list);
		var capable_char_number_same_side = battle_char_list_get_combat_capable_number(same_side_list);
		
		//var the_index = battle_clist_get_highest_lust_targetcapable_char_i(opposite_side_list);
		//if the_index < 0 var highest_lust_char_opposite_side = noone;
		//else var highest_lust_char_opposite_side = opposite_side_list[| the_index];
		var highest_lust_char_opposite_side = battle_clist_get_highest_effective_lust_ratio_char(opposite_side_list);
		
		//var the_index = battle_clist_get_lowest_hp_targetcapable_char_i(opposite_side_list);
		//if the_index < 0 var lowest_hp_char_opposite_side = noone;
		//else var lowest_hp_char_opposite_side = opposite_side_list[| the_index];
		var lowest_hp_char_opposite_side = battle_clist_get_lowest_effective_hp_ratio_char(opposite_side_list);
		
		var power_level_same_side = battle_char_list_get_capable_power_level(same_side_list);
		var power_level_opposite_side = battle_char_list_get_capable_power_level(opposite_side_list);
	

		var bodyguard_char_same_side = battle_char_list_get_bodyguard_char(same_side_list);
		var bodyguard_char_opposite_side = battle_char_list_get_bodyguard_char(opposite_side_list);
		
		
		
		
		
	var decision_score = 0; // 0 - 100 (100 = most wanted)
	var decision_script = noone;
	var decision_arg0 = 0;
	
	/*
	AI_AGGRESSIVE
	AI_SEDUCTIVE
	AI_SUPPORTIVE
	AI_CONSERVATIVE
	*/
	
	
	// PHY ATTACK
	if char_get_info(action_char,GAME_CHAR_AI) != AI_SEDUCTIVE
	{
		if action_char[? GAME_CHAR_TORU] > char_get_toru_max(action_char) * 0.5 && action_char[? GAME_CHAR_STAMINA] > char_get_stamina_max(action_char) * 0.4
			var current_score = 50;
		else if action_char[? GAME_CHAR_TORU] > char_get_toru_max(action_char) * 0.4 && action_char[? GAME_CHAR_STAMINA] > char_get_stamina_max(action_char) * 0.3
			var current_score = 55;
		else
			var current_score = 60;


		if current_score > decision_score
		{
			decision_score = current_score;

			if char_get_INT(action_char)/(char_get_INT(action_char)+char_get_STR(action_char)) && char_get_info(action_char,GAME_CHAR_TORU) >= 10
				decision_script = battle_process_toru_atk_CMD;
			else
				decision_script = battle_process_phy_atk_CMD;
			
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(lowest_hp_char_opposite_side);
		}

	}	
	
	// FLIRT
	if char_get_info(action_char,GAME_CHAR_AI) == AI_SEDUCTIVE
	{
		var current_score = 50;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_flirt_atk_CMD;
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(highest_lust_char_opposite_side);
		}
		
	}
	
	// SKILL_HEAL
	if skill_get_cost_enough(action_char,SKILL_HEAL,char_get_skill_level(action_char,SKILL_HEAL)) &&
	   lowest_hp_ratio_same_side < 0.8
	{
		
		if power_level_opposite_side > power_level_same_side * 2.0 var current_score = 50;
		else if power_level_opposite_side > power_level_same_side * 1.8 var current_score = 40;
		else if power_level_opposite_side > power_level_same_side * 1.6 var current_score = 30;
		else if power_level_opposite_side > power_level_same_side * 1.4 var current_score = 20;
		else if power_level_opposite_side > power_level_same_side * 1.2 var current_score = 10;
		else var current_score = 0;
		
		if lowest_hp_ratio_same_side < 0.3 current_score += 120;
		else current_score += (0.7 - lowest_hp_ratio_same_side)*100;

		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score+= 20;
		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 20;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_heal_CMD;
			decision_arg0 = char_get_char_id(lowest_hp_char_same_side);
		}
	}
	
	// SKILL_CLEANSE
	if skill_get_cost_enough(action_char,SKILL_CLEANSE,char_get_skill_level(action_char,SKILL_CLEANSE))
	{
		
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE bonus_score+=20;
		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE bonus_score-= 10;
		
		for(var kk = 0; kk < ds_list_size(same_side_list);kk++)
		{
			var target_char = same_side_list[| kk];
			var debuff_turns_total = ds_list_get_sum_of_numbers(target_char[? GAME_CHAR_DEBUFF_TURN_LIST]);
			
			if debuff_turns_total > 0
			{
				var current_score = 40 + bonus_score + debuff_turns_total*20;
				if current_score > decision_score
				{
					decision_score = current_score;
					decision_script = battle_process_cleanse_CMD;
					decision_arg0 = char_get_char_id(target_char);
				}
			}
		}
	}
	
	
	// SKILL_SPEED_UP
	if skill_get_cost_enough(action_char,SKILL_SPEED_UP,char_get_skill_level(action_char,SKILL_SPEED_UP))
	{
		
		if power_level_opposite_side > power_level_same_side * 2.0 var bonus_score = 30;
		else if power_level_opposite_side > power_level_same_side * 1.8 var bonus_score = 25;
		else if power_level_opposite_side > power_level_same_side * 1.6 var bonus_score = 20;
		else if power_level_opposite_side > power_level_same_side * 1.4 var bonus_score = 15;
		else if power_level_opposite_side > power_level_same_side * 1.2 var bonus_score = 10;
		else var bonus_score = 0;
		
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE bonus_score+=20;
		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE bonus_score-= 10;
		
		if char_get_skill_level(action_char,SKILL_SPEED_UP) >= 3 bonus_score+=10;
		else if char_get_skill_level(action_char,SKILL_SPEED_UP) == 2 bonus_score+=5;
		
		for(var kk = 0; kk < ds_list_size(same_side_list);kk++)
		{
			var target_char = same_side_list[| kk];
			if battle_char_get_buff_level(target_char,BUFF_SPEED_UP) == 0
			{
				var current_score = bonus_score + (target_char[? GAME_CHAR_LEVEL] - action_char[? GAME_CHAR_LEVEL])*10;
				if target_char[? GAME_CHAR_HEALTH] < char_get_health_max(target_char)*0.1 current_score += 20;
				else if target_char[? GAME_CHAR_HEALTH] < char_get_health_max(target_char)*0.2 current_score += 10;
				else if target_char[? GAME_CHAR_HEALTH] > char_get_health_max(target_char)*0.8 current_score -= 10;
				if (char_get_STR(target_char)+char_get_INT(target_char))/2 > (char_get_FOU(target_char)+char_get_END(target_char))/2
					current_score += 10;
				else
					current_score -= 10;
				if current_score > decision_score
				{
					decision_score = current_score;
					decision_script = battle_process_speed_up_CMD;
					decision_arg0 = char_get_char_id(target_char);
				}
			}
		}
	}
	
	
	// SKILL_DEFENSE_UP
	if skill_get_cost_enough(action_char,SKILL_DEFENSE_UP,char_get_skill_level(action_char,SKILL_DEFENSE_UP))
	{
		
		if power_level_opposite_side > power_level_same_side * 2.0 var bonus_score = 30;
		else if power_level_opposite_side > power_level_same_side * 1.8 var bonus_score = 25;
		else if power_level_opposite_side > power_level_same_side * 1.6 var bonus_score = 20;
		else if power_level_opposite_side > power_level_same_side * 1.4 var bonus_score = 15;
		else if power_level_opposite_side > power_level_same_side * 1.2 var bonus_score = 10;
		else var bonus_score = 0;
		
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE bonus_score+=20;
		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE bonus_score-= 10;
		
		if char_get_skill_level(action_char,SKILL_DEFENSE_UP) >= 3 bonus_score+=10;
		else if char_get_skill_level(action_char,SKILL_DEFENSE_UP) == 2 bonus_score+=5;
		
		for(var kk = 0; kk < ds_list_size(same_side_list);kk++)
		{
			var target_char = same_side_list[| kk];
			if battle_char_get_buff_level(target_char,BUFF_PHY_ARMOR_UP2) == 0 && battle_char_get_buff_level(target_char,BUFF_TORU_ARMOR_UP2) == 0
			{
				var current_score = bonus_score + (target_char[? GAME_CHAR_LEVEL] - action_char[? GAME_CHAR_LEVEL])*10;
				if target_char[? GAME_CHAR_HEALTH] < char_get_health_max(target_char)*0.1 current_score -= 20;
				else if target_char[? GAME_CHAR_HEALTH] < char_get_health_max(target_char)*0.2 current_score -= 10;
				else if target_char[? GAME_CHAR_HEALTH] > char_get_health_max(target_char)*0.8 current_score += 10;
				if current_score > decision_score
				{
					decision_score = current_score;
					decision_script = battle_process_defense_up_CMD;
					decision_arg0 = char_get_char_id(target_char);
				}
			}
		}
	}

	// SKILL_BODYGUARD
	if skill_get_cost_enough(action_char,SKILL_BODYGUARD,char_get_skill_level(action_char,SKILL_BODYGUARD)) &&
		!battle_char_get_buff_level(action_char,SKILL_BODYGUARD) &&
		capable_char_number_same_side >= 2
	{
		var current_score = 50;
		if (char_get_FOU(action_char) + char_get_END(action_char))/2 > (char_get_STR(action_char) + char_get_INT(action_char)+ char_get_WIS(action_char))/3 * 1.5 current_score += 30;
		else if (char_get_FOU(action_char) + char_get_END(action_char))/2 > (char_get_STR(action_char) + char_get_INT(action_char)+ char_get_WIS(action_char))/3 * 1.2 current_score += 15;
		else if (char_get_FOU(action_char) + char_get_END(action_char))/2 > (char_get_STR(action_char) + char_get_INT(action_char)+ char_get_WIS(action_char))/3 current_score += 0;
		else if (char_get_FOU(action_char) + char_get_END(action_char))/2 > (char_get_STR(action_char) + char_get_INT(action_char)+ char_get_WIS(action_char))/3 * 0.8 current_score -= 15;
		else current_score -= 30;
		
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score+= 20;
		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 10;
		
		if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.8 current_score += 20;
		else if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.7 current_score += 10;
		else if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.6 current_score += 0;
		else if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.5 current_score -= 10;
		else if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.4 current_score -= 30;
		else if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.3 current_score -= 50;
		else if action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char) > 0.2 current_score -= 70;
		else current_score -= 100;
		
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_bodyguard_CMD;
			decision_arg0 = ARG_NOT_EXISTS;
		}
	}
	
	// SKILL_BARRIER
	if skill_get_cost_enough(action_char,SKILL_BARRIER,char_get_skill_level(action_char,SKILL_BARRIER)) &&
		!battle_char_get_buff_level(action_char,BUFF_PHY_ARMOR_UP) &&
		!battle_char_get_buff_level(action_char,BUFF_TORU_ARMOR_UP) && 
		capable_char_number_same_side >= 3
	{

		
		if power_level_opposite_side > power_level_same_side * 2.0 var current_score = 50;
		else if power_level_opposite_side > power_level_same_side * 1.8 var current_score = 40;
		else if power_level_opposite_side > power_level_same_side * 1.6 var current_score = 30;
		else if power_level_opposite_side > power_level_same_side * 1.4 var current_score = 20;
		else if power_level_opposite_side > power_level_same_side * 1.2 var current_score = 10;
		else var current_score = 0;
		
		if capable_char_number_same_side == 3 current_score += 10;
		else if capable_char_number_same_side == 4 current_score += 30;
		else if capable_char_number_same_side == 5 current_score += 50;
		
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score+= 20;
		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 20;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_barrier_CMD;
			decision_arg0 = ARG_NOT_EXISTS;
		}
	}

	// SKILL_HIDE
	if skill_get_cost_enough(action_char,SKILL_HIDE,char_get_skill_level(action_char,SKILL_HIDE)) &&
	   !battle_char_get_buff_level(action_char,BUFF_HIDE)
	{
		var hp_ratio_self = action_char[? GAME_CHAR_HEALTH] / char_get_health_max(action_char);
		if hp_ratio_self < 0.3 var current_score = 110;
		else var current_score = 0.75 - hp_ratio_self;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 10;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_hide_CMD;
			decision_arg0 = ARG_NOT_EXISTS;
		}
	}

	// SKILL_ENDLESS_WHIP
	if skill_get_cost_enough(action_char,SKILL_ENDLESS_WHIP,char_get_skill_level(action_char,SKILL_ENDLESS_WHIP))
	{
		var current_score = 70;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 100;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_endless_whip_CMD;
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(lowest_hp_char_opposite_side);
		}
	}
	
	// SKILL_OCCULT
	if skill_get_cost_enough(action_char,SKILL_OCCULT,char_get_skill_level(action_char,SKILL_OCCULT)) &&
	   capable_char_number_opposite_side >= 3
	{
		if capable_char_number_opposite_side == 3 var current_score = 65;
		else if capable_char_number_opposite_side == 4 var current_score = 75;
		else if capable_char_number_opposite_side == 5 var current_score = 85;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score+= 10;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_occult_CMD;
			decision_arg0 = ARG_NOT_EXISTS;
		}
	}
	
	// SKILL_THUNDERSTORM
	if skill_get_cost_enough(action_char,SKILL_THUNDERSTORM,char_get_skill_level(action_char,SKILL_THUNDERSTORM)) &&
	   capable_char_number_opposite_side >= 3
	{
		if capable_char_number_opposite_side == 3 var current_score = 70;
		else if capable_char_number_opposite_side == 4 var current_score = 80;
		else if capable_char_number_opposite_side == 5 var current_score = 90;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 50;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_thunderstorm_CMD;
			decision_arg0 = ARG_NOT_EXISTS;
		}
	}

	// SKILL_BLIZZARD
	if skill_get_cost_enough(action_char,SKILL_BLIZZARD,char_get_skill_level(action_char,SKILL_BLIZZARD)) &&
	   capable_char_number_opposite_side >= 3
	{
		if capable_char_number_opposite_side == 3 var current_score = 70;
		else if capable_char_number_opposite_side == 4 var current_score = 80;
		else if capable_char_number_opposite_side == 5 var current_score = 90;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 50;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_thunderstorm_CMD;
			decision_arg0 = ARG_NOT_EXISTS;
		}
	}
	
	// SKILL_FIREBALL
	if skill_get_cost_enough(action_char,SKILL_FIREBALL,char_get_skill_level(action_char,SKILL_FIREBALL))
	{
		var current_score = 70;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 100;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_fireball_CMD;
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(lowest_hp_char_opposite_side);
		}
	}

	// SKILL_DOUBLE_HIT
	if skill_get_cost_enough(action_char,SKILL_DOUBLE_HIT,char_get_skill_level(action_char,SKILL_DOUBLE_HIT))
	{
		var current_score = 70;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 100;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_double_hit_CMD;
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(lowest_hp_char_opposite_side);
		}
	}
	
	// SKILL_TACKLE
	if skill_get_cost_enough(action_char,SKILL_TACKLE,char_get_skill_level(action_char,SKILL_TACKLE))
	{
		var bonus_score = 0;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE bonus_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE bonus_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE bonus_score-= 100;
		
		if char_get_skill_level(action_char,SKILL_TACKLE) >= 3 bonus_score+=10;
		else if char_get_skill_level(action_char,SKILL_TACKLE) == 2 bonus_score+=5;
		
		for(var kk = 0; kk < ds_list_size(opposite_side_list);kk++)
		{
			var target_char = opposite_side_list[| kk];
			
			if bodyguard_char_opposite_side != noone || bodyguard_char_opposite_side == target_char
			{
				if battle_char_get_buff_level(target_char,DEBUFF_STUN) == 0
				{
				
					var current_score = bonus_score;
			
					if char_get_hit_chance(action_char) - char_get_resistance_chance(target_char) > 0.5 current_score += 30;
					else if char_get_hit_chance(action_char) - char_get_resistance_chance(target_char) > 0.2 current_score += 20;
					else if char_get_hit_chance(action_char) - char_get_resistance_chance(target_char) > 0 current_score += 10;
					else if char_get_hit_chance(action_char) - char_get_resistance_chance(target_char) > -0.1 current_score += 0;
					else if char_get_hit_chance(action_char) - char_get_resistance_chance(target_char) > -0.2 current_score -= 10;
					else if char_get_hit_chance(action_char) - char_get_resistance_chance(target_char) > -0.3 current_score -= 20;
					else current_score -= 30;

					if current_score > decision_score
					{
						decision_score = current_score;
						decision_script = battle_process_tackle_CMD;
						decision_arg0 = char_get_char_id(target_char);
					}
				}
			}

		}
	}

	// SKILL_BAM2
	if skill_get_cost_enough(action_char,SKILL_BAM2,char_get_skill_level(action_char,SKILL_BAM2))
	{
		var current_score = 65;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 100;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_bam2_CMD;
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(lowest_hp_char_opposite_side);
		}
	}
	
	// SKILL_BAM
	if skill_get_cost_enough(action_char,SKILL_BAM,char_get_skill_level(action_char,SKILL_BAM))
	{
		var current_score = 60;

		if action_char[? GAME_CHAR_AI] == AI_CONSERVATIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SUPPORTIVE current_score-= 30;
		if action_char[? GAME_CHAR_AI] == AI_SEDUCTIVE current_score-= 100;
		
		if current_score > decision_score
		{
			decision_score = current_score;
			decision_script = battle_process_bam_CMD;
			if bodyguard_char_opposite_side != noone decision_arg0 = char_get_char_id(bodyguard_char_opposite_side);
			else decision_arg0 = char_get_char_id(lowest_hp_char_opposite_side);
		}
	}	

	if decision_script != noone
	{
		//script_execute_add(0,decision_script,decision_arg0);
		script_execute_ext(decision_script,decision_arg0);
	}
	else
		battle_process_wait_CMD();


		ds_list_destroy(opposite_side_list);
		ds_list_destroy(same_side_list);



	
}