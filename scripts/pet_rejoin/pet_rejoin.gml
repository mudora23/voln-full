///pet_rejoin(ID/Name);
/// @description
/// @param ID/Name
with obj_control
{
	if !pet_is_full()
	{
		if !is_string(argument0) var char_map = char_id_get_char(argument0);
		else var char_map = pet_r_get_char(argument0);
	    var pet_list = var_map[? VAR_PET_LIST];
	    var pet_r_list = var_map[? VAR_PET_R_LIST];
	    var pet_r_index = ds_list_find_index(pet_r_list,char_map);
	    if pet_r_index>=0
	    {
			var new_char = ds_map_create();
			ds_map_copy(new_char,pet_r_list[| pet_r_index]);
	        ds_list_add(pet_list,new_char);

	        ds_list_mark_as_map(pet_list,ds_list_size(pet_list)-1);
	        ds_list_delete(pet_r_list,pet_r_index);
	    }
	}
	else
	{
		if !is_string(argument0) var char_map = char_id_get_char(argument0);
		else var char_map = pet_r_get_char(argument0);
	    var storage_list = var_map[? VAR_STORAGE_LIST];
	    var pet_r_list = var_map[? VAR_PET_R_LIST];
	    var pet_r_index = ds_list_find_index(pet_r_list,char_map);
	    if pet_r_index>=0
	    {

			var new_char = ds_map_create();
			ds_map_copy(new_char,pet_r_list[| pet_r_index]);
	        ds_list_add(storage_list,new_char);
			
			
	        ds_list_mark_as_map(storage_list,ds_list_size(storage_list)-1);
	        ds_list_delete(pet_r_list,pet_r_index);
	    }	
		
	}
}
