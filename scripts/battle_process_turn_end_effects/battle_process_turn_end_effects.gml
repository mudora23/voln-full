/// scene_script_battle_process_turn_end_effects();
/// @description

var char_map = battle_get_current_action_char();
var buff_list = char_map[? GAME_CHAR_BUFF_LIST];
var buff_turn_list = char_map[? GAME_CHAR_BUFF_TURN_LIST];
var buff_level_list = char_map[? GAME_CHAR_BUFF_LEVEL_LIST];
var buff_source_char_id_list = char_map[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST];
var debuff_list = char_map[? GAME_CHAR_DEBUFF_LIST];
var debuff_turn_list = char_map[? GAME_CHAR_DEBUFF_TURN_LIST];
var debuff_level_list = char_map[? GAME_CHAR_DEBUFF_LEVEL_LIST];
var debuff_source_char_id_list = char_map[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST];











// remove effects if worn off
for(var i = 0;i<ds_list_size(debuff_list);i++)
{
	debuff_turn_list[| i]-=0.5;
	if debuff_turn_list[| i] <= 0
	{
		ds_list_delete(debuff_list,i);
		ds_list_delete(debuff_turn_list,i);
		ds_list_delete(debuff_level_list,i);
		ds_list_delete(debuff_source_char_id_list,i);
		i--;
	}
}
for(var i = 0;i<ds_list_size(buff_list);i++)
{
	buff_turn_list[| i]-=0.5;
	if buff_turn_list[| i] <= 0
	{
		ds_list_delete(buff_list,i);
		ds_list_delete(buff_turn_list,i);
		ds_list_delete(buff_level_list,i);
		ds_list_delete(buff_source_char_id_list,i);
		i--;
	}
}




script_execute_add_unique(0,battle_process_die);