///ds_map_delete_duplicate(target_map,source_map);
/// @description
/// @param target_map
/// @param source_map

var temp_list = ds_list_create();

var key = ds_map_find_first(argument0);
while !is_undefined(key)
{
	if ds_map_exists(argument1,key) ds_list_add(temp_list,key);
	var key = ds_map_find_next(argument0,key);
}
for(var i = 0; i < ds_list_size(temp_list);i++)
	ds_map_delete(argument0,temp_list[| i]);

ds_list_destroy(temp_list);