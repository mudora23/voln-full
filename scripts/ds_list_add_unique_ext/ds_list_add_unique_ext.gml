///ds_list_add_unique_ext(id,value,add_times,max_times);
/// @description - add the value to the list if its not X times in the list
/// @param id
/// @param value
/// @param add_times
/// @param max_times
repeat(argument[2])
{
	if ds_list_value_exists_count(argument[0],argument[1]) < argument[3]
		ds_list_add(argument[0],argument[1]);
}
