///scene_sprite_list_size();
/// @description  

with obj_control
{
	return ds_list_size(obj_control.var_map[? VAR_SPRITE_LIST_ID]);

}