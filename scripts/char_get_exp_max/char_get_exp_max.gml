///char_get_exp_max(char);
/// @description
/// @param char
var char = argument[0];
return 10 + power(char[? GAME_CHAR_LEVEL],1.2);
