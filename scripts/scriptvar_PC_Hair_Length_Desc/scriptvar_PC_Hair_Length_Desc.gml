///scriptvar_PC_Hair_Length_Desc();
/// @description
var num = var_map_get(PC_Hair_Length_N);
if num == -1 return "bald";
else if num == 0 return "shaved";
else if num == 1 return "close-cropped";
else if num == 2 return "short";
else if num == 3 && var_map_get(PC_Hair_N) == 4 return "puffy";
else if num == 3 return "shaggy";
else if num == 4 return "longish";
else return "long";