///char_set_char_opinion(whos_opinion_name,opinion_about_whom_map,value);
/// @description
/// @param whos_opinion_name
/// @param opinion_about_whom_map
/// @param value

var whos_opinion_name = argument0;
var opinion_about_whom_map = argument1;
var value = argument2;

var opinion_map = opinion_about_whom_map[? GAME_CHAR_OPINION_MAP];

opinion_map[? whos_opinion_name] = value;