///draw_sprite_in_area_stretch_to_fit_in_visible_area(spr,img,x,y,w,h,color,alpha,area_x,area_y,area_w,area_h);
/// @description
/// @param spr
/// @param img
/// @param x
/// @param y
/// @param w
/// @param h
/// @param color
/// @param alpha
/// @param area_x
/// @param area_y
/// @param area_w
/// @param area_h

var spr = argument0;
var img = argument1;
var xx = argument2;
var yy = argument3;
var w = argument4;
var h = argument5;
var color = argument6;
var alpha = argument7;
var area_x = argument8;
var area_y = argument9;
var area_w = argument10;
var area_h = argument11;

var xratio = w / sprite_get_width(spr);
var yratio = h / sprite_get_height(spr);

draw_sprite_in_visible_area(spr,img,xx,yy,xratio,yratio,color,alpha,area_x,area_y,area_w,area_h);
