///inventory_item_count(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	var name_list = obj_control.var_map[? VAR_INVENTORY_LIST];
	var amount_list = obj_control.var_map[? VAR_INVENTORY_AMOUNT_LIST];
	var exist_index = ds_list_find_index(name_list,name);
	
	if exist_index >= 0
	{
		return amount_list[| exist_index];
	}
	return 0;
}