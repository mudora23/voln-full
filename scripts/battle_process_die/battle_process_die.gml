/// scene_script_battle_process_die();
/// @description
//scene_clear_ext(false,false,true,true,true);
	scene_choices_list_clear();

// check if char in the order list should be out of combat, remove them
var order_list = obj_control.var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST];
for(var i = 0; i < ds_list_size(order_list);i++)
{
	var char = order_list[| i];
	if char[? GAME_CHAR_HEALTH] <= 0 || char[? GAME_CHAR_LUST] >= char_get_lust_max(char)
	{
		if char[? GAME_CHAR_OWNER] == OWNER_PLAYER || char[? GAME_CHAR_OWNER] == OWNER_ALLY
			var char_source_color_tag = 7;
		else
			var char_source_color_tag = 5;
			
		// messages
		if char[? GAME_CHAR_HEALTH] <= 0
		{
			var battlelog_pieces_list = ds_list_create();
			ds_list_add(battlelog_pieces_list,text_add_markup(char[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
			ds_list_add(battlelog_pieces_list,text_add_markup(" falls",TAG_FONT_N,TAG_COLOR_DAMAGE));
			ds_list_add(battlelog_pieces_list,text_add_markup(", exhausted!",TAG_FONT_N,0));
			
			var battle_log = string_append_from_list(battlelog_pieces_list);
			battlelog_add(battle_log);
			ds_list_destroy(battlelog_pieces_list);		
		}
		if char[? GAME_CHAR_LUST] >= char_get_lust_max(char)
		{
			var battlelog_pieces_list = ds_list_create();
			ds_list_add(battlelog_pieces_list,text_add_markup(char[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
			if percent_chance(50)
			{
				ds_list_add(battlelog_pieces_list,text_add_markup(" whimpers,",TAG_FONT_N,TAG_COLOR_NORMAL));
				ds_list_add(battlelog_pieces_list,text_add_markup(" too aroused",TAG_FONT_N,TAG_COLOR_DAMAGE));
				ds_list_add(battlelog_pieces_list,text_add_markup(" to keep fighting!",TAG_FONT_N,TAG_COLOR_NORMAL));
			}
			else
			{
				ds_list_add(battlelog_pieces_list,text_add_markup(" is",TAG_FONT_N,TAG_COLOR_NORMAL));
				ds_list_add(battlelog_pieces_list,text_add_markup(" too horny",TAG_FONT_N,TAG_COLOR_FLIRT));
				ds_list_add(battlelog_pieces_list,text_add_markup(" to attack!",TAG_FONT_N,TAG_COLOR_NORMAL));
			}
			
			var battle_log = string_append_from_list(battlelog_pieces_list);
			battlelog_add(battle_log);
			ds_list_destroy(battlelog_pieces_list);		
		}



		// fade to dark
		char[? GAME_CHAR_COLOR_TARGET] = c_0hp;
		
		// remove all buff / debuff effects
		ds_list_clear(char[? GAME_CHAR_BUFF_LIST]);
		ds_list_clear(char[? GAME_CHAR_BUFF_TURN_LIST]);
		ds_list_clear(char[? GAME_CHAR_BUFF_LEVEL_LIST]);
		ds_list_clear(char[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST]);
		ds_list_clear(char[? GAME_CHAR_DEBUFF_LIST]);
		ds_list_clear(char[? GAME_CHAR_DEBUFF_TURN_LIST]);
		ds_list_clear(char[? GAME_CHAR_DEBUFF_LEVEL_LIST]);
		ds_list_clear(char[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST]);

		
		
		ds_list_delete(order_list,i);
		i--;
	}
}








// check if all enemies died
var enemy_alive = false;
var enemy_list = var_map[? VAR_ENEMY_LIST];
var enemy_hp0 = 0;
var enemy_lustmax = 0;
for(var i = 0; i < ds_list_size(enemy_list);i++)
{
	var enemy = enemy_list[| i];
	if enemy[? GAME_CHAR_HEALTH] > 0 && enemy[? GAME_CHAR_LUST] < char_get_lust_max(enemy)
	{
		enemy_alive = true;
		break;
	}
	else if enemy[? GAME_CHAR_HEALTH] <= 0
		enemy_hp0++;
	else
		enemy_lustmax++;
}

// check if all allies and player died
var ally_alive = false;
var ally_list = var_map[? VAR_ALLY_LIST];
var ally_hp0 = 0;
var ally_lustmax = 0;
for(var i = 0; i < ds_list_size(ally_list);i++)
{
	var ally = ally_list[| i];
	if ally[? GAME_CHAR_HEALTH] > 0 && ally[? GAME_CHAR_LUST] < char_get_lust_max(ally)
	{
		ally_alive = true;
		break;
	}
	else if ally[? GAME_CHAR_HEALTH] <= 0
		ally_hp0++;
	else
		ally_lustmax++;
}

var player_alive = false;
var player = var_map[? VAR_PLAYER];
var player_hp0 = 0;
var player_lustmax = 0;
if player[? GAME_CHAR_HEALTH] > 0 && player[? GAME_CHAR_LUST] < char_get_lust_max(player)
{
	player_alive = true;
}
else if player[? GAME_CHAR_HEALTH] <= 0
	player_hp0++;
else
	player_lustmax++;


// game end type
var game_end_type = "";
if !enemy_alive
{
	if enemy_hp0 >= enemy_lustmax game_end_type = GAME_DOMINANT_VICTORY;
	else game_end_type = GAME_SEDUCTIVE_VICTORY;

	// item drop
	var gold_gain = 0;
	var salo_wurm_gain = 0;
	var enemy_list = var_map[? VAR_ENEMY_LIST];
	
	for(var i = 0; i < ds_list_size(enemy_list);i++)
	{
		var enemy = enemy_list[| i];
		if enemy[? GAME_CHAR_GOLD_DROP] > 0
			gold_gain += (enemy[? GAME_CHAR_GOLD_DROP] + enemy[? GAME_CHAR_LEVEL]*5)*random_range(0.8,1.2);
		
		// quest - S_0004_FELGOR_INTRO_EVENT2 
		if qj_exists("S_0004_FELGOR_INTRO_EVENT2_6_A_2_QJ") &&
		   inventory_item_count(ITEM_Salo_wurm) < 5 &&
		   (enemy[? GAME_CHAR_NAME] == NAME_IMP_1 || enemy[? GAME_CHAR_NAME] == NAME_IMP_2)
		   {
				inventory_add_item(ITEM_Salo_wurm,1);
				salo_wurm_gain++;
		   }
		   

	}
	if game_end_type == GAME_SEDUCTIVE_VICTORY
		gold_gain *= 0.5;
	gold_gain = round(gold_gain);
	
	
	obj_control.var_map[? VAR_GOLD]+=gold_gain;
	if gold_gain > 0 voln_play_sfx(sfx_get_gold);
	
	
	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(string(gold_gain),TAG_FONT_N,TAG_COLOR_EFFECT));
	ds_list_add(battlelog_pieces_list,text_add_markup(" gold is obtained.",TAG_FONT_N,TAG_COLOR_NORMAL));

	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);
	
	if salo_wurm_gain > 0
	{
		var battlelog_pieces_list = ds_list_create();
		ds_list_add(battlelog_pieces_list,text_add_markup(string(salo_wurm_gain),TAG_FONT_N,TAG_COLOR_EFFECT));
		if salo_wurm_gain == 1
			ds_list_add(battlelog_pieces_list,text_add_markup(" salo wurm is obtained.",TAG_FONT_N,TAG_COLOR_NORMAL));
		else
			ds_list_add(battlelog_pieces_list,text_add_markup(" salo wurms are obtained.",TAG_FONT_N,TAG_COLOR_NORMAL));

		var battle_log = string_append_from_list(battlelog_pieces_list);
		battlelog_add(battle_log);
		ds_list_destroy(battlelog_pieces_list);
	}

	// gain EXP
	var enemy_average_level = battle_char_list_get_average_level(enemy_list);
	var player_ally_average_level = battle_char_list_get_average_level_ext(ally_list,player);
	var enemy_exp_worth = battle_char_level_get_exp_worth(enemy_average_level);

	for(var i = 0; i < ds_list_size(ally_list);i++)
	{
		var ally = ally_list[| i];
		
		var enemy_exp_modifier = battle_char_level_get_exp_modifier(ally[? GAME_CHAR_LEVEL],enemy_average_level);
		var exp_gain = enemy_exp_worth * enemy_exp_modifier;
		var real_exp_gain = exp_gain * enemy_get_number() / (ally_get_number()+1);
		
		scene_char_exp_gain(ally,real_exp_gain);
	}

	var enemy_exp_modifier = battle_char_level_get_exp_modifier(player[? GAME_CHAR_LEVEL],enemy_average_level);
	var exp_gain = enemy_exp_worth * enemy_exp_modifier;
	var real_exp_gain = exp_gain * enemy_get_number() / (ally_get_number()+1);
	
	scene_char_exp_gain(player,real_exp_gain);
	

	
}
if (!player_alive && !ally_alive)
{
	if ally_hp0+player_hp0 >= ally_lustmax+player_lustmax game_end_type = GAME_DOMINANT_DEFEAT;
	else game_end_type = GAME_SEDUCTIVE_DEFEAT;
	
	
	// gain EXP
	var enemy_average_level = battle_char_list_get_average_level(enemy_list);
	var player_ally_average_level = battle_char_list_get_average_level_ext(ally_list,player);
	var enemy_exp_worth = battle_char_level_get_exp_worth(enemy_average_level);

	for(var i = 0; i < ds_list_size(ally_list);i++)
	{
		var ally = ally_list[| i];
		
		var enemy_exp_modifier = battle_char_level_get_exp_modifier(ally[? GAME_CHAR_LEVEL],enemy_average_level);
		var exp_gain = enemy_exp_worth * enemy_exp_modifier*0.3;
		var real_exp_gain = exp_gain * enemy_get_number() / (ally_get_number()+1);
		
		scene_char_exp_gain(ally,real_exp_gain);
	}

	var enemy_exp_modifier = battle_char_level_get_exp_modifier(player[? GAME_CHAR_LEVEL],enemy_average_level);
	var exp_gain = enemy_exp_worth * enemy_exp_modifier*0.3;
	var real_exp_gain = exp_gain * enemy_get_number() / (ally_get_number()+1);
	
	scene_char_exp_gain(player,real_exp_gain);
	
}

if !enemy_alive || (!player_alive && !ally_alive)
{
	
	// remove effects and recoer stamina
	for(var i = 0; i < ds_list_size(ally_list);i++)
	{
		var ally = ally_list[| i];
		ds_list_clear(ally[? GAME_CHAR_DEBUFF_LIST]);
		ds_list_clear(ally[? GAME_CHAR_DEBUFF_TURN_LIST]);
		ds_list_clear(ally[? GAME_CHAR_DEBUFF_LEVEL_LIST]);
		ds_list_clear(ally[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST]);
		ds_list_clear(ally[? GAME_CHAR_BUFF_LIST]);
		ds_list_clear(ally[? GAME_CHAR_BUFF_TURN_LIST]);
		ds_list_clear(ally[? GAME_CHAR_BUFF_LEVEL_LIST]);
		ds_list_clear(ally[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST]);
		ally[? GAME_CHAR_STAMINA] = min(ally[? GAME_CHAR_STAMINA] + char_get_stamina_regen_per_turn(ally), char_get_stamina_max(ally));
	}
	for(var i = 0; i < ds_list_size(enemy_list);i++)
	{
		var enemy = enemy_list[| i];
		ds_list_clear(enemy[? GAME_CHAR_DEBUFF_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_DEBUFF_TURN_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_DEBUFF_LEVEL_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_BUFF_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_BUFF_TURN_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_BUFF_LEVEL_LIST]);
		ds_list_clear(enemy[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST]);
		enemy[? GAME_CHAR_STAMINA] = min(enemy[? GAME_CHAR_STAMINA] + char_get_stamina_regen_per_turn(enemy), char_get_stamina_max(enemy));
	}
	var player = var_map[? VAR_PLAYER];
		ds_list_clear(player[? GAME_CHAR_DEBUFF_LIST]);
		ds_list_clear(player[? GAME_CHAR_DEBUFF_TURN_LIST]);
		ds_list_clear(player[? GAME_CHAR_DEBUFF_LEVEL_LIST]);
		ds_list_clear(player[? GAME_CHAR_DEBUFF_SOURCE_CHAR_ID_LIST]);
		ds_list_clear(player[? GAME_CHAR_BUFF_LIST]);
		ds_list_clear(player[? GAME_CHAR_BUFF_TURN_LIST]);
		ds_list_clear(player[? GAME_CHAR_BUFF_LEVEL_LIST]);
		ds_list_clear(player[? GAME_CHAR_BUFF_SOURCE_CHAR_ID_LIST]);
	player[? GAME_CHAR_STAMINA] = min(player[? GAME_CHAR_STAMINA] + char_get_stamina_regen_per_turn(player), char_get_stamina_max(player));

	var_map[? VAR_CHAR_BATTLE_ACTION_CHAR_MAP_POINTER] = noone;
	
	//show_debug_message("game_end_type: "+game_end_type);
	
	
	
	
	
	var_map_set(VAR_SCRIPT_DIALOG,"");
	var_map_set(VAR_SCRIPT_DIALOG_NUMBER,0);
	var_map_set(BATTLE_END_TYPE,game_end_type);
	

	var_map_set(VAR_TEMP_SCENE,"");
	var_map_set(VAR_TEMP_SCENE_TYPE,"");
	ds_list_clear(var_map_get(VAR_TEMP_SCENE_LIST));
	
	
	if (game_end_type == GAME_SEDUCTIVE_DEFEAT || game_end_type == GAME_DOMINANT_DEFEAT) && var_map[? BATTLE_LOSS_SCENE] != ""
	{
		var_map_set(VAR_MODE,VAR_MODE_NORMAL);
		scene_jump(var_map[? BATTLE_LOSS_SCENE_LABEL],var_map[? BATTLE_LOSS_SCENE]);
	}
	else if (game_end_type == GAME_SEDUCTIVE_VICTORY || game_end_type == GAME_DOMINANT_VICTORY) && var_map[? BATTLE_WIN_SCENE] != ""
	{
		var_map_set(VAR_MODE,VAR_MODE_NORMAL);
		scene_jump(var_map[? BATTLE_WIN_SCENE_LABEL],var_map[? BATTLE_WIN_SCENE]);
	}
	else
	{
		var_map_set(VAR_MODE,VAR_MODE_NORMAL); 
		//show_message("scene_jump('Afterbattle','CS_extra_RBS');");
		scene_jump("Afterbattle","CS_extra_RBS");
	}
	
	
	ds_list_clear(var_map_get(VAR_CHAR_BATTLE_ACTION_CHAR_ORDER_LIST));

	
	var_map[? BATTLE_WIN_SCENE] = "";
	var_map[? BATTLE_WIN_SCENE_LABEL] = "";
	var_map[? BATTLE_LOSS_SCENE] = "";
	var_map[? BATTLE_LOSS_SCENE_LABEL] = "";
	
	
	//exit;
}
else
	script_execute_add_unique(0,battle_process_next_char);