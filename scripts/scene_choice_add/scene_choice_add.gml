///scene_choice_add(name,label_or_posi_goto,script_goto,only_if_this_chunk_is_unread,bool_req1,bool_req2,bool_req3,tooltip,extrascript,extrascript_arg0...extrascript_arg9);
/// @description
/// @param name
/// @param label_or_posi_goto
/// @param script_goto
/// @param only_if_this_chunk_is_unread
/// @param bool_req1
/// @param bool_req2
/// @param bool_req3
/// @param tooltip
/// @param extrascript
/// @param extrascript_arg0...extrascript_arg6

if (argument_count <= 3 || argument[3] == "" || !chunk_mark_get(argument[3])) &&
	(argument_count <= 4 || argument[4]) &&
	(argument_count <= 5 || argument[5]) &&
	(argument_count <= 6 || argument[6])
{
	if argument_count > 0 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_NAME],argument[0]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_NAME],"");
	
	if argument_count > 1 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_POSI_OR_LABEL],argument[1]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_POSI_OR_LABEL],"");
	
	if argument_count > 2 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_SCRIPT],script_get_name_ext(argument[2]));
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_SCRIPT],"");
	
	if argument_count > 7 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_TOOLTIP],argument[7]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_TOOLTIP],"");
	
	ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_SURFACE],noone);
	
	if argument_count > 8 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT],script_get_name_ext(argument[8]));
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT],"");
	
	if argument_count > 9 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG0],argument[9]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG0],ARG_NOT_EXISTS);
	
	if argument_count > 10 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG1],argument[10]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG1],ARG_NOT_EXISTS);
	
	if argument_count > 11 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG2],argument[11]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG2],ARG_NOT_EXISTS);
	
	if argument_count > 12 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG3],argument[12]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG3],ARG_NOT_EXISTS);
	
	if argument_count > 13 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG4],argument[13]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG4],ARG_NOT_EXISTS);
	
	if argument_count > 14 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG5],argument[14]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG5],ARG_NOT_EXISTS);
	
	if argument_count > 15 ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG6],argument[15]);
	else ds_list_add(obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG6],ARG_NOT_EXISTS);
	

		
	obj_command_panel.choice_image_alpha = 0;
}