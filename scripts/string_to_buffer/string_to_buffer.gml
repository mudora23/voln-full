var str = argument0, len = string_length(str), b = buffer_create(len, buffer_fast, 1), i;

for (i = 1; i <= len; ++i) {
    buffer_write(b, buffer_u8, ord(string_char_at(str, i)));
} 

return b;

