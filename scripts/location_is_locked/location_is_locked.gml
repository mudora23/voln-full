///location_is_locked(LOCATION);
/// @description
/// @param LOCATION

with obj_control
{
	if string_count("LOCATION_",argument[0]) == 0
		return ds_list_find_index(var_map_get(VAR_LOCATION_UNLOCKED_LIST),"LOCATION_"+string(argument[0])) < 0;
	else
		return ds_list_find_index(var_map_get(VAR_LOCATION_UNLOCKED_LIST),argument[0]) < 0;
}


