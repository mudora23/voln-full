///scriptvar_PC_Armor_eq_Desc();
/// @description
var armor = var_map_get(PC_Armor_eq);
if armor == "" return "no armor";
else if armor == "Leather Jacket" return "a jacket of rough leather";