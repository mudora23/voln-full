///_end();
/// @description

// step
var_map_add(VAR_SCRIPT_PROC_STEP,1);


// jump
if var_map_get(VAR_NEXT_SCRIPT) != ""
{
	var_map_set(VAR_SCRIPT,var_map_get(VAR_NEXT_SCRIPT));
	var_map_set(VAR_SCRIPT_POSI,0);
	var_map_var_map_clear(VAR_SCRIPT_LABEL_MAP);
	var_map_set(VAR_SCRIPT_PROC_NUMBER,0);
	var_map_set(VAR_SCRIPT_PROC_STEP,0);
	
	var_map_set(VAR_NEXT_SCRIPT,"");
}
else if is_string(var_map_get(VAR_NEXT_SCRIPT_POSI_OR_LABEL)) && var_map_get(VAR_NEXT_SCRIPT_POSI_OR_LABEL) != ""
{
	//show_debug_message("Finding the label to jump to..."+string(var_map_var_map_get(VAR_SCRIPT_LABEL_MAP,VAR_NEXT_SCRIPT_POSI_OR_LABEL)));
	if var_map_var_map_get(VAR_SCRIPT_LABEL_MAP,VAR_NEXT_SCRIPT_POSI_OR_LABEL) != NOT_EXISTS
	{
		//show_debug_message("label jumping..."+string(var_map_var_map_get(VAR_SCRIPT_LABEL_MAP,VAR_NEXT_SCRIPT_POSI_OR_LABEL)));
		var_map_set(VAR_SCRIPT_POSI,round(var_map_var_map_get(VAR_SCRIPT_LABEL_MAP,VAR_NEXT_SCRIPT_POSI_OR_LABEL)));
		var_map_set(VAR_SCRIPT_PROC_NUMBER,0);
		var_map_set(VAR_SCRIPT_PROC_STEP,0);
			
		var_map_set(VAR_NEXT_SCRIPT_POSI_OR_LABEL,noone);
	}
}
else if !is_noone(var_map_get(VAR_NEXT_SCRIPT_POSI_OR_LABEL))
{
	//show_debug_message("jumping..."+string(var_map_get(VAR_NEXT_SCRIPT_POSI_OR_LABEL)));
	var_map_set(VAR_SCRIPT_POSI,round(var_map_get(VAR_NEXT_SCRIPT_POSI_OR_LABEL)));
	var_map_set(VAR_SCRIPT_PROC_NUMBER,0);
	var_map_set(VAR_SCRIPT_PROC_STEP,0);
		
	var_map_set(VAR_NEXT_SCRIPT_POSI_OR_LABEL,noone);
}
