///CS_breeding();
/// @description -

_label("start");

_label("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT");
	if percent_chance(50)
		_jump("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT_YES");
	else
		_jump("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT_NO");
		
_label("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT_YES");
	_ds_map_replace(char_id_get_char(var_map_get(VAR_TEMP_CHARID)),GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT,true);
	_dialog("S_PREGNANT_CONFIRM");
	_jump("",CS_prelocation);
		
_label("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_OR_NOT_NO");
	_execute(0,game_char_map_reset_impregnate_keys,char_id_get_char(var_map_get(VAR_TEMP_CHARID)));
	_jump("",CS_prelocation);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BIGGER");
	_ds_map_replace(char_id_get_char(var_map_get(VAR_TEMP_CHARID)),GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BIGGER,true);
	_dialog("S_PREGNANT_BIGGER");
	_jump("",CS_prelocation);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
	
_label("GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BORN");
	_ds_map_replace(char_id_get_char(var_map_get(VAR_TEMP_CHARID)),GAME_CHAR_IMPREGNATE_SCENE_PREGNANT_BORN,true);
	_execute(0,game_char_map_reset_impregnate_keys,char_id_get_char(var_map_get(VAR_TEMP_CHARID)));
	_execute(0,scene_baby_born);
	_dialog("S_PREGNANT_BORN");
	// [SHOW NEW CHARACTER SPRITE, CENTER] (will discuss with you how this is determined) (next, a chunk about their basic info)
	_dialog("S_PREGNANT_BORN_2");
	_dialog("S_PREGNANT_BORN_3");
	_jump("",CS_prelocation);
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("GAME_CHAR_BECOME_AN_ADULT");
	_ds_map_replace(char_id_get_char(var_map_get(VAR_TEMP_CHARID)),GAME_CHAR_ADULT,true);
	_execute(0,char_rejoin,var_map_get(VAR_TEMP_CHARID));
	_dialog("S_PREGNANT_BECOME_AN_ADULT");
	_dialog("S_PREGNANT_BECOME_AN_ADULT_2");
	_player_offspring_customize(var_map_get(VAR_TEMP_CHARID));
	// {chosen_bundie_name} joins team, taking a slot as a prisoner.  Can be immediately promoted to Ally, once there’s the space in party.
	_jump("",CS_prelocation);

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
