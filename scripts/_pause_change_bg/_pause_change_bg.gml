///_pause_change_bg(ID,sprite_target,FADE_SPEED,hardsec);
/// @description
/// @param ID
/// @param sprite_target
/// @param hardsec
if scene_is_current()
{
	if obj_control.var_map[? VAR_SCRIPT_PROC_STEP] == 0
	{
		scene_sprite_list_change_fade_speed(argument0,argument2);
		scene_sprite_list_change_sprite(argument0,spr_bg_black);
		script_execute_add(argument3,scene_sprite_list_change_sprite,argument0,argument1);
	}
	
	// non-clicking
	if obj_control.var_map[? VAR_SCRIPT_PROC_STEP]/room_speed >= argument2
	{
		scene_jump_next();
	}
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);