///battle_process_defense_up_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
			
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" defenses up.",TAG_FONT_N,TAG_COLOR_EFFECT));

	skill_spend_cost(char_source,SKILL_DEFENSE_UP,char_get_skill_level(char_source,SKILL_DEFENSE_UP));
	
	battle_char_buff_apply(char_target,BUFF_PHY_ARMOR_UP2,char_get_skill_level(battle_get_current_action_char(),BUFF_PHY_ARMOR_UP2),3);
	battle_char_buff_apply(char_target,BUFF_TORU_ARMOR_UP2,char_get_skill_level(battle_get_current_action_char(),BUFF_TORU_ARMOR_UP2),3);
	
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);

	voln_play_sfx(sfx_Buff1);
	
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);

}