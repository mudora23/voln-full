///story_execute_get_raw_text(marked_text);
/// @description
/// @param marked_text

var raw_marked_string_list = story_execute(argument0);
var return_text = raw_marked_string_list[| 0];		
ds_list_destroy(raw_marked_string_list);
return return_text;
		