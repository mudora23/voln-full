///scriptvar_PC_Hips_Size_Desc();
/// @description
var num = var_map_get(PC_Hips_Size_N);
if num == 0 return "slender";
else if num == 1 return "proportionate";
else if num == 2 return choose("flared","gurly");
else return choose("broad","capably-wide");