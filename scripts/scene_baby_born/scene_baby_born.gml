///scene_baby_born();
///@description
var parent_id = var_map_get(VAR_TEMP_CHARID);
var parent_id2 = var_map_get(VAR_TEMP_CHARID2);
var mom = char_id_get_char(parent_id);
var dad = char_id_get_char(parent_id2);
if mom != noone && dad != noone
{
	
	// choose the non unqiue parent as basis of the child if possible.
	if !mom[? GAME_CHAR_UNIQUE]
		var inherit_parent = mom;
	else if !dad[? GAME_CHAR_UNIQUE]
		var inherit_parent = dad;
	else if percent_chance(50)
		var inherit_parent = mom;
	else
		var inherit_parent = dad;
	
	/*if ally_exists(char_get_char_id(inherit_parent)) && !ally_is_full()
		var child = ally_add_char(inherit_parent[? GAME_CHAR_NAME],false,1);
	else if pet_exists(parent_id) && !pet_is_full()
		var child = pet_add_char(inherit_parent[? GAME_CHAR_NAME],false,1);
	else
		var child = storage_add_char(inherit_parent[? GAME_CHAR_NAME],false,1);
	*/
	
	if dad == char_get_player() || mom == char_get_player()
	{
		var child = ally_r_add_char(NAME_PLAYER_OFFSPRING,false,1);
		//show_message("ally_r_add_char");
	}
	else if ally_exists(char_get_char_id(mom))
	{
		var child = ally_r_add_char(inherit_parent[? GAME_CHAR_NAME],false,1);
		//show_message("ally_r_add_char");
	}
	else
	{
		var child = pet_r_add_char(inherit_parent[? GAME_CHAR_NAME],false,1);
		//show_message("pet_r_add_char");
	}

	
	// setup
	child[? GAME_CHAR_MOM_ID] = parent_id;
	child[? GAME_CHAR_DAD_ID] = parent_id2;
	child[? GAME_CHAR_HOURS_SINCE_BORN] = 0;
	child[? GAME_CHAR_ADULT] = false;
	
	
	child[? GAME_CHAR_FROM_BREEDING] = true;
	
	
	

	
	// skills breeding
	var skill_inherit_list = ds_list_create();
	
	var char = mom;
	
	var char_default_skill_map = char[? GAME_CHAR_SKILLS_MAP];
	var skill_key = ds_map_find_first(char_default_skill_map);
	while !is_undefined(skill_key)
	{
		if !ds_map_exists(child[? GAME_CHAR_SKILLS_MAP],skill_key) &&
		   !ds_map_exists(child[? GAME_CHAR_BONUS_SKILLS_MAP],skill_key) &&
		   percent_chance(skill_get_inherit_chance(skill_key)*100)
			ds_list_add_unique(skill_inherit_list,skill_key);
		var skill_key = ds_map_find_next(char_default_skill_map,skill_key);
	}
	
	var char_bonus_skill_map = char[? GAME_CHAR_BONUS_SKILLS_MAP];
	var skill_key = ds_map_find_first(char_bonus_skill_map);
	while !is_undefined(skill_key)
	{
		if !ds_map_exists(child[? GAME_CHAR_SKILLS_MAP],skill_key) &&
		   !ds_map_exists(child[? GAME_CHAR_BONUS_SKILLS_MAP],skill_key) &&
		   percent_chance(skill_get_inherit_chance(skill_key)*100)
			ds_list_add_unique(skill_inherit_list,skill_key);
		var skill_key = ds_map_find_next(char_bonus_skill_map,skill_key);
	}
	
	var char = dad;
	
	var char_default_skill_map = char[? GAME_CHAR_SKILLS_MAP];
	var skill_key = ds_map_find_first(char_default_skill_map);
	while !is_undefined(skill_key)
	{
		if !ds_map_exists(child[? GAME_CHAR_SKILLS_MAP],skill_key) &&
		   !ds_map_exists(child[? GAME_CHAR_BONUS_SKILLS_MAP],skill_key) &&
		   percent_chance(skill_get_inherit_chance(skill_key)*100)
			ds_list_add_unique(skill_inherit_list,skill_key);
		var skill_key = ds_map_find_next(char_default_skill_map,skill_key);
	}
	
	var char_bonus_skill_map = char[? GAME_CHAR_BONUS_SKILLS_MAP];
	var skill_key = ds_map_find_first(char_bonus_skill_map);
	while !is_undefined(skill_key)
	{
		if !ds_map_exists(child[? GAME_CHAR_SKILLS_MAP],skill_key) &&
		   !ds_map_exists(child[? GAME_CHAR_BONUS_SKILLS_MAP],skill_key) &&
		   percent_chance(skill_get_inherit_chance(skill_key)*100)
			ds_list_add_unique(skill_inherit_list,skill_key);
		var skill_key = ds_map_find_next(char_bonus_skill_map,skill_key);
	}

	if ds_list_size(skill_inherit_list) > 0
	{
		ds_list_shuffle(skill_inherit_list);
		
		ds_map_add_unique(child[? GAME_CHAR_BONUS_SKILLS_MAP],skill_inherit_list[| 0],0);
		
		if ds_list_size(skill_inherit_list) > 1
			ds_map_add_unique(child[? GAME_CHAR_BONUS_SKILLS_MAP],skill_inherit_list[| 1],0);
	
		char_skills_recalculate(child);
	}
	
	ds_list_destroy(skill_inherit_list);
	
	// stats breeding (80%~130% stats difference; on average +10% stats increase)
	child[? GAME_CHAR_STR_RATE] = (mom[? GAME_CHAR_STR_RATE] + mom[? GAME_CHAR_BONUS_STR_RATE] + dad[? GAME_CHAR_STR_RATE] + dad[? GAME_CHAR_BONUS_STR_RATE])/2 * random_range(0.8,1.3);
	child[? GAME_CHAR_FOU_RATE] = (mom[? GAME_CHAR_FOU_RATE] + mom[? GAME_CHAR_BONUS_FOU_RATE] + dad[? GAME_CHAR_FOU_RATE] + dad[? GAME_CHAR_BONUS_FOU_RATE])/2 * random_range(0.8,1.3);
	child[? GAME_CHAR_DEX_RATE] = (mom[? GAME_CHAR_DEX_RATE] + mom[? GAME_CHAR_BONUS_DEX_RATE] + dad[? GAME_CHAR_DEX_RATE] + dad[? GAME_CHAR_BONUS_DEX_RATE])/2 * random_range(0.8,1.3);
	child[? GAME_CHAR_END_RATE] = (mom[? GAME_CHAR_END_RATE] + mom[? GAME_CHAR_BONUS_END_RATE] + dad[? GAME_CHAR_END_RATE] + dad[? GAME_CHAR_BONUS_END_RATE])/2 * random_range(0.8,1.3);
	child[? GAME_CHAR_INT_RATE] = (mom[? GAME_CHAR_INT_RATE] + mom[? GAME_CHAR_BONUS_INT_RATE] + dad[? GAME_CHAR_INT_RATE] + dad[? GAME_CHAR_BONUS_INT_RATE])/2 * random_range(0.8,1.3);
	child[? GAME_CHAR_WIS_RATE] = (mom[? GAME_CHAR_WIS_RATE] + mom[? GAME_CHAR_BONUS_WIS_RATE] + dad[? GAME_CHAR_WIS_RATE] + dad[? GAME_CHAR_BONUS_WIS_RATE])/2 * random_range(0.8,1.3);
	child[? GAME_CHAR_SPI_RATE] = (mom[? GAME_CHAR_SPI_RATE] + mom[? GAME_CHAR_BONUS_SPI_RATE] + dad[? GAME_CHAR_SPI_RATE] + dad[? GAME_CHAR_BONUS_SPI_RATE])/2 * random_range(0.8,1.3);
	
	// superior kid (10% chance)
	if mom[? GAME_CHAR_SUPERIOR] && dad[? GAME_CHAR_SUPERIOR]
	{
		if percent_chance(30) child[? GAME_CHAR_SUPERIOR] = true;
	}
	else if mom[? GAME_CHAR_SUPERIOR] || dad[? GAME_CHAR_SUPERIOR]
	{
		if percent_chance(15) child[? GAME_CHAR_SUPERIOR] = true;
	}
	else
	{
		if percent_chance(10) child[? GAME_CHAR_SUPERIOR] = true;
	}
	
	// color breeding
	var class = char_get_class_name(child);
	
	// colors breeding
	char_set_hair_eyes_skin_color_from_parents(child);
	char_set_random_clothes_colors_from_ini(child,"GAME_CHAR_COLOR_"+string_upper(class)+"_1","GAME_CHAR_COLOR_"+string_upper(class)+"_2","GAME_CHAR_COLOR_"+string_upper(class)+"_3","GAME_CHAR_COLOR_"+string_upper(class)+"_4",class);
	
	
	var_map_set(VAR_TEMP_CHARID,child[? GAME_CHAR_ID]);
}