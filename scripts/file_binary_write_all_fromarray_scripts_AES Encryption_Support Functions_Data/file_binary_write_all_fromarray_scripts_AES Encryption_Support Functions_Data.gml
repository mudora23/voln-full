///file_binary_write_all(filename,byteArray)

var fn = argument0, b = file_bin_open(fn, 1), i, a = argument1,s = array_length_1d(a);

file_bin_rewrite(b);

for (i=0;i<s;++i){
    file_bin_write_byte(b, a[@ i]);
}

file_bin_close(b);
