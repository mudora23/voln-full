///ds_map_add_unique(id,key,value);
/// @description
/// @param id
/// @param key
/// @param value
if !ds_map_exists(argument[0],argument[1])
{
	return ds_map_add(argument[0],argument[1],argument[2]);
}
