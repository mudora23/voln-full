///battle_char_list_get_capable_power_level(char_list);
/// @description
/// @param char_list

var power_level = 0;

for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];
	if battle_char_get_combat_capable(char)
	{
		
		var hp_ratio = char[? GAME_CHAR_HEALTH] / char_get_health_max(char);
		var lust_ratio = 1 - (char[? GAME_CHAR_LUST] / char_get_lust_max(char));
		var taken_ratio = min(hp_ratio,lust_ratio);
		power_level += char[? GAME_CHAR_LEVEL] * taken_ratio;
	}
}

power_level*=(1+0.1*battle_char_list_get_combat_capable_number(argument0));

return power_level;
