///enemy_exists(name);
/// @description
/// @param name
var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
for(var i = 0;i< ds_list_size(enemy_list);i++)
{
	var char_map = enemy_list[| i];
	if char_map[? GAME_CHAR_NAME] == argument0 || char_map[? GAME_CHAR_NAME_TYPE] == argument0
		return true;
}
return false;
