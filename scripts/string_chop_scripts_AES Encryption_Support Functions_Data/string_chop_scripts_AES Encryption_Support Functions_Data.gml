///string_chop(string[, n])
var n = 1, s = argument[0], r;
if (argument_count > 1) {
    n = argument[1];
}
r[0] = "";

var count = ceil(string_length(s)/n), i;

for (i = 0; i < count; ++i) {
    r[array_length_1d(r) - (i == 0)] = string_copy(s, i * n + 1, min(string_length(s) - (i * n), n));
}

return r;


