///skill_get_floating_tooltip(skill_name);
/// @desription
/// @param skill_name

var skill_map = obj_control.skill_wiki_map[? argument0];
if skill_map[? SKILLDISPLAYNAME] != ""
{
	var tt = skill_map[? SKILLDISPLAYNAME] + "\r\n"+skill_map[? SKILLDES];
	var bool_level_req = ds_map_exists(skill_map,SKILL_REQ_LEVEL_LIST) && ds_list_size(skill_map[? SKILL_REQ_LEVEL_LIST]) > 0;
	var max_level_req = 1;
	if bool_level_req
		for(var i = 0; i < ds_list_size(skill_map[? SKILL_REQ_LEVEL_LIST]);i++)
			max_level_req = max(max_level_req, ds_list_find_value(skill_map[? SKILL_REQ_LEVEL_LIST],i));
	var bool_vit_cost = (ds_map_exists(skill_map,SKILL_VITALITY_LIST) && ds_list_size(skill_map[? SKILL_VITALITY_LIST]) > 0);
	var bool_toru_cost = (ds_map_exists(skill_map,SKILL_TORU_LIST) && ds_list_size(skill_map[? SKILL_TORU_LIST]) > 0);
	var bool_stamina_cost = (ds_map_exists(skill_map,SKILL_STAMINA_LIST) && ds_list_size(skill_map[? SKILL_STAMINA_LIST]) > 0);




	if !bool_level_req || max_level_req <= 1
	{
		if bool_vit_cost || bool_toru_cost || bool_stamina_cost tt += "\r\n\r\n";
		if bool_vit_cost tt += "Vitality cost "+string(ds_list_find_value(skill_map[? SKILL_VITALITY_LIST],0))+" ";
		if bool_toru_cost tt += "Toru cost "+string(ds_list_find_value(skill_map[? SKILL_TORU_LIST],0))+" ";
		if bool_stamina_cost tt += "Stamina cost "+string(ds_list_find_value(skill_map[? SKILL_STAMINA_LIST],0))+" ";
	}
	else
	{
		
		if ds_list_size(skill_map[? SKILL_REQ_LEVEL_LIST]) > 0 tt += "\r\n";
		for(var i = 0; i < ds_list_size(skill_map[? SKILL_REQ_LEVEL_LIST]);i++)
		{
			var req_level = ds_list_find_value(skill_map[? SKILL_REQ_LEVEL_LIST],i);
			tt += "\r\nLevel "+string(i+1)+": ";
			if bool_vit_cost tt += "Vitality cost "+string(ds_list_find_value(skill_map[? SKILL_VITALITY_LIST],i))+" ";
			if bool_toru_cost tt += "Toru cost "+string(ds_list_find_value(skill_map[? SKILL_TORU_LIST],i))+" ";
			if bool_stamina_cost tt += "Stamina cost "+string(ds_list_find_value(skill_map[? SKILL_STAMINA_LIST],i))+" ";
			if char_get_skill_level(char_get_player(),argument0) <= i && req_level > 1 && char_get_info(char_get_player(),GAME_CHAR_LEVEL) < req_level
				tt += "(unlocks at LV"+ string(req_level)+")";
		}
	}
	return tt;
}
else
	return "";