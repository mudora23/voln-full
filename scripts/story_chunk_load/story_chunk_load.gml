///story_chunk_load(story_chunk);
/// @description
/// @param story_chunk
var story_chunk_full_path = story_chunk_get_full_path(argument0);

if file_exists(story_chunk_full_path)
{
	var file = file_text_open_read(story_chunk_full_path);
	var file_content = file_text_readln(file);
	if string_length(file_content) < 2000
		var file_content = voln_decrypt(file_content);
	file_text_close(file);
}
else
{
	var file_content = text_add_markup(argument[0],TAG_FONT_N,TAG_COLOR_NORMAL)
}
return file_content;