///SL1_HUN_DAOA();
/// @description Nirholt - HUN DAOA

_label("start");

_label("SL1_HUN_DAOA_INTRO");
	_dialog("SL1_HUN_DAOA_INTRO");
	_show("balwag",spr_char_Bal_r_ss,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_HUN_DAOA_INTRO_2",true);
	_jump("SL1_HUN_DAOA_INTRO_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_INTRO_Choice");
	_choice("“It’s none of your business.  And put on a shirt.”","SL1_HUN_DAOA_INTRO_2_A","","SL1_HUN_DAOA_INTRO_2_A");
	_choice("“Yes.”","SL1_HUN_DAOA_INTRO_2_B","","SL1_HUN_DAOA_INTRO_2_A");
	_choice("“I’m "+string(scriptvar_PC_Name())+".  Who are you?”","SL1_HUN_DAOA_INTRO_2_C");
	_choice("“What is this place?”","SL1_HUN_DAOA_INTRO_2_D");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_INTRO_2_A");
	_dialog("SL1_HUN_DAOA_INTRO_2_A",true);
	_jump("SL1_HUN_DAOA_INTRO_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_INTRO_2_B");
	_dialog("SL1_HUN_DAOA_INTRO_2_B");
	_dialog("SL1_HUN_DAOA_INTRO_2_B_2");
	_jump("SL1_HUN_DAOA_INTRO_3");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_INTRO_2_C");
	_dialog("SL1_HUN_DAOA_INTRO_2_C");
	_jump("SL1_HUN_DAOA_INTRO_3");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_INTRO_2_D");
	_dialog("SL1_HUN_DAOA_INTRO_2_D");
	_jump_if_read("SL1_HUN_DAOA_INTRO_3","","SL1_HUN_DAOA_INTRO_2_A");
	_jump_if_unread("SL1_HUN_DAOA_INTRO_2_D_2","","SL1_HUN_DAOA_INTRO_2_A");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_INTRO_2_D_2");
	_dialog("SL1_HUN_DAOA_INTRO_2_D_2");
	_dialog("SL1_HUN_DAOA_INTRO_2_D_3");
	_jump("SL1_HUN_DAOA_INTRO_3");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_HUN_DAOA_INTRO_3");
	_dialog("SL1_HUN_DAOA_INTRO_3",true);
	_jump("SL1_HUN_DAOA_ENTER_Choice");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_HUN_DAOA_ENTER_Choice");
	if qj_exists("S_0005_FINDING_ZEYD_QJ") && !chunk_mark_get("S_0005_13_A_2")
		_choice("Enter","S_0005_1B","S_0005");
	else if !chunk_mark_get("S_0005_13_A_2")
		_choice("Enter","SL1_HUN_DAOA_INTRO_3_A");
	else if !chunk_mark_get("S_0005_WON_14_C")
		_choice("Enter","S_0005_SL1_HUN_DAOA_INSIDE","S_0005");
	else
		_choice("[?]Enter","SL1_HUN_DAOA_INSIDE");
		
	if chunk_mark_get("S_0005_WON_14_C") && chunk_mark_get("SS_HUN_DAOA_1_1") && !chunk_mark_get("SL1_HUN_DAOA_CHAT")
		_choice("Chat","SL1_HUN_DAOA_CHAT");
		
	if chunk_mark_get("S_0005_WON_14_C") && !chunk_mark_get("SS_HUN_DAOA_1_1")
		_choice("Suggest sex","SS_HUN_DAOA","CS_extra_SS");
	else if chunk_mark_get("S_0005_WON_14_C") && chunk_mark_get("SS_HUN_DAOA_1_1")
		_choice("Suggest sex","SL1_HUN_DAOA_SUGGEST");
		
	_choice("Leave","SL1_HUN_DAOA_INTRO_3_B");	
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_HUN_DAOA_INTRO_3_A");
	_dialog("SL1_HUN_DAOA_INTRO_3_A");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_HUN_DAOA_INTRO_3_B");
	_dialog("SL1_HUN_DAOA_INTRO_3_B");
	_jump("MENU","SL1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_HUN_DAOA_CHAT");
	_dialog("SL1_HUN_DAOA_CHAT",true);
	_jump("SL1_HUN_DAOA_ENTER_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("SL1_HUN_DAOA_SUGGEST");
	_dialog("SL1_HUN_DAOA_SUGGEST",true);
	_jump("SS_HUN_DAOA_BALWAG_Choice","SS_HUN_DAOA_BALWAG");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_HUN_DAOA_RETURN");
	_dialog("SL1_HUN_DAOA_RETURN");
	_show("balwag",spr_char_Bal_r_ss,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_HUN_DAOA_RETURN_2",true);
	_jump("SL1_HUN_DAOA_ENTER_Choice");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////


	
	
	
	
	
	
	
	
	
	
	
	
	

