///battle_process_flirt_atk_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();
	


	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);


	var battlelog_pieces_list = ds_list_create();
	
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" Flirts",TAG_FONT_N,TAG_COLOR_FLIRT));
	ds_list_add(battlelog_pieces_list,text_add_markup(" with",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));

	
	// performing
	var dodge_chance = char_get_dodge_chance(char_target);
	var hit_chance = char_get_hit_chance(char_source);
	
	//var real_cost = min(char_source[? GAME_CHAR_STAMINA], stamina_cost);
	//char_source[? GAME_CHAR_STAMINA] -= real_cost;
	//draw_floating_text(char_source[? GAME_CHAR_X_FLOATING_TEXT],char_source[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_cost)),COLOR_STAMINA);
	
	
	if percent_chance(80-dodge_chance+hit_chance)
	{

		var damage = char_get_lust_damage(char_source)* random_range(0.8,1.2);
		var damage_size_adjust = 1;
		var shake_size_adjust = 1;
		if percent_chance(char_get_crit_chance(char_source)){damage*=char_get_crit_multiply_ratio(char_source);voln_play_sfx(sfx_Hit8);damage_size_adjust+=0.4;shake_size_adjust+=0.4;}
		var armor = char_get_lust_armor(char_target);
		var real_damage = damage * clamp(((100-armor)/100),0,1);
		char_target[? GAME_CHAR_LUST] = min(100,char_target[? GAME_CHAR_LUST] + real_damage);
		
		
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"+"+string(round(real_damage)),COLOR_LUST,0,damage_size_adjust);
		
		ally_get_hit_port(char_target);
		
		ds_list_add(battlelog_pieces_list,text_add_markup(". The creature seems",TAG_FONT_N,TAG_COLOR_NORMAL));
		var lust_level = char_target[? GAME_CHAR_LUST];
		if lust_level < 25 var the_word = choose("interested","curious");
		else if lust_level < 50 var the_word = choose("tempted","attracted","desirous");
		else if lust_level < 75 var the_word = choose("tantalized","enticed","allured");
		else var the_word = choose("libidinous","lustful","inflamed");
		
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+the_word+".",TAG_FONT_N,TAG_COLOR_NORMAL));
		
		char_target[? GAME_CHAR_COLOR] = COLOR_LUST;
		char_target[? GAME_CHAR_SHAKE_AMOUNT] = 20*shake_size_adjust;
			
		var debug_message = "Basic flirt atk is performed,";
		debug_message+= " source: ";
		debug_message+= string(damage);
		debug_message+= "("+string(char_source[? GAME_CHAR_OWNER])+")";
		debug_message+= " target: ";
		debug_message+= string(armor);
		debug_message+= "("+string(char_target[? GAME_CHAR_OWNER])+")";

	}
	else
	{
		draw_floating_text(char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_LUST,0,0.7);
		ds_list_add(battlelog_pieces_list,text_add_markup(", but it has no effect.",TAG_FONT_N,TAG_COLOR_NORMAL));
	}
		

	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);

	voln_play_sfx(sfx_Bless2);
	script_execute_add(BATTLE_PAUSE_TIME_SEC,battle_process_turn_end);
	
	//show_debug_message("------Battle 'Basic flirt atk CMD script' is executed!------");

}