///battle_char_create_opposite_side_list(char);
/// @description
/// @param char

if argument0[? GAME_CHAR_OWNER] == OWNER_ENEMY var is_player_side = true;
else var is_player_side = false;

if battle_char_get_debuff_level(argument0,DEBUFF_TEMPT) > 0 is_player_side = !is_player_side;

if is_player_side
{
	var opposite_side_list = ds_list_create();
	var the_list = var_map_get(VAR_ALLY_LIST);
	for(var i = 0 ; i < ds_list_size(the_list);i++)
		ds_list_add(opposite_side_list,the_list[| i]);
	ds_list_add(opposite_side_list,var_map_get(VAR_PLAYER));
}
else
{
	var opposite_side_list = ds_list_create();
	var the_list = var_map_get(VAR_ENEMY_LIST);
	for(var i = 0 ; i < ds_list_size(the_list);i++)
		ds_list_add(opposite_side_list,the_list[| i]);
}
ds_list_delete_value(opposite_side_list,argument0);
return opposite_side_list;