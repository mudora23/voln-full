///char_get_lust_armor(char_map);
/// @description
/// @param char_map
var char_map = argument[0];
var number = char_get_LRR(char_map);
if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;
return clamp(number,0,100);
