///battle_process_endless_whip_CMD();
/// @description
/// @param chosen_index
with obj_control
{
	var stamina_cost = 100;
	
	if char_get_skill_level(battle_get_current_action_char(),SKILL_OCCULT) <= 1 var damage_growth_ratio = 1;
	if char_get_skill_level(battle_get_current_action_char(),SKILL_OCCULT) == 2 var damage_growth_ratio = 1.2;
	if char_get_skill_level(battle_get_current_action_char(),SKILL_OCCULT) >= 3 var damage_growth_ratio = 1.5;

	scene_choices_list_clear();
	var char_source = battle_get_current_action_char();
	var char_source_color_tag = battle_char_get_color_tag(char_source);
	var char_target = char_id_get_char(argument[0]);
	var char_target_color_tag = battle_char_get_color_tag(char_target);
	
	skill_spend_cost(char_source,SKILL_ENDLESS_WHIP,char_get_skill_level(char_source,SKILL_ENDLESS_WHIP));

	var battlelog_pieces_list = ds_list_create();
	ds_list_add(battlelog_pieces_list,text_add_markup(char_source[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_source_color_tag));
	ds_list_add(battlelog_pieces_list,text_add_markup(" casts",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" Endless whip",TAG_FONT_N,TAG_COLOR_EFFECT));
	ds_list_add(battlelog_pieces_list,text_add_markup(" at",TAG_FONT_N,TAG_COLOR_NORMAL));
	ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));
	
	// performing
	var dodge_chance = char_get_dodge_chance(char_target);
	var hit_chance = char_get_hit_chance(char_source);
	
	

	
	var total_real_damage = 0;
	
	
	for(var i = 0; i < 6;i++)
	{
		if i == 0 var cast_time = 0;
		else if i == 1 || i == 2 || i == 3 || i == 4 var cast_time = 0.5+i/6;
		else var cast_time = 2;
		
		if i == 0 var damage = char_get_phy_damage(char_source)* random_range(0.3*damage_growth_ratio,0.4*damage_growth_ratio);
		else if i == 1 || i == 2 || i == 3 || i == 4 var damage = char_get_phy_damage(char_source)* random_range(0.3*damage_growth_ratio,0.4*damage_growth_ratio);
		else var damage = char_get_phy_damage(char_source)* random_range(0.8*damage_growth_ratio,1.0*damage_growth_ratio);
		
		if i == 0 var shake_amount = 10;
		else if i == 1 || i == 2 || i == 3 || i == 4 var shake_amount = 10;
		else var shake_amount = 20;
		
		if i == 0 var text_size_ratio = 0.5;
		else if i == 1 || i == 2 || i == 3 || i == 4 var text_size_ratio = 0.5;
		else var text_size_ratio = 1.2;
		
		script_execute_add(cast_time,postfx_ps_onhit,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT]);
		script_execute_add(cast_time,voln_play_sfx,choose(sfx_Whip1,sfx_Whip2,sfx_Whip3));
		
		if percent_chance(80-dodge_chance+hit_chance)
		{
			if percent_chance(char_get_crit_chance(char_source)) damage*=char_get_crit_multiply_ratio(char_source);
			var armor = char_get_phy_armor(char_target);
			var real_damage = damage * clamp(((100-armor)/100),0,1);
			total_real_damage += real_damage;
		
			char_target[? GAME_CHAR_HEALTH] = max(0,char_target[? GAME_CHAR_HEALTH] - real_damage);
		
			script_execute_add(cast_time,draw_floating_text,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"-"+string(round(real_damage)),COLOR_HP,text_size_ratio);
			ally_get_hit_port(char_target);
			char_target[? GAME_CHAR_COLOR] = COLOR_HP;
			char_target[? GAME_CHAR_SHAKE_AMOUNT] = shake_amount;
		}
		else
			script_execute_add(cast_time,draw_floating_text,char_target[? GAME_CHAR_X_FLOATING_TEXT],char_target[? GAME_CHAR_Y_FLOATING_TEXT],"MISSED",COLOR_HP,text_size_ratio,0.7);
			

		
	}
	
	if total_real_damage > 0
	{
		ds_list_add(battlelog_pieces_list,text_add_markup(" hitting the",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+char_target[? GAME_CHAR_DISPLAYNAME],TAG_FONT_N,char_target_color_tag));	
		ds_list_add(battlelog_pieces_list,text_add_markup(" for",TAG_FONT_N,TAG_COLOR_NORMAL));
		ds_list_add(battlelog_pieces_list,text_add_markup(" "+string(round(total_real_damage)),TAG_FONT_N,TAG_COLOR_DAMAGE));
		ds_list_add(battlelog_pieces_list,text_add_markup(" damage!",TAG_FONT_N,TAG_COLOR_NORMAL));
	}
	else
	{
		ds_list_add(battlelog_pieces_list,text_add_markup(" and misses!",TAG_FONT_N,TAG_COLOR_NORMAL));	
	}
	
	
	var battle_log = string_append_from_list(battlelog_pieces_list);
	battlelog_add(battle_log);
	ds_list_destroy(battlelog_pieces_list);

	script_execute_add(2.5,battle_process_turn_end);
	

}