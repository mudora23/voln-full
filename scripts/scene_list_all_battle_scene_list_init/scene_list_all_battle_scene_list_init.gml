///scene_list_all_battle_scene_list_init(temp_list);
/// @description
/// @param temp_list
var temp_list = argument[0];

if var_map_get(BATTLE_END_TYPE) == GAME_DOMINANT_VICTORY
	scene_list_add_dv(temp_list);
if var_map_get(BATTLE_END_TYPE) == GAME_SEDUCTIVE_VICTORY
	scene_list_add_sv(temp_list);
if var_map_get(BATTLE_END_TYPE) == GAME_DOMINANT_DEFEAT || var_map_get(BATTLE_END_TYPE) == GAME_SEDUCTIVE_DEFEAT
	scene_list_add_d(temp_list);
	
//show_message("scene_list_all_battle_scene_list_init ds_list_size(all_battle_scene_list): "+string(ds_list_size(temp_list)));
















