///S_0003();
/// @description

_label("start");
	_dialog("",true);
	_execute(0,postfx_color_cut,0.1,2.5,0.6,c_dkdkwhite,1);
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_main_gui(VAR_GUI_NORMAL);
	_show("bg",spr_bg_dkdkwhite,5,5,1,1,0,1,FADE_SLOW_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_BG,1);
	_pause(3.2,0,true);
	_dialog("S_0003_2");
	_change("bg",spr_bg_Forest_Deep_01_midsun);
	_dialog("S_0003_3");
	_show("zeyd",spr_char_Zeyd_f_a,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0003_4");
	_dialog("S_0003_5");
	_movex("zeyd",XNUM_RIGHT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("ku",spr_char_Ku_n_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0003_6");
	_dialog("S_0003_7");
	_dialog("S_0003_8");
	_dialog("S_0003_9");
	_dialog("S_0003_10");
	_dialog("S_0003_11");
	_dialog("S_0003_12");
	_dialog("S_0003_13");
	_dialog("S_0003_14");
	_dialog("S_0003_15");
	_dialog("S_0003_16");
	_dialog("S_0003_17");
	_dialog("S_0003_18");
	_dialog("S_0003_19");
	_dialog("S_0003_20");
	_execute(0,postfx_shake,0.1,0.1,0.1,15);
	_dialog("S_0003_21",true);
	_jump("S_0003_Choice1");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0003_Choice1");
	_choice("“What did you do to my comrades?”","S_0003_22_1A","","S_0003_22_1A");
	_choice("“Are you saying you want to suck my pork?”","S_0003_24_1B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0003_22_1A");
	_dialog("S_0003_22_1A");
	_dialog("S_0003_23_1A",true);
	_jump("S_0003_Choice1");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0003_24_1B");
	_change("ku",spr_char_Ku_n_ss);
	_dialog("S_0003_24_1B");
	_dialog("S_0003_25");
	_change("ku",spr_char_Ku_n_n);
	_dialog("S_0003_26");
	_dialog("S_0003_27");
	_dialog("S_0003_28");
	_dialog("S_0003_29",true);
	_jump("S_0003_Choice2");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0003_Choice2");
	_choice("Attempt to convince Zeyd to cooperate with Kûdi.","S_0003_30_2A");
	_choice("Seduce Zeyd instead, planning to pay Kûdi the resultant fluids.","S_0003_32","","S_0003_32_2B");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0003_30_2A");
	_dialog("S_0003_30_2A");
	_dialog("S_0003_31_2A");
	_showcg("ku_CG",spr_cg_Ku_sex01_1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,1,fa_right,fa_center,fa_right,fa_none,false,true);
	_dialog("S_0003_31_2A_2");
	_hidecg_all();
	_jump("S_0003_34");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0003_32");
	if char_get_char_opinion(NAME_ZEYD,obj_control.var_map[? VAR_PLAYER]) >= 1
		_jump("S_0003_33_2B");
	else
		_jump("S_0003_32_2B");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("S_0003_32_2B");
	_dialog("S_0003_32_2B");
	_jump("S_0003_Choice2");
	
_label("S_0003_33_2B");
	_dialog("S_0003_33_2B");
	_showcg("ku_CG",spr_cg_Ku_sex01_1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,1,fa_right,fa_center,fa_right,fa_none,false,true);
	_dialog("S_0003_33_2B_2");
	_hidecg_all();
	_jump("S_0003_34");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("S_0003_34");
	_dialog("S_0003_34");
	_dialog("S_0003_35");
	_hide("ku",FADE_NORMAL_SEC);
	_dialog("S_0003_36");
	_show("ku",spr_char_Ku_n_n,XNUM_LEFT,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0003_37");
	_dialog("S_0003_38");
	_dialog("S_0003_39");
	_dialog("S_0003_40");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_dialog("S_0003_41");
	_dialog("S_0003_42");
	_show("zeyd",spr_char_Zeyd_f_a,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0003_43");
	_movex("zeyd",XNUM_RIGHT,SMOOTH_SLIDE_NORMAL_SEC);
	_show("ku",spr_char_Ku_n_n,0,XNUM_LEFT,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0003_44");
	_dialog("S_0003_45");
	_dialog("S_0003_46");
	_dialog("S_0003_47");
	_dialog("S_0003_48");
	_dialog("S_0003_49");
	_dialog("S_0003_50");
	_dialog("S_0003_51");
	_dialog("S_0003_52");
	_dialog("S_0003_53");
	_dialog("S_0003_54");
	_dialog("S_0003_55");
	_dialog("S_0003_56");
	_dialog("S_0003_57");
	_dialog("S_0003_58");
	_hide_all();
	_dialog("S_0003_59");
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("S_0003_60");
	_dialog("S_0003_61");
	_execute(0,postfx_shake,0.1,0.1,0.1,15);
	_dialog("S_0003_62");
	_dialog("S_0003_63",true);
	_choice("“If I must.”","S_0003_64_3A");
	_choice("“No, I can’t.”","S_0003_66_3B");
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
	
_label("S_0003_64_3A");
	_dialog("S_0003_64_3A");
	_dialog("S_0003_65_3A");
	_jump("S_0003_68");
	
_label("S_0003_66_3B");
	_dialog("S_0003_66_3B");
	_dialog("S_0003_67_3B");
	_jump("S_0003_68");
	
_label("S_0003_68");
	_dialog("S_0003_68");
	_qj_add("S_0004_QJ");
	_colorscheme(COLORSCHEME_GETITEM);
	_execute(0,inventory_add_item,ITEM_MESO_EY_TONIC_POOR,10);
	_dialog_item(ITEM_MESO_EY_TONIC_POOR,10);
	_colorscheme(COLORSCHEME_NORMAL);
	_main_gui(VAR_GUI_NORMAL);
	
	_var_map_set(VAR_BATTLELOG_ACTIVE,false);
	
	_execute(0,ally_add_char,NAME_ZEYD,true,1);
	_execute(0,char_set_info,ally_get_char(NAME_ZEYD),GAME_CHAR_LUST,50);
	_execute(0,ally_storage_lock,NAME_ZEYD,true);

	_execute(0,ally_add_char,NAME_KUDI,true,3);
	_execute(0,char_set_info,ally_get_char(NAME_KUDI),GAME_CHAR_LUST,50);
	_execute(0,ally_storage_lock,NAME_KUDI,true);
	
	_var_map_set(VAR_BATTLELOG_ACTIVE,true);
	
	_execute(0,location_unlock,1,2,3,4,5,6,7);
	_execute(0,location_unlock,43,44,45,46,37,47,48,49,50);
	_change_bg("bg",location_get_bg(),FADE_NORMAL_SEC,1.5);
	_map();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
