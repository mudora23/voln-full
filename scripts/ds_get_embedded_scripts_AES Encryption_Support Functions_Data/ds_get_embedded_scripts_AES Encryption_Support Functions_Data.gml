///ds_get_embedded(struture,key/index,...)

var _ds = argument[0], c = argument_count;

for (var i = 1; i < c; ++i) {
    if (is_undefined(_ds)) return undefined;
    if (is_string(argument[i])) {
        _ds = _ds[? argument[i]];
    } else {
        _ds = _ds[| argument[i]];
    }
}   

return _ds;


