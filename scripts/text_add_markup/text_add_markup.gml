///text_add_markup(raw_dialog,TAG_FONT,TAG_COLOR);
/// @description
/// @param raw_dialog
/// @param TAG_FONT
/// @param TAG_COLOR
var text_final = "";
for(var i = 1; i <= string_length(argument0);i++)
{
    text_final+=string_char_at(argument0,i);
    text_final+=string(argument1);   
	text_final+=string(argument2);
}
return text_final;