///postfx_ps_largehit(x,y);
/// @description -
/// @param x
/// @param y

if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);

part_emitter_region(global.ps, global.large_hit1, xp-7, xp+9, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.large_hit1, global.large_hit1, 1);
part_emitter_region(global.ps, global.large_hit2, xp+0, xp+2, yp-1, yp+1, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.large_hit2, global.large_hit2, 1);
part_emitter_region(global.ps, global.large_hit3, xp-7, xp+9, yp-8, yp+8, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.large_hit3, global.large_hit3, 1);