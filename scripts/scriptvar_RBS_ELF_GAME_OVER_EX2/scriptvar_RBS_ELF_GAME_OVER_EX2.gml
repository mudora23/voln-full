///scriptvar_RBS_ELF_GAME_OVER_EX2();
/// @description

if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];

	return "Soon, "+string(ally_name)+" comes to join you.";
}
else
	return "";
