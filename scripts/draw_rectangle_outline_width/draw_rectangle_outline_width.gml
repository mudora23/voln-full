///draw_rectangle_outline_width(x1,y1,x2,y2,col,width,alpha);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param col
/// @param width
/// @param alpha
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var col = argument4;
var width = argument5;
var alpha = argument6;

var old_alpha = draw_get_alpha();
draw_set_alpha(alpha);
draw_line_width_color(x1-width/2,y1,x2+width/2,y1,width,col,col);//top
draw_line_width_color(x2+width/2,y2,x1-width/2,y2,width,col,col);//bot
draw_line_width_color(x1,y1+width/2,x1,y2-width/2,width,col,col);//left
draw_line_width_color(x2,y2-width/2,x2,y1+width/2,width,col,col);//right


draw_set_alpha(old_alpha);