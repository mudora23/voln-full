///player_ally_relieve_all_except(NAME_,NAME_...);
/// @description
/// @param NAME_
/// @param NAME_...

with obj_control
{
	var name_list = ds_list_create();
	var j = 0;
	while(argument_count > j)
	{
		ds_list_add(name_list,argument[j]);
		j++;
	}
	
	var ally_list = var_map[? VAR_ALLY_LIST];
    for(var i = 0;i<ds_list_size(ally_list);i++)
    {
        var ally = ally_list[| i];
		if !ds_list_value_exists(name_list,ally[? GAME_CHAR_NAME])
			ally[? GAME_CHAR_LUST] = 0;
    }
	
	var player = var_map_get(VAR_PLAYER);
	if !ds_list_value_exists(name_list,player[? GAME_CHAR_NAME])
		player[? GAME_CHAR_LUST] = 0;
		
	ds_list_destroy(name_list);
}


