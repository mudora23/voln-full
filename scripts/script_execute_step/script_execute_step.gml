///script_execute_step();
/// @description  
with obj_control
{
    var execute_list = obj_control.var_map[? VAR_EXECUTE_LIST];
    var execute_list_delay = obj_control.var_map[? VAR_EXECUTE_LIST_DELAY];
	var execute_list_arg0 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG0];
    var execute_list_arg1 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG1];
    var execute_list_arg2 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG2];
    var execute_list_arg3 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG3];
    var execute_list_arg4 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG4];
    var execute_list_arg5 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG5];
    var execute_list_arg6 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG6];
    var execute_list_arg7 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG7];
    var execute_list_arg8 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG8];
    var execute_list_arg9 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG9];
    var execute_list_arg10 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG10];
    var execute_list_arg11 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG11];
    var execute_list_arg12 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG12];
    var execute_list_arg13 = obj_control.var_map[? VAR_EXECUTE_LIST_ARG13];


	if ds_list_size(execute_list) == 0
	{
		//script_execute_list_clear(); // bug auto-solving
		ds_list_clear(execute_list);
		ds_list_clear(execute_list_delay);
		ds_list_clear(execute_list_arg0);
		ds_list_clear(execute_list_arg1);
		ds_list_clear(execute_list_arg2);
		ds_list_clear(execute_list_arg3);
		ds_list_clear(execute_list_arg4);
		ds_list_clear(execute_list_arg5);
		ds_list_clear(execute_list_arg6);
		ds_list_clear(execute_list_arg7);
		ds_list_clear(execute_list_arg8);
		ds_list_clear(execute_list_arg9);
		ds_list_clear(execute_list_arg10);
		ds_list_clear(execute_list_arg11);
		ds_list_clear(execute_list_arg12);
		ds_list_clear(execute_list_arg13);
	}
	
    for(var i = 0; i < ds_list_size(execute_list);i++)
    {
        execute_list_delay[| i] -= 1/room_speed;//(1/room_speed);
		
		
		if obj_control.debug_enable
		{
			var error_message = "SCRIPT_EXECUTE_STEP_ERROR: ";
			
			if ds_list_size(execute_list_delay) <= i ||
			   ds_list_size(execute_list_arg0) <= i ||
			   ds_list_size(execute_list_arg1) <= i || 
			   ds_list_size(execute_list_arg2) <= i || 
			   ds_list_size(execute_list_arg3) <= i || 
			   ds_list_size(execute_list_arg4) <= i || 
			   ds_list_size(execute_list_arg5) <= i || 
			   ds_list_size(execute_list_arg6) <= i || 
			   ds_list_size(execute_list_arg7) <= i || 
			   ds_list_size(execute_list_arg8) <= i || 
			   ds_list_size(execute_list_arg9) <= i || 
			   ds_list_size(execute_list_arg10) <= i || 
			   ds_list_size(execute_list_arg11) <= i || 
			   ds_list_size(execute_list_arg12) <= i || 
			   ds_list_size(execute_list_arg13) <= i
			   {
				   error_message += "script_lists_value_missing; ";
			   }
			if asset_get_type(execute_list[| i]) != asset_script error_message += "script_not_exists; ";
			if !is_real(execute_list_delay[| i]) error_message += "script_delay_is_not_a_number; ";
			
			if error_message != "SCRIPT_EXECUTE_STEP_ERROR: "
			{
				error_message += " script_name:"+string(ds_list_find_value(execute_list,i));
				error_message += " script_delay:"+string(ds_list_find_value(execute_list_delay,i));
				error_message += " script_arg0:"+string(ds_list_find_value(execute_list_arg0,i));
				error_message += " script_arg1:"+string(ds_list_find_value(execute_list_arg1,i));
				error_message += " script_arg2:"+string(ds_list_find_value(execute_list_arg2,i));
				error_message += " script_arg3:"+string(ds_list_find_value(execute_list_arg3,i));
				error_message += " script_arg4:"+string(ds_list_find_value(execute_list_arg4,i));
				error_message += " script_arg5:"+string(ds_list_find_value(execute_list_arg5,i));
				error_message += " script_arg6:"+string(ds_list_find_value(execute_list_arg6,i));
				error_message += " script_arg7:"+string(ds_list_find_value(execute_list_arg7,i));
				error_message += " script_arg8:"+string(ds_list_find_value(execute_list_arg8,i));
				error_message += " script_arg9:"+string(ds_list_find_value(execute_list_arg9,i));
				error_message += " script_arg10:"+string(ds_list_find_value(execute_list_arg10,i));
				error_message += " script_arg11:"+string(ds_list_find_value(execute_list_arg11,i));
				error_message += " script_arg12:"+string(ds_list_find_value(execute_list_arg12,i));
				error_message += " script_arg13:"+string(ds_list_find_value(execute_list_arg13,i));
				show_debug_message(error_message);
			}
		}
		
        if execute_list_delay[| i] <= 0
        {
			
			//show_message("script_get_name_ext(execute_list[| i]): "+script_get_name_ext(execute_list[| i]));
			
			script_execute_ext(execute_list[| i],execute_list_arg0[| i],execute_list_arg1[| i],execute_list_arg2[| i],execute_list_arg3[| i],execute_list_arg4[| i],execute_list_arg5[| i],execute_list_arg6[| i],execute_list_arg7[| i],execute_list_arg8[| i],execute_list_arg9[| i],execute_list_arg10[| i],execute_list_arg11[| i],execute_list_arg12[| i],execute_list_arg13[| i]);
   
            ds_list_delete(execute_list,i);
            ds_list_delete(execute_list_delay,i);
            ds_list_delete(execute_list_arg0,i);
            ds_list_delete(execute_list_arg1,i);
            ds_list_delete(execute_list_arg2,i);
            ds_list_delete(execute_list_arg3,i);
            ds_list_delete(execute_list_arg4,i);
            ds_list_delete(execute_list_arg5,i);
            ds_list_delete(execute_list_arg6,i);
            ds_list_delete(execute_list_arg7,i);
            ds_list_delete(execute_list_arg8,i);
            ds_list_delete(execute_list_arg9,i);
            ds_list_delete(execute_list_arg10,i);
            ds_list_delete(execute_list_arg11,i);
            ds_list_delete(execute_list_arg12,i);
            ds_list_delete(execute_list_arg13,i);
			
			i--;
        }
    }
}

