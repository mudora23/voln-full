///ds_list_values_swap(list1,list2);
/// @description
/// @param list1
/// @param list2

var list1 = argument0;
var list2 = argument1;
var list1_new = ds_list_create();
for(var i = 0; i < ds_list_size(list1);i++)
    ds_list_add(list1_new,list1[| i]);

ds_list_clear(list1);
for(var i = 0; i < ds_list_size(list2);i++)
    ds_list_add(list1,list2[| i]);

ds_list_clear(list2);   
for(var i = 0; i < ds_list_size(list1_new);i++)
    ds_list_add(list2,list1_new[| i]);
 
ds_list_destroy(list1_new);