///scene_list_add_type_list(scene_list,type_list);
/// @description  
/// @param scene_list
/// @param type_list

var story_list = argument0;
var type_list = argument1;

for(var i = 0; i < ds_list_size(story_list);i++)
{
	var type = scene_get_type(story_list[| i]);
	ds_list_add_unique(type_list,type);
}
ds_list_sort(type_list,true);
return type_list;