///dl(type1, dat1, type2, dat2, ...)
//types: m: map, l: list, '' (blank): normal
if (argument_count mod 2 != 0)
    show_error("Invalid arguments", false);
var a = ds_list_create();
for (var i = 0; i < argument_count / 2; i += 1) {
    a[| i] = argument[(i * 2) + 1];
    switch (argument[i * 2]) {
        case "m":
            ds_list_mark_as_map(a, i);
            break;
            
        case "l":
            ds_list_mark_as_map(a, i);
            break;
            
        default: break;
    }
}
return a;


