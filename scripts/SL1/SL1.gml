///SL1();
/// @description Nirholt

_label("start");
	if qj_exists("S_0007_SL1_Bandit_Beatdown_Finish_QJ")
		_jump("S_0007_SL1_Bandit_Beatdown_Finish","S_0007");
	else if !chunk_mark_get("SL1_INTRO")
		_jump("SL1_INTRO");
	else
		_jump("SL1_RETURN");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_INTRO");
	_dialog("SL1_INTRO");
	_show("bg",spr_bg_Nirholt_Distant_01_midsun,5,5,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_BG,1); 
	_show("ku",spr_char_Ku_n_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_VERYFAST_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL1_INTRO_2");
	_hide("ku",FADE_NORMAL_SEC);
	_dialog("SL1_INTRO_3");
	_execute(0,ally_dismiss,NAME_KUDI);
	_execute(0,ally_dismiss,NAME_ZEYD);
	_jump("MENU");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("SL1_RETURN");
	_dialog("SL1_RETURN",true);
	_choice("Enter","RETURN");
	_choice("Leave","LEAVE");
	_block();
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("RETURN");
	_dialog("SL1_RETURN_2");
	_jump("MENU");

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("MENU");
	_hide_all_except("bg",FADE_NORMAL_SEC);
	_change_bg("bg",spr_bg_Nirholt_Streets_01_midsun,FADE_NORMAL_SEC,1.5);
	_dialog("SL1_MENU",true);
	
	// Zeni Tą
	if !chunk_mark_get("SL1_TENI_TA_INTRO")
		_choice("An apartment complex","SL1_TENI_TA_INTRO","SL1_TENI_TA");
	else
		_choice("Zeni Tą","SL1_TENI_TA_RETURN","SL1_TENI_TA");
	
	// Gold Silt
	if !chunk_mark_get("SL1_GOLD_SILT_INTRO")
		_choice("A store with gold lettering in the window","SL1_GOLD_SILT_INTRO","SL1_GOLD_SILT");
	else
		_choice("Gold Silt","SL1_GOLD_SILT_RETURN","SL1_GOLD_SILT");
		
	// Lellurd’s
	if !chunk_mark_get("SL1_LELLURDS_INTRO")
		_choice("A gaudy-looking boutique","SL1_LELLURDS_INTRO","SL1_LELLURDS");
	else
		_choice("Lellurd’s","SL1_LELLURDS_RETURN","SL1_LELLURDS");
		
	// Dag’s
	if !chunk_mark_get("SL1_DAGS_INTRO")
		_choice("A pub","SL1_DAGS_INTRO","SL1_DAGS","",true,true,true,"",voln_play_sfx,sfx_Wooden_Door_Open_2);
	else
		_choice("Dag’s","SL1_DAGS_RETURN","SL1_DAGS","",true,true,true,"",voln_play_sfx,sfx_Wooden_Door_Open_2);
		
	// Jam’s
	if chunk_mark_get("S_0005_WON") && !chunk_mark_get("S_0006_2")
		_choice("Jam’s","S_0006_2","S_0006");	
	else if !chunk_mark_get("SL1_JAMS_INTRO")
		_choice("A small, windowless building with a white door","SL1_JAMS_INTRO","SL1_JAMS");
	else
		_choice("Jam’s","SL1_JAMS_RETURN","SL1_JAMS");	
		
	// Meet Hut
	if !chunk_mark_get("SL1_MEET_HUT_INTRO")
		_choice("What must be the hostelry Kûdi mentioned","SL1_MEET_HUT_INTRO","SL1_MEET_HUT");
	else
		_choice("Meet Hut","SL1_MEET_HUT_RETURN","SL1_MEET_HUT","",true,true,true,"",voln_play_sfx,sfx_Wooden_Door_Open_2);
		
	// Hûn Dao’a
	if qj_exists("S_0005_FINDING_ZEYD_QJ") && chunk_mark_get("SL1_HUN_DAOA_INTRO") && !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE")
		_choice("Hûn Dao’a","S_0005_1B","S_0005");
	else if qj_exists("S_0005_FINDING_ZEYD_QJ") && chunk_mark_get("SL1_HUN_DAOA_INTRO")
		_choice("Hûn Dao’a","S_0005_7","S_0005");
	else if !chunk_mark_get("SL1_HUN_DAOA_INTRO")
		_choice("A passage to the subway","SL1_HUN_DAOA_INTRO","SL1_HUN_DAOA");
	else
		_choice("Hûn Dao’a","SL1_HUN_DAOA_RETURN","SL1_HUN_DAOA");	
		
	// Yim Trek
	if !chunk_mark_get("SL1_YIM_TREK_INTRO")
		_choice("A raceway","SL1_YIM_TREK_INTRO","SL1_YIM_TREK");
	else
		_choice("Yim Trek","SL1_YIM_TREK_RETURN","SL1_YIM_TREK");		
		
	// Bred Best
	if qj_exists("S_0004_FELGOR_INTRO_EVENT_B_16_B_5_QJ")
		_choice("Bred Best","S_0004_FELGOR_INTRO_EVENT2","S_0004");
	else if !chunk_mark_get("SL1_BRED_BEST_INTRO")
		_choice("Stables","SL1_BRED_BEST_INTRO","SL1_BRED_BEST");
	else
		_choice("Bred Best","SL1_BRED_BEST_RETURN","SL1_BRED_BEST");		
		
	// Old Yoji
	if !chunk_mark_get("SL1_OLD_YOJI_INTRO")
		_choice("A barn","SL1_OLD_YOJI_INTRO","SL1_OLD_YOJI");
	else if !chunk_mark_get("SL43_OLD_YOJI")
		_choice("Old Yoji","SL1_OLD_YOJI_RETURN","SL1_OLD_YOJI");
	else
		_choice("Old Yoji","SL1_OLD_YOJI_RETURN_OPEN","SL1_OLD_YOJI");	
	
	
	// Leave
	if qj_exists("S_0005_FINDING_ZEYD_QJ") && chunk_mark_get("SL1_HUN_DAOA_INTRO") && !chunk_mark_get("S_0005_SL1_HUN_DAOA_INSIDE")
		_choice("Leave","S_0005_1A","S_0005");
	else if qj_exists("S_0005_FINDING_ZEYD_QJ") && chunk_mark_get("SL1_HUN_DAOA_INTRO")
		_choice("Leave","S_0005_6_1","S_0005");
	else if chunk_mark_get("S_0005_WON") && !chunk_mark_get("S_0006_2")
		_choice("Leave","S_0006_1","S_0006");
	else if chunk_mark_get("S_0007_SL1_Bandit_Beatdown_Finish") && !chunk_mark_get("S_0008_MAKEOVER_1") && (!chunk_mark_get("S_0008_FELGORKUDI_JOIN_1") || !ally_exists(NAME_KUDI) || !ally_exists(NAME_FELGOR) || !ally_exists(NAME_ZEYD))
		_choice("Leave","S_0008_FELGORKUDI_JOIN","S_0008");
	else
		_choice("Leave","LEAVE");	
	_block();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////

_label("LEAVE");
	_map();

////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////














