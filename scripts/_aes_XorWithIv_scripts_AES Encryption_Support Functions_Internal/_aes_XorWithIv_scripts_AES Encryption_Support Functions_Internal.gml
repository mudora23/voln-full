///_aes_XorWithIv(string,IV)

var i, buf = argument0, Iv = argument1, KEYLEN = array_length_1d(Iv);

for(i = 0; i < KEYLEN; ++i)
{
  buf[@i] ^= Iv[@i];
}


