///scriptvar_RBS_ORK_GAME_OVER_EX1();
/// @description

if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];
	return "You’re too weak to adjust yourself to check on "+string(ally_name)+".  How pathetic you feel, spent so completely.  ";
}
else
	return "";
