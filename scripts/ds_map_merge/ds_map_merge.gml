///ds_map_merge(map1, map2)
//merges map2 onto map1
var map1 = argument0,
    map2 = argument1;
    
var size = ds_map_size(map2);
var key = ds_map_find_first(map2);
for (var i = 0; i < size; i++;)
{
    map1[? key] = map2[? key];
    key = ds_map_find_next(map2, key);
}

return map1;


