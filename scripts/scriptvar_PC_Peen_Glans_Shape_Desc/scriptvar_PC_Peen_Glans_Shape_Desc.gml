///scriptvar_PC_Peen_Glans_Shape_Desc();
/// @description
if var_map_get(PC_Peen_Glans_Shape_N) == 0
    return choose("spade-shaped","slightly pointed");
else if var_map_get(PC_Peen_Glans_Shape_N) == 1
    return choose("bell-shaped","round");
else
    return choose("spear-shaped","pointed");