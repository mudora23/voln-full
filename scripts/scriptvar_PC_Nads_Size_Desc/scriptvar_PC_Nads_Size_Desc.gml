///scriptvar_PC_Nads_Size_Desc();
/// @description
var num = var_map_get(PC_Nads_Size_N);
if num < 0.9 return choose("small","little");
else if num < 1.1 return choose("proportionate","appreciable");
else if num < 2 return choose("big","substantial","large","full");
else if num < 3 return choose("bloated","oversized","huge","swollen-looking","bulbous");
else return choose("insanely big","monstrous","titanic","massive","horrifying");