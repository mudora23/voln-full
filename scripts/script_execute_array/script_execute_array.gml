///script_execute_array(script,array_of_args);
///@description
/// @param script
/// @apram array_of_args
var array_l = array_length_1d(argument1);
if array_l > 15 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9],argument1[10],argument1[11],argument1[12],argument1[13],argument1[14],argument1[15]);
else if array_l > 14 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9],argument1[10],argument1[11],argument1[12],argument1[13],argument1[14]);
else if array_l > 13 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9],argument1[10],argument1[11],argument1[12],argument1[13]);
else if array_l > 12 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9],argument1[10],argument1[11],argument1[12]);
else if array_l > 11 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9],argument1[10],argument1[11]);
else if array_l > 10 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9],argument1[10]);
else if array_l > 9 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8],argument1[9]);
else if array_l > 8 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7],argument1[8]);
else if array_l > 7 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6],argument1[7]);
else if array_l > 6 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5],argument1[6]);
else if array_l > 5 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4],argument1[5]);
else if array_l > 4 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3],argument1[4]);
else if array_l > 3 script_execute(argument0,argument1[0],argument1[1],argument1[2],argument1[3]);
else if array_l > 2 script_execute(argument0,argument1[0],argument1[1],argument1[2]);
else if array_l > 1 script_execute(argument0,argument1[0],argument1[1]);
else if array_l > 0 script_execute(argument0,argument1[0]);
else script_execute(argument0);








