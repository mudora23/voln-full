///battle_process_choose_item_CMD();
/// @description
with obj_control
{
	scene_choices_list_clear();

	var name_list = obj_control.var_map[? VAR_INVENTORY_LIST];
	var amount_list = obj_control.var_map[? VAR_INVENTORY_AMOUNT_LIST];
	
	for(var i = 0; i < ds_list_size(name_list) ;i++)
	{
		if item_can_use_in_combat(name_list[| i])
			scene_choiceCMD_add(item_get_displayname(name_list[| i]),battle_process_choose_CMD,true,"battle_process_item_CMD",name_list[| i]);
	}
	scene_choiceCMD_add("Cancel",battle_process_player);
}