///draw_skill_icon(x,y,SKILL);
/// @description
/// @param x
/// @param y
/// @param SKILL
var xx = argument0;
var yy = argument1;
var the_char = obj_control.var_map[? VAR_PLAYER];
var the_skill = argument2;
var the_skill_map = obj_control.skill_wiki_map[? the_skill];
var the_skill_sprite = the_skill_map[? SKILLICON];
var the_kill_name = the_skill_map[? SKILLDISPLAYNAME]
var the_kill_des = the_skill_map[? SKILLDES]
var the_skill_req_list = the_skill_map[? REQUIREMENT]; #macro REQUIREMENT "REQUIREMENT"
var the_skill_level_max = floor(ds_list_size(the_skill_req_list) / 7);
var the_skill_level_current = char_get_skill_level(the_char,the_skill);

var spr_skill_over = mouse_over_area_center(xx,yy,sprite_get_width(spr_skill_PH),sprite_get_height(spr_skill_PH));
if spr_skill_over var spr_color_blending = c_yellow;
else var spr_color_blending = c_white;

var req_passed = false;




/*if the_skill == SKILL_PH
{
	draw_sprite_ext(spr_skill_PH,-1,xx,yy,1,1,0,spr_color_blending,1);
}
if the_skill == SKILL_FIREBALL
{
	draw_sprite_ext(spr_skill_fireball,-1,xx,yy,1,1,0,spr_color_blending,1);
}
if the_skill == SKILL_DOUBLE_HIT
{
	draw_sprite_ext(spr_skill_doublehit,-1,xx,yy,1,1,0,spr_color_blending,1);
}
if the_skill == SKILL_THUNDERSTORM
{
	draw_sprite_ext(spr_skill_thunderstorm,-1,xx,yy,1,1,0,spr_color_blending,1);
}
if the_skill == SKILL_OCCULT
{
	draw_sprite_ext(spr_skill_occult,-1,xx,yy,1,1,0,spr_color_blending,1);
}*/

draw_sprite_ext(the_skill_sprite,-1,xx,yy,1,1,0,spr_color_blending,1);

if the_skill != SKILL_PH && the_skill_level_current < 1
{
	
	draw_sprite_ext(spr_lock,0,xx,yy,sprite_get_width(the_skill_sprite)/sprite_get_width(spr_lock),sprite_get_height(the_skill_sprite)/sprite_get_height(spr_lock),0,c_white,0.9);
}



if the_skill != SKILL_PH && spr_skill_over
{
	if char_get_skill_level(the_char,the_skill) > 0
		var tooltip_str = the_kill_name + " LV" + string(char_get_skill_level(the_char,the_skill));
	else
		var tooltip_str = the_kill_name;
		
		
	tooltip_str+=" - "+the_kill_des;

	if char_get_skill_level(the_char,the_skill) < the_skill_level_max
	{
		var req_fail_any = false;
		
		if char_get_skill_level(the_char,the_skill) > 0
			tooltip_str +=" (LV"+string(char_get_skill_level(the_char,the_skill)+1)+" req.: ";
		else
			tooltip_str +=" (Req.: ";
			
		var stat_index = char_get_skill_level(the_char,the_skill) * 7;
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="STR "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_STR(the_char)
				req_fail_any = true;
		}
		stat_index++;
	
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="FOU "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_FOU(the_char)
				req_fail_any = true;
		}
		stat_index++;
	
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="DEX "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_DEX(the_char)
				req_fail_any = true;
		}
		stat_index++;
	
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="END "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_END(the_char)
				req_fail_any = true;
		}
		stat_index++;
	
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="INT "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_INT(the_char)
				req_fail_any = true;
		}
		stat_index++;
	
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="WIS "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_WIS(the_char)
				req_fail_any = true;
		}
		stat_index++;
	
		if the_skill_req_list[| stat_index] != 0
		{
			tooltip_str+="SPI "+string(the_skill_req_list[| stat_index])+"  ";
			if the_skill_req_list[| stat_index] > char_get_SPI(the_char)
				req_fail_any = true;
		}
	
		tooltip_str+=")";
		
		

		
		
		if !req_fail_any && obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS] > 0
		{
			draw_sprite_ext(spr_button_polygon_levelup,-1,xx,yy,0.5,0.5,0,c_white,draw_get_alpha());
			if action_button_pressed_get()
			{
				if ds_map_exists(the_char[? GAME_CHAR_SKILLS_MAP],the_skill)
					ds_map_replace(the_char[? GAME_CHAR_SKILLS_MAP],the_skill,ds_map_find_value(the_char[? GAME_CHAR_SKILLS_MAP],the_skill)+1);
				else
					ds_map_add(the_char[? GAME_CHAR_SKILLS_MAP],the_skill,1);
				obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS]--; 
			}
	
		}
		
	}

	obj_menu.tooltip = tooltip_str;	
}











