///skill_wiki_map_init();
/// @description
with obj_control
{
	skill_wiki_map = ds_map_create();
	
	#macro SKILLDISPLAYNAME "SKILLDISPLAYNAME"
	#macro SKILLDES "SKILLDES"
	#macro SKILLICON "SKILLICON"
	#macro SKILLTEXT "SKILLTEXT"
	#macro SKILLTEXTCOL "SKILLTEXTCOL"
	#macro SKILLTEXTCOLLIGHT "SKILLTEXTCOLLIGHT"
	#macro SKILLCATEGORY "CATEGORY"
		#macro SKILLCATEGORY_MIGHT "SKILLCATEGORY_MIGHT"
		#macro SKILLCATEGORY_TORU "SKILLCATEGORY_TORU"
		#macro SKILLCATEGORY_STEALTH "SKILLCATEGORY_STEALTH"
	#macro SKILLX "SKILL_X"
	#macro SKILLY "SKILL_Y"
	#macro SKILL_INHERIT_CHANCE "SKILL_INHERIT_CHANCE"
	#macro SKILL_REQ_LEVEL_LIST "SKILL_REQ_LEVEL_LIST"
	#macro SKILL_REQ_SKILL_LIST "SKILL_REQ_SKILL_LIST"
	#macro SKILL_VITALITY_LIST "SKILL_VITALITY_LIST"
	#macro SKILL_TORU_LIST "SKILL_TORU_LIST"
	#macro SKILL_STAMINA_LIST "SKILL_STAMINA_LIST"

		
	#macro SKILL_START "SKILL_START"
	ds_map_add_unique_map(skill_wiki_map,SKILL_START,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_START];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_start);
		ds_map_add_unique(skill_map,SKILLCATEGORY,"");
		ds_map_add_unique(skill_map,SKILLTEXT,"");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,c_white);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,c_white);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
	skill_add_missing_keys(skill_map);

	#macro SKILL_END_1 "SKILL_END_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_END_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_END_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endurance Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_end);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"END");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+0*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+0*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_END_2 "SKILL_END_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_END_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_END_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endurance Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_end);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"END");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+0*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+0*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_END_3 "SKILL_END_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_END_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_END_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endurance Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_end);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"END");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+0*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+0*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
	
		#macro SKILL_DEFENSE_UP "SKILL_DEFENSE_UP"
		ds_map_add_unique_map(skill_wiki_map,SKILL_DEFENSE_UP,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_DEFENSE_UP];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Defense Up");
			ds_map_add_unique(skill_map,SKILLDES,"Significantly increase the defense of an ally.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_defense_up);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
			ds_map_add_unique(skill_map,SKILLTEXT,"Defense Up");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(1200,38.5+6.7*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(1200,38.5+6.7*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],2,6,10);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],50,70,90);
		skill_add_missing_keys(skill_map);
			
	#macro SKILL_END_4 "SKILL_END_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_END_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_END_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endurance Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_end);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"END");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+0*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+0*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_END_5 "SKILL_END_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_END_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_END_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endurance Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_end);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"END");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+0*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+0*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
	
	
		#macro SKILL_BODYGUARD "SKILL_BODYGUARD"
		ds_map_add_unique_map(skill_wiki_map,SKILL_BODYGUARD,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_BODYGUARD];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Bodyguard");
			ds_map_add_unique(skill_map,SKILLDES,"Enemies can only target this character.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_bodyguard);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
			ds_map_add_unique(skill_map,SKILLTEXT,"Bodyguard");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(1800,38.5+0.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(1800,38.5+0.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.2);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_5);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_5);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],19,22,25);
			ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_STAMINA_LIST],50,60,70);
		skill_add_missing_keys(skill_map);
			
	#macro SKILL_END_6 "SKILL_END_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_END_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_END_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endurance Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_end);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"END");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+0*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+0*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
		
	#macro SKILL_FOU_1 "SKILL_FOU_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FOU_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FOU_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Foundation Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_fou);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"FOU");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+1*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+1*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_FOU_2 "SKILL_FOU_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FOU_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FOU_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Foundation Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_fou);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"FOU");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);

		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+1*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+1*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_FOU_3 "SKILL_FOU_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FOU_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FOU_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Foundation Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_fou);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"FOU");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+1*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+1*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_FOU_4 "SKILL_FOU_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FOU_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FOU_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Foundation Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_fou);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"FOU");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+1*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+1*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_FOU_5 "SKILL_FOU_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FOU_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FOU_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Foundation Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_fou);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"FOU");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+1*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+1*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);

	#macro SKILL_FOU_6 "SKILL_FOU_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FOU_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FOU_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Foundation Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_fou);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"FOU");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+1*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+1*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);

	#macro SKILL_STR_1 "SKILL_STR_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STR_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STR_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Strength Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_str);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"STR");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+2*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+2*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_STR_2 "SKILL_STR_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STR_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STR_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Strength Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_str);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"STR");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+2*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+2*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_STR_3 "SKILL_STR_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STR_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STR_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Strength Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_str);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"STR");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+2*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+2*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
			#macro SKILL_BAM "SKILL_BAM"
			ds_map_add_unique_map(skill_wiki_map,SKILL_BAM,ds_map_create());
				var skill_map = skill_wiki_map[? SKILL_BAM];
				ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Bam");
				ds_map_add_unique(skill_map,SKILLDES,"Bam");
				ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
				ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
				ds_map_add_unique(skill_map,SKILLTEXT,"Bam");
				ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
				ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
				//ds_map_add_unique(skill_map,SKILLX,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLX)+lengthdir_x(300,48)); 
				//ds_map_add_unique(skill_map,SKILLY,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLY)+lengthdir_y(300,48));
				ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.5);
				ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
					ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_2);
				ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],3,5,12);
				ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_STAMINA_LIST],40,50,60);
				ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_TORU_LIST],100,120,140);
			skill_add_missing_keys(skill_map);
			
			#macro SKILL_TACKLE "SKILL_TACKLE"
			ds_map_add_unique_map(skill_wiki_map,SKILL_TACKLE,ds_map_create());
				var skill_map = skill_wiki_map[? SKILL_TACKLE];
				ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Tackle");
				ds_map_add_unique(skill_map,SKILLDES,"Tackle an enemy.");
				ds_map_add_unique(skill_map,SKILLICON,spr_skill_tackle);
				ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
				ds_map_add_unique(skill_map,SKILLTEXT,"Tackle");
				ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
				ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
				ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(1200,38.5+1.5*360/7));
				ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(1200,38.5+1.5*360/7));
				ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.3);
				ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
					ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_3);
					ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_FOU_3);
				ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],10,12,18);
				ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_STAMINA_LIST],30,40,50);
			skill_add_missing_keys(skill_map);
			
					
			#macro SKILL_BAM2 "SKILL_BAM2"
			ds_map_add_unique_map(skill_wiki_map,SKILL_BAM2,ds_map_create());
				var skill_map = skill_wiki_map[? SKILL_BAM2];
				ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"BAM!!");
				ds_map_add_unique(skill_map,SKILLDES,"BAM!!");
				ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
				ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
				ds_map_add_unique(skill_map,SKILLTEXT,"BAM!!");
				ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
				ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
				//ds_map_add_unique(skill_map,SKILLX,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLX)+lengthdir_x(300,48)); 
				//ds_map_add_unique(skill_map,SKILLY,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLY)+lengthdir_y(300,48));
				ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.4);
				ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
					ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_2);
				ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],5,8,15);
				ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_STAMINA_LIST],60,70,80);
				ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
					ds_list_add(skill_map[? SKILL_TORU_LIST],60,80,100);
			skill_add_missing_keys(skill_map);
			
	#macro SKILL_STR_4 "SKILL_STR_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STR_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STR_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Strength Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_str);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"STR");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+2*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+2*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_STR_5 "SKILL_STR_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STR_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STR_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Strength Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_str);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"STR");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+2*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+2*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
			
		#macro SKILL_WRECK "SKILL_WRECK"
		ds_map_add_unique_map(skill_wiki_map,SKILL_WRECK,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_WRECK];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wreck");
			ds_map_add_unique(skill_map,SKILLDES,"Reckless anger can have serious consequences.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_wreck);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
			ds_map_add_unique(skill_map,SKILLTEXT,"Wreck");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLX)+lengthdir_x(300,48)); 
		ds_map_add_unique(skill_map,SKILLY,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLY)+lengthdir_y(300,48));
		ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.1);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_5);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],20,23,28);
			ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_STAMINA_LIST],50,60,70);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_STR_6 "SKILL_STR_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STR_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STR_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Strength Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_str);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLTEXT,"STR");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_HP_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_HP);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+2*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+2*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
		
	#macro SKILL_DEX_1 "SKILL_DEX_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DEX_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DEX_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Dexterity Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_dex);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"DEX");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+3*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+3*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);
			
		#macro SKILL_DOUBLE_HIT "SKILL_DOUBLE_HIT"
		ds_map_add_unique_map(skill_wiki_map,SKILL_DOUBLE_HIT,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_DOUBLE_HIT];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Double Hit");
			ds_map_add_unique(skill_map,SKILLDES,"Throwing two strikes is twice as nice.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_doublehit);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
			ds_map_add_unique(skill_map,SKILLTEXT,"Double Hit");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(600,38.5+2.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(600,38.5+2.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_1);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_STR_1);
			ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,4,10);
				ds_list_add(skill_map[? SKILL_STAMINA_LIST],25,35,45);
		skill_add_missing_keys(skill_map);
				
		#macro SKILL_HIDE "SKILL_HIDE"
		ds_map_add_unique_map(skill_wiki_map,SKILL_HIDE,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_HIDE];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Hide");
			ds_map_add_unique(skill_map,SKILLDES,"Merge with the shadows.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_hide);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
			ds_map_add_unique(skill_map,SKILLTEXT,"Hide");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(600,38.5+3.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(600,38.5+3.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_1);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_1);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],3);
			ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_STAMINA_LIST],30,30,30);
		skill_add_missing_keys(skill_map);
		
	#macro SKILL_DEX_2 "SKILL_DEX_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DEX_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DEX_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Dexterity Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_dex);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"DEX");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+3*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+3*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_DEX_3 "SKILL_DEX_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DEX_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DEX_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Dexterity Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_dex);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"DEX");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+3*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+3*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
		#macro SKILL_ENDLESS_WHIP "SKILL_ENDLESS_WHIP"
		ds_map_add_unique_map(skill_wiki_map,SKILL_ENDLESS_WHIP,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_ENDLESS_WHIP];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Endless whip");
			ds_map_add_unique(skill_map,SKILLDES,"Endless whip");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
			ds_map_add_unique(skill_map,SKILLTEXT,"Endless whip");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
			//ds_map_add_unique(skill_map,SKILLX,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLX)+lengthdir_x(300,48)); 
			//ds_map_add_unique(skill_map,SKILLY,ds_map_find_value(ds_map_find_value(skill_wiki_map,SKILL_STR_5),SKILLY)+lengthdir_y(300,48));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.2);
			//ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			//	ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],4,6,10);
			ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_STAMINA_LIST],70,80,90);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],40,50,60);
		skill_add_missing_keys(skill_map);
			
			
		#macro SKILL_SPEED_UP "SKILL_SPEED_UP"
		ds_map_add_unique_map(skill_wiki_map,SKILL_SPEED_UP,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_SPEED_UP];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Speed Up");
			ds_map_add_unique(skill_map,SKILLDES,"Increase the speed of an ally.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_speed_up);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Speed Up");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(1200,38.5+3.3*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(1200,38.5+3.3*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],6,9,12);
			ds_map_add_unique_list(skill_map,SKILL_STAMINA_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_STAMINA_LIST],30,40,50);
		skill_add_missing_keys(skill_map);
			
	#macro SKILL_DEX_4 "SKILL_DEX_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DEX_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DEX_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Dexterity Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_dex);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"DEX");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+3*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+3*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_DEX_5 "SKILL_DEX_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DEX_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DEX_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Dexterity Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_dex);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"DEX");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+3*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+3*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_DEX_6 "SKILL_DEX_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DEX_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DEX_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Dexterity Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_dex);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"DEX");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+3*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+3*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_DEX_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_WIS_1 "SKILL_WIS_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WIS_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WIS_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wisdom Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_wis);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"WIS");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+4*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+4*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_WIS_2 "SKILL_WIS_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WIS_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WIS_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wisdom Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_wis);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"WIS");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+4*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+4*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_WIS_3 "SKILL_WIS_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WIS_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WIS_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wisdom Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_wis);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"WIS");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+4*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+4*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_WIS_4 "SKILL_WIS_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WIS_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WIS_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wisdom Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_wis);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"WIS");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+4*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+4*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_WIS_5 "SKILL_WIS_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WIS_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WIS_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wisdom Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_wis);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"WIS");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+4*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+4*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_WIS_6 "SKILL_WIS_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WIS_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WIS_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wisdom Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_wis);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLTEXT,"WIS");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_STAMINA_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_STAMINA);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+4*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+4*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
		
	#macro SKILL_INT_1 "SKILL_INT_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_INT_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_INT_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Intellect Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_int);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"INT");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+5*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+5*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);

	#macro SKILL_INT_2 "SKILL_INT_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_INT_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_INT_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Intellect Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_int);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"INT");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+5*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+5*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_1);
	skill_add_missing_keys(skill_map);
			
		#macro SKILL_OCCULT "SKILL_OCCULT"
		ds_map_add_unique_map(skill_wiki_map,SKILL_OCCULT,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_OCCULT];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Occult");
			ds_map_add_unique(skill_map,SKILLDES,"In a dark world, the power of temptation corrupts all.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_occult);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Occult");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+5.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+5.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.2);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_3);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],15,17,20);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],100,120,150);
		skill_add_missing_keys(skill_map);
			
	#macro SKILL_INT_3 "SKILL_INT_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_INT_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_INT_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Intellect Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_int);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"INT");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+5*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+5*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
	
		#macro SKILL_FIREBALL "SKILL_FIREBALL"
		ds_map_add_unique_map(skill_wiki_map,SKILL_FIREBALL,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_FIREBALL];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Fireball");
			ds_map_add_unique(skill_map,SKILLDES,"Ignite the air itself with a scorching missile.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_fireball);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Fireball");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+4.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+4.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.2);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_3);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_WIS_3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],6,8,12);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],30,50,80);
		skill_add_missing_keys(skill_map);
			
	#macro SKILL_INT_4 "SKILL_INT_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_INT_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_INT_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Intellect Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_int);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"INT");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+5*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+5*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);
			
	#macro SKILL_INT_5 "SKILL_INT_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_INT_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_INT_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Intellect Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_int);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"INT");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+5*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+5*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
			
		#macro SKILL_THUNDERSTORM "SKILL_THUNDERSTORM"
		ds_map_add_unique_map(skill_wiki_map,SKILL_THUNDERSTORM,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_THUNDERSTORM];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Thunderstorm");
			ds_map_add_unique(skill_map,SKILLDES,"Call down electric energy from the green heavens.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_thunderstorm);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Thunderstorm");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+5.3*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+5.3*360/7));
		ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.2);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_5);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],18,20,25);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],130,180,220);
		skill_add_missing_keys(skill_map);
		
		#macro SKILL_BLIZZARD "SKILL_BLIZZARD"
		ds_map_add_unique_map(skill_wiki_map,SKILL_BLIZZARD,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_BLIZZARD];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Blizzard");
			ds_map_add_unique(skill_map,SKILLDES,"AOE toru freezing attack.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_blizzard);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Blizzard");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+4.7*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+4.7*360/7));
		ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.2);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_5);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],18,20,25);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],130,180,220);
		skill_add_missing_keys(skill_map);
			
	#macro SKILL_INT_6 "SKILL_INT_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_INT_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_INT_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Intellect Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_int);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"INT");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+5*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+5*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_INT_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);
		
	#macro SKILL_SPI_1 "SKILL_SPI_1"
	ds_map_add_unique_map(skill_wiki_map,SKILL_SPI_1,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_SPI_1];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Spirit Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_spi);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"SPI");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300,38.5+6*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300,38.5+6*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_START);
	skill_add_missing_keys(skill_map);
			
		#macro SKILL_HEAL "SKILL_HEAL"
		ds_map_add_unique_map(skill_wiki_map,SKILL_HEAL,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_HEAL];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Heal");
			ds_map_add_unique(skill_map,SKILLDES,"The power to mend flesh.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_heal);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Heal");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(600,38.5+6.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(600,38.5+6.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.4);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_1);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_END_1);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],3,8,14);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],20,40,60);
		skill_add_missing_keys(skill_map);
		
	#macro SKILL_SPI_2 "SKILL_SPI_2"
	ds_map_add_unique_map(skill_wiki_map,SKILL_SPI_2,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_SPI_2];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Spirit Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_spi);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"SPI");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+6*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+6*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_1);
	skill_add_missing_keys(skill_map);
	
		#macro SKILL_CLEANSE "SKILL_CLEANSE"
		ds_map_add_unique_map(skill_wiki_map,SKILL_CLEANSE,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_CLEANSE];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Cleanse");
			ds_map_add_unique(skill_map,SKILLDES,"Remove all de-buffs of an ally.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_cleanse);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Cleanse");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*2,38.5+6.5*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*2,38.5+6.5*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.4);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_HEAL);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],20);
		skill_add_missing_keys(skill_map);

	#macro SKILL_SPI_3 "SKILL_SPI_3"
	ds_map_add_unique_map(skill_wiki_map,SKILL_SPI_3,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_SPI_3];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Spirit Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_spi);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"SPI");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*3,38.5+6*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*3,38.5+6*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_2);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);

		#macro SKILL_BARRIER "SKILL_BARRIER"
		ds_map_add_unique_map(skill_wiki_map,SKILL_BARRIER,ds_map_create());
			var skill_map = skill_wiki_map[? SKILL_BARRIER];
			ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Barrier");
			ds_map_add_unique(skill_map,SKILLDES,"This spell produces a field of energy that acts as a shield, covering the caster and their allies.");
			ds_map_add_unique(skill_map,SKILLICON,spr_skill_barrier);
			ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
			ds_map_add_unique(skill_map,SKILLTEXT,"Barrier");
			ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
			ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
			ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(1200,38.5+6.3*360/7));
			ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(1200,38.5+6.3*360/7));
			ds_map_add_unique(skill_map,SKILL_INHERIT_CHANCE,0.3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_3);
				ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_3);
			ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,8,15);
			ds_map_add_unique_list(skill_map,SKILL_TORU_LIST,ds_list_create());
				ds_list_add(skill_map[? SKILL_TORU_LIST],80,120,160);
		skill_add_missing_keys(skill_map);


	#macro SKILL_SPI_4 "SKILL_SPI_4"
	ds_map_add_unique_map(skill_wiki_map,SKILL_SPI_4,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_SPI_4];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Spirit Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_spi);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"SPI");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*4,38.5+6*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*4,38.5+6*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_3);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1);
	skill_add_missing_keys(skill_map);


	#macro SKILL_SPI_5 "SKILL_SPI_5"
	ds_map_add_unique_map(skill_wiki_map,SKILL_SPI_5,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_SPI_5];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Spirit Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_spi);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"SPI");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*5,38.5+6*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*5,38.5+6*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_4);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);

	#macro SKILL_SPI_6 "SKILL_SPI_6"
	ds_map_add_unique_map(skill_wiki_map,SKILL_SPI_6,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_SPI_6];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Spirit Training");
		ds_map_add_unique(skill_map,SKILLDES,"");
		ds_map_add_unique(skill_map,SKILLICON,spr_button_stats_spi);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLTEXT,"SPI");
		ds_map_add_unique(skill_map,SKILLTEXTCOL,COLOR_TORU_DARK);
		ds_map_add_unique(skill_map,SKILLTEXTCOLLIGHT,COLOR_TORU);
		ds_map_add_unique(skill_map,SKILLX,0+lengthdir_x(300+300*6,38.5+6*360/7));
		ds_map_add_unique(skill_map,SKILLY,0+lengthdir_y(300+300*6,38.5+6*360/7));
		ds_map_add_unique_list(skill_map,SKILL_REQ_SKILL_LIST,ds_list_create());
			ds_list_add_unique(skill_map[? SKILL_REQ_SKILL_LIST],SKILL_SPI_5);
		ds_map_add_unique_list(skill_map,SKILL_REQ_LEVEL_LIST,ds_list_create());
			ds_list_add(skill_map[? SKILL_REQ_LEVEL_LIST],1,1,1);
	skill_add_missing_keys(skill_map);






/*

	#macro SKILL_PH "SKILL_PH"
	ds_map_add_unique_map(skill_wiki_map,SKILL_PH,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_PH];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"SKILL_PH");
		ds_map_add_unique(skill_map,SKILLDES,"SKILL_PH_DES");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
		
			
	#macro SKILL_PHY_ATK "SKILL_PHY_ATK"
	ds_map_add_unique_map(skill_wiki_map,SKILL_PHY_ATK,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_PHY_ATK];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Basic attack");
		ds_map_add_unique(skill_map,SKILLDES,"a basic attack.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
	
	#macro SKILL_TORU_ATK "SKILL_TORU_ATK"
	ds_map_add_unique_map(skill_wiki_map,SKILL_TORU_ATK,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_TORU_ATK];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Toru attack");
		ds_map_add_unique(skill_map,SKILLDES,"a toru attack.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());

	#macro SKILL_FLIRT "SKILL_FLIRT"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FLIRT,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FLIRT];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Flirt");
		ds_map_add_unique(skill_map,SKILLDES,"a flirt attack.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());

	#macro SKILL_PHY_ARMOR_TORU_ARMOR_UP "SKILL_PHY_ARMOR_TORU_ARMOR_UP"
	ds_map_add_unique_map(skill_wiki_map,SKILL_PHY_ARMOR_TORU_ARMOR_UP,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_PHY_ARMOR_TORU_ARMOR_UP];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Might");
		ds_map_add_unique(skill_map,SKILLDES,"Might.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());

	#macro SKILL_DOUBLE_HIT "SKILL_DOUBLE_HIT"
	ds_map_add_unique_map(skill_wiki_map,SKILL_DOUBLE_HIT,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_DOUBLE_HIT];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Double hit");
		ds_map_add_unique(skill_map,SKILLDES,"Throwing two strikes is twice as nice.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_doublehit);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
	
	#macro SKILL_FIREBALL "SKILL_FIREBALL"
	ds_map_add_unique_map(skill_wiki_map,SKILL_FIREBALL,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_FIREBALL];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Fireball");
		ds_map_add_unique(skill_map,SKILLDES,"Ignite the air itself with a scorching missile.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_fireball);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());

	#macro SKILL_OCCULT "SKILL_OCCULT"
	ds_map_add_unique_map(skill_wiki_map,SKILL_OCCULT,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_OCCULT];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Occult");
		ds_map_add_unique(skill_map,SKILLDES,"In a dark world, the power of temptation corrupts all.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_occult);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
	

	#macro SKILL_THUNDERSTORM "SKILL_THUNDERSTORM"
	ds_map_add_unique_map(skill_wiki_map,SKILL_THUNDERSTORM,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_THUNDERSTORM];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Thunderstorm");
		ds_map_add_unique(skill_map,SKILLDES,"Call down electric energy from the green heavens.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_thunderstorm);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());

	#macro SKILL_THIEF "SKILL_THIEF"
	ds_map_add_unique_map(skill_wiki_map,SKILL_THIEF,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_THIEF];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Thief");
		ds_map_add_unique(skill_map,SKILLDES,"Thief sense.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
			
	#macro SKILL_I_AM_THE_MAIN_CHARACTER "SKILL_I_AM_THE_MAIN_CHARACTER"
	ds_map_add_unique_map(skill_wiki_map,SKILL_I_AM_THE_MAIN_CHARACTER,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_I_AM_THE_MAIN_CHARACTER];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"I AM THE MAIN CHARACTER");
		ds_map_add_unique(skill_map,SKILLDES,"Main Character power.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_PH);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
			
			
			
			
			
			
			
    #macro SKILL_STEALTH "SKILL_STEALTH"
	ds_map_add_unique_map(skill_wiki_map,SKILL_STEALTH,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_STEALTH];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Stealth");
		ds_map_add_unique(skill_map,SKILLDES,"Merge with the shadows.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_hide);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
	
	#macro SKILL_HEAL "SKILL_HEAL"
	ds_map_add_unique_map(skill_wiki_map,SKILL_HEAL,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_HEAL];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Heal");
		ds_map_add_unique(skill_map,SKILLDES,"The power to mend flesh.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_heal);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_TORU);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
			
	#macro SKILL_WRECK "SKILL_WRECK"
	ds_map_add_unique_map(skill_wiki_map,SKILL_WRECK,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_WRECK];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Wreck");
		ds_map_add_unique(skill_map,SKILLDES,"Reckless anger can have serious consequences.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_wreck);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_MIGHT);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());
			
	#macro SKILL_HIDE "SKILL_HIDE"
	ds_map_add_unique_map(skill_wiki_map,SKILL_HIDE,ds_map_create());
		var skill_map = skill_wiki_map[? SKILL_HIDE];
		ds_map_add_unique(skill_map,SKILLDISPLAYNAME,"Hide");
		ds_map_add_unique(skill_map,SKILLDES,"Merge with the shadows.");
		ds_map_add_unique(skill_map,SKILLICON,spr_skill_hide);
		ds_map_add_unique(skill_map,SKILLCATEGORY,SKILLCATEGORY_STEALTH);
		ds_map_add_unique(skill_map,SKILLX,0);
		ds_map_add_unique(skill_map,SKILLY,0);
		ds_map_add_unique_list(skill_map,SKILLCONNECTIONLIST,ds_list_create());

*/



	skill_xmax = 0;
	skill_xmin = 0;
	skill_ymax = 0;
	skill_ymin = 0;

	var skill_key = ds_map_find_first(obj_control.skill_wiki_map);
	while !is_undefined(skill_key)
	{
		var skill_map = obj_control.skill_wiki_map[? skill_key];
		
		if ds_map_exists(skill_map,SKILLX) && ds_map_exists(skill_map,SKILLY)
		{
			var skill_x = skill_map[? SKILLX];
			var skill_y = skill_map[? SKILLY];
	
			skill_xmax = max(skill_xmax, skill_x);
			skill_xmin = min(skill_xmin, skill_x);
			skill_ymax = max(skill_ymax, skill_y);
			skill_ymin = min(skill_ymin, skill_y);
		}
		
		skill_key = ds_map_find_next(obj_control.skill_wiki_map,skill_key);
	}	
	
}

