///script_get_name_ext(script_in_any_form);
/// @description
/// @param script_in_any_form
if is_string(argument0)
{
	if asset_get_type(argument0) == asset_script
		return argument0;
	else
		return "";
}
else
{
	if script_exists(argument0)
		return script_get_name(argument0);
	else
		return "";
}	