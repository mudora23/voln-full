///_playernameinput(x,y,jump_label);
/// @description  
/// @param x
/// @param y
/// @param jump_label

if scene_is_current()
{
	if !instance_exists(obj_player_name_input)
	{
		var inst = instance_create_depth(argument0,argument1,DEPTH_MAIN-1,obj_player_name_input);
		inst.jump_label = argument2;
	}
	
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);