///clothes_sets_get_image_index(spr);
/// @description
/// @param spr

if sprite_get_name_ext(argument0) == "" return 0;

var clothes_sets_map = obj_control.var_map[? VAR_CLOTHES_SETS_MAP];
var map_key = ds_map_find_first(clothes_sets_map);
while !is_undefined(map_key)
{
	 if string_count(map_key,sprite_get_name_ext(argument0)) > 0
		return clothes_sets_map[? map_key];
	var map_key = ds_map_find_next(clothes_sets_map,map_key);
}

return 0;

