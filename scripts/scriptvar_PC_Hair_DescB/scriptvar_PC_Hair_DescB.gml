///scriptvar_PC_Hair_DescB();
/// @description
if var_map_get(PC_Hair_N) == 4 && var_map_get(PC_Hair_Length_N) == 4 return "formed into a puffy mound atop your head";
else if var_map_get(PC_Hair_N) == 4 && var_map_get(PC_Hair_Length_N) == 5 return "formed into a cloud-like mass atop your head";
else return "";