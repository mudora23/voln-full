///item_get_type(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	var item_map = item_get_map(name);
	
	if ds_map_exists(item_map,ITEM_TYPE)
		var item_type = item_map[? ITEM_TYPE];
	else
		var item_type = "";
	
	return item_type;
	
}