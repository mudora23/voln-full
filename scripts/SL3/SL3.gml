///SL3();
/// @description Dleyn

_label("start");
	_jump("","CS_location");
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL3_FELGOR_JOIN");
	_dialog("SL3_FELGOR_JOIN");
	_show("felg",spr_char_Felg_c_n,10,XNUM_CENTER,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_SPRITE,CHAR_SPR_RATIO_NORMAL);
	_dialog("SL3_FELGOR_JOIN_2");
	_dialog("SL3_FELGOR_JOIN_3");
	_dialog("SL3_FELGOR_JOIN_4");
	_dialog("SL3_FELGOR_JOIN_5");
	_colorscheme(COLORSCHEME_GETITEM);
	if ally_is_full()
		_dialog("Felgor joined but the team was full. Felgor will wait in town.");
	else
		_dialog("Felgor joined.");
	_execute(0,ally_add_char,NAME_FELGOR,true,char_get_info(obj_control.var_map[? VAR_PLAYER],GAME_CHAR_LEVEL)+1);
	_colorscheme(COLORSCHEME_NORMAL);
	_jump("SL3_Exit");
	
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////
		
_label("SL3_Exit");	
	_map();
		
////////////////////////////////////////////////////////////
	_border(100); // add lines after this
////////////////////////////////////////////////////////////		
		
		
		