///scene_recover_sleep_8hours();
/// @description
with obj_control
{
	var temp_list = var_map_get_all_character_map_list();
	//ds_list_add_multiple(temp_list,var_map[? VAR_ALLY_LIST]);
	//ds_list_add_multiple(temp_list,var_map[? VAR_ALLY_R_LIST]);
	//ds_list_add_multiple(temp_list,var_map[? VAR_PET_LIST]);
	//ds_list_add_multiple(temp_list,var_map[? VAR_PET_R_LIST]);
	//ds_list_add_multiple(temp_list,var_map[? VAR_STORAGE_LIST]);
	//ds_list_add(temp_list,var_map[? VAR_PLAYER]);
	
	for(var i = 0; i < ds_list_size(temp_list);i++)
	{
		var char = temp_list[| i];
		char[? GAME_CHAR_VITALITY] = min(char[? GAME_CHAR_VITALITY]+char_get_vitality_max(char)*0.8,char_get_vitality_max(char));
		char[? GAME_CHAR_HEALTH] = min(char[? GAME_CHAR_HEALTH]+char_get_health_regen(char)*2*8,char_get_health_max(char));
		char[? GAME_CHAR_STAMINA] = char_get_stamina_max(char);
		char[? GAME_CHAR_TORU] = min(char[? GAME_CHAR_TORU]+char_get_toru_regen(char)*8,char_get_toru_max(char));
		char[? GAME_CHAR_LUST] = min(char[? GAME_CHAR_LUST]+char_get_LGR(char)/20*8,char_get_lust_max(char));
		char[? GAME_CHAR_COLOR_TARGET] = c_white;
		char_stats_cap(char);
	}
	
	ds_list_destroy(temp_list);
}