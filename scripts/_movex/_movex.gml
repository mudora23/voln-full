///_movex(ID,xnum_target,slide_speed);
/// @description  
/// @param ID
/// @param xnum_target
/// @param slide_speed

if scene_is_current()
{
	scene_sprite_list_change_x(argument0,argument1,argument2);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);