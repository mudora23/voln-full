///debug_battle_elf(number);
/// @description
with obj_control
{
	//var_map_set(VAR_MODE,VAR_MODE_BATTLE);

	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);
	
	scene_choices_list_clear();
	
	create_enemies(argument0,5,0,NAME_ELF_BANDITS);
	
	script_execute_add(0,battle_process_setup);
	//show_message("battle_process_setup in SBS_0001_Battle");

}	