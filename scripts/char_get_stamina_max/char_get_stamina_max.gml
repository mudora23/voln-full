///char_get_stamina_max(char_map);
/// @description
/// @param char_map
var char_map = argument[0];
return 100*char_get_vitality_penalty_ratio(argument0);