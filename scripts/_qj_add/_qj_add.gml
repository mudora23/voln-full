///_qj_add(chunk);
/// @description  
/// @param chunk

if scene_is_current()
{
	if !ds_list_value_exists(var_map_get(VAR_QUEST_JOURNAL_LIST),argument0)
	{
		ds_list_add(var_map_get(VAR_QUEST_JOURNAL_LIST),argument0);
		var_map_set(VAR_NEW_QJ_NOTICE,true);
	}
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);