///postfx_ps_doublehit_2(x,y);
/// @description -
/// @param x
/// @param y

if argument_count < 2 exit;
var xp = real(argument[0]);
var yp = real(argument[1]);

part_emitter_region(global.ps, global.ps_doublehit2, xp-111, xp-109, yp-114, yp-112, ps_shape_rectangle, ps_distr_linear);
part_emitter_burst(global.ps, global.ps_doublehit2, global.ps_doublehit2, 1);
