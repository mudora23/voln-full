///scriptvar_PC_Muscularity_Desc();
/// @description
var num = var_map_get(PC_Muscularity_N);
if num <= 10 return choose("average","normal");
else if num <= 20 return choose("sporty","fit");
else if num <= 40 return choose("strong","powerful","lustily-built","strapping");
else if num <= 60 return choose("brawny","beefy","thick with muscle","stout");
else if num <= 80 return choose("large-with-muscle","thick and hard","brutally strong","big and strong");
else return choose("hugely muscular","massive-with-muscle","monstrously strong","ludicrously powerful","hulking");