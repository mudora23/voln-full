///ds_list_swap(list,index1,index2);
/// @description
/// @param list
/// @param index1
/// @param index2
var old_value = argument0[| argument1];
argument0[| argument1] = argument0[| argument2];
argument0[| argument2] = old_value;