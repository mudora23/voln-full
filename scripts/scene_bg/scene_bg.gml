///scene_bg(BG_NAME);
/// @description  
/// @param BG_NAME
with obj_control
{
	if scene_sprite_list_exists("bg") scene_sprite_list_change_sprite("bg",argument0);
	else scene_sprite_list_add("bg",argument0,5,5,1,1,0,1,FADE_NORMAL_SEC,SMOOTH_SLIDE_NORMAL_SEC,ORDER_BG,1);
}