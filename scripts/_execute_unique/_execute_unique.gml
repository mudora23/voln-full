///_execute_unique(delay_sec,script,script_arg0...arg13);
/// @description  
/// @param delay_sec
/// @param script
/// @param script_arg0...arg13

if scene_is_current()
{
	if !ds_list_value_exists(obj_control.var_map[? VAR_EXECUTE_LIST],script_get_name_ext(argument[1]))
	{
	    if argument_count > 15 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14],argument[15]);
	    if argument_count > 14 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14]);
		if argument_count > 13 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13]);
		if argument_count > 12 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12]);
		if argument_count > 11 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
		if argument_count > 10 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10]);
		if argument_count > 9 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9]);
		if argument_count > 8 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8]);
		if argument_count > 7 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7]);
		if argument_count > 6 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6]);
		if argument_count > 5 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5]);
		if argument_count > 4 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4]);
		if argument_count > 3 script_execute_add(argument[0],argument[1],argument[2],argument[3]);
		if argument_count > 2 script_execute_add(argument[0],argument[1],argument[2]);
		if argument_count > 1 script_execute_add(argument[0],argument[1]);
	}

	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);