///_aes_SubBytes(state)
var state = argument0;
var i, j;
for(i = 0; i < 4; ++i)
{
  for(j = 0; j < 4; ++j)
  {
    state[@i,j] = _aes_getSBoxValue(state[@i,j]);
  }
}

