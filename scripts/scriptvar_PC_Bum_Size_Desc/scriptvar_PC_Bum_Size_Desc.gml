///scriptvar_PC_Bum_Size_Desc();
/// @description
var num = var_map_get(PC_Bum_Size_N);
if num == 0 return choose("small","slight");
else if num == 1 return choose("averagely-sized","proportionate");
else if num == 2 return choose("big","large");
else return choose("oversized","huge");