///ds_list_of_char_phrase_as_string(list);
/// @description
/// @param list
var __list = argument[0];
var __string = "{";
for(var i = 0; i < ds_list_size(__list); i++)
{	
	__string += string(char_get_info(__list[| i],GAME_CHAR_DISPLAYNAME))+", ";
}
__string += "}";

return __string;