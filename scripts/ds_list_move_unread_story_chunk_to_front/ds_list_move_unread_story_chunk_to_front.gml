///ds_list_move_unread_story_chunk_to_front(list_of_story_chunks);
/// @description -
/// @param list_of_story_chunks

for(var i = 0; i < ds_list_size(argument0);i++)
{
	var story_chunk = argument0[| i];
	if !chunk_mark_get(story_chunk)
	{
		ds_list_delete(argument0,i);
		ds_list_insert(argument0,0,story_chunk);
		break;
	}
}
