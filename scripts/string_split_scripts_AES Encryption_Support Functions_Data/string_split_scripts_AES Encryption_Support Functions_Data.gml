///string_split(string [,sep])
//splits a string into an array based on sep
//sep is space by default
var lastchar, arr, num, wordcount,  c, i, ns, sep;
ns = argument[0]
if (argument_count < 2)
    sep = " ";
else
    sep = argument[1];

lastchar = sep;
arr = 0;
num = "";
wordcount = 0;
for (i = 1; i <= string_length(ns); ++i) {
    c = string_char_at(ns, i);
    if (c != sep) {
        if (lastchar == sep)
            arr[wordcount] = c;
        else
            arr[wordcount] = arr[wordcount] + c;
    } else {
        if (lastchar != sep)
            wordcount++;
    }
    lastchar = c;
}

if (wordcount = 0) {
    arr[0] = ns;
}

return arr;


