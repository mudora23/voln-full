///scene_store_clear();
/// @description

ds_list_clear(var_map_get(VAR_STORE_LIST_NAME));

var surface_list = var_map_get(VAR_STORE_LIST_SURFACE);
for(var i = 0; i < ds_list_size(surface_list);i++)
	if surface_exists(surface_list[| i])
		surface_free(surface_list[| i]);
ds_list_clear(surface_list);

ds_list_clear(var_map_get(VAR_STORE_LIST_ALPHA));





var_map_set(VAR_SCRIPT_DIALOG,"");
var_map_set(VAR_SCRIPT_DIALOG_RAW,"");
if surface_exists(var_map_get(VAR_SCRIPT_DIALOG_SURFACE))
	surface_free(var_map_get(VAR_SCRIPT_DIALOG_SURFACE));
var_map_set(VAR_MODE,VAR_MODE_NORMAL);
