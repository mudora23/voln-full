///scriptvar_PC_Eye_Pupil_Desc();
/// @description
var num = var_map_get(PC_Eye_Pupil_N);
if var_map_get(PC_Eye_Color_N) == 5 return "";
else if num == 0 return "";
else if num == 1 return "pupils like pin-pricks";
else if num == 2 return choose("pupils huge","pupils oversized");
else return "pupils shriked";