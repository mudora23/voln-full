///draw_menu_skill_tree(x1,y1,x2,y2);
/// @description
/// @param x1
/// @param y1
/// @param x2
/// @param y2

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var w = x2-x1;
var h = y2-y1;
var padding_w = 25;
var padding_h = 50;

draw_set_font(font_01n_reg);

// draw bg
draw_set_alpha(menu_open_bg_image_alpha);
draw_set_color(colorscheme_almostblack);
//draw_rectangle(x1,y1,x2,y2, false);
draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,w,h,draw_get_color(),draw_get_alpha());
draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,w,h,draw_get_color(),draw_get_alpha());

// skill
var skill_section_w = (w - padding_w*6)/3;
var skill_section_h = (h - padding_h*2);

// skill - Might
var section_x1 = x1 + padding_w*2;
var section_x2 = section_x1 + skill_section_w;
var section_y1 = y1 + padding_h;
var section_y2 = section_y1 + skill_section_h;

	// title
	draw_set_halign(fa_center);
	draw_set_color(c_ltgray);
	//draw_set_font(font_general_30_b_small_caps);
	draw_text(section_x1+skill_section_w/2,section_y1,"Might");
	
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);	
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+500;
		draw_skill_icon(xx,yy,SKILL_WRECK);
		
		
		
// skill - Toru
var section_x1 = section_x2 + padding_w;
var section_x2 = section_x1 + skill_section_w;
var section_y1 = y1 + padding_h;
var section_y2 = section_y1 + skill_section_h;
		
	draw_set_halign(fa_center);
	draw_set_color(c_ltgray);
	//draw_set_font(font_general_30_b_small_caps);
	draw_text(section_x1+skill_section_w/2,section_y1,"Toru");
	
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_FIREBALL);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_HEAL);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_OCCULT);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_THUNDERSTORM);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);	
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+500;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		
// skill - Stealth
var section_x1 = section_x2 + padding_w;
var section_x2 = section_x1 + skill_section_w;
var section_y1 = y1 + padding_h;
var section_y2 = section_y1 + skill_section_h;
		
	draw_set_halign(fa_center);
	draw_set_color(c_ltgray);
	//draw_set_font(font_general_30_b_small_caps);
	draw_text(section_x1+skill_section_w/2,section_y1,"Stealth");
	
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_HIDE);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+100;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_DOUBLE_HIT);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+200;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+300;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.25;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		var xx = section_x1+skill_section_w*0.75;
		var yy = section_y1+400;
		draw_skill_icon(xx,yy,SKILL_PH);	
		
		var xx = section_x1+skill_section_w*0.5;
		var yy = section_y1+500;
		draw_skill_icon(xx,yy,SKILL_PH);
		
		
		
if obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS] > 0
{
	//draw_set_font(font_general_20);
	draw_set_halign(fa_center);
	draw_set_color(COLOR_EFFECT);
	draw_text_transformed(x1+w/2,y2-50,"Unused skill points:",1,1,0);
	//draw_set_font(font_general_45_b);
	draw_text_transformed(x1+w/2,y2-33,obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS],1,1,0);
}
		
		