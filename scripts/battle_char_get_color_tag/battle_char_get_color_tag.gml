///battle_char_get_color_tag(char);
/// @param char
if argument0[? GAME_CHAR_OWNER] == OWNER_PLAYER || argument0[? GAME_CHAR_OWNER] == OWNER_ALLY
	return TAG_COLOR_ALLY_NAME;
else
	return TAG_COLOR_ENEMY_NAME;