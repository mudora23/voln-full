///postfx_burn(x,y,level[1-3]);
/// @description  
/// @param x
/// @param y

ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_NAME_LIST],POSTFX_BURN);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_LIST],0.1*argument[2]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_LIFE_MAX_LIST],0.1*argument[2]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG0_LIST],argument[0]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG1_LIST],argument[1]);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG2_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG3_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG4_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG5_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG6_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG7_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG8_LIST],0);
ds_list_add(obj_control.var_map[? VAR_POSTFX_LIST_ARG9_LIST],0);


