///skill_get_cost_enough(char,SKILL,level);
/// @descirption
/// @param char
/// @param SKILL
/// @param level

var char = argument0;
var skill = argument1;
var skill_map = obj_control.skill_wiki_map[? skill];
var level = argument2;

if level < 1
	return false;
else if ds_list_size(skill_map[? SKILL_VITALITY_LIST]) >= level && ds_list_find_value(skill_map[? SKILL_VITALITY_LIST],level-1) > char[? GAME_CHAR_VITALITY]
	return false;
else if ds_list_size(skill_map[? SKILL_TORU_LIST]) >= level && ds_list_find_value(skill_map[? SKILL_TORU_LIST],level-1) > char[? GAME_CHAR_TORU]
	return false;
else if ds_list_size(skill_map[? SKILL_STAMINA_LIST]) >= level && ds_list_find_value(skill_map[? SKILL_STAMINA_LIST],level-1) > char[? GAME_CHAR_STAMINA]
	return false;
else
	return true;
