///scene_sprites_list_clear_except(ID,ID2...,fade_speed);
/// @description
with obj_control
{
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	var sprite_fade_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_FADE_SPEED];
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	
	for(var i = 0; i < ds_list_size(sprite_id_list);i++)
	{
		if !(argument_count > 1 && sprite_id_list[| i] == argument[0] ||
		   argument_count > 2 && sprite_id_list[| i] == argument[1] ||
		   argument_count > 3 && sprite_id_list[| i] == argument[2] ||
		   argument_count > 4 && sprite_id_list[| i] == argument[3] ||
		   argument_count > 5 && sprite_id_list[| i] == argument[4] ||
		   argument_count > 6 && sprite_id_list[| i] == argument[5] ||
		   argument_count > 7 && sprite_id_list[| i] == argument[6] ||
		   argument_count > 8 && sprite_id_list[| i] == argument[7] ||
		   argument_count > 9 && sprite_id_list[| i] == argument[8] ||
		   argument_count > 10 && sprite_id_list[| i] == argument[9] ||
		   argument_count > 11 && sprite_id_list[| i] == argument[10] ||
		   argument_count > 12 && sprite_id_list[| i] == argument[11] ||
		   argument_count > 13 && sprite_id_list[| i] == argument[12] ||
		   argument_count > 14 && sprite_id_list[| i] == argument[13] ||
		   argument_count > 15 && sprite_id_list[| i] == argument[14])
		{
			sprite_fade_speed_list[| i] = argument[argument_count-1];
			sprite_alpha_target_list[| i] = 0;
		}
	}
}