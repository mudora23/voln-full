///char_get_dodge_chance(char_map);
/// @description
/// @param char_map

var number = 8*power(char_get_DEX(argument0),0.5);
if battle_char_get_long_term_buff_exists(argument0,LONG_TERM_BUFF_ORANGE_BERRY) number*=1.1;
if battle_char_get_long_term_debuff_exists(argument0,LONG_TERM_DEBUFF_PURPLE_BERRY) number/=1.1;
//if char_get_skill_level(argument0,SKILL_THIEF) number+=20;
//if char_get_skill_level(argument0,SKILL_I_AM_THE_MAIN_CHARACTER) number+=10;

if char_get_info(argument0,GAME_CHAR_NAME) == NAME_ZEYD number += 20;

number += char_get_info(argument0,GAME_CHAR_LEVEL);
return min(100,number);
