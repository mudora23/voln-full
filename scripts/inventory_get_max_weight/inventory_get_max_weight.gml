///inventory_get_max_weight();
/// @description

with obj_control
{
/*	var name_list = obj_control.var_map[? VAR_INVENTORY_LIST];
	var amount_list = obj_control.var_map[? VAR_INVENTORY_AMOUNT_LIST];
	var total_weight = 0;
	
	for(var i = 0; i < ds_list_size(name_list);i++)
	{
		var name = name_list[| i];
		var weight = item_get_weight(name);
		var amount = amount_list[| i];
		if amount > 0 total_weight += weight*amount;
	}
*/
	//return 100;
	
	var max_weight = 0;
	var char = var_map[? VAR_PLAYER];
	max_weight += char_get_STR(char);
	max_weight += char_get_FOU(char);
	max_weight += char_get_END(char);
	
	for(var i = 0; i < ds_list_size(var_map[? VAR_ALLY_LIST]);i++)
	{
		var char = ds_list_find_value(var_map[? VAR_ALLY_LIST],i);
		max_weight += char_get_STR(char);
		max_weight += char_get_FOU(char);
		max_weight += char_get_END(char);
	}
	
	return round(max_weight);
}