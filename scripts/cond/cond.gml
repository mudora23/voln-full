///cond(cond,true_res,false_res);
/// @description
/// @param cond
/// @param true_res
/// @param false_res

if argument0 return argument1;
else return argument2;