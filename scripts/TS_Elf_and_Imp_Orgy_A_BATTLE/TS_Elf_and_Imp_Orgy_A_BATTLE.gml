///TS_Elf_and_Imp_Orgy_A_BATTLE();
/// @description
with obj_control
{
	//var_map_set(VAR_MODE,VAR_MODE_BATTLE);

	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);

	create_enemies(ally_get_number()+1,10,0,NAME_ELF_BANDITS,NAME_IMP);
	
	
	var_map[? BATTLE_WIN_SCENE] = "CS_extra_TS";
	var_map[? BATTLE_WIN_SCENE_LABEL] = "TS_Elf_and_Imp_Orgy_A_Won";
	var_map[? BATTLE_LOSS_SCENE] = "CS_extra_TS";
	var_map[? BATTLE_LOSS_SCENE_LABEL] = "TS_Elf_and_Imp_Orgy_A_Loss";
	
	
	script_execute_add(0,battle_process_setup);
	//show_message("battle_process_setup in SBS_0001_Battle");

}	