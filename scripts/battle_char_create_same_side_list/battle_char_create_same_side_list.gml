///battle_char_create_same_side_list(char);
/// @description
/// @param char

if argument0[? GAME_CHAR_OWNER] == OWNER_ENEMY var is_player_side = false;
else var is_player_side = true;

if battle_char_get_debuff_level(argument0,DEBUFF_TEMPT) > 0 is_player_side = !is_player_side;

if !is_player_side
{
	var same_side_list = ds_list_create();
	var the_list = var_map_get(VAR_ENEMY_LIST);
	for(var i = 0 ; i < ds_list_size(the_list);i++)
		ds_list_add(same_side_list,the_list[| i]);
}
else
{
	var same_side_list = ds_list_create();
	var the_list = var_map_get(VAR_ALLY_LIST);
	for(var i = 0 ; i < ds_list_size(the_list);i++)
		ds_list_add(same_side_list,the_list[| i]);
	ds_list_add(same_side_list,var_map_get(VAR_PLAYER));
}
ds_list_add_unique(same_side_list,argument0);
return same_side_list;