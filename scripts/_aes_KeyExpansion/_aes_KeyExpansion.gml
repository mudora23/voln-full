///_aes_KeyExpansion(RoundKey, Key)
var i, j, k, tempa;
var RoundKey = argument0, Key = argument1;

var Nk = global._aes_Nk, Nb = global._aes_Nb, Nr = global._aes_Nr, Rcon = global._aes_Rcon;

for(i = 0; i < Nk; ++i)
{
  RoundKey[@(i * 4) + 0] = Key[@(i * 4) + 0];
  RoundKey[@(i * 4) + 1] = Key[@(i * 4) + 1];
  RoundKey[@(i * 4) + 2] = Key[@(i * 4) + 2];
  RoundKey[@(i * 4) + 3] = Key[@(i * 4) + 3];
}


for(; (i < (Nb * (Nr + 1))); ++i)
{
  for(j = 0; j < 4; ++j)
  {
    tempa[j]=RoundKey[@(i-1) * 4 + j];
  }
  if ((i mod Nk) == 0)
  {

    // RotWord()
    {
      k = tempa[0];
      tempa[0] = tempa[1];
      tempa[1] = tempa[2];
      tempa[2] = tempa[3];
      tempa[3] = k;
    }
    
    // Subword()
    {
      tempa[0] = _aes_getSBoxValue(tempa[0]);
      tempa[1] = _aes_getSBoxValue(tempa[1]);
      tempa[2] = _aes_getSBoxValue(tempa[2]);
      tempa[3] = _aes_getSBoxValue(tempa[3]);
    }

    tempa[0] =  tempa[0] ^ Rcon[| i/Nk];
  }
  else if (Nk > 6 && (i mod Nk) == 4)
  {
    // Function Subword()
    {
      tempa[0] = _aes_getSBoxValue(tempa[0]);
      tempa[1] = _aes_getSBoxValue(tempa[1]);
      tempa[2] = _aes_getSBoxValue(tempa[2]);
      tempa[3] = _aes_getSBoxValue(tempa[3]);
    }
  }
  RoundKey[@i * 4 + 0] = RoundKey[@(i - Nk) * 4 + 0] ^ tempa[0];
  RoundKey[@i * 4 + 1] = RoundKey[@(i - Nk) * 4 + 1] ^ tempa[1];
  RoundKey[@i * 4 + 2] = RoundKey[@(i - Nk) * 4 + 2] ^ tempa[2];
  RoundKey[@i * 4 + 3] = RoundKey[@(i - Nk) * 4 + 3] ^ tempa[3];
}

