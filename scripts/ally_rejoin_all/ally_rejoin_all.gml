///ally_rejoin_all();
/// @description
with obj_control
{
	var ally_list = var_map[? VAR_ALLY_R_LIST];
    for(var i = 0;i<ds_list_size(ally_list);i++)
    {
        ally_rejoin(char_get_info(ally_list[| i],GAME_CHAR_ID));
		i--;
    }
	
}