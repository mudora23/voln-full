///file_binary_read_all(filename)

var fn = argument0, b = file_bin_open(fn, 0), a, s = file_bin_size(b), i;

for (i=0;i<s;++i){
    a[i] = file_bin_read_byte(b);
}

file_bin_close(b);

return a;

