///draw_thumb_in_area(spr,xx,yy,width,height,color,char);
/// @description
/// @param spr
/// @param xx
/// @param yy
/// @param width
/// @param height
/// @param color
/// @param char
if argument0 != spr_char_player_offspring_thumb
	draw_sprite_in_area(argument0,argument1,argument2,argument3,argument4,argument5);
else
{
	var char_x = argument1;
	var char_y = argument2;
	var char_scale_x = argument3 / sprite_get_width(argument0);
	var char_scale_y = argument4 / sprite_get_height(argument0);
	var class = char_get_class_name(argument6);

	draw_sprite_ext(argument0,0,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_SKIN],image_alpha);
	draw_sprite_ext(argument0,1,char_x,char_y,char_scale_x,char_scale_y,0,argument5,image_alpha);
	draw_sprite_ext(argument0,2,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_EYE],image_alpha);
		gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
		draw_sprite_ext(argument0,3,char_x,char_y,char_scale_x,char_scale_y,0,argument5,1);
		gpu_set_blendmode(bm_normal);
	draw_sprite_ext(argument0,4,char_x,char_y,char_scale_x,char_scale_y,0,argument5,1);
	
	if class == "thief"
	{
		draw_sprite_ext(argument0,5,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_THIEF_1],1);
		draw_sprite_ext(argument0,6,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_THIEF_2],1);
		draw_sprite_ext(argument0,7,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_THIEF_3],1);
		draw_sprite_ext(argument0,8,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_THIEF_4],1);
		draw_sprite_ext(argument0,9,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_HAIR],1);
			gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
			draw_sprite_ext(argument0,10,char_x,char_y,char_scale_x,char_scale_y,0,argument5,1);
			gpu_set_blendmode(bm_normal);
		draw_sprite_ext(argument0,11,char_x,char_y,char_scale_x,char_scale_y,0,max(0.5,color_get_value(argument6[? GAME_CHAR_COLOR_HAIR])/255),1);
	}
	else if class == "warrior"
	{
		draw_sprite_ext(argument0,12,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WARRIOR_1],1);
		draw_sprite_ext(argument0,13,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WARRIOR_2],1);
		draw_sprite_ext(argument0,14,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WARRIOR_3],1);
		draw_sprite_ext(argument0,15,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WARRIOR_4],1);
		draw_sprite_ext(argument0,16,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_HAIR],1);
			gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
			draw_sprite_ext(argument0,17,char_x,char_y,char_scale_x,char_scale_y,0,argument5,1);
			gpu_set_blendmode(bm_normal);
		draw_sprite_ext(argument0,18,char_x,char_y,char_scale_x,char_scale_y,0,max(0.5,color_get_value(argument6[? GAME_CHAR_COLOR_HAIR])/255),1);
	}
	else
	{
		draw_sprite_ext(argument0,19,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WIZARD_1],1);
		draw_sprite_ext(argument0,20,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WIZARD_2],1);
		draw_sprite_ext(argument0,21,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WIZARD_3],1);
		draw_sprite_ext(argument0,22,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_WIZARD_4],1);
		draw_sprite_ext(argument0,23,char_x,char_y,char_scale_x,char_scale_y,0,argument6[? GAME_CHAR_COLOR_HAIR],1);
			gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
			draw_sprite_ext(argument0,24,char_x,char_y,char_scale_x,char_scale_y,0,argument5,1);
			gpu_set_blendmode(bm_normal);
		draw_sprite_ext(argument0,25,char_x,char_y,char_scale_x,char_scale_y,0,max(0.5,color_get_value(argument6[? GAME_CHAR_COLOR_HAIR])/255),1);
	}

}