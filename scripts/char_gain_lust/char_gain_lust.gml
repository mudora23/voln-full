///char_gain_lust(char,amount);
///@description char gains and loses lust within the lust cap
///@param char
///@param amount
char_set_info(argument0,GAME_CHAR_LUST,clamp(char_get_info(argument0,GAME_CHAR_LUST)+argument1,0,char_get_lust_max(argument0))); 