///ds_list_value_exists_count(id,value);
/// @description
/// @param id
/// @param value
var count = 0;
for(var i = 0; i < ds_list_size(argument0);i++)
{
	if argument0[| i] == argument1 count++;
}
return count;