///enemy_add_char(name,unique?,level[optional]);
/// @description
/// @param name
/// @param unique?
/// @param level[optional]

//if argument_count > 2
	//show_debug_message("enemy_add_char  name:"+string(argument[0])+" unique? "+string(argument[1])+" level[optional]: "+string(argument[2]));
//else
	//show_debug_message("enemy_add_char  name:"+string(argument[0])+" unique? "+string(argument[1]));


var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
if !argument[1] || !enemy_exists(argument[0])
{
	ds_list_add(enemy_list,ds_map_create());
	var enemy_index = ds_list_size(enemy_list)-1;
	ds_list_mark_as_map(enemy_list,enemy_index);
	
	if argument_count > 2
		game_char_map_init_from_wiki(enemy_list[| enemy_index],argument[0],OWNER_ENEMY,argument[2]);
	else
		game_char_map_init_from_wiki(enemy_list[| enemy_index],argument[0],OWNER_ENEMY);
	
	char_stats_recalculate(enemy_list[| enemy_index]);
	char_skills_recalculate(enemy_list[| enemy_index]);
	
	char_set_info(enemy_list[| enemy_index],GAME_CHAR_HEALTH,char_get_health_max(enemy_list[| enemy_index]));
	char_set_info(enemy_list[| enemy_index],GAME_CHAR_TORU,char_get_toru_max(enemy_list[| enemy_index]));
	char_set_info(enemy_list[| enemy_index],GAME_CHAR_STAMINA,char_get_stamina_max(enemy_list[| enemy_index]));
	
	return enemy_list[| enemy_index];
}

return noone;