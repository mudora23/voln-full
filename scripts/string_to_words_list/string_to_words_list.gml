///string_to_words_list(string,widthmax);
/// @description
/// @param string
/// @param widthmax

var msg = argument0; //string to split
var splitBy = " "; //string to split the first string by
var slot = 0;
var words_list = ds_list_create(); //array to hold all splits
var str2 = ""; //var to hold the current split we're working on building

var i;
for (i = 1; i < (string_length(msg)+1); i++) {
	var currStr = string_char_at(msg,i);
    //var currStr = string_copy(msg, i, 1);
    if (currStr == splitBy) {
        words_list[| slot] = str2;
        slot++;
        str2 = "";
		if i == string_length(msg)/* && (i == 1 || string_char_at(msg,i-1) != " ")*/
			words_list[| slot] = "";
    }
    else if (currStr == "\r") {
        words_list[| slot] = str2;
        slot++;
		words_list[| slot] = "\r";
		slot++;
        str2 = "";
    }
    else if (currStr == "\n") {
	    words_list[| slot] = str2;
	    slot++;
		if string_char_at(msg,i-1) != "\r"
		{
			words_list[| slot] = "\n";
			slot++;
		}
		str2 = "";
    }
    else if (string_width(str2)>= argument1) {
        str2 = str2 + currStr;
        words_list[| slot] = str2;
        slot++;
        str2 = "";
    }
    else {
        str2 = str2 + currStr;
        words_list[| slot] = str2;
    }
}

return words_list;