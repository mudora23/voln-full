///var_map_set(KEY,VAL);
/// @description
/// @param KEY
/// @param VAL
var var_key = argument[0];
var var_val = argument[1];

if var_key == VAR_SCRIPT_POSI
	obj_control.var_map[? VAR_SCRIPT_PROC_NUMBER] = 0;

obj_control.var_map[? var_key] = var_val;
