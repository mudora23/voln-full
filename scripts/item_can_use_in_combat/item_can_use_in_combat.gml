///item_can_use_in_combat(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	var item_map = item_get_map(name);
	if item_map != noone
		var item_use_in_combat = item_map[? ITEM_USE_IN_COMBAT];
	else
		var item_use_in_combat = false;
	
	return item_use_in_combat;
	
}