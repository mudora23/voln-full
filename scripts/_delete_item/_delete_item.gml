///_delete_item(name,amount);
/// @description  
/// @param name
/// @param amount

if scene_is_current()
{
	inventory_delete_item(argument0,argument1);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);