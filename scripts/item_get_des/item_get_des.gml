///item_get_des(name);
/// @description
/// @param name

with obj_control
{
	var name = argument[0];
	var item_map = item_get_map(name);
	
	if ds_map_exists(item_map,ITEM_DES)
		var item_des = item_map[? ITEM_DES];
	else
		var item_des = "";
	
	return item_des;
	
}