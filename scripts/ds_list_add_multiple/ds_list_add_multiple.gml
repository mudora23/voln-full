///ds_list_add_multiple(list,list_of_values);
/// @description add a list of values to the list
/// @param list
/// @param list_of_values
for(var i = 0; i < ds_list_size(argument1);i++)
	ds_list_add(argument0,argument1[| i]);