///ratio_to_a_point(val,pivot,min,max);
/// @description  
/// @param val
/// @param pivot
/// @param min
/// @param max

var val = argument[0];
var pivot = argument[1];
var minval = argument[2];
var maxval = argument[3];

if val < pivot
    return (val - minval) / (pivot - minval);
else
    return (maxval - val) / (maxval - pivot);