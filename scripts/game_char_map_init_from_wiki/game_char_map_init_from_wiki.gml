///game_char_map_init_from_wiki(char_map,char_name,owner,level[optional]);
/// @description
/// @param char_map
/// @param char_name
/// @param owner
/// @param level[optional]
var char_map = argument[0];
var char_name = argument[1];
var char_owner = argument[2];
if argument_count > 3 var char_target_level = argument[3];
else var char_target_level = 1;

with obj_control
{
	if ds_map_exists(char_wiki_map,char_name)
	{
		ds_map_copy(char_map,char_wiki_map[? char_name]);
	}
	else
	{
		ds_map_clear(char_map);
	}

	// add missing keys
	game_char_map_add_missing_keys(char_map,char_owner);

	
	char_level_up(char_map,char_target_level - char_map[? GAME_CHAR_LEVEL]);


	
}
