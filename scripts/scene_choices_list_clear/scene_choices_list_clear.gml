///scene_choices_list_clear();
/// @description
with obj_control
{
    var key = ds_map_find_first(var_map);
    while(!is_undefined(key))
    {
        if string_count("VAR_CHOICES_LIST", key) > 0
		{
			if key == VAR_CHOICES_LIST_SURFACE
			{
				var surface_list = obj_control.var_map[? key];
				for(var i = 0; i < ds_list_size(surface_list);i++)
				{
					if surface_exists(surface_list[| i])
					{
						surface_free(surface_list[| i]);
						surface_list[| i] = noone;
					}
				}
			}
            ds_list_clear(obj_control.var_map[? key]);
		}
        
        key = ds_map_find_next(var_map,key);
    }
	
	obj_control.current_keyboard_choice = 0;
	obj_control.enemies_choice_enable = false;
	obj_control.current_enemies_choice = noone;
	obj_control.allies_choice_enable = false;
	obj_control.current_allies_choice = noone;




	if event_type != ev_create
	{
		//if surface_exists(obj_command_panel.choice_surface)
		//	surface_free(obj_command_panel.choice_surface);
			
		obj_command_panel.choice_yoffset = 0;
		obj_command_panel.choice_yoffset_target = 0;
		obj_command_panel.choice_image_alpha = 0;

	}

}