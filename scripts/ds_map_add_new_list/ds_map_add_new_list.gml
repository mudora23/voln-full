///ds_map_add_new_list(id,key,value);
/// @description
/// @param id
/// @param key
/// @param value
if ds_map_exists(argument[0],argument[1])
{
	ds_map_delete(argument[0],argument[1]);
}

return ds_map_add_list(argument[0],argument[1],argument[2]);
