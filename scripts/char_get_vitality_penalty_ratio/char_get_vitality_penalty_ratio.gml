///char_get_vitality_penalty_ratio(char);
/// @description
/// @param char
var char = argument0;
if ds_map_exists(char,GAME_CHAR_VITALITY)
	return min((char[? GAME_CHAR_VITALITY] / char_get_vitality_max(char))+0.5,1);
else
	return 1;
