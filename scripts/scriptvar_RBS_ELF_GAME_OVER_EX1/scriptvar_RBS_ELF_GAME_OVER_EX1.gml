///scriptvar_RBS_ORK_GAME_OVER_EX1();
/// @description

if ally_get_number() == 1
{
	var ally_list = var_map_get(VAR_ALLY_LIST);
	var ally = ally_list[| 0];
	var ally_name = ally[? GAME_CHAR_DISPLAYNAME];

	return "and piling "+string(ally_name)+" in next to you, ";
}
else
	return "";
