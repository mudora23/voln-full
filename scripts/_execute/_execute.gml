///_execute(delay_sec,script,script_arg0...arg13);
/// @description  
/// @param delay_sec
/// @param script
/// @param script_arg0...arg13

if scene_is_current()
{
	// if no delay, execute now.
	if argument[0] == 0
	{
	    if argument_count > 15 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14],argument[15]);
	    else if argument_count > 14 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14]);
		else if argument_count > 13 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13]);
		else if argument_count > 12 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12]);
		else if argument_count > 11 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
		else if argument_count > 10 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10]);
		else if argument_count > 9 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9]);
		else if argument_count > 8 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8]);
		else if argument_count > 7 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7]);
		else if argument_count > 6 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5],argument[6]);
		else if argument_count > 5 script_execute_ext(argument[1],argument[2],argument[3],argument[4],argument[5]);
		else if argument_count > 4 script_execute_ext(argument[1],argument[2],argument[3],argument[4]);
		else if argument_count > 3 script_execute_ext(argument[1],argument[2],argument[3]);
		else if argument_count > 2 script_execute_ext(argument[1],argument[2]);
		else if argument_count > 1 script_execute_ext(argument[1]);
	}
	else
	{
	    if argument_count > 15 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14],argument[15]);
	    else if argument_count > 14 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13],argument[14]);
		else if argument_count > 13 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12],argument[13]);
		else if argument_count > 12 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11],argument[12]);
		else if argument_count > 11 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10],argument[11]);
		else if argument_count > 10 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9],argument[10]);
		else if argument_count > 9 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8],argument[9]);
		else if argument_count > 8 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7],argument[8]);
		else if argument_count > 7 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6],argument[7]);
		else if argument_count > 6 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5],argument[6]);
		else if argument_count > 5 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5]);
		else if argument_count > 4 script_execute_add(argument[0],argument[1],argument[2],argument[3],argument[4]);
		else if argument_count > 3 script_execute_add(argument[0],argument[1],argument[2],argument[3]);
		else if argument_count > 2 script_execute_add(argument[0],argument[1],argument[2]);
		else if argument_count > 1 script_execute_add(argument[0],argument[1]);
	}

	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);