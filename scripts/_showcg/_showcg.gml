///_showcg(ID,cg,alpha,alpha_target,fade_speed,slide_speed,order,halign,valign,slide_from_halign,slide_from_valign,hstick,vstick);
/// @description  
/// @param ID
/// @param cg
/// @param alpha
/// @param alpha_target
/// @param fade_speed
/// @param slide_speed
/// @param order
/// @param halign
/// @param valign
/// @param slide_from_halign
/// @param slide_from_valign
/// @param hstick
/// @param vstick

if scene_is_current()
{
	scene_cg_list_add(argument0,argument1,argument2,argument3,argument4,argument5,argument6,argument7,argument8,argument9,argument10,argument11,argument12);
	scene_jump_next();
}

var_map_add(VAR_SCRIPT_POSI_FLOATING,1);