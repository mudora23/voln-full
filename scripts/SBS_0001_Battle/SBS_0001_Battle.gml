///SBS_0001_Battle();
/// @description
with obj_control
{
	//var_map_set(VAR_MODE,VAR_MODE_BATTLE);

	var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];
	ds_list_clear(enemy_list);
	
	enemy_add_char(NAME_ELF_BANDITS_1,false,1);
	enemy_add_char(NAME_ELF_BANDITS_2,false,1);
	enemy_add_char(NAME_ELF_BANDITS_1,false,1);
	
	script_execute_add(0,battle_process_setup);
	//show_message("battle_process_setup in SBS_0001_Battle");

}	