///battle_char_list_get_highest_lust_capable_char(char_list);
/// @description
/// @param char_list
var the_index = battle_char_list_get_highest_lust_capable_char_i(argument0);
if the_index == noone return noone;
return argument0[| the_index];
