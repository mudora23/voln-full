///scriptvar_PC_Weight_Muscularity_Desc();
/// @description
var wnum = var_map_get(PC_Weight_N);
var mnum = var_map_get(PC_Muscularity_N);

if number_in_range(wnum,61,100) && number_in_range(mnum,41,60) return choose("shockingly huge","morbidly big","monolithic","impossibly big","hulking","mountainous");
else if number_in_range(wnum,61,100) && number_in_range(mnum,11,40) return choose("monstrously fat","hideously thick","brutally bloated","profusely burly","massively husky");
else if number_in_range(wnum,21,60) && number_in_range(mnum,11,40) return choose("husky","burly","stout","sturdy","beefy");
else if number_in_range(wnum,-20,-1) && number_in_range(mnum,11,40) return choose("wiry","limber","strapping","athletic");
else if number_in_range(wnum,-60,-21) && number_in_range(mnum,21,60) return choose("sinewy","rock-hard","perfectly muscular","statuesquely built");
else if number_in_range(wnum,-100,-41) && number_in_range(mnum,41,75) return choose("hideously sinewy","impossibly hard","cruelly well-defined","horribly muscular");
else if number_in_range(wnum,-100,-41) && number_in_range(mnum,76,100) return choose("painfully huge","painfully sinewy","monstrously vascular","hugely hard","disturbingly chiseled","craggily massive");
else return scriptvar_PC_Muscularity_Desc();