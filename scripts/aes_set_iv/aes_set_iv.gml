///aes_set_iv(iv)

if (array_length_1d(argument0) != global._aes_KEYLEN)
    show_error("Invalid Initialization Vector Length", true);

global._aes_IV = 0;
global._aes_IV = argument0;
