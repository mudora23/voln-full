///scene_sprite_list_change_alpha_ext(ID,alpha_target,fade_speed);
/// @description  
/// @param ID
/// @param alpha_target
/// @param fade_speed

with obj_control
{
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	var sprite_fade_speed_list = obj_control.var_map[? VAR_SPRITE_LIST_FADE_SPEED];
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	
	if ds_list_find_index(sprite_id_list,argument[0]) >= 0
	{
		var existing_index = ds_list_find_index(sprite_id_list,argument[0]);
		
		sprite_alpha_target_list[| existing_index] = argument[1];
		sprite_fade_speed_list[| existing_index] = argument[2];
	
	}

}