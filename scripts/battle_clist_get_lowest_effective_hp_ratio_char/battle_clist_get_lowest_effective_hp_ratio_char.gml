///battle_clist_get_lowest_effective_hp_ratio_char(char_list);
/// @description 
/// @param char_list

var real_hp_ratio = 1;
var return_char = noone;

for(var i = 0; i < ds_list_size(argument0);i++)
{
	var char = argument0[| i];

	if battle_char_get_combat_capable(char)
	{
		if char == noone || battle_char_get_effective_hp_ratio(char) < real_hp_ratio
		{
			return_char = char;
			real_hp_ratio = battle_char_get_effective_hp_ratio(char);
		}

	}
}

return return_char;