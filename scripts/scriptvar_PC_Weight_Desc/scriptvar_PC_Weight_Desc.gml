///scriptvar_PC_Weight_Desc();
/// @description
var num = var_map_get(PC_Weight_N);
if num < -80 return choose("tragically emaciated","bone-thin","skeletal","cadaverous");
else if num < -60 return choose("waifish","wasted-away","emaciated","gaunt");
else if num < -40 return choose("stick-thin","anorexic","bony","underfed");
else if num < -20 return choose("skinny","twiggy","spare","lean");
else if num < -10 return choose("slight","trim","slender");
else if num < 0 return choose("thin","slim");
else if num == 0 return "average";
else if num <= 10 return choose("slightly chubby","soft");
else if num <= 20 return choose("upholstered","thick","plump");
else if num <= 40 return choose("portly","fat","meaty","porky");
else if num <= 60 return choose("rotund","ponderous","distended","inflated");
else if num <= 80 return choose("greatly fat","obese","corpulent","enormously plump");
else return choose("spherical","hugely obese","vastly corpulent","horrendously bloated","mountainously fat");