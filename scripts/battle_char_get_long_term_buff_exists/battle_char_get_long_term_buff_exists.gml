///battle_char_get_long_term_buff_exists(char,buff);
/// @description
/// @param char
/// @param buff
var char_map = argument0;
var char_buff_list = char_map[? GAME_CHAR_LONG_TERM_BUFF_LIST];
var char_buff = argument1;
return ds_list_value_exists(char_buff_list,char_buff);