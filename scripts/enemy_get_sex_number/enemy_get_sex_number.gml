///enemy_get_sex_number();
/// @descirption - Return the number of enemies the player has had sex with
var number = 0;
var enemy_list = obj_control.var_map[? VAR_ENEMY_LIST];

for(var i = 0; i < ds_list_size(enemy_list);i++)
{
	var enemy = enemy_list[| i];
	if enemy[? GAME_CHAR_SEX] number++;
}
return number;