///scene_sprite_list_change_alpha(ID,alpha_target);
/// @description  
/// @param ID
/// @param alpha_target

with obj_control
{
	var sprite_alpha_target_list = obj_control.var_map[? VAR_SPRITE_LIST_ALPHA_TARGET];
	var sprite_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
	
	if ds_list_find_index(sprite_id_list,argument[0]) >= 0
	{
		var existing_index = ds_list_find_index(sprite_id_list,argument[0]);
		
		sprite_alpha_target_list[| existing_index] = argument[1];
	
	}

}