///game_time_get_season_name(month);
/// @description
/// @param month
if argument0 == 1 return "Demmie";
if argument0 == 2 return "Demmie";
if argument0 == 3 return "Summər";
if argument0 == 4 return "Summər";
if argument0 == 5 return "Vål";
if argument0 == 6 return "Vål";
if argument0 == 7 return "Winter";
if argument0 == 8 return "Winter";
if argument0 == 9 return "Kim";
if argument0 == 10 return "Kim";
return "Unknown";