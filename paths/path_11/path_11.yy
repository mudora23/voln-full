{
    "id": "5726978f-77f0-453b-b6f4-4c8add26b4f0",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_11",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "864c4b58-83ad-451b-8d9b-d6f4e625d7fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3041,
            "y": 860,
            "speed": 100
        },
        {
            "id": "4b25b34f-b268-464f-b5f9-b22839669971",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3071,
            "y": 842,
            "speed": 100
        },
        {
            "id": "dbcb1716-01c8-40a6-b34e-857f684cf763",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3104,
            "y": 812,
            "speed": 100
        },
        {
            "id": "28c5d5c8-642b-4b25-9e7c-b596b8a9a3fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3216,
            "y": 730,
            "speed": 100
        },
        {
            "id": "7d326b40-5e82-4fe9-a9ab-23d01a02ae35",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3306,
            "y": 675,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}