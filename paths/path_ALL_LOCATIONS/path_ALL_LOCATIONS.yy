{
    "id": "259cd5a8-cdc8-401e-9310-f4e43ffda5de",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_ALL_LOCATIONS",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "84e10f5a-6429-4ba9-aafc-1f2da6858476",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1605,
            "y": 853,
            "speed": 100
        },
        {
            "id": "ea184d38-0005-46e1-b896-cf96898cb7f7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1859,
            "y": 765,
            "speed": 100
        },
        {
            "id": "04eaa6dc-ff60-4f7b-98b2-64feae5b9229",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1717,
            "y": 919,
            "speed": 100
        },
        {
            "id": "19dfd43f-57c6-470d-b3bc-de31fc572af0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1817,
            "y": 989,
            "speed": 100
        },
        {
            "id": "447ce9b2-c5f3-4181-a089-7883fa981595",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1945,
            "y": 1141,
            "speed": 100
        },
        {
            "id": "70502402-7160-471c-90a9-e5a2dba436c1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2109,
            "y": 1095,
            "speed": 100
        },
        {
            "id": "5067b2b5-2c53-47b1-86c1-a3317a5e68a4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2285,
            "y": 1140,
            "speed": 100
        },
        {
            "id": "39da7399-7bcc-4993-8484-9898414d8bf1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2257,
            "y": 1292,
            "speed": 100
        },
        {
            "id": "0c39ac79-7d67-492d-9284-e7581ccd9c21",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2357,
            "y": 1386,
            "speed": 100
        },
        {
            "id": "5aa765c5-a7f6-40c5-932d-ce1e5aa341ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2585,
            "y": 1438,
            "speed": 100
        },
        {
            "id": "f80baffb-7bc7-488f-8666-7cec6a7305bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2647,
            "y": 1160,
            "speed": 100
        },
        {
            "id": "9eead015-63a5-42d3-88d2-c97cb20ce522",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2847,
            "y": 1030,
            "speed": 100
        },
        {
            "id": "82ac9009-2ba9-4149-9bf6-161b8c8938e1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3041,
            "y": 860,
            "speed": 100
        },
        {
            "id": "f93460e0-e841-4b6e-bc36-3660f9b24852",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3306,
            "y": 675,
            "speed": 100
        },
        {
            "id": "928eacbc-7bfd-4979-91ad-ec5b5e991d1a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3194,
            "y": 505,
            "speed": 100
        },
        {
            "id": "9f73fa4d-fe32-4f21-bfee-60f18e16634f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3218,
            "y": 247,
            "speed": 100
        },
        {
            "id": "fc59dd8a-b515-4192-93e7-1f99f4213e62",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3332,
            "y": 347,
            "speed": 100
        },
        {
            "id": "105c1060-c637-418f-8908-b1f4616351a5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3662,
            "y": 571,
            "speed": 100
        },
        {
            "id": "5adca2f9-8d39-48c9-bd37-9a8fb33a5d69",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3110,
            "y": 1052,
            "speed": 100
        },
        {
            "id": "1da915d5-3303-4f57-b964-fc610c2ad634",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2934,
            "y": 1170,
            "speed": 100
        },
        {
            "id": "cba328dc-8e00-4ac6-9089-16f76ba1c51c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3066,
            "y": 1238,
            "speed": 100
        },
        {
            "id": "e54ccd2e-601c-4568-8b6e-3326b2518fd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3066,
            "y": 1396,
            "speed": 100
        },
        {
            "id": "96ea6234-8051-4a00-b892-39d74968f11f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3022,
            "y": 1638,
            "speed": 100
        },
        {
            "id": "879f4d66-709a-483e-b457-a133394acc0d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2820,
            "y": 1744,
            "speed": 100
        },
        {
            "id": "26b7ca07-ca15-408d-b95f-1b41897dc0cb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2794,
            "y": 1600,
            "speed": 100
        },
        {
            "id": "61596dc2-ea04-4ce9-ae02-1290f7ab4d67",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2718,
            "y": 1905,
            "speed": 100
        },
        {
            "id": "96918512-2972-4e7d-89bc-f1acb912ce08",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2570,
            "y": 1903,
            "speed": 100
        },
        {
            "id": "2f47b6a7-b4f3-4a2a-beca-b444d56344c8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2458,
            "y": 1989,
            "speed": 100
        },
        {
            "id": "46d2944d-3c05-4cff-bfe9-5d7e4c91b6fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2492,
            "y": 2165,
            "speed": 100
        },
        {
            "id": "e7840228-3a71-4095-8d6b-54447edc7b92",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2442,
            "y": 2315,
            "speed": 100
        },
        {
            "id": "54d862ba-a5eb-4c67-84e5-4f3dc8684825",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2328,
            "y": 1843,
            "speed": 100
        },
        {
            "id": "846fc72e-686c-45b9-878e-fb19630facf2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2084,
            "y": 2000,
            "speed": 100
        },
        {
            "id": "eb6f9144-57ea-4515-a0b1-a7880550cd69",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1964,
            "y": 1915,
            "speed": 100
        },
        {
            "id": "2d8cca47-c412-475a-a43e-065e2bccf158",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1900,
            "y": 2027,
            "speed": 100
        },
        {
            "id": "64d2340d-4cc2-4651-9fad-340d690528b5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1730,
            "y": 2000,
            "speed": 100
        },
        {
            "id": "cbf97176-5787-483e-a6f6-df1c31470a22",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1650,
            "y": 1885,
            "speed": 100
        },
        {
            "id": "e5ffc347-2202-4bf0-9dda-0c3c8494935f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1512,
            "y": 2067,
            "speed": 100
        },
        {
            "id": "a820b8d7-0ff1-4a70-a09c-71d577833c9b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1826,
            "y": 1767,
            "speed": 100
        },
        {
            "id": "c47ca3ce-d050-49e1-9159-9a4fc41fa3c4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2016,
            "y": 1639,
            "speed": 100
        },
        {
            "id": "29d334e0-d09b-47f3-9fb3-fbe0664a936b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2162,
            "y": 1525,
            "speed": 100
        },
        {
            "id": "e71267f7-dd7b-4946-b737-a79e494945f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1914,
            "y": 1521,
            "speed": 100
        },
        {
            "id": "aa4005d8-c844-422d-b994-cd3d45963e48",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1836,
            "y": 1305,
            "speed": 100
        },
        {
            "id": "c955aa3f-81b9-4901-acf2-514f82eb4bb9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1776,
            "y": 1443,
            "speed": 100
        },
        {
            "id": "b385b22d-d4d1-4041-9e38-5f52e8ad706e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1616,
            "y": 1503,
            "speed": 100
        },
        {
            "id": "e4fbfd93-419b-4850-9587-34ba869fcf15",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1602,
            "y": 1649,
            "speed": 100
        },
        {
            "id": "cfe7a014-e0c1-4790-b111-10826df809ee",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1320,
            "y": 1781,
            "speed": 100
        },
        {
            "id": "69dbbffc-e25f-457f-bce7-076d45e7d786",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1076,
            "y": 1927,
            "speed": 100
        },
        {
            "id": "03a0bb62-18bd-4f3b-af9c-577692e5f1f2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1024,
            "y": 2147,
            "speed": 100
        },
        {
            "id": "ee5985c2-f3d7-4a48-b9fb-b15e5924f253",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 892,
            "y": 2295,
            "speed": 100
        },
        {
            "id": "191a17e8-de22-4d81-9316-325f6954dfdb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 470,
            "y": 1347,
            "speed": 100
        },
        {
            "id": "d891fee2-66a8-4836-baf9-9940017363bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3411,
            "y": 959,
            "speed": 100
        },
        {
            "id": "fa249a90-07a0-43c7-99c2-508c3b2b3bcc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1266,
            "y": 1237,
            "speed": 100
        },
        {
            "id": "e9ab23e6-6da3-46ee-83b3-a0834e24a2a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3671,
            "y": 2141,
            "speed": 100
        },
        {
            "id": "c93e7032-3ac7-42f4-964e-924ce89ce33d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3753,
            "y": 805,
            "speed": 100
        },
        {
            "id": "f734fcc7-7a61-42e3-b7fd-80d9fc643d6e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 4277,
            "y": 1589,
            "speed": 100
        },
        {
            "id": "c01e8cda-aabf-4cb3-84a9-376d496ed601",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1286,
            "y": 316,
            "speed": 100
        },
        {
            "id": "25d32a89-77df-4a4a-a9ce-c3e1240efa50",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2118,
            "y": 1904,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}