{
    "id": "3048a9a0-b952-4886-a696-9a51013a8087",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_17",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "a286f661-5a2e-4a4f-a0a5-584e236ce001",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3306,
            "y": 675,
            "speed": 100
        },
        {
            "id": "4cbab9b8-15f9-4d3d-99f7-dbbfeda6f737",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3418,
            "y": 626,
            "speed": 100
        },
        {
            "id": "b1f1d62e-bb98-4dd7-9e72-e66c20a10793",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3522,
            "y": 618,
            "speed": 100
        },
        {
            "id": "4cf67efb-aa6a-42dd-abb6-1c5735140f74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3572,
            "y": 622,
            "speed": 100
        },
        {
            "id": "0ab72210-fdd0-4a38-8dc8-ea65bea4f3e3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3631,
            "y": 589,
            "speed": 100
        },
        {
            "id": "86bbf1a4-68c6-4b50-94b3-da3b2b6c0369",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3662,
            "y": 571,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}