{
    "id": "e4bc9fae-a85f-4ee2-b13d-9cf4f973c319",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_44",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "48a3114f-25ea-4262-9e1a-1c3aa2286054",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2585,
            "y": 1438,
            "speed": 100
        },
        {
            "id": "4c6773bf-758c-4381-8ee7-9fb7a10660d5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2599,
            "y": 1355,
            "speed": 100
        },
        {
            "id": "b80deb33-e2c4-496a-bc16-a8c3afac00f3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2632,
            "y": 1290,
            "speed": 100
        },
        {
            "id": "5fd1e21a-ae08-45e6-807b-68f2f23cd179",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2665,
            "y": 1256,
            "speed": 100
        },
        {
            "id": "866b54c2-83f7-421a-8500-b63e93f523a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2647,
            "y": 1160,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}