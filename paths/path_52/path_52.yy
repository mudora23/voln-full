{
    "id": "cd4b4c7d-dd0d-4e25-a19d-b1232cb523cc",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_52",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "382f4d2d-aee4-4fe7-b361-e9cf40a4c7a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1650,
            "y": 1885,
            "speed": 100
        },
        {
            "id": "cffafe1f-5f8c-4354-ad4d-0126847ea9bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1555,
            "y": 1875,
            "speed": 100
        },
        {
            "id": "e082a712-a01f-4b7b-b91b-a025c88f5970",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1493,
            "y": 1830,
            "speed": 100
        },
        {
            "id": "ef4a1375-fbb5-4986-a10d-b780db30d812",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1394,
            "y": 1779,
            "speed": 100
        },
        {
            "id": "76003a3c-55c7-4d8c-9ef0-b2e27b77aa2d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1320,
            "y": 1781,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}