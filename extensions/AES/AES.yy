{
    "id": "122fdab3-6678-41d9-828b-c7c8886b60c3",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "AES",
    "IncludedResources": [
        "Scripts\\AES Encryption\\aes_encrypt_array",
        "Scripts\\AES Encryption\\aes_generate_iv",
        "Scripts\\AES Encryption\\Support Functions\\Internal\\_aes_KeyExpansion",
        "Scripts\\AES Encryption\\Support Functions\\Internal\\_aes_InvCipher",
        "Scripts\\AES Encryption\\Support Functions\\Internal\\_aes_Cipher",
        "Scripts\\AES Encryption\\Support Functions\\Internal\\_aes_KeyExpansion.gml",
        "Scripts\\AES Encryption\\Support Functions\\Internal\\_aes_InvCipher.gml",
        "Scripts\\AES Encryption\\Support Functions\\Internal\\_aes_Cipher.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_to_hex",
        "Scripts\\AES Encryption\\Support Functions\\Data\\ds_list_add_map",
        "Scripts\\AES Encryption\\Support Functions\\Data\\file_binary_read_all_toarray",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_duplicate",
        "Scripts\\AES Encryption\\Support Functions\\Data\\string_to_buffer",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_transpose",
        "Scripts\\AES Encryption\\Support Functions\\Data\\hex_to_array",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_to_base64",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_to_hex.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\ds_list_add_map.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\file_binary_read_all_toarray.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_duplicate.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\string_to_buffer.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_transpose.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\hex_to_array.gml",
        "Scripts\\AES Encryption\\Support Functions\\Data\\array_to_base64.gml",
        "Objects\\obj_AES_Example",
        "Rooms\\rm_AES_Test",
        "Included Files\\GML-AES-Rev1.0.pdf"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2017-29-09 04:02:04",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "c40b074d-5a99-4895-8f83-82ab5c8bf85c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 123146358329582,
            "filename": "aes_controlscripts.gml",
            "final": "_aes_init",
            "functions": [
                {
                    "id": "1ade7d94-95fe-4273-b334-238f648264a5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_aes_init",
                    "help": "_aes_init()",
                    "hidden": false,
                    "kind": 11,
                    "name": "_aes_init",
                    "returnType": 2
                },
                {
                    "id": "e44c9c51-fa63-49f6-8d9c-bee4446b51a2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "_aes_end",
                    "help": "_aes_end()",
                    "hidden": false,
                    "kind": 11,
                    "name": "_aes_end",
                    "returnType": 2
                }
            ],
            "init": "_aes_init",
            "kind": 2,
            "order": [
                
            ],
            "origname": "extensions\\aes_controlscripts.gml",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "aes.eladga.me",
    "productID": "B82FFA7F773D76E64755BCDBB20B6B7C",
    "sourcedir": "",
    "version": "1.0.1"
}