#define _aes_init
///_aes_init()
globalvar _aes;
_aes = ds_map_create();
global._aes_sbox = _aes_lookup_sbox();
global._aes_rsbox = _aes_lookup_rsbox();
global._aes_Rcon = _aes_lookup_Rcon();
global._aes_Nb = 4;
global._aes_Nk = 4;
global._aes_Nr = 10;
global._aes_KEYLEN = 16;
global._aes_IV = array_create(global._aes_KEYLEN);

#define _aes_end
global._aes_IV = 0;
ds_list_destroy(global._aes_Rcon);
ds_list_destroy(global._aes_rsbox);
ds_list_destroy(global._aes_sbox);
ds_map_destroy(_aes);

