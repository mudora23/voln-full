/// @description Insert description here
// You can write your code in this editor

menu_bg_image_alpha = 0;
menu_text_image_alpha = 0;
menu_open = "Main Menu";
menu_open_previous = "Main Menu";
menu_open_bg_image_alpha = 0;
menu_open_text_image_alpha = 0;
menu_hiding = false;
menu_char_id_deleting = "";

menu_title_area_y1 = 30;
menu_title_area_y2 = 150;

menu_content_area_x1 = 351;
menu_content_area_y1 = 200;
menu_content_area_y2 = 900;

menu_listing_area_x2 = 350;
menu_listing_height = 50;

menu_footer_area_y1 = 950;
menu_footer_area_y2 = 1050;

menu_idle_bg_colorscheme = make_color_rgb(5,5,5);
menu_idle_text_colorscheme = c_ltgray;

menu_selected_bg_colorscheme = c_ltgray;
menu_selected_text_colorscheme = make_color_rgb(5,5,5);

colorscheme_almostblack = make_color_rgb(15,15,15);

colorscheme_button_idle_bg = c_lightblack;
colorscheme_button_idle_text = c_ltgray;
colorscheme_button_hover_bg = c_ltgray;
colorscheme_button_hover_text = c_lightblack;

inventory_surface = noone;
inventory_creating_step = 0;
inventory_yoffset = 0;
inventory_yoffset_target = 0;

inventory_using_item = false;
inventory_using_item_name = "";
inventory_using_item_alpha = 1;
inventory_using_item_alpha_min = 0.2;
inventory_using_item_alpha_max = 0.5;
inventory_using_item_alpha_increasing = false;

bar_holding = "";

tooltip = "";

thumbnail_set = false;


coin_animated_frame = 0;
jar_animated_frame = 0;


// quest joural
quest_journal = "";
var qj_list = var_map_get(VAR_QUEST_JOURNAL_LIST);

for(var i = 0; i < ds_list_size(qj_list);i++)
{
	quest_journal+=story_execute_get_raw_text(story_chunk_load(qj_list[| i]));
	quest_journal+="\r";
}

quest_journal_surface = noone;



// char desc

char_desc_1 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART1")); 
char_desc_2 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART2")); 
char_desc_3 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART3")); 
char_desc_4 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART4")); 
char_desc_5 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART5")); 
char_desc_6 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART6")); 
char_desc_8 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART8")); 
char_desc_9 = story_execute_get_raw_text(story_chunk_load("CHAR_DESC_PART9")); 


char_desc = "";

if chunk_mark_get("S_0001_2")
{
	// part 1
	char_desc += char_desc_1;

	// part 2 (GAME_CHAR_DESC_BODY_ENABLED)
	if var_map_get(GAME_CHAR_DESC_BODY_ENABLED)
	    char_desc += char_desc_2;

	// part 3
	char_desc += char_desc_3;

	// part 4
	char_desc += char_desc_4;

	// part 5
	char_desc += char_desc_5;

	// part 6-8 (GAME_CHAR_DESC_HAS_NADS_ENABLED)
	if var_map_get(GAME_CHAR_DESC_HAS_NADS_ENABLED)
	{
		char_desc += char_desc_6;
		char_desc += char_desc_4;
		char_desc += char_desc_8;
	}

	// part 9
	char_desc += char_desc_9;


	//text = text_remove_newmarkups(char_desc); 
	//char_desc = story_execute_newmarkups(char_desc); 
	//char_desc = text;
}

char_desc_surface = noone;




// skill menu
skill_menu_xoffset = 550;
skill_menu_yoffset = 350;
skill_menu_xoffset_target = skill_menu_xoffset;
skill_menu_yoffset_target = skill_menu_yoffset;

skill_menu_zoom_ratio_default = 0.5;
skill_menu_zoom_ratio = skill_menu_zoom_ratio_default;
skill_menu_holding = false;

skill_menu_flashing_ratio = 1;

//obj_control.skill_xmax = 0;
//obj_control.skill_xmin = 0;
//obj_control.skill_ymax = 0;
//obj_control.skill_ymin = 0;

