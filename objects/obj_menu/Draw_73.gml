/// @description Insert description here
// You can write your code in this editor

draw_set_font(font_01n_sc);


if inventory_using_item
{
	if inventory_using_item_alpha_increasing
		inventory_using_item_alpha = approach(inventory_using_item_alpha,0.5,FADE_VERYSLOW_SEC/room_speed);
	else
		inventory_using_item_alpha = approach(inventory_using_item_alpha,0.2,FADE_VERYSLOW_SEC/room_speed);
		
	if inventory_using_item_alpha == 0.5
		inventory_using_item_alpha_increasing = false;
	if inventory_using_item_alpha == 0.2
		inventory_using_item_alpha_increasing = true;
}
else
{
	inventory_using_item_alpha = 1;
	inventory_using_item_alpha_increasing = false;
}



tooltip = "";

if !thumbnail_set
{
	thumbnail_setup();
	thumbnail_set = true;
}


if menu_hiding
{
    menu_bg_image_alpha = approach(menu_bg_image_alpha,0,FADE_NORMAL_SEC/room_speed);
    menu_text_image_alpha = approach(menu_text_image_alpha,0,FADE_NORMAL_SEC/room_speed);
    menu_open_bg_image_alpha = approach(menu_open_bg_image_alpha,0,FADE_NORMAL_SEC/room_speed);
    menu_open_text_image_alpha = approach(menu_open_text_image_alpha,0,FADE_NORMAL_SEC/room_speed);
}
else
{
    menu_bg_image_alpha = approach(menu_bg_image_alpha,0.9,FADE_NORMAL_SEC/room_speed);
    menu_text_image_alpha = approach(menu_text_image_alpha,1,FADE_NORMAL_SEC/room_speed);
    menu_open_bg_image_alpha = approach(menu_open_bg_image_alpha,0.9,FADE_NORMAL_SEC/room_speed);
    menu_open_text_image_alpha = approach(menu_open_text_image_alpha,1,FADE_NORMAL_SEC/room_speed);
}
			
// blur
/*var uni_resolution_hoz = shader_get_uniform(shd_gaussian_horizontal,"resolution");
var uni_resolution_vert = shader_get_uniform(shd_gaussian_vertical,"resolution");
var var_resolution_x = room_width;
var var_resolution_y = room_height;

var uni_blur_amount_hoz = shader_get_uniform(shd_gaussian_vertical,"blur_amount");
var uni_blur_amount_vert = shader_get_uniform(shd_gaussian_horizontal,"blur_amount");
var var_blur_amount = 8.0;
var var_resolution_x = window_get_width(); 
var var_resolution_y = window_get_height();
var var_resolution_ratio = min(var_resolution_x/room_width,var_resolution_y/room_height);

shader_set(shd_gaussian_horizontal);
	shader_set_uniform_f(uni_resolution_hoz, var_resolution_x, var_resolution_y);
	shader_set_uniform_f(uni_blur_amount_hoz, var_blur_amount);
	draw_surface_ext(application_surface,0,0,var_resolution_ratio,var_resolution_ratio,0,c_white,1);
shader_reset();
shader_set(shd_gaussian_vertical);
    shader_set_uniform_f(uni_resolution_vert, var_resolution_x, var_resolution_y);
    shader_set_uniform_f(uni_blur_amount_vert, var_blur_amount);
    draw_surface_ext(application_surface,0,0,var_resolution_ratio,var_resolution_ratio,0,c_white,1);
shader_reset();*/

// dark
draw_set_color(c_black);
draw_set_alpha(menu_bg_image_alpha*0.95);
draw_rectangle(0, 0, room_width, room_height, false);

// title BG
draw_set_color(menu_idle_bg_colorscheme);
draw_set_alpha(menu_bg_image_alpha);
//draw_rectangle(0, menu_title_area_y1, room_width, menu_title_area_y2, false);
var xx = 20;
var yy = menu_title_area_y1;
var xx_wdith = room_width-40;
var yy_wdith = menu_title_area_y2-menu_title_area_y1;
draw_sprite_in_area_fill(spr_gui_bg_seemless,xx,yy,xx_wdith,yy_wdith,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),menu_bg_image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_zero);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,xx,yy,xx_wdith,yy_wdith,c_white,menu_bg_image_alpha);
gpu_set_blendmode(bm_normal);
draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,xx,yy,xx_wdith,yy_wdith,menu_bg_image_alpha,fa_right,fa_bottom);


// title text
draw_set_color(c_dkwhite);
draw_set_alpha(menu_open_text_image_alpha);
draw_set_valign(fa_center);
draw_set_halign(fa_left);
//draw_set_font(font_general_60_b_small_caps);
draw_text_transformed(100, (menu_title_area_y1 + menu_title_area_y2) / 2,menu_open,1.8,1.8,0);


// listing
//draw_set_font(font_general_20_b_small_caps);

var x1 = 0;
var y1 = menu_content_area_y1;
var x2 = menu_listing_area_x2;
var y2 = menu_content_area_y2;
draw_set_color(menu_idle_bg_colorscheme);
draw_set_alpha(menu_bg_image_alpha);
draw_rectangle(x1,y1,x2,y2,false);

	
if room == room_game
{
	var x1 = 0;
	var y1 = menu_content_area_y1;
	var x2 = menu_listing_area_x2;
	var y2 = y1 + menu_listing_height - 1;
	#macro MENULISTING_RESUME "Resume"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_RESUME,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_ITEMS "Items"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_ITEMS,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_APPEARANCE "Appearance"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_APPEARANCE,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);
	
	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_ATTRIBUTES_AND_SKILLS "Attributes and skills"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_ATTRIBUTES_AND_SKILLS,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_ALLIES "Allies"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_ALLIES,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_PETS "Pets"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_PETS,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_QUEST_JOURNAL "Quest Journal"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_QUEST_JOURNAL,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_SAVE "Save"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_SAVE,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,var_map_get(VAR_MODE)!=VAR_MODE_BATTLE && var_map_get(VAR_MODE)!=VAR_MODE_STORE && var_map_get(VAR_MODE)!=VAR_MODE_SEX_CHOOSING && var_map_get(VAR_MODE)!=VAR_MODE_CAPTURE_CHOOSING/* && var_map_get(VAR_MODE)!=VAR_MODE_MAP*/);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_LOAD "Load"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_LOAD,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_OPTIONS "Options"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_OPTIONS,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	#macro MENULISTING_EXIT "Exit"
	draw_menu_listing_button(x1,y1,x2,y2,MENULISTING_EXIT,menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

}
else
{
	var x1 = 0;
	var y1 = menu_content_area_y1;
	var x2 = menu_listing_area_x2;
	var y2 = y1 + menu_listing_height - 1;
	draw_menu_listing_button(x1,y1,x2,y2,"Return",menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);

	y1 += menu_listing_height;
	y2 += menu_listing_height;
	draw_menu_listing_button(x1,y1,x2,y2,"Load",menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);
	
	y1 += menu_listing_height;
	y2 += menu_listing_height;
	draw_menu_listing_button(x1,y1,x2,y2,"Options",menu_idle_bg_colorscheme,menu_idle_text_colorscheme,menu_selected_bg_colorscheme,menu_selected_text_colorscheme,true);


}





if menu_open == MENULISTING_RESUME || menu_open == "Return" instance_destroy();

// content
if menu_open != "Main Menu" && menu_open != MENULISTING_RESUME && menu_open != "Return"
{
    var x1 = menu_listing_area_x2+1;
    var y1 = menu_content_area_y1;
    var x2 = room_width-20;
    var y2 = menu_content_area_y2;
    draw_set_color(menu_selected_bg_colorscheme);
	draw_set_alpha(menu_bg_image_alpha);
    //draw_rectangle(x1,y1,x2,y2,false);
	draw_sprite_in_area_fill(spr_gui_bg_seemless,x1,y1,x2-x1,y2-y1,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),menu_bg_image_alpha,fa_right,fa_bottom);
	gpu_set_blendmode_ext(bm_dest_color, bm_zero);
	draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x1,y1,x2-x1,y2-y1,c_white,menu_bg_image_alpha);
	gpu_set_blendmode(bm_normal);

    if menu_open == MENULISTING_ITEMS
    {

        
        var padding = 20;
		var card_padding = 10;
		var card_width = 450;
		var card_height = 110;
		var card_x1 = x1+padding;
		var card_y1 = y1+padding;
		var card_x2 = card_x1+card_width;
		var card_y2 = card_y1+card_height;
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
        draw_set_alpha(menu_open_text_image_alpha);
		draw_menu_inventory_char_card(obj_control.var_map[? VAR_PLAYER],card_padding,card_x1,card_y1,card_x2,card_y2);
		
		
		var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
		for(var i = 0; i < ds_list_size(ally_list);i++)
		{

			var ally = ally_list[| i];
			card_y1 += card_height + card_padding;
			card_y2 += card_height + card_padding;
			draw_set_halign(fa_left);
			draw_set_valign(fa_top);
			draw_set_alpha(menu_open_text_image_alpha);
			draw_menu_inventory_char_card(ally,card_padding,card_x1,card_y1,card_x2,card_y2);
		}


















        var inventory_x1 = card_x2 +padding;
        var inventory_y1 = y1 + padding + 45;
        var inventory_x2 = x2 - padding;
        var inventory_y2 = y2 - padding;
        var inventory_height = inventory_y2-inventory_y1;
        var inventory_width = inventory_x2 - inventory_x1;
        

        
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		draw_set_alpha(menu_open_text_image_alpha);
        draw_set_color(colorscheme_almostblack);
        //draw_rectangle(inventory_x1,inventory_y1,inventory_x2,inventory_y2,false);
		draw_sprite_stretched_ext(spr_button_choice,2,inventory_x1,inventory_y1-40,inventory_x2-inventory_x1,inventory_y2-inventory_y1+80,draw_get_color(),draw_get_alpha());
		draw_sprite_stretched_ext(spr_button_choice,3,inventory_x1,inventory_y1-40,inventory_x2-inventory_x1,inventory_y2-inventory_y1+80,draw_get_color(),draw_get_alpha());

        draw_set_color(c_lightblack);
        //draw_rectangle(inventory_x1,y1 + padding, inventory_x2,y1 + padding+45,false);
		draw_sprite_stretched_ext(spr_button_choice,2,inventory_x1,y1 + padding,inventory_x2-inventory_x1,50,draw_get_color(),draw_get_alpha());
		draw_sprite_stretched_ext(spr_button_choice,3,inventory_x1,y1 + padding,inventory_x2-inventory_x1,50,draw_get_color(),draw_get_alpha());


        draw_set_color(c_silver);
        draw_text(inventory_x1+40,y1 + padding + 2,"Inventory");
		
		var inventory_total_weight = inventory_get_total_weight();
		if inventory_total_weight > inventory_get_max_weight() draw_set_color(c_red);
		else draw_set_color(c_lime);
		draw_text(inventory_x1+40+string_width("Inventory"),y1 + padding + 2," ("+string(inventory_total_weight)+"lb/"+string(inventory_get_max_weight())+"lb)");
		draw_set_color(c_silver);
		
        var name_list = obj_control.var_map[? VAR_INVENTORY_LIST];
        //var type_list = obj_control.game_map[? GAME_INVENTORY_LIST_TYPE_LIST];
        var amount_list = obj_control.var_map[? VAR_INVENTORY_AMOUNT_LIST];
        //var des_list = obj_control.var_map[? GAME_INVENTORY_LIST_DES_LIST];
        //var use_in_menu_list = obj_control.game_map[? GAME_INVENTORY_LIST_USE_IN_MENU_LIST];
        //var use_in_combat_list = obj_control.game_map[? GAME_INVENTORY_LIST_USE_IN_COMBAT_LIST];
        
		
		inventory_yoffset = smooth_approach_room_speed(inventory_yoffset,inventory_yoffset_target,SMOOTH_SLIDE_NORMAL_SEC);
		
		var mouse_on_inventory = mouse_over_area(inventory_x1,inventory_y1,inventory_x2-inventory_x1,inventory_y2-inventory_y1);
		if mouse_on_inventory
		{
			if mouse_wheel_down() inventory_yoffset_target -= 120;
			if mouse_wheel_up() inventory_yoffset_target += 120;
		}
		
		if !surface_exists(inventory_surface)
		{
			inventory_surface = surface_create(1920,5000);
			surface_set_target(inventory_surface);
			draw_clear_alpha(c_white,0);
			surface_reset_target();
			inventory_creating_step = 0;
		}
		else
		{
			inventory_creating_step++;
		}
		
		
		
        var yy = inventory_y1 + inventory_yoffset + 20;
        var item_height = 62;
        //draw_set_font(font_general_25);

        for(var i = 0; i < ds_list_size(name_list);i++)
        {

			
	        var name = name_list[| i];
		    var displayname = item_get_displayname(name);
	        var type = item_get_type(name);
	        var amount = amount_list[| i];
	        var des = item_get_des(name);
	        var use_in_menu = item_can_use_in_menu(name) && var_map_get(VAR_MODE) != VAR_MODE_BATTLE;
	        var use_in_combat = item_can_use_in_combat(name);
	        var item_hover = mouse_over_area(inventory_x1,yy,inventory_x2-inventory_x1,item_height-1);
            
				if inventory_creating_step == 1
				{
					surface_set_target(inventory_surface);
					
					//draw_set_font(font_general_20);
					draw_set_alpha(1);
					
					var item_icon_x1 = inventory_x1+30;
					var item_icon_y_center = yy+item_height/2 - inventory_yoffset;
					var item_icon_w = string_height("W");
					var item_icon_h = string_height("W");

					draw_itemtype_icon_in_area(item_icon_x1,item_icon_y_center-item_icon_h/2,item_icon_w,item_icon_h,type);
					
			        draw_set_valign(fa_bottom);
			        draw_set_halign(fa_left);
					draw_set_alpha(1);
					draw_set_color(c_white);
					//draw_text_outlined(item_icon_x1+item_icon_w,item_icon_y_center+item_icon_h/2+8,amount,c_dkwhite,c_black,0.5,3,10); 
					draw_text_transformed(item_icon_x1+item_icon_w-7,item_icon_y_center+item_icon_h/2+5,amount,0.5,0.5,0); 
					
					draw_set_color(c_white);
			        draw_set_valign(fa_center);
			        draw_set_halign(fa_left);
					draw_set_alpha(1);
					draw_text(item_icon_x1 + item_icon_w + 20,item_icon_y_center,displayname+" ("+string(item_get_weight(name)*amount)+"lb)");
					

					//draw_text_outlined(item_xx+50,item_yy,name_list[| ii],c_white,c_almostblack,1,3,20);
					//draw_text_outlined(item_xx+300,item_yy,"Amount: "+string(price_list[| ii]),c_white,c_almostblack,1,3,20);
				
						//draw_set_font(font_general_text_17);
						//draw_text_outlined_ext(item_xx+20,item_yy+35,des_list[| ii],c_white,c_almostblack,1,3,20,27,aside_frame_area_width-40);
						//draw_text_outlined(item_xx+50,item_yy+30,des_list[| ii],c_white,c_lightblack,1,3,20);
					surface_reset_target();
				}
					
			if yy >= inventory_y1 - item_height/4 && yy <= inventory_y2 + item_height/4
			{

				if item_hover && !inventory_using_item
	            {
	                draw_set_color(c_silver);
					draw_set_alpha(0.1);
	                draw_rectangle(inventory_x1+20,max(yy,inventory_y1),inventory_x2-20,min(yy+item_height-1,inventory_y2-20),false);
	                draw_set_color(c_lightblack);
					draw_set_alpha(menu_open_text_image_alpha);
				
				

				

	                tooltip = displayname+" ("+string(item_get_weight(name)*amount)+"lb) - " + des;
					

					
	                if use_in_menu
	                {
	                    var button_x1 = inventory_x2 - 120;
	                    var button_x2 = inventory_x2 - 30;
	                    var button_width = button_x2 - button_x1;
	                    var button_y1 = yy + 10;
	                    var button_height = item_height - 20;
	                    var button_y2 = button_y1 + button_height;
	                    var button_hover = mouse_over_area(button_x1,button_y1,button_width,button_height);
	                    draw_set_valign(fa_center);
	                    draw_set_halign(fa_center);
	                    if button_hover
	                    {
	                        draw_set_color(c_silver);
	                        draw_rectangle(button_x1,max(button_y1,inventory_y1),button_x2,min(button_y2,inventory_y2-20),false);
	                        draw_set_color(c_lightblack);
	                        draw_rectangle(button_x1,max(button_y1,inventory_y1),button_x2,min(button_y2,inventory_y2-20),true);
							if button_y1 >= inventory_y1 && button_y1+button_height < inventory_y2
								draw_text(button_x1+button_width/2,button_y1+button_height/2,"Use");
	                        if action_button_pressed_get()
	                        {
	                            action_button_pressed_set(false);
								inventory_using_item = true;
								inventory_using_item_name = name;

								


	                        }
	                    }
	                    else
	                    {
	                        draw_set_color(c_lightblack);
	                        draw_rectangle(button_x1,max(button_y1,inventory_y1),button_x2,min(button_y2,inventory_y2-20),false);
	                        draw_set_color(c_silver);
							if button_y1 >= inventory_y1 && button_y1+button_height < inventory_y2
								draw_text(button_x1+button_width/2,button_y1+button_height/2,"Use");
	                    }
	                }
	            }
	            else
	            {
	                //draw_set_color(c_silver);
	                //draw_text(inventory_x1+30,yy+item_height/2,name+" ("+string(amount)+")");
	            }
            }
            yy+=item_height
        }
        if ds_list_size(name_list) == 0
        {
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(c_silver);
            draw_text_transformed(inventory_x1+inventory_width/2,inventory_y1+inventory_height/2,"Your inventory is empty.",0.7,0.7,0); 
        }
		else
		{
			if surface_exists(inventory_surface)
				draw_surface_in_visible_area(inventory_surface,0,inventory_yoffset,1,1,c_white,1,inventory_x1,inventory_y1+10,inventory_x2-inventory_x1,inventory_y2-inventory_y1-20);

		}
		inventory_yoffset_target = clamp(inventory_yoffset_target,min(-(yy - inventory_y1 - 20 - inventory_yoffset - (inventory_y2-inventory_y1) + 50),0),0);

		
        draw_set_valign(fa_center);
        draw_set_halign(fa_left);
		draw_set_color(c_ltgray);
		// gold icon
		if floor(coin_animated_frame) == 0
			coin_animated_frame += (1/room_speed)/1.5;
		else
			coin_animated_frame += (1/room_speed)*10;
		
		if floor(coin_animated_frame) >= 3
			coin_animated_frame = 0;
		
		var icon = spr_icon_coin;
		var icon_width = sprite_get_width(icon);
		var icon_area_width = 120+string_width(string(obj_control.var_map[? VAR_PET_R_LIST]));
		var icon_height = sprite_get_height(icon);
		
		var xx = x1+padding+icon_width/2;
		var yy = y2-10-icon_height/2;
		
		var icon_area_hover = mouse_over_area(xx-icon_width/2,yy-icon_height/2,icon_area_width,icon_height);
		if icon_area_hover
		{
			draw_sprite(icon, 4, xx,yy+5);
			tooltip = "This is the total amount of gold you have.";
		}
		draw_sprite(icon, coin_animated_frame, xx,yy);
		draw_text(xx+icon_width/2+5,yy,obj_control.var_map[? VAR_GOLD]);
		
		// jar icon
		if floor(jar_animated_frame) == 0
			jar_animated_frame += (1/room_speed)/2;
		else
			jar_animated_frame += (1/room_speed)*10;
		
		if floor(jar_animated_frame) >= 3
			jar_animated_frame = 0;
			
		var icon = spr_icon_jar;
		var icon_width = sprite_get_width(icon);
		var icon_area_width = 200;
		var icon_height = sprite_get_height(icon);
		
		var xx = xx+icon_area_width;

		var icon_area_hover = mouse_over_area(xx-icon_width/2,yy-icon_height/2,icon_area_width,icon_height);
		if icon_area_hover
		{
			draw_sprite(icon, 4, xx,yy+5);
			tooltip = "This is the total amount of empty jars you have.";
		}
		draw_sprite(icon, coin_animated_frame, xx,yy);
		draw_text(xx+icon_width/2+5,yy,obj_control.var_map[? VAR_JAR]);   

		


	}
    if menu_open == MENULISTING_APPEARANCE
    {

        var padding = 40;
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		draw_set_alpha(menu_open_text_image_alpha);
        draw_set_color(colorscheme_almostblack);




        var char_desc_x1 = x1 + padding;
        var char_desc_x2 = x2 - padding;
        var char_desc_width = char_desc_x2 - char_desc_x1;
        var char_desc_y1 = y1+padding;
       
        var char_desc_y2 = y2 - padding;
	    var char_desc_height = char_desc_y2 - char_desc_y1;

        //draw_set_font(font_general_20_char_desc);
        draw_set_color(colorscheme_almostblack);

		draw_sprite_stretched_ext(spr_button_choice,2,char_desc_x1,char_desc_y1-40,char_desc_x2-char_desc_x1,char_desc_y2-char_desc_y1+80,draw_get_color(),draw_get_alpha());
		draw_sprite_stretched_ext(spr_button_choice,3,char_desc_x1,char_desc_y1-40,char_desc_x2-char_desc_x1,char_desc_y2-char_desc_y1+80,draw_get_color(),draw_get_alpha());
        draw_set_valign(fa_top);
        draw_set_halign(fa_left);   
        draw_set_color(c_ltgray);
		draw_set_font(font_01n_reg);
		
		char_desc_surface = draw_justified_text_in_area(char_desc,char_desc_surface,char_desc_x1+10,char_desc_y1,char_desc_x2-20,char_desc_y2,padding,5,20,0.8);
		


	}
    
    if menu_open == MENULISTING_ATTRIBUTES_AND_SKILLS
    {
		draw_menu_player_frame(x1+20,y1+20,x1+20+300,y2-20,char_get_player());
		draw_menu_skill_tree(x1+20+300,y1+20,x2,y2-20);
		
    }
	
    if menu_open == MENULISTING_ALLIES
    {
        var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
        if ds_list_size(ally_list) > 0
            draw_menu_ally_frame(x1+20,y1+20,x1+20+300,y2-20,ally_list[| 0]);
        if ds_list_size(ally_list) > 1
            draw_menu_ally_frame(x1+20+300+30,y1+20,x1+20+300*2+30,y2-20,ally_list[| 1]);
        if ds_list_size(ally_list) > 2
            draw_menu_ally_frame(x1+20+300*2+30*2,y1+20,x1+20+300*3+30*2,y2-20,ally_list[| 2]);
        if ds_list_size(ally_list) > 3
            draw_menu_ally_frame(x1+20+300*3+30*3,y1+20,x1+20+300*4+30*3,y2-20,ally_list[| 3]);
    }
    
    if menu_open == MENULISTING_PETS
    {
        var pet_list = obj_control.var_map[? VAR_PET_LIST];
        if ds_list_size(pet_list) > 0
            draw_menu_pet_frame(x1+20,y1+20,x1+20+300,y2-20,pet_list[| 0]);
        if ds_list_size(pet_list) > 1
            draw_menu_pet_frame(x1+20+300+30,y1+20,x1+20+300*2+30,y2-20,pet_list[| 1]);
        if ds_list_size(pet_list) > 2
            draw_menu_pet_frame(x1+20+300*2+30*2,y1+20,x1+20+300*3+30*2,y2-20,pet_list[| 2]);
        if ds_list_size(pet_list) > 3
            draw_menu_pet_frame(x1+20+300*3+30*3,y1+20,x1+20+300*4+30*3,y2-20,pet_list[| 3]);
    }
	
    if menu_open == MENULISTING_QUEST_JOURNAL
    {
		var_map_set(VAR_NEW_QJ_NOTICE,false);
        draw_set_valign(fa_top);
        draw_set_halign(fa_left);
        draw_set_font(font_01n_reg);
		draw_set_alpha(menu_open_text_image_alpha);
        //draw_set_color(c_lightblack);
        //draw_rectangle(x1+50,y1+50,x2-50,y2-50,false);
        draw_set_alpha(menu_open_text_image_alpha);
		
        draw_set_color(colorscheme_almostblack);
        //draw_rectangle(char_desc_x1,char_desc_y1,char_desc_x2,char_desc_y2,false);
		draw_sprite_stretched_ext(spr_button_choice,2,x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
		draw_sprite_stretched_ext(spr_button_choice,3,x1,y1,x2-x1,y2-y1,draw_get_color(),draw_get_alpha());
		
		
		draw_set_color(c_silver);
        //draw_text_ext(x1+80, y1+80, quest_journal, 50, x2-x1-200);
		draw_justified_text_in_area(quest_journal,quest_journal_surface,x1,y1,x2,y2,80,15,25,1);
	}
	
    if menu_open == MENULISTING_SAVE
    {
        var xmargin = 50;
        var ymargin = 50;
        var file_width = (x2-x1-xmargin*3)/2;
        var file_height = (y2-y1-ymargin*4)/3;
        
        draw_saveload_slot(x1+xmargin,y1+ymargin,x1+xmargin+file_width,y1+ymargin+file_height,"Save",1);
        draw_saveload_slot(x1+xmargin*2+file_width,y1+ymargin,x1+xmargin*2+file_width*2,y1+ymargin+file_height,"Save",2);
        draw_saveload_slot(x1+xmargin,y1+ymargin*2+file_height,x1+xmargin+file_width,y1+ymargin*2+file_height*2,"Save",3);
        draw_saveload_slot(x1+xmargin*2+file_width,y1+ymargin*2+file_height,x1+xmargin*2+file_width*2,y1+ymargin*2+file_height*2,"Save",4);
        draw_saveload_slot(x1+xmargin,y1+ymargin*3+file_height*2,x1+xmargin+file_width,y1+ymargin*3+file_height*3,"Save",5);
        draw_saveload_slot(x1+xmargin*2+file_width,y1+ymargin*3+file_height*2,x1+xmargin*2+file_width*2,y1+ymargin*3+file_height*3,"Save",6);
    }
    if menu_open == MENULISTING_LOAD
    {
        var xmargin = 50;
        var ymargin = 50;
        var file_width = (x2-x1-xmargin*3)/2;
        var file_height = (y2-y1-ymargin*4)/3;
        
        draw_saveload_slot(x1+xmargin,y1+ymargin,x1+xmargin+file_width,y1+ymargin+file_height,"Load",1);
        draw_saveload_slot(x1+xmargin*2+file_width,y1+ymargin,x1+xmargin*2+file_width*2,y1+ymargin+file_height,"Load",2);
        draw_saveload_slot(x1+xmargin,y1+ymargin*2+file_height,x1+xmargin+file_width,y1+ymargin*2+file_height*2,"Load",3);
        draw_saveload_slot(x1+xmargin*2+file_width,y1+ymargin*2+file_height,x1+xmargin*2+file_width*2,y1+ymargin*2+file_height*2,"Load",4);
        draw_saveload_slot(x1+xmargin,y1+ymargin*3+file_height*2,x1+xmargin+file_width,y1+ymargin*3+file_height*3,"Load",5);
        draw_saveload_slot(x1+xmargin*2+file_width,y1+ymargin*3+file_height*2,x1+xmargin*2+file_width*2,y1+ymargin*3+file_height*3,"Load",6);
    }
    if menu_open == MENULISTING_OPTIONS
    {
        draw_set_alpha(menu_open_text_image_alpha);
        
        // Screen Mode
        
            // text
            var text_x = x1 + 150;
            var text_y = y1 + 50;
            draw_set_valign(fa_top);
            draw_set_halign(fa_left);
            draw_set_color(menu_idle_text_colorscheme);
            //draw_set_font(font_general_45_b);
            draw_text(text_x,text_y,"Screen Mode");
            
            // BG
            var bar_x = x1 + 600;
            var bar_y = text_y;
            var bar_width = 800;
            var bar_height = 60;
            
            // button
            var button_x = bar_x;
            var button_y = bar_y;
            var button_width = bar_width/2;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_FULLSCREEN] || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_FULLSCREEN] = true;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Fullscreen");
            
            // button 2
            var button_x = bar_x+bar_width/2;
            var button_y = bar_y;
            var button_width = bar_width/2;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if !obj_control.settings_map[? SETTINGS_FULLSCREEN] || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_FULLSCREEN] = false;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Windowed");
            
            
        // VSync Mode
        
            // text
            var text_x = x1 + 150;
            var text_y = y1 + 50 + 80*1;
            draw_set_valign(fa_top);
            draw_set_halign(fa_left);
            draw_set_color(menu_idle_text_colorscheme);
            //draw_set_font(font_general_45_b);
            draw_text(text_x,text_y,"VSync");
            
            // BG
            var bar_x = x1 + 600;
            var bar_y = text_y;
            var bar_width = 800;
            var bar_height = 60;
            
            // button
            var button_x = bar_x;
            var button_y = bar_y;
            var button_width = bar_width/2;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_VSYNC] || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_VSYNC] = true;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Yes");
            
            // button 2
            var button_x = bar_x+bar_width/2;
            var button_y = bar_y;
            var button_width = bar_width/2;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if !obj_control.settings_map[? SETTINGS_VSYNC] || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_VSYNC] = false;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"No");
            
        // Anti-aliasing
        
            // text
            var text_x = x1 + 150;
            var text_y = y1 + 50 + 80*2;
            draw_set_valign(fa_top);
            draw_set_halign(fa_left);
            draw_set_color(menu_idle_text_colorscheme);
            //draw_set_font(font_general_45_b);
            draw_text(text_x,text_y,"Anti-aliasing");
            
            // BG
            var bar_x = x1 + 600;
            var bar_y = text_y;
            var bar_width = 800;
            var bar_height = 60;
            
            // button
            var button_x = bar_x;
            var button_y = bar_y;
            var button_width = bar_width/5;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_AA] == 0 || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_AA] = 0;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Off");
            
            // button 2
            var button_x = bar_x+bar_width/5;
            var button_y = bar_y;
            var button_width = bar_width/5;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_AA] == 2 || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_AA] = 2;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"2x");
            
            // button 3
            var button_x = bar_x+bar_width/5*2;
            var button_y = bar_y;
            var button_width = bar_width/5;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_AA] == 4 || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_AA] = 4;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"4x");
            
            // button 4
            var button_x = bar_x+bar_width/5*3;
            var button_y = bar_y;
            var button_width = bar_width/5;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_AA] == 8 || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_AA] = 8;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"8x");
            
            // button 5
            var button_x = bar_x+bar_width/5*4;
            var button_y = bar_y;
            var button_width = bar_width/5;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_AA] == 16 || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_AA] = 16;
				obj_control.settings_map_display_reset_needed = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"16x");
            
            
        // Music Level
        
            // text
            var text_x = x1 + 150;
            var text_y = y1 + 50 + 80*3;
            draw_set_valign(fa_top);
            draw_set_halign(fa_left);
            draw_set_color(menu_idle_text_colorscheme);
            //draw_set_font(font_general_45_b);
            draw_text(text_x,text_y,"Music Level");
            
            // bar
            var bar_x = x1 + 600;
            var bar_y = text_y;
            var bar_width = 800;
            var bar_height = 60;
            var bar_hover = mouse_over_area(bar_x,bar_y,bar_width,bar_height);
			draw_set_color(menu_idle_bg_colorscheme);
            //draw_rectangle(bar_x,bar_y,bar_x+bar_width,bar_y+bar_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha());
			
            if bar_hover && action_button_pressed_get()
			{
				action_button_pressed_set(false);
                bar_holding = "Music Level";
				voln_play_sfx(sfx_mouse_click);
			}
            if !mouse_check_button(mb_left) && bar_holding == "Music Level"
                bar_holding = "";
            if bar_holding == "Music Level"
            {
                obj_control.settings_map[? SETTINGS_MUSIC_LEVEL] = round(clamp((mouse_x - bar_x) / bar_width,0,1)*100);
            }
            draw_set_color(c_silver);
            //draw_rectangle(bar_x,bar_y,bar_x+bar_width*obj_control.settings_map[? SETTINGS_MUSIC_LEVEL]/100,bar_y+bar_height,false);
			draw_sprite_in_area_stretch_to_fit_in_visible_area(spr_button_choice,2,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha(),bar_x,bar_y,bar_width*obj_control.settings_map[? SETTINGS_MUSIC_LEVEL]/100,bar_height);
			draw_sprite_in_area_stretch_to_fit_in_visible_area(spr_button_choice,3,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha(),bar_x,bar_y,bar_width*obj_control.settings_map[? SETTINGS_MUSIC_LEVEL]/100,bar_height);
            
        // SFX Level
        
            // text
            var text_x = x1 + 150;
            var text_y = y1 + 50 + 80*4;
            draw_set_valign(fa_top);
            draw_set_halign(fa_left);
            draw_set_color(menu_idle_text_colorscheme);
            //draw_set_font(font_general_45_b);
            draw_text(text_x,text_y,"SFX Level");
            
            // bar
            var bar_x = x1 + 600;
            var bar_y = text_y;
            var bar_width = 800;
            var bar_height = 60;
            var bar_hover = mouse_over_area(bar_x,bar_y,bar_width,bar_height);
			draw_set_color(menu_idle_bg_colorscheme);
            //draw_rectangle(bar_x,bar_y,bar_x+bar_width,bar_y+bar_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha());
			
			
            if bar_hover && action_button_pressed_get()
			{
				action_button_pressed_set(false);
                bar_holding = "SFX Level";
				voln_play_sfx(sfx_mouse_click);
			}
            if !mouse_check_button(mb_left) && bar_holding == "SFX Level"
                bar_holding = "";
            if bar_holding == "SFX Level"
            {
                obj_control.settings_map[? SETTINGS_SFX_LEVEL] = round(clamp((mouse_x - bar_x) / bar_width,0,1)*100);
            }
            draw_set_color(c_silver);
            //draw_rectangle(bar_x,bar_y,bar_x+bar_width*obj_control.settings_map[? SETTINGS_SFX_LEVEL]/100,bar_y+bar_height,false);
			draw_sprite_in_area_stretch_to_fit_in_visible_area(spr_button_choice,2,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha(),bar_x,bar_y,bar_width*obj_control.settings_map[? SETTINGS_SFX_LEVEL]/100,bar_height);
			draw_sprite_in_area_stretch_to_fit_in_visible_area(spr_button_choice,3,bar_x,bar_y,bar_width,bar_height,draw_get_color(),draw_get_alpha(),bar_x,bar_y,bar_width*obj_control.settings_map[? SETTINGS_SFX_LEVEL]/100,bar_height);
            
			
        // Copy story to clipboard
        
            // text
            var text_x = x1 + 150;
            var text_y = y1 + 50 + 80*5;
            draw_set_valign(fa_top);
            draw_set_halign(fa_left);
            draw_set_color(menu_idle_text_colorscheme);
            //draw_set_font(font_general_45_b);
            draw_text(text_x,text_y,"Copy story to clipboard");
            
            // BG
            var bar_x = x1 + 600;
            var bar_y = text_y;
            var bar_width = 800;
            var bar_height = 60;
            
            // button
            var button_x = bar_x;
            var button_y = bar_y;
            var button_width = bar_width/2;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD] || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD] = true;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Auto");
            
            // button 2
            var button_x = bar_x+bar_width/2;
            var button_y = bar_y;
            var button_width = bar_width/2;
            var button_height = bar_height;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if !obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD] || button_hover
            {
                var button_color = c_silver;
                var button_text_color = menu_selected_text_colorscheme;
            }
            else
            {
                var button_color = menu_idle_bg_colorscheme;
                var button_text_color = c_gray;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD] = false;
            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Manual");
			
			
        // Restore / Restore Defaults
        
            // button
            var button_x = (x1+x2)/2 - 550;
            var button_y = y1 + 50 + 80*6+30;
            var button_width = 400;
            var button_height = 80;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if button_hover
            {
                var button_color = colorscheme_button_hover_bg;
                var button_text_color = colorscheme_button_hover_text;
            }
            else
            {
                var button_color = colorscheme_button_idle_bg;
                var button_text_color = colorscheme_button_idle_text;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                with obj_control
                {
                    settings_map_apply();
                    ds_map_json_save(settings_map,"settings.dat");
                }

            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Apply");
            
            // button2
            var button_x = (x1+x2)/2 + 50;
            var button_width = 400;
            var button_height = 80;
            var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
            if button_hover
            {
                var button_color = colorscheme_button_hover_bg;
                var button_text_color = colorscheme_button_hover_text;
            }
            else
            {
                var button_color = colorscheme_button_idle_bg;
                var button_text_color = colorscheme_button_idle_text;
            }
            if button_hover && action_button_pressed_get()
            {
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
                with obj_control
                {
                    ds_map_destroy(settings_map);
                    if file_exists("settings.dat") file_delete("settings.dat");
                    settings_map_init();
                    settings_map_apply();
                }

            }
            draw_set_color(button_color);
            //draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_x,button_y,button_width,button_height,button_color,draw_get_alpha());
            draw_set_valign(fa_center);
            draw_set_halign(fa_center);
            draw_set_color(button_text_color);
            //draw_set_font(font_general_30_b_small_caps);
            draw_text(button_x+button_width/2,button_y+button_height/2,"Restore Defaults");

    }

    if menu_open == MENULISTING_EXIT
    {
        draw_set_valign(fa_center);
        draw_set_halign(fa_center);
        draw_set_font(font_01b_reg);
        draw_set_color(c_ltgray);
        draw_set_alpha(menu_open_text_image_alpha);
        draw_text((x1+x2)/2,(y1+y2)/2.5,"Do you really want to quit?\rAny unsaved progress will be lost!");
         draw_set_font(font_01b_reg);
		 
        var button_xx_center = (x1+x2)/3;
        var button_yy_center = (y1+y2)/3*2;
        var button_width = 300;
        var button_height = 120;
        var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);
        if button_hover
        {
            draw_set_color(c_silver);
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(menu_selected_text_colorscheme);
            draw_text(button_xx_center,button_yy_center,"Yes");
            if button_hover && action_button_pressed_get()
            {
                room_goto(room_title_screen);
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
				scene_sprites_list_clear();
				scene_cg_list_clear();
				script_execute_list_clear();
                instance_destroy();
            }
                
        }
        else
        {
            draw_set_color(make_color_rgb(15,15,15));
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(c_gray);
            draw_text(button_xx_center,button_yy_center,"Yes");
        }
        
        var button_xx_center = (x1+x2)/3*2;
        var button_yy_center = (y1+y2)/3*2;
        var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);
        if button_hover
        {
            draw_set_color(c_silver);
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(menu_selected_text_colorscheme);
            draw_text(button_xx_center,button_yy_center,"No");
            if button_hover && action_button_pressed_get()
            {
                instance_destroy();
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
            }
        }
        else
        {
            draw_set_color(make_color_rgb(15,15,15));
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(c_gray);
            draw_text(button_xx_center,button_yy_center,"No");
        }

    }
	
	#macro MENULISTING_DELETE_CHAR "Delete character"
    if menu_open == MENULISTING_DELETE_CHAR
    {
        draw_set_valign(fa_center);
        draw_set_halign(fa_center);
        draw_set_font(font_01b_reg);
        draw_set_color(c_ltgray);
        draw_set_alpha(menu_open_text_image_alpha);
        draw_text((x1+x2)/2,(y1+y2)/2.5,"Dismiss this character permanently?\rThis action cannot be reverted!");
         draw_set_font(font_01b_reg);
		 
        var button_xx_center = (x1+x2)/3;
        var button_yy_center = (y1+y2)/3*2;
        var button_width = 300;
        var button_height = 120;
        var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);
        if button_hover
        {
            draw_set_color(c_silver);
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(menu_selected_text_colorscheme);
            draw_text(button_xx_center,button_yy_center,"Yes");
            if button_hover && action_button_pressed_get()
            {
                
				char_id_remove_char(menu_char_id_deleting);
				
                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
				
				menu_open = menu_open_previous;
				
            }
                
        }
        else
        {
            draw_set_color(make_color_rgb(15,15,15));
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(c_gray);
            draw_text(button_xx_center,button_yy_center,"Yes");
        }
        
        var button_xx_center = (x1+x2)/3*2;
        var button_yy_center = (y1+y2)/3*2;
        var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);
        if button_hover
        {
            draw_set_color(c_silver);
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(menu_selected_text_colorscheme);
            draw_text(button_xx_center,button_yy_center,"No");
            if button_hover && action_button_pressed_get()
            {

                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
				
				 menu_open = menu_open_previous;
				 menu_char_id_deleting = "";
            }
        }
        else
        {
            draw_set_color(make_color_rgb(15,15,15));
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(c_gray);
            draw_text(button_xx_center,button_yy_center,"No");
        }

    }
	
	#macro MENULISTING_DELETE_SAVE "Delete save"
    if menu_open == MENULISTING_DELETE_SAVE
    {
        draw_set_valign(fa_center);
        draw_set_halign(fa_center);
        draw_set_font(font_01b_reg);
        draw_set_color(c_ltgray);
        draw_set_alpha(menu_open_text_image_alpha);
        draw_text((x1+x2)/2,(y1+y2)/2.5,"Delete this save permanently?\rThis action cannot be reverted!");
         draw_set_font(font_01b_reg);
		 
        var button_xx_center = (x1+x2)/3;
        var button_yy_center = (y1+y2)/3*2;
        var button_width = 300;
        var button_height = 120;
        var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);
        if button_hover
        {
            draw_set_color(c_silver);
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(menu_selected_text_colorscheme);
            draw_text(button_xx_center,button_yy_center,"Yes");
            if button_hover && action_button_pressed_get()
            {
                
				voln_delete(menu_save_deleting);
				
				savefiles_info_setup(menu_save_deleting);

                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
				
				menu_open = menu_open_previous;
				
            }
                
        }
        else
        {
            draw_set_color(make_color_rgb(15,15,15));
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(c_gray);
            draw_text(button_xx_center,button_yy_center,"Yes");
        }
        
        var button_xx_center = (x1+x2)/3*2;
        var button_yy_center = (y1+y2)/3*2;
        var button_hover = mouse_over_area_center(button_xx_center,button_yy_center,button_width,button_height);
        if button_hover
        {
            draw_set_color(c_silver);
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(menu_selected_text_colorscheme);
            draw_text(button_xx_center,button_yy_center,"No");
            if button_hover && action_button_pressed_get()
            {

                action_button_pressed_set(false);
				voln_play_sfx(sfx_mouse_click);
				
				 menu_open = menu_open_previous;
				 menu_char_id_deleting = "";
            }
        }
        else
        {
            draw_set_color(make_color_rgb(15,15,15));
            //draw_rectangle(button_xx_center-button_width/2,button_yy_center-button_height/2,button_xx_center+button_width/2,button_yy_center+button_height/2,false);
			draw_sprite_stretched_ext(spr_button_choice,2,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_sprite_stretched_ext(spr_button_choice,3,button_xx_center-button_width/2,button_yy_center-button_height/2,button_width,button_height,draw_get_color(),draw_get_alpha());
			draw_set_color(c_gray);
            draw_text(button_xx_center,button_yy_center,"No");
        }

    }
	
	draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x1,y1,x2-x1,y2-y1,menu_bg_image_alpha,fa_right,fa_bottom);

	
	
}


// tooltip BG
draw_set_color(menu_idle_bg_colorscheme);
draw_set_alpha(menu_bg_image_alpha);
//draw_rectangle(0, menu_footer_area_y1, room_width, menu_footer_area_y2, false);
var xx = 0;
var yy = menu_footer_area_y1;
var xx_wdith = room_width;
var yy_wdith = menu_footer_area_y2-menu_footer_area_y1;
draw_sprite_in_area_fill(spr_gui_bg_seemless,xx,yy,xx_wdith,yy_wdith,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),menu_bg_image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_zero);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,xx,yy,xx_wdith,yy_wdith,c_white,menu_bg_image_alpha);
gpu_set_blendmode(bm_normal);
//draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,xx,yy,xx_wdith,yy_wdith,menu_bg_image_alpha,fa_right,fa_bottom);


// tooltip text
draw_set_color(c_dkwhite);
draw_set_alpha(menu_text_image_alpha);
draw_set_valign(fa_center);
draw_set_halign(fa_left);
draw_set_font(font_01n_reg);
//draw_set_font(font_general_30);
draw_text_ext(100, (menu_footer_area_y1 + menu_footer_area_y2) / 2,tooltip,40,room_width-200);


draw_reset();