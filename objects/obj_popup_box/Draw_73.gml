/// @description Insert description here
// You can write your code in this editor
/*if msg != "" && !surface_exists(surface)
{
	surface = surface_create(area_width,area_height);
	
	surface_set_target(surface);
	draw_clear_alpha(c_black,0);
	gpu_set_blendmode_ext(bm_src_alpha, bm_dest_alpha);
	
	draw_set_font(obj_control.font_01_reg_bo);
	
	draw_set_color(COLOR_NORMAL);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text_ext(x+area_width/2,y+area_height/2,msg,50,area_width - 300);

	gpu_set_blendmode(bm_normal);
	surface_reset_target();	
}*/



if msg != ""// && surface_exists(surface)
{
	draw_set_alpha(image_alpha);

	//////////////////////////
	//     Main Frame BG
	//////////////////////////
	draw_set_color(c_black);
	draw_set_alpha(image_alpha*0.5);
	draw_rectangle(0,0,room_width,room_height,false);
	draw_set_alpha(image_alpha);
	draw_set_color(c_white);
	draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(/*244,167,66*/81,22,22),image_alpha,fa_right,fa_bottom);
	gpu_set_blendmode_ext(bm_dest_color, bm_zero);
	draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,image_alpha);
	gpu_set_blendmode(bm_normal);

	//////////////////////////
	//     msg
	//////////////////////////
	//draw_surface_ext(surface,room_width / 2 - area_width /2,room_height / 2 - area_height /2,1,1,0,c_white,draw_get_alpha());
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_font(font_01n_reg);
	draw_text_ext(x+area_width/2,y+area_height/2,msg,50,area_width - 300);

	//////////////////////////
	//     borders
	//////////////////////////
	draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x,y,area_width,area_height,1,fa_right,fa_bottom);



	// controls
	if image_alpha >= 0.8 && (keyboard_check(vk_anykey) || mouse_check_button(mb_any))
	{
		//if surface_exists(surface) surface_free(surface);
		//instance_destroy();
		leaving = true;
	}
}

if !leaving
	image_alpha = approach(image_alpha,1,FADE_FAST_SEC/room_speed);
if leaving
{
	image_alpha = approach(image_alpha,0,FADE_FAST_SEC/room_speed);
	if image_alpha == 0 instance_destroy();
}