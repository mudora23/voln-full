/// @description Insert description here
// You can write your code in this editor
if char_id != noone
{
	image_alpha = approach(image_alpha,1,5/room_speed);
	var char = char_id_get_char(char_id);
	var class = char_get_class_name(char);
	
	// colors breeding
	if !char_init
	{
		char_set_random_name_from_ini(char);
		keyboard_string = char[? GAME_CHAR_DISPLAYNAME];
		
		var button_xx = 750;
		var button_yy = 400;
		var inst = instance_create_depth(button_xx,button_yy,depth-10,obj_color_selector);
		inst.selector_char = char;
		inst.selector_key = "GAME_CHAR_COLOR_"+string_upper(class)+"_1";
		
		button_xx += 120;
		var inst = instance_create_depth(button_xx,button_yy,depth-10,obj_color_selector);
		inst.selector_char = char;
		inst.selector_key = "GAME_CHAR_COLOR_"+string_upper(class)+"_2";

		button_xx += 120;
		var inst = instance_create_depth(button_xx,button_yy,depth-10,obj_color_selector);
		inst.selector_char = char;
		inst.selector_key = "GAME_CHAR_COLOR_"+string_upper(class)+"_3";
		
		button_xx += 120;
		var inst = instance_create_depth(button_xx,button_yy,depth-10,obj_color_selector);
		inst.selector_char = char;
		inst.selector_key = "GAME_CHAR_COLOR_"+string_upper(class)+"_4";
		
		
		char_init = true;
	}

	var col_1 = "GAME_CHAR_COLOR_"+string_upper(class)+"_1";
	var col_2 = "GAME_CHAR_COLOR_"+string_upper(class)+"_2";
	var col_3 = "GAME_CHAR_COLOR_"+string_upper(class)+"_3";
	var col_4 = "GAME_CHAR_COLOR_"+string_upper(class)+"_4";
		
	//////////////////////////
	//     Main Frame BG
	//////////////////////////
	var frame_padding_x = 100;
	var frame_padding_y = 50;
	draw_set_color(c_almostblack);
	draw_set_alpha(image_alpha*0.6);
	draw_rectangle(0,0,room_width,room_height,false);
	draw_sprite_in_area_fill(spr_gui_bg_seemless,frame_padding_x,frame_padding_y,room_width-frame_padding_x*2,room_height-frame_padding_y*2,make_color_rgb(81,22,22),image_alpha,fa_right,fa_bottom);
	gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
	draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,frame_padding_x,frame_padding_y,room_width-frame_padding_x*2,room_height-frame_padding_y*2,c_white,image_alpha);
	gpu_set_blendmode(bm_normal);
	
	//////////////////////////
	//     Character
	//////////////////////////
	var char_x = frame_padding_x;
	var char_y = frame_padding_y;
	var char_scale = (room_height - frame_padding_y*2)/ sprite_get_height(spr_char_player_offspring);
	
	draw_sprite_ext(spr_char_player_offspring,0,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_SKIN],image_alpha);
	draw_sprite_ext(spr_char_player_offspring,1,char_x,char_y,char_scale,char_scale,0,c_white,image_alpha);
	draw_sprite_ext(spr_char_player_offspring,2,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_EYE],image_alpha);
		gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
		draw_sprite_ext(spr_char_player_offspring,3,char_x,char_y,char_scale,char_scale,0,c_white,1);
		gpu_set_blendmode(bm_normal);
	draw_sprite_ext(spr_char_player_offspring,4,char_x,char_y,char_scale,char_scale,0,c_white,1);
	
	if class == "thief"
	{
		draw_sprite_ext(spr_char_player_offspring,5,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_THIEF_1],1);
		draw_sprite_ext(spr_char_player_offspring,6,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_THIEF_2],1);
		draw_sprite_ext(spr_char_player_offspring,7,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_THIEF_3],1);
		draw_sprite_ext(spr_char_player_offspring,8,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_THIEF_4],1);
		draw_sprite_ext(spr_char_player_offspring,9,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_HAIR],1);
			gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
			draw_sprite_ext(spr_char_player_offspring,10,char_x,char_y,char_scale,char_scale,0,c_white,1);
			gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_char_player_offspring,11,char_x,char_y,char_scale,char_scale,0,max(0.5,color_get_value(char[? GAME_CHAR_COLOR_HAIR])/255),1);
	}
	else if class == "warrior"
	{
		draw_sprite_ext(spr_char_player_offspring,12,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WARRIOR_1],1);
		draw_sprite_ext(spr_char_player_offspring,13,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WARRIOR_2],1);
		draw_sprite_ext(spr_char_player_offspring,14,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WARRIOR_3],1);
		draw_sprite_ext(spr_char_player_offspring,15,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WARRIOR_4],1);
		draw_sprite_ext(spr_char_player_offspring,16,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_HAIR],1);
			gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
			draw_sprite_ext(spr_char_player_offspring,17,char_x,char_y,char_scale,char_scale,0,c_white,1);
			gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_char_player_offspring,18,char_x,char_y,char_scale,char_scale,0,max(0.5,color_get_value(char[? GAME_CHAR_COLOR_HAIR])/255),1);
	}
	else
	{
		draw_sprite_ext(spr_char_player_offspring,19,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WIZARD_1],1);
		draw_sprite_ext(spr_char_player_offspring,20,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WIZARD_2],1);
		draw_sprite_ext(spr_char_player_offspring,21,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WIZARD_3],1);
		draw_sprite_ext(spr_char_player_offspring,22,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_WIZARD_4],1);
		draw_sprite_ext(spr_char_player_offspring,23,char_x,char_y,char_scale,char_scale,0,char[? GAME_CHAR_COLOR_HAIR],1);
			gpu_set_blendmode_ext(bm_zero, bm_inv_src_alpha);
			draw_sprite_ext(spr_char_player_offspring,24,char_x,char_y,char_scale,char_scale,0,c_white,1);
			gpu_set_blendmode(bm_normal);
		draw_sprite_ext(spr_char_player_offspring,25,char_x,char_y,char_scale,char_scale,0,max(0.5,color_get_value(char[? GAME_CHAR_COLOR_HAIR])/255),1);
	}
	
	
	
	
	
	
	draw_reset();

	var width = 570;
	var box_x = 700;
	var box_y = 300;
	draw_set_font(spr_font_01_reg_bo);

	if string_width(keyboard_string) < width - 15
		char[? GAME_CHAR_DISPLAYNAME] = keyboard_string;
	else
		keyboard_string = char[? GAME_CHAR_DISPLAYNAME];

	draw_set_color(c_almostblack);
	draw_rectangle(box_x-5,box_y-5,box_x+width+10,box_y+45+10,false);
	draw_set_color(c_dkdkwhite);
	draw_rectangle(box_x,box_y,box_x+width,box_y+45,false);

	draw_set_color(c_almostblack);
	draw_text(box_x+5,box_y,char[? GAME_CHAR_DISPLAYNAME]);
	draw_set_alpha(obj_control.flashing_alpha_slow);
	draw_line_width(box_x+5+string_width(char[? GAME_CHAR_DISPLAYNAME])+5,box_y+3,box_x+5+string_width(char[? GAME_CHAR_DISPLAYNAME])+5,box_y+45-6,2);

	draw_reset();
	
	
	
	
	
	
	var button_xx = 1320;
	var button_yy = 300;
	var button_spr = spr_dice;
	var button_hover = mouse_over_area(button_xx,button_yy,sprite_get_width(button_spr),sprite_get_height(button_spr));
	if button_hover var button_color = c_battle_outline;
	else var button_color = c_white;
	
	draw_sprite_ext(spr_dice,0,button_xx,button_yy,1,1,0,button_color,image_alpha);
	if button_hover && mouse_check_button_pressed(mb_left)
	{
		char_set_random_name_from_ini(char);
		keyboard_string = char[? GAME_CHAR_DISPLAYNAME];
	}
	
	
	
	var button_xx = 1320;
	var button_yy = 400;
	var button_spr = spr_dice;
	var button_hover = mouse_over_area(button_xx,button_yy,sprite_get_width(button_spr),sprite_get_height(button_spr));
	if button_hover var button_color = c_battle_outline;
	else var button_color = c_white;
	
	draw_sprite_ext(spr_dice,0,button_xx,button_yy,1,1,0,button_color,image_alpha);
	if button_hover && mouse_check_button_pressed(mb_left)
	{
		char_set_random_clothes_colors_from_ini(char,"GAME_CHAR_COLOR_"+string_upper(class)+"_1","GAME_CHAR_COLOR_"+string_upper(class)+"_2","GAME_CHAR_COLOR_"+string_upper(class)+"_3","GAME_CHAR_COLOR_"+string_upper(class)+"_4",class);
	}
	
	
	//////////////////////////
	//     Title
	//////////////////////////
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_font(font_01b_reg);
	
	if char[? GAME_CHAR_SUPERIOR]
		draw_text_outlined(1100,150,"Your SUPERIOR offspring has joined the force!!!",c_silver,c_almostblack,0.8,3,8);
	else
		draw_text_outlined(1100,150,"Your offspring has joined the force!",c_silver,c_almostblack,0.8,3,8);
	
	
	//////////////////////////
	//     Done button
	//////////////////////////
	var done_xx = 1450;
	var done_yy = 290;
	var done_w = 300;
	var done_h = 200;
	var done_hover = mouse_over_area(done_xx,done_yy,done_w,done_h);
	
	draw_sprite_ext(spr_button_choice,4,done_xx,done_yy,done_w/sprite_get_width(spr_button_choice),done_h/sprite_get_height(spr_button_choice),0,c_white,image_alpha*(0.3+done_hover*0.4));
	draw_sprite_ext(spr_button_choice,5,done_xx,done_yy,done_w/sprite_get_width(spr_button_choice),done_h/sprite_get_height(spr_button_choice),0,c_white,image_alpha*(0.3+done_hover*0.4));
	
	draw_text_outlined(done_xx+done_w/2,done_yy+done_h/2,"Done",c_silver,c_almostblack,1,3,8);
	
	if done_hover && mouse_check_button_pressed(mb_left)
	{
		voln_play_sfx(sfx_mouse_click_choice); 
		scene_jump_next();
	}
	
	//////////////////////////
	//     polygon
	//////////////////////////
	draw_stats_polygon(1100,750,200,char_get_END(char),char_get_FOU(char),char_get_STR(char),char_get_DEX(char),char_get_WIS(char),char_get_INT(char),char_get_SPI(char),false,false);
	
	
	//////////////////////////
	//     borders
	//////////////////////////
	draw_borders_in_area(spr_gui_border_top_left_c,spr_gui_border_top_right_c,spr_gui_border_bot_left_c,spr_gui_border_bot_right_c,spr_gui_border_top_seemless_c,spr_gui_border_bot_seemless_c,spr_gui_border_left_seemless_c,spr_gui_border_right_seemless_c,frame_padding_x,frame_padding_y,room_width-frame_padding_x*2,room_height-frame_padding_y*2,1,fa_right,fa_bottom);

	
}


if current_script_posi != var_map_get(VAR_SCRIPT_POSI) 
{
	with obj_color_selector instance_destroy();
	instance_destroy();
}