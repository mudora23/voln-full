{
    "id": "eeec2d2c-ce37-4847-b05b-304c3e3b6e68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mini_map",
    "eventList": [
        {
            "id": "3b350cfc-939f-469c-8ed2-c24e340a5659",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eeec2d2c-ce37-4847-b05b-304c3e3b6e68"
        },
        {
            "id": "945c0025-d4f9-4ca2-82c6-e79cf6ba691b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eeec2d2c-ce37-4847-b05b-304c3e3b6e68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d0e0aa5-b845-4554-b8ad-fb740d3357ea",
    "visible": true
}