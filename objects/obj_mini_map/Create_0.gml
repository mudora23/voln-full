/// @description Insert description here
// You can write your code in this editor
depth = DEPTH_MINIMAP;
area_width = sprite_width;
area_height = sprite_height;

map_zoom_ratio_default = 1/3;

map_zoom_ratio = map_zoom_ratio_default;
map_zoom_ratio_target = map_zoom_ratio_default;
map_xoffset = 0;
map_yoffset = 0;
map_xoffset_target = 0;
map_yoffset_target = 0;
map_holding = false;
map_alpha = 1;

player_icon_image_index_dir = 0; // dir
player_icon_image_index_dir_frame = 3; // frame per dir
player_icon_image_index = 0; // which frame in a dir
player_icon_image_index_speed = 0; // timer
player_icon_image_index_speed_max = 0.2; // timer max

with obj_map_player
{
	var location_name = obj_control.var_map[? VAR_LOCATION];
	var location_map = obj_control.location_map[? location_name];
	var location_x = location_map[? "x"];
	var location_y = location_map[? "y"];
	x = location_x;
	y = location_y;
}

map_xoffset = obj_map_player.x*map_zoom_ratio - area_width/2;
map_yoffset = obj_map_player.y*map_zoom_ratio - area_height/2;
map_xoffset_target = map_xoffset;
map_yoffset_target = map_yoffset;
map_animated_frame = 0;

image_alpha = 0;
