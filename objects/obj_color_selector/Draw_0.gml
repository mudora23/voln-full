/// @description Insert description here
// You can write your code in this editor

if !open
{

	draw_set_color(selector_char[? selector_key]);
	draw_rectangle(x,y,x+sprite_width,y+sprite_height,false);
	if mouse_check_button_pressed(mb_left) && mouse_over_area(x,y,sprite_width,sprite_height) && !instance_exists(obj_examine)
	{
		with obj_color_selector if id != other.id open = false;
		open = !open;
		if open
		{
			just_open = true;
			color_hue = color_get_hue(selector_char[? selector_key]);
			color_saturation = color_get_saturation(selector_char[? selector_key]);
			color_value = color_get_value(selector_char[? selector_key]);
		}
	}
	//draw_set_color(c_silver);
	//draw_text_transformed(x,y+sprite_height+2,selector_name,0.8,0.8,0);


}








/*

draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

var text = selector_key;
var text_xx = x;
var text_yy = y;
if selector_color == "R" draw_text(text_xx,text_yy,text+"("+string(color_get_red(selector_char[? selector_key]))+")");
if selector_color == "G" draw_text(text_xx,text_yy,text+"("+string(color_get_green(selector_char[? selector_key]))+")");
if selector_color == "B" draw_text(text_xx,text_yy,text+"("+string(color_get_blue(selector_char[? selector_key]))+")");

var bar_xx = x + 100;
var bar_yy = y;
var bar_width = 200;
var bar_height = 15;
if selector_color == "R" var button_ratio = color_get_red(selector_char[? selector_key]) / 255;
if selector_color == "G" var button_ratio = color_get_green(selector_char[? selector_key]) / 255;
if selector_color == "B" var button_ratio = color_get_blue(selector_char[? selector_key]) / 255;
if selector_color == "R" var button_xx = bar_xx + bar_width * color_get_red(selector_char[? selector_key]) / 255;
if selector_color == "G" var button_xx = bar_xx + bar_width * color_get_green(selector_char[? selector_key]) / 255;
if selector_color == "B" var button_xx = bar_xx + bar_width * color_get_blue(selector_char[? selector_key]) / 255;
var button_yy = bar_yy + bar_height/2;
var button_size = 5;
var button_over = point_distance(button_xx,button_yy,mouse_x,mouse_y) <= button_size;
if button_over && mouse_check_button_pressed(mb_left) && !instance_exists(obj_examine) button_holding = true;
if !mouse_check_button(mb_left) button_holding = false;

if button_holding || button_over
{
	color1 = c_white;
	color2 = c_silver;
}
else
{
	color2 = c_white;
	color1 = c_silver;
}

if selector_color == "R" draw_set_color(c_red);
else if selector_color == "G" draw_set_color(c_green);
else if selector_color == "B" draw_set_color(c_blue);
else draw_set_color(c_white);
draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,c_black,draw_get_color(),draw_get_color(),c_black,false);
draw_set_color(color1);
draw_circle(button_xx,button_yy,button_size,false);

if button_holding
{
	if selector_color == "R"
		selector_char[? selector_key] = make_color_rgb(round(clamp((mouse_x - bar_xx)/bar_width,0,1)*255),color_get_green(selector_char[? selector_key]),color_get_blue(selector_char[? selector_key]));
	if selector_color == "G"
		selector_char[? selector_key] = make_color_rgb(color_get_red(selector_char[? selector_key]),round(clamp((mouse_x - bar_xx)/bar_width,0,1)*255),color_get_blue(selector_char[? selector_key]));
	if selector_color == "B"
		selector_char[? selector_key] = make_color_rgb(color_get_red(selector_char[? selector_key]),color_get_green(selector_char[? selector_key]),round(clamp((mouse_x - bar_xx)/bar_width,0,1)*255));
}
draw_reset();
*/
draw_reset();