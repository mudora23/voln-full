/// @description Insert description here
// You can write your code in this editor
if open
{
	//draw_set_color(c_almostblack);
	//draw_set_alpha(0.7);
	//draw_rectangle(0,0,room_width,250,false);
	//draw_reset();
	
	draw_set_color(selector_char[? selector_key]);
	draw_rectangle(x,y,x+sprite_width,y+sprite_height,false);
	if !just_open && mouse_check_button_pressed(mb_left)
	{
		if mouse_over_area(x,y,sprite_width,sprite_height)
		{
			//with obj_color_selector if id != other.id open = false;
			open = false;
		}
	}

	//draw_set_color(c_silver);
	//draw_text_transformed(x,y+sprite_height+2,selector_name,0.8,0.8,0);
	
	var x1 = x;
	var x2 = x1 + 100;
	var x3 = x2 + 255;
	var y1 = y+sprite_height+10;
	var y2 = y1 + 255;
	var y3 = y2 + 30;
	var padding = 10;
	var current_color = selector_char[? selector_key];
	var color_hue_ratio = color_hue / 255;
	var color_saturation_ratio = color_saturation / 255;
	var color_value_ratio = color_value / 255;
	
	// close
	if !just_open
	{
		if (mouse_check_button_pressed(mb_left) && !mouse_over_area(x1-10,y1-10,x3-x1+20,y3-y1+20)) ||
		   mouse_check_button_pressed(mb_right)
		{
			with obj_color_selector if id != other.id open = false;
			open = false;
		}
	}

	// selected color area
	draw_set_color(current_color);
	draw_rectangle(x1,y1,x2-1,y3-1,false);
	//draw_set_color(make_color_hsv(color_hue,(color_saturation + 127) % 255,(color_value + 127) % 255));
	//draw_text(x1,y1,"R: "+string(color_get_red(current_color))+"\nG: "+string(color_get_green(current_color))+"\nB: "+string(color_get_blue(current_color))+"\n\nH: "+string(color_get_hue(current_color))+"\nS: "+string(color_get_saturation(current_color))+"\nV: "+string(color_get_value(current_color))+"\n\nCode:\n"+string(current_color));
	
	// selecting color area
	var area_x = x2;
	var area_y = y1;
	var area_w = x3 - x2;
	var area_h = y2 - y1;
	draw_set_color(make_color_hsv(color_hue,255,255));
	draw_rectangle(area_x,area_y,area_x+area_w-1,area_y+area_h-1,false);
	draw_sprite_stretched(spr_color_picker_bg,0,area_x,area_y,area_w,area_h);
	
	var area_button_x = area_x + area_w * color_saturation_ratio;
	var area_button_y = area_y + area_h * (1-color_value_ratio);
	var area_radius = 8;
	
	draw_set_color(c_silver);
	draw_circle(area_button_x,area_button_y,area_radius+3,false);
	draw_set_color(current_color); 
	draw_circle(area_button_x,area_button_y,area_radius,false);
	
	if mouse_over_area(area_x,area_y,area_w,area_h) && mouse_check_button_pressed(mb_left) clicked_on = "area";
	if clicked_on == "area"
	{
		if mouse_check_button(mb_left)
		{
			var mouse_saturation_ratio = (clamp(mouse_x,area_x,area_x+area_w) - area_x)/ area_w;
			var mouse_saturation = mouse_saturation_ratio * 255;
			var mouse_value_ratio = 1-((clamp(mouse_y,area_y,area_y+area_h) - area_y)/ area_h);
			var mouse_value = mouse_value_ratio * 255;
			
			color_saturation = mouse_saturation;
			color_value = mouse_value;
			selector_char[? selector_key] = make_color_hsv(color_hue,mouse_saturation,mouse_value);
		
		
		}
	}
	
	
	// selecting hue area
	draw_set_color(c_almostblack);
	draw_rectangle(x2,y2,x3,y3,false);
	var hue_x = x2+padding;
	var hue_y = y2+padding;
	var hue_w = x3-x2-padding*2;
	var hue_h = y3-y2-padding*2;
	
	draw_sprite_stretched(spr_hue,0,hue_x,hue_y,hue_w,hue_h);

	var hue_button_x = hue_x + hue_w * color_hue_ratio;
	var hue_button_y = hue_y + hue_h/2;
	var hue_radius = 8;
	
	draw_set_color(c_silver);
	draw_circle(hue_button_x,hue_button_y,hue_radius+3,false);
	draw_set_color(make_color_hsv(color_hue,255,255)); 
	draw_circle(hue_button_x,hue_button_y,hue_radius,false);


	if mouse_over_area(x2,y2,x3-x2,y3-y2) && mouse_check_button_pressed(mb_left) clicked_on = "hue";
	if clicked_on == "hue"
	{
		if mouse_check_button(mb_left)
		{
			var mouse_hue_ratio = (clamp(mouse_x,hue_x,hue_x+hue_w) - hue_x)/ hue_w;
			var mouse_hue = mouse_hue_ratio * 255;
			color_hue = mouse_hue;
			selector_char[? selector_key] = make_color_hsv(mouse_hue,color_saturation,color_value);
		}
	}
	
}

if mouse_check_button_released(mb_left) clicked_on = "";

just_open = false;