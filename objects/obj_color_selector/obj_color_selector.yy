{
    "id": "356fa53b-90ba-4214-a96a-5b9f46eaa60f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_color_selector",
    "eventList": [
        {
            "id": "7cfc38f1-9403-4fff-a106-b0390f70ed3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "356fa53b-90ba-4214-a96a-5b9f46eaa60f"
        },
        {
            "id": "f3df31d4-d660-4de5-a365-791cecc88f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "356fa53b-90ba-4214-a96a-5b9f46eaa60f"
        },
        {
            "id": "f712aeb7-20e3-4c7b-b69f-b963619f796a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "356fa53b-90ba-4214-a96a-5b9f46eaa60f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec2b22e9-ad53-40df-bf1f-89eadba465c2",
    "visible": true
}