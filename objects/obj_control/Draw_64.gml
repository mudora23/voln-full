/// @description Insert description here
// You can write your code in this editor

if instance_exists(obj_title_screen) && !menu_exists() application_surface_draw_enable(false);
else application_surface_draw_enable(true);



draw_reset();

postfx_step();

/*if keyboard_check_pressed(vk_f1)
{
	//show_debug_message("Width size ratio: "+string(window_get_width()/room_width)+"; Height size ratio: "+string(window_get_height()/room_height));
	//show_debug_message("view_xport[0]: "+string(view_xport[0])+"; view_yport[0]: "+string(view_yport[0]));
	//show_debug_message("view_get_xport(0): "+string(view_get_xport(0))+"; view_get_yport(0): "+string(view_get_yport(0)));
	//show_debug_message("window_get_x(): "+string(window_get_x())+"; window_get_y(): "+string(window_get_y()));
}*/



// position checking
//draw_rectangle_outline_width(0,0,room_width*window_get_size_ratio(),room_height*window_get_size_ratio(),c_yellow,10,1);







// debug
//if mouse_check_button_pressed(mb_right)
//	postfx_punch_in(room_width/2,100,0.03);




var var_resolution_x = /*window_get_width()*/room_width; 
var var_resolution_y = /*window_get_height()*/room_height;
var var_resolution_ratio = min(var_resolution_x/room_width,var_resolution_y/room_height);
draw_sprite(spr_mouse_cursor,-1,mouse_x*var_resolution_ratio,mouse_y*var_resolution_ratio);
if mouse_on_examine_sprite != noone
{
	draw_sprite(spr_mouse_cursor_examine,1,mouse_x*var_resolution_ratio,mouse_y*var_resolution_ratio);
	draw_sprite(spr_mouse_cursor_examine,0,mouse_x*var_resolution_ratio,mouse_y*var_resolution_ratio);
	if cancel_button_pressed_get() && cancel_button_mouse_pressed_get()
	{
		if !instance_exists(obj_examine)
		{
			cancel_button_pressed_set(false);
			instance_create_depth(0,0,DEPTH_DEBUG,obj_examine);
		}
		
	}
}
	
	