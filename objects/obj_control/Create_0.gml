/// @description Insert description here
// You can write your code in this editor

///////////////////////////
///////////////////////////
///////////////////////////
debug_enable = true;
version = "0.91DEBUG";
patreon_only = false;
story_encrypted = false;
///////////////////////////
patreon_only_string = "P";
non_patreon_string = "";
///////////////////////////
///////////////////////////
///////////////////////////

debug_enable_controller = debug_enable;


randomize();

buff_debuff_icon_sizeS = 32;
buff_debuff_icon_sizeL = 64;

#macro FONT_MAP_STRING " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ÄÅÏÐÛÜÞäåïðûüþĄąĖėĦħĹĺŊŋŔŕŖŗŞşŽžƏəṠṡ–—‘’‚‛“”„‟…′″‴"

font_01_reg = font_add_sprite_ext(spr_font_01_reg,FONT_MAP_STRING,true,-11);
font_01_reg_bo = font_add_sprite_ext(spr_font_01_reg_bo,FONT_MAP_STRING,true,-11);
font_01_reg_bo_it = font_add_sprite_ext(spr_font_01_reg_bo_it,FONT_MAP_STRING,true,-11);
font_01_reg_it = font_add_sprite_ext(spr_font_01_reg_it,FONT_MAP_STRING,true,-11);
font_01_sc = font_add_sprite_ext(spr_font_01_sc,FONT_MAP_STRING,true,-11);
font_01_sc_bo = font_add_sprite_ext(spr_font_01_sc_bo,FONT_MAP_STRING,true,-11);
font_01_sc_bo_it = font_add_sprite_ext(spr_font_01_sc_bo_it,FONT_MAP_STRING,true,-11);
font_01_sc_it = font_add_sprite_ext(spr_font_01_sc_it,FONT_MAP_STRING,true,-11);

music_current_id = noone;


room_speed = 60;
game_set_speed(60, gamespeed_fps);

debug = -1;

colorR = 81;
colorG = 22;
colorB = 22;

colorR_main = 81;
colorG_main = 22;
colorB_main = 22;

colorR_side = 81;
colorG_side = 22;
colorB_side = 22;

char_wiki_map_init();
skill_wiki_map_init();
item_wiki_map_init();
scene_list_type_wiki_map_init();
short_term_buff_debuff_wiki_map_init();

var_map_init();
var_map_add_missing_keys();

target_location = LOCATION_7;
location_map_init();
location_map_update_path_and_neighbor_list();
location_map_update_visited_and_visible_info();

postfx_ps_init();

room_goto(room_title_screen);

mouse_on_examine_sprite = noone;


depth = DEPTH_DEBUG;


main_gui_image_alpha = 0;
bg_gui_image_alpha = 0;

current_enemies_choice = noone;
current_keyboard_choice = noone;


allies_choice_enable = false;

hovered_char_on_screen = noone;
hovered_char_on_screen_delay = 0;

window_set_cursor(cr_none);



settings_map_init();
settings_map_apply();

obj_control.info_list_of_map = ds_list_create();
ds_list_add(obj_control.info_list_of_map,noone,noone,noone,noone,noone,noone,noone);
savefiles_info_setup();

obj_control.pausescreen = noone;
obj_control.surf1 = noone;
obj_control.thumbnails_width = 192;
obj_control.thumbnails_height = 108;

obj_control.flashing_alpha = 0;
obj_control.flashing_alpha_increasing = true;
obj_control.flashing_alpha_slow = 0;
obj_control.flashing_alpha_slow_increasing = true;

/*
var i,j,k;
var order_list = ds_list_create();
ds_list_add(order_list,5,4,9,1,5,6,8,10);
var ascend = false;
for (i = 1; i < ds_list_size(order_list); ++i)
{
    k = order_list[| i];
    j = i - 1;
    while j >= 0 && ((!ascend && k > order_list[| j]) || (ascend && k < order_list[| j]))
    {
		ds_list_swap(order_list,j+1,j);
        --j;
    }
}
show_message(ds_list_phrase_as_string(order_list));
*/

if debug_enable
{
	
	debug_pages_total = 6;
	
	debug_page_yoffset = 0;
	debug_page_yoffset_target = 0;
	
	debug_chunk_name = "";

	debug_item_name_list = ds_list_create();
	var item_name = ds_map_find_first(item_wiki_map);
	while(!is_undefined(item_name))
	{
		ds_list_add(debug_item_name_list,item_name);
		item_name = ds_map_find_next(item_wiki_map,item_name);
	}

	ds_list_sort(debug_item_name_list,true);
	
	
	debug_battle_travel_scenes_enabled = true;
	
	debug_frame_rate_real_list = ds_list_create();
	debug_frame_rate_real_refresh_rate = 60;
	debug_frame_rate_real = 0;
	
	
	debug_side_quests_list = ds_list_create();
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_CF1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_CB1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_CM1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_MB1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_MM1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_MM2");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_YS1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_YL1");
	ds_list_add(debug_side_quests_list,"TS_Berries-TS_Berries_YM1");

	
	ds_list_sort(debug_side_quests_list,true);
	
	
	
	

}










