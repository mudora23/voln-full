{
    "id": "70b3f694-53f7-4b56-a93c-75b59fac99dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_control",
    "eventList": [
        {
            "id": "bd466a8d-2c33-49f1-bead-eeae5765329c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70b3f694-53f7-4b56-a93c-75b59fac99dd"
        },
        {
            "id": "9153836d-8aaf-43b5-8b7b-a6e75b0a6476",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "70b3f694-53f7-4b56-a93c-75b59fac99dd"
        },
        {
            "id": "9503ec31-429f-4ae6-ba0e-c3814b97b569",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "70b3f694-53f7-4b56-a93c-75b59fac99dd"
        },
        {
            "id": "7e744d81-18ad-41d7-accf-bb9456533ac4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "70b3f694-53f7-4b56-a93c-75b59fac99dd"
        },
        {
            "id": "7a0a9749-545e-4ef1-9d10-6bd3a582db37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "70b3f694-53f7-4b56-a93c-75b59fac99dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7c6ebe9a-5698-460b-a13b-b60bf304d150",
    "visible": true
}