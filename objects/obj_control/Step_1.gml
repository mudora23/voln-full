/// @description Insert description here
// You can write your code in this editor

// playtime
if !instance_exists(obj_menu) var_map_add(VAR_PLAYTIME,(1/room_speed));


action_button_mouse_pressed_set(mouse_check_button_pressed(mb_left));
cancel_button_mouse_pressed_set(mouse_check_button_pressed(mb_right));
if mouse_check_button(mb_left) action_button_mouse_hold_set(action_button_mouse_hold_get()+1/room_speed);
else action_button_mouse_hold_set(0);

action_button_keyboard_pressed_set(keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter));
cancel_button_keyboard_pressed_set(keyboard_check_pressed(vk_escape));
if keyboard_check(vk_space) || keyboard_check(vk_enter) action_button_keyboard_hold_set(action_button_keyboard_hold_get()+1/room_speed);
else action_button_keyboard_hold_set(0);



if room == room_game  && !menu_exists() && (cancel_button_keyboard_pressed_get() || (cancel_button_mouse_pressed_get() && obj_control.mouse_on_examine_sprite == noone))
{
	instance_create_depth(0,0,DEPTH_MENU,obj_menu);
	voln_play_sfx(sfx_inventory_open);
	cancel_button_pressed_set(false);
}
else if cancel_button_pressed_get() && menu_exists() && instance_exists(obj_menu) && !obj_menu.inventory_using_item
{
	instance_destroy(obj_menu);
	cancel_button_pressed_set(false);
}



if obj_control.flashing_alpha_increasing
{
	obj_control.flashing_alpha = approach(obj_control.flashing_alpha,1,FADE_NORMAL_SEC/room_speed);
	if obj_control.flashing_alpha >= 1 obj_control.flashing_alpha_increasing = false;
}
else
{
	obj_control.flashing_alpha = approach(obj_control.flashing_alpha,0,FADE_NORMAL_SEC/room_speed);
	if obj_control.flashing_alpha <= 0 obj_control.flashing_alpha_increasing = true;
}

if obj_control.flashing_alpha_slow_increasing
{
	obj_control.flashing_alpha_slow = approach(obj_control.flashing_alpha_slow,1,FADE_SLOW_SEC/room_speed);
	if obj_control.flashing_alpha_slow >= 1 obj_control.flashing_alpha_slow_increasing = false;
}
else
{
	obj_control.flashing_alpha_slow = approach(obj_control.flashing_alpha_slow,0,FADE_SLOW_SEC/room_speed);
	if obj_control.flashing_alpha_slow <= 0 obj_control.flashing_alpha_slow_increasing = true;
}


mouse_on_examine_sprite = noone;

hovered_char_on_screen_delay--;
if hovered_char_on_screen_delay <= 0
{
	hovered_char_on_screen = noone;
}

if var_map_get(VAR_GUI_PRESET) == VAR_GUI_MIN || var_map_get(VAR_GUI_PRESET) == VAR_GUI_MIN_WITH_BG || room != room_game main_gui_image_alpha = approach(main_gui_image_alpha,0,FADE_NORMAL_SEC/room_speed);
else main_gui_image_alpha = approach(main_gui_image_alpha,1,FADE_NORMAL_SEC/room_speed);

if var_map_get(VAR_GUI_PRESET) == VAR_GUI_MIN || room != room_game bg_gui_image_alpha = approach(bg_gui_image_alpha,0,FADE_NORMAL_SEC/room_speed);
else bg_gui_image_alpha = approach(bg_gui_image_alpha,1,FADE_NORMAL_SEC/room_speed);
	
script_execute_step();

draw_set_floating_tooltip("",0,0,fa_left,fa_top,false);
draw_set_menu_floating_tooltip("",0,0,fa_left,fa_top,false);

if var_map_get(VAR_GUI_COLORSCHEME) == COLORSCHEME_GETITEM
{
	obj_control.colorR = 244;
	obj_control.colorG = 167;
	obj_control.colorB = 66;
}
else
{
	obj_control.colorR = 81;
	obj_control.colorG = 22;
	obj_control.colorB = 22;
}
colorR_main = smooth_approach_room_speed(colorR_main,colorR,SMOOTH_SLIDE_FAST_SEC);
colorG_main = smooth_approach_room_speed(colorG_main,colorG,SMOOTH_SLIDE_FAST_SEC);
colorB_main = smooth_approach_room_speed(colorB_main,colorB,SMOOTH_SLIDE_FAST_SEC);






///////////////////////////////////
////////////////////   Music
///////////////////////////////////

if room == room_title_screen
    var music_target_index = amb_cave;
else if !obj_control.var_map[? VAR_BG_MUSIC_AUTO]
	var music_target_index = asset_get_index(obj_control.var_map[? VAR_BG_MUSIC]);
else
{
	if var_map_get(VAR_MODE) == VAR_MODE_BATTLE/* || scene_get_mode() == GAME_MODE_INVENTORY || scene_get_mode() == GAME_MODE_INVENTORY_CENTER*/
		obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_Twilight_Moon);
	else if scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Building_Interior_01_midsun)) ||
	        scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Nirholt_MH_01)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Nirholt_HD_02))
		obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_Spiritual_Woods);
	else if scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Cave_Interior_01_midsun) )
		obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_cave);	
	else if scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Cult_RitualPit_01) )
		obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_Torture_Chamber);		
	else if scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Brook_01_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Cabin_01_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Cabin_02_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Deep_01_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Nest_01_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Ruins_01_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Forest_Temple_01_midsun)) ||
			scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Nirholt_Distant_01_midsun))
		obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_Summer_Forest);	
	else if scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Nirholt_HD_01_crowd)) ||
	        scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Nirholt_Dags_01_crowd))
		obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_bar_ambience_with_crowd);	
	else if scene_sprite_list_sprite_name_exists(sprite_get_name(spr_bg_Nirholt_Streets_01_midsun))
	    obj_control.var_map[? VAR_BG_MUSIC] = audio_get_name(amb_pedestrian_traffic_ambience_with_footsteps);
	else
		obj_control.var_map[? VAR_BG_MUSIC] = "";

	if asset_get_type(obj_control.var_map[? VAR_BG_MUSIC]) == asset_sound
		var music_target_index = asset_get_index(obj_control.var_map[? VAR_BG_MUSIC]);
	else
		var music_target_index = noone;
}
	
	

if audio_is_playing(music_current_id)
{
    if !audio_is_playing(music_target_index)
    {
        if audio_sound_get_gain(music_current_id) != 0
        {
            audio_sound_gain(music_current_id, 0, approach(audio_sound_get_gain(music_current_id)*100,0,50/room_speed));
        }
        else
            audio_stop_sound(music_current_id);
        
    }
    else
    {
        if audio_sound_get_gain(music_current_id) != (settings_map[? SETTINGS_MUSIC_LEVEL]/100)
        {
            audio_sound_gain(music_current_id, settings_map[? SETTINGS_MUSIC_LEVEL]/100, approach(abs(audio_sound_get_gain(music_current_id) - (settings_map[? SETTINGS_MUSIC_LEVEL]/100))*100,0,50/room_speed));
        }
    }
}
else
{
    if music_target_index != noone
    {
        music_current_id = audio_play_sound(music_target_index,1,true);
        audio_sound_gain(music_current_id, 0, 0);
        audio_sound_gain(music_current_id, (settings_map[? SETTINGS_MUSIC_LEVEL]/100), 500);
    }
}

