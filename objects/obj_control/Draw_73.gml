/// @description Insert description here
// You can write your code in this editor


draw_reset();
draw_floating_tooltip();

if debug_enable
{
	var text_x = 0;
	var text_y = 0;
	

	if debug == 1
	{

		if mouse_wheel_down() debug_page_yoffset_target += 200;
		if mouse_wheel_up() debug_page_yoffset_target -= 200;
	
		debug_page_yoffset = smooth_approach_room_speed(debug_page_yoffset,debug_page_yoffset_target,SMOOTH_SLIDE_FAST_SEC);

		draw_sprite(spr_dim,-1,0,0);
	
	    text_y = 100-debug_page_yoffset;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		
		draw_set_font(font_01b_reg);
	    draw_text(room_width/2, text_y, "Type a number to switch between pages.");
		text_y += 100;
		
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		draw_set_font(font_01b_reg);
		draw_text(room_width/2, text_y, "---------------------------- System Information ----------------------------");
		text_y += 50;

		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);

		text_x = 20;

		draw_text(text_x, text_y, "FPS: "+string(fps)); 
		text_y += 50;
		
		draw_text(text_x, text_y, "Real FPS: "+string(fps_real)); 
		text_y += 50;
		
		
		text_y += 50;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		draw_set_font(font_01b_reg);
		draw_text(room_width/2, text_y, "---------------------------- Game Information ----------------------------");
		text_y += 70;
		
		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);

		draw_text(text_x, text_y, "VAR_MODE: "+string(var_map_get(VAR_MODE))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_GUI_PRESET: "+string(var_map_get(VAR_GUI_PRESET))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_SCRIPT: "+script_get_name_ext(var_map_get(VAR_SCRIPT))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_LOCATION: "+string(var_map_get(VAR_LOCATION))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_TIME_YEAR: "+string(var_map_get(VAR_TIME_YEAR))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_TIME_MONTH: "+string(var_map_get(VAR_TIME_MONTH))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_TIME_DAY: "+string(var_map_get(VAR_TIME_DAY))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_TIME_HOUR: "+string(var_map_get(VAR_TIME_HOUR))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_HOURS_SINCE_LAST_SLEEP: "+string(var_map_get(VAR_HOURS_SINCE_LAST_SLEEP))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_HOURS_SINCE_LAST_REST: "+string(var_map_get(VAR_HOURS_SINCE_LAST_REST))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "VAR_HOURS_SINCE_LAST_BLESSING: "+string(var_map_get(VAR_HOURS_SINCE_LAST_BLESSING))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "ds_list_size(var_map_get(VAR_CHOICES_LIST_NAME)): "+string(ds_list_size(var_map_get(VAR_CHOICES_LIST_NAME)))); 
		text_y += 50;

		draw_text(text_x, text_y, "obj_command_panel.choice_image_alpha: "+string(obj_command_panel.choice_image_alpha)); 
		text_y += 50;

		draw_text(text_x, text_y, "VAR_SCRIPT_SHOW_CHOICES: "+string(var_map_get(VAR_SCRIPT_SHOW_CHOICES))); 
		text_y += 50;
		
		draw_text(text_x, text_y, "window_mouse_get_x: "+string(window_mouse_get_x())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "window_mouse_get_y: "+string(window_mouse_get_y())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_mouse_get_x: "+string(display_mouse_get_x())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_mouse_get_y: "+string(display_mouse_get_y())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "window_get_width: "+string(window_get_width())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "window_get_height: "+string(window_get_height())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "window_get_x: "+string(window_get_x())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "window_get_y: "+string(window_get_y())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_get_width: "+string(display_get_width())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_get_height: "+string(display_get_height())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_get_gui_width: "+string(display_get_gui_width())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_get_gui_height: "+string(display_get_gui_height())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_get_dpi_x: "+string(display_get_dpi_x())); 
		text_y += 50;
		
		draw_text(text_x, text_y, "display_get_dpi_y: "+string(display_get_dpi_y())); 
		text_y += 50;

		//////////////////////////////////////////////////////////////


	}

	if debug == 2
	{

		if mouse_wheel_down() debug_page_yoffset_target += 200;
		if mouse_wheel_up() debug_page_yoffset_target -= 200;
	
		debug_page_yoffset = smooth_approach_room_speed(debug_page_yoffset,debug_page_yoffset_target,SMOOTH_SLIDE_FAST_SEC);

		draw_sprite(spr_dim,-1,0,0);
	
	    text_y = 100-debug_page_yoffset;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		
		draw_set_font(font_01b_reg);
	    draw_text(room_width/2, text_y, "Type a number to switch between pages.");
		text_y += 100;
		
		
		draw_set_font(font_01b_reg);
		draw_text(room_width/2, text_y, "---------------------------- Teams Information ----------------------------");
		text_y += 50;

		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);

		text_x = 20;
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_y += 30;
		draw_text(text_x, text_y, "---------------- Player ----------------"); 
		text_y += 80;
		
		text_x = 20;
		
			var char = var_map_get(VAR_PLAYER);
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_set_color(c_yellow);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			
				draw_char_info_debug(char);
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			}
			text_y += 50;
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_y += 30;
		draw_text(text_x, text_y, "---------------- Allies ----------------"); 
		text_y += 80;
		
		text_x = 20;
		
		var char_list = var_map_get(VAR_ALLY_LIST);
		
		for(var i =0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_set_color(c_yellow);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			
				draw_char_info_debug(char);
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			}
			text_y += 50;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_y += 30;
		draw_text(text_x, text_y, "---------------- Pets ----------------"); 
		text_y += 80;
		
		text_x = 20;
		
		var char_list = var_map_get(VAR_PET_LIST);
		
		for(var i =0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_set_color(c_yellow);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			
				draw_char_info_debug(char);
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			}
			text_y += 50;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_y += 30;
		draw_text(text_x, text_y, "---------------- Storage ----------------"); 
		text_y += 80;
		
		text_x = 20;
		
		var char_list = var_map_get(VAR_STORAGE_LIST);
		
		for(var i =0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_set_color(c_yellow);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			
				draw_char_info_debug(char);
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			}
			text_y += 50;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_y += 30;
		draw_text(text_x, text_y, "---------------- Allies (Reserved) ----------------"); 
		text_y += 80;
		
		text_x = 20;
		
		var char_list = var_map_get(VAR_ALLY_R_LIST);
		
		for(var i =0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_set_color(c_yellow);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			
				draw_char_info_debug(char);
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			}
			text_y += 50;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_y += 30;
		draw_text(text_x, text_y, "---------------- Pets (Reserved) ----------------"); 
		text_y += 80;
		
		text_x = 20;
		
		var char_list = var_map_get(VAR_PET_R_LIST);
		
		for(var i =0; i < ds_list_size(char_list);i++)
		{
			var char = char_list[| i];
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_set_color(c_yellow);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			
				draw_char_info_debug(char);
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, char[? GAME_CHAR_DISPLAYNAME]+" ("+string(char[? GAME_CHAR_LEVEL])+")");
			}
			text_y += 50;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	}
	
	if debug == 3
	{

		if mouse_wheel_down() debug_page_yoffset_target += 200;
		if mouse_wheel_up() debug_page_yoffset_target -= 200;
	
		debug_page_yoffset = smooth_approach_room_speed(debug_page_yoffset,debug_page_yoffset_target,SMOOTH_SLIDE_FAST_SEC);

		draw_sprite(spr_dim,-1,0,0);
	
	    text_y = 100-debug_page_yoffset;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		
		draw_set_font(font_01b_reg);
	    draw_text(room_width/2, text_y, "Type a number to switch between pages.");
		text_y += 100;
		
		
		draw_set_font(font_01b_reg);
		draw_text(room_width/2, text_y, "---------------------------- All Items ----------------------------");
		text_y += 140;
		
		var str = ">>>>> ADD ALL ITEMS HERE <<<<<";
		var hover = mouse_over_area_center(room_width/2,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(room_width/2, text_y, str);}
		else { draw_set_color(c_white); draw_text(room_width/2, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left)
		{
			for(var i = 0; i < ds_list_size(debug_item_name_list);i++)
			{
				var item_name = debug_item_name_list[| i];
				inventory_add_item(item_name,1);
			}
		}
		text_y += 100;

		//////////////////////////////////////////////////////////////
		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		for(var i = 0; i < ds_list_size(debug_item_name_list);i++)
		{
			var item_name = debug_item_name_list[| i];
			var item_displayname = item_get_displayname(item_name);
		
			text_x = 20;
			text_y += 50;

			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
			}


			draw_set_color(c_white);
			
			draw_text(text_x, text_y, item_displayname);
			text_x += 600;
			
			draw_text(text_x, text_y, "Inventory: "+string(inventory_item_count(item_name)));
			text_x += 300;
			
			draw_text(text_x, text_y, "Price: "+string(item_get_price(item_name)));
			text_x += 250;
			
			var str = "ADD 1";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) inventory_add_item(item_name,1);
			text_x += 250;
			
			var str = "REMOVE 1";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) inventory_delete_item(item_name,1);
			text_x += 300;
			
			var str = "USE 1";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) CS_using(char_get_player(),item_name);
			text_x += 250;
			
			/*var hover = mouse_over_area(text_x,text_y,string_width(minus),string_height(minus));
			if hover draw_text_outlined(text_x, text_y, minus, c_yellow, c_black, 1,1,2);
			else draw_text_outlined(text_x, text_y, minus, c_white, c_black, 1,1,2);
			if hover && mouse_check_button_pressed(mb_left) var_set(event_key,false);
			text_x += 60;
			var hover = mouse_over_area(text_x,text_y,string_width(plus),string_height(plus));
			if hover draw_text_outlined(text_x, text_y, plus, c_yellow, c_black, 1,1,2);
			else draw_text_outlined(text_x, text_y, plus, c_white, c_black, 1,1,2);
			if hover && mouse_check_button_pressed(mb_left) var_set(event_key,true);
			text_x += 60;
			draw_text_outlined(text_x, text_y, "("+string(var_get_event_days_since_unlocked(event_key))+")", c_yellow, c_black, 1,1,2);
				*/

		}

		//////////////////////////////////////////////////////////////


	}
	

	
	
	if debug == 4
	{

		if mouse_wheel_down() debug_page_yoffset_target += 200;
		if mouse_wheel_up() debug_page_yoffset_target -= 200;
	
		debug_page_yoffset = smooth_approach_room_speed(debug_page_yoffset,debug_page_yoffset_target,SMOOTH_SLIDE_FAST_SEC);

		draw_sprite(spr_dim,-1,0,0);
	
	    text_y = 100-debug_page_yoffset;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		
		draw_set_font(font_01b_reg);
	    draw_text(room_width/2, text_y, "Type a number to switch between pages.");
		text_y += 100;
		
		
		draw_set_font(font_01b_reg);
		draw_text(room_width/2, text_y, "---------------------------- Commands ----------------------------");


		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_yellow);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
			draw_set_color(c_yellow);
			draw_text(text_x, text_y, "All characters LEVEL UP!!");
			
			if mouse_check_button_pressed(mb_left)
				S_levelup_all();
		}
		else
		{
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_text(text_x, text_y, "All characters LEVEL UP!!");
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_yellow);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
			draw_set_color(c_yellow);
			draw_text(text_x, text_y, "All characters FULL HEAL!!");
			
			if mouse_check_button_pressed(mb_left)
				S_heal_all();
		}
		else
		{
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_text(text_x, text_y, "All characters FULL HEAL!!");
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		text_x = 20;
		text_y += 50;

        if var_map_get(VAR_MODE) != VAR_MODE_NORMAL
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You cannot clear the script execution waiting list at the moment.");
		}
		else if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_yellow);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
			draw_set_color(c_yellow);
			draw_text_transformed(text_x, text_y, "Clear the script execution waiting list. (Condition: "+script_execute_list_get_condition()+")",0.8,0.8,0);
			
			if mouse_check_button_pressed(mb_left)
			{
				script_execute_list_clear();
				voln_play_sfx(sfx_item_use);
			}
		}
		else
		{
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_text_transformed(text_x, text_y, "Clear the script execution waiting list. (Condition: "+script_execute_list_get_condition()+")",0.8,0.8,0);
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
		var the_text = "Battles and travel scenes";
		if debug_battle_travel_scenes_enabled the_text+= " (Currently enabled)";
		else the_text+= " (Currently disabled)";
		
		draw_text(text_x, text_y, the_text);
		text_x += 1000;
			
		var str = "ENABLE";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover || debug_battle_travel_scenes_enabled { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) debug_battle_travel_scenes_enabled = true;
		text_x += 300;
		
		var str = "DISABLE";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover || !debug_battle_travel_scenes_enabled { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) debug_battle_travel_scenes_enabled = false;
		text_x += 300;
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
			
		draw_text(text_x, text_y, "Fight Elfs (#)");
		text_x += 600;
		
		if var_map_get(VAR_MODE) != VAR_MODE_NORMAL
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You cannot use debug tools to enter fights at the moment.");
			
		}
		else
		{
			var str = "1";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_elf(1);
			text_x += 150;
		
			var str = "2";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_elf(2);
			text_x += 150;
		
			var str = "3";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_elf(3);
			text_x += 150;
		
			var str = "4";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_elf(4);
			text_x += 150;
		
			var str = "5";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_elf(5);
			text_x += 150;
		
			var str = "6";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_elf(6);
			text_x += 150;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
			
		draw_text(text_x, text_y, "Fight Green Orks (#)");
		text_x += 600;
			
		if var_map_get(VAR_MODE) != VAR_MODE_NORMAL
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You cannot use debug tools to enter fights at the moment.");
			
		}
		else
		{
			
			var str = "1";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_ork(1);
			text_x += 150;
		
			var str = "2";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_ork(2);
			text_x += 150;
		
			var str = "3";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_ork(3);
			text_x += 150;
		
			var str = "4";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_ork(4);
			text_x += 150;
		
			var str = "5";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_ork(5);
			text_x += 150;
		
			var str = "6";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_ork(6);
			text_x += 150;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
			
		draw_text(text_x, text_y, "Fight Red Orks (#)");
		text_x += 600;
			
		if var_map_get(VAR_MODE) != VAR_MODE_NORMAL
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You cannot use debug tools to enter fights at the moment.");
			
		}
		else
		{
			
			var str = "1";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_red_ork(1);
			text_x += 150;
		
			var str = "2";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_red_ork(2);
			text_x += 150;
		
			var str = "3";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_red_ork(3);
			text_x += 150;
		
			var str = "4";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_red_ork(4);
			text_x += 150;
		
			var str = "5";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_red_ork(5);
			text_x += 150;
		
			var str = "6";
			var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
			if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
			else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
			if hover && mouse_check_button_pressed(mb_left) debug_battle_red_ork(6);
			text_x += 150;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
			
		draw_text(text_x, text_y, "Time passes");
		text_x += 300;
			
		var str = "(1 hour)";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) game_time_one_hour_passes();
		text_x += 300;
		
		var str = "(10 hours)";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) repeat(10) game_time_one_hour_passes();
		text_x += 300;
		
		var str = "(20 hours)";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) repeat(20) game_time_one_hour_passes();
		text_x += 300;

		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
			
		draw_text(text_x, text_y, "Playtime "+seconds_to_timestamp(var_map_get(VAR_PLAYTIME)));
		text_x += 300;
			
		var str = "(+ 5 minutes playtime)";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_PLAYTIME,60*5);
		text_x += 450;
		
		var str = "(+ 30 minutes playtime)";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_PLAYTIME,60*30);
		text_x += 450;
		
		var str = "(+ 5 hours playtime)";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_PLAYTIME,60*60*5);
		text_x += 450;

		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}


		draw_set_color(c_white);
			
		draw_text(text_x, text_y, "Gold");
		text_x += 300;
			
		var str = "ADD 100";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_GOLD,100);
		text_x += 250;
		
		var str = "ADD 1000";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_GOLD,1000);
		text_x += 250;
		
		var str = "REMOVE 100";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add_ext(VAR_GOLD,-100,0,var_map_get(VAR_GOLD));
		text_x += 300;
		
		var str = "REMOVE 1000";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add_ext(VAR_GOLD,-1000,0,var_map_get(VAR_GOLD));
		text_x += 300;
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		text_x = 20;
		text_y += 50;

		if mouse_over_area(0,text_y,room_width,49)
		{
			draw_set_alpha(0.7);
			draw_set_color(c_dkgray);
			draw_rectangle(0,text_y,room_width,text_y+49,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			draw_rectangle(0,text_y,room_width,text_y+49,true);
		}

		draw_set_color(c_white);
		draw_text(text_x, text_y, "Jar");
		text_x += 300;
			
		var str = "ADD 1";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_JAR,1);
		text_x += 250;
		
		var str = "ADD 10";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add(VAR_JAR,10);
		text_x += 250;
		
		var str = "REMOVE 1";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add_ext(VAR_GOLD,-1,0,var_map_get(VAR_JAR));
		text_x += 300;
		
		var str = "REMOVE 10";
		var hover = mouse_over_area(text_x,text_y,string_width(str),string_height(str));
		if hover { draw_set_color(c_yellow); draw_text(text_x, text_y, str);}
		else { draw_set_color(c_white); draw_text(text_x, text_y, str);}
		if hover && mouse_check_button_pressed(mb_left) var_map_add_ext(VAR_GOLD,-10,0,var_map_get(VAR_JAR));
		text_x += 300;	
			
		//////////////////////////////////////////////////////////////


	}
	
	
	if debug == 5
	{

		if mouse_wheel_down() debug_page_yoffset_target += 200;
		if mouse_wheel_up() debug_page_yoffset_target -= 200;
	
		debug_page_yoffset = smooth_approach_room_speed(debug_page_yoffset,debug_page_yoffset_target,SMOOTH_SLIDE_FAST_SEC);

		draw_sprite(spr_dim,-1,0,0);
	
	    text_y = 100-debug_page_yoffset;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		
		draw_set_font(font_01b_reg);
	    draw_text(room_width/2, text_y, "Type a number to switch between pages.");
		text_y += 100;
		
		
		draw_set_font(font_01b_reg);
		draw_text(room_width/2, text_y, "---------------------------- Main Quests ----------------------------");
		text_y += 80;
		draw_set_color(c_maroon);
		draw_text(room_width/2, text_y, "(WARNING: This is in testing.)");
		draw_set_color(c_white);
		text_y += 60;
		
		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);

		text_x = 20;
		text_y += 50;
		
		
		if var_map_get(VAR_MODE) != VAR_MODE_NORMAL
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You cannot skip quests at the moment.");
			
		}
		else if !chunk_mark_get("S_0003_68")
		{
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_text(text_x, text_y, "Skip scene 1 to 3 - Intro event (S_0001 - S_0003)");
			
				if mouse_check_button_pressed(mb_left)
				{
					ds_list_clear(var_map_get(VAR_QUEST_JOURNAL_LIST));
					scene_choices_list_clear();
					var_map_set(VAR_MODE,VAR_MODE_NORMAL);
					var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
					scene_cg_sprites_list_clear();
					with obj_player_name_input instance_destroy();
					chunk_mark_set("S_0003_68",true);
					ds_list_add_unique(var_map_get(VAR_QUEST_JOURNAL_LIST),"S_0004_QJ");
					ally_add_char(NAME_ZEYD,true,1);
					char_set_info(ally_get_char(NAME_ZEYD),GAME_CHAR_LUST,50);
					ally_storage_lock(NAME_ZEYD,true);
					ally_add_char(NAME_KUDI,true,3);
					char_set_info(ally_get_char(NAME_KUDI),GAME_CHAR_LUST,50);
					inventory_add_item(ITEM_MESO_EY_TONIC_POOR,10);
					ally_storage_lock(NAME_KUDI,true);
					location_unlock(1,2,3,4,5,6,7);
					location_unlock(43,44,45,46,37,47,48,49,50);

					scene_jump("","CS_location");
				}
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, "Skip scene 1 to 3 - Intro event (S_0001 - S_0003)");
			}
		}
		
		else if !chunk_mark_get("S_0004_FELGOR_INTRO_EVENT_FINISH")
		{
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_text(text_x, text_y, "Skip scene 4 - Welcome to Nirholt (S_0004)");
			
				if mouse_check_button_pressed(mb_left)
				{
					ds_list_clear(var_map_get(VAR_QUEST_JOURNAL_LIST));
					scene_choices_list_clear();
					var_map_set(VAR_MODE,VAR_MODE_NORMAL);
					var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
					scene_cg_sprites_list_clear();
					ally_rejoin(NAME_KUDI);
					chunk_mark_set("SL1_INTRO",true);
					chunk_mark_set("SL1_MEET_HUT_INTRO",true);
					chunk_mark_set("SL1_BRED_BEST_INTRO",true);
					chunk_mark_set("SL1_HUN_DAOA_INTRO",true);
					chunk_mark_set("SL1_DAGS_INTRO",true);
					obj_control.var_map[? VAR_LOCATION] = LOCATION_1;
					location_map_update_path_and_neighbor_list();
					location_map_update_visited_and_visible_info();
					scene_jump("S_0004_FELGOR_INTRO_EVENT_FINISH","S_0004");
				}
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, "Skip scene 4 - Welcome to Nirholt (S_0004)");
			}
		}
		
		else if !chunk_mark_get("S_0005_WON_14_C")
		{
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_text(text_x, text_y, "Skip scene 5 - Saving Zeyd (S_0005)");
			
				if mouse_check_button_pressed(mb_left)
				{
					ds_list_clear(var_map_get(VAR_QUEST_JOURNAL_LIST));
					scene_choices_list_clear();
					ally_rejoin(NAME_ZEYD);
					var_map_set(VAR_MODE,VAR_MODE_NORMAL);
					var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
					scene_cg_sprites_list_clear();
					chunk_mark_set("S_0005_WON_14_C",true);
					obj_control.var_map[? VAR_LOCATION] = LOCATION_1;
					location_map_update_path_and_neighbor_list();
					location_map_update_visited_and_visible_info();
					scene_jump("S_0005_WON_14_C","S_0005");
				}
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, "Skip scene 5 - Saving Zeyd (S_0005)");
			}
		}
		
		else if !chunk_mark_get("S_0006_2")
		{
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_text(text_x, text_y, "Skip scene 6 - Zeyd Healing (S_0006)");
			
				if mouse_check_button_pressed(mb_left)
				{
					ds_list_clear(var_map_get(VAR_QUEST_JOURNAL_LIST));
					scene_choices_list_clear();
					var_map_set(VAR_MODE,VAR_MODE_NORMAL);
					var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
					chunk_mark_set("S_0006_2",true);
					chunk_mark_set("S_0006_35",true);
					ally_storage_lock(NAME_KUDI,false);
					ally_storage_lock(NAME_ZEYD,false);
					ds_list_add_unique(var_map_get(VAR_QUEST_JOURNAL_LIST),"S_0007_QJ");
					scene_cg_sprites_list_clear();
					obj_control.var_map[? VAR_LOCATION] = LOCATION_1;
					location_map_update_path_and_neighbor_list();
					location_map_update_visited_and_visible_info();
					scene_jump("MENU","SL1");
				}
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, "Skip scene 6 - Zeyd Healing (S_0006)");
			}
		}
		else if !chunk_mark_get("S_0007_SL1_Bandit_Beatdown_Finish")
		{
			if mouse_over_area(0,text_y,room_width,49)
			{
				draw_set_alpha(0.7);
				draw_set_color(c_dkgray);
				draw_rectangle(0,text_y,room_width,text_y+49,false);
				draw_set_alpha(1);
				draw_set_color(c_yellow);
				draw_rectangle(0,text_y,room_width,text_y+49,true);
				draw_text(text_x, text_y, "Skip scene 7 - Bandit Beatdown (S_0007)");
			
				if mouse_check_button_pressed(mb_left)
				{
					ally_add_char(NAME_FELGOR,true,char_get_info(obj_control.var_map[? VAR_PLAYER],GAME_CHAR_LEVEL)+1);
					ally_add_char(NAME_LORN,true,char_get_info(obj_control.var_map[? VAR_PLAYER],GAME_CHAR_LEVEL)+1);
					ds_list_clear(var_map_get(VAR_QUEST_JOURNAL_LIST));
					scene_choices_list_clear();
					var_map_set(VAR_MODE,VAR_MODE_NORMAL);
					var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
					chunk_mark_set("S_0007",true);
					chunk_mark_set("S_0007_2",true);
					chunk_mark_set("S_0007_SL1_Bandit_Beatdown_Finish",true);
					chunk_mark_set("S_0007_SL1_Bandit_Beatdown_Finish_8",true);
					chunk_mark_set("SL42_S_0007_Won",true);
					location_unlock(LOCATION_42);
					scene_cg_sprites_list_clear();
					obj_control.var_map[? VAR_LOCATION] = LOCATION_1;
					location_map_update_path_and_neighbor_list();
					location_map_update_visited_and_visible_info();
					scene_jump("MENU","SL1");
				}
			}
			else
			{
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_text(text_x, text_y, "Skip scene 7 - Bandit Beatdown (S_0007)");
			}
		}
		// S_0008
		// chunk_mark_set("S_0008_MAKEOVER_1",true);
		// S_0008_FELGORKUDI_JOIN_1
		// _execute(0,location_unlock,39,40,41,8,9,11,12,13,14,18,35);
		else
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You have done all the quests that can be skipped.");	
		}








		text_x = 20;
		text_y += 120;
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
		draw_set_font(font_01b_reg);
		draw_set_color(c_white);
		draw_text(room_width/2, text_y, "---------------------------- Side Quests ----------------------------");
		text_y += 80;
		draw_set_color(c_maroon);
		draw_text(room_width/2, text_y, "(WARNING: This is in testing.)");
		draw_set_color(c_white);
		text_y += 60;
		
		draw_set_font(font_01n_reg);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);



		if var_map_get(VAR_MODE) != VAR_MODE_NORMAL
		{
			draw_set_alpha(1);
			draw_set_color(c_silver);
			draw_text(text_x, text_y, "You cannot enter side quests at the moment.");
			
		}
		else
		{
			// for every side quest
			for(var i = 0; i < ds_list_size(debug_side_quests_list);i++){
				var quest_script_label = debug_side_quests_list[| i];
				var quest_script = "";
				var quest_label = "";
				var j = 1;
			
				while j <= string_length(quest_script_label)
				{
					if string_char_at(quest_script_label,j) != "-"
					{
						quest_script+=string_char_at(quest_script_label,j);
						j++;
					}
					else
					{
						j++;
						break;
					}
				}
			
				while j <= string_length(quest_script_label)
				{
					quest_label+=string_char_at(quest_script_label,j);
					j++;
				}
			
			
				var quest_string = quest_script + " - " +quest_label;
				if mouse_over_area(0,text_y,room_width,50)
				{
					draw_set_alpha(0.7);
					draw_set_color(c_dkgray);
					draw_rectangle(0,text_y,room_width,text_y+49,false);
					draw_set_alpha(1);
					draw_set_color(c_yellow);
					draw_rectangle(0,text_y,room_width,text_y+49,true);
					draw_text(text_x, text_y, quest_string);
			
					if mouse_check_button_pressed(mb_left)
					{
						scene_choices_list_clear();
						var_map_set(VAR_EXTRA_AFTER_SCENE_LABEL,"start");
						var_map_set(VAR_EXTRA_AFTER_SCENE,"CS_location");
						var_map_set(VAR_GUI_PRESET,VAR_GUI_NORMAL);
						scene_jump(quest_script_label,quest_script);
					}
				}
				else
				{
					draw_set_alpha(1);
					draw_set_color(c_white);
					draw_text(text_x, text_y, quest_string);
				}
			
				text_x = 20;
				text_y += 50;

			}

		}











	}

	if debug == 6
	{
		
		
		
		
		
	}
		
	if debug > 0
	{

		
		//////////////////////////////////////////////////////////////

		draw_set_color(c_dkgray);
		draw_set_alpha(0.8);
		draw_rectangle(0,0,room_width,110,false);
		
		draw_set_font(font_01b_reg);
		var name = "DEBUG PAGE "+string(debug)+" of "+string(debug_pages_total);
		draw_text_outlined(room_width-20-string_width(name), 20, name, c_white, c_black, 1,1,2);
	
	
	
		draw_reset();
		
	}

	for(var i = 1; i <= debug_pages_total; i++)
	{
		if keyboard_check_pressed(ord(string(i))) && room == room_game
		{
			with obj_map_view instance_destroy();
			
			if debug == i debug = -1;
			else {debug = i; if debug == 6 instance_create_depth(0,0,DEPTH_DEBUG+1,obj_map_view);}
			debug_page_yoffset = -100;
			debug_page_yoffset_target = -100;
		}
	}

	if keyboard_check_pressed(vk_escape)
	{
		debug = -1;
		with obj_map_view instance_destroy();
	}
	

	
	


	/*if keyboard_check_pressed(ord("F"))
	{
		draw_floating_text_debug("F pressed.");
		var ally_list = var_map_get(VAR_ALLY_LIST);
		for(var k = 0; k < ds_list_size(ally_list);k++)
		{
			var ally = ally_list[| k];
			draw_floating_text(ally[? GAME_CHAR_X_FLOATING_TEXT],ally[? GAME_CHAR_Y_FLOATING_TEXT],"FLOATING TEXT TESTING",c_white);
		}
		var player = char_get_player();
		draw_floating_text(player[? GAME_CHAR_X_FLOATING_TEXT],player[? GAME_CHAR_Y_FLOATING_TEXT],"FLOATING TEXT TESTING",c_white);
	}
	if keyboard_check_pressed(ord("A"))
	{
		draw_floating_text_debug("A pressed.");
		location_map_update_path_and_neighbor_list();
		location_map_update_visited_and_visible_info();
	}

	if keyboard_check_pressed(ord("H"))
	{
		draw_floating_text_debug("H pressed.");
		S_heal_all();
	}

	if keyboard_check_pressed(ord("U"))
	{
		draw_floating_text_debug("U pressed.");
		S_levelup_all();
	}

	if keyboard_check_pressed(ord("C"))
	{
		draw_floating_text_debug("C pressed.");
		scene_choices_list_clear();
	}
	if keyboard_check_pressed(ord("M"))
	{
		draw_floating_text_debug("M pressed.");
		show_popup_box("M pressed. Popup box testing testing testing testing testing testing testing testing testing testing");
	}




	if keyboard_check_pressed(ord("S"))
	{
		scene_choices_list_clear();
		scene_choice_add("Buy \""+string(item_get_displayname(ITEM_BEEF_JERKED))+"\"","CS_buying","CS_buying","",true,true,true,"",scene_store_clear);
		if item_get_price(ITEM_BEEF_JERKED) * 10 <= var_map_get(VAR_GOLD)
			scene_choice_add("Buy \""+string(item_get_displayname(ITEM_BEEF_JERKED))+"\" x10","CS_buying10","CS_buying","",true,true,true,"",scene_store_clear);
		else
			scene_choice_add("[?]Buy \""+string(item_get_displayname(ITEM_BEEF_JERKED))+"\" x10","CS_buying10","CS_buying","",true,true,true,"",scene_store_clear);
		scene_choice_add("Exit","CS_exit","CS_buying","",true,true,true,"",scene_store_clear);
	}

	if keyboard_check_pressed(ord("I"))
	{
		//inventory_add_item(ITEM_MESO_EY_TONIC_POOR,10);
		show_message(irandom_range(0,0));
	}
*/

	if keyboard_check_pressed(ord("B"))
		postfx_burn(mouse_x,mouse_y,choose(1,2,3));
		
	// DEBUG
	if obj_control.debug_enable
	{

		draw_reset();
		ds_list_add(obj_control.debug_frame_rate_real_list,fps_real);
		if ds_list_size(obj_control.debug_frame_rate_real_list) >= obj_control.debug_frame_rate_real_refresh_rate
		{
			obj_control.debug_frame_rate_real = ds_list_get_sum_of_numbers(obj_control.debug_frame_rate_real_list)/ds_list_size(obj_control.debug_frame_rate_real_list);
			ds_list_clear(obj_control.debug_frame_rate_real_list);
		}
		draw_set_font(font_01s_reg);
		draw_set_alpha(0.8);
		draw_text_outlined(10,10,"FPS: "+string(fps),c_dkwhite,c_almostblack,1,3,8);
		draw_text_outlined(10,30,"FPS (real): "+string(round(obj_control.debug_frame_rate_real)),c_dkwhite,c_almostblack,1,3,8);
		draw_reset();
	}

}

if debug_enable_controller && keyboard_check_pressed(vk_tab)
{
	debug_enable = !debug_enable;
}
	
mouse_xprevious = mouse_x;
mouse_yprevious = mouse_y;
	
	