{
    "id": "e744f3c0-9a69-4bd7-9a3d-8cc33d780d03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_view",
    "eventList": [
        {
            "id": "c27e3f3a-3b55-4a1c-912f-4d02e7e987eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e744f3c0-9a69-4bd7-9a3d-8cc33d780d03"
        },
        {
            "id": "c423d86a-d496-493a-9cc8-e7c8592e7185",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e744f3c0-9a69-4bd7-9a3d-8cc33d780d03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}