/// @description Insert description here
// You can write your code in this editor



draw_map_in_area(map,map_xsize,map_ysize,map_area_x1,map_area_y1,map_area_x2-map_area_x1,map_area_y2-map_area_y1,round(map_xoffset),round(map_yoffset),1,map_zoom_ratio,map_zoom_ratio);





var path_num = 1;
while path_exists(asset_get_index("path_"+string(path_num)))
{
	var location_path = asset_get_index("path_"+string(path_num));
	var location_path_x1 = path_get_x( location_path, 0);
	var location_path_y1 = path_get_y( location_path, 0);
	var location_path_x2 = path_get_x( location_path, 1);
	var location_path_y2 = path_get_y( location_path, 1);
	
	if location_map_on_hover != noone && location_map_on_hover[? "x"] == location_path_x1 && location_map_on_hover[? "y"] == location_path_y1
	{
		var path_color = c_yellow;
		var path_alpha = 1;
	}
	else if location_map_on_hover != noone && location_map_on_hover[? "x"] == location_path_x2 && location_map_on_hover[? "y"] == location_path_y2
	{
		var path_color = c_yellow;
		var path_alpha = 1;
	}
	else
	{
		var path_color = c_white;
		var path_alpha = 0.5;
	}	
	
	var path_length = path_get_length(location_path);
	var path_point_dis = 40;
	for(var jj = 0;jj<1;jj+=path_point_dis/path_length)
	{
		draw_sprite_in_visible_area(spr_map_path_point,0,map_area_x1-round(map_xoffset)+path_get_x( location_path, jj )*map_zoom_ratio-sprite_get_width(spr_map_path_point)/2,map_area_y1-round(map_yoffset)+path_get_y( location_path, jj )*map_zoom_ratio-sprite_get_height(spr_map_path_point)/2,1,1,path_color,path_alpha,map_area_x1,map_area_y1,map_area_x2-map_area_x1,map_area_y2-map_area_y1);
	}
	
	if path_number_drawn && 
	   point_in_rectangle(map_area_x1+path_get_x( location_path, 0.5)*map_zoom_ratio-round(map_xoffset),map_area_y1+path_get_y( location_path, 0.5)*map_zoom_ratio-round(map_yoffset),map_area_x1+40,map_area_y1+40,map_area_x2-40,map_area_y2-40)
		draw_tooltip(map_area_x1+path_get_x( location_path, 0.5)*map_zoom_ratio-round(map_xoffset),map_area_y1+path_get_y( location_path, 0.5)*map_zoom_ratio-round(map_yoffset),"("+string(path_num)+")",fa_center,fa_center,false);
	path_num++;
}





location_map_on_hover = noone;
var the_map = obj_control.location_map;
var the_location = ds_map_find_first(the_map);
while !is_undefined(the_location)
{
	var the_location_map = the_map[? the_location];
	var location_x = the_location_map[? "x"];
	var location_y = the_location_map[? "y"];
	var location_marker = the_location_map[? "marker"];
	var location_name = the_location_map[? "name"];
	var location_region = the_location_map[? "region"];
	var location_bg = the_location_map[? "bg"];
	
	var location_realx = location_x*map_zoom_ratio-round(map_xoffset)-sprite_get_width(location_marker)/2+map_area_x1;
	var location_realy = location_y*map_zoom_ratio-round(map_yoffset)-sprite_get_height(location_marker)/2+map_area_y1;
	
	var location_hover = mouse_over_area(location_realx,location_realy,sprite_get_width(location_marker),sprite_get_height(location_marker));
	
	if location_hover location_map_on_hover = the_location_map;
	

	if location_hover draw_sprite_in_visible_area(location_marker,1,location_realx,location_realy,1,1,c_white,1,map_area_x1,map_area_y1,map_area_x2-map_area_x1,map_area_y2-map_area_y1);
	draw_sprite_in_visible_area(location_marker,0,location_realx,location_realy,1,1,c_white,1,map_area_x1,map_area_y1,map_area_x2-map_area_x1,map_area_y2-map_area_y1);
	
	if location_is_locked(the_location)
		draw_sprite_in_visible_area(spr_lock,1,location_realx+sprite_get_width(location_marker)/2-sprite_get_width(spr_lock)/4,location_realy+sprite_get_height(location_marker)/2-sprite_get_height(spr_lock)/4,0.5,0.5,c_white,0.5,map_area_x1,map_area_y1,map_area_x2-map_area_x1,map_area_y2-map_area_y1);

	
	if location_get_current() == the_location
	{
		draw_set_halign(fa_center);
		draw_set_valign(fa_bottom);
		draw_set_color(c_red);
		draw_set_font(font_01s_reg);
		draw_circle(location_realx+sprite_get_width(location_marker)/2,location_realy+sprite_get_height(location_marker)/2,18,true);
		draw_circle(location_realx+sprite_get_width(location_marker)/2,location_realy+sprite_get_height(location_marker)/2,19,true);
		draw_circle(location_realx+sprite_get_width(location_marker)/2,location_realy+sprite_get_height(location_marker)/2,20,true);
		draw_circle(location_realx+sprite_get_width(location_marker)/2,location_realy+sprite_get_height(location_marker)/2,21,true);
		draw_circle(location_realx+sprite_get_width(location_marker)/2,location_realy+sprite_get_height(location_marker)/2,22,true);
		draw_text_transformed_color(location_realx+sprite_get_width(location_marker)/2,location_realy+sprite_get_height(location_marker)/2-20,"YOU ARE HERE",1,1,0,c_orange,c_orange,c_lime,c_lime,1);
	}

	
	if location_hover &&
	   point_in_rectangle(map_area_x1+location_x*map_zoom_ratio-round(map_xoffset),map_area_y1+location_y*map_zoom_ratio-round(map_yoffset)-20,map_area_x1+80,map_area_y1+40,map_area_x2-80,map_area_y2-40)
		draw_tooltip(map_area_x1+location_x*map_zoom_ratio-round(map_xoffset),map_area_y1+location_y*map_zoom_ratio-round(map_yoffset)-20,string_copy(the_location,10,string_length(the_location)-9)+" - "+location_name+" (X: "+string(location_x)+" Y: "+string(location_y)+")",fa_center,fa_bottom,false);
	else if location_name_drawn &&
	   point_in_rectangle(map_area_x1+location_x*map_zoom_ratio-round(map_xoffset),map_area_y1+location_y*map_zoom_ratio-round(map_yoffset)-20,map_area_x1+80,map_area_y1+40,map_area_x2-80,map_area_y2-40)
		draw_tooltip(map_area_x1+location_x*map_zoom_ratio-round(map_xoffset),map_area_y1+location_y*map_zoom_ratio-round(map_yoffset)-20,string_copy(the_location,10,string_length(the_location)-9)+" - "+location_name,fa_center,fa_bottom,false);
	

	the_location = ds_map_find_next(the_map,the_location);
}



draw_reset();
draw_set_font(font_01s_reg);
var button_width = map_area_x1 - 1;
var button_height = 100;

var button_x = 0;
var button_y = map_area_y1+1;
var button_string = "Display location name";
var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
if button_hover || location_name_drawn
{
	draw_set_color(c_dkwhite);
	draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
	draw_set_color(c_almostblack);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(button_x+button_width/2,button_y+button_height/2,button_string);
}
else
{
	draw_set_color(c_almostblack);
	draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
	draw_set_color(c_dkwhite);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(button_x+button_width/2,button_y+button_height/2,button_string);	
}

if button_hover && mouse_check_button_pressed(mb_left)
	location_name_drawn = !location_name_drawn;


button_y += button_height + 1;
var button_string = "Display path #";
var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
if button_hover || path_number_drawn
{
	draw_set_color(c_dkwhite);
	draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
	draw_set_color(c_almostblack);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(button_x+button_width/2,button_y+button_height/2,button_string);
}
else
{
	draw_set_color(c_almostblack);
	draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
	draw_set_color(c_dkwhite);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(button_x+button_width/2,button_y+button_height/2,button_string);	
}

if button_hover && mouse_check_button_pressed(mb_left)
	path_number_drawn = !path_number_drawn;






button_y += button_height + 1;
draw_set_color(c_black);
draw_rectangle(button_x,button_y,button_x+button_width,room_height - button_height,false);





button_y = room_height - button_height;
var button_string = "BACK";
var button_hover = mouse_over_area(button_x,button_y,button_width,button_height);
if button_hover
{
	draw_set_color(c_dkwhite);
	draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
	draw_set_color(c_almostblack);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(button_x+button_width/2,button_y+button_height/2,button_string);
}
else
{
	draw_set_color(c_almostblack);
	draw_rectangle(button_x,button_y,button_x+button_width,button_y+button_height,false);
	draw_set_color(c_dkwhite);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(button_x+button_width/2,button_y+button_height/2,button_string);	
}

if button_hover && mouse_check_button_pressed(mb_left)
{
	obj_control.debug = -1;
	instance_destroy();
}



if mouse_over_area(map_area_x1,map_area_y1,map_area_x2-map_area_x1,map_area_y2-map_area_y1)
{

	var x_axis = mouse_x + map_xoffset - map_area_x1;
	var y_axis = mouse_y + map_yoffset - map_area_y1;

	var button_string = "X:"+string(round(x_axis/map_zoom_ratio))+" Y:"+string(round(y_axis/map_zoom_ratio));
	var button_x = room_width - string_width(button_string);
	var button_y = room_height - string_height(button_string);
	draw_set_alpha(0.5);
	draw_set_color(c_almostblack);
	draw_rectangle(button_x,button_y,room_width,room_height,false);
	draw_set_alpha(1);
	draw_set_color(c_dkwhite);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_text(button_x,button_y,button_string);	

}




draw_set_color(c_black);
draw_rectangle(0,0,room_width,map_area_y1,false);



if mouse_check_button_pressed(mb_left) map_holding = true;
if !mouse_check_button(mb_left) map_holding = false;

if map_holding
{
	map_xoffset += mouse_xprevious - mouse_x;
	map_yoffset += mouse_yprevious - mouse_y;
	
	map_xoffset = clamp(map_xoffset,0,map_total_width*map_zoom_ratio-(map_area_x2-map_area_x1));
	map_yoffset = clamp(map_yoffset,0,map_total_height*map_zoom_ratio-(map_area_y2-map_area_y1));
}

mouse_xprevious = mouse_x;
mouse_yprevious = mouse_y;
draw_reset();