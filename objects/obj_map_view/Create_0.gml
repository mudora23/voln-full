/// @description Insert description here
// You can write your code in this editor

// map variables
map = spr_map_bg;
map_xsize = 10;
map_ysize = 10;
map_width = sprite_get_width(map);
map_height = sprite_get_height(map);
map_total_width = map_width * map_xsize;
map_total_height = map_height * map_ysize;
		
map_zoom_ratio_default = 0.5;
map_zoom_ratio = map_zoom_ratio_default;

map_xoffset = 0;
map_yoffset = 0;

map_holding = false;
map_alpha = 1;
map_animated_frame = 0;

mouse_xprevious = mouse_x;
mouse_yprevious = mouse_y;

location_map_on_hover = noone;

location_name_drawn = false;
path_number_drawn = false;

map_area_x1 = 200;
map_area_y1 = 111;
map_area_x2 = room_width;
map_area_y2 = room_height;

depth = DEPTH_DEBUG + 1;