/// @description Insert description here
// You can write your code in this editor
if char != noone
{

	draw_set_alpha(1);

	var width = 155;
	var height = 35;
	var row_num = 3 + (char[? GAME_CHAR_NAME] != NAME_ZEYD) + (char == char_get_player());
	
	if x1 == noone x1 = x2 - width;
	if x2 == noone x2 = x1 + width;
	if y1 == noone y1 = y2 - height*row_num;
	if y2 == noone y2 = y1 + height*row_num;
	
	draw_set_color(c_almostblack);
	draw_rectangle(x1,y1,x2,y2,false);
	draw_set_color(c_dkgray);
	draw_rectangle(x1,y1,x2,y2,true);
	
	var text_padding = 5;
	draw_set_font(font_01s_reg);
	
	var text_xx = x1;
	var text_yy = y1 - height;
	
	
	
	if char == char_get_player()
	{
		text_yy += height;
		var text = AI_NONE;
		var text_col = c_white;
		if mouse_over_area(text_xx,text_yy,width-1,height-1)
		{
			draw_set_color(text_col);
			draw_rectangle(text_xx+1,text_yy+1,text_xx+width-2,text_yy+height-2,false);
			draw_set_color(c_almostblack);
			draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
			if mouse_check_button_pressed(mb_left)
			{
				voln_play_sfx(sfx_mouse_click);
				char[? GAME_CHAR_AI] = text;
			}
		}
		else
		{
			draw_set_color(text_col);
			draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
		}
	}
	
	
	
	text_yy += height;
	var text = AI_AGGRESSIVE;
	var text_col = COLOR_DAMAGE;
	if mouse_over_area(text_xx,text_yy,width-1,height-1)
	{
		draw_set_color(text_col);
		draw_rectangle(text_xx+1,text_yy+1,text_xx+width-2,text_yy+height-2,false);
		draw_set_color(c_almostblack);
		draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
		if mouse_check_button_pressed(mb_left)
		{
			voln_play_sfx(sfx_mouse_click);
			char[? GAME_CHAR_AI] = text;
		}
	}
	else
	{
		draw_set_color(text_col);
		draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
	}
	
	
	if char[? GAME_CHAR_NAME] != NAME_ZEYD
	{
		text_yy += height;
		var text = AI_SEDUCTIVE;
		var text_col = COLOR_LUST;
		if mouse_over_area(text_xx,text_yy,width-1,height-1)
		{
			draw_set_color(text_col);
			draw_rectangle(text_xx+1,text_yy+1,text_xx+width-2,text_yy+height-2,false);
			draw_set_color(c_almostblack);
			draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
			if mouse_check_button_pressed(mb_left)
			{
				voln_play_sfx(sfx_mouse_click);
				char[? GAME_CHAR_AI] = text;
			}
		}
		else
		{
			draw_set_color(text_col);
			draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
		}
	}
	
	
	text_yy += height;
	var text = AI_SUPPORTIVE;
	var text_col = COLOR_HEAL;
	if mouse_over_area(text_xx,text_yy,width-1,height-1)
	{
		draw_set_color(text_col);
		draw_rectangle(text_xx+1,text_yy+1,text_xx+width-2,text_yy+height-2,false);
		draw_set_color(c_almostblack);
		draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
		if mouse_check_button_pressed(mb_left)
		{
			voln_play_sfx(sfx_mouse_click);
			char[? GAME_CHAR_AI] = text;
		}
	}
	else
	{
		draw_set_color(text_col);
		draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
	}
	
	
	text_yy += height;
	var text = AI_CONSERVATIVE;
	var text_col = COLOR_EFFECT;
	if mouse_over_area(text_xx,text_yy,width-1,height-1)
	{
		draw_set_color(text_col);
		draw_rectangle(text_xx+1,text_yy+1,text_xx+width-2,text_yy+height-2,false);
		draw_set_color(c_almostblack);
		draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
		if mouse_check_button_pressed(mb_left)
		{
			voln_play_sfx(sfx_mouse_click);
			char[? GAME_CHAR_AI] = text;
		}
	}
	else
	{
		draw_set_color(text_col);
		draw_text(text_xx+text_padding,text_yy+text_padding,"AI: "+text);
	}
	
	
	
	
	
	
	if drawn
	{
		if mouse_check_button_pressed(mb_any) instance_destroy();
		if keyboard_check_pressed(vk_anykey) instance_destroy();
	}
	
	drawn = true;
	
	
}