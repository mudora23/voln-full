{
    "id": "d75ba5d2-aa43-4f45-af08-d7366d47cf07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_player",
    "eventList": [
        {
            "id": "5e8739be-5018-4b4b-b159-68ba3ecb7e19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "d75ba5d2-aa43-4f45-af08-d7366d47cf07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fa0d5210-ecdc-4dd1-916c-3f40eca8d217",
    "visible": false
}