{
    "id": "18a49564-f2b9-4cec-aaef-78b2c7e65718",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ally",
    "eventList": [
        {
            "id": "4d67c8e1-bc1d-4c5d-b0cf-1944a6d3820d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "18a49564-f2b9-4cec-aaef-78b2c7e65718"
        },
        {
            "id": "439dac70-0ed0-49f9-bd71-e0ea4b8640ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18a49564-f2b9-4cec-aaef-78b2c7e65718"
        },
        {
            "id": "134eb3fb-7232-47eb-9533-d9b0653dc0a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "18a49564-f2b9-4cec-aaef-78b2c7e65718"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e4ac833f-228e-4495-bfc7-6186882f0ffb",
    "visible": true
}