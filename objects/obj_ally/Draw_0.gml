/// @description Insert description here
// You can write your code in this editor




/*if scene_get_GUI() == GAME_GUI_MIN image_alpha = approach(image_alpha,0,FADE_NORMAL_SEC*(1/room_speed));
else image_alpha = approach(image_alpha,1,FADE_NORMAL_SEC*(1/room_speed));
draw_set_alpha(image_alpha);*/

image_alpha = obj_control.main_gui_image_alpha;

alter_image_alpha = approach(alter_image_alpha,0,FADE_NORMAL_SEC*(1/room_speed));


//draw_self();
if image_alpha > 0
{
draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,image_alpha);
gpu_set_blendmode(bm_normal);
}


if created
{


	if ally_index >= 0 && ds_list_size(obj_control.var_map[? VAR_ALLY_LIST]) > ally_index
	{
	
		var ally_list = obj_control.var_map[? VAR_ALLY_LIST];
		var char = ally_list[| ally_index];
		
		
		if mouse_over_area(x,y,area_width,area_height) && !menu_exists()
		{
			obj_control.hovered_char_on_screen_delay = 2;
			obj_control.hovered_char_on_screen = char;
		}
		

		
		char[? GAME_CHAR_X_FLOATING_TEXT] = x+area_width/2;
		char[? GAME_CHAR_Y_FLOATING_TEXT] = y+area_height/2;	
		


		
		
		draw_reset();
		
		
	    var color_r_target = color_get_red(char[? GAME_CHAR_COLOR_TARGET]);
	    var color_g_target = color_get_green(char[? GAME_CHAR_COLOR_TARGET]);
	    var color_b_target = color_get_blue(char[? GAME_CHAR_COLOR_TARGET]);
		if char[? GAME_CHAR_HEALTH] > 0 && char[? GAME_CHAR_LUST] < char_get_lust_max(char)
			char[? GAME_CHAR_COLOR_TARGET] = make_color_rgb(255,255,255);
		else
			char[? GAME_CHAR_COLOR_TARGET] = c_lightblack;
		
	    var color_r = color_get_red(char[? GAME_CHAR_COLOR]);
	    var color_g = color_get_green(char[? GAME_CHAR_COLOR]);
	    var color_b = color_get_blue(char[? GAME_CHAR_COLOR]);
	    var color_r_target = color_get_red(char[? GAME_CHAR_COLOR_TARGET]);
	    var color_g_target = color_get_green(char[? GAME_CHAR_COLOR_TARGET]);
	    var color_b_target = color_get_blue(char[? GAME_CHAR_COLOR_TARGET]);
	    color_r = smooth_approach_room_speed(color_r,color_r_target,SMOOTH_SLIDE_SLOW_SEC);
	    color_g = smooth_approach_room_speed(color_g,color_g_target,SMOOTH_SLIDE_SLOW_SEC);
	    color_b = smooth_approach_room_speed(color_b,color_b_target,SMOOTH_SLIDE_SLOW_SEC);
	    var current_color = make_color_rgb(color_r,color_g,color_b);
		char[? GAME_CHAR_COLOR] = current_color;
		
		char[? GAME_CHAR_SHAKE_AMOUNT] = smooth_approach_room_speed(char[? GAME_CHAR_SHAKE_AMOUNT], 0, SMOOTH_SLIDE_NORMAL_SEC);
		
		draw_set_alpha(image_alpha);
		var xx = x+random_range(-char[? GAME_CHAR_SHAKE_AMOUNT],char[? GAME_CHAR_SHAKE_AMOUNT]);;
		var yy = y+random_range(-char[? GAME_CHAR_SHAKE_AMOUNT],char[? GAME_CHAR_SHAKE_AMOUNT]);;
		var spr = asset_get_index(char[? GAME_CHAR_THUMB]);
		if spr < 0 spr = spr_white;
		var width = area_width;
		var bar_height = 12;
		var height = area_height-bar_height*4;
		if char[? GAME_CHAR_HEALTH] > 0 && char[? GAME_CHAR_HEALTH] <= char_get_health_max(char)*0.4
		{
			draw_set_alpha(image_alpha*obj_control.flashing_alpha*0.5);
			draw_rectangle_color(xx,yy,xx+width,yy+height,COLOR_HP,COLOR_HP,COLOR_HP,COLOR_HP,false);
			draw_set_alpha(image_alpha);
		}
		if char[? GAME_CHAR_LUST] >= char_get_lust_max(char)*0.6 && char[? GAME_CHAR_HEALTH] < char_get_lust_max(char)
		{
			draw_set_alpha(image_alpha*(1-obj_control.flashing_alpha)*0.5);
			draw_rectangle_color(xx,yy,xx+width,yy+height,COLOR_LUST,COLOR_LUST,COLOR_LUST,COLOR_LUST,false);
			draw_set_alpha(image_alpha);
		}
		
		if var_map_get(VAR_MODE) == VAR_MODE_BATTLE
			if obj_control.hovered_char_on_screen == char || battle_get_current_action_char() == char
			{
				draw_set_color(COLOR_ALLY_NAME);
				draw_set_alpha(0.1*obj_control.flashing_alpha);
				draw_rectangle(xx,yy,x+width,y+height,false);
				draw_reset();		
			}
		
		draw_thumb_in_area(spr,xx,yy,width,height,char[? GAME_CHAR_COLOR],char);
		if sprite_get_number(spr) > 1 && spr != spr_char_player_offspring_thumb
			draw_sprite_in_area_ext(spr,1,xx,yy,width,height,char[? GAME_CHAR_COLOR],alter_image_alpha);
			

			
		draw_set_alpha(image_alpha);
		
		
		// AI mode
		draw_set_font(font_01s_sc);
		var ai_mode_text = "AI: "+char[? GAME_CHAR_AI];
		var ai_mode_ratio = 1;
		var ai_mode_xx = x + 10;
		var ai_mode_yy = y+area_height-bar_height*4-string_height(ai_mode_text)*ai_mode_ratio;

		
		var ai_mode_hover = mouse_over_area(ai_mode_xx,ai_mode_yy,string_width(ai_mode_text)*ai_mode_ratio,string_height(ai_mode_text)*ai_mode_ratio) && !menu_exists() && !main_gui_clicking_disabled();
		
		if ai_mode_hover var outline_col = c_dkgray;
		else var outline_col = c_almostblack;
		
		if char[? GAME_CHAR_AI] == AI_AGGRESSIVE var ai_col = COLOR_DAMAGE;
		else if char[? GAME_CHAR_AI] == AI_SEDUCTIVE var ai_col = COLOR_LUST;
		else if char[? GAME_CHAR_AI] == AI_SUPPORTIVE var ai_col = COLOR_HEAL;
		else if char[? GAME_CHAR_AI] == AI_CONSERVATIVE var ai_col = COLOR_EFFECT;
		else if char[? GAME_CHAR_AI] == AI_NONE var ai_col = c_white;
		else var ai_col = c_white;
		
		draw_text_outlined(ai_mode_xx,ai_mode_yy,ai_mode_text,ai_col,outline_col,ai_mode_ratio,3,15);
		
		if ai_mode_hover && action_button_pressed_get() && !menu_exists()
		{
			action_button_pressed_set(false);
			/*if char[? GAME_CHAR_AI] == AI_AGGRESSIVE
				char[? GAME_CHAR_AI] = AI_SEDUCTIVE;
			else if char[? GAME_CHAR_AI] == AI_SEDUCTIVE
				char[? GAME_CHAR_AI] = AI_SUPPORTIVE;
			else if char[? GAME_CHAR_AI] == AI_SUPPORTIVE
				char[? GAME_CHAR_AI] = AI_CONSERVATIVE;
			else if char[? GAME_CHAR_AI] == AI_CONSERVATIVE
				char[? GAME_CHAR_AI] = AI_NONE;
			else if char[? GAME_CHAR_AI] == AI_NONE
				char[? GAME_CHAR_AI] = AI_AGGRESSIVE;
			else
				char[? GAME_CHAR_AI] = AI_NONE;*/
			with obj_ai_selector instance_destroy();
			voln_play_sfx(sfx_mouse_click);
			instance_create_depth(0,0,DEPTH_MENU+1,obj_ai_selector);
			obj_ai_selector.x1 = ai_mode_xx;
			obj_ai_selector.y2 = ai_mode_yy-10;
			obj_ai_selector.char = char;
		}
		draw_reset();
		draw_set_alpha(image_alpha);

		
		// draw buff debuff icons
		var buff_list = char[? GAME_CHAR_BUFF_LIST];
		var buff_turn_list = char[? GAME_CHAR_BUFF_TURN_LIST];
		var debuff_list = char[? GAME_CHAR_DEBUFF_LIST];
		var debuff_turn_list = char[? GAME_CHAR_DEBUFF_TURN_LIST];
		var buff_debuff_icon_width = obj_control.buff_debuff_icon_sizeS;
		var buff_debuff_icon_height = obj_control.buff_debuff_icon_sizeS;
		var buff_debuff_icon_xx = x+area_width-buff_debuff_icon_width-15;
		var buff_debuff_icon_yy = y;
		
		for(var i = 0;i<ds_list_size(debuff_list);i++)
		{			
			var debuff = debuff_list[| i];
			var debuff_turn = debuff_list[| i];
			var debuff_icon = short_term_buff_debuff_get_icon(debuff);
			draw_sprite_in_area(debuff_icon,buff_debuff_icon_xx+sprite_get_xoffset(debuff_icon),buff_debuff_icon_yy+sprite_get_yoffset(debuff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
			if debuff_turn > 1 draw_text_outlined(xx,yy,debuff_turn,c_silver,c_almostblack,1,2,8);
			if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(debuff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
			buff_debuff_icon_xx-=buff_debuff_icon_width;
		}
		
		for(var i = 0;i<ds_list_size(buff_list);i++)
		{			
			var buff = buff_list[| i];
			var buff_turn = buff_list[| i];
			var buff_icon = short_term_buff_debuff_get_icon(buff);
			draw_sprite_in_area(buff_icon,buff_debuff_icon_xx+sprite_get_xoffset(buff_icon),buff_debuff_icon_yy+sprite_get_yoffset(buff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
			if buff_turn > 1 draw_text_outlined(xx,yy,buff_turn,c_silver,c_almostblack,1,2,8);
			if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(buff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
			buff_debuff_icon_xx-=buff_debuff_icon_width;
		}
		
		
		draw_reset();
		
		
		
		
		
		
		
		// bars
		var ratio_per_sec = 3;
		char_health = char[? GAME_CHAR_HEALTH];
		char_healthmax = char_get_health_max(char);
		char_stamina = char[? GAME_CHAR_STAMINA];
		char_staminamax = char_get_stamina_max(char);
		char_lust = char[? GAME_CHAR_LUST];
		char_lustmax = char_get_lust_max(char);
		char_toru = char[? GAME_CHAR_TORU];
		char_torumax = char_get_toru_max(char);
		
		char[? GAME_CHAR_HEALTH_VISUAL] = smooth_approach_room_speed(char[? GAME_CHAR_HEALTH_VISUAL],char_health,ratio_per_sec);
		char_healthmax_visual = char_get_health_max(char);//char_healthmax_visual = smooth_approach(char_healthmax_visual,char_healthmax,ratio_per_sec);
		char_stamina_visual = smooth_approach_room_speed(char_stamina_visual,char_stamina,ratio_per_sec);
		char_staminamax_visual = char_get_stamina_max(char);//char_staminamax_visual = smooth_approach(char_staminamax_visual,char_staminamax,ratio_per_sec);
		char[? GAME_CHAR_LUST_VISUAL] = smooth_approach_room_speed(char[? GAME_CHAR_LUST_VISUAL],char_lust,ratio_per_sec);
		char_lustmax_visual = char_get_lust_max(char);//char_lustmax_visual = smooth_approach(char_lustmax_visual,char_lustmax,ratio_per_sec);
		char_toru_visual = smooth_approach_room_speed(char_toru_visual,char_toru,ratio_per_sec);
		char_torumax_visual = char_get_toru_max(char);//char_torumax_visual = smooth_approach(char_torumax_visual,char_torumax,ratio_per_sec);











		// hp
		var bar_xx=x+0;
		var bar_width = area_width;
		var bar_yy=y+area_height-bar_height*4;
		var bar_text_size_ratio = 1;
		var bar_text_outline_width = 2;
		var bar_text_outline_fid = 10;
		
		
		var bar_color = COLOR_HP;
		var bar_color_dark = COLOR_HP_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_healthmax_visual;
		var bar_value = char_health;
		var bar_value_visual = char[? GAME_CHAR_HEALTH_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar1_text = "Health: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
      
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height-1,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);

		// lust
		bar_yy+=bar_height;
		var bar_color = COLOR_LUST;
		var bar_color_dark = COLOR_LUST_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_lustmax_visual;
		var bar_value = char_lust;
		var bar_value_visual = char[? GAME_CHAR_LUST_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar2_text = "Lust: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
      
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height-1,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);
		
		
		// stamina
		bar_yy+=bar_height;
		var bar_color = COLOR_STAMINA;
		var bar_color_dark = COLOR_STAMINA_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_staminamax_visual;
		var bar_value = char_stamina;
		var bar_value_visual = char_stamina_visual;
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar3_text = "Stamina: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
      
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height-1,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);
		
		
		// toru
		bar_yy+=bar_height;
		var bar_color = COLOR_TORU;
		var bar_color_dark = COLOR_TORU_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_torumax_visual;
		var bar_value = char_toru;
		var bar_value_visual = char_toru_visual;
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar4_text = "Toru: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height-1,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
      
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height-1,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);




		// this ally's turn (highlight)
		
			//draw_rectangle_outline_width(x,y,x+sprite_width,y+sprite_height,c_yellow,10,image_alpha);
	
	//if battle_get_current_action_char() == char && battle_char_get_buff_level(char,BUFF_HIDE) <= 0
	//	draw_rectangle_outline_width(x,y,x+sprite_width,y+sprite_height,c_battle_outline,5,obj_control.flashing_alpha);

			// tooltip
			var bars_left=x;
			var bars_top=y+area_height-bar_height*4;
			var bars_width = bar_width;
			var bars_height = bar_height*4;
			if mouse_over_area(bars_left,bars_top,bars_width,bars_height) && !menu_exists()
			{
				draw_set_font(font_01s_sc);
				draw_set_valign(fa_bottom);
				draw_set_halign(fa_left);
				var tooltip_xx = bars_left;
				var tooltip_yy = bars_top-1;
				var tooltip_width = bar_width;
				
				var tooltip_text = bar4_text;
				var tooltip_height = string_height(tooltip_text);
				draw_set_color(c_lightblack);
				draw_set_alpha(0.9*image_alpha);
				draw_rectangle(tooltip_xx,tooltip_yy-tooltip_height,tooltip_xx+tooltip_width,tooltip_yy,false);
				draw_set_color(COLOR_TORU);
				draw_set_alpha(image_alpha);
				draw_text(tooltip_xx,tooltip_yy,tooltip_text);
				tooltip_yy-=tooltip_height+1;
				
				var tooltip_text = bar3_text;
				var tooltip_height = string_height(tooltip_text);
				draw_set_color(c_lightblack);
				draw_set_alpha(0.9*image_alpha);
				draw_rectangle(tooltip_xx,tooltip_yy-tooltip_height,tooltip_xx+tooltip_width,tooltip_yy,false);
				draw_set_color(COLOR_STAMINA);
				draw_set_alpha(image_alpha);
				draw_text(tooltip_xx,tooltip_yy,tooltip_text);
				tooltip_yy-=tooltip_height+1;
				
				var tooltip_text = bar2_text;
				var tooltip_height = string_height(tooltip_text);
				draw_set_color(c_lightblack);
				draw_set_alpha(0.9*image_alpha);
				draw_rectangle(tooltip_xx,tooltip_yy-tooltip_height,tooltip_xx+tooltip_width,tooltip_yy,false);
				draw_set_color(COLOR_LUST);
				draw_set_alpha(image_alpha);
				draw_text(tooltip_xx,tooltip_yy,tooltip_text);
				tooltip_yy-=tooltip_height+1;
				
				var tooltip_text = bar1_text;
				var tooltip_height = string_height(tooltip_text);
				draw_set_color(c_lightblack);
				draw_set_alpha(0.9*image_alpha);
				draw_rectangle(tooltip_xx,tooltip_yy-tooltip_height,tooltip_xx+tooltip_width,tooltip_yy,false);
				draw_set_color(COLOR_HP);
				draw_set_alpha(image_alpha);
				draw_text(tooltip_xx,tooltip_yy,tooltip_text);
				tooltip_yy-=tooltip_height+1;

			}
		
		
		/*
		// Info stats
		var spr = spr_gui_info_icon;
		var spr_scale = 0.6;
		var spr_width = sprite_get_width(spr)*spr_scale;
		var spr_height = sprite_get_height(spr)*spr_scale;
		var padding = 5;
		var xx = x+padding;
		var yy = y+padding;
		var hover = mouse_over_area(xx,yy,spr_width,spr_height);
		
		draw_sprite_ext(spr,-1,xx,yy,spr_scale,spr_scale,0,c_white,image_alpha);
		if hover
			draw_tooltip_basic_stats(x+sprite_width+padding,y+padding,char);
		
		
		// Fighting stats
		var spr = spr_gui_fight_icon;
		var spr_scale = 0.6;
		var spr_width = sprite_get_width(spr)*spr_scale;
		var spr_height = sprite_get_height(spr)*spr_scale;
		var padding = 5;
		var xx = x+sprite_width-spr_width-padding;
		var yy = y + padding;
		var hover = mouse_over_area(xx,yy,spr_width,spr_height);
		
		draw_sprite_ext(spr,-1,xx,yy,spr_scale,spr_scale,0,c_white,image_alpha);
		if hover
			draw_tooltip_fighting_stats(x+sprite_width+padding,y+padding,char);
			
			
		// name
		var text_color = COLOR_NORMAL;
		var outline_width = 4;
		var outline_fidelity = 40;
		var outline_color = c_black;
		draw_reset();
		draw_set_halign(fa_center);
		draw_set_color(c_black);
		draw_set_alpha(image_alpha);
		//draw_text_transformed(x+sprite_width/2,y + padding*2,char[? GAME_CHAR_DISPLAYNAME],0.6,0.6,0);
		//draw_text_outlined(x+sprite_width/2,y + padding*2,string(char[? GAME_CHAR_DISPLAYNAME]),text_color,outline_color,0.6,outline_width,outline_fidelity);
		
		
		*/
		
		
		
		
		
		
		
		
		
		
		
		
		/*if obj_control.allies_choice_enable && !menu_exists() && ((action_button_pressed_get() && mouse_over_area(x,y,area_width,area_height)) || obj_control.hovered_char_on_screen == char)
		{
			//var choice_scene_list = obj_control.var_map[? GAME_SCENE_CHOICE_LIST_SCENE_LIST];
			//var choice_scene_arg0_list = obj_control.var_map[? GAME_SCENE_CHOICE_LIST_SCENE_ARG0_LIST];
			//var choice_scene_arg1_list = obj_control.var_map[? GAME_SCENE_CHOICE_LIST_SCENE_ARG1_LIST];
			//var choice_scene_arg2_list = obj_control.var_map[? GAME_SCENE_CHOICE_LIST_SCENE_ARG2_LIST];
			//var choice_scene_arg3_list = obj_control.var_map[? GAME_SCENE_CHOICE_LIST_SCENE_ARG3_LIST];
			//var choice_scene_arg4_list = obj_control.var_map[? GAME_SCENE_CHOICE_LIST_SCENE_ARG4_LIST];
			var choice_script_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT];
			var choice_script_arg0_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG0];
			var choice_script_arg1_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG1];
			var choice_script_arg2_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG2];
			var choice_script_arg3_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG3];
			var choice_script_arg4_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG4];
			var choice_script_arg5_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG5];
			var choice_script_arg6_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG6];
			
			for(var j = 0; j < ds_list_size(choice_script_list); j++)
			{

				if !is_string(choice_script_arg0_list[| j]) && choice_script_arg0_list[| j] == ally_index
				{
					action_button_pressed_set(false);
					voln_play_sfx(sfx_mouse_click_choice); 
					//var choice_scene = script_get_index_ext(choice_scene_list[| j]);
					var choice_script = script_get_index_ext(choice_script_list[| j]);
					
					if choice_script != noone
						script_execute_add(0,choice_script,choice_script_arg0_list[| j],choice_script_arg1_list[| j],choice_script_arg2_list[| j],choice_script_arg3_list[| j],choice_script_arg4_list[| j],choice_script_arg5_list[| j],choice_script_arg6_list[| j]);
					
					//if choice_scene != noone
					//	script_execute(choice_scene,choice_scene_arg0_list[| j],choice_scene_arg1_list[| j],choice_scene_arg2_list[| j],choice_scene_arg3_list[| j],choice_scene_arg4_list[| j]);
				}

			}
			
			
		}*/
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		draw_reset();
		
		//draw_sprite(spr_white,-1,char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT]);
		//if mouse_check_button_pressed(mb_middle)
		//	show_message("char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT]: "+string(char[? GAME_CHAR_X_FLOATING_TEXT])+", "+string(char[? GAME_CHAR_X_FLOATING_TEXT]));

	}
}

if image_alpha > 0
	draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x,y,area_width,area_height,image_alpha,fa_right,fa_bottom);


//draw_placeholder_sprite_notes("Ally");