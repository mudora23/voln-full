if ally_index >= 0 && ds_list_size(obj_control.var_map[? VAR_ALLY_LIST]) > ally_index
{
	char = ally_get_char(ally_index);
	
	char[? GAME_CHAR_HEALTH_VISUAL] = char[? GAME_CHAR_HEALTH];
	char_stamina_visual = char[? GAME_CHAR_STAMINA];
	char[? GAME_CHAR_LUST_VISUAL] = char[? GAME_CHAR_LUST];
	char_toru_visual = char[? GAME_CHAR_TORU];
	
	created = true;
}
else alarm[0] = 1;