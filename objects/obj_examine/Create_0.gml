/// @description Insert description here
// You can write your code in this editor
depth = DEPTH_EXAMINE;
sprite_index = obj_control.mouse_on_examine_sprite;
sprite_width_fit_ratio = 1920/sprite_width;
sprite_height_fit_ratio = 1080/sprite_height;

sprite_best_fit_ratio = min(sprite_width_fit_ratio,sprite_height_fit_ratio);
sprite_ratio = sprite_best_fit_ratio;
sprite_center_x = room_width/2;
sprite_center_y = room_height/2;
image_alpha = 0;

//sprite_ratio_target = sprite_ratio;
//sprite_center_x_target = sprite_center_x;
//sprite_center_y_target = sprite_center_y;
image_alpha_target = 1;

mouse_previousx = mouse_x;
mouse_previousy = mouse_y;



		// restriction
		var halign_map = obj_control.var_map[? VAR_CG_HALIGN_MAP];
		var valign_map = obj_control.var_map[? VAR_CG_VALIGN_MAP];

		if ds_map_exists(halign_map,sprite_get_name(sprite_index))
			var halign = halign_map[? sprite_get_name(sprite_index)];
		else
			var halign = fa_none;

		if ds_map_exists(valign_map,sprite_get_name(sprite_index))
			var valign = valign_map[? sprite_get_name(sprite_index)];
		else
			var valign = fa_none;	



		// ratio
		if halign == fa_center
		{
			sprite_ratio = clamp(sprite_ratio,room_width / sprite_get_width(sprite_index),1);
		}
		if valign == fa_center
		{
			sprite_ratio = clamp(sprite_ratio,room_height / sprite_get_height(sprite_index),1);
		}

		// size
		if halign == fa_left
			sprite_center_x = sprite_width/2 * sprite_ratio;
		else if halign == fa_right
			sprite_center_x = room_width - sprite_width/2 * sprite_ratio;
		else if halign == fa_center
			sprite_center_x = room_width/2;
	
		if valign == fa_top
			sprite_center_y = sprite_height/2 * sprite_ratio;
		else if valign == fa_bottom
			sprite_center_y = room_height - sprite_height/2 * sprite_ratio;
		else if valign == fa_center
			sprite_center_y = room_height/2;
	
	
		sprite_ratio_target = sprite_ratio;
		sprite_center_x_target = sprite_center_x;
		sprite_center_y_target = sprite_center_y;