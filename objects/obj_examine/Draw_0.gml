/// @description Insert description here
// You can write your code in this editor

if mouse_wheel_up()
    sprite_ratio_target+=sprite_ratio_target*0.2;
if mouse_wheel_down()
    sprite_ratio_target-=sprite_ratio_target*0.2;
if mouse_check_button(mb_left)
{
    sprite_center_x_target += mouse_x - mouse_previousx;
    sprite_center_y_target += mouse_y - mouse_previousy;
}
if keyboard_check_pressed(vk_backspace) ||
   keyboard_check_pressed(vk_escape) ||
   keyboard_check_pressed(vk_space) ||
   keyboard_check_pressed(vk_delete) ||
   cancel_button_pressed_get()
   {
	cancel_button_pressed_set(false);
	image_alpha_target = 0;
	}
	
if image_alpha_target == 0 && image_alpha == 0 instance_destroy();
 

// restriction

var halign_map = obj_control.var_map[? VAR_CG_HALIGN_MAP];
var valign_map = obj_control.var_map[? VAR_CG_VALIGN_MAP];

if ds_map_exists(halign_map,sprite_get_name(sprite_index))
	var halign = halign_map[? sprite_get_name(sprite_index)];
else
	var halign = fa_none;

if ds_map_exists(valign_map,sprite_get_name(sprite_index))
	var valign = valign_map[? sprite_get_name(sprite_index)];
else
	var valign = fa_none;		
		
		
if halign == fa_center || valign == fa_center
{
	if halign == fa_center
		sprite_ratio_target = clamp(sprite_ratio_target,room_width / sprite_get_width(sprite_index),1);
	if valign == fa_center
		sprite_ratio_target = clamp(sprite_ratio_target,room_height / sprite_get_height(sprite_index),1);
}
else
{
	sprite_ratio_target = clamp(sprite_ratio_target,sprite_best_fit_ratio*0.7,1);
}



/*var x1_target = sprite_width/2*sprite_ratio_target;
var x2_target = room_width-sprite_width/2*sprite_ratio_target;
var xmin_target = min(-sprite_width/2*sprite_ratio_target,x1_target,x2_target);
var xmax_target = max(room_width-sprite_width/2*sprite_ratio_target,x1_target,x2_target);
var y1_target = sprite_height/2*sprite_ratio_target;
var y2_target = room_height-sprite_height/2*sprite_ratio_target;
var ymin_target = min(-sprite_height/2*sprite_ratio_target,y1_target,y2_target);
var ymax_target = max(room_height-sprite_height/2*sprite_ratio_target,y1_target,y2_target);*/



/*var x1 = sprite_width/2*sprite_ratio;
var x2 = room_width-sprite_width/2*sprite_ratio;
var xmin = min(0,x1,x2);
var xmax = max(room_width,x1,x2);
var y1 = sprite_height/2*sprite_ratio;
var y2 = room_height-sprite_height/2*sprite_ratio;
var ymin = min(0,y1,y2);
var ymax = max(room_height,y1,y2);*/

if halign == fa_left
{
	xmin_target = 0;
	xmax_target = sprite_width/2*sprite_ratio_target;
	
}
else if halign == fa_right
{
	xmin_target = room_width-sprite_width/2*sprite_ratio_target;
	xmax_target = room_width;
}
else if halign == fa_center
{
	xmax_target = sprite_width/2*sprite_ratio_target;
	xmin_target = room_width-sprite_width/2*sprite_ratio_target;
}
else
{
	var x1_target = sprite_width/2*sprite_ratio_target;
	var x2_target = room_width-sprite_width/2*sprite_ratio_target;
	var xmin_target = min(0,x1_target,x2_target);
	var xmax_target = max(room_width,x1_target,x2_target);

}
	
if valign == fa_top
{
	ymin_target = 0;
	ymax_target = sprite_height/2*sprite_ratio_target;
}
else if valign == fa_bottom
{
	ymin_target = room_height-sprite_height/2*sprite_ratio_target;
	ymax_target = room_height;
}
else if valign == fa_center
{
	ymax_target = sprite_height/2*sprite_ratio_target;
	ymin_target = room_height-sprite_height/2*sprite_ratio_target;
} 
else
{

	var y1_target = sprite_height/2*sprite_ratio_target;
	var y2_target = room_height-sprite_height/2*sprite_ratio_target;
	var ymin_target = min(0,y1_target,y2_target);
	var ymax_target = max(room_height,y1_target,y2_target);

}

sprite_center_x_target = clamp(sprite_center_x_target,xmin_target,xmax_target);
sprite_center_y_target = clamp(sprite_center_y_target,ymin_target,ymax_target);
//sprite_center_x = clamp(sprite_center_x,xmin,xmax);
//sprite_center_y = clamp(sprite_center_y,ymin,ymax);

//var ymin = min(y1,y2);
//var ymax = max(y1,y2);
//sprite_center_x_target = clamp(sprite_center_x_target,0,room_width);
//sprite_center_y_target = clamp(sprite_center_y_target,0,room_height);

//sprite_ratio = approach(sprite_ratio,sprite_ratio_target,FADE_NORMAL_SEC/room_speed);
sprite_ratio = smooth_approach_room_speed(sprite_ratio,sprite_ratio_target,SMOOTH_SLIDE_VERYFAST_SEC);
sprite_center_x = smooth_approach_room_speed(sprite_center_x,sprite_center_x_target,SMOOTH_SLIDE_VERYFAST_SEC);
sprite_center_y = smooth_approach_room_speed(sprite_center_y,sprite_center_y_target,SMOOTH_SLIDE_VERYFAST_SEC);
image_alpha = approach(image_alpha,image_alpha_target,FADE_FAST_SEC/room_speed);

draw_set_alpha(image_alpha*0.85);
draw_set_color(c_lightblack);
draw_rectangle(0,0,room_width,room_height,false);
draw_sprite_ext( sprite_index, clothes_sets_get_image_index(sprite_index), sprite_center_x-sprite_width/2*sprite_ratio, sprite_center_y-sprite_height/2*sprite_ratio, sprite_ratio, sprite_ratio, 0, c_white, image_alpha );
draw_reset();

mouse_previousx = mouse_x;
mouse_previousy = mouse_y;

