/// @description Insert description here
// You can write your code in this editor
depth = DEPTH_LOG;

area_width = sprite_width;
area_height = sprite_height;

battlelog_sprite_list = ds_list_create();
battlelog_queue_list = ds_list_create();
battlelog_processing_char = noone;
battlelog_processing_char_remaining = 0;
battlelog_processing_string = "";
battlelog_processing_x_padding = 7;
battlelog_processing_y_padding = 15;
battlelog_processing_widthmax = 340;
battlelog_processing_lineheight = 25;
battlelog_surface_w = 360;
battlelog_surface_h = 200;
battlelog_surface = surface_create(battlelog_surface_w,battlelog_surface_h);

battlelog_yoffset = 0;
battlelog_yoffset_target = 0;

battlelog_autobreak_list = ds_list_create();
battlelog_line_justified_list = ds_list_create();


battlelog_max_records = 40;
battlelog_processing_char_speed_default = 4;
battlelog_processing_char_speed = 10000;

letter_drawn_xx_list = ds_list_create();
letter_drawn_yy_list = ds_list_create();
letter_drawn_current_line_list = ds_list_create();

var_width = 5;
var_outfidelity = 30;

battlelog_text_size = 0.6;

image_alpha = 0;

ds_list_add_multiple(battlelog_queue_list,var_map_get(VAR_BATTLELOG_COPY_LIST));