{
    "id": "6b1f4ab5-7964-49a9-af96-487898a45bca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlelog",
    "eventList": [
        {
            "id": "cb423bcf-0351-467e-83b3-b3715b1081fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6b1f4ab5-7964-49a9-af96-487898a45bca"
        },
        {
            "id": "df6e3183-eca7-4f89-926a-ccbf94830db4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b1f4ab5-7964-49a9-af96-487898a45bca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4c74733e-8571-4683-8148-4f4677b60af5",
    "visible": true
}