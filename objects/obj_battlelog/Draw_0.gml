/// @description Insert description here
// You can write your code in this editor
//draw_self();

// VAR_BATTLELOG_COPY_LIST
while ds_list_size(var_map_get(VAR_BATTLELOG_COPY_LIST)) > battlelog_max_records
	ds_list_delete(var_map_get(VAR_BATTLELOG_COPY_LIST),0);

if ds_list_size(battlelog_queue_list) == 0
	battlelog_processing_char_speed = battlelog_processing_char_speed_default;

image_alpha = obj_control.main_gui_image_alpha;

draw_set_alpha(image_alpha);


if image_alpha > 0
{
draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,image_alpha);
gpu_set_blendmode(bm_normal);
}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////		
/////////////////// text rendering ////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////	
if battlelog_processing_char == noone
{
    if ds_list_size(battlelog_queue_list) > 0
    {
        battlelog_processing_string = battlelog_queue_list[| 0];
        battlelog_processing_char = 1;
        battlelog_processing_char_remaining = 0;
        if surface_exists(battlelog_surface) surface_free(battlelog_surface);
        battlelog_surface = surface_create(battlelog_surface_w,battlelog_surface_h);
        ds_list_delete(battlelog_queue_list,0);   
        battlelog_processing_x = battlelog_processing_x_padding;
        battlelog_processing_y = battlelog_processing_y_padding;
        ds_list_clear(battlelog_autobreak_list);
        battlelog_autobreak_list = battlelog_get_autobreak_list(battlelog_processing_string,battlelog_processing_widthmax,battlelog_text_size);
        ds_list_clear(battlelog_line_justified_list);


    }   
}
else if !surface_exists(battlelog_surface)
{
    battlelog_surface = surface_create(battlelog_surface_w,battlelog_surface_h);
    battlelog_processing_char = 1;
    battlelog_processing_char_remaining = 0;
    battlelog_processing_x = battlelog_processing_x_padding;
    battlelog_processing_y = battlelog_processing_y_padding;
}
else
{
    battlelog_processing_char_remaining += battlelog_processing_char_speed;
    surface_set_target(battlelog_surface);
    //gpu_set_blendmode_ext(bm_src_alpha, bm_one);
	gpu_set_blendmode_ext(bm_src_alpha, bm_dest_alpha);
    
    while (battlelog_processing_char_remaining >= 1)
    {
		var i = battlelog_processing_char;
        var the_char = string_char_at(battlelog_processing_string, i);
		var the_font = real(string_char_at(battlelog_processing_string, i+1));
        var the_color = real(string_char_at(battlelog_processing_string, i+2));

        if i == 1
        { 
            draw_clear_alpha(c_black, 0);
			//ds_list_clear(letter_drawn_xx_list);
			//ds_list_clear(letter_drawn_yy_list);
			//ds_list_clear(letter_drawn_current_line_list);
			//letter_drawn_xx_list[| 1] = battlelog_processing_x_padding;
			//letter_drawn_yy_list[| 1] = battlelog_processing_y_padding;
			//letter_drawn_current_line_list[| 1] = 1;

        }
        draw_reset();
        //draw_set_font(font_battlelog);
        
        //var letter_drawn_xx = battlelog_processing_x;//letter_drawn_xx_list[| i];
        //var letter_drawn_yy = battlelog_processing_y;//letter_drawn_yy_list[| i];
        //var letter_drawn_current_line = //letter_drawn_current_line_list[| i];

        /*if ds_list_size(battlelog_line_justified_list) < letter_drawn_current_line + 1
        {
            var line_justified_list_xx = letter_drawn_xx;
            var line_justified_list_current_line_chars = 0;
            
            for(var j = i; j <= string_length(battlelog_processing_string);j+=2)
            {
                var the_char_j = string_char_at(battlelog_processing_string,j);
                
                if ds_list_find_index(battlelog_autobreak_list,j) >= 0
                {
                    var available_space = battlelog_processing_widthmax - line_justified_list_xx;
                    var available_space_per_char = available_space / line_justified_list_current_line_chars;
                    battlelog_line_justified_list[| letter_drawn_current_line] = available_space_per_char;
                    break;
                }
                else if the_char_j == "\r"
                {
                    if line_justified_list_xx > battlelog_processing_widthmax
                    {
                        var available_space = battlelog_processing_widthmax - line_justified_list_xx;
                        var available_space_per_char = available_space / line_justified_list_current_line_chars;
                        battlelog_line_justified_list[| letter_drawn_current_line] = available_space_per_char;;
                    }
                    else battlelog_line_justified_list[| letter_drawn_current_line] = 0;
                    break;
                }

                else if j == string_length(battlelog_processing_string) - 1
                {
                    if line_justified_list_xx > battlelog_processing_widthmax
                    {
                        var available_space = battlelog_processing_widthmax - line_justified_list_xx;
                        var available_space_per_char = available_space / line_justified_list_current_line_chars;
                        battlelog_line_justified_list[| letter_drawn_current_line] = available_space_per_char;
                    }
                    else battlelog_line_justified_list[| letter_drawn_current_line] = 0;
                    break;
                }
                else
                {
                    line_justified_list_current_line_chars++;
                    line_justified_list_xx+=string_width(the_char_j);
                }
                
            }
        }
		*/

        if the_color == TAG_COLOR_NORMAL draw_set_color(COLOR_NORMAL);
        if the_color == TAG_COLOR_DAMAGE draw_set_color(COLOR_DAMAGE);
        if the_color == TAG_COLOR_EFFECT draw_set_color(COLOR_EFFECT);
        if the_color == TAG_COLOR_LOCATION draw_set_color(COLOR_LOCATION);
        if the_color == TAG_COLOR_HEAL draw_set_color(COLOR_HEAL);
        if the_color == TAG_COLOR_ENEMY_NAME draw_set_color(COLOR_ENEMY_NAME);
		if the_color == TAG_COLOR_FLIRT draw_set_color(COLOR_FLIRT);
		if the_color == TAG_COLOR_ALLY_NAME draw_set_color(COLOR_ALLY_NAME);
		if the_color == TAG_COLOR_TORU draw_set_color(COLOR_TORU);

        //draw_text_outlined(letter_drawn_xx,letter_drawn_yy,the_char,draw_get_color(),c_black,1,var_width,var_outfidelity);
        //draw_text(battlelog_processing_x,battlelog_processing_y,the_char);
		draw_text_transformed(battlelog_processing_x,battlelog_processing_y,the_char,battlelog_text_size,battlelog_text_size,0);
		battlelog_processing_char+=3;
        battlelog_processing_char_remaining--;
        dialog_surface_text_yposition = battlelog_processing_y + string_height(the_char)*battlelog_text_size;

        if the_char == "\r"
        {
            //letter_drawn_current_line++;
            battlelog_processing_x = battlelog_processing_x_padding;
            battlelog_processing_y += battlelog_processing_lineheight+20;
        }
        else if ds_list_find_index(battlelog_autobreak_list,i) >= 0
        {
            //letter_drawn_current_line++;
            battlelog_processing_x = battlelog_processing_x_padding;
            battlelog_processing_y += battlelog_processing_lineheight;
        
        }
        else
        {
			//draw_set_font(font_battlelog);
			battlelog_processing_x+=string_width(the_char)*battlelog_text_size;
            //if ds_list_size(battlelog_line_justified_list) > letter_drawn_current_line
                //letter_drawn_xx+=string_width(the_char)/*+battlelog_line_justified_list[| letter_drawn_current_line]*/;
            //else
                //letter_drawn_xx+=string_width(the_char);
        }
            
        //letter_drawn_xx_list[| i+1] = letter_drawn_xx;
        //letter_drawn_yy_list[| i+1] = letter_drawn_yy;
        //letter_drawn_current_line_list[| i+1] = letter_drawn_current_line;

        if battlelog_processing_char >= string_length(battlelog_processing_string)
        {
            battlelog_processing_char = noone;
            
            // creating sprites
            var new_sprite = sprite_create_from_surface(battlelog_surface, 0, 0, battlelog_surface_w, dialog_surface_text_yposition, false, true, 0, 0);
            ds_list_add(battlelog_sprite_list,new_sprite);
			while ds_list_size(battlelog_sprite_list) > battlelog_max_records
				ds_list_delete(battlelog_sprite_list,0);
			battlelog_yoffset=9999;
			battlelog_yoffset_target=9999;
            break;
        }    
        
    } 

    gpu_set_blendmode(bm_normal);
    surface_reset_target();

}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////	
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////	
	
	
	
	
	
var total_height = 0;
for(var j = 0; j < ds_list_size(battlelog_sprite_list);j++)
{
	total_height += sprite_get_height(battlelog_sprite_list[| j]);
}

if mouse_over_area(x,y,area_width,area_height)
{
	if mouse_wheel_up() battlelog_yoffset_target -= 200;
	if mouse_wheel_down() battlelog_yoffset_target += 200;
}


battlelog_yoffset=clamp(battlelog_yoffset,0,max(0,total_height-area_height+20));
battlelog_yoffset_target=clamp(battlelog_yoffset_target,0,max(0,total_height-area_height+20));	
battlelog_yoffset=smooth_approach_room_speed(battlelog_yoffset,battlelog_yoffset_target,SMOOTH_SLIDE_FAST_SEC);

var xx = 0;
var yy = 0;	
for(var j = 0; j < ds_list_size(battlelog_sprite_list);j++)
{
	draw_sprite_in_visible_area(battlelog_sprite_list[| j],-1,x+xx,y+yy-battlelog_yoffset,1,1,c_white,image_alpha,x,y,area_width,area_height);
	yy+=sprite_get_height(battlelog_sprite_list[| j]);
}

draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x,y,area_width,area_height,image_alpha,fa_right,fa_bottom);
	
	
	
//draw_placeholder_sprite_notes("Log History");