/// @description Insert description here
// You can write your code in this editor
draw_reset();

var width = 570;
draw_set_font(spr_font_01_reg_bo);

//keyboard_string = string_copy(keyboard_string,1,15);
if string_width(keyboard_string) < width - 15
	text = keyboard_string;
else
	keyboard_string = text;


draw_set_color(c_almostblack);
draw_rectangle(x-5,y-5,x+width+10,y+45+10,false);
draw_set_color(c_dkdkwhite);
draw_rectangle(x,y,x+width,y+45,false);

//gpu_set_blendmode_ext(ds_list_find_value(obj_main.bm_list,obj_main.bm1), ds_list_find_value(obj_main.bm_list,obj_main.bm2));
//draw_text_spacing(x,y,text,3);
draw_set_color(c_almostblack);
draw_text(x+5,y,text);
draw_set_alpha(obj_control.flashing_alpha_slow);
draw_line_width(x+5+string_width(text)+5,y+3,x+5+string_width(text)+5,y+45-6,2);
//gpu_set_blendmode(bm_normal);

if keyboard_check_pressed(vk_enter)
{
	if text != ""
	{

		var player = obj_control.var_map[? VAR_PLAYER];
		player[? GAME_CHAR_DISPLAYNAME] = text;

		obj_control.var_map[? PC_Name] = text;
		
		//show_debug_message("after typing name... jumping to..."+string(jump_label));
		scene_jump(jump_label);
		keyboard_string = "";
		instance_destroy();
	}

}

draw_reset();
