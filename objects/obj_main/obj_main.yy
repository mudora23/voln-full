{
    "id": "5cae9f0d-025f-485b-b5b2-5e73f68105bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_main",
    "eventList": [
        {
            "id": "1ab5211d-b343-4f95-ba76-b4524a69d4eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5cae9f0d-025f-485b-b5b2-5e73f68105bd"
        },
        {
            "id": "7922d6e5-9345-492a-8e1a-00a116fe4e55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cae9f0d-025f-485b-b5b2-5e73f68105bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28ef0398-55bd-48a7-add6-7796bcb896ef",
    "visible": true
}