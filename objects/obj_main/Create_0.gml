/// @description Insert description here
// You can write your code in this editor

dialog_yoffset = 0;
dialog_yoffset_target = 0;
	
area_width = sprite_width;
area_height = sprite_height;

clipboard_icon_alpha = 0;
mouse_on_clipboard = false;

bm_list = ds_list_create();
ds_list_add(bm_list,bm_zero,bm_one,bm_src_colour,bm_inv_src_colour,bm_src_alpha,bm_inv_src_alpha,bm_dest_alpha,bm_inv_dest_alpha,bm_dest_colour,bm_inv_dest_colour,bm_src_alpha_sat);
bm1 = 0;
bm2 = 0;
bm3 = 0;
bm4 = 0;

depth = DEPTH_MAIN;

holding_scroll = false;



// map variables
map = spr_map_bg;
map_xsize = 10;
map_ysize = 10;
map_width = sprite_get_width(map);
map_height = sprite_get_height(map);
map_total_width = map_width * map_xsize;
map_total_height = map_height * map_ysize;
		
map_zoom_ratio_default = 0.5;
map_zoom_ratio = map_zoom_ratio_default;
map_zoom_ratio_target = map_zoom_ratio_default;
map_xoffset = 0;
map_yoffset = 0;
map_xoffset_target = 0;
map_yoffset_target = 0;
map_holding = false;
map_alpha = 1;
map_animated_frame = 0;

player_icon_image_index_dir = 0; // dir
player_icon_image_index_dir_frame = 3; // frame per dir
player_icon_image_index = 0; // which frame in a dir
player_icon_image_index_speed = 0; // timer
player_icon_image_index_speed_max = 0.2; // timer max



if !obj_control.loading scene_jump("",S_questionaire);

if obj_control.loading
{
	obj_map_player.x = ds_map_find_value(obj_control.location_map[? location_get_current()],"x");
	obj_map_player.y = ds_map_find_value(obj_control.location_map[? location_get_current()],"y");
	
	map_xoffset_target = obj_map_player.x*map_zoom_ratio - area_width/2;
	map_yoffset_target = obj_map_player.y*map_zoom_ratio - area_height/2;
	map_xoffset = obj_map_player.x*map_zoom_ratio - area_width/2;
	map_yoffset = obj_map_player.y*map_zoom_ratio - area_height/2;
}

obj_control.loading = false;

obj_control.enemies_choice_enable = false;
obj_control.current_enemies_choice = 0;

