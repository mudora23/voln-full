/// @description Insert description here
// You can write your code in this editor


//////////////////////////
//     Main Frame Position
//////////////////////////
var sprites_exists = false;
var sprite_list_id_list = obj_control.var_map[? VAR_SPRITE_LIST_ID];
for(var i = 0; i < ds_list_size(sprite_list_id_list);i++)
{
	if sprite_list_id_list[| i] != "bg"
	{
		sprites_exists = true;
		break;
	}
}


var target_xx = 437;
var target_yy = 329;
var target_width = sprite_width;
var target_height = sprite_height;

var cg_index_list = obj_control.var_map[? VAR_CG_LIST_INDEX_NAME];

if var_map_get(VAR_MODE) == VAR_MODE_BATTLE || var_map_get(VAR_MODE) == VAR_MODE_SEX_CHOOSING || var_map_get(VAR_MODE) == VAR_MODE_CAPTURE_CHOOSING || scene_sprite_list_sprite_wb_exists()
{
	target_yy = 329+200;
	target_height = sprite_height-200;
}
else if var_map_get(VAR_MODE) == VAR_MODE_STORE
{
	target_yy = 329-300;
	target_height = sprite_height+300;
}
else if ds_list_size(cg_index_list) > 0 && ds_list_value_exists(cg_index_list,"spr_cg_forest_felgor_baozet")
{
	target_xx = room_width - sprite_width - 30;
	target_yy = 200;
	target_height = 680;
}
else if ds_list_size(cg_index_list) > 0 && ds_list_value_exists(cg_index_list,"spr_cg_S_0007")
{
	target_xx = 30;
	target_yy = 200;
	target_height = 680;
}
else if ds_list_size(cg_index_list) > 0 && ds_list_value_exists(cg_index_list,"spr_cg_m0RV")
{
	target_xx = 150;
	target_yy = 200;
	target_height = 680;
}
else if ds_list_size(cg_index_list) > 0 && ds_list_value_exists(cg_index_list,"spr_cg_Ku_sex01_1")
{
	target_xx = 30;
	target_yy = 200;
	target_height = 680;
}
else if ds_list_size(cg_index_list) > 0 && ds_list_value_exists(cg_index_list,"spr_cg_Ku_sex02_1")
{
	target_xx = 200;
	target_yy = 200;
	target_height = 680;
}
else if sprites_exists
{
	target_yy = 329+100;
	target_height = sprite_height-100;
}
	

x = smooth_approach_room_speed(x,target_xx,SMOOTH_SLIDE_NORMAL_SEC);
y = smooth_approach_room_speed(y,target_yy,SMOOTH_SLIDE_NORMAL_SEC);
area_width = smooth_approach_room_speed(area_width,target_width,SMOOTH_SLIDE_NORMAL_SEC);
area_height = smooth_approach_room_speed(area_height,target_height,SMOOTH_SLIDE_NORMAL_SEC);




//////////////////////////
//     Main Frame BG
//////////////////////////
draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(obj_control.colorR_main,obj_control.colorG_main,obj_control.colorB_main),image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,image_alpha);
gpu_set_blendmode(bm_normal);


//////////////////////////
//     scripts
//////////////////////////
if var_map_get(VAR_MODE) == VAR_MODE_NORMAL || var_map_get(VAR_MODE) == VAR_MODE_SEX_CHOOSING || var_map_get(VAR_MODE) == VAR_MODE_CAPTURE_CHOOSING// || var_map_get(VAR_MODE) == VAR_MODE_STORE
{
	_start();

	if asset_get_type(var_map_get(VAR_SCRIPT)) == asset_script
		script_execute(asset_get_index(var_map_get(VAR_SCRIPT)));

	_end();
}


//////////////////////////
//     dialogs
//////////////////////////
draw_dialog_step();


		
// copy to clipboard
if os_type == os_windows
{	
	if !menu_exists()
	{
		if mouse_over_area(x,y,area_width,area_height) && var_map_get(VAR_SCRIPT_DIALOG_RAW) != ""
			clipboard_icon_alpha = approach(clipboard_icon_alpha,0.15,1/room_speed);
		else
			clipboard_icon_alpha = approach(clipboard_icon_alpha,0,3/room_speed);
	}
	
	mouse_on_clipboard = false;
	if var_map_get(VAR_SCRIPT_DIALOG_RAW) != "" && var_map_get(VAR_MODE) != VAR_MODE_STORE
	{
		var clipboard = spr_icon_clipboard;
		var clipboard_w = sprite_get_width(clipboard);
		var clipboard_h = sprite_get_height(clipboard);
		var clipboard_xx = obj_main.x+obj_main.area_width-clipboard_w;
		var clipboard_yy = obj_main.y;
		mouse_on_clipboard = mouse_over_area(clipboard_xx,clipboard_yy,clipboard_w,clipboard_h);
		if mouse_on_clipboard && !menu_exists()
		{

			if !obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD]
			{
				draw_sprite_ext(clipboard,1,clipboard_xx,clipboard_yy,1,1,0,c_battle_outline,clipboard_icon_alpha*4);
				draw_set_floating_tooltip("Copy text to clipboard",clipboard_xx+clipboard_w/2,clipboard_yy,fa_center,fa_bottom,false);
				if action_button_mouse_pressed_get()
				{
					action_button_pressed_set(false);
					clipboard_set_text(var_map_get(VAR_SCRIPT_DIALOG_RAW));
					voln_play_sfx(sfx_copy_to_clipboard);
				}
			}
			else
			{
				draw_set_floating_tooltip("Copy text to clipboard automatically (ON)",clipboard_xx+clipboard_w/2,clipboard_yy,fa_center,fa_bottom,false);				
			}

		}
		draw_sprite_ext(clipboard,0,clipboard_xx,clipboard_yy,1,1,0,c_silver,clipboard_icon_alpha);
		if obj_control.settings_map[? SETTINGS_COPY_TO_CLIPBOARD]
		{
			draw_set_halign(fa_center);
			draw_set_valign(fa_center);
			draw_set_color(c_white);
			draw_set_alpha(clipboard_icon_alpha*1.5);
			draw_set_font(font_01s_reg);
			draw_text_transformed(clipboard_xx+clipboard_w/2,clipboard_yy+clipboard_h,"Auto",0.8,0.8,0);
			draw_reset();
		}
	}
}

// DEBUG - chunk name
if obj_control.debug_enable
{
			
	draw_set_halign(fa_right);
	draw_set_valign(fa_bottom);
	draw_set_font(font_01s_reg);
	draw_set_color(c_white);
	draw_set_alpha(0.8);
	draw_text(obj_main.x+obj_main.area_width-70,obj_main.y+35,obj_control.debug_chunk_name);
	draw_reset();

}

//////////////////////////
//     store
//////////////////////////
draw_store_step();





//////////////////////////
//     main map
//////////////////////////
draw_main_map_step();


//////////////////////////
//     battle
//////////////////////////
if var_map_get(VAR_MODE) == VAR_MODE_BATTLE || var_map_get(VAR_MODE) == VAR_MODE_SEX_CHOOSING || var_map_get(VAR_MODE) == VAR_MODE_CAPTURE_CHOOSING
{
	enemies_in_battle_step();
}



//////////////////////////
//     battle ordering list
//////////////////////////
if var_map_get(VAR_MODE) == VAR_MODE_BATTLE
{
	ordering_list_in_battle_step();
}

//////////////////////////
//     borders
//////////////////////////
draw_borders_in_area(spr_gui_border_top_left_c,spr_gui_border_top_right_c,spr_gui_border_bot_left_c,spr_gui_border_bot_right_c,spr_gui_border_top_seemless_c,spr_gui_border_bot_seemless_c,spr_gui_border_left_seemless_c,spr_gui_border_right_seemless_c,x,y,area_width,area_height,1,fa_right,fa_bottom);






if keyboard_check_pressed(ord("Z"))
{
	bm1++;
	if ds_list_size(bm_list) <= bm1
		bm1 = 0;
}
if keyboard_check_pressed(ord("X"))
{
	bm2++;
	if ds_list_size(bm_list) <= bm2
		bm2 = 0;
}
if keyboard_check_pressed(ord("C"))
{
	bm3++;
	if ds_list_size(bm_list) <= bm3
		bm3 = 0;
}
if keyboard_check_pressed(ord("V"))
{
	bm4++;
	if ds_list_size(bm_list) <= bm4
		bm4 = 0;
}
	
	
	
	

	
	
	
	