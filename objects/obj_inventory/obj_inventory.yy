{
    "id": "55ff1d5a-fa0b-4819-9083-147a667b5f4c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory",
    "eventList": [
        {
            "id": "2e37d51c-954e-41fd-831f-64baa6a53b10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "55ff1d5a-fa0b-4819-9083-147a667b5f4c"
        },
        {
            "id": "3792e285-b73f-46e8-a919-f56728f65a71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55ff1d5a-fa0b-4819-9083-147a667b5f4c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c3be5441-bf26-4340-8965-3201a8fdd2a5",
    "visible": true
}