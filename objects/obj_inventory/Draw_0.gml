/// @description Insert description here
// You can write your code in this editor

/*
if scene_get_GUI() == GAME_GUI_MIN image_alpha = approach(image_alpha,0,FADE_NORMAL_SEC*(1/room_speed));
else image_alpha = approach(image_alpha,1,FADE_NORMAL_SEC*(1/room_speed));
draw_set_alpha(image_alpha);
*/

image_alpha = obj_control.main_gui_image_alpha;

if image_alpha > 0
{
draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,image_alpha);
gpu_set_blendmode(bm_normal);
}




//draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,c_white,1,fa_right,fa_bottom);
//gpu_set_blendmode_ext(bm_dest_color, bm_zero);
//draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,1);
//gpu_set_blendmode(bm_normal);
//draw_borders_in_area(spr_gui_border_top_left,spr_gui_border_top_right,spr_gui_border_bot_left,spr_gui_border_bot_right,spr_gui_border_top_seemless,spr_gui_border_bot_seemless,spr_gui_border_left_seemless,spr_gui_border_right_seemless,x,y,area_width,area_height,1,fa_right,fa_bottom);

if mouse_over_area(x,y,area_width,area_height) && !menu_exists() && image_alpha > 0.5
{
	draw_sprite_ext(sprite_index,1,x,y,1,1,0,c_battle_outline,image_alpha);
	if !instance_exists(obj_menu) && !main_gui_clicking_disabled() && action_button_pressed_get() && draw_get_alpha() > 0.5
	{
		action_button_pressed_set(false);
		voln_play_sfx(sfx_inventory_open);
		instance_create_depth(0,0,DEPTH_MENU,obj_menu);
		obj_menu.menu_open = "Items";
	}
	
}

draw_sprite_ext(sprite_index,0,x,y,1,1,0,c_white,image_alpha);

draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x,y,area_width,area_height,image_alpha,fa_right,fa_bottom);
	





//draw_placeholder_sprite_notes("Inventory");