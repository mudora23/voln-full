{
    "id": "48f8c89a-adcc-476f-9a97-0d9b533ee57a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_information",
    "eventList": [
        {
            "id": "6d5ab09a-45fc-4abc-8424-32e36e08e414",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48f8c89a-adcc-476f-9a97-0d9b533ee57a"
        },
        {
            "id": "5e3a45dc-ceab-401a-b9e4-cf124d519b41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "48f8c89a-adcc-476f-9a97-0d9b533ee57a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a0b7271-4a4d-4a04-a218-36b8a33c2027",
    "visible": true
}