/// @description Insert description here
// You can write your code in this editor

char = obj_control.var_map[? VAR_PLAYER];

char_name_visual = char[? GAME_CHAR_NAME];
char_displayname_visual = char[? GAME_CHAR_DISPLAYNAME];
char_vitality_visual = char[? GAME_CHAR_VITALITY];
char_vitalitymax_visual = char_get_vitality_max(char);
char[? GAME_CHAR_HEALTH_VISUAL] = char[? GAME_CHAR_HEALTH];
char_healthmax_visual = char_get_health_max(char);
char_stamina_visual = char[? GAME_CHAR_STAMINA];
char_staminamax_visual = char_get_stamina_max(char);
char[? GAME_CHAR_LUST_VISUAL] = char[? GAME_CHAR_LUST];
char_lustmax_visual = char_get_lust_max(char);
char_toru_visual = char[? GAME_CHAR_TORU];
char_torumax_visual = char_get_toru_max(char);
char_exp_visual = char[? GAME_CHAR_EXP];
char_level_visual = char[? GAME_CHAR_LEVEL];

char_str_visual = char[? GAME_CHAR_STR];
char_fou_visual = char[? GAME_CHAR_FOU];
char_dex_visual = char[? GAME_CHAR_DEX];
char_end_visual = char[? GAME_CHAR_END];
char_int_visual = char[? GAME_CHAR_INT];
char_wis_visual = char[? GAME_CHAR_WIS];
char_spi_visual = char[? GAME_CHAR_SPI];


coin_animated_frame = 0;
jar_animated_frame = 0;


depth = DEPTH_PLAYER_INFO;
image_alpha = 0;

area_width = sprite_width;
area_height = sprite_height;