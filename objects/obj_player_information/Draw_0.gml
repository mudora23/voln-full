/// @description Insert description here
// You can write your code in this editor


// update
draw_reset();

/*if scene_get_GUI() == GAME_GUI_MIN image_alpha = approach(image_alpha,0,FADE_NORMAL_SEC*(1/room_speed));
else image_alpha = approach(image_alpha,1,FADE_NORMAL_SEC*(1/room_speed));
draw_set_alpha(image_alpha);*/

image_alpha = obj_control.main_gui_image_alpha;


		draw_set_font(font_01s_reg);
        draw_set_valign(fa_center);
        draw_set_halign(fa_left);
		draw_set_color(c_ltgray);
		draw_set_alpha(image_alpha);
		// gold icon
		if floor(coin_animated_frame) == 0
			coin_animated_frame += (1/room_speed)/1.5;
		else
			coin_animated_frame += (1/room_speed)*10;
		
		if floor(coin_animated_frame) >= 3
			coin_animated_frame = 0;
		
		var icon_ratio = 0.7;
		
		var icon = spr_icon_coin;
		var icon_width = sprite_get_width(icon)*icon_ratio;
		var icon_area_width = 80+string_width(string(round(obj_control.var_map[? VAR_GOLD])));
		var icon_height = sprite_get_height(icon)*icon_ratio;
		var xx = x + 180;
		var yy = y - icon_height/2 - 10;

		var icon_area_hover = mouse_over_area(xx-icon_width/2,yy-icon_height/2,icon_area_width,icon_height) && image_alpha > 0;
		if icon_area_hover
		{
			draw_sprite_ext(icon, 4, xx,yy,icon_ratio,icon_ratio,0,c_white,image_alpha);
			if !menu_exists() && !main_gui_clicking_disabled()
				draw_set_floating_tooltip("This is the total amount of gold you have.",xx+icon_area_width/2,yy-icon_height/2,fa_center,fa_bottom,false);
			//tooltip = "This is the total amount of gold you have.";
		}
		draw_sprite_ext(icon, coin_animated_frame, xx,yy,icon_ratio,icon_ratio,0,c_white,image_alpha);
		draw_text_outlined(xx+icon_width/2+5,yy,obj_control.var_map[? VAR_GOLD],c_dkwhite,c_almostblack,1,3,15);
		
		// jar icon
		if floor(jar_animated_frame) == 0
			jar_animated_frame += (1/room_speed)/2;
		else
			jar_animated_frame += (1/room_speed)*10;
		
		if floor(jar_animated_frame) >= 3
			jar_animated_frame = 0;
			
			
		var icon = spr_icon_jar;
		var icon_width = sprite_get_width(icon)*icon_ratio;
		var icon_height = sprite_get_height(icon)*icon_ratio;
		var xx = x + 180 + icon_area_width;
		var yy = y - icon_height/2 - 10;
		var icon_area_width = 150;

		var icon_area_hover = mouse_over_area(xx-icon_width/2,yy-icon_height/2,icon_area_width,icon_height) && image_alpha > 0;
		if icon_area_hover
		{
			draw_sprite_ext(icon, 4, xx,yy,icon_ratio,icon_ratio,0,c_white,image_alpha);
			if !menu_exists() && !main_gui_clicking_disabled()
				draw_set_floating_tooltip("This is the total amount of empty jars you have.",xx+icon_area_width/2,yy-icon_height/2,fa_center,fa_bottom,false);
			//tooltip = "This is the total amount of empty jars you have.";
		}
		draw_sprite_ext(icon, jar_animated_frame, xx,yy,icon_ratio,icon_ratio,0,c_white,image_alpha);
		draw_text_outlined(xx+icon_width/2+5,yy,obj_control.var_map[? VAR_JAR],c_dkwhite,c_almostblack,1,3,15);






var time_1 = "";
if obj_control.var_map[? VAR_TIME_HOUR] < 10 time_1 += "0";
time_1 += string(floor(obj_control.var_map[? VAR_TIME_HOUR]));
var time_2 = ":";
var time_3 = "00";
var time_3_num = floor((obj_control.var_map[? VAR_TIME_HOUR] - floor(obj_control.var_map[? VAR_TIME_HOUR]))*60);
if time_3_num < 10 var time_3 = "0"+string(time_3_num);
else var time_3 = string(time_3_num);


var date = string(obj_control.var_map[? VAR_TIME_YEAR]);
date += ", ";
date += game_time_get_month_name(obj_control.var_map[? VAR_TIME_MONTH]);
date += " ";
date += string(obj_control.var_map[? VAR_TIME_DAY]);

draw_set_font(font_01s_reg);
draw_set_alpha(image_alpha);
draw_text_outlined(10,y-string_height(date)-string_height(time_3)/*-50*/,time_1,c_dkwhite,c_almostblack,1,3,15);
draw_set_alpha((obj_control.flashing_alpha_slow >= 0.3)*image_alpha);
draw_text_outlined(10+string_width(time_1)+5,y-string_height(date)-string_height(time_3)/*-50*/,time_2,c_dkwhite,c_almostblack,1,3,15);
draw_set_alpha(image_alpha);
draw_text_outlined(10+string_width(time_1)+string_width(time_2)+10,y-string_height(date)-string_height(time_3)/*-50*/,time_3,c_dkwhite,c_almostblack,1,3,15);
draw_text_outlined(10,y-string_height(date)/*-50*/,date,c_dkwhite,c_almostblack,1,3,15);


draw_set_halign(fa_left);
draw_set_valign(fa_top);

var message_yy = y - string_height(date)-string_height(time_3) - 10;
var message_xx = 10;

// Overwhelmed
var inventory_total_weight = inventory_get_total_weight();
var inventory_max_weight = inventory_get_max_weight();
var inventory_overweight_ratio = inventory_total_weight / inventory_max_weight;
if inventory_overweight_ratio > 1
{
	message_yy -= 25;
	var text = "[!] Overwhelmed ("+string(round(inventory_overweight_ratio*100))+"%)";
	draw_set_alpha(image_alpha*max(0.7,obj_control.flashing_alpha_slow));
	draw_text_outlined(message_xx,message_yy,text,COLOR_HP,c_almostblack,1,3,15);
	draw_set_alpha(image_alpha);
	if mouse_over_area(message_xx,message_yy,string_width(text),string_height(text)) && !menu_exists()
		draw_set_floating_tooltip("You are overwhelmed. Your Vitality drains much faster overtime.\n\nYou can increase the maximum weight you can carry by:\n    -- Bringing more teammates with you.\n    -- Having more MIGHT attributes(STR,FOU,END) within your team.",message_xx+string_width(text)+20,message_yy,fa_left,fa_top,false);
}

// Quest Journal Notice
if var_map_get(VAR_NEW_QJ_NOTICE)
{
	if ds_list_size(var_map_get(VAR_QUEST_JOURNAL_LIST)) == 0 var_map_set(VAR_NEW_QJ_NOTICE,false);
	message_yy -= 25;
	var text = "[!] Quest Journal has been updated!";
	draw_set_alpha(image_alpha*max(0.7,obj_control.flashing_alpha_slow));
	draw_text_outlined(message_xx,message_yy,text,COLOR_EFFECT,c_almostblack,1,3,15);
	draw_set_alpha(image_alpha);
	if mouse_over_area(message_xx,message_yy,string_width(text),string_height(text)) && !menu_exists()
	{
		draw_set_floating_tooltip("Your quest Journal has been updated. Left click to view.",message_xx+string_width(text)+20,message_yy,fa_left,fa_top,false);
		if action_button_mouse_pressed_get() && !menu_exists()
		{
			action_button_mouse_pressed_set(false);
			instance_create_depth(0,0,DEPTH_MENU,obj_menu);
			voln_play_sfx(sfx_inventory_open);
			obj_menu.menu_open = MENULISTING_QUEST_JOURNAL;
		}
	}
}

// Unspent skill points
if obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS] > 0
{
	message_yy -= 25;
	var text = "[!] Unused skill points ("+string(obj_control.var_map[? VAR_PLAYER_UNSPEND_SKILL_POINTS])+")";
	draw_set_alpha(image_alpha*max(0.7,obj_control.flashing_alpha_slow));
	draw_text_outlined(message_xx,message_yy,text,COLOR_EFFECT,c_almostblack,1,3,15);
	draw_set_alpha(image_alpha);
	if mouse_over_area(message_xx,message_yy,string_width(text),string_height(text)) && !menu_exists()
	{
		draw_set_floating_tooltip("You have unused skill point(s). Left click to assign.",message_xx+string_width(text)+20,message_yy,fa_left,fa_top,false);
		if action_button_mouse_pressed_get() && !menu_exists()
		{
			action_button_mouse_pressed_set(false);
			instance_create_depth(0,0,DEPTH_MENU,obj_menu);
			voln_play_sfx(sfx_inventory_open);
			obj_menu.menu_open = MENULISTING_ATTRIBUTES_AND_SKILLS;
		}
	}
}
















if image_alpha > 0
{
draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),obj_control.main_gui_image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,obj_control.main_gui_image_alpha);
gpu_set_blendmode(bm_normal);

if var_map_get(VAR_MODE) == VAR_MODE_BATTLE
	if obj_control.hovered_char_on_screen == obj_control.var_map[? VAR_PLAYER] || battle_get_current_action_char() == char
	{
		draw_set_color(COLOR_ALLY_NAME);
		draw_set_alpha(0.05*obj_control.flashing_alpha);
		draw_rectangle(x,y,x+area_width,y+area_height,false);
		draw_reset();		
	}


draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x,y,area_width,area_height,obj_control.main_gui_image_alpha,fa_right,fa_bottom);
}


//if char_get_current_action_char() == char
//	draw_borders_in_area(spr_gui_borderC_top_left_c,spr_gui_borderC_top_right_c,spr_gui_borderC_bot_left_c,spr_gui_borderC_bot_right_c,spr_gui_borderC_top_seemless_c,spr_gui_borderC_bot_seemless_c,spr_gui_borderC_left_seemless_c,spr_gui_borderC_right_seemless_c,x,y,area_width,area_height,image_alpha*obj_control.flashing_alpha,fa_right,fa_bottom,c_yellow);
	



char = obj_control.var_map[? VAR_PLAYER];

if mouse_over_area(x,y,area_width,area_height) && !menu_exists()
{
	obj_control.hovered_char_on_screen_delay = 2;
	obj_control.hovered_char_on_screen = char;
}
	

char[? GAME_CHAR_X_FLOATING_TEXT] = x+area_width/2;
char[? GAME_CHAR_Y_FLOATING_TEXT] = y+area_height/2;


		
char_name = char[? GAME_CHAR_NAME];
char_displayname = char[? GAME_CHAR_DISPLAYNAME];
char_vitality = char[? GAME_CHAR_VITALITY];
char_vitalitymax = char_get_vitality_max(char);
char_health = char[? GAME_CHAR_HEALTH];
char_healthmax = char_get_health_max(char);
char_stamina = char[? GAME_CHAR_STAMINA];
char_staminamax = char_get_stamina_max(char);
char_lust = char[? GAME_CHAR_LUST];
char_lustmax = char_get_lust_max(char);
char_toru = char[? GAME_CHAR_TORU];
char_torumax = char_get_toru_max(char);
char_exp = char[? GAME_CHAR_EXP];
char_level = char[? GAME_CHAR_LEVEL];



char_str = char_get_STR(char);
char_fou = char_get_FOU(char);
char_end = char_get_END(char);
char_dex = char_get_DEX(char);
char_wis = char_get_WIS(char);
char_int = char_get_INT(char);
char_spi = char_get_SPI(char);

// visual position
var ratio_per_sec = 3;

char_name_visual = char_name;
char_displayname_visual = char_displayname;
char_vitality_visual = smooth_approach_room_speed(char_vitality_visual,char_vitality,ratio_per_sec);
char_vitalitymax_visual = char_get_vitality_max(char);
char[? GAME_CHAR_HEALTH_VISUAL] = smooth_approach_room_speed(char[? GAME_CHAR_HEALTH_VISUAL],char_health,ratio_per_sec);
char_healthmax_visual = char_get_health_max(char);//char_healthmax_visual = smooth_approach(char_healthmax_visual,char_healthmax,ratio_per_sec);
char_stamina_visual = smooth_approach_room_speed(char_stamina_visual,char_stamina,ratio_per_sec);
char_staminamax_visual = char_get_stamina_max(char);//char_staminamax_visual = smooth_approach(char_staminamax_visual,char_staminamax,ratio_per_sec);
char[? GAME_CHAR_LUST_VISUAL] = smooth_approach_room_speed(char[? GAME_CHAR_LUST_VISUAL],char_lust,ratio_per_sec);
char_lustmax_visual = char_get_lust_max(char);//char_lustmax_visual = smooth_approach(char_lustmax_visual,char_lustmax,ratio_per_sec);
char_toru_visual = smooth_approach_room_speed(char_toru_visual,char_toru,ratio_per_sec);
char_torumax_visual = char_get_toru_max(char);//char_torumax_visual = smooth_approach(char_torumax_visual,char_torumax,ratio_per_sec);
char_exp_visual = smooth_approach_room_speed(char_exp_visual,char_exp,ratio_per_sec);
char_level_visual = char_level;

char_str_visual = smooth_approach_room_speed(char_str_visual,char_str,ratio_per_sec);
char_fou_visual = smooth_approach_room_speed(char_fou_visual,char_fou,ratio_per_sec);
char_dex_visual = smooth_approach_room_speed(char_dex_visual,char_dex,ratio_per_sec);
char_end_visual = smooth_approach_room_speed(char_end_visual,char_end,ratio_per_sec);
char_int_visual = smooth_approach_room_speed(char_int_visual,char_int,ratio_per_sec);
char_wis_visual = smooth_approach_room_speed(char_wis_visual,char_wis,ratio_per_sec);
char_spi_visual = smooth_approach_room_speed(char_spi_visual,char_spi,ratio_per_sec);

//exit;

// drawing
//draw_self();
//draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,c_white,1,fa_right,fa_bottom);
//gpu_set_blendmode_ext(bm_dest_color, bm_zero);
//draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,1);
//gpu_set_blendmode(bm_normal);
//draw_borders_in_area(spr_gui_border_top_left,spr_gui_border_top_right,spr_gui_border_bot_left,spr_gui_border_bot_right,spr_gui_border_top_seemless,spr_gui_border_bot_seemless,spr_gui_border_left_seemless,spr_gui_border_right_seemless,x,y,area_width,area_height,1,fa_right,fa_bottom);

draw_reset();
	
// player's turn (highlight)

	//if battle_get_current_action_char() == char && battle_char_get_buff_level(char,BUFF_HIDE) <= 0
	//	draw_rectangle_outline_width(x,y,x+sprite_width,y+sprite_height,c_battle_outline,5,obj_control.flashing_alpha);

	var padding = 20;
	var xx = x+padding;
	var yy = y+padding;
	var ratio_per_sec = 3;
	var outline_width = 3;
	var outline_fidelity = 30;
	var outline_color = c_black;
	var text_color = c_dkwhite;
	draw_set_alpha(image_alpha);
	draw_set_color(text_color);
	draw_set_font(font_01n_reg);
	draw_text_transformed(x+padding,yy,string(scriptvar_PC_Name()),0.8,0.8,0);
	//draw_text_outlined(x+padding,yy,string(scriptvar_PC_Name()),text_color,outline_color,0.8,outline_width,outline_fidelity);
	draw_reset();
	
	
	
	draw_set_alpha(image_alpha);
		
		
	// AI mode
	draw_set_font(font_01s_sc);
	var ai_mode_text = "AI: "+char[? GAME_CHAR_AI];
	var ai_mode_ratio = 1;
	
	yy += 40;

		
	var ai_mode_hover = mouse_over_area(xx/*x+area_width-10-string_width(ai_mode_text)*ai_mode_ratio*/,yy,string_width(ai_mode_text)*ai_mode_ratio,string_height(ai_mode_text)*ai_mode_ratio) && !menu_exists() && !main_gui_clicking_disabled();
		
	if ai_mode_hover var outline_col = c_dkgray;
	else var outline_col = c_almostblack;
		
	if char[? GAME_CHAR_AI] == AI_AGGRESSIVE var ai_col = COLOR_DAMAGE;
	else if char[? GAME_CHAR_AI] == AI_SEDUCTIVE var ai_col = COLOR_LUST;
	else if char[? GAME_CHAR_AI] == AI_SUPPORTIVE var ai_col = COLOR_HEAL;
	else if char[? GAME_CHAR_AI] == AI_CONSERVATIVE var ai_col = COLOR_EFFECT;
	else if char[? GAME_CHAR_AI] == AI_NONE var ai_col = c_white;
	else var ai_col = c_white;
		
	draw_set_halign(fa_left);
	draw_text_outlined(xx/*x+area_width-10*/,yy,ai_mode_text,ai_col,outline_col,ai_mode_ratio,3,15);
		
	if ai_mode_hover && action_button_pressed_get() && !menu_exists()
	{
		action_button_pressed_set(false);
		/*if char[? GAME_CHAR_AI] == AI_AGGRESSIVE
			char[? GAME_CHAR_AI] = AI_SEDUCTIVE;
		else if char[? GAME_CHAR_AI] == AI_SEDUCTIVE
			char[? GAME_CHAR_AI] = AI_SUPPORTIVE;
		else if char[? GAME_CHAR_AI] == AI_SUPPORTIVE
			char[? GAME_CHAR_AI] = AI_CONSERVATIVE;
		else if char[? GAME_CHAR_AI] == AI_CONSERVATIVE
			char[? GAME_CHAR_AI] = AI_NONE;
		else if char[? GAME_CHAR_AI] == AI_NONE
			char[? GAME_CHAR_AI] = AI_AGGRESSIVE;
		else
			char[? GAME_CHAR_AI] = AI_NONE;*/
		with obj_ai_selector instance_destroy();
		voln_play_sfx(sfx_mouse_click);
		instance_create_depth(0,0,DEPTH_MENU+1,obj_ai_selector);
		obj_ai_selector.x1 = xx;
		obj_ai_selector.y1 = yy+string_height(ai_mode_text)+10;
		obj_ai_selector.char = char;
	}
	draw_reset();

	
	
	
	
	
	
	
	
	
	
	
	
	// draw buff debuff icons
	var buff_list = char[? GAME_CHAR_BUFF_LIST];
	var debuff_list = char[? GAME_CHAR_DEBUFF_LIST];
	var buff_debuff_icon_width = obj_control.buff_debuff_icon_sizeL;
	var buff_debuff_icon_height = obj_control.buff_debuff_icon_sizeL;
	var buff_debuff_icon_xx = x+area_width-buff_debuff_icon_width;
	var buff_debuff_icon_yy = y;
		
	for(var i = 0;i<ds_list_size(debuff_list);i++)
	{			
		var debuff = debuff_list[| i];
		var debuff_icon = short_term_buff_debuff_get_icon(debuff);
		draw_sprite_in_area(debuff_icon,buff_debuff_icon_xx+sprite_get_xoffset(debuff_icon),buff_debuff_icon_yy+sprite_get_yoffset(debuff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
		if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(debuff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
		buff_debuff_icon_xx-=buff_debuff_icon_width;
	}
		
	for(var i = 0;i<ds_list_size(buff_list);i++)
	{			
		var buff = buff_list[| i];
		var buff_icon = short_term_buff_debuff_get_icon(buff);
		draw_sprite_in_area(buff_icon,buff_debuff_icon_xx+sprite_get_xoffset(buff_icon),buff_debuff_icon_yy+sprite_get_yoffset(buff_icon),buff_debuff_icon_width,buff_debuff_icon_height);
		if mouse_over_area(buff_debuff_icon_xx,buff_debuff_icon_yy,buff_debuff_icon_width,buff_debuff_icon_height) draw_set_floating_tooltip(short_term_buff_debuff_get_tooltip(buff),mouse_x,mouse_y-10,fa_center,fa_bottom,false);
		buff_debuff_icon_xx-=buff_debuff_icon_width;
	}
	
	
	
	// bars
	var bar_xx = xx;
	var bar_yy = yy+15;
	var bar_width = sprite_width-padding*2;
	var bar_height = 18;
	var bar_padding = 2;
	var outline_color = c_black;
	
	draw_set_font(font_01n_sc);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	var bar_text_size_ratio = 1;
	var bar_text_outline_width = 1;
	var bar_text_outline_fid = 10;
	draw_set_alpha(image_alpha);
	
	var bars_hover = mouse_over_area(bar_xx,bar_yy+bar_height+bar_padding,bar_width,bar_height*6+bar_padding*6);
	
		// Vitality
		bar_yy+=bar_height+bar_padding;
		var bar_color = COLOR_VITALITY;
		var bar_color_dark = COLOR_VITALITY_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_vitalitymax_visual;
		var bar_value = char_vitality;
		var bar_value_visual = char_vitality_visual;
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar_text = "Vitality: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));
		

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
        if bars_hover && !menu_exists()
		{
			draw_set_color(c_lightblack);
			draw_set_alpha(image_alpha*0.9);
			draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
			draw_set_color(COLOR_VITALITY);
			draw_set_alpha(image_alpha);
			draw_text_transformed(bar_xx,bar_yy,"Vitality: "+string(round(bar_value))+"/"+string(round(char_get_vitality_max(char))),0.5,0.5,0);
		}
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width/2,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);


		// hp
		bar_yy+=bar_height+bar_padding;
		var bar_color = COLOR_HP;
		var bar_color_dark = COLOR_HP_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_healthmax_visual;
		var bar_value = char_health;
		var bar_value_visual = char[? GAME_CHAR_HEALTH_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar_text = "Health: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));
		

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
        if bars_hover && !menu_exists()
		{
			draw_set_color(c_lightblack);
			draw_set_alpha(image_alpha*0.9);
			draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
			draw_set_color(COLOR_HP);
			draw_set_alpha(image_alpha);
			draw_text_transformed(bar_xx,bar_yy,"Health: "+string(round(bar_value))+"/"+string(round(char_get_health_max(char))),0.5,0.5,0);
		}
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width/2,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);

		// lust
		bar_yy+=bar_height+bar_padding;
		var bar_color = COLOR_LUST;
		var bar_color_dark = COLOR_LUST_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_lustmax_visual;
		var bar_value = char_lust;
		var bar_value_visual = char[? GAME_CHAR_LUST_VISUAL];
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar_text = "Lust: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
        if bars_hover && !menu_exists()
		{
			draw_set_color(c_lightblack);
			draw_set_alpha(image_alpha*0.9);
			draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
			draw_set_color(COLOR_LUST);
			draw_set_alpha(image_alpha);
			draw_text_transformed(bar_xx,bar_yy,"Lust: "+string(round(bar_value))+"/"+string(round(char_get_lust_max(char))),0.5,0.5,0);
		}
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width/2,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);
		
		
		// stamina
		bar_yy+=bar_height+bar_padding;
		var bar_color = COLOR_STAMINA;
		var bar_color_dark = COLOR_STAMINA_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_staminamax_visual;
		var bar_value = char_stamina;
		var bar_value_visual = char_stamina_visual;
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar_text = "Stamina: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
        if bars_hover && !menu_exists()
		{
			draw_set_color(c_lightblack);
			draw_set_alpha(image_alpha*0.9);
			draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
			draw_set_color(COLOR_STAMINA);
			draw_set_alpha(image_alpha);
			draw_text_transformed(bar_xx,bar_yy,"Stamina: "+string(round(bar_value))+"/"+string(round(char_get_stamina_max(char))),0.5,0.5,0);
		}
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width/2,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);
		
		
		// toru
		bar_yy+=bar_height+bar_padding;
		var bar_color = COLOR_TORU;
		var bar_color_dark = COLOR_TORU_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_torumax_visual;
		var bar_value = char_toru;
		var bar_value_visual = char_toru_visual;
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar_text = "Toru: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
        if bars_hover && !menu_exists()
		{
			draw_set_color(c_lightblack);
			draw_set_alpha(image_alpha*0.9);
			draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
			draw_set_color(COLOR_TORU);
			draw_set_alpha(image_alpha);
			draw_text_transformed(bar_xx,bar_yy,"Toru: "+string(round(bar_value))+"/"+string(round(char_get_toru_max(char))),0.5,0.5,0);
		}
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width/2,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);
		
		// Exp
		bar_yy+=bar_height+bar_padding;
		var bar_color = COLOR_EXP;
		var bar_color_dark = COLOR_EXP_DARK;
		var bar_color_outline = c_black;
		var bar_value_max_visual = char_get_exp_max(char);
		var bar_value = char_exp;
		var bar_value_visual = char_exp_visual;
		var bar_width_per_value = bar_width / bar_value_max_visual;
		var bar_text = "EXP: "+string(round(bar_value_visual))+"/"+string(round(bar_value_max_visual));

		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy+bar_height,bar_color,bar_color,bar_color,bar_color,false);
		//draw_rectangle_color(bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_xx+bar_width_per_value*max(bar_value,bar_value_visual),bar_yy+bar_height,bar_color_dark,bar_color_dark,bar_color_dark,bar_color_dark,false);
        draw_sprite_in_visible_area(spr_bar_white,1,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),c_lightblack,image_alpha,bar_xx,bar_yy,bar_width,bar_height-1);
		draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color,image_alpha,bar_xx,bar_yy,bar_width_per_value*min(bar_value,bar_value_visual),bar_height-1);
        draw_sprite_in_visible_area(spr_bar_white,0,bar_xx,bar_yy,bar_width/sprite_get_width(spr_bar_white),bar_height/sprite_get_height(spr_bar_white),bar_color_dark,image_alpha,bar_xx+bar_width_per_value*min(bar_value,bar_value_visual),bar_yy,bar_width_per_value*abs(bar_value-bar_value_visual),bar_height-1);
        if bars_hover && !menu_exists()
		{
			draw_set_color(c_lightblack);
			draw_set_alpha(image_alpha*0.9);
			draw_rectangle(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,false);
			draw_set_color(COLOR_EXP);
			draw_set_alpha(image_alpha);
			draw_text_transformed(bar_xx,bar_yy,"Exp: "+string(round(bar_value))+"/"+string(round(char_get_exp_max(char))),0.5,0.5,0);
		}
		
		//draw_rectangle_color(bar_xx,bar_yy,bar_xx+bar_width,bar_yy+bar_height,bar_color_outline,bar_color_outline,bar_color_outline,bar_color_outline,true);
		//draw_text_outlined(bar_xx+bar_width/2,bar_yy+bar_height/2,bar_text,bar_color,bar_color_dark,bar_text_size_ratio,bar_text_outline_width,bar_text_outline_fid);

	// polygon
	draw_set_font(font_01n_reg);
	draw_stats_polygon(x+sprite_width/2,y+sprite_height*2/3+35,80,char_end_visual,char_fou_visual,char_str_visual,char_dex_visual,char_wis_visual,char_int_visual,char_spi_visual,false,false);



	/*
	// polygon
	var polygon_radius = sprite_width/3.5;
	var polygon_radius_text = polygon_radius+20;
	var polygon_centerx = x+sprite_width/2;
	var polygon_centery = bar_yy+bar_height+polygon_radius+40;
	var polygon_corner = 7;
	var polygon_angle_per_corner = 360 / polygon_corner;
	var polygon_angle = polygon_angle_per_corner/2;
	var polygon_value_max = 100;
	var polygon_x_list_text = ds_list_create();
	var polygon_y_list_text = ds_list_create();
	var polygon_x_list_outline = ds_list_create();
	var polygon_y_list_outline = ds_list_create();
	var polygon_x_list = ds_list_create();
	var polygon_y_list = ds_list_create();
	var polygon_x_list_visual = ds_list_create();
	var polygon_y_list_visual = ds_list_create();
	var polygon_color = c_dkgray;
	var polygon_color_light = c_dkgray;
	var polygon_color_outline = c_black;
	var polygon_line_width = 3;
	var polygon_line_outwidth = 5;
	
	var polygon_stats_value_name = ds_list_create();
	var polygon_stats_value = ds_list_create();
	var polygon_stats_value_visual = ds_list_create();
		
		// list of points
		ds_list_add(polygon_stats_value_name,"STR","FOU","DEX","END","INT","WIS","SPI");
		ds_list_add(polygon_stats_value,char_str,char_fou,char_dex,char_end,char_int,char_wis,char_spi);
		ds_list_add(polygon_stats_value_visual,char_str_visual,char_fou_visual,char_dex_visual,char_end_visual,char_int_visual,char_wis_visual,char_spi_visual);
	
		// processing
		for (var i = 0; i < ds_list_size(polygon_stats_value_name);i++)
		{
			var polygon_radius_value = polygon_stats_value[| i] / polygon_value_max * polygon_radius;
			var polygon_radius_value_visual = polygon_stats_value_visual[| i] / polygon_value_max * polygon_radius;

			ds_list_add(polygon_x_list_text, polygon_centerx + lengthdir_x(polygon_radius_text,polygon_angle));
			ds_list_add(polygon_y_list_text, polygon_centery + lengthdir_y(polygon_radius_text,polygon_angle));
			ds_list_add(polygon_x_list_outline, polygon_centerx + lengthdir_x(polygon_radius,polygon_angle));
			ds_list_add(polygon_y_list_outline, polygon_centery + lengthdir_y(polygon_radius,polygon_angle));
			ds_list_add(polygon_x_list, polygon_centerx + lengthdir_x(polygon_radius_value,polygon_angle));
			ds_list_add(polygon_y_list, polygon_centery + lengthdir_y(polygon_radius_value,polygon_angle));
			ds_list_add(polygon_x_list_visual, polygon_centerx + lengthdir_x(polygon_radius_value_visual,polygon_angle));
			ds_list_add(polygon_y_list_visual, polygon_centery + lengthdir_y(polygon_radius_value_visual,polygon_angle));
			
			polygon_angle+=polygon_angle_per_corner;
		}
		
		// drawing
		for (var i = 0; i < ds_list_size(polygon_x_list_visual);i++)
		{
			if i == 0 var j = ds_list_size(polygon_x_list_visual) -1;
			else var j = i - 1;
			
			// value (visual)
			draw_set_color(polygon_color_light);
			draw_line_width(polygon_x_list_visual[| j], polygon_y_list_visual[| j], polygon_x_list_visual[| i], polygon_y_list_visual[| i], polygon_line_width);
			
			// value
			draw_set_color(polygon_color);
			draw_line_width(polygon_x_list[| j], polygon_y_list[| j], polygon_x_list[| i], polygon_y_list[| i], polygon_line_width);
			
			// outline
			draw_set_color(polygon_color_outline);
			draw_line_width(polygon_x_list_outline[| j], polygon_y_list_outline[| j], polygon_x_list_outline[| i], polygon_y_list_outline[| i], polygon_line_outwidth);
			
			// text
			draw_text(polygon_x_list_text[| i],polygon_y_list_text[| i],polygon_stats_value_name[| i]);

		}
		
		// clear
		ds_list_destroy(polygon_x_list_text);
		ds_list_destroy(polygon_y_list_text);	
		ds_list_destroy(polygon_x_list_outline);
		ds_list_destroy(polygon_y_list_outline);
		ds_list_destroy(polygon_x_list);
		ds_list_destroy(polygon_y_list);
		ds_list_destroy(polygon_x_list_visual);
		ds_list_destroy(polygon_y_list_visual);
	
		ds_list_destroy(polygon_stats_value_name);
		ds_list_destroy(polygon_stats_value);	
		ds_list_destroy(polygon_stats_value_visual);	



		
		// Basic stats
		draw_reset();
		draw_set_alpha(image_alpha);
		var spr = spr_gui_info_icon;
		var spr_width = sprite_get_width(spr);
		var spr_height = sprite_get_height(spr);
		var padding = 10;
		var xx = x + padding;
		var yy = y + padding;
		var hover = mouse_over_area(xx,yy,spr_width,spr_height);
		
		draw_sprite(spr,-1,xx,yy);
		
		if hover
			draw_tooltip_basic_stats(x+sprite_width+padding,y+padding,char);
			
		// Fighting stats
		draw_reset();
		var spr = spr_gui_fight_icon;
		var spr_width = sprite_get_width(spr);
		var spr_height = sprite_get_height(spr);
		var padding = 10;
		var xx = x+sprite_width-spr_width-padding;
		var yy = y + padding;
		var hover = mouse_over_area(xx,yy,spr_width,spr_height);
		
		draw_sprite(spr,-1,xx,yy);
		
		if hover
			draw_tooltip_fighting_stats(x+sprite_width+padding,y+padding,char);
		*/




//draw_placeholder_sprite_notes("Player Information");

//draw_sprite(spr_white,-1,char[? GAME_CHAR_X_FLOATING_TEXT],char[? GAME_CHAR_Y_FLOATING_TEXT]);