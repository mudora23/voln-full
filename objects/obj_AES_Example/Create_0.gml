/// Demo
// Set up content and keys to encrypt
var content = hex_to_array("328831E0435A3137F6309807A88DA234"),
        key = hex_to_array("2B28AB097EAEF7CF15D2154F16A6883C");
    
        
//create a random initialization vector and activate it
var iv = aes_generate_iv();
aes_set_iv(iv);


var encrypted_content = aes_encrypt_array(content, key);

var decrypted_content = aes_decrypt_array(encrypted_content, key);


show_message("Encrypted content: " + array_to_hex(encrypted_content));
show_message("Decrypted content: " + array_to_hex(decrypted_content));

/*
    Also can use encoding types:
        - array_to_base64
        - array_to_string
          Etc...
*/

/* */
/*  */
