{
    "id": "177f2568-1527-4a5c-b120-2be0cef67b68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_command_panel",
    "eventList": [
        {
            "id": "e9f76a45-ed63-4220-9179-23667fef8cfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "177f2568-1527-4a5c-b120-2be0cef67b68"
        },
        {
            "id": "387eb0c8-f1f2-4e7b-95b7-590f927e3d9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "177f2568-1527-4a5c-b120-2be0cef67b68"
        },
        {
            "id": "c4303785-5e54-4eb3-860d-ac0deb3cb0ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "177f2568-1527-4a5c-b120-2be0cef67b68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2521ffd5-c034-4b1f-b0aa-dcf9e4a45596",
    "visible": true
}