/// @description Insert description here
// You can write your code in this editor

draw_reset();
draw_set_alpha(image_alpha);

var spr_connector = spr_gui_brass_connector_c;
var spr_connector_ratio = 0.5;
var spr_connector_width = sprite_get_width(spr_connector)*spr_connector_ratio;
var spr_connector_height = sprite_get_height(spr_connector)*spr_connector_ratio;

draw_sprite_ext(spr_gui_brass_connector_c,-1,x+area_width/2-spr_connector_height/2,y-spr_connector_width/2,spr_connector_ratio,spr_connector_ratio,270,c_white,image_alpha);
draw_sprite_ext(spr_gui_brass_connector_c,-1,x+area_width/2+spr_connector_height/2,y-spr_connector_width/2,spr_connector_ratio,spr_connector_ratio,90,c_white,image_alpha);


draw_sprite_in_area_fill(spr_gui_bg_seemless,x,y,area_width,area_height,make_color_rgb(obj_control.colorR_side,obj_control.colorG_side,obj_control.colorB_side),image_alpha,fa_right,fa_bottom);
gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);
draw_sprite_in_area_stretch_to_fit(spr_gui_overlay_multiply,0,x,y,area_width,area_height,c_white,image_alpha);
gpu_set_blendmode(bm_normal);



var choice_name_list = obj_control.var_map[? VAR_CHOICES_LIST_NAME];
var choice_script_list = obj_control.var_map[? VAR_CHOICES_LIST_SCRIPT];
var choice_posi_or_label_list = obj_control.var_map[? VAR_CHOICES_LIST_POSI_OR_LABEL];
var choice_tooltip_list = obj_control.var_map[? VAR_CHOICES_LIST_TOOLTIP];
var choice_surface_list = obj_control.var_map[? VAR_CHOICES_LIST_SURFACE];

var choice_extrascript_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT];
var choice_extrascript_arg0_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG0];
var choice_extrascript_arg1_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG1];
var choice_extrascript_arg2_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG2];
var choice_extrascript_arg3_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG3];
var choice_extrascript_arg4_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG4];
var choice_extrascript_arg5_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG5];
var choice_extrascript_arg6_list = obj_control.var_map[? VAR_CHOICES_LIST_EXTRASCRIPT_ARG6];


var choice_number = ds_list_size(choice_name_list);
var choice_height = max(area_height/choice_number,choice_height_max);
var choice_width = area_width;

/*if ds_list_size(choice_name_list) > 0
{
	if keyboard_check_pressed(vk_up)
		obj_control.current_keyboard_choice = max(obj_control.current_keyboard_choice-1,1);
	if keyboard_check_pressed(vk_down)
		obj_control.current_keyboard_choice = min(obj_control.current_keyboard_choice+1,ds_list_size(choice_name_list));
}*/









		if mouse_over_area(x,y,area_width,area_height) && mouse_wheel_down()
			choice_yoffset_target += 100;
		if mouse_over_area(x,y,area_width,area_height) && mouse_wheel_up()
			choice_yoffset_target -= 100;

		choice_yoffset_target = clamp(choice_yoffset_target,0,max(0,choice_number*choice_height-area_height));
		choice_yoffset = smooth_approach_room_speed(choice_yoffset,choice_yoffset_target,5);

		var scroll_spr = spr_button_scroll;
		var scroll_spr_width = sprite_get_width(scroll_spr);
		var scroll_spr_height = sprite_get_height(scroll_spr);
		if choice_number * choice_height > area_height
		{
			var scroll_xx = x + area_width + 22;
			var scroll_yy = y + (choice_yoffset/(choice_number*choice_height-area_height))*(area_height - scroll_spr_height);
			var scroll_hover_r = mouse_over_area(scroll_xx,scroll_yy,scroll_spr_width,scroll_spr_height);
			if mouse_check_button_pressed(mb_left) && scroll_hover_r
				choice_holding_scroll = true;
			
			draw_sprite_ext(spr_gui_brass_connector_c,-1,scroll_xx-5,scroll_yy+ scroll_spr_height/2,-0.3,0.5,0,c_white,image_alpha*obj_command_panel.choice_image_alpha);
			draw_sprite_ext(spr_button_scroll,-1,scroll_xx,scroll_yy,1,1,0,c_white,(image_alpha/2+(scroll_hover_r || choice_holding_scroll)*image_alpha/2)*obj_command_panel.choice_image_alpha);
			
			var scroll_xx = x - 22;
			var scroll_hover_l = mouse_over_area(scroll_xx-scroll_spr_width,scroll_yy,scroll_spr_width,scroll_spr_height);
			if mouse_check_button_pressed(mb_left) && scroll_hover_l
				choice_holding_scroll = true;
				
			draw_sprite_ext(spr_gui_brass_connector_c,-1,scroll_xx+5,scroll_yy+ scroll_spr_height/2,-0.3,0.5,0,c_white,image_alpha*obj_command_panel.choice_image_alpha);
			draw_sprite_ext(spr_button_scroll,-1,scroll_xx,scroll_yy,-1,1,0,c_white,(image_alpha/2+(scroll_hover_l || choice_holding_scroll)*image_alpha/2)*obj_command_panel.choice_image_alpha);
			
			if choice_holding_scroll
			{
				var scroll_ratio = clamp((mouse_y - y - scroll_spr_height/2)/(area_height-scroll_spr_height),0,1);
				choice_yoffset = max(0,(choice_number*choice_height-area_height)) * scroll_ratio
				choice_yoffset_target = choice_yoffset;
			}
			
			
			
			if !mouse_check_button(mb_left)
				choice_holding_scroll = false;
		}



/*if !surface_exists(choice_surface) && ds_list_size(choice_name_list) > 0
{
	draw_set_valign(fa_center);
	draw_set_halign(fa_center);
	draw_set_font(obj_control.font_01_reg);
	choice_surface = surface_create(area_width,choice_height_max*15);
	surface_set_target(choice_surface);

	draw_clear_alpha(c_black, 0);
	
	var choice_yy = (choice_height*1.07)/2;
	var choice_xx = choice_width/2;
	
	for(var i = 0; i < ds_list_size(choice_name_list); i++)
	{
		var choice_name_without_markup = string_replace(choice_name_list[| i],"[#]","");
		choice_name_without_markup = string_replace(choice_name_without_markup,"[!]","");
		choice_name_without_markup = string_replace(choice_name_without_markup,"[?]","");

			var out_col = c_black;
			var in_col = c_white;
			gpu_set_blendmode_ext(bm_src_alpha, bm_one);

		
		
		if string_count("\r",choice_name_list[| i]) > 0
		{
			
			var size_ratio = min(1,(choice_height-20)/string_height(choice_name_without_markup));
			draw_text_transformed(choice_xx,choice_yy,choice_name_without_markup,size_ratio,size_ratio,0);
		}
		else
		{
			var size_ratio = min(1,(choice_height-20)/string_height_ext(choice_name_without_markup,35,area_width - 20));
			draw_text_ext_transformed(choice_xx,choice_yy,choice_name_without_markup,35,area_width - 20,size_ratio,size_ratio,0);
		}
			
		choice_yy+=choice_height;
		gpu_set_blendmode(bm_normal);
	}

	
	surface_reset_target();
	draw_reset();

	// choie alpha
	obj_command_panel.choice_image_alpha = 1;
	obj_command_panel.choice_image_alpha_increasing = true;
}*/


/*if obj_main.updating_dialog_surface_step > 100000 || obj_control.game_map[? GAME_SCENE_DIALOG_MODE] == GAME_DIALOG_NONE || obj_control.game_map[? GAME_SCENE_MODE] == GAME_MODE_BATTLE
	obj_command_panel.choice_image_alpha_increasing = true;
if obj_command_panel.choice_image_alpha_increasing
	obj_command_panel.choice_image_alpha = approach(obj_command_panel.choice_image_alpha,1,FADE_NORMAL_SEC/room_speed);
*/

if obj_command_panel.choice_image_alpha_increasing
	obj_command_panel.choice_image_alpha = approach(obj_command_panel.choice_image_alpha,1,FADE_NORMAL_SEC/room_speed);
else
	obj_command_panel.choice_image_alpha = 0;
	
if obj_command_panel.choice_image_alpha > 0.2
{

	for(var i = 0; i < ds_list_size(choice_name_list); i++)
	{
		var choice_xx = x;
		var choice_yy = y+i*choice_height;
		var choice_hover = mouse_over_area(x,y,area_width,area_height) && mouse_over_area(choice_xx,choice_yy-choice_yoffset,choice_width,choice_height) && !menu_exists();
		if choice_hover obj_control.current_keyboard_choice = 0;
		
		
		if choice_hover || (obj_control.current_keyboard_choice == i +1)
		{
			if obj_control.allies_choice_enable{obj_control.hovered_char_on_screen_delay = 2; obj_control.hovered_char_on_screen = char_id_get_char(real(choice_extrascript_arg0_list[| i]));}
			if obj_control.enemies_choice_enable{obj_control.hovered_char_on_screen_delay = 2; obj_control.hovered_char_on_screen = char_id_get_char(real(choice_extrascript_arg0_list[| i]));}
			
		}
		
		
	
		if (
		    ((choice_hover || (obj_control.current_keyboard_choice == i +1))) ||
		    (obj_control.allies_choice_enable && obj_control.hovered_char_on_screen == char_id_get_char(real(choice_extrascript_arg0_list[| i]))) || 
		    (obj_control.enemies_choice_enable && obj_control.hovered_char_on_screen == char_id_get_char(real(choice_extrascript_arg0_list[| i])))
		   )
		    && string_count("[?]",choice_name_list[| i]) == 0
		{

			draw_sprite_in_visible_area(spr_button_choice,4+string_count("[#]",choice_name_list[| i])*2+string_count("[?]",choice_name_list[| i])*4,choice_xx,choice_yy-choice_yoffset,choice_width/sprite_get_width(spr_button_choice),choice_height/sprite_get_height(spr_button_choice),c_white,image_alpha*0.7*obj_command_panel.choice_image_alpha,x,y,area_width,area_height);
	        draw_sprite_in_visible_area(spr_button_choice,5+string_count("[#]",choice_name_list[| i])*2+string_count("[?]",choice_name_list[| i])*4,choice_xx,choice_yy-choice_yoffset,choice_width/sprite_get_width(spr_button_choice),choice_height/sprite_get_height(spr_button_choice),c_white,image_alpha*0.7*obj_command_panel.choice_image_alpha,x,y,area_width,area_height);
        
			/*if obj_control.enemies_choice_enable
			{
				if choice_scene_arg0_list[| i] < enemy_get_number()
				obj_control.current_enemies_choice = choice_scene_arg0_list[| i];
			}*/

		}
		else
		{

			draw_sprite_in_visible_area(spr_button_choice,4+string_count("[#]",choice_name_list[| i])*2+string_count("[?]",choice_name_list[| i])*4,choice_xx,choice_yy-choice_yoffset,choice_width/sprite_get_width(spr_button_choice),choice_height/sprite_get_height(spr_button_choice),c_white,image_alpha*0.2*obj_command_panel.choice_image_alpha,x,y,area_width,area_height);
	        draw_sprite_in_visible_area(spr_button_choice,5+string_count("[#]",choice_name_list[| i])*2+string_count("[?]",choice_name_list[| i])*4,choice_xx,choice_yy-choice_yoffset,choice_width/sprite_get_width(spr_button_choice),choice_height/sprite_get_height(spr_button_choice),c_white,image_alpha*0.2*obj_command_panel.choice_image_alpha,x,y,area_width,area_height);
    
		}
		
		if !surface_exists(choice_surface_list[| i])
		{
			choice_surface_list[| i] = surface_create(choice_width,choice_height);
			surface_set_target(choice_surface_list[| i]);
			draw_clear_alpha(c_black,0);
			
			var choice_name_without_markup = string_replace(choice_name_list[| i],"[#]","");
			choice_name_without_markup = string_replace(choice_name_without_markup,"[!]","");
			choice_name_without_markup = string_replace(choice_name_without_markup,"[?]","");

			gpu_set_blendmode_ext(bm_src_alpha, bm_dest_alpha);

				draw_set_halign(fa_center);
				draw_set_valign(fa_center);
			if string_count("\r",choice_name_list[| i]) > 0
			{
				var size_ratio = min(0.8,(choice_height-20)/string_height(choice_name_without_markup));
				draw_text_transformed(choice_width/2,choice_height/2,choice_name_without_markup,size_ratio,size_ratio,0);

			}
			else
			{
				var size_ratio = clamp((choice_height-20)/string_height_ext(choice_name_without_markup,50,area_width -20),0.6,0.8);
				draw_text_ext_transformed(choice_width/2,choice_height/2,choice_name_without_markup,50,(area_width - 20)/size_ratio,size_ratio,size_ratio,0);
			}
			gpu_set_blendmode(bm_normal);
			surface_reset_target();
		}
		
		draw_surface_in_visible_area(choice_surface_list[| i],choice_xx,choice_yy-choice_yoffset,1,1,c_white,obj_command_panel.choice_image_alpha,x,y,area_width,area_height);
		
		
	
		if (choice_hover || (obj_control.current_keyboard_choice == i +1)) && choice_tooltip_list[| i] != ""
			draw_set_floating_tooltip(choice_tooltip_list[| i],x+area_width/2,y,fa_center,fa_bottom,true);
		

	
	

	
		/*if obj_control.debug == 1
		{
			draw_set_halign(fa_left);
			draw_text_outlined(choice_xx+choice_width,choice_yy+(choice_height*1.07)/2,"scene: "+string(choice_scene_list[| i])+" \rarg0: "+string(choice_scene_arg0_list[| i]),c_white,c_black,0.7,4,25);
			draw_set_halign(fa_right);
			draw_text_outlined(choice_xx,choice_yy+(choice_height*1.07)/2+30,"script: "+string(choice_script_list[| i])+" \rarg0: "+string(choice_script_arg0_list[| i]),c_white,c_black,0.7,4,25);
		}
		*/
		
		if (((choice_hover || (obj_control.current_keyboard_choice == i +1))) ||
		    (obj_control.allies_choice_enable && obj_control.hovered_char_on_screen == char_id_get_char(real(choice_extrascript_arg0_list[| i]))) || 
		    (obj_control.enemies_choice_enable && obj_control.hovered_char_on_screen == char_id_get_char(real(choice_extrascript_arg0_list[| i])))
			)
			&& action_button_pressed_get()
			&& string_count("[?]",choice_name_list[| i]) == 0
			&& !menu_exists()
	
		{
			action_button_pressed_set(false);
			voln_play_sfx(sfx_mouse_click_choice); 
			////show_debug_message("------ Command Panel: A choice has been chosen ------");

			var choice_script = choice_script_list[| i];
			var choice_posi_or_label = choice_posi_or_label_list[| i];
			var choice_extrascript = choice_extrascript_list[| i];
			var choice_extrascript_arg0 = choice_extrascript_arg0_list[| i];
			var choice_extrascript_arg1 = choice_extrascript_arg1_list[| i];
			var choice_extrascript_arg2 = choice_extrascript_arg2_list[| i];
			var choice_extrascript_arg3 = choice_extrascript_arg3_list[| i];
			var choice_extrascript_arg4 = choice_extrascript_arg4_list[| i];
			var choice_extrascript_arg5 = choice_extrascript_arg5_list[| i];
			var choice_extrascript_arg6 = choice_extrascript_arg6_list[| i];
			
			if (is_string(choice_script) && choice_script != "") || (!is_string(choice_script) && choice_script != noone)
				scene_jump("",choice_script);
			if (is_string(choice_posi_or_label) && choice_posi_or_label != "") || (!is_string(choice_posi_or_label) && choice_posi_or_label != noone)
				scene_jump(choice_posi_or_label);
			
			if script_get_index_ext(choice_extrascript) != noone
				script_execute_add(0,choice_extrascript,choice_extrascript_arg0,choice_extrascript_arg1,choice_extrascript_arg2,choice_extrascript_arg3,choice_extrascript_arg4,choice_extrascript_arg5,choice_extrascript_arg6);
			
			scene_choices_list_clear();
		}
	}

	//draw_surface_in_visible_area(choice_surface,x,y-choice_yoffset,1,1,c_white,1*obj_command_panel.choice_image_alpha,x,y,area_width,area_height);
	draw_reset();
}
draw_borders_in_area(spr_gui_borderB_top_left_c,spr_gui_borderB_top_right_c,spr_gui_borderB_bot_left_c,spr_gui_borderB_bot_right_c,spr_gui_borderB_top_seemless_c,spr_gui_borderB_bot_seemless_c,spr_gui_borderB_left_seemless_c,spr_gui_borderB_right_seemless_c,x,y,area_width,area_height,1,fa_right,fa_bottom);


draw_reset();
