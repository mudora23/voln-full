/// @description Insert description here
// You can write your code in this editor

uni_settings = shader_get_uniform(shd_vignette_noise, "u_settings");
uni_vignette_colour = shader_get_uniform(shd_vignette_noise, "u_vignette_colour");


colR_min = 252;
colG_min = 181;
colB_min = 159;

colR_max = 255;
colG_max = 255;
colB_max = 255;

colR = (colR_max - colR_min)/2;
colG = (colG_max - colG_min)/2;
colB = (colB_max - colB_min)/2;

colR_inc = false;
colG_inc = false;
colB_inc = false;

noise_min = 0.2;
noise_max = 0.3;
noise = 0.25;
noise_inc = false;

depth = DEPTH_MENU+1;



panel_spr = spr_title_screen_panel;
panel_xx = room_width / 2;
panel_yy = room_height + sprite_get_height(panel_spr);
panel_yy_target = panel_yy;
alarm[0] = room_speed * 0.5;

start_ratio = 1;
start_yy_offset_target = -500;
start_yy_offset = -500;
load_ratio = 1;
load_yy_offset_target = -200;
load_yy_offset = -200;
options_ratio = 1;
options_yy_offset_target = 100;
options_yy_offset = 100;
exit_ratio = 1;
exit_yy_offset_target = 400;
exit_yy_offset = 400;
patreon_ratio = 0.8;
patreon_yy_offset_target = 700;
patreon_yy_offset = 700;

surface2 = noone;






