/// @description Insert description here
// You can write your code in this editor

//if !surface_exists(surface1)
//	surface1 = surface_create(display_get_gui_width(),display_get_gui_height());
	
//surface_set_target(surface1);

draw_self();


gpu_set_blendmode_ext(bm_dest_color, bm_inv_src_alpha);

panel_yy = smooth_approach_room_speed(panel_yy,panel_yy_target,SMOOTH_SLIDE_NORMAL_SEC);
draw_sprite_ext(panel_spr,-1,panel_xx,panel_yy,1,1,0,c_white,0.8);

gpu_set_blendmode(bm_normal);

draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_font(font_01b_reg);

var xx = panel_xx;
start_yy_offset = smooth_approach_room_speed(start_yy_offset,start_yy_offset_target,SMOOTH_SLIDE_SLOW_SEC);


var text = "Support us on Patreon!";
patreon_yy_offset = smooth_approach_room_speed(patreon_yy_offset,patreon_yy_offset_target,SMOOTH_SLIDE_SLOW_SEC);
var hover = mouse_over_area_center(xx,panel_yy+patreon_yy_offset,string_width(text)*patreon_ratio,string_height(text)*patreon_ratio) && !menu_exists();
draw_set_color(c_yellow);
if hover patreon_ratio = smooth_approach_room_speed(patreon_ratio,1.0,FADE_VERYFAST_SEC);
else patreon_ratio = smooth_approach_room_speed(patreon_ratio,0.7,FADE_NORMAL_SEC);

draw_text_transformed(xx,panel_yy+patreon_yy_offset,text,patreon_ratio,patreon_ratio,0);

if hover && action_button_pressed_get()
{
	action_button_pressed_set(false);
	voln_play_sfx(sfx_mouse_click);
	url_open("https://www.patreon.com/Voln");
}


draw_set_color(c_white);



var text = "Exit";
exit_yy_offset = smooth_approach_room_speed(exit_yy_offset,exit_yy_offset_target,SMOOTH_SLIDE_SLOW_SEC);
var hover = mouse_over_area_center(xx,panel_yy+exit_yy_offset,string_width(text)*exit_ratio,string_height(text)*exit_ratio) && !menu_exists();

if hover exit_ratio = smooth_approach_room_speed(exit_ratio,1.2,FADE_VERYFAST_SEC);
else exit_ratio = smooth_approach_room_speed(exit_ratio,0.8,FADE_NORMAL_SEC);

draw_text_transformed(xx,panel_yy+exit_yy_offset,text,exit_ratio,exit_ratio,0);

if hover && action_button_pressed_get()
{
	action_button_pressed_set(false);
	voln_play_sfx(sfx_mouse_click);
	game_end();
}

var text = "Options";
options_yy_offset = smooth_approach_room_speed(options_yy_offset,options_yy_offset_target,SMOOTH_SLIDE_SLOW_SEC);
var hover = mouse_over_area_center(xx,panel_yy+options_yy_offset,string_width(text)*options_ratio,string_height(text)*options_ratio) && !menu_exists();

if hover options_ratio = smooth_approach_room_speed(options_ratio,1.2,FADE_VERYFAST_SEC);
else options_ratio = smooth_approach_room_speed(options_ratio,0.8,FADE_NORMAL_SEC);

draw_text_transformed(xx,panel_yy+options_yy_offset,text,options_ratio,options_ratio,0);

if hover && action_button_pressed_get()
{
	action_button_pressed_set(false);
	voln_play_sfx(sfx_mouse_click);
	instance_create_depth(0,0,DEPTH_MENU,obj_menu);
	obj_menu.menu_open = "Options";
}

var text = "Load";

load_yy_offset = smooth_approach_room_speed(load_yy_offset,load_yy_offset_target,SMOOTH_SLIDE_SLOW_SEC);
var hover = mouse_over_area_center(xx,panel_yy+load_yy_offset,string_width(text)*load_ratio,string_height(text)*load_ratio) && !menu_exists();

if hover load_ratio = smooth_approach_room_speed(load_ratio,1.2,FADE_VERYFAST_SEC);
else load_ratio = smooth_approach_room_speed(load_ratio,0.8,FADE_NORMAL_SEC);


draw_text_transformed(xx,panel_yy+load_yy_offset,text,load_ratio,load_ratio,0);

if hover && action_button_pressed_get()
{
	action_button_pressed_set(false);
	voln_play_sfx(sfx_mouse_click);
	instance_create_depth(0,0,DEPTH_MENU,obj_menu);
	obj_menu.menu_open = "Load";
}


var text = "Start";
var hover = mouse_over_area_center(xx,panel_yy+start_yy_offset,string_width(text)*start_ratio,string_height(text)*start_ratio) && !menu_exists();

if hover start_ratio = smooth_approach_room_speed(start_ratio,1.2,FADE_VERYFAST_SEC);
else start_ratio = smooth_approach_room_speed(start_ratio,0.8,FADE_NORMAL_SEC);


draw_text_transformed(xx,panel_yy+start_yy_offset,text,start_ratio,start_ratio,0);

if hover && action_button_pressed_get()
{
	action_button_pressed_set(false);
	voln_play_sfx(sfx_game_new);
	obj_control.loading = false;

	if obj_control.var_map != noone ds_map_destroy(obj_control.var_map);
	
	var_map_destroy();
	var_map_init();
	var_map_add_missing_keys();
	
	location_map_update_path_and_neighbor_list();
	location_map_update_visited_and_visible_info();

	center_the_ship();
	
	room_goto(room_game);
}

if obj_control.debug_enable
{
	draw_set_font(font_01n_reg);
	draw_set_color(c_gray);
	draw_text_transformed(room_width/2,room_height/2-40,"Debug mode is ON, please press '1' ~ '"+string(obj_control.debug_pages_total)+"' while IN GAME to use debug tools; press 'tab' to enable/disable debug mode.",0.8,0.8,0);
}

draw_reset();








//surface_reset_target();
