{
    "id": "b59c5d58-91c2-4f28-a1b3-fd50885a9b85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title_screen",
    "eventList": [
        {
            "id": "5a50be86-2ed2-4628-bbeb-670dc84e5b22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b59c5d58-91c2-4f28-a1b3-fd50885a9b85"
        },
        {
            "id": "9955ed76-6a4d-47ec-850b-f89442845924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b59c5d58-91c2-4f28-a1b3-fd50885a9b85"
        },
        {
            "id": "80e3d45f-f0b5-4ad8-a3b5-533bd1ebaedf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b59c5d58-91c2-4f28-a1b3-fd50885a9b85"
        },
        {
            "id": "1e9d4e1c-1009-45a2-a2c1-0a679a3441db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b59c5d58-91c2-4f28-a1b3-fd50885a9b85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "11716d9c-cdbb-4c10-8512-1ba9e68d6a80",
    "visible": true
}