/// @description Insert description here
// You can write your code in this editor
if !menu_exists()
{
	
	//ANIMATION
	/*
		ideally this should be moved out the draw event
		for better control...
	*/
	var timeMult = 0.0015; //make the animation slower or faster...
	var time = current_time * timeMult;
	var oscillatorValue = sin(time);
	var oscillatorValueAbs = abs(sin(time));

	//SHADER SETUP

		// Display setup ---------------------------------------------------------------------------
		//display_set_gui_size(room_width, room_height);

		if os_type == os_android
		{     
		    vRatio = display_get_gui_width() / display_get_gui_height(); //distort the sampler uvs according to the screen ratio in the mobile devices...
		}else{
		    vRatio = 1.0;
		}
    
		// Samplers / Textures ---------------------------------------------------------------------
		fog_map0 = smp_fog;
		fog_map1 = smp_fogNormal;

		// Shader setup ----------------------------------------------------------------------------
		var shader    = shd_fog;
		var u_vRatio  = shader_get_uniform(shader, "u_vRatio");
		var u_speed = shader_get_uniform(shader, "u_speed");
		var u_time = shader_get_uniform(shader, "u_time");
		var u_fogColor = shader_get_uniform(shader, "u_fogColor");
		var u_fogScale = shader_get_uniform(shader, "u_fogScale");
		var u_distAmount = shader_get_uniform(shader, "u_distAmount");
		var sampler0  = shader_get_sampler_index(shader, "sampler0")
		var texture0  = sprite_get_texture(fog_map0,0);
		var sampler1  = shader_get_sampler_index(shader, "sampler1")
		var texture1  = sprite_get_texture(fog_map1,0);
		
		var var_resolution_ratio = window_get_size_ratio();

    
		var spe = 0.025;
		var scale = 0.75;
		var dist = 0.005;
    
		var fogColor_RGB;
		fogColor_RGB[0] = 0.9;
		fogColor_RGB[1] = 1.0;
		fogColor_RGB[2] = 0.95;

		surface_set_target(surface2);
		shader_set(shader);
		    shader_set_uniform_f(u_vRatio, vRatio);
		    shader_set_uniform_f(u_time, time);
		    shader_set_uniform_f(u_speed, spe);
		    shader_set_uniform_f(u_fogScale, scale);
		    shader_set_uniform_f(u_distAmount, dist);
		    shader_set_uniform_f(u_fogColor, fogColor_RGB[0],fogColor_RGB[1],fogColor_RGB[2]);
		    gpu_set_texrepeat_ext(sampler0, true);
		    gpu_set_texrepeat_ext(sampler1, true);
		    texture_set_stage(sampler0,texture0);
		    texture_set_stage(sampler1,texture1);
			draw_surface_ext(application_surface,0,0,var_resolution_ratio,var_resolution_ratio,0,c_white,1);
		shader_reset();
		surface_reset_target();



	if colR_inc && colR == colR_max colR_inc = false;
	if !colR_inc && colR == colR_min colR_inc = true;
	if colG_inc && colG == colG_max colG_inc = false;
	if !colG_inc && colG == colG_min colG_inc = true;
	if colB_inc && colB == colB_max colB_inc = false;
	if !colB_inc && colB == colB_min colB_inc = true;

	if colR_inc colR = approach(colR,colR_max,1);
	else colR = approach(colR,colR_min,1);
	if colG_inc colG = approach(colG,colG_max,1);
	else colG = approach(colG,colG_min,1);
	if colB_inc colB = approach(colB,colB_max,1);
	else colB = approach(colB,colB_min,1);

	if noise_inc && noise == noise_max noise_inc = false;
	if !noise_inc && noise == noise_min noise_inc = true;
	if noise_inc noise = approach(noise,noise_max,0.001);
	else noise = approach(noise,noise_min,0.001);

		if !surface_exists(surface2)
			surface2 = surface_create(room_width,room_height);

	

		shader_set(shd_vignette_noise);
			var outcircle = 1.9;
			var incircle = outcircle - 0.6;
			shader_set_uniform_f(uni_settings, incircle, outcircle, noise, 1); //vignette inner circle size, vignette outter circle size, noise strength, noise enable (1 or 0 only).
			shader_set_uniform_f(uni_vignette_colour, colR, colG, colB); //R,G,B - 0 to 255
			//var var_resolution_x = window_get_width(); 
			//var var_resolution_y = window_get_height();
			//var var_resolution_ratio = min(var_resolution_x/room_width,var_resolution_y/room_height);
			draw_surface_ext(surface2,0,0,1,1,0,c_white,1);
		shader_reset();



	draw_set_halign(fa_right);
	draw_set_valign(fa_bottom);
	draw_set_color(c_gray);
	draw_set_font(font_01n_reg);
	draw_set_alpha(0.7);

	var var_resolution_ratio = window_get_size_ratio();

	var version_string = "Beta v"+string(obj_control.version);

	if obj_control.patreon_only version_string += obj_control.patreon_only_string;
	else version_string += obj_control.non_patreon_string;

	//surface_set_target(surface2);
	draw_text_transformed((room_width - 10)*var_resolution_ratio, (room_height - 10)*var_resolution_ratio, version_string,var_resolution_ratio*0.7,var_resolution_ratio*0.7,0);
	//surface_reset_target();

	draw_reset();
	
	
	

	/*if obj_control.debug_enable
	{
		draw_set_font(font_01s_reg);
		var button_xx = 10;
		var button_yy = 10;
		var button_string = "Debug - View Map";
		var button_w = string_width(button_string);
		var button_h = string_height(button_string);
		var button_hover = mouse_over_area(button_xx,button_yy,button_w/var_resolution_ratio,button_h/var_resolution_ratio);
		
		if button_hover
		{
			draw_set_color(c_dkdkwhite);
			draw_rectangle(button_xx,button_yy,button_xx+button_w,button_yy+button_h,false);
			draw_set_color(c_lightblack);
			draw_text(button_xx,button_yy,button_string);
			if action_button_pressed_get()
			{
				room_goto(room_map);	
				action_button_pressed_set(false);
			}
		}
		else
		{
			draw_set_color(c_lightblack);
			draw_rectangle(button_xx,button_yy,button_xx+button_w,button_yy+button_h,false);
			draw_set_color(c_dkdkwhite);
			draw_text(button_xx,button_yy,button_string);	
		}
		
		draw_reset();
	}*/
	

}
